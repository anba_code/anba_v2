C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.PRTSFC                                                        
C*************************          PRTSFC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PRTSFC(IELSET,SIGMG,SIGML,NSOLL,                                
     *                  SIGM,SIGMAG,SIGMAL,                                     
     *                  SIGMAF,GPQ)                                             
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      REAL  SIGMAG,SIGMAL,SIGMAF                                                
      LOGICAL DUMP,INCORE                                                       
                                         
      character*3 lab(3)

      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3         
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELPIA/  LREC,NPI,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),               
     1                T2(3,3),DG(6,6),W                                         
      COMMON /ELPIB/  INT1,INT2,TETA,DEN,IFLTET                                 
      COMMON /ELSTRA/ LRECD,NPINTG,ENNEST(3,12),CPINT(2),DAREA,BETA,            
     1                T11(3,3),T22(3,3),DGELAS(6,6),WGHT                        
      COMMON /ELSTRB/ INTG1,INTG2,DENSI,THICK,ANGLAM                            
      COMMON /SISRIF/ XS,YS,SS,CS,sclx,scly,IFLSR,iflsro,iflsrs                           
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /CAZZO/ NSOL
C                                                                               
      DIMENSION IELSET(1)
      dimension sl(6),sg(6),sf(6)                                                       
      DIMENSION SIGMG(3,1),SIGML(3,1),EPSP(3,1)                                 
      DIMENSION SIGM(1),GPQ(3,6),EPSIL(NPSI,1)                                  
      DIMENSION SIGMAG(NPSI,1),SIGMAL(NPSI,1),EPSI(NPSI,1)                      
      DIMENSION SIGMAF(NPSI,1),EPSIF(NPSI,1)
      dimension vel(6),idlam(30),spes(30),rotaz(30)                  
      data lab/'sz ','sx ','txz'/
      NSOL = NSOLL
                                    
C                                                                               
      CALL INTEST                                                               
      WRITE(IOUT,6110)                                                          
      WRITE(IOUT,6100)(I,(GPQ(I,K),K=1,6),I=1,3)                                
      RETURN                                                                    
6100  FORMAT(11X,'B(',I1,')',1X,6E15.6)                                         
6110  FORMAT(//,40X,'*** B  - INTEGRALI DEGLI SFORZI SULLA SEZIONE ***'         
     1///                T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                   
     2  T86,'MY',T101,'MZ'/)                                                    
C                                                                               
      ENTRY PRTCOR(SIGM,IELSET,NESEL,ixsol)                                      
C                                                                               
C                                                                               
C     STAMPA GLI SFORZI NEI CORRENTI                                            
C                                                                               
C                                                                               
C                                     
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif
      
      LSUP=40                                                                   
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,1110)                                                          
      else
      WRITE(Iunstr,1111)                                                          
      endif
                                                
      DO 1500 IEL=1,NCORR                                                       
      IF(IEL.LE.LSUP) GOTO 150                                                  

      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,1110)                                                          
      else
      WRITE(Iunstr,1111)                                                          
      endif

      LSUP=LSUP+40                                                              
C                                                                               
 150  CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,11)                                                    
      IF(IFLMR.EQ.0)GOTO 150                                                    
      IF(IPSEL.EQ.0) GOTO 1500                                                  
      IF(DUMP(14)) CALL JREAD(ISLSFC,SIGM,6*2)                               
      CALL JREAD(ISLSFC,SIGM,6*2)                                            
      if(ixsol.gt.0) then                   
      WRITE(IOUT,1100)IDEL,(SIGM(I),I=1,NSOL)                                   
      else
      WRITE(Iunstr,1101)IDEL,SIGM(NSOL)                                   
      endif
 1500 CONTINUE                                                                  
      RETURN                                                                    
1100  FORMAT(9X,I6,1X,6E15.6)                                                   
1110  FORMAT(//,40X,'*** SFORZI NEGLI ELEMENTI DI CORRENTE (SIGMA) ***'         
     1///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                   
     2  T86,'MY',T101,'MZ'/)                                                    
1101  FORMAT(9X,I6,1X,6E15.6)                                                   
1111  FORMAT(//,40X,'*** FORMATO NASTRAN (SIGMA) ***'         
     1///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                   
     2  T86,'MY',T101,'MZ'/)                                                    
C                                                                               
C                                                                               
      ENTRY PRTGIU(SIGM,IELSET,NESEL,ixsol)                                      
C                                                                               
C                                                                               
C     STAMPA GLI SFORZI NELLE GIUNZIONI CON DUE NODI                            
C                                                                               
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif

      LSUP=40                                                                   
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,2100)                                                          
      else
      WRITE(Iunstr,2101)                                                          
      endif

      DO 2500 IEL=1,NGIUR                                                       
      IF(IEL.LE.LSUP)GOTO 250                                                   
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,2100)                                                          
      else
      WRITE(Iunstr,2101)                                                          
      endif
      LSUP=LSUP+40                                                              
250   CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,18)                                                    
      CALL JPOSRL(ISLELS,6)                                                     
      IF(IFLMR.EQ.0)GOTO 250                                                    
      IF(IPSEL.EQ.0) GOTO 2500                                                  
      IF(DUMP(14)) CALL JREAD(ISLSFC,SIGM,NSOL*2)                               
      CALL JREAD(ISLSFC,SIGM,NSOL*2)                                            
      if(ixsol.gt.0) then                   
      WRITE(IOUT,2110) IDEL,(SIGM(I),I=1,NSOL)                                  
      else
      WRITE(Iunstr,2111) IDEL,SIGM(NSOL)                                  
      endif
 2500 CONTINUE                                                                  
      RETURN                                                                    
2100  FORMAT(//,40X,'*** SFORZI NEGLI ELEMENTI DI GIUNZIONE (TAU) ***'          
     1   ///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                
     2   T86,'MX',T101,'MZ'/)                                                   
2110  FORMAT(9X,I6,1X,6E15.6)                                                   
2101  FORMAT(//,40X,'*** SFORZI NEGLI ELEMENTI DI GIUNZIONE (TAU) ***'          
     1   ///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                
     2   T86,'MX',T101,'MZ'/)                                                   
2111  FORMAT(9X,I6,1X,6E15.6)                                                   
C                                                                               
C                                                                               
      ENTRY PRTPAN(SIGMG,SIGML,                                                 
     1                  EPSI,IELSET,NESEL,ixsol)                                 
C                                                                               
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif

      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
      WRITE(IOUT,3109)                                                          
      else
      WRITE(Iunstr,3101)                                                          
      endif

      NRIG=0                                                                    
      DO 3500 IEL=1,NPANR                                                       
350   CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,NCON*5+4)                                              

      CALL JREAD(ISLELM,NPINT,1)                                                
c      print*,itip,idel,ncon
c      print*,'npint',npint

      IF(IFLMR.EQ.0.OR.IPSEL.EQ.0) GOTO 36                                      
      NRIG=NRIG+NPINT*3+1                                                       
      IF(NRIG.LE.40) GOTO 36                                                    
      NRIG=NPINT*ILOCA+1                                                        
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
 36   CALL JPOSRL(ISLELM,10)                                                     
      call jread(islelm,nskip,1)                                                
        call jread(islelm,nlami,1)
c        print*,' nlami',nlami                                             
        if(nlami.gt.30)then                                                    
          write(iout,*)' troppe lamine per recupero sforzi '                
          stop                                                              
        endif                                                                 
        do iii=1,nlami                                                        
          call jread(islelm,idlam(iii),1)                                   
          call jread(islelm,spes(iii),2)                                    
          call jread(islelm,rotaz(iii),2)                                   
          rotaz(iii)=rotaz(iii)*180./3.141592654d0                                 
c      print*,'lam',iii,idlam(iii),spes(iii),rotaz(iii)                                   
          call jposrl(islelm,5*2)
cc          call jread(islelm,car(1,iii),5*2)
cc          car(6,iii)=0.                                 
cc      print*,'car',(car(io,iii),io=1,5)                                             
cc          call medcar(vel,car(1,iii),dent,spes(iii),rotaz(iii))            
cc      print*,'dop',idlam(iii),spes(iii),rotaz(iii)                                   
        enddo                                                                 
                                                                                
      sl(1)=0
      sl(2)=0
      sg(1)=0
      sg(2)=0
      sg(3)=0
      DO 310 IN=1,NPINT                                                         
      CALL JPOSRL(ISLELS,4*NCON+12)                                             
      CALL JREAD (ISLELS,Y,2)                                                   
      CALL JREAD (ISLELS,X,2)                                                   
      CALL JREAD (ISLELS,X,2)
c      print*,'in,x,y',in,x,y                                                   
      X=-X                                                                      
      if(iflsrs.eq.1) then                                                      
      r=sqrt(x*x+y*y)                                                           
      y=atan2(y,x)*180./3.1415                                                  
      x=r                                                                       
      endif                                                                     
      CALL JPOSRL(ISLELS,6)                                                     
      IF(IFLMR.EQ.0)GOTO 310                                                    
      IF(IPSEL.EQ.0)GOTO 310 
      
      IF(DUMP(14)) CALL JREAD (ISLSFC,EPSP,6*6)                              
      do ilam=1,nlami                                                           
      IF(DUMP(14)) CALL JREAD (ISLSFC,EPSL,6*6)                              

cc      CALL JREAD (ISLSFC,SIGMG,6*6)                                          
      CALL JREAD (ISLSFC,SIGML,3*2*6)
      
c      write(iout,77) 'el',idel,ilam,rotaz(ilam)                                      
c 77    format(a,2i4,8E12.5)                                                       
c      do iiz=1,6
c      write(iout,77) 'sigm',in,iiz,(sigml(kiz,iiz),kiz=1,3)  
c      enddo       
      if(ixsol.gt.0) then                   
        IF(IN.EQ.1.and.ilam.eq.1) THEN
          WRITE(IOUT,3310)IDEL,x,y
        ENDIF                                       
          WRITE(IOUT,3311)in,ilam,(lab(isf),(SIGML(ISF,I),I=1,NSOL),
     *    ISF=1,3)

3100  FORMAT(///40X,'***  SFORZI NEGLI ELEMENTI DI PANNELLO ***'///             
     1  1X,'ID.ELEM     COORDINATE P.INTEG',10x                                                  
     2 ,'TX',14X,'TY',14X,'TZ',14X,'MX',14X,'MY',14X,'MZ')                                            
3109  FORMAT(11x,'P.INTEG LAYER  STRESS')
3310  FORMAT(/(I7,1X,'X=',E10.4,2x,'Y=',E10.4))                                         
3311  FORMAT(/(8x,2i7,6x,a3,6(3X,E13.6),2(/22x,6x,a3,6(3X,E13.6))))                                         
      else                              
cc    output per nastran
      endif
      enddo
310   CONTINUE                                                                  

cc      if(ixsol.lt.0) then                   
cc      sl(1)=sl(1)/npin
cc      sl(2)=sl(2)/npin
cc      sg(1)=sg(1)/npin
cc      sg(2)=sg(2)/npin
cc      sg(3)=sg(3)/npin
cc      IF(ILOCA.EQ.1) THEN                                                       
cc      WRITE(Iunstr,3610)IDEL,IN,SL(1),SL(2)                                       
cc      ELSE IF(ILOCA.EQ.2)THEN                                                   
cc      WRITE(Iunstr,3510)IDEL,IN,SG(1),SG(2),SG(3)                 
cc      END IF                                                                    
cc      endif

      IF(IFLMR.EQ.0) GOTO 350                                                   
 3500 CONTINUE                                                                  
      RETURN                                                                    
                                             
3110  FORMAT(/(2I7,1X,'L',6(1X,2E9.3))/1X,'X=',E10.4,2X,'G',6(1X,2E9.3)/        
     1                                 1X,'Y=',E10.4,2X,'G',6(10X,E9.3))        
3111  FORMAT(/(7X,I7,1X,'L',6(1X,2E9.3))/                                       
     1         1X,'X=',E10.4,2X,'G',6(1X,2E9.3)/                                
     2         1X,'Y=',E10.4,2X,'G',6(10X,E9.3))                                
3210  FORMAT(/(2I7,1X,'G',6(1X,2E9.3)))                                         
 3211 FORMAT(7X,I7,1X,'G',6(1X,2E9.3))                                          
cc3310  FORMAT(/(2I7,1X,'L',6(1X,2E9.3),2(/21x,6(1X,2E9.3))))                                         
cc 3311 FORMAT(14X,I7,1X,'L',6(1X,2E9.3))                                          
                 
                 
3101  FORMAT(///40X,'***  FORMATI NASTRAN ***'///             
     1  1X,'ID.ELEM  P.INTEG',                                                  
     2 9X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'//                   
     2 17X,6('   SIGMA     TAU   '))                                            
3410  FORMAT(/(2I7,1X,'L',6(1X,2E9.3))/1X,'X=',E10.4,2X,'G',6(1X,2E9.3)/        
     1                                 1X,'Y=',E10.4,2X,'G',6(10X,E9.3))        
3411  FORMAT(/(7X,I7,1X,'L',6(1X,2E9.3))/                                       
     1         1X,'X=',E10.4,2X,'G',6(1X,2E9.3)/                                
     2         1X,'Y=',E10.4,2X,'G',6(10X,E9.3))                                
3510  FORMAT(/(2I7,1X,'G',6(1X,2E9.3)))                                         
 3511 FORMAT(7X,I7,1X,'G',6(1X,2E9.3))                                          
3610  FORMAT(/(2I7,1X,'L',6(1X,2E9.3)))                                         
 3611 FORMAT(7X,I7,1X,'L',6(1X,2E9.3))                                          

C                                                                               
C                                                                               
      ENTRY PRTEPL(SIGMAG,SIGMAL,EPSI,                                          
     1   ITY,IELSET,NESEL,ixsol)                                                 
C                                                                               
C         STAMPA GLI SFORZI DEGLI ELEMENTI PIANI(ITY=5)                         
C                                    E DI LAMINA(ITY=4)                         
C                                                                               
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif

      NWORD=6*6                                                              
      NELI=NLAMR                                                                
      IF(ITY.EQ.5) NELI=NELPR                                                   
      DO 4500 IEL=1,NELI                                                        
c       do 7775 k=1,6                                                           
c       write(40+k,7778) iel                                                    
c       write(50+k,7778) iel                                                    
c7775    continue                                                               
                                                                                
 450  CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
C      CALL JPOSRL(ISLELM,NCON*5+7+72)                                          
C-----nuova istruzione: tiene conto scrittura in ISLELM dei coeff.termici       
      IF(ITIP.EQ.5) THEN                                                        
      CALL JPOSRL(ISLELM,NCON*5+10+72 + 6*2)                                    
      ELSE                                                                      
      CALL JPOSRL(ISLELM,NCON*5+7+72)                                           
      ENDIF                                                                     
C-----                                                                          
      IF(ITY.EQ.4) THEN                                                         
      CALL JPOSRL(ISLELM,NCON)                                                  
      END IF                                                                    
      CALL JREAD(ISLELS,LREC,2)                                                 
      IN=0                                                                      
      NBL=(NPI+5)/6                                                             
      NN=6                                                                      
      ipi=2                                                                     
      ipo=0                                                                     
      DO 410 IW=1,NBL                                                           
      IF(IFLMR.EQ.0.OR.IPSEL.EQ.0)GOTO 411                                      
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      IF(ITY.EQ.4) THEN                                                         
      WRITE(IOUT,4100)IDEL                                                      
       IF(ILOCA.EQ.1)THEN                                                       
       WRITE(IOUT,4101)                                                         
       ELSE IF(ILOCA.EQ.2)THEN                                                  
       WRITE(IOUT,4102)                                                         
       ELSE IF(ILOCA.EQ.3)THEN                                                  
       WRITE(IOUT,4103)                                                         
       END IF                                                                   
      ELSE                                                                      
      WRITE(IOUT,5100)IDEL                                                      
       IF(ILOCA.EQ.1)THEN                                                       
       WRITE(IOUT,4101)                                                         
       ELSE IF(ILOCA.EQ.2)THEN                                                  
       WRITE(IOUT,4102)                                                         
       ELSE IF(ILOCA.EQ.3)THEN                                                  
       WRITE(IOUT,4103)                                                         
       END IF                                                                   
      END IF                                                                    
      else
      WRITE(Iunstr,4109)                                                      
      endif
 411  IF(IW.EQ.NBL) NN=NPI-(NBL-1)*NN                                           
      do ilo=1,6
      sl(ilo)=0
      sg(ilo)=0
      enddo                  
      DO 410 LL=1,NN                                                            
      IN=IN+1                                                                   
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      IF(IFLMR.EQ.0)GOTO 410                                                    
      IF(IPSEL.EQ.0) GOTO 410                                                   
C                                                                               
      IF(DUMP(14)) CALL JREAD (ISLSFC,EPSI,NWORD)                               
C      CALL DWRITE('EPSI',EPSI,6,6,6,1)                                         
      CALL JREAD (ISLSFC,SIGMAG,NWORD)                                          
      CALL JREAD (ISLSFC,SIGMAL,NWORD)                                        
      
      if(iflsrs.eq.1) then                                                      
      r=sqrt(cpi(1)**2+cpi(2)**2)                                               
      y=atan2(cpi(2),cpi(1))*180./3.1415     
      cpi(1)=r                                                                  
      cpi(2)=y                                                                  
      endif                            

      if(ixsol.gt.0) then                   
                                               
       IF(ILOCA.EQ.1)THEN                                                       
      WRITE(IOUT,4211)    IN,(SIGMAL(1,K),K=1,NSOL)                             
      WRITE(IOUT,4212)       (SIGMAL(2,K),K=1,NSOL)                             
      WRITE(IOUT,4213)CPI(1),(SIGMAL(3,K),K=1,NSOL)                             
      WRITE(IOUT,4214)CPI(2),(SIGMAL(4,K),K=1,NSOL)                             
      WRITE(IOUT,4215)       (SIGMAL(5,K),K=1,NSOL)                             
      WRITE(IOUT,4216)       (SIGMAL(6,K),K=1,NSOL)                             
       ELSE IF(ILOCA.EQ.2)THEN                                                  
      WRITE(IOUT,4211)    IN,(SIGMAG(1,K),K=1,NSOL)                             
      WRITE(IOUT,4212)       (SIGMAG(2,K),K=1,NSOL)                             
      WRITE(IOUT,4213)CPI(1),(SIGMAG(3,K),K=1,NSOL)                             
      WRITE(IOUT,4214)CPI(2),(SIGMAG(4,K),K=1,NSOL)                             
      WRITE(IOUT,4215)       (SIGMAG(5,K),K=1,NSOL)                             
      WRITE(IOUT,4216)       (SIGMAG(6,K),K=1,NSOL)                             
       ELSE IF(ILOCA.EQ.3)THEN                                                  
      WRITE(IOUT,4111)    IN,(SIGMAL(1,K),SIGMAG(1,K),K=1,NSOL)                 
      WRITE(IOUT,4112)       (SIGMAL(2,K),SIGMAG(2,K),K=1,NSOL)                 
      WRITE(IOUT,4113)CPI(1),(SIGMAL(3,K),SIGMAG(3,K),K=1,NSOL)                 
      WRITE(IOUT,4114)CPI(2),(SIGMAL(4,K),SIGMAG(4,K),K=1,NSOL)                 
      WRITE(IOUT,4115)       (SIGMAL(5,K),SIGMAG(5,K),K=1,NSOL)                 
      WRITE(IOUT,4116)       (SIGMAL(6,K),SIGMAG(6,K),K=1,NSOL)                 
      END IF                                                                    
      else
                                               
      do ilo=1,6
      sl(ilo)=sl(ilo)+SIGMAL(ilo,NSOL)                             
      sg(ilo)=sg(ilo)+SIGMAg(ilo,NSOL)                             
      enddo
      endif
c     ipo=ipo+1                                                                 
c     if(ipo.eq.ipi) then                                                       
c       ipi=ipi+3                                                               
c       do 7777 k=1,6                                                           
c       write(40+k,7778) cpi(1),cpi(2),(sigmal(isfo,k),isfo=1,6)                
c       write(50+k,7778) cpi(1),cpi(2),(sigmag(isfo,k),isfo=1,6)                
7777    continue                                                                
7778    format(8e14.6)                                                          
c     endif                                                                     
 410  CONTINUE                                                                  

      if(ixsol.lt.0) then 
      do ilo=1,6
      sl(ilo)=sl(ilo)/npi
      sg(ilo)=sg(ilo)/npi
      enddo                  
       IF(ILOCA.EQ.1)THEN                                                       
      WRITE(Iunstr,4411)    IN,SL(1)                             
      WRITE(Iunstr,4412)       SL(2)                             
      WRITE(Iunstr,4413)CPI(1),SL(3)                             
      WRITE(Iunstr,4414)CPI(2),SL(4)                             
      WRITE(Iunstr,4415)       SL(5)                             
      WRITE(Iunstr,4416)       SL(6)                             
       ELSE IF(ILOCA.EQ.2)THEN                                                  
      WRITE(Iunstr,4411)    IN,SG(1)                             
      WRITE(Iunstr,4412)       SG(2)                             
      WRITE(Iunstr,4413)CPI(1),SG(3)                             
      WRITE(Iunstr,4414)CPI(2),SG(4)                             
      WRITE(Iunstr,4415)       SG(5)                             
      WRITE(Iunstr,4416)       SG(6)                             
      END IF                                                                    

      endif


      IF(IFLMR.EQ.0) GOTO 450                                                   
 4500 CONTINUE                                                                  
      RETURN                                                                    
4100  FORMAT(/41X,'***  SFORZI NELL''ELEMENTO DI LAMINA N.RO',I6,'  ***'        
     1 ///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)               
4103  FORMAT(16X,6('    RIF LOC.   GEN.'))                                      
4102  FORMAT(16X,6('    RIFER. GENERALE'))                                      
4101  FORMAT(16X,6('    RIFER.  LOCALE '))                                      
 5100 FORMAT(/46X,'***  SFORZI NELL''ELEMENTO PIANO N.RO',I6,'  ***'///         
     1 26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                  
4111  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,2E9.3))                               
4112  FORMAT(14X,' TYZ',6(1X,2E9.3))                                            
4113  FORMAT(' X =',E10.4,' SZ ',6(1X,2E9.3))                                   
4114  FORMAT(' Y =',E10.4,' SX ',6(1X,2E9.3))                                   
4115  FORMAT(14X,' SY ',6(1X,2E9.3))                                            
4116  FORMAT(14X,' TXY',6(1X,2E9.3))                                            
4211  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,E14.6,4X))                            
4212  FORMAT(14X,' TYZ',6(1X,E14.6,4X))                                         
4213  FORMAT(' X =',E10.4,' SZ ',6(1X,E14.6,4X))                                
4214  FORMAT(' Y =',E10.4,' SX ',6(1X,E14.6,4X))                                
4215  FORMAT(14X,' SY ',6(1X,E14.6,4X))                                         
4216  FORMAT(14X,' TXY',6(1X,E14.6,4X))                                         


4109  FORMAT(/41X,'***  FORMATO NASTRAN  ***'        
     1 ///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)               
4411  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,E14.6,4X))                            
4412  FORMAT(14X,' TYZ',6(1X,E14.6,4X))                                         
4413  FORMAT(' X =',E10.4,' SZ ',6(1X,E14.6,4X))                                
4414  FORMAT(' Y =',E10.4,' SX ',6(1X,E14.6,4X))                                
4415  FORMAT(14X,' SY ',6(1X,E14.6,4X))                                         
4416  FORMAT(14X,' TXY',6(1X,E14.6,4X))                                         

C                                                                               
C                                                                               
      ENTRY PRTSTR(SIGMAG,SIGMAL,SIGMAF,EPSI,EPSIL,EPSIF,                       
     *             ITY,IELSET,NESEL,ixsol)                                       
C                                                                               
C         STAMPA GLI SFORZI DEGLI ELEMENTI STRATIFICATI(ITY=8)                  
C                                                                               
      write(iout,*) ' flag coordinate pi',iflsrs  

      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif
      
      NWORD=6*NPSI                                                           
      NELI=NELSR                                                                
      DO 8500 IEL=1,NELI                                                        
c       do 7776 k=1,6                                                           
c       write(40+k,7778) iel                                                    
c       write(50+k,7778) iel                                                    
c7776    continue                                                                
 850  CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,NCON*5)                                                
      CALL JREAD(ISLELM,NSTRAT,1)                                               
      NUMPOS=NSTRAT*80                                                          
      CALL JPOSRL(ISLELM,NUMPOS)                                                
      CALL JREAD(ISLELS,LRECD,2)                                                
      IN=0                                                                      
      NBL=(NPINTG*NSTRAT+5)/6                                                   
      NN=6                                                                      
      ipo=0                                                                     
      ipi=2                                                                     
      DO 810 IW=1,NBL                                                           
      IF(IFLMR.EQ.0.OR.IPSEL.EQ.0)GOTO 811                                      
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,8100)IDEL                                                      
      else
      WRITE(Iunstr,8109)                                                      
      endif
 811   IF(IW.EQ.NBL) NN=NPINTG*NSTRAT-(NBL-1)*NN  
 
      do ilo=1,6
      sl(ilo)=0
      sg(ilo)=0
      sf(ilo)=0
      enddo                  
 
                               
       DO 810 LL=1,NN                                                           
       IN=IN+1                                                                  
       CALL JREAD(ISLELS,ENNEST,LRECD)                                          
       IF(IFLMR.EQ.0)GOTO 810                                                   
       IF(IPSEL.EQ.0)GOTO 810                                                   
C                                                                               
       IF(DUMP(14)) CALL JPOSRL(ISLSFC,2*NWORD)                                 
       CALL JREAD (ISLSFC,SIGMAG,NWORD)                                         
       CALL JREAD (ISLSFC,SIGMAL,NWORD)                                         
       IF(DUMP(14).AND.DUMP(20)) CALL JPOSRL(ISLSFC,NWORD)                      
       IF(DUMP(19)) CALL JREAD(ISLSFC,SIGMAF,NWORD) 
                                   
      if(iflsrs.eq.1) then                                                      
      r=sqrt(cpint(1)**2+cpint(2)**2)                                           
      y=atan2(cpint(2),cpint(1))*180./3.1415                             
      cpint(1)=r                                                                
      cpint(2)=y                                                                
      endif                                                                     

      if(ixsol.gt.0) then                   
       IF(ILOCA.EQ.1)THEN                                                       
       WRITE(IOUT,8101)                                                         
        WRITE(IOUT,8211)      IN,(SIGMAL(1,K),K=1,NSOL)                         
        WRITE(IOUT,8212)         (SIGMAL(2,K),K=1,NSOL)                         
        WRITE(IOUT,8213)CPINT(1),(SIGMAL(3,K),K=1,NSOL)                         
        WRITE(IOUT,8214)CPINT(2),(SIGMAL(4,K),K=1,NSOL)                         
        WRITE(IOUT,8215)         (SIGMAL(5,K),K=1,NSOL)                         
        WRITE(IOUT,8216)         (SIGMAL(6,K),K=1,NSOL)                         
C                                                                               
       ELSE IF(ILOCA.EQ.2)THEN                                                  
       WRITE(IOUT,8102)                                                         
        WRITE(IOUT,8211)      IN,(SIGMAG(1,K),K=1,NSOL)                         
        WRITE(IOUT,8212)         (SIGMAG(2,K),K=1,NSOL)                         
        WRITE(IOUT,8213)CPINT(1),(SIGMAG(3,K),K=1,NSOL)                         
        WRITE(IOUT,8214)CPINT(2),(SIGMAG(4,K),K=1,NSOL)                         
        WRITE(IOUT,8215)         (SIGMAG(5,K),K=1,NSOL)                         
        WRITE(IOUT,8216)         (SIGMAG(6,K),K=1,NSOL)                         
       ELSE IF(ILOCA.EQ.3)THEN                                                  
       WRITE(IOUT,8103)                                                         
        WRITE(IOUT,8111)      IN,(SIGMAL(1,K),SIGMAG(1,K),K=1,NSOL)             
        WRITE(IOUT,8112)         (SIGMAL(2,K),SIGMAG(2,K),K=1,NSOL)             
        WRITE(IOUT,8113)CPINT(1),(SIGMAL(3,K),SIGMAG(3,K),K=1,NSOL)             
        WRITE(IOUT,4114)CPINT(2),(SIGMAL(4,K),SIGMAG(4,K),K=1,NSOL)             
        WRITE(IOUT,4115)         (SIGMAL(5,K),SIGMAG(5,K),K=1,NSOL)             
        WRITE(IOUT,4116)         (SIGMAL(6,K),SIGMAG(6,K),K=1,NSOL)             
       END IF                                                                   
       IF(DUMP(19)) THEN                                                        
       WRITE(IOUT,8104)                                                         
        WRITE(IOUT,8211)      IN,(SIGMAF(1,K),K=1,NSOL)                         
        WRITE(IOUT,8212)         (SIGMAF(2,K),K=1,NSOL)                         
        WRITE(IOUT,8213)CPINT(1),(SIGMAF(3,K),K=1,NSOL)                         
        WRITE(IOUT,8214)CPINT(2),(SIGMAF(4,K),K=1,NSOL)                         
        WRITE(IOUT,8215)         (SIGMAF(5,K),K=1,NSOL)                         
        WRITE(IOUT,8216)         (SIGMAF(6,K),K=1,NSOL)                         
       END IF                                                                   
      else
                                               
      do ilo=1,6
      sl(ilo)=sl(ilo)+SIGMAL(ilo,NSOL)                             
      sg(ilo)=sg(ilo)+SIGMAg(ilo,NSOL)                             
      sf(ilo)=sf(ilo)+SIGMAf(ilo,NSOL)                             
      enddo
      endif
c     ipo=ipo+1                                                                 
c     if(ipo.eq.ipi) then                                                       
c       ipi=ipi+3                                                               
c       do 7767 k=1,6                                                           
c       write(40+k,7778) cpint(1),cpint(2),(sigmal(isfo,k),isfo=1,6)            
c       write(50+k,7778) cpint(1),cpint(2),(sigmag(isfo,k),isfo=1,6)            
c7767    continue                                                                
c     endif                                                                     
 810   CONTINUE                                                                 



      if(ixsol.lt.0) then                   
      do ilo=1,6
      sl(ilo)=sl(ilo)/npintg*nstrat
      sg(ilo)=sg(ilo)/npintg*nstrat
      sf(ilo)=sf(ilo)/npintg*nstrat
      enddo                  
       IF(ILOCA.EQ.1)THEN                                                       
       WRITE(Iunstr,8301)                                                         
        WRITE(Iunstr,8411)      IN,SL(1)                         
        WRITE(Iunstr,8412)         SL(2)                         
        WRITE(Iunstr,8413)CPINT(1),SL(3)                         
        WRITE(Iunstr,8414)CPINT(2),SL(4)                         
        WRITE(Iunstr,8415)         SL(5)                         
        WRITE(Iunstr,8416)         SL(6)                         
C                                                                               
       ELSE IF(ILOCA.EQ.2)THEN                                                  
       WRITE(Iunstr,8302)                                                         
        WRITE(Iunstr,8411)      IN,SG(1)                         
        WRITE(Iunstr,8412)         SG(2)                         
        WRITE(Iunstr,8413)CPINT(1),SG(3)                         
        WRITE(Iunstr,8414)CPINT(2),SG(4)                         
        WRITE(Iunstr,8415)         SG(5)                         
        WRITE(Iunstr,8416)         SG(6)                         
       END IF                                                                   
       IF(DUMP(19)) THEN                                                        
       WRITE(Iunstr,8304)                                                         
        WRITE(Iunstr,8411)      IN,SF(1)                         
        WRITE(Iunstr,8412)         SF(2)                         
        WRITE(Iunstr,8413)CPINT(1),SF(3)                         
        WRITE(Iunstr,8414)CPINT(2),SF(4)                         
        WRITE(Iunstr,8415)         SF(5)                         
        WRITE(Iunstr,8416)         SF(6)                         
       END IF                                                                   
      endif


      IF(IFLMR.EQ.0) GOTO 850                                                   
 8500 CONTINUE                                                                  
      RETURN                                                                    
8100  FORMAT(/41X,'***  SFORZI NELL''ELEMENTO STRATIFICATO N.RO',I6,'  '        
     1,'***',///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)         
8103  FORMAT(/16X,6('    RIF LOC.   GEN.'))                                     
8102  FORMAT(/16X,6('    RIFER. GENERALE'))                                     
8101  FORMAT(/16X,6('    RIFER.  LOCALE '))                                     
8104  FORMAT(/16X,6('    RIFER.  FIBRE  '))                                     
8111  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,2E9.3))                               
8112  FORMAT(14X,' TYZ',6(1X,2E9.3))                                            
8113  FORMAT(' X =',E10.4,' SZ ',6(1X,2E9.3))                                   
8114  FORMAT(' Y =',E10.4,' SX ',6(1X,2E9.3))                                   
8115  FORMAT(14X,' SY ',6(1X,2E9.3))                                            
8116  FORMAT(14X,' TXY',6(1X,2E9.3))                                            
8211  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,E14.6,4X))                            
8212  FORMAT(14X,' TYZ',6(1X,E14.6,4X))                                         
8213  FORMAT(' X =',E10.4,' SZ ',6(1X,E14.6,4X))                                
8214  FORMAT(' Y =',E10.4,' SX ',6(1X,E14.6,4X))                                
8215  FORMAT(14X,' SY ',6(1X,E14.6,4X))                                         
8216  FORMAT(14X,' TXY',6(1X,E14.6,4X))                                         

8109  FORMAT(/41X,'***  SFORZI NELL''ELEMENTO STRATIFICATO N.RO',I6,'  '        
     1,'***',///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)         
8303  FORMAT(/16X,6('    RIF LOC.   GEN.'))                                     
8302  FORMAT(/16X,6('    RIFER. GENERALE'))                                     
8301  FORMAT(/16X,6('    RIFER.  LOCALE '))                                     
8304  FORMAT(/16X,6('    RIFER.  FIBRE  '))                                     
8311  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,2E9.3))                               
8312  FORMAT(14X,' TYZ',6(1X,2E9.3))                                            
8313  FORMAT(' X =',E10.4,' SZ ',6(1X,2E9.3))                                   
8314  FORMAT(' Y =',E10.4,' SX ',6(1X,2E9.3))                                   
8315  FORMAT(14X,' SY ',6(1X,2E9.3))                                            
8316  FORMAT(14X,' TXY',6(1X,2E9.3))                                            
8411  FORMAT(/' P.INT. N.',I4,' TXZ',6(1X,E14.6,4X))                            
8412  FORMAT(14X,' TYZ',6(1X,E14.6,4X))                                         
8413  FORMAT(' X =',E10.4,' SZ ',6(1X,E14.6,4X))                                
8414  FORMAT(' Y =',E10.4,' SX ',6(1X,E14.6,4X))                                
8415  FORMAT(14X,' SY ',6(1X,E14.6,4X))                                         
8416  FORMAT(14X,' TXY',6(1X,E14.6,4X))                                         

      END                                                                       

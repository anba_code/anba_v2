C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C-----------------------------------------------------------------              
C - NOTA: la quarta riga del common /ISLOTS/ contiene slot della -              
C -       trattazione termica                                    -              
C-----------------------------------------------------------------              
C@ELT,I ANBA*ANBA.MODASS                                                        
C*************************          MODASS          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MODASS                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLMRG,ISLTEM,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
C                                                                               
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      CALL JCRERS                                                               
C                                                                               
      DO 8754 iopl=1,maxdim                                                     
8754  iv(iopl)=0                                                                
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT         ,JNUME )               
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC   ,JNUINT)               
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1     ,JIPIAZ)               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXNOD       ,JICON )               
      CALL JALLOC(*2000,'ICOMP   ','INT.',1,MAXNOD       ,JICOMP)               
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
      CALL JALLOC(*2000,'ELMAT2  ','D.P.',1,MAXDEL*MAXDEL,JELMA2)               
      CALL JALLOC(*2000,'ELMAT3  ','D.P.',1,MAXDEL*MAXDEL,JELMA3)               
      CALL JALLOC(*2000,'MINRIG  ','INT.',1,NEQV+1       ,JMNRIG)               
      CALL JALLOC(*2000,'NWDEL   ','INT.',1,NELEM*3        ,JNWDEL)             
      CALL JALLOC(*2000,'VMASS   ','D.P.',1,10           ,JVMASS)               
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
      CALL JOPEN('*ELS    ',ISLELS,1)                                           
C----- solo se richiesta trattazione termica, apre slot termici                 
      IF (JEXIST('*TMP    ').EQ.1) THEN                                         
      CALL JOPEN('*MTT    ',ISMTER,1)                                           
      END IF                                                                    
C-----fine apertura slot trattazione termica                                    
                                                                                
      JRCOMP=JELMAT                                                             
c      JLIM=JVMASS+11                                                           
C                                                                               
c      KK=JLIM-JIPIAZ                                                           
c      CALL IZERO(IV(JIPIAZ),MAXDEL)                                            
c      CALL IZERO(IV(JICON),MAXNOD)                                             
c      CALL IZERO(IV(JICOMP),MAXNOD)                                            
c      CALL DZERO(DV(JELMAT),MAXDEL*MAXDEL)                                     
c      CALL IZERO(IV(JMNRIG),NEQV+1)                                            
c      CALL IZERO(IV(JNWDEL),NELEM)                                             
c      CALL DZERO(DV(JVMASS),6)                                                 
C                                                                               
       print*,' ***   INIZIO COSTRUZIONE MATRICI E PUNTATORI   ***'             
      CALL VTTPZZ(IV(JNUME),IV(JNUINT),IV(JIPIAZ),IV(JICON),IV(JICOMP),         
     1  DV(JELMAT),DV(JELMA2),DV(JELMA3),                                       
     2  MAXDEL,DV(JRCOMP),IV(JMNRIG),IV(JNWDEL),DV(JVMASS))                     
      print*,' ***   FINE                                     ***'              
C                                                                               
C-----se rischiesta trattazione termica, chiudo slot matrici termiche           
      IF (JEXIST('*TMP    ').EQ.1) THEN                                         
      CALL JCLOSE(ISMTER)                                                       
      END IF                                                                    
C-----fine chiusura slot termici                                                
      CALL JCLOSE(ISLELS)                                                       
      CALL JCLOSE(ISLELM)                                                       
      CALL JCLOSE(ISLMTN)                                                       
      CALL JCLOSE(ISLMTF)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JOPEN('*NWD    ',ISLNWD,1)                                           
      CALL JWRITE(ISLNWD,IV(JNWDEL),NELEM*3)                                    
      CALL JCLOSE(ISLNWD)                                                       
C                                                                               
      CALL JOPEN('*MAS    ',ISLMAS,1)                                           
      CALL JWRITE(ISLMAS,DV(JVMASS),20)                                         
      CALL JCLOSE(ISLMAS)                                                       
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      NPOS=NMPC+INCOU                                                           
C                                                                               
      print*,' ***   INIZIO ANALISI FRONTE DI ASSEMBLAGGIO   ***'               
      CALL PREFRO(IV(JMNRIG),NEQV,ISLMRG,MAXLRG,NSTO,NPOS,MAXDEL,IGEOM)         
      print*,' ***   FINE                                    ***'               
      CALL JCLOSE(ISLMRG)                                                       
C                                                                               
                                                                                
      CALL JCRERS                                                               
C                                                                               
      IF(INCORE) THEN                                                           
C                                                                               
      CALL INCASB                                                               
C                                                                               
      ELSE                                                                      
C                                                                               
      CALL OUCASB                                                               
C                                                                               
      END IF                                                                    
C                                                                               
      CALL CHKPNT                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.ASSMAS                                                        
C*************************          ASMASS          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE ASMASS(VMASS,PD,X,Y)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION VMASS(1)                                                        
      VMASS(1)=VMASS(1)+PD                                                      
      VMASS(2)=VMASS(2)+PD*X                                                    
      VMASS(3)=VMASS(3)+PD*Y                                                    
      VMASS(4)=VMASS(4)+PD*X*X                                                  
      VMASS(5)=VMASS(5)+PD*Y*Y                                                  
      VMASS(6)=VMASS(6)+PD*X*Y                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.ASSMAS                                                        
C*************************          ASMASS          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE ASMASS2(VMASS,PD,X,Y,Z)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION VMASS(1)                                                        
      VMASS(1)=VMASS(1)+PD                                                      
      VMASS(2)=VMASS(2)+PD*X                                                    
      VMASS(3)=VMASS(3)+PD*Y                                                    
      VMASS(4)=VMASS(4)+PD*X*X                                                  
      VMASS(5)=VMASS(5)+PD*Y*Y                                                  
      VMASS(6)=VMASS(6)+PD*X*Y                                                  

      VMASS(7)=VMASS(7)+PD*Z                                                    
      VMASS(8)=VMASS(8)+PD*X*Z                                                  
      VMASS(9)=VMASS(9)+PD*Y*Z                                                  
      VMASS(10)=VMASS(10)+PD*Z*Z                                                  

      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.ASSELM                                                        
C*************************          ASSELM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE ASSELM(IPIAZ,KHED,LHED,KDEST,LDEST,                            
     1  ASSMAT,NRASMT,ELMAT,NRELMT,IPLH,NPLH,INDRM)                             
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),ASSMAT(NRASMT,1),ELMAT(NRELMT,1)                       
      DIMENSION KHED(1),LHED(1),KDEST(1),LDEST(1)                               
      COMMON /PARASS/ITIP,IND,NT,NT1,LINF,LSUP,NRIG,NCOL,nth                    
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      IF(DUMP(9))WRITE(IOUT,200)                                                
      NT=NT-1                                                                   
C                                                                               
C     CALCOLO DEI VETTORI DI CARICAMENTO DEI TERMINI DI ELMAT                   
C                                                                               
      DO 30 LK=1,NT                                                             
      IEQ=IPIAZ(LK)                                                             
C                                                                               
C     CONTROLLO DI APPARTENENZA DELL'EQUAZIONE AL BLOCCO                        
C                                                                               
      IF(IEQ.LT.LINF)GOTO 45                                                    
      IF(IEQ.GT.LSUP)GOTO 45                                                    
      IRIG=IEQ-LINF+1                                                           
C                                                                               
C     LA RIGA 'IEQ' DEVE ESSERE INSERITA IN 'ASSMAT'                            
C                                                                               
      KDEST(LK)=IRIG                                                            
      GOTO 35                                                                   
C                                                                               
C     LA RIGA 'IEQ' NON RIENTRA NEL BLOCCO DI ASSEMBLAGGIO                      
C                                                                               
   45 KDEST(LK)=0                                                               
C                                                                               
C     LA RIGA 'IEQ' E' GIA' PRESENTE NEL BLOCCO DI ASSEMBLAGGIO                 
C                                                                               
   35 IF(IEQ.LT.LINF)GOTO 62                                                    
      IF(NCOL.EQ.0)GOTO  1                                                      
      LL=IPOSL(IEQ,LHED,NCOL)                                                   
      IF(LL.NE.0) GOTO 80                                                       
C                                                                               
C     LA COLONNA 'IEQ' DEVE ESSERE INSERITA IN 'ASSMAT'                         
C                                                                               
 60   CONTINUE                                                                  
C     WRITE(IOUT,505)(LHED(IS),IS=1,NCOL)                                       
      GOTO (1,2),IPLH                                                           
2     DO 61 IE=1,NCOL                                                           
      IF(LHED(IE).LT.0) GOTO 31                                                 
61    CONTINUE                                                                  
      WRITE(IOUT,510) IEQ,IPLH,NPLH                                             
      CALL JHALT                                                                
      STOP                                                                      
   1  NCOL=NCOL+1                                                               
C                                                                               
C     TEST SULLE DIMENSIONI RAGGIUNTE DA 'ASSMAT'                               
C                                                                               
      IF(NCOL.GT.MAXLRG) GOTO 600                                               
      LDEST(LK)=NCOL                                                            
      LHED(NCOL)=IEQ                                                            
      GOTO 30                                                                   
 31   LDEST(LK)=IE                                                              
      LHED(IE)=IEQ                                                              
      NPLH=NPLH-1                                                               
      IF(NPLH.EQ.0) IPLH=1                                                      
      GOTO 30                                                                   
C                                                                               
C     LA COLONNA 'IEQ' NON RIENTRA NEL BLOCCO DI ASSEMBLAGGIO                   
C                                                                               
   62 LDEST(LK)=0                                                               
      GOTO 30                                                                   
C                                                                               
C     LA COLONNA 'IEQ' E' GIA' PRESENTE NEL BLOCCO DI ASSEMBLAGGIO              
C                                                                               
   80 LDEST(LK)=LL                                                              
   30 CONTINUE                                                                  
   65 CONTINUE                                                                  
      IF(DUMP(9)) THEN                                                          
      WRITE(IOUT,500)IND,ITIP,NT                                                
      WRITE(IOUT,501)(IPIAZ(IS),IS=1,NT)                                        
      WRITE(IOUT,503)(LDEST(IS),IS=1,NT)                                        
      WRITE(IOUT,505)(LHED(IS),IS=1,NCOL)                                       
      END IF                                                                    
C                                                                               
C     INIZIO DEL CICLO DI ASSEMBLAGGIO DEI TERMINI DI 'ELMAT'                   
C                                                                               
      DO 90 J1=1,NT                                                             
      IND1=IPIAZ(J1)                                                            
      DO 100 J2=J1,NT                                                           
      IP1=J1                                                                    
      IP2=J2                                                                    
      IND2=IPIAZ(IP2)                                                           
C                                                                               
C     VIENE ASSEMBLATO SOLO IL TRIANGOLO SUPERIORE                              
C                                                                               
      IF(IND1.LE.IND2)GOTO 110                                                  
      IS=IP1                                                                    
      IP1=IP2                                                                   
      IP2=IS                                                                    
  110 CONTINUE                                                                  
      INDR=KDEST(IP1)                                                           
      IF(INDRM.LT.INDR) INDRM=INDR                                              
      IF(INDR.EQ.0)GOTO 100                                                     
      INDC=LDEST(IP2)                                                           
      IF(INDC.EQ.0)GOTO 100                                                     
      ASSMAT(INDR,INDC)=ASSMAT(INDR,INDC)+ELMAT(J1,J2)                          
      if(ind1.eq.ind2) then                                                     
      if(j1.ne.j2) ASSMAT(INDR,INDC)=ASSMAT(INDR,INDC)+ELMAT(J1,J2)             
      endif                                                                     
  100 CONTINUE                                                                  
   90 CONTINUE                                                                  
C                                                                               
C     FINE CICLO DI ASSEMBLAGGIO                                                
C                                                                               
      RETURN                                                                    
600   WRITE(IOUT,204)NCOL,MAXLRG                                                
      CALL JHALT                                                                
      STOP                                                                      
204   FORMAT(' *** AVVISO (ASSELM): -NCOL- DI -ASSMAT- (',I8,')',               
     1 ' SUPERA IL MASSIMO DIMENSIONAMENTO (',I8,')  ***')                      
 200  FORMAT(//' *** STAMPE DI CONTROLLO SUBROUTINE "ASSELM" ***')              
 500  FORMAT(//'  ELEMENTO N.RO',I6,' DI TIPO',I6,' CON',I6,' TERMINI')         
 501  FORMAT(/10X,'VETTORE PIAZZAMENTO',/5(22I6/))                              
 503  FORMAT(/2X,' VETT. LDEST',18I6)                                           
 505  FORMAT(/2X,' VETT. LHED ',18I6)                                           
510   FORMAT(' *** AVVISO (ASSELM): ERRORE NELLA ASSEGNAZIONE DEL VALORE        
     1 DI -IPLH-  EQUAZ. N. ',I6,'  IPLH,NPLH',2I6,'  ***')                     
      END                                                                       
C@ELT,I ANBA*ANBA.ASSFRO                                                        
C*************************          ASSFRO          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE ASSFRO(IELEM,IPIAZ,KHED,LHED,KDEST,LDEST,                      
     1  IHED,ASSMAT,NRASMT,VETT,ELMAT,NRELMT,ASSPSI,NRASPS,NEQAS,IPELM)         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      INTEGER SPCCOD                                                            
      DIMENSION IELEM(1),IPIAZ(1),ASSMAT(NRASMT,1),IPELM(1)                     
      DIMENSION KHED(1),LHED(1),KDEST(1),LDEST(1),VETT(1)                       
      DIMENSION IHED(1),ELMAT(NRELMT,1),ASSPSI(NRASPS,1)                        
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3            
      COMMON /PARASS/ ITIP,IND,NT,NT1,LINF,LSUP,NRIG,NCOL,NTH                   
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),IECHO,IAUTO,IGEOM,C                             
      DATA MPCCOD/6/,SPCCOD/7/                                                  
      LSUP=0                                                                    
      WRITE(IOUT,200)                                                           
C                                                                               
C     CALCOLO DELLE DIMENSIONI DEI BLOCCHI DI ASSEMBLAGGIO                      
C                                                                               
      NBLAS=(NEQV-NPSI+NEQAS-1)/NEQAS                                           
      WRITE(IOUT,201)NBLAS                                                      
      MAXFRO=0                                                                  
      NCOL=0                                                                    
      IPLH=1                                                                    
      NPLH=0                                                                    
C                                                                               
C     CICLO DI ASSEMBLAGGIO PER BLOCCHI DELLA MATRICE DEL SISTEMA               
C                                                                               
      DO 5 I=1,NBLAS                                                            
      INDRM=0                                                                   
      IF(I.EQ.NBLAS) NEQAS=NEQV-NPSI-(NBLAS-1)*NEQAS                            
      LINF=LSUP+1                                                               
      LSUP=LSUP+NEQAS                                                           
      WRITE(IOUT,202)I,NEQAS,LINF,LSUP                                          
C                                                                               
C     CICLO DI ASSEMBLAGGIO DELLE EQUAZIONI DEL BLOCCO I-ESIMO                  
C                                                                               
      DO 7 IL=1,NEQAS                                                           
      KHED(IL)=LINF+IL-1                                                        
7     CONTINUE                                                                  
      NRIG=NEQAS                                                                
      DO 10 IK=1,NEQAS                                                          
      CALL JREAD(ISLTAB,NELEQI,1)                                               
      CALL JREAD(ISLTAB,IELEM,NELEQI)                                           
      IF(NELEQI.EQ.0)GOTO 15                                                    
C                                                                               
C     CICLO DI ASSEMBLAGGIO DEGLI ELEMENTI CHE DANNO CONTRIBUTO                 
C     ALL'EQUAZIONE IK-ESIMA                                                    
C                                                                               
      DO 20 K=1,NELEQI                                                          
      IND=IELEM(K)                                                              
      IPO=IPELM(IND)                                                            
      CALL JPOSAB(ISLVPZ,IPO)                                                   
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT  ,1)                                                 
      CALL JREAD(ISLVPZ,NT1 ,1)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NT)                                               
      NTM1=NT-1                                                                 
      DO 21 IS=1,NTM1                                                           
      CALL JREAD(ISLVPZ,ELMAT(1,IS),NT1*2)                                      
21    CONTINUE                                                                  
      IF(ITIP.NE.MPCCOD)GOTO 25                                                 
C                                                                               
C     ASSEMBLAGGIO -MPC-                                                        
C                                                                               
      CALL ASSMPC(IPIAZ,KHED,LHED,LDEST,KDEST,                                  
     1  ASSMAT,NRASMT,ELMAT,NRELMT,IPLH,NPLH)                                   
      GOTO 30                                                                   
C                                                                               
C     ASSEMBLAGGIO ELEMENTI ED -SPC-                                            
C                                                                               
25    CALL ASSELM(IPIAZ,KHED,LHED,KDEST,LDEST,                                  
     1  ASSMAT,NRASMT,ELMAT,NRELMT,IPLH,NPLH,INDRM)                             
30    CONTINUE                                                                  
      IF(DUMP(9))WRITE(IOUT,507)NRIG,NCOL                                       
20    CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO SUGLI ELEMENTI                                             
C                                                                               
15    IRIGA=LINF+IK-1                                                           
      IPRIG=IK                                                                  
C                                                                               
C     SCRITTURA SU FILE DELLA EQUAZIONE IK-ESIMA                                
C                                                                               
      CALL WROUTE(IHED,LHED,IPIAZ,VETT,ASSMAT,NRASMT,IRIGA,NCOL,IPLH,           
     1            NPLH,IPRIG)                                                   
10    CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO SULLE EQUAZIONI                                            
C                                                                               
5     CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO SUI BLOCCHI DI ASSEMBLAGGIO                                
C                                                                               
      IF(NPSI.EQ.0) RETURN                                                      
C                                                                               
C     INIZIALIZZAZIONE DEI PARAMETRI RELATIVI ALL'ASSEMBLAGGIO                  
C     DELLE EQUAZIONI DEI PARAMETRI DI DEFORMAZIONE                             
C                                                                               
      LINF=NEQV-NPSI+1                                                          
      LSUP=NEQV                                                                 
      NCOL=0                                                                    
      NRIG=NPSI                                                                 
      IPLH=1                                                                    
      NPLH=0                                                                    
      DO 8 IL=1,NPSI                                                            
      KHED(IL)=LINF+IL-1                                                        
8     CONTINUE                                                                  
C                                                                               
C     CICLO DI ASSEMBLAGGIO SU TUTTI GLI ELEMENTI CHE NON SONO                  
C         -MPC- OD -SPC-                                                        
C                                                                               
      CALL JPOSAB(ISLVPZ,1)                                                     
      DO 400 K=1,NELEM                                                          
      IND=K                                                                     
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT  ,1)                                                 
      CALL JREAD(ISLVPZ,NT1 ,1)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NT)                                               
      NTM1=NT-1                                                                 
      DO 401 IS=1,NTM1                                                          
      CALL JREAD(ISLVPZ,ELMAT(1,IS),NT1*2)                                      
401   CONTINUE                                                                  
      IF(ITIP.EQ.MPCCOD)GOTO 400                                                
      IF(ITIP.EQ.SPCCOD)GOTO 400                                                
C                                                                               
C     ASSEMBLAGGIO DELL'ELEMENTO K-ESIMO                                        
C                                                                               
      CALL ASSELM(IPIAZ,KHED,LHED,KDEST,LDEST,                                  
     1   ASSPSI,NRASPS,ELMAT,NRELMT,IPLH,NPLH,INDRM)                            
400   CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO DI ASSEMBLAGGIO                                            
C                                                                               
C                                                                               
C     VENGONO VINCOLATI I PARAMETRI DI DEFORMAZIONE SPECIFICATI IN IPGCO        
C                                                                               
      DO 410 IL=1,NPSI                                                          
      IF(IPGCON(IL).EQ.0) GOTO 410                                              
      ASSPSI(IL,IL)=CONST                                                       
410   CONTINUE                                                                  
C     SCRITTURA SU FILE DELLE EQUAZIONI COMPLETATE                              
C                                                                               
      IBASE=NEQV-NPSI                                                           
      DO 90 IL=1,NPSI                                                           
      IRIGA=IBASE+IL                                                            
      IPRIG=IL                                                                  
      CALL WROUTE(IHED,LHED,IPIAZ,VETT,ASSPSI,NRASPS,IRIGA,NCOL,IPLH,           
     1            NPLH,IPRIG)                                                   
90    CONTINUE                                                                  
      RETURN                                                                    
200   FORMAT(//' ***   STAMPE DI CONTROLLO DELLA ROUTINE DI ASSEMBLAGG',        
     1   'IO FRONTALE   ***'/)                                                  
201   FORMAT(/'  N.RO DI BLOCCHI DI ASSEMBLAGGIO :',I8)                         
502   FORMAT(1X,12E10.4)                                                        
202   FORMAT(/'  BLOCCO DI ASSEMBLAGGIO N.RO',I4,' COMPOSTO DA ',I4,            
     1   '  EQUAZIONI, LIMITI INFERIORE E SUPERIORE :',2I6)                     
507   FORMAT(/T10,' NUMERO DI RIGHE E COLONNE ASSMAT',2I8)                      
      END                                                                       
C@ELT,I ANBA*ANBA.ASSMPC                                                        
C*************************          ASSMPC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE ASSMPC(IPIAZ,KHED,LHED,LDEST,KDEST,                            
     1  ASSMAT,NRASMT,ELMAT,NRELMT,IPLH,NPLH)                                   
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE ESEGUE L'ASSEMBLAGGIO DEI VINCOLI DI TIPO -MPC-         
C                                                                               
C     L'EQUAZIONE DI VINCOLO VIENE ASSEMBLATA DOPO QUELLA CORRISPONDENTE        
C     AL PRIMO TERMINE INDICATO NELLA SEQUENZA DI INGRESSO                      
C                                                                               
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),ASSMAT(NRASMT,1),ELMAT(NRELMT,1)                       
      DIMENSION KHED(1),LHED(1),LDEST(1),KDEST(1)                               
      COMMON /PARASS/ ITIP,IND,NT,NT1,LINF,LSUP,NRIG,NCOL,nth                   
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3            
      IF(DUMP(9)) WRITE(IOUT,200)                                               
      ELMAT(1,NT)=0.D0                                                          
C                                                                               
C     RIORDINO DEI DATI RELATIVI ALL'MPC CONSIDERATO IN BASE AL CONTENUT        
C     DEL VETTORE DI PIAZZAMENTO                                                
C                                                                               
      CALL SHLSRT(NT-1,IPIAZ,ELMAT,1,1,2)                                       
C                                                                               
C     PREPARAZIONE DEI PUNTATORI PER L'ASSEMBLAGGIO DEI TERMINI DELLA           
C     EQUAZIONE DI VINCOLO DI ORDINE INFERIORE A QUELLO DIAGONALE               
C                                                                               
      DO 10 LK=1,NT                                                             
      IEQ=IPIAZ(LK)                                                             
C                                                                               
C     TEST DI INDIVIDUAZIONE DEL TERMINE CORRISPONDENTE AL N.RO DELLA           
C     EQUAZIONE                                                                 
C                                                                               
      IF(IEQ.GT.IPIAZ(NT)) GOTO 30                                              
C                                                                               
C     VERIFICA DELL'APPARTENENZA AL BLOCCO DI ASSEMBLAGGIO DEL                  
C     TERMINE -IEQ-                                                             
C                                                                               
      IF(IEQ.LT.LINF)GOTO 15                                                    
      IF(IEQ.GT.LSUP)GOTO 245                                                   
      IRIG=IEQ-LINF+1                                                           
C                                                                               
C     VIENE COSTRUITO IL VETTORE DI INDIRIZZAMENTO                              
C                                                                               
      KDEST(LK)=IRIG                                                            
      GOTO 10                                                                   
C                                                                               
C     LA RIGA -IEQ- NON FA PARTE DEL BLOCCO DI ASSEMBLAGGIO                     
C                                                                               
   15 KDEST(LK)=0                                                               
   10 CONTINUE                                                                  
C                                                                               
C     VIENE INSERITA LA COLONNA CORRISPONDENTE ALL'EQUAZIONE DI                 
C     VINCOLO (SICURAMENTE NON ANCORA PRESENTE)                                 
C                                                                               
   30 CONTINUE                                                                  
      GOTO(3,4),IPLH                                                            
 4    DO 165 IE=1,NCOL                                                          
      IF(LHED(IE).LT.0)GOTO 168                                                 
 165  CONTINUE                                                                  
      WRITE(IOUT,510)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 3    NCOL=NCOL+1                                                               
C                                                                               
C     TEST SULLE DIMENSIONI RAGGIUNTE DA 'ASSMAT'                               
C                                                                               
      IF(NCOL.GT.MAXLRG) GOTO 600                                               
      INDC=NCOL                                                                 
      LHED(NCOL)=IPIAZ(NT)                                                      
      GOTO 130                                                                  
 168  INDC=IE                                                                   
      LHED(IE)=IPIAZ(NT)                                                        
      NPLH=NPLH-1                                                               
      IF(NPLH.EQ.0)IPLH=1                                                       
 130  CONTINUE                                                                  
C                                                                               
C     ASSEMBLAGGIO DEI TERMINI DI ORDINE INFERIORE AL DIAGONALE                 
C                                                                               
      NSUP=LK-1                                                                 
      DO 35 J=1,NSUP                                                            
      INDR=KDEST(J)                                                             
      ASSMAT(INDR,INDC)=ASSMAT(INDR,INDC)+ELMAT(1,J)                            
   35 CONTINUE                                                                  
      JJ=LK                                                                     
      IF(DUMP(9)) THEN                                                          
      WRITE(IOUT,500)IND,ITIP,NT                                                
      WRITE(IOUT,503)INDC                                                       
      JJM1=JJ-1                                                                 
      WRITE(IOUT,504)(KDEST(IS),IS=1,JJM1)                                      
      WRITE(IOUT,505)(LHED(IS),IS=1,NCOL)                                       
      END IF                                                                    
   40 CONTINUE                                                                  
      IEQ=IPIAZ(NT)                                                             
C                                                                               
C     PREPARAZIONE DEI PUNTATORI PER L'ASSEMBLAGGIO DEI TERMINI DELLA           
C     EQUAZIONE DI VINCOLO DI ORDINE SUPERIORE A QUELLO DIAGONALE               
C                                                                               
C                                                                               
C     VERIFICA DELL'APPARTENENZA AL BLOCCO DI ASSEMBLAGGIO DELLA                
C     EQUAZIONE DI VINCOLO -IEQ-                                                
C                                                                               
      IF(IEQ.LT.LINF)GOTO 246                                                   
      IF(IEQ.GT.LSUP)GOTO 245                                                   
C                                                                               
C     ASSEMBLAGGIO DELL'EQUAZIONE DI VINCOLO                                    
C                                                                               
      INDR=IEQ-LINF+1                                                           
C                                                                               
C     PREPARAZIONE DEI PUNTATORI PER L'ASSEMBLAGGIO DEI TERMINI DELLA           
C     EQUAZIONE DI VINCOLO DI ORDINE SUPERIORE A QUELLO DIAGONALE               
C                                                                               
      DO 230 LK=JJ,NT                                                           
      IEQ=IPIAZ(LK)                                                             
      IF(NCOL.EQ.0)GOTO 262                                                     
C                                                                               
C     VIENE CONTROLLATO SE LA COLONNA NON E' GIA' STATA INSERITA NEL            
C     BLOCCO DI ASSMEBLAGGIO                                                    
C                                                                               
      LL=IPOSL(IEQ,LHED,NCOL)                                                   
      IF(LL.NE.0)GOTO 280                                                       
C                                                                               
C     LA COLONNA -IEQ- VIENE INSERITA NEL BLOCCO DI ASSEMBLAGGIO                
C                                                                               
 262  CONTINUE                                                                  
      GOTO (1,2),IPLH                                                           
2     DO 265 IE=1,NCOL                                                          
      IF(LHED(IE).LT.0) GOTO 268                                                
 265  CONTINUE                                                                  
      WRITE(IOUT,510)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 1    NCOL=NCOL+1                                                               
C                                                                               
C     TEST SULLE DIMENSIONI RAGGIUNTE DA 'ASSMAT'                               
C                                                                               
      IF(NCOL.GT.MAXLRG) GOTO 600                                               
      LDEST(LK)=NCOL                                                            
      LHED(NCOL)=IEQ                                                            
      GOTO 230                                                                  
268   LDEST(LK)=IE                                                              
      LHED(IE)=IEQ                                                              
      NPLH=NPLH-1                                                               
      IF(NPLH.EQ.0) IPLH=1                                                      
      GOTO 230                                                                  
C                                                                               
C     LA COLONNA -IEQ- E' GIA' PRESENTE NEL BLOCCO DI ASSEMBLAGGIO              
C                                                                               
  280 LDEST(LK)=LL                                                              
  230 CONTINUE                                                                  
      IF(DUMP(9))THEN                                                           
      WRITE(IOUT,501)(IPIAZ(IS),IS=1,NT)                                        
      WRITE(IOUT,509)                                                           
      DO 555 MS=1,NT1                                                           
      WRITE(IOUT,502)(ELMAT(1,IS),IS=1,NT)                                      
  555 CONTINUE                                                                  
      WRITE(IOUT,503)(LDEST(IS),IS=JJ,NT)                                       
      WRITE(IOUT,504)INDR                                                       
      WRITE(IOUT,505)(LHED(IS),IS=1,NCOL)                                       
      END IF                                                                    
C                                                                               
C     ASSEMBLAGGIO DEI TERMINI DELL'EQUAZIONE DI VINCOLO                        
C                                                                               
      DO 100 J2=JJ,NT                                                           
      INDC=LDEST(J2)                                                            
      ASSMAT(INDR,INDC)=ASSMAT(INDR,INDC)+ELMAT(1,J2)                           
  100 CONTINUE                                                                  
  245 RETURN                                                                    
  246 WRITE(IOUT,901)IND                                                        
      RETURN                                                                    
600   WRITE(IOUT,204)NCOL,MAXLRG                                                
      CALL JHALT                                                                
      STOP                                                                      
204   FORMAT(' *** AVVISO (ASSMPC): -NCOL- DI -ASSMAT- (',I8,')',               
     1 ' SUPERA IL MASSIMO DIMENSIONAMENTO (',I8,')  ***')                      
  200 FORMAT(//' *** STAMPE DI CONTROLLO SUBR. "ASSMPC" ***')                   
  500 FORMAT(//'  ELEMENTO N.RO',I6,' DI TIPO',I6,' CON',I6,' TERMINI')         
  501 FORMAT(/10X,'VETTORE PIAZZAMENTO',/5(22I6/))                              
  502 FORMAT(12F10.4)                                                           
  503 FORMAT(/2X,' VETT. LDEST',16I6)                                           
  504 FORMAT(/2X,' VETT. KDEST',16I6)                                           
  505 FORMAT(/2X,' VETT. LHED ',16I6)                                           
  509 FORMAT(/10X,'MATRICE ELEMENTO'/)                                          
  510 FORMAT(' *** AVVISO (ASSMPC): ERRORE NELLA ASSEGNAZIONE DEL VALORE        
     1 DI - IPLH-  ***')                                                        
  901 FORMAT('  SUBR. ASSMPC : ELEMENTO N.RO',I5,' NON UTILIZZ.')               
      END                                                                       
C@ELT,I ANBA*ANBA.BMAT                                                          
C*************************          BMAT          **********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE BMAT(XY,NCON,P,XX,YY,B,SHAPE,Z,DS,DX,DY)                             
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION XY(1),B(4),SHAPE(4),Z(2,6),X(2)                                 
      DIMENSION DERIV(4)                                                        
C                                                                               
C                                                                               
C     NCON =  2,3,4 NODI DEL PANNELLO                                           
C     P    =  PUNTO DI INTEGRAZIONE                                             
C     XY   =  COORDINATE NODI                                                   
C     XX,YY=  COORDINATE NEL PUNTO DI INTEGRAZIONE                              
C     B    =  VETTORE DERIVATE                                                  
C     SHAPE=  VETTORE FUNZIONI DI FORMA                                         
C     Z    =  MATRICE Z                                                         
C                                                                               
      P2=P*P                                                                    
      IGOTO=NCON-1                                                              
      GOTO(100,200,300),IGOTO                                                   
100   SHAPE(1)=.5*(1-P)                                                         
      SHAPE(2)=.5*(1+P)                                                         
      DERIV(1)=-.5                                                              
      DERIV(2)=.5                                                               
      GOTO 1000                                                                 
200   SHAPE(1)=.5*(P2-P)                                                        
      SHAPE(2)=1.-P2                                                            
      SHAPE(3)=.5*(P2+P)                                                        
      DERIV(1)=P-.5                                                             
      DERIV(2)=-2.*P                                                            
      DERIV(3)=P+.5                                                             
      GOTO 1000                                                                 
300   SHAPE(1)=(-1.+P*(1.+P*(9.-9.*P)))/16.                                     
      SHAPE(2)=(9.+P*(-27.+P*(-9.+27.*P)))/16.                                  
      SHAPE(3)=(9.+P*(27.+P*(-9.-27.*P)))/16.                                   
      SHAPE(4)=(-1.+P*(-1.+P*(9.+9.*P)))/16.                                    
      DERIV(1)=(1.+P*(18.-27.*P))/16.                                           
      DERIV(2)=(-27.+P*(-18.+81.*P))/16.                                        
      DERIV(3)=(27.+P*(-18.-81.*P))/16.                                         
      DERIV(4)=(-1.+P*(18.+27.*P))/16.                                          
C                                                                               
C                                                                               
C     CALCOLO COORDINATE PUNTO DI INTEGRAZIONE                                  
C                                                                               
C                                                                               
1000  DO 1 I = 1,2                                                              
      X(I)=0.                                                                   
      DO 1 K=1,NCON                                                             
      KK=2*(K-1)+I                                                              
1     X(I)=X(I)+SHAPE(K)*XY(KK)
C                                                                               
C                                                                               
C     CALCOLO DX DY E DS NEL PUNTO DI INTEGRAZIONE                              
C                                                                               
C                                                                               
      DX=0.                                                                     
      DY=0.                                                                     
      DO 2 K=1,NCON                                                             
      DX=DX+DERIV(K)*XY(2*K-1)                                                  
2     DY=DY+DERIV(K)*XY(2*K)                                                    
      DS2=DX*DX+DY*DY                                                           
      DS=DSQRT(DS2)                                                             
      DO 3K=1,NCON                                                              
3     B(K)=DERIV(K)/DS                                                          
        DX=DX/DS                                                              
        DY=DY/DS                                                              
      DO4 I=1,2                                                                 
      DO4J=1,6                                                                  
4     Z(I,J)=0.                                                                 
      Z(1,3)=1.                                                                 
      Z(1,4)=X(2)                                                               
      Z(1,5)=-X(1)                                                              
c      Z(2,1)=DX/DS                                                              
c      Z(2,2)=DY/DS                                                              
c      Z(2,6)=(DY*X(1)-DX*X(2))/DS                                               
        Z(2,1)=DX                                                              
        Z(2,2)=DY                                                              
        Z(2,6)=(DY*X(1)-DX*X(2))                                               
      XX=X(1)                                                                   
      YY=X(2)                       
      RETURN                                                                    
      END                                                                       

C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C------------------------------------------------     BDGFBD     -------
C-----------------------------------------------------------------------
C
C     BLOCK DATA GESTIONE FILE - GFBD
C
C     I PARAMETRI EVENTUALMENTE MODIFICABILI DA PARTE DELL'UTENTE
C     SONO:
C     - TMAXFL = NUMERO MASSIMO DI FILES POSSIBILI ALL'INTERNO
C                DELLA BASE DATI (VALORE ATTUALE=200)
C     - TMAXOP = NUMERO MASSIMO DI FILES DELLA BASE DATI APERTI
C                SIMULTANEAMENTE (VALORE ATTUALE=7). TALE VALORE
C                DEVE ESSERE UGUALE A TMAXDB
C     NEL CASO IN CUI VENGANO AGGIORNATI TALI VALORI E' NECESSARIO
C     VERIFICARE IL DIMESIONAMENTO DEI COMMON
C
      BLOCK DATA BDGFBD
C
      IMPLICIT INTEGER (A-Z)
C
C     COMMON AVENTI DIMENSIONE FISSATA
C
C     COMMON SETTAGGIO PARAMETRI BASE DATI
      COMMON /JSETPR/ TNWSCT,TNSTRK,TNWPAG,TNSBP1,TNSBP2,TMSKSH,
     *                TSCTSH,TUNISH,TNWCPL,TMAXFL,TMAXOP,TMAXDB
C
C     COMMON CHE DEFINISCE I FILES DI SISTEMA
C     SYSFIL E' DIMENSIONATO 2*NSYFIL
      COMMON /JSYFIL/ MASTER,NSYFIL,SYSFIL(2)
C
      COMMON /JBIT/ SHIFT(16)
C
C     COMMON /JPOOL/ DIMENSIONE=TNWCPL
      COMMON /JPOOL/ POOL(512)
C
C     COMMON AVENTI DIMENSIONE VARIABILE IN FUNZIONE DEI PARAMETRI
C     TMAXFL E TMAXOP. IN QUESTO CASO LA PRIMA PAROLA DEL COMMON
C     DEVE CONTENERE IL DIMENSIONAMENTO EFFETTIVO
C
C     COMMOM /JPAR/ DIMENSIONE=58+5*TMAXFL+2*NSYFIL
      COMMON /JPAR/ PAR(1060)
C
C     COMMON /JBFMNG/ DIMENSIONE=9+TMAXOP*(2+15+TMAXDB+
C                     TNWPAG/TNSBP1+TNWPAG/TNSBP2)+TMAXDB
      COMMON /JBFMNG/ BUFMNG(7352)
C
C     COMMON /JDATBF/ DIMENSIONE=TMAXDB*(2+TNWPAG)
      COMMON /JDATBF/ DATBUF(28686)
C
ccc   COMMON/JFTNIO/ IINP,IOUT
C
C
C---- INIZIALIZZAZIONI -------------------------------------------------
C
      DATA TNWSCT/512/, TNSTRK/8/   , TNWPAG/4096/, TNSBP1/8/,
     *     TNSBP2/8/  , TMSKSH/8/   , TSCTSH/15/  , TUNISH/3/,
     *     TNWCPL/512/, TMAXFL/200/ , TMAXOP/7/   , TMAXDB/7/
C
      DATA PAR /1060,1059*0/
C
C     DEFINIZIONE DEI FILES DI SISTEMA
C        FILE MASTER = MASTER
C        FILES DI BASE DATI(NSYFIL) = 1 = DATABASE
C        IL FILE DATABASE E' DIMENSIONATO DI 16000 PAGINE
      DATA MASTER /2/
      DATA NSYFIL /1/
      DATA SYSFIL /3,16000/
      DATA SHIFT /1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192,
     *            16384,32768/
      DATA POOL /512,511*0/
      DATA BUFMNG /7352,7351*0/
      DATA DATBUF /28686,28685*0/
C
ccc   DATA IINP/5/,IOUT/6/
C
C
      END

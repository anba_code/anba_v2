C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.CASELM                                                        
C*************************          CASELM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CASELM(IPIAZ,KHED,LHED,KDEST,LDEST,                            
     1  CASSMT,NRASMT,COMPL,NRELMT,IPLH,NPLH,INDRM)                             
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 COMPL,CASSMT                                                   
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),CASSMT(NRASMT,1),COMPL(NRELMT,1)                       
      DIMENSION KHED(1),LHED(1),KDEST(1),LDEST(1)                               
      COMMON /PARASS/ITIP,IND,NT,NT1,LINF,LSUP,NRIG,NCOL,nth                    
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),IECHO,IAUTO,IGEOM,C                             
      IF(DUMP(9))WRITE(IOUT,200)                                                
      NT=NT-1                                                                   
C                                                                               
C     CALCOLO DEI VETTORI DI CARICAMENTO DEI TERMINI DI ELMAT                   
C                                                                               
      DO 30 LK=1,NT                                                             
      IEQ=IPIAZ(LK)                                                             
C                                                                               
C     CONTROLLO DI APPARTENENZA DELL'EQUAZIONE AL BLOCCO                        
C                                                                               
      IF(IEQ.LT.LINF)GOTO 45                                                    
      IF(IEQ.GT.LSUP)GOTO 45                                                    
      IRIG=IEQ-LINF+1                                                           
C                                                                               
C     LA RIGA 'IEQ' DEVE ESSERE INSERITA IN 'CASSMT'                            
C                                                                               
      KDEST(LK)=IRIG                                                            
      GOTO 35                                                                   
C                                                                               
C     LA RIGA 'IEQ' NON RIENTRA NEL BLOCCO DI ASSEMBLAGGIO                      
C                                                                               
   45 KDEST(LK)=0                                                               
C                                                                               
C     LA RIGA 'IEQ' E' GIA' PRESENTE NEL BLOCCO DI ASSEMBLAGGIO                 
C                                                                               
   35 IF(IEQ.LT.LINF)GOTO 62                                                    
      IF(NCOL.EQ.0)GOTO  1                                                      
      LL=IPOSL(IEQ,LHED,NCOL)                                                   
      IF(LL.NE.0) GOTO 80                                                       
C                                                                               
C     LA COLONNA 'IEQ' DEVE ESSERE INSERITA IN 'CASSMT'                         
C                                                                               
 60   CONTINUE                                                                  
C     WRITE(IOUT,505)(LHED(IS),IS=1,NCOL)                                       
      GOTO (1,2),IPLH                                                           
2     DO 61 IE=1,NCOL                                                           
      IF(LHED(IE).LT.0) GOTO 31                                                 
61    CONTINUE                                                                  
      WRITE(IOUT,510) IEQ,IPLH,NPLH                                             
      CALL JHALT                                                                
      STOP                                                                      
   1  NCOL=NCOL+1                                                               
C                                                                               
C     TEST SULLE DIMENSIONI RAGGIUNTE DA 'ASSMAT'                               
C                                                                               
      IF(NCOL.GT.MAXLRG) GOTO 600                                               
      LDEST(LK)=NCOL                                                            
      LHED(NCOL)=IEQ                                                            
      GOTO 30                                                                   
 31   LDEST(LK)=IE                                                              
      LHED(IE)=IEQ                                                              
      NPLH=NPLH-1                                                               
      IF(NPLH.EQ.0) IPLH=1                                                      
      GOTO 30                                                                   
C                                                                               
C     LA COLONNA 'IEQ' NON RIENTRA NEL BLOCCO DI ASSEMBLAGGIO                   
C                                                                               
   62 LDEST(LK)=0                                                               
      GOTO 30                                                                   
C                                                                               
C     LA COLONNA 'IEQ' E' GIA' PRESENTE NEL BLOCCO DI ASSEMBLAGGIO              
C                                                                               
   80 LDEST(LK)=LL                                                              
   30 CONTINUE                                                                  
   65 CONTINUE                                                                  
      IF(DUMP(9)) THEN                                                          
      WRITE(IOUT,500)IND,ITIP,NT                                                
      WRITE(IOUT,501)(IPIAZ(IS),IS=1,NT)                                        
      WRITE(IOUT,503)(LDEST(IS),IS=1,NT)                                        
      WRITE(IOUT,505)(LHED(IS),IS=1,NCOL)                                       
      END IF                                                                    
C                                                                               
C     INIZIO DEL CICLO DI ASSEMBLAGGIO DEI TERMINI DI 'COMPL'                   
C                                                                               
C   CONTROLLARE SE IL CICLO CHE SEGUE  E' CORRETTO                              
      DO 90 J1=1,NT                                                             
      IND1=IPIAZ(J1)                                                            
      DO 100 J2=J1,NT                                                           
      IP1=J1                                                                    
      IP2=J2                                                                    
      IND2=IPIAZ(IP2)                                                           
C                                                                               
C     VIENE ASSEMBLATO SOLO IL TRIANGOLO SUPERIORE                              
C                                                                               
      IF(IND1.LE.IND2)GOTO 110                                                  
      IS=IP1                                                                    
      IP1=IP2                                                                   
      IP2=IS                                                                    
      COMPL(J1,J2)=DCONJG(COMPL(J1,J2))                                         
 110  CONTINUE                                                                  
      INDR=KDEST(IP1)                                                           
      IF(INDRM.LT.INDR) INDRM=INDR                                              
      IF(INDR.EQ.0)GOTO 100                                                     
      INDC=LDEST(IP2)                                                           
      IF(INDC.EQ.0)GOTO 100                                                     
      CASSMT(INDR,INDC)=CASSMT(INDR,INDC)+COMPL(J1,J2)                          
  100 CONTINUE                                                                  
   90 CONTINUE                                                                  
C                                                                               
C     FINE CICLO DI ASSEMBLAGGIO                                                
C                                                                               
      RETURN                                                                    
600   WRITE(IOUT,204)NCOL,MAXLRG                                                
      CALL JHALT                                                                
      STOP                                                                      
204   FORMAT(' *** AVVISO (CASELM): -NCOL- DI -CASSMT- (',I8,')',               
     1 ' SUPERA IL MASSIMO DIMENSIONAMENTO (',I8,')  ***')                      
 200  FORMAT(//' *** STAMPE DI CONTROLLO SUBROUTINE "CASELM" ***')              
 500  FORMAT(//'  ELEMENTO N.RO',I6,' DI TIPO',I6,' CON',I6,' TERMINI')         
 501  FORMAT(/10X,'VETTORE PIAZZAMENTO',/5(22I6/))                              
 503  FORMAT(/2X,' VETT. LDEST',18I6)                                           
 505  FORMAT(/2X,' VETT. LHED ',18I6)                                           
510   FORMAT(' *** AVVISO (CASELM): ERRORE NELLA ASSEGNAZIONE DEL VALORE        
     1 DI -IPLH-  EQUAZ. N. ',I6,'  IPLH,NPLH',2I6,'  ***')                     
      END                                                                       
C@ELT,I ANBA*ANBA.CASFRO                                                        
C*************************         CASFRO          ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CASFRO(IELEM,IPIAZ,KHED,LHED,KDEST,LDEST,                      
     1  IHED,CASSMT,NRASMT,CVETT,ELMAT,NRELMT,CASPSI,NRASPS,NEQAS,IPELM,        
     2  COMPL)                                                                  
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 CASSMT,CASPSI,COMPL                                            
      LOGICAL DUMP,incore                                                       
      INTEGER SPCCOD                                                            
      DIMENSION IELEM(1),IPIAZ(1),CASSMT(NRASMT,1),IPELM(1)                     
      DIMENSION KHED(1),LHED(1),KDEST(1),LDEST(1),CVETT(1)                      
      DIMENSION IHED(1),ELMAT(NRELMT,1),CASPSI(NRASPS,1)                        
      DIMENSION COMPL(NRELMT,1)                                                 
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3            
      COMMON /PARASS/ ITIP,IND,NT,NT1,LINF,LSUP,NRIG,NCOL,NTH                   
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),IECHO,IAUTO,IGEOM,C                             
      DATA MPCCOD/6/,SPCCOD/7/                                                  
      LSUP=0                                                                    
      IF (DUMP(9))WRITE(IOUT,200)                                               
C                                                                               
C     CALCOLO DELLE DIMENSIONI DEI BLOCCHI DI ASSEMBLAGGIO                      
C                                                                               
      NBLAS=(NEQV-NPSI+NEQAS-1)/NEQAS                                           
      IF(DUMP(9))WRITE(IOUT,201)NBLAS                                           
      MAXFRO=0                                                                  
      NCOL=0                                                                    
      IPLH=1                                                                    
      NPLH=0                                                                    
C                                                                               
C     CICLO DI ASSEMBLAGGIO PER BLOCCHI DELLA MATRICE DEL SISTEMA               
C                                                                               
      DO 5 I=1,NBLAS                                                            
      INDRM=0                                                                   
      IF(I.EQ.NBLAS) NEQAS=NEQV-NPSI-(NBLAS-1)*NEQAS                            
      LINF=LSUP+1                                                               
      LSUP=LSUP+NEQAS                                                           
      IF (DUMP(9))WRITE(IOUT,202)I,NEQAS,LINF,LSUP                              
C                                                                               
C     CICLO DI ASSEMBLAGGIO DELLE EQUAZIONI DEL BLOCCO I-ESIMO                  
C                                                                               
      DO 7 IL=1,NEQAS                                                           
      KHED(IL)=LINF+IL-1                                                        
7     CONTINUE                                                                  
      NRIG=NEQAS                                                                
      DO 10 IK=1,NEQAS                                                          
      CALL JREAD(ISLTAB,NELEQI,1)                                               
      CALL JREAD(ISLTAB,IELEM,NELEQI)                                           
      IF(NELEQI.EQ.0)GOTO 15                                                    
C                                                                               
C     CICLO DI ASSEMBLAGGIO DEGLI ELEMENTI CHE DANNO CONTRIBUTO                 
C     ALL'EQUAZIONE IK-ESIMA                                                    
C                                                                               
      DO 20 K=1,NELEQI                                                          
      IND=IELEM(K)                                                              
      IPO=IPELM(IND)                                                            
      IPF=IPELM(IND+NELEM)                                                      
      IPN=IPELM(IND+2*NELEM)                                                    
      CALL JPOSAB(ISLVPZ,IPO)                                                   
      CALL JPOSAB(ISLMTF,IPF)                                                   
      CALL JPOSAB(ISLMTN,IPN)                                                   
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT  ,1)                                                 
      CALL JREAD(ISLVPZ,NT1 ,1)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NT)                                               
      NTM1=NT-1                                                                 
      DO 21 IS=1,NTM1                                                           
      CALL JREAD(ISLVPZ,ELMAT(1,IS),NT1*2)                                      
21    CONTINUE                                                                  
      IF(ITIP.NE.MPCCOD)GOTO 25                                                 
C                                                                               
C     ASSEMBLAGGIO -MPC-                                                        
C                                                                               
C      CALL ASSMPC(IPIAZ,KHED,LHED,LDEST,KDEST,                                 
C    1  ASSMAT,NRASMT,ELMAT,NRELMT,IPLH,NPLH)                                   
C      GOTO 3000                                                                
C                                                                               
C     ASSEMBLAGGIO ELEMENTI ED -SPC-                                            
C                                                                               
      WRITE(IOUT,508)                                                           
      GOTO 3000                                                                 
25    CONTINUE                                                                  
      DO 30 IR=1,NT1                                                            
      DO 35 IC=1,NT1                                                            
      COMPL(IR,IC)=DCMPLX(ELMAT(IR,IC),0.D0)                                    
35    CONTINUE                                                                  
30    CONTINUE                                                                  
      IF (ITIP.NE.6.AND.ITIP.NE.7)THEN                                          
           NTNPSI=(NT1-NPSI)*2                                                  
           DO 40 IS=1,NT1-NPSI                                                  
           CALL JREAD(ISLMTF,ELMAT(1,IS),NTNPSI)                                
40        CONTINUE                                                              
            DO 50 IR=1,NT1-NPSI                                                 
            DO 55 IC=1,NT1-NPSI                                                 
            COMPL(IR,IC)=COMPL(IR,IC)+DCMPLX(ELMAT(IR,IC)*C**2.,0.D0)           
55        CONTINUE                                                              
50        CONTINUE                                                              
      DO 60 IS=1,NT1                                                            
      CALL JREAD(ISLMTN,ELMAT(1,IS),NT1*2)                                      
60    CONTINUE                                                                  
      DO 70 IR=1,NT1                                                            
      DO 75 IC=1,NT1                                                            
      COMPL(IR,IC)=COMPL(IR,IC)-DCMPLX(0.D0,ELMAT(IR,IC)*C)                     
75    CONTINUE                                                                  
70    CONTINUE                                                                  
      ENDIF                                                                     
444   FORMAT((1X,10E10.4))                                                      
      CALL CASELM(IPIAZ,KHED,LHED,KDEST,LDEST,                                  
     1  CASSMT,NRASMT,COMPL,NRELMT,IPLH,NPLH,INDRM)                             
      IF(DUMP(9))WRITE(IOUT,507)NRIG,NCOL                                       
20    CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO SUGLI ELEMENTI                                             
C                                                                               
15    IRIGA=LINF+IK-1                                                           
      IPRIG=IK                                                                  
C                                                                               
C     SCRITTURA SU FILE DELLA EQUAZIONE IK-ESIMA                                
C                                                                               
      CALL CWROUT(IHED,LHED,IPIAZ,CVETT,CASSMT,NRASMT,IRIGA,NCOL,IPLH,          
     1            NPLH,IPRIG)                                                   
10    CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO SULLE EQUAZIONI                                            
C                                                                               
5     CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO SUI BLOCCHI DI ASSEMBLAGGIO                                
C                                                                               
      IF(NPSI.EQ.0) RETURN                                                      
C                                                                               
C     INIZIALIZZAZIONE DEI PARAMETRI RELATIVI ALL'ASSEMBLAGGIO                  
C     DELLE EQUAZIONI DEI PARAMETRI DI DEFORMAZIONE                             
C                                                                               
      LINF=NEQV-NPSI+1                                                          
      LSUP=NEQV                                                                 
      NCOL=0                                                                    
      NRIG=NPSI                                                                 
      IPLH=1                                                                    
      NPLH=0                                                                    
      DO 8 IL=1,NPSI                                                            
      KHED(IL)=LINF+IL-1                                                        
8     CONTINUE                                                                  
C                                                                               
C     CICLO DI ASSEMBLAGGIO SU TUTTI GLI ELEMENTI CHE NON SONO                  
C         -MPC- OD -SPC-                                                        
C                                                                               
      CALL JPOSAB(ISLVPZ,1)                                                     
      CALL JPOSAB(ISLMTN,1)                                                     
      DO 80 K=1,NELEM                                                           
      IND=K                                                                     
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT  ,1)                                                 
      CALL JREAD(ISLVPZ,NT1 ,1)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NT)                                               
      NTM1=NT-1                                                                 
      DO 85 IS=1,NTM1                                                           
      CALL JREAD(ISLVPZ,ELMAT(1,IS),NT1*2)                                      
85    CONTINUE                                                                  
      IF(ITIP.EQ.MPCCOD)GOTO 80                                                 
      IF(ITIP.EQ.SPCCOD)GOTO 80                                                 
      DO 110 IR=1,NT1                                                           
      DO 115 IC=1,NT1                                                           
      COMPL(IR,IC)=DCMPLX(ELMAT(IR,IC),0.D0)                                    
115   CONTINUE                                                                  
110   CONTINUE                                                                  
      DO 95 IS=1,NT1                                                            
      CALL JREAD(ISLMTN,ELMAT(1,IS),NT1*2)                                      
95    CONTINUE                                                                  
      DO 100 IR=1,NT1                                                           
      DO 105 IC=1,NT1                                                           
      COMPL(IR,IC)=COMPL(IR,IC)-DCMPLX(0.D0,ELMAT(IR,IC)*C)                     
105   CONTINUE                                                                  
100   CONTINUE                                                                  
C                                                                               
C     ASSEMBLAGGIO DELL'ELEMENTO K-ESIMO                                        
C                                                                               
      CALL CASELM(IPIAZ,KHED,LHED,KDEST,LDEST,                                  
     1   CASPSI,NRASPS,COMPL,NRELMT,IPLH,NPLH,INDRM)                            
80    CONTINUE                                                                  
C                                                                               
C     FINE DEL CICLO DI ASSEMBLAGGIO                                            
C                                                                               
C                                                                               
C     VENGONO VINCOLATI I PARAMETRI DI DEFORMAZIONE SPECIFICATI IN IPGCO        
C                                                                               
      DO 410 IL=1,NPSI                                                          
      IF(IPGCON(IL).EQ.0) GOTO 410                                              
      CASPSI(IL,IL)=CONST                                                       
410   CONTINUE                                                                  
C     SCRITTURA SU FILE DELLE EQUAZIONI COMPLETATE                              
C                                                                               
      IBASE=NEQV-NPSI                                                           
      DO 90 IL=1,NPSI                                                           
      IRIGA=IBASE+IL                                                            
      IPRIG=IL                                                                  
      CALL CWROUT(IHED,LHED,IPIAZ,CVETT,CASPSI,NRASPS,IRIGA,NCOL,IPLH,          
     1            NPLH,IPRIG)                                                   
90    CONTINUE                                                                  
      RETURN                                                                    
3000  CALL JHALT                                                                
      STOP                                                                      
508   FORMAT(' ***** IL PROGRAMMA SI INTERROMPE PERCHE'' NELLA VERSIONE         
     1PER LA TRAVE CURVA',/,' NON SONO DISPONIBILI GLI ELEMENTI DI TIPO         
     2 MPC!')                                                                   
200   FORMAT(//' ***   STAMPE DI CONTROLLO DELLA ROUTINE DI ASSEMBLAGG',        
     1   'IO FRONTALE  PARTE COMPLESSA  ***'/)                                  
201   FORMAT(/'  N.RO DI BLOCCHI DI ASSEMBLAGGIO :',I8)                         
502   FORMAT(1X,12E10.4)                                                        
202   FORMAT(/'  BLOCCO DI ASSEMBLAGGIO N.RO',I4,' COMPOSTO DA ',I4,            
     1   '  EQUAZIONI, LIMITI INFERIORE E SUPERIORE :',2I6)                     
507   FORMAT(/T10,' NUMERO DI RIGHE E COLONNE CASSMT',2I8)                      
      END                                                                       
C@ELT,I ANBA*ANBA.OUCASB                                                        
C*************************          OUCASB          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE OUCASB                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQAS,NST        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),IECHO,IAUTO,IGEOM,C                             
C                                                                               
      do 8513 iopl=1,maxdim                                                     
8513  iv(iopl)=0                                                                
C                                                                               
CCCC     ISTRUZIONE CAMBIATA PER AGGIORNAMENTO ROUTINES MMS                     
C                                                                               
C      MEMDIS=MAXDIM-5*MAXLRG-2*MAXDEL-MAXDEL*MAXDEL*2-NELMAX-NELEM             
C    *       -14*7-3-3*898                                                      
C                                                                               
      MEMDIS=MAXDIM-7*MAXLRG-2*MAXDEL-MAXDEL*MAXDEL*6-NELMAX-NELEM*3            
     *       -14*8                                                              
      NEQAS=MEMDIS/(1+MAXLRG*4)                                                 
      IF(NEQAS.GT.NEQV-NPSI)NEQAS=NEQV-NPSI                                     
      NBLOC=(NEQV-NPSI+NEQAS-1)/NEQAS                                           
C                                                                               
CCCC     ISTRUZIONE CAMBIATA PER AGGIORNAMENTO ROUTINES MMS                     
C                                                                               
                                                                                
C      MEMDIS=MAXDIM-MAXDEL-MAXDEL-NELEM-9*10                                   
C                                                                               
      MEMDIS=MAXDIM-MAXDEL-MAXDEL-NELEM-9*10                                    
      INE=(MEMDIS-NBLOC)/(NBLOC+NBLOC)                                          
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL      ,JIPIAZ)                
      CALL JALLOC(*2000,'IVBL    ','INT.',1,MAXDEL      ,JIVBL )                
      CALL JALLOC(*2000,'LNBL    ','INT.',1,NBLOC       ,JLNBL )                
      CALL JALLOC(*2000,'MTEL    ','INT.',1,NBLOC*INE   ,JMTEL )                
      CALL JALLOC(*2000,'MTEQ    ','INT.',1,NBLOC*INE   ,JMTEQ )                
      CALL JALLOC(*2000,'IVORD   ','INT.',1,NELEM       ,JIVORD)                
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*TAB    ',ISLTAB,1)                                           
      CALL JOPEN('*TEM    ',ISLTEM,1)                                           
C                                                                               
C                                                                               
C      JLIM=JIVORD+NELEM-1                                                      
C      KK=JLIM-JIPIAZ                                                           
c      CALL IZERO(IV(JIPIAZ),KK)                                                
      NNEQ=NEQAS                                                                
      NEQAS1=NEQAS                                                              
C                                                                               
      print*,' ***   INIZIO COSTRUZIONE TABELLA DI ASSEMBLAGGIO   ***'          
      CALL PRETAB(IV(JIPIAZ),IV(JIVORD),IV(JIVBL),IV(JLNBL),IV(JMTEL),          
     1     IV(JMTEQ),NNEQ,NBLOC,INE)                                            
      print*,' ***   FINE                                         ***'          
      CALL JCLOSE(ISLTEM)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JCLOSE(ISLTAB)                                                       
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      do 8514 iopl=1,maxdim                                                     
8514  iv(iopl)=0                                                                
      CALL JALLOC(*2000,'IPELM   ','INT.',1,NELEM      ,JIPELM)                 
      CALL JOPEN('*NWD    ',ISLNWD,1)                                           
      CALL JREAD(ISLNWD,IV(JIPELM),NELEM)                                       
      CALL JCLOSE(ISLNWD)                                                       
      MAXPZ=MAXLRG                                                              
      IF(MAXLRG.LT.MAXDEL) MAXPZ=MAXDEL                                         
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXPZ       ,JIPIAZ)                
      CALL JALLOC(*2000,'IELEM   ','INT.',1,NELMAX      ,JIELEM)                
      CALL JALLOC(*2000,'LHED    ','INT.',1,MAXLRG      ,JLHED )                
      CALL JALLOC(*2000,'KDEST   ','INT.',1,MAXDEL      ,JKDEST)                
      CALL JALLOC(*2000,'LDEST   ','INT.',1,MAXDEL      ,JLDEST)                
      CALL JALLOC(*2000,'IHED    ','INT.',1,MAXLRG       ,JIHED )               
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
      CALL JALLOC(*2000,'KHED    ','INT.',1,NEQAS       ,JKHED )                
      CALL JALLOC(*2000,'VETT    ','D.P.',1,MAXLRG      ,JVETT )                
      CALL JALLOC(*2000,'ASSMAT  ','D.P.',1,MAXLRG*NEQAS,JASMAT)                
      CALL JOPEN('*ASM    ',ISLASM,1)                                           
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*TAB    ',ISLTAB,1)                                           
                                                                                
C                                                                               
C                                                                               
C      JLIM=JKHED+NEQAS-1                                                       
C      KK=JLIM-JIPIAZ                                                           
      JASPSI=JASMAT                                                             
c      CALL IZERO(IV(JIPIAZ),KK)                                                
      NRASMT=NEQAS                                                              
C                                                                               
       CALL JPRMEM                                                              
       CALL JPRTAB                                                              
       print*,' ***   INIZIO ASSEMBLAGGIO FRONTALE   ***'                       
      CALL ASSFRO(IV(JIELEM),IV(JIPIAZ),IV(JKHED),IV(JLHED),                    
     1 IV(JKDEST),IV(JLDEST),IV(JIHED),DV(JASMAT),NRASMT,DV(JVETT),             
     2 DV(JELMAT),MAXDEL,DV(JASPSI),NPSI,NEQAS,IV(JIPELM))                      
C                                                                               
      CALL JCLOSE(ISLTAB)                                                       
      CALL JCLOSE(ISLASM)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
C                                                                               
C                                                                               
C************************** PARTE CURVA *******************                     
C                                                                               
      IF (IGEOM.EQ.1)THEN                                                       
C                                                                               
      CALL JCRERS                                                               
C                                                                               
C      do8765 iopl=1,maxdim                                                     
C8765  iv(iopl)=0                                                               
      CALL JALLOC(*2000,'IPELM   ','INT.',1,NELEM*3    ,JIPELM)                 
      CALL JOPEN('*NWD    ',ISLNWD,1)                                           
      CALL JREAD(ISLNWD,IV(JIPELM),NELEM*3)                                     
      CALL JCLOSE(ISLNWD)                                                       
      MAXPZ=MAXLRG                                                              
      IF(MAXLRG.LT.MAXDEL) MAXPZ=MAXDEL                                         
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXPZ       ,JIPIAZ)                
      CALL JALLOC(*2000,'IELEM   ','INT.',1,NELMAX      ,JIELEM)                
      CALL JALLOC(*2000,'LHED    ','INT.',1,MAXLRG      ,JLHED )                
      CALL JALLOC(*2000,'KDEST   ','INT.',1,MAXDEL      ,JKDEST)                
      CALL JALLOC(*2000,'LDEST   ','INT.',1,MAXDEL      ,JLDEST)                
      CALL JALLOC(*2000,'IHED    ','INT.',1,MAXLRG       ,JIHED )               
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
      CALL JALLOC(*2000,'KHED    ','INT.',1,NEQAS1       ,JKHED )               
      CALL JALLOC(*2000,'COMPL   ','D.P.',1,MAXDEL*MAXDEL*2,JCOMPL)             
      CALL JALLOC(*2000,'CVETT   ','D.P.',1,MAXLRG*2,JCVETT)                    
      CALL JALLOC(*2000,'CASSMT  ','D.P.',1,MAXLRG*NEQAS1*2,JCASMT)             
C                                                                               
      JLIM=JCASMT+MAXLRG*NEQAS1*2-1                                             
      CALL IZERO(IV(JIPIAZ),JLIM-JIPIAZ+1)                                      
C                                                                               
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*ASC    ',ISLASC,1)                                           
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*TAB    ',ISLTAB,1)                                           
      JCAPSI=JCASMT                                                             
      NRASMT=NEQAS1                                                             
      CALL CASFRO(IV(JIELEM),IV(JIPIAZ),IV(JKHED),IV(JLHED),                    
     1 IV(JKDEST),IV(JLDEST),IV(JIHED),DV(JCASMT),NRASMT,DV(JCVETT),            
     2 DV(JELMAT),MAXDEL,DV(JCAPSI),NPSI,NEQAS1,IV(JIPELM),DV(JCOMPL))          
      CALL JCLOSE(ISLTAB)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JCLOSE(ISLASC)                                                       
      CALL JCLOSE(ISLMTF)                                                       
      CALL JCLOSE(ISLMTN)                                                       
      ENDIF                                                                     
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.CWROUT                                                        
C*************************          CWROUT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CWROUT(IHED,LHED,IPIAZ,CVETT,CASSMT,NRASMT,IRIGA,NCOL,         
     1                 IPLH,NPLH,IPRIG)                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 CVETT,CASSMT                                                   
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),CASSMT(NRASMT,1),CVETT(1)                              
      DIMENSION LHED(1),IHED(1)                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NCDC,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      ICOL=0                                                                    
      DO 130 JK=1,NCOL                                                          
      IF(LHED(JK).LT.0) GOTO 130                                                
      ICOL=ICOL+1                                                               
      IHED(ICOL)=LHED(JK)                                                       
  130 CONTINUE                                                                  
      CALL SHLSRT(ICOL,IHED,R,1,1,1)                                            
      IF(IHED(1).NE.IRIGA) GOTO 160                                             
      DO 135 JK=1,ICOL                                                          
      IHED(JK)=IPOSL(IHED(JK),LHED,NCOL)                                        
  135 CONTINUE                                                                  
      NTERM=0                                                                   
      DO 150 LK=1,ICOL                                                          
      ICEQL=IHED(LK)                                                            
      NTERM=NTERM+1                                                             
      IPIAZ(NTERM)=LHED(ICEQL)                                                  
      CVETT(NTERM)=CASSMT(IPRIG,ICEQL)                                          
      CASSMT(IPRIG,ICEQL)=DCMPLX(0.D0,0.D0)                                     
  150 CONTINUE                                                                  
      IF(MAXFRO.GE.NTERM)GOTO 170                                               
      MAXFRO=NTERM                                                              
      IF(MAXFRO.GT.MAXLRG) GOTO 200                                             
      WRITE(IOUT,789)MAXFRO                                                     
  789 FORMAT('  FRONTE ATTUALE :',I8)                                           
 170  CONTINUE                                                                  
      CALL JWRITE(ISLASC,NTERM,1)                                               
      CALL JWRITE(ISLASC,IPIAZ,NTERM)                                           
      CALL JWRITE(ISLASC,CVETT,NTERM*4)                                         
       NWWRT=NTERM*5+2                                                          
       CALL JWRITE(ISLASC,NWWRT,1)                                              
      IPOS=IHED(1)                                                              
      LHED(IPOS)=-LHED(IPOS)                                                    
      NPLH=NPLH+1                                                               
      IPLH=2                                                                    
      IF(DUMP(9).OR.DUMP(5))WRITE(IOUT,904) IRIGA,(IPIAZ(L),L=1,ICOL)           
      IF(DUMP(5))            WRITE(IOUT,905)       (CVETT(L),L=1,ICOL)          
 904  FORMAT(' EQUAZ. SCRITTA N.',I6,'  VETT. PIAZZ.'/20(20I6/))                
 905  FORMAT(' EQUAZ. COEFF.'/40(3X,10D12.6/))                                  
      RETURN                                                                    
  200 WRITE(IOUT,902)MAXFRO,MAXLRG                                              
  161 CALL JHALT                                                                
      STOP                                                                      
  160 WRITE(IOUT,903) IRIGA,(LHED(L),L=1,NCOL)                                  
      GOTO 161                                                                  
  902 FORMAT(' *** AVVISO (CWROUT): IL MASSIMO FRONTE EFFETTIVO (',I8,          
     1 ') SUPERA IL MASSIMO FATTORIZZABILE (',I8,')  ***')                      
  903 FORMAT(' *** AVVISO (CWROUT): NON E'' PRESENTE IL TERMINE DIAGONAL        
     1E DELLA RIGA ',I6/'  LHED',20(12I10/))                                    
      END                                                                       

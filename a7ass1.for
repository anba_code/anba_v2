C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C------  NOTA: la cappa e' stata modificata                                     
C------  CAP7 CAP8 CAP9 costruiscono sottomatrici termiche                      
C@ELT,I ANBA*ANBA.CAPPA                                                         
C*************************          CAPPA          ********************* MOD    
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CAPPA(RIG,NRIG)                                                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION D(6,6),CPI(1),ENC(1),ENR(1),RIG(NRIG,1),BTAR(3,3),              
     1          BTBR(3,3),BTAC(3,3),BTBC(3,3),COE(2),TEMP(3,3),CDC(6)           
     1          ,DT(6,6),   CDLTZ(*)                                            
      RETURN                                                                    
      ENTRY  CAP1(D,ENR,ENC,RIG,NRIG,DEL,COE,CDC,IGEOM)                         
C                                                                               
C     COSTRUZIONE MATRICE H-CIRCONFLESSO                                        
C                                                                               
      IF (IGEOM .EQ. 0.and.abs(cdc(3)-1.d0).lt.0.0001) THEN                                                    
      ENR2=ENR(2)*ENC(1)                                                        
      ENR3=ENR(3)*ENC(1)                                                        
      RIG(1,1)=RIG(1,1)+ENR2*D(4,1)+ENR3*D(6,1)                                 
      RIG(1,2)=RIG(1,2)+ENR2*D(4,2)+ENR3*D(6,2)                                 
      RIG(1,3)=RIG(1,3)+ENR2*D(4,3)+ENR3*D(6,3)                                 
      RIG(2,1)=RIG(2,1)+ENR3*D(5,1)+ENR2*D(6,1)                                 
      RIG(2,2)=RIG(2,2)+ENR3*D(5,2)+ENR2*D(6,2)                                 
      RIG(2,3)=RIG(2,3)+ENR3*D(5,3)+ENR2*D(6,3)                                 
      RIG(3,1)=RIG(3,1)+ENR2*D(1,1)+ENR3*D(2,1)                                 
      RIG(3,2)=RIG(3,2)+ENR2*D(1,2)+ENR3*D(2,2)                                 
      RIG(3,3)=RIG(3,3)+ENR2*D(1,3)+ENR3*D(2,3)                                 
      ELSE                                                                      
      CALL BTABUP(DEL,ENR,BTAR,COE,CDC(4))                                      
      CALL BTBBUP(DEL,ENR,BTBR)                                                 
      ENC1=ENC(1)                                                               
      DO 20 I=1,3                                                               
      DO 20 K=1,3                                                               
      DO 20 L=1,3                                                               
      RIG(I,K)=RIG(I,K)+(BTAR(I,L)*D(L,K)+BTBR(I,L)*D(L+3,K))*ENC1              
  20  CONTINUE                                                                  
      ENDIF                                                                     
      RETURN                                                                    
      ENTRY  CAP2(D,ENC,CPI,RIG,NRIG)                                           
C                                                                               
C     COSTRUZIONE MATRICE R-TILDE TRASPOSTA                                     
C                                                                               
      ENC1=ENC(1)                                                               
      CPI1=CPI(1)                                                               
      CPI2=CPI(2)                                                               
      RIG(1,1)=RIG(1,1)+D(1,1)*ENC1                                             
      RIG(1,2)=RIG(1,2)+D(1,2)*ENC1                                             
      RIG(1,3)=RIG(1,3)+D(1,3)*ENC1                                             
      RIG(2,1)=RIG(2,1)+D(2,1)*ENC1                                             
      RIG(2,2)=RIG(2,2)+D(2,2)*ENC1                                             
      RIG(2,3)=RIG(2,3)+D(2,3)*ENC1                                             
      RIG(3,1)=RIG(3,1)+D(3,1)*ENC1                                             
      RIG(3,2)=RIG(3,2)+D(3,2)*ENC1                                             
      RIG(3,3)=RIG(3,3)+D(3,3)*ENC1                                             
      RIG(4,1)=RIG(4,1)+CPI2*D(3,1)*ENC1                                        
      RIG(4,2)=RIG(4,2)+CPI2*D(3,2)*ENC1                                        
      RIG(4,3)=RIG(4,3)+CPI2*D(3,3)*ENC1                                        
      RIG(5,1)=RIG(5,1)-CPI1*D(3,1)*ENC1                                        
      RIG(5,2)=RIG(5,2)-CPI1*D(3,2)*ENC1                                        
      RIG(5,3)=RIG(5,3)-CPI1*D(3,3)*ENC1                                        
      RIG(6,1)=RIG(6,1)+(CPI1*D(2,1)-CPI2*D(1,1))*ENC1                          
      RIG(6,2)=RIG(6,2)+(CPI1*D(2,2)-CPI2*D(2,1))*ENC1                          
      RIG(6,3)=RIG(6,3)+(CPI1*D(2,3)-CPI2*D(3,1))*ENC1                          
      RETURN                                                                    
      ENTRY CAP3(D,ENR,ENC,RIG,NRIG)                                            
C                                                                               
C     COSTRUZIONE MATRICE K-TILDE                                               
C                                                                               
      ENC1=ENC(1)                                                               
      ENR1=ENR(1)                                                               
      DO 10 I=1,3                                                               
      DO 10 K=1,3                                                               
      RIG(I,K)=RIG(I,K)+D(I,K)*ENR1*ENC1                                        
10    CONTINUE                                                                  
      RETURN                                                                    
      ENTRY CAP4(D,ENR,ENC,RIG,NRIG,DEL,COE,CDC,IGEOM,DT)                       
C                                                                               
C     COSTRUZIONE MATRICE K-CIRCONFLESSO                                        
C                                                                               
      IF (IGEOM .EQ. 0.and.abs(cdc(3)-1.d0).lt.0.0001) THEN                                                    
      ENR2=ENR(2)                                                               
      ENR3=ENR(3)                                                               
      ENC2=ENC(2)                                                               
      ENC3=ENC(3)                                                               
C                                                                               
      RIG(1,1)=RIG(1,1)+ENR2*(D(4,4)*ENC2+D(4,6)*ENC3)+                         
     1                  ENR3*(D(6,4)*ENC2+D(6,6)*ENC3)                          
      RIG(1,2)=RIG(1,2)+ENR2*(D(4,5)*ENC3+D(4,6)*ENC2)+                         
     1                  ENR3*(D(6,5)*ENC3+D(6,6)*ENC2)                          
      RIG(1,3)=RIG(1,3)+ENR2*(D(4,1)*ENC2+D(4,2)*ENC3)+                         
     1                  ENR3*(D(6,1)*ENC2+D(6,2)*ENC3)                          
      RIG(2,1)=RIG(2,1)+ENR3*(D(5,4)*ENC2+D(5,6)*ENC3)+                         
     1                  ENR2*(D(6,4)*ENC2+D(6,6)*ENC3)                          
      RIG(2,2)=RIG(2,2)+ENR3*(D(5,5)*ENC3+D(5,6)*ENC2)+                         
     1                  ENR2*(D(6,5)*ENC3+D(6,6)*ENC2)                          
      RIG(2,3)=RIG(2,3)+ENR3*(D(5,1)*ENC2+D(5,2)*ENC3)+                         
     1                  ENR2*(D(6,1)*ENC2+D(6,2)*ENC3)                          
      RIG(3,1)=RIG(3,1)+ENR2*(D(1,4)*ENC2+D(1,6)*ENC3)+                         
     1                  ENR3*(D(2,4)*ENC2+D(2,6)*ENC3)                          
      RIG(3,2)=RIG(3,2)+ENR2*(D(1,5)*ENC3+D(1,6)*ENC2)+                         
     1                  ENR3*(D(2,5)*ENC3+D(2,6)*ENC2)                          
      RIG(3,3)=RIG(3,3)+ENR2*(D(1,1)*ENC2+D(1,2)*ENC3)+                         
     1                  ENR3*(D(2,1)*ENC2+D(2,2)*ENC3)                          
      ELSE                                                                      
      CALL BTBBUP(DEL,ENR,BTBR)                                                 
      CALL BTABUP(DEL,ENR,BTAR,COE,CDC(4))                                      
      CALL BTBBUP(DEL,ENC,BTBC)                                                 
      CALL BTABUP(DEL,ENC,BTAC,COE,CDC(4))                                      
      CALL DABCT3 (BTAR,DT(1,1),6,BTAC)                                         
      CALL DABCT3 (BTBR,DT(4,1),6,BTAC)                                         
      CALL DABCT3 (BTAR,DT(1,4),6,BTBC)                                         
      CALL DABCT3 (BTBR,DT(4,4),6,BTBC)                                         
      DO 7 I=1,3                                                                
      DO 7 K=1,3                                                                
      RIG(I,K)=RIG(I,K)+DT(I,K)+DT(I+3,K)+DT(I,K+3)+DT(I+3,K+3)                 
   7  CONTINUE                                                                  
      ENDIF                                                                     
      RETURN                                                                    
      ENTRY CAP5(D,ENR,CPI,RIG,NRIG,DEL,COE,CDC,IGEOM)                          
C                                                                               
C     COSTRUZIONE MATRICE R-CIRCONFLESSO                                        
C                                                                               
      IF (IGEOM .EQ. 0.and.abs(cdc(3)-1.d0).lt.0.0001) THEN                                                    
      ENR2=ENR(2)                                                               
      ENR3=ENR(3)                                                               
      CPI1=CPI(1)                                                               
      CPI2=CPI(2)                                                               
C                                                                               
      RIG(1,1)=RIG(1,1)+ENR2*D(4,1)+ENR3*D(6,1)                                 
      RIG(1,2)=RIG(1,2)+ENR2*D(4,2)+ENR3*D(6,2)                                 
      RIG(1,3)=RIG(1,3)+ENR2*D(4,3)+ENR3*D(6,3)                                 
      RIG(1,4)=RIG(1,4)+(ENR2*D(4,3)+ENR3*D(6,3))*CPI2                          
      RIG(1,5)=RIG(1,5)-(ENR2*D(4,3)+ENR3*D(6,3))*CPI1                          
      RIG(1,6)=RIG(1,6)+ENR2*(-D(4,1)*CPI2+D(4,2)*CPI1)+                        
     1                  ENR3*(-D(6,1)*CPI2+D(6,2)*CPI1)                         
      RIG(2,1)=RIG(2,1)+ENR3*D(5,1)+ENR2*D(6,1)                                 
      RIG(2,2)=RIG(2,2)+ENR3*D(5,2)+ENR2*D(6,2)                                 
      RIG(2,3)=RIG(2,3)+ENR3*D(5,3)+ENR2*D(6,3)                                 
      RIG(2,4)=RIG(2,4)+(ENR3*D(5,3)+ENR2*D(6,3))*CPI2                          
      RIG(2,5)=RIG(2,5)-(ENR3*D(5,3)+ENR2*D(6,3))*CPI1                          
      RIG(2,6)=RIG(2,6)+ENR3*(-D(5,1)*CPI2+D(5,2)*CPI1)+                        
     1                  ENR2*(-D(6,1)*CPI2+D(6,2)*CPI1)                         
      RIG(3,1)=RIG(3,1)+ENR2*D(1,1)+ENR3*D(2,1)                                 
      RIG(3,2)=RIG(3,2)+ENR2*D(1,2)+ENR3*D(2,2)                                 
      RIG(3,3)=RIG(3,3)+ENR2*D(1,3)+ENR3*D(2,3)                                 
      RIG(3,4)=RIG(3,4)+(ENR2*D(1,3)+ENR3*D(2,3))*CPI2                          
      RIG(3,5)=RIG(3,5)-(ENR2*D(1,3)+ENR3*D(2,3))*CPI1                          
      RIG(3,6)=RIG(3,6)+ENR2*(-D(1,1)*CPI2+D(1,2)*CPI1)+                        
     1                  ENR3*(-D(2,1)*CPI2+D(2,2)*CPI1)                         
      ELSE                                                                      
      CALL BTBBUP(DEL,ENR,BTBR)                                                 
      CALL BTABUP(DEL,ENR,BTAR,COE,CDC(4))                                      
      DO 30 I=1,3                                                               
      DO 30 K=1,3                                                               
      TEMP(I,K)=0.D0                                                            
      DO 30 L=1,3                                                               
      TEMP(I,K)=TEMP(I,K)+BTAR(I,L)*D(L,K)+BTBR(I,L)*                           
     1          D(L+3,K)                                                        
  30  CONTINUE                                                                  
      DO 40 I=1,3                                                               
      DO 40 K=1,3                                                               
      RIG(I,K)=RIG(I,K)+TEMP(I,K)                                               
  40  CONTINUE                                                                  
      RIG(1,4)=RIG(1,4)+TEMP(1,3)*CPI(2)                                        
      RIG(1,5)=RIG(1,5)-TEMP(1,3)*CPI(1)                                        
      RIG(1,6)=RIG(1,6)-TEMP(1,1)*CPI(2)+TEMP(1,2)*CPI(1)                       
      RIG(2,4)=RIG(2,4)+TEMP(2,3)*CPI(2)                                        
      RIG(2,5)=RIG(2,5)-TEMP(2,3)*CPI(1)                                        
      RIG(2,6)=RIG(2,6)-TEMP(2,1)*CPI(2)+TEMP(2,2)*CPI(1)                       
      RIG(3,4)=RIG(3,4)+TEMP(3,3)*CPI(2)                                        
      RIG(3,5)=RIG(3,5)-TEMP(3,3)*CPI(1)                                        
      RIG(3,6)=RIG(3,6)-TEMP(3,1)*CPI(2)+TEMP(3,2)*CPI(1)                       
      ENDIF                                                                     
      RETURN                                                                    
      ENTRY CAP6(D,CPI,RIG,NRIG)                                                
C                                                                               
C     COSTRUZIONE MATRICE K-SEGNATA                                             
C                                                                               
      CPI1=CPI(1)                                                               
      CPI2=CPI(2)                                                               
C                                                                               
      RIG(1,1)=RIG(1,1)+D(1,1)                                                  
      RIG(1,2)=RIG(1,2)+D(1,2)                                                  
      RIG(1,3)=RIG(1,3)+D(1,3)                                                  
      RIG(1,4)=RIG(1,4)+D(1,3)*CPI2                                             
      RIG(1,5)=RIG(1,5)-D(1,3)*CPI1                                             
      RIG(1,6)=RIG(1,6)-D(1,1)*CPI2+D(1,2)*CPI1                                 
      RIG(2,1)=RIG(2,1)+D(2,1)                                                  
      RIG(2,2)=RIG(2,2)+D(2,2)                                                  
      RIG(2,3)=RIG(2,3)+D(2,3)                                                  
      RIG(2,4)=RIG(2,4)+D(2,3)*CPI2                                             
      RIG(2,5)=RIG(2,5)-D(2,3)*CPI1                                             
      RIG(2,6)=RIG(2,6)-D(2,1)*CPI2+D(2,2)*CPI1                                 
      RIG(3,1)=RIG(3,1)+D(3,1)                                                  
      RIG(3,2)=RIG(3,2)+D(3,2)                                                  
      RIG(3,3)=RIG(3,3)+D(3,3)                                                  
      RIG(3,4)=RIG(3,4)+D(3,3)*CPI2                                             
      RIG(3,5)=RIG(3,5)-D(3,3)*CPI1                                             
      RIG(3,6)=RIG(3,6)-D(3,1)*CPI2+D(3,2)*CPI1                                 
      RIG(4,1)=RIG(4,1)+D(3,1)*CPI2                                             
      RIG(4,2)=RIG(4,2)+D(3,2)*CPI2                                             
      RIG(4,3)=RIG(4,3)+D(3,3)*CPI2                                             
      RIG(4,4)=RIG(4,4)+D(3,3)*CPI2*CPI2                                        
      RIG(4,5)=RIG(4,5)-D(3,3)*CPI1*CPI2                                        
      RIG(4,6)=RIG(4,6)+(-D(3,1)*CPI2+D(3,2)*CPI1)*CPI2                         
      RIG(5,1)=RIG(5,1)-D(3,1)*CPI1                                             
      RIG(5,2)=RIG(5,2)-D(3,2)*CPI1                                             
      RIG(5,3)=RIG(5,3)-D(3,3)*CPI1                                             
      RIG(5,4)=RIG(5,4)-D(3,3)*CPI1*CPI2                                        
      RIG(5,5)=RIG(5,5)+D(3,3)*CPI1*CPI1                                        
      RIG(5,6)=RIG(5,6)-(-D(3,1)*CPI2+D(3,2)*CPI1)*CPI1                         
      RIG(6,1)=RIG(6,1)-D(1,1)*CPI2+D(2,1)*CPI1                                 
      RIG(6,2)=RIG(6,2)-D(1,2)*CPI2+D(2,2)*CPI1                                 
      RIG(6,3)=RIG(6,3)-D(1,3)*CPI2+D(2,3)*CPI1                                 
      RIG(6,4)=RIG(6,4)-D(1,3)*CPI2*CPI2+D(2,3)*CPI1*CPI2                       
      RIG(6,5)=RIG(6,5)+D(1,3)*CPI2*CPI1-D(2,3)*CPI1*CPI1                       
      RIG(6,6)=RIG(6,6)-CPI2*(-D(1,1)*CPI2+D(1,2)*CPI1)+                        
     1                  CPI1*(-D(2,1)*CPI2+D(2,2)*CPI1)                         
      RETURN                                                                    
C                                                                               
C  ----*- INIZIO costruzione sottomatrici trattazione termica ----              
C                                                                               
      ENTRY CAP7(D,ENR,ENC,RIG,NRIG,CDLTZ)                                      
C                                                                               
C     COSTRUZIONE DELLA MATRICE [Bt][D]{CDLTZ}{NTt}                             
C                                                                               
      ENR2=ENR(2)*ENC(1)                                                        
      ENR3=ENR(3)*ENC(1)                                                        
      SOMMA1=0.                                                                 
      SOMMA2=0.                                                                 
      DO 100,I=1,6                                                              
      SOMMA1=SOMMA1+D(4,I)*CDLTZ(I)                                             
      SOMMA2=SOMMA2+D(6,I)*CDLTZ(I)                                             
100   CONTINUE                                                                  
      RIG(1,1)=RIG(1,1)+ENR2*SOMMA1+ENR3*SOMMA2                                 
      SOMMA1=0.                                                                 
      DO 110,I=1,6                                                              
110   SOMMA1=SOMMA1+D(5,I)*CDLTZ(I)                                             
      RIG(2,1)=RIG(2,1)+ENR3*SOMMA1+ENR2*SOMMA2                                 
      SOMMA1=0.                                                                 
      SOMMA2=0.                                                                 
      DO 120,I=1,6                                                              
      SOMMA1=SOMMA1+D(1,I)*CDLTZ(I)                                             
      SOMMA2=SOMMA2+D(2,I)*CDLTZ(I)                                             
120   CONTINUE                                                                  
      RIG(3,1)=RIG(3,1)+ENR2*SOMMA1+ENR3*SOMMA2                                 
      RETURN                                                                    
C                                                                               
      ENTRY CAP8(D,ENR,ENC,RIG,NRIG,CDLTZ)                                      
C                                                                               
C     COSTRUZIONE DELLA MATRICE [Nt][St][D]{CDLTZ}{NTt}                         
C                                                                               
      SOMMA1=0.                                                                 
      DO 130,I=1,6                                                              
130   SOMMA1=SOMMA1+D(1,I)*CDLTZ(I)                                             
      RIG(1,1)=RIG(1,1)+ENR(1)*ENC(1)*SOMMA1                                    
      SOMMA1=0.                                                                 
      DO 135,I=1,6                                                              
135   SOMMA1=SOMMA1+D(2,I)*CDLTZ(I)                                             
      RIG(2,1)=RIG(2,1)+ENR(1)*ENC(1)*SOMMA1                                    
      SOMMA1=0.                                                                 
      DO 140,I=1,6                                                              
140   SOMMA1=SOMMA1+D(3,I)*CDLTZ(I)                                             
      RIG(3,1)=RIG(3,1)+ENR(1)*ENC(1)*SOMMA1                                    
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY CAP9(D,ENC,RIG,NRIG,CDLTZ,CPI)                                      
C                                                                               
C     COSTRUZIONE DELLA MATRICE [Zt][St][D]{CDLTZ}{NTt}                         
C                                                                               
      SOMMA1=0.                                                                 
      DO 150,I=1,6                                                              
150   SOMMA1=SOMMA1+D(1,I)*CDLTZ(I)                                             
      RIG(1,1)=RIG(1,1)+ENC(1)*SOMMA1                                           
      SOMMA2=0.                                                                 
      DO 160,I=1,6                                                              
160   SOMMA2=SOMMA2+D(2,I)*CDLTZ(I)                                             
      RIG(2,1)=RIG(2,1)+ENC(1)*SOMMA2                                           
      SOMMA2=0.                                                                 
      DO 170,I=1,6                                                              
170   SOMMA2=SOMMA2+D(3,I)*CDLTZ(I)                                             
      RIG(3,1)=RIG(3,1)+ENC(1)*SOMMA2                                           
      RIG(4,1)=RIG(4,1)+ENC(1)*SOMMA2*CPI(2)                                    
      RIG(5,1)=RIG(5,1)-ENC(1)*SOMMA2*CPI(1)                                    
      SOMMA2=0.                                                                 
      DO 180,I=1,6                                                              
180   SOMMA2=SOMMA2+D(2,I)*CDLTZ(I)                                             
      RIG(6,1)=RIG(6,1)+ENC(1)*(CPI(1)*SOMMA2-CPI(2)*SOMMA1)                    
      RETURN                                                                    
      END                                                                       
C                                                                               
C-----FINE costruzione sottomatrici trattazione termica -----                   
C                                                                               
C@ELT,I ANBA*ANBA.DABCT3                                                        
C*************************          DABCT3          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DABCT3(A,B,NRDB,C)                                             
C                                                                               
C     CALCOLA IL PRODOTTO DELLE MATRICI (3*3) "A" * "B" * "C-TRASPOSTA"         
C                                                                               
      REAL*8 A,B,C,D                                                            
      DIMENSION A(3,3),B(NRDB,1),C(3,3),D(3,3)                                  
      DO 10 I=1,3                                                               
      DO 10 K=1,3                                                               
      D(I,K)=0.                                                                 
      DO 10 L=1,3                                                               
10    D(I,K)=D(I,K)+A(I,L)*B(L,K)                                               
      DO 20 I=1,3                                                               
      DO 20 K=1,3                                                               
      B(I,K)=0.                                                                 
      DO 20 L=1,3                                                               
20    B(I,K)=B(I,K)+D(I,L)*C(K,L)                                               
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.DAT2D                                                         
C*************************          DAT2D          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DAT2D(NPUNTI,NPA,NPB,PUNTI,PESI)                               
C                                                                               
C     CALCOLA LE COORD. ADIMENSIONALI DEI PUNTI DI INTEGRAZIONE                 
C     ED I RELATIVI "PESI" PER ELEMENTI BIDIMENSIONALI                          
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      logical dump,incore                                                       
      DIMENSION PUNTI(2,1),PESI(1),X(21),W(21)                                  
      COMMON /JFTNIO/ INPT,IOUT,ISYM,dump(22),incore,ILOCA                      
      COMMON /PNTINT/ IFLPI                                                     
      DATA X/                                                                   
     *  0.D0,                                                                   
     * -.5773502691896D0,.5773502691896D0,                                      
     * -.7745966692415D0,0.D0,.7745966692415D0,                                 
     * -.8611363115941D0,-.3399810435849D0,.3399810435849D0,                    
     *  .8611363115941D0,                                                       
     * -.90617985D0,-.53846931D0,0.D0,.53846931D0,.90617985D0,                  
     * 0.D0,                                                                    
     * -1.D0,1.D0,                                                              
     * -1.D0,0.D0,1.D0/                                                         
      DATA  W/                                                                  
     * 2.D0,                                                                    
     * 1.D0,1.D0,                                                               
     *.5555555555556D0,.8888888888889D0,.5555555555556D0,                       
     *.3478548451375D0,.6521451548625D0,.6521451548625D0,                       
     *.3478548451375D0,                                                         
     *.23692689D0,.47862867D0,.568888888889D0,.47862867D0,.23692689D0,          
     * 2.D0,                                                                    
     * 1.D0,1.D0,                                                               
     * .3333333333D0,1.3333333333D0,.3333333333D0/                              
      DATA MAXPUN/5/                                                            
C                                                                               
CCC      NP1=NPA/2+1                                                            
CCC      NP2=NPB/2+1                                                            
      NP1=NPA                                                                   
      NP2=NPB                                                                   
      IF(MAX0(NP1,NP2).GT.MAXPUN) GOTO100                                       
 222  CONTINUE                                                                  
      IP0=0                                                                     
      IF(IFLPI.NE.0) THEN                                                       
      IP0=IP0+15                                                                
      IF(NPA.GT.3.OR.NPB.GT.3) IP0=0                                            
      END IF                                                                    
      K1=((NP1-1)*NP1)/2+IP0                                                    
      K2=((NP2-1)*NP2)/2+IP0                                                    
      NPUNTI=0                                                                  
      DO 40 K=1,NP2                                                             
      TX=X(K2+K)                                                                
      TP=W(K2+K)                                                                
      DO 30 I=1,NP1                                                             
      NPUNTI=NPUNTI+1                                                           
      PUNTI(1,NPUNTI)=X(K1+I)                                                   
      PUNTI(2,NPUNTI)=TX                                                        
      PESI(NPUNTI)=TP*W(K1+I)                                                   
 30   CONTINUE                                                                  
 40   CONTINUE                                                                  
      RETURN                                                                    
 100  WRITE(IOUT,200) NPUNTI,MAXPUN                                             
 200  FORMAT(/' *** AVVISO (DAT2D): L''ORDINE DI INTEGRAZIONE RICHIE',          
     1  'STO (',I2,' ) SUPERA IL MASSIMO CONSENTITO (',I2,' ) ***')             
      NP1=MAXPUN                                                                
      NP2=MAXPUN                                                                
      GOTO 222                                                                  
      END                                                                       
C@ELT,I ANBA*ANBA.FORLAM                                                        
C*************************          FORLAM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE FORLAM(P,Q,NCOPP,ENNE)                                         
C                                                                               
C     QUESTA SUBROUTINE COSTRUISCE LE FUNZIONI DI FORMA                         
C     PER L'ELEMENTO DI LAMINA                                                  
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION ENNE(3,1)                                                       
      P2=P*P                                                                    
      IGOTO=NCOPP-1                                                             
      GOTO (101,102,103),IGOTO                                                  
 101  ENNE(1,3)= .25*(1.D0-P)*(1.D0-Q)                                          
      ENNE(1,4)= .25*(1.D0+P)*(1.D0-Q)                                          
      ENNE(1,1)= .25*(1.D0+P)*(1.D0+Q)                                          
      ENNE(1,2)= .25*(1.D0-P)*(1.D0+Q)                                          
C                                                                               
      ENNE(2,3)=-.25*(1.D0-Q)                                                   
      ENNE(2,4)= .25*(1.D0-Q)                                                   
      ENNE(2,1)= .25*(1.D0+Q)                                                   
      ENNE(2,2)=-.25*(1.D0+Q)                                                   
C                                                                               
      ENNE(3,3)=-.25*(1.D0-P)                                                   
      ENNE(3,4)=-.25*(1.D0+P)                                                   
      ENNE(3,1)= .25*(1.D0+P)                                                   
      ENNE(3,2)= .25*(1.D0-P)                                                   
C                                                                               
      RETURN                                                                    
 102  ENNE(1,3)= .25*(P2-P)*(1.D0-Q)                                            
      ENNE(1,4)= .25*(P2+P)*(1.D0-Q)                                            
      ENNE(1,1)= .25*(P2+P)*(1.D0+Q)                                            
      ENNE(1,2)= .25*(P2-P)*(1.D0+Q)                                            
      ENNE(1,6)= .5 *(1.D0-P2)*(1.D0-Q)                                         
      ENNE(1,5)= .5 *(1.D0-P2)*(1.D0+Q)                                         
C                                                                               
      ENNE(2,3)= .25*(2.D0*P-1.D0)*(1.D0-Q)                                     
      ENNE(2,4)= .25*(2.D0*P+1.D0)*(1.D0-Q)                                     
      ENNE(2,1)= .25*(2.D0*P+1.D0)*(1.D0+Q)                                     
      ENNE(2,2)= .25*(2.D0*P-1.D0)*(1.D0+Q)                                     
      ENNE(2,6)= - P * (1.D0-Q)                                                 
      ENNE(2,5)= - P * (1.D0+Q)                                                 
C                                                                               
      ENNE(3,3)=-.25 * (P2-P)                                                   
      ENNE(3,4)=-.25 * (P2+P)                                                   
      ENNE(3,1)= .25 * (P2+P)                                                   
      ENNE(3,2)= .25 * (P2-P)                                                   
      ENNE(3,6)=-.5  * (1.D0-P2)                                                
      ENNE(3,5)= .5  * (1.D0-P2)                                                
C                                                                               
      RETURN                                                                    
 103  ENNE(1,3)=(1.D0-Q)*(-1.+P*(1.+P*(9.-9.*P)))/32.                           
      ENNE(1,7)=(1.D0-Q)*(9.+P*(-27.+P*(-9.+27.*P)))/32.                        
      ENNE(1,8)=(1.D0-Q)*(9.+P*(27.+P*(-9.-27.*P)))/32.                         
      ENNE(1,4)=(1.D0-Q)*(-1.+P*(-1.+P*(9.+9.*P)))/32.                          
      ENNE(1,2)=(1.D0+Q)*(-1.+P*(1.+P*(9.-9.*P)))/32.                           
      ENNE(1,6)=(1.D0+Q)*(9.+P*(-27.+P*(-9.+27.*P)))/32.                        
      ENNE(1,5)=(1.D0+Q)*(9.+P*(27.+P*(-9.-27.*P)))/32.                         
      ENNE(1,1)=(1.D0+Q)*(-1.+P*(-1.+P*(9.+9.*P)))/32.                          
C                                                                               
      ENNE(2,3)=(1.D0-Q)*(1.+P*(18.-27.*P))/32.                                 
      ENNE(2,7)=(1.D0-Q)*(-27.+P*(-18.+81.*P))/32.                              
      ENNE(2,8)=(1.D0-Q)*(27.+P*(-18.-81.*P))/32.                               
      ENNE(2,4)=(1.D0-Q)*(-1.+P*(18.+27.*P))/32.                                
      ENNE(2,2)=(1.D0+Q)*(1.+P*(18.-27.*P))/32.                                 
      ENNE(2,6)=(1.D0+Q)*(-27.+P*(-18.+81.*P))/32.                              
      ENNE(2,5)=(1.D0+Q)*(27.+P*(-18.-81.*P))/32.                               
      ENNE(2,1)=(1.D0+Q)*(-1.+P*(18.+27.*P))/32.                                
C                                                                               
      ENNE(3,3)=-(-1.+P*(1.+P*(9.-9.*P)))/32.                                   
      ENNE(3,7)=-(9.+P*(-27.+P*(-9.+27.*P)))/32.                                
      ENNE(3,8)=-(9.+P*(27.+P*(-9.-27.*P)))/32.                                 
      ENNE(3,4)=-(-1.+P*(-1.+P*(9.+9.*P)))/32.                                  
      ENNE(3,2)= (-1.+P*(1.+P*(9.-9.*P)))/32.                                   
      ENNE(3,6)= (9.+P*(-27.+P*(-9.+27.*P)))/32.                                
      ENNE(3,5)= (9.+P*(27.+P*(-9.-27.*P)))/32.                                 
      ENNE(3,1)= (-1.+P*(-1.+P*(9.+9.*P)))/32.                                  
C                                                                               
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.FORM2D                                                        
C*************************          FORM2D          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE FORM2D(P,Q,NIN,NOU,ENNE,ICON)                                  
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION ENNE(3,1),ICON(1)                                               
      logical dump,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,dump(22),incore,ILOCA                      
C                                                                               
C     QUESTA SUBROUTINE COSTRUISCE LE FUNZIONI DI FORMA E LE LORO               
C     DERIVATE PER L'ELEMENTO PIANO                                             
C                                                                               
      IF(NIN.LT.12) THEN                                                        
        ENNE(1,3)= .25 * (1.D0+P) * (1.D0+Q)                                    
        ENNE(1,4)= .25 * (1.D0-P) * (1.D0+Q)                                    
        ENNE(1,1)= .25 * (1.D0-P) * (1.D0-Q)                                    
        ENNE(1,2)= .25 * (1.D0+P) * (1.D0-Q)                                    
C                                                                               
        ENNE(2,3)= .25 * (1.D0+Q)                                               
        ENNE(2,4)=-.25 * (1.D0+Q)                                               
        ENNE(2,1)=-.25 * (1.D0-Q)                                               
        ENNE(2,2)= .25 * (1.D0-Q)                                               
C                                                                               
        ENNE(3,3)= .25 * (1.D0+P)                                               
        ENNE(3,4)= .25 * (1.D0-P)                                               
        ENNE(3,1)=-.25 * (1.D0-P)                                               
        ENNE(3,2)=-.25 * (1.D0+P)                                               
C                                                                               
        NOU=NIN                                                                 
        IF(NIN.EQ.4)RETURN                                                      
        P2=P*P                                                                  
        Q2=Q*Q                                                                  
        P3=P2*P                                                                 
        Q3=Q2*Q                                                                 
        IF(NIN.NE.8) GOTO 520                                                   
C                                                                               
        ENNE(1,7)= .5 * (1.D0-P2) * (1.D0+Q )                                   
        ENNE(1,8)= .5 * (1.D0-P ) * (1.D0-Q2)                                   
        ENNE(1,5)= .5 * (1.D0-P2) * (1.D0-Q )                                   
        ENNE(1,6)= .5 * (1.D0+P ) * (1.D0-Q2)                                   
C                                                                               
        ENNE(2,7)=- P * (1.D0+Q )                                               
        ENNE(2,8)=-.5 * (1.D0-Q2)                                               
        ENNE(2,5)=- P * (1.D0-Q )                                               
        ENNE(2,6)= .5 * (1.D0-Q2)                                               
C                                                                               
        ENNE(3,7)= .5 * (1.D0-P2)                                               
        ENNE(3,8)=- Q * (1.D0-P )                                               
        ENNE(3,5)=-.5 * (1.D0-P2)                                               
        ENNE(3,6)=- Q * (1.D0+P )                                               
C                                                                               
        DO 20 I=5,8                                                             
        IF(ICON(I).NE.0) GOTO 20                                                
          ENNE(1,I)=0.D0                                                        
          ENNE(2,I)=0.D0                                                        
          ENNE(3,I)=0.D0                                                        
 20     CONTINUE                                                                
C                                                                               
        DO 30 I=1,3                                                             
          ENNE(I,1)=ENNE(I,1)-.5*(ENNE(I,8)+ENNE(I,5))                          
          ENNE(I,2)=ENNE(I,2)-.5*(ENNE(I,5)+ENNE(I,6))                          
          ENNE(I,3)=ENNE(I,3)-.5*(ENNE(I,6)+ENNE(I,7))                          
          ENNE(I,4)=ENNE(I,4)-.5*(ENNE(I,7)+ENNE(I,8))                          
 30     CONTINUE                                                                
        GOTO 60                                                                 
      ELSE                                                                      
C                                                                               
        ENNE(1,3) = 1./32.*(1.+P)*(1.+Q)*(-10.+9.*(P*P+Q*Q))                    
        ENNE(1,4) = 1./32.*(1.-P)*(1.+Q)*(-10.+9.*(P*P+Q*Q))                    
        ENNE(1,1) = 1./32.*(1.-P)*(1.-Q)*(-10.+9.*(P*P+Q*Q))                    
        ENNE(1,2) = 1./32.*(1.+P)*(1.-Q)*(-10.+9.*(P*P+Q*Q))                    
C                                                                               
        ENNE(1,9) = 9./32.*(1.+Q)*(1.-P*P)*(1.+9.*1./3.*P)                      
        ENNE(1,10) = 9./32.*(1.+Q)*(1.-P*P)*(1.-9.*1./3.*P)                      
C                                                                               
        ENNE(1,11) = 9./32.*(1.-P)*(1.-Q*Q)*(1.+9.*1./3.*Q)                      
        ENNE(1,12) = 9./32.*(1.-P)*(1.-Q*Q)*(1.-9.*1./3.*Q)                      
C                                                                               
        ENNE(1,5) = 9./32.*(1.-Q)*(1.-P*P)*(1.-9.*1./3.*P)                      
        ENNE(1,6)= 9./32.*(1.-Q)*(1.-P*P)*(1.+9.*1./3.*P)                      
C                                                                               
        ENNE(1,7)= 9./32.*(1.+P)*(1.-Q*Q)*(1.-9.*1./3.*Q)                      
        ENNE(1,8)= 9./32.*(1.+P)*(1.-Q*Q)*(1.+9.*1./3.*Q)                      
C                                                                               
C                                                                               
        ENNE(2,3) =  1./32.*(Q+1.)*(27.*P*P+18.*P+9.*Q*Q-10.)                   
        ENNE(2,4) = -1./32.*(Q+1.)*(27.*P*P-18.*P+9.*Q*Q-10.)                   
        ENNE(2,1) =  1./32.*(Q-1.)*(27.*P*P-18.*P+9.*Q*Q-10.)                   
        ENNE(2,2) = -1./32.*(Q-1.)*(27.*P*P+18.*P+9.*Q*Q-10.)                   
        
        ENNE(2,9) = -9./32.*(Q+1.)*(9.*P*P+2.*P-3.)                             
        ENNE(2,10) =  9./32.*(Q+1.)*(9.*P*P-2.*P-3.)                             
        ENNE(2,11) =  9./32.*(3*Q+1.)*(Q*Q-1.)                                   
        ENNE(2,12) = -9./32.*(3*Q-1.)*(Q*Q-1.)                                   
        ENNE(2,5) = -9./32.*(Q-1.)*(9.*P*P-2.*P-3.)                             
        ENNE(2,6) = 9./32.*(Q-1.)*(9.*P*P+2.*P-3.)                             
        ENNE(2,7) = 9./32.*(3*Q-1.)*(Q*Q-1.)                                   
        ENNE(2,8) = -9./32.*(3*Q+1.)*(Q*Q-1.)                                  
C                                                                               
        ENNE(3,3) =  1./32.*(P+1.)*(9.*P*P+27.*Q*Q+18.*Q-10.)                   
        ENNE(3,4) = -1./32.*(P-1.)*(9.*P*P+27.*Q*Q+18.*Q-10.)                   
        ENNE(3,1) =  1./32.*(P-1.)*(9.*P*P+27.*Q*Q-18.*Q-10.)                   
        ENNE(3,2) = -1./32.*(P+1.)*(9.*P*P+27.*Q*Q-18.*Q-10.)                   
        ENNE(3,9) = -9./32.*(3*P+1.)*(P*P-1.)                                   
        ENNE(3,10) =  9./32.*(3*P-1.)*(P*P-1.)                                   
        ENNE(3,11) =  9./32.*(9.*Q*Q+2.*Q-3.)*(P-1.)                             
        ENNE(3,12) = -9./32.*(9.*Q*Q-2.*Q-3.)*(P-1.)                             
        ENNE(3,5) = -9./32.*(3*P-1.)*(P*P-1.)                                   
        ENNE(3,6) = 9./32.*(3*P+1.)*(P*P-1.)                                   
        ENNE(3,7) = 9./32.*(9.*Q*Q-2.*Q-3.)*(P+1.)                             
        ENNE(3,8) = -9./32.*(9.*Q*Q+2.*Q-3.)*(P+1.)                            
C                                                                               
      ENDIF                                                                     
 60   LINF=5                                                                    
      IPOS=LINF                                                                 
      LSUP=NIN                                                                  
      NOU=NIN                                                                   
      DO 40 I=LINF,LSUP                                                         
      IF(ICON(I).EQ.0)GOTO 45                                                   
CCC      ICON(IPOS)=ICON(I)                                                     
      ENNE(1,IPOS)=ENNE(1,I)                                                    
      ENNE(2,IPOS)=ENNE(2,I)                                                    
      ENNE(3,IPOS)=ENNE(3,I)                                                    
      IPOS=IPOS+1                                                               
      GOTO 40                                                                   
 45   CONTINUE                                                                  
      NOU=NOU-1                                                                 
 40   CONTINUE                                                                  
      RETURN                                                                    
 520  WRITE(IOUT,620)                                                           
 620  FORMAT(///' *** AVVISO (FORM2D): NUMERO DI NODI NON ',                    
     1  'CONGRUENTE :',I4,' ANZICHE'' 8 ***'/)                                  
      CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C@ELT,I ANBA*ANBA.INCASB                                                        
C*************************          INCASB          ******************** MOD    
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INCASB                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLMRG,ISLTEM,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
C                                                                               
C                                                                               
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV         ,JNA   )
C      CALL JPRMMS       
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1     ,JIPIAZ)               
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
      CALL JALLOC(*2000,'RK      ','D.P.',1,NSTO         ,JRK   )               
C                                                                               
      CALL DZERO(DV(JRK),NSTO)                                                  
C                                                                               
      CALL ASSEL (IV(JNA),IV(JIPIAZ),DV(JRK),DV(JELMAT),                        
     *         MAXDEL,NELES,ISLVPZ,NPSI,NEQV)                                   
      CALL JOPEN('*ASM    ',ISLASM,1)                                           
      CALL JWRITE(ISLASM,DV(JRK),NSTO*2)                                        
      CALL JCLOSE(ISLASM)                                                       
      IF(IGEOM.EQ.1)THEN                                                        
      CALL JFREE(*2000,'RK      ')                                              
      CALL JFREE(*2000,'ELMAT   ')                                              
      CALL JALLOC(*2000,'RK      ','D.P.',1,NEQV*NEQV*2  ,JRK   )               
      CALL JALLOC(*2000,'CELMA   ','D.P.',1,MAXDEL*MAXDEL*2,JCELMA)             
      CALL DZERO(DV(JRK),NEQV*NEQV*2)                                           
      CALL DZERO(DV(JCELMA),MAXDEL*MAXDEL*2)                                    
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JPOSAB(ISLVPZ,1)                                                     
C                                                                               
      CALL CASSEL (IV(JNA),IV(JIPIAZ),DV(JRK),DV(JCELMA),                       
     *         MAXDEL,NELES,ISLVPZ,ISLMTF,ISLMTN,NPSI,NEQV)                     
      CALL JCLOSE(ISLMTF)                                                       
      CALL JCLOSE(ISLMTN)                                                       
      CALL JOPEN('*ASC    ',ISLASC,1)                                           
      CALL JWRITE(ISLASC,DV(JRK),NEQV*NEQV*4)                                   
      CALL JCLOSE(ISLASC)                                                       
      ENDIF                                                                     
      CALL JCLOSE(ISLVPZ)                                                       
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.ASSEL                                                         
C **************************      ASSEL     *************************** MOD     
      SUBROUTINE ASSEL(NA,IPIAZ,RK,ELMAT,MAXDEL,NELES,ISLVPZ,NPSI,NEQV)         
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      DIMENSION NA(1),IPIAZ(1),RK(1),ELMAT(MAXDEL,1)                            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ELGEN/  ITIP,NTERM,NT1,NN1                                        
C                                                                               
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      DO 300 K=1,NELES                                                          
      CALL JREAD(ISLVPZ,ITIP,3)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      NTERM=NTERM-1                                                             
      DO 310 I=1,NTERM                                                          
      CALL JREAD(ISLVPZ,ELMAT(1,I),NT1*2)                                       
 310  CONTINUE                                                                  
      CALL FEMADS(RK,NA,ELMAT,IPIAZ,NTERM,MAXDEL)                               
 300  CONTINUE                                                                  
C                                                                               
      DO 400 IL=1,NPSI                                                          
      IF(IPGCON(IL).EQ.0) GOTO 400                                              
      IN=NEQV-NPSI+IL                                                           
      IN=NA(IN)                                                                 
      RK(IN)=RK(IN)+CONST                                                       
 400  CONTINUE                                                                  
          IF(DUMP(5))THEN                                                       
          WRITE(IOUT,*)' MATRICE ASSEMBLATA'                                    
          WRITE(IOUT,500)1,1,1                                                  
          WRITE(IOUT,200)RK(1)                                                  
          DO 323 IO=2,NEQV                                                      
          WRITE(IOUT,500)IO,IO-NA(IO)+NA(IO-1)+1,IO                             
          WRITE(IOUT,200)(RK(IK),IK=NA(IO-1)+1,NA(IO))                          
 323      CONTINUE                                                              
          END IF                                                                
200       FORMAT(12(1X,E10.4))                                                  
500       FORMAT(' COLONNA N.RO ',I6,';  RIGHE DA',I6,'  A',I6)                 
C                                                                               
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.CASSEL                                                        
C **************************     CASSEL     *************************** MOD7    
      SUBROUTINE CASSEL(NA,IPIAZ,RK,CELMA,MAXDEL,NELES,ISLVPZ,ISLMTF,           
     *                  ISLMTN,NPSI,NEQV)                                       
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 RK,CELMA                                                       
      LOGICAL DUMP,INCORE                                                       
      DIMENSION NA(1),IPIAZ(1),RK(NEQV,1),CELMA(MAXDEL,1)                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ELGEN/  ITIP,NTERM,NT1,NN1                                        
C                                                                               
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      WRITE(IOUT,'(/,A,10X,I10)') ' NELES IN CASSEL =   ',NELES                 
      DO 300 K=1,NELES                                                          
      CALL JREAD(ISLVPZ,ITIP,3)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      IF(ITIP.NE.6.AND.ITIP.NE.7) CALL JPOSRL(ISLMTF,NT1*(NT1-NPSI)*2)          
      DO 310 I=1,NTERM-1                                                        
      DO 310 L=1,NT1                                                            
       CALL JREAD(ISLVPZ,RRK,2)                                                 
CC     RH=CONST                                                                 
       RH=0.D0                                                                  
       RM=0.D0                                                                  
       IF(ITIP.NE.6.AND.ITIP.NE.7)THEN                                          
       CALL JREAD(ISLMTN,RH,2)                                                  
       IF (I.LE.NT1-NPSI.AND.L.LE.NT1-NPSI) CALL JREAD(ISLMTF,RM,2)             
       ENDIF                                                                    
      RK(IPIAZ(L),IPIAZ(I))=RK(IPIAZ(L),IPIAZ(I))+DCMPLX(RRK+C**2*RM,           
     1                      -C*RH)                                              
 310  CONTINUE                                                                  
 300  CONTINUE                                                                  
C                                                                               
      DO 400 IL=1,NPSI                                                          
      IF(IPGCON(IL).EQ.0) GOTO 400                                              
      IN=NEQV-NPSI+IL                                                           
      RK(IN,IN)=RK(IN,IN)+CONST                                                 
 400  CONTINUE                                                                  
          IF(DUMP(5))THEN                                                       
          WRITE(IOUT,*)' MATRICE ASSEMBLATA'                                    
          DO 323 IO=1,NEQV                                                      
          WRITE(IOUT,500)IO  
          WRITE(IOUT,200)(RK(IO,IK),IK=1,NEQV)                                  
 323      CONTINUE                                                              
          END IF                                                                
200       FORMAT(12(1X,E10.4))                                                  
500       FORMAT(' RIGA N.RO ',I6)
C                                                                               
      RETURN                                                                    
C                                                                               
      END                                                                       
                                                                                
                                                                                
C@ELT,I ANBA*ANBA.JAC2D                                                         
C*************************          JAC2D          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE JAC2D(ENNE,NNOD,XN,CPI,DA,BETA)                                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION ENNE(3,1),XN(2,NNOD),RJ(2,2),RJM1(2,2),F(2),CPI(2)              
C                                                                               
C     QUESTA SUBROUTINE CALCOLA LO JACOBIANO (RJ) NEL PUNTO DI                  
C     INTEGRAZIONE, IL SUO INVERSO (RJM1), L'ELEMENTO D'AREA (DA),              
C     L'INCLINAZIONE DELLA LINEA COORDINATA CSI (BETA)                          
C                                                                               
         DATA ICOUNT/1/                                                         
C        PRINT*,'  CONNESSIONE N.',ICOUNT                                       
         ICOUNT=ICOUNT+1                                                        
      DO 10 I=1,2                                                               
      DO 10 K=1,2                                                               
      RJ(I,K)=0.D0                                                              
      DO 10 N=1,NNOD                                                            
      RJ(I,K)=RJ(I,K)+ENNE(K+1,N)*XN(I,N)                                       
 10   CONTINUE                                                                  
C                                                                               
      DO 30 I=1,2                                                               
      CPI(I)=0.D0                                                               
      DO 30 N=1,NNOD                                                            
      CPI(I)=CPI(I)+ENNE(1,N)*XN(I,N)                                           
C       PRINT*,I,N,ENNE(1,N),XN(I,N)                                            
 30   CONTINUE                                                                  
C                                                                               
      DA=RJ(1,1)*RJ(2,2)-RJ(1,2)*RJ(2,1)                                        
C                                                                               
C         INSERIRE TEST SUL VALORE DI DA                                        
C                                                                               
C                                                                               
      RJM1(1,1)= RJ(2,2)/DA                                                     
      RJM1(1,2)=-RJ(1,2)/DA                                                     
      RJM1(2,1)=-RJ(2,1)/DA                                                     
      RJM1(2,2)= RJ(1,1)/DA                                                     
      BETA=DATAN2(RJ(2,1),RJ(1,1))                                              
C                                                                               
      DO 40 N=1,NNOD                                                            
      DO 20 I=1,2                                                               
      F(I)=0.D0                                                                 
      DO 20 K=1,2                                                               
      F(I)=F(I)+RJM1(K,I)*ENNE(K+1,N)                                           
 20   CONTINUE                                                                  
      ENNE(2,N)=F(1)                                                            
      ENNE(3,N)=F(2)                                                            
 40   CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.LMAT                                                          
C*************************          LMAT          **********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE LMAT(XY,NCON,P,SINLM,COSLM)                                    
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION XY(1),SHAPE(4),DERIV(4)                                         
C                                                                               
C                                                                               
C     NCON =  2,3,4 COPPIE DI NODI DELLA LAMINA                                 
C     P    =  COORDINATA ADIMENSIONALE DEL NODO                                 
C     XY   =  COORDINATE NODI                                                   
C     SHAPE=  VETTORE FUNZIONI DI FORMA                                         
C                                                                               
      P2=P*P                                                                    
      IGOTO=NCON-1                                                              
      GOTO(100,200,300),IGOTO                                                   
100   SHAPE(1)=.5*(1-P)                                                         
      SHAPE(2)=.5*(1+P)                                                         
      DERIV(1)=-.5                                                              
      DERIV(2)=.5                                                               
      GOTO 1000                                                                 
200   SHAPE(1)=.5*(P2-P)                                                        
      SHAPE(2)=1.-P2                                                            
      SHAPE(3)=.5*(P2+P)                                                        
      DERIV(1)=P-.5                                                             
      DERIV(2)=-2.*P                                                            
      DERIV(3)=P+.5                                                             
      GOTO 1000                                                                 
300   SHAPE(1)=(-1.+P*(1.+P*(9.-9.*P)))/16.                                     
      SHAPE(2)=(9.+P*(-27.+P*(-9.+27.*P)))/16.                                  
      SHAPE(3)=(9.+P*(27.+P*(-9.-27.*P)))/16.                                   
      SHAPE(4)=(-1.+P*(-1.+P*(9.+9.*P)))/16.                                    
      DERIV(1)=(1.+P*(18.-27.*P))/16.                                           
      DERIV(2)=(-27.+P*(-18.+81.*P))/16.                                        
      DERIV(3)=(27.+P*(-18.-81.*P))/16.                                         
      DERIV(4)=(-1.+P*(18.+27.*P))/16.                                          
C                                                                               
1000  CONTINUE                                                                  
C                                                                               
C                                                                               
C     CALCOLO DX DY E DS NEL PUNTO DI INTEGRAZIONE                              
C                                                                               
C                                                                               
      DX=0.                                                                     
      DY=0.                                                                     
      DO 2 K=1,NCON                                                             
      DX=DX+DERIV(K)*XY(2*K-1)                                                  
2     DY=DY+DERIV(K)*XY(2*K)                                                    
      DS2=DX*DX+DY*DY                                                           
      DS=DSQRT(DS2)                                                             
      COSLM=DX/DS                                                               
      SINLM=DY/DS                                                               
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MATCOR                                                        
C*************************          MATCOR          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATCOR(ELMAT,NRELMT,VMASS)                                     
C                                                                               
C                                                                               
C     CALCOLA LE MATRICI DEI CORRENTI                                           
C                                                                               
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION ELMAT(NRELMT,1),VMASS(1)                                        
C                                                                               
C                                                                               
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                        
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3          
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /CORR/ E,RO,AREA                                                   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,1)                                                 
      CALL JREAD(ISLELM,X,2)                                                    
      CALL JREAD(ISLELM,Y,2)                                                    
      CALL JREAD(ISLELM,E,6)                                                    
      IF(NCON.NE.1)GOTO 1100                                                    
      C=AREA*RO                                                                 
      AREA=AREA*E                                                               
      NCSPSI=NCS+NPSI                                                           
      DO 10 I=1,NCSPSI                                                          
      DO 10 J=1,NCSPSI                                                          
      ELMAT(I,J)=0.D0                                                           
10    CONTINUE                                                                  
          IF(IFLMR.EQ.0)GOTO 40                                                 
      ELMAT(NCS+3,NCS)= AREA                                                    
      ELMAT(NCS+4,NCS)= AREA*Y                                                  
      ELMAT(NCS+5,NCS)=-AREA*X                                                  
      DO 20 I=1,NCS                                                             
      CALL JWRITE(ISLMTF,ELMAT(1,I),(NCS+NPSI)*2)                               
20    CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITE('MATRICE FLESS',ELMAT,                            
     *             NRELMT,NCSPSI,NCS,1)                                         
      ELMAT(NCS,NCS+3)= AREA                                                    
      ELMAT(NCS,NCS+4)= AREA*Y                                                  
      ELMAT(NCS,NCS+5)=-AREA*X                                                  
      ELMAT(NCS+3,NCS)=-AREA                                                    
      ELMAT(NCS+4,NCS)=-AREA*Y                                                  
      ELMAT(NCS+5,NCS)=AREA*X                                                   
      DO 50 I=1,NCSPSI                                                          
      CALL JWRITE(ISLMTN,ELMAT(1,I),(NCSPSI)*2)                                 
50    CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITE('MATRICE TNOTO',ELMAT,                            
     *             NRELMT,NCSPSI,NCSPSI,1)                                      
      ELMAT(NCS+3,NCS)= 0.D0                                                    
      ELMAT(NCS+4,NCS)= 0.D0                                                    
      ELMAT(NCS+5,NCS)= 0.D0                                                    
      ELMAT(NCS,NCS+3)= 0.D0                                                    
      ELMAT(NCS,NCS+4)= 0.D0                                                    
      ELMAT(NCS,NCS+5)= 0.D0                                                    
      ELMAT(NCS,NCS)= AREA                                                      
CC        WRITE(IOUT,103)                                                       
      DO 30 I=1,NCS                                                             
      CALL JWRITE(ISLMTF,ELMAT(1,I),NCS*2)                                      
30    CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITE('MATRICE MASSA',ELMAT,                            
     *             NRELMT,NCS,NCS,1)                                            
      ELMAT(NCS,NCS)=0.D0                                                       
      ELMAT(6,6)= AREA                                                          
      ELMAT(6,7)= AREA*Y                                                        
      ELMAT(6,8)=-AREA*X                                                        
      ELMAT(7,7)= AREA*Y*Y                                                      
      ELMAT(7,8)=-AREA*X*Y                                                      
      ELMAT(8,8)= AREA*X*X                                                      
      ELMAT(7,6)=ELMAT(6,7)                                                     
      ELMAT(8,6)=ELMAT(6,8)                                                     
      ELMAT(8,7)=ELMAT(7,8)                                                     
C                                                                               
  40  CONTINUE                                                                  
      CALL ASMASS(VMASS,C,X,Y)                                                  
      RETURN                                                                    
1100  WRITE(IOUT,1200)ITIP,IDEL,NCON,ICON,X,Y,E,RO,AREA                         
1200  FORMAT(/' *** AVVISO (MATCOR): DATI ELEMENTO NON RICONOSCIUTO',           
     1 ' COME CORRENTE ***'/20X,'TIPO,IDENTIF.,N.RO CONNESS.,CONNESS.',         
     2 4I8/20X,'COORDINATE X Y,MOD.ELASTICITA'',DENSITA'',AREA',5E12.5)         
1300  FORMAT(5F20.10)                                                           
      CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
                                                                                
C------- NOTA :la matelp e' stata modificata --------------------               
C----    aggiunta la costruzione delle matrici termiche ---------               
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++              
C     25 luglio: modifica al common ELPI1 e a LREC per tener conto              
C     della trasformazione di CDLTZ                                             
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++               
C@ELT,I ANBA*ANBA.MATELP                                                        
C*************************          MATELP          ******************** MOD    
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATELP(ELMAT,NRELMT,VMASS,IPIAZ)                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
c++++++++++++++++++++++ 31 agosto   +++++++++++++++++++++++++++++++++           
c     Ho ripristinato il common ELPI1 originale [ togliendo CDLTZ(6) ]          
      COMMON /ELPI1/  LREC,NPINT,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),             
     1                T2(3,3),D(6,6),W,DEL,COE(2)                               
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++           
      COMMON /ELPI2/  INT1,INT2,TETA,DEN,alfort,IFLTET                          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),IAUTCN,             
     1                IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                    
      DIMENSION PUNTI(2,25),PESI(25),XY(2,12),ICON(12),ELMAT(NRELMT,1)          
     1         ,DG(6,6),VMASS(1),DT(6,6),IPIAZ(1) ,cdltz(6),cdlt(6)             
C                                                                               
      LREC=196                                                                  
      CALL CAPPA(ELMAT,NRELMT)                                                  
C                                                                               
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,NCON)                                              
      CALL JREAD(ISLELM,XY,NCON*4)                                              
      CALL JREAD(ISLELM,nwr,1)                                                  
      CALL JREAD(ISLELM,INT1,nwr)                                               
      DO 5 K=1,6                                                                
      CALL JREAD(ISLELM,DG(1,K),12)                                             
 5    CONTINUE                                                                  
      CALL JREAD(ISLELM,CDLT ,12)                                               
      CALL DAT2D(NPINT,INT1,INT2,PUNTI,PESI)                                    
      CALL JWRITE(ISLELS,LREC,2)                                                
      JUMP=0                                                                    
                                                                                
      DO 10 NPT=1,NPINT                                                         
      CALL FORM2D(PUNTI(1,NPT),PUNTI(2,NPT),NCON,NCONR,ENNE,ICON)               
      CALL JAC2D(ENNE,NCONR,XY,CPI,DA,BETA)
      
cc      cpi(1)=cpi(1)*cdc(3)
                                           
      DA=PESI(NPT)*DA                                                           
      DP=DA*DEN                                                                 
      DEL=CDC(3)+CDC(4)*CPI(2)-CDC(5)*CPI(1)                                    
      COE(1)=CDC(1)-CDC(6)*CPI(2)                                               
      COE(2)=CDC(2)+CDC(6)*CPI(1)                                               
      DP=DA*DEN*DEL                                                             
      W=DA/DEL                                                                  
c      CALL ASMASS(VMASS,DP,CPI(1),CPI(2))                                       
      CALL ASMASS2(VMASS,DP,CPI(1)*cdc(3),CPI(2),cpi(1)*cdc(1))                                       
      ALFA=TETA                                                                 
      IF(IFLTET.EQ.1) ALFA=ALFA+BETA                                            
C                                                                               
      SINA=DSIN(ALFA)                                                           
      COSA=DCOS(ALFA)                                                           
      SINA2=SINA*SINA                                                           
      COSA2=COSA*COSA                                                           
      SIN2A=DSIN(2.D0*ALFA)                                                     
      COS2A=DCOS(2.D0*ALFA)                                                     
C                                                                               
      T1(1,1)= COSA                                                             
      T1(1,2)=-SINA                                                             
      T1(1,3)= 0.D0                                                             
      T1(2,1)= SINA                                                             
      T1(2,2)= COSA                                                             
      T1(2,3)= 0.D0                                                             
      T1(3,1)= 0.D0                                                             
      T1(3,2)= 0.D0                                                             
      T1(3,3)= 1.D0                                                             
C                                                                               
      T2(1,1)= COSA2                                                            
      T2(1,2)= SINA2                                                            
      T2(1,3)=-SIN2A                                                            
      T2(2,1)= SINA2                                                            
      T2(2,2)= COSA2                                                            
      T2(2,3)= SIN2A                                                            
      T2(3,1)= SIN2A/2.D0                                                       
      T2(3,2)=-SIN2A/2.D0                                                       
      T2(3,3)= COS2A                                                            
      DO 14 I=1,6                                                               
      DO 14 K=1,6                                                               
      D(I,K)=DG(I,K)*W                                                          
 14   CONTINUE                                                                  
C                                                                               
      CALL DABCT3(T1,D(1,1),6,T1)                                               
      CALL DABCT3(T1,D(1,4),6,T2)                                               
      CALL DABCT3(T2,D(4,1),6,T1)                                               
      CALL DABCT3(T2,D(4,4),6,T2)                                               
      CALL JWRITE(ISLELS,ENNE,LREC)                                             
c     calcolo coeff. di dilatazioni termiche in coordinate assolute             
      CALL DPROMM(T1,3,3,3,CDLT(1),6,CDLTZ(1),6,1,0,0)                          
      CALL DPROMM(T2,3,3,3,CDLT(4),6,CDLTZ(4),6,1,0,0)                          
c     if(npt.eq.2) then                                                         
c     write(iout,*)alfa*180./3.1415                                             
c     write(iout,8) (cdlt(iop),iop=1,6)                                         
c     write(iout,8) (cdltz(iop),iop=1,6)                                        
c8    format(6e12.5)                                                            
c     endif                                                                     
      JUMP=JUMP+LREC                                                            
 10   CONTINUE                                                                  
      IF(IFLMR.EQ.0)RETURN                                                      
      IELM1=NCONR*NCS                                                           
      IELMAT=NCONR*NCS+NPSI                                                     
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      DO 20 NPT=1,NPINT                                                         
      CALL JREAD(ISLELS,ENNE,LREC)                                              
c                                                                               
c-----stampa della D()                                                          
c750   format(//'stampa matrice elastica'/)                                     
c760   format(5x,'D(',i2,',',i2,')=',e12.5)                                     
c      write(iout,750)                                                          
c      do 755,i1=1,6                                                            
c      do 755,i2=1,6                                                            
c      write(iout,760) i1,i2,d(i1,i2)                                           
c755   continue                                                                 
c-----fine stampa D()                                                           
c                                                                               
      DO 30 K=1,NCONR                                                           
      JK=(K-1)*NCS+1                                                            
      DO 40 I=1,NCONR                                                           
      JI=(I-1)*NCS+1                                                            
      CALL CAP1(D,ENNE(1,I),ENNE(1,K),ELMAT(JI,JK),NRELMT,DEL,COE,              
     1          CDC,IGEOM)                                                      
 40   CONTINUE                                                                  
      JI=IELM1+1                                                                
      CALL CAP2(D,ENNE(1,K),CPI,ELMAT(JI,JK),NRELMT)                            
 30   CONTINUE                                                                  
 20   CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICI H-CIRCONFLESSO ED R-CIRCONFLESSO TRASP.          
C                                                                               
      DO 250 I=1,IELM1                                                          
      CALL JWRITE(ISLMTF,ELMAT(1,I),IELMAT*2)                                   
 250  CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE FLESS',ELMAT,                            
     *             NRELMT,IELMAT,IELM1,1,IPIAZ)                                 
      DO 300 I1=1,IELM1                                                         
      DO 300 K1=I1,IELM1                                                        
      H=ELMAT(I1,K1)-ELMAT(K1,I1)                                               
      ELMAT(I1,K1)=-H                                                           
      ELMAT(K1,I1)= H                                                           
300   CONTINUE                                                                  
      DO 302 I=1,NPSI                                                           
      I1=IELM1+I                                                                
      DO 302 I2=1,IELM1                                                         
      ELMAT(I2,I1)=ELMAT(I1,I2)                                                 
      ELMAT(I1,I2)=-ELMAT(I1,I2)                                                
302   CONTINUE                                                                  
CC      WRITE(IOUT,1408)                                                        
      DO 350 I=1,IELMAT                                                         
CC      WRITE(IOUT,1406)(ELMAT(I,L),L=1,IELMAT)                                 
      CALL JWRITE(ISLMTN,ELMAT(1,I),IELMAT*2)                                   
350   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE TNOTO',ELMAT,                            
     *             NRELMT,IELMAT,IELMAT,1,IPIAZ)                                
C                                                                               
C     FINE COSTRUZIONE MATRICE TERMINE NOTO                                     
C                                                                               
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      DO 55 K=1,IELM1                                                           
      DO 55 I=K,IELM1                                                           
      ELMAT(I,K)=0.D0                                                           
 55   CONTINUE                                                                  
      DO 60 NPT=1,NPINT                                                         
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 70 K=1,NCONR                                                           
      JK=(K-1)*NCS+1                                                            
      DO 70 I=K,NCONR                                                           
      JI=(I-1)*NCS+1                                                            
      CALL CAP3(D,ENNE(1,I),ENNE(1,K),ELMAT(JI,JK),NRELMT)                      
 70   CONTINUE                                                                  
 60   CONTINUE                                                                  
      DO 80 K=1,IELM1                                                           
      DO 80 I=K,IELM1                                                           
      ELMAT(K,I)=ELMAT(I,K)                                                     
 80   CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICE K-TILDE                                          
C                                                                               
      DO 450 I=1,IELM1                                                          
      CALL JWRITE(ISLMTF,ELMAT(1,I),IELM1*2)                                    
CC      WRITE(IOUT,1406)(ELMAT(I,L),L=1,IELM1)                                  
450   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE MASSA',ELMAT,                            
     *             NRELMT,IELM1,IELM1,1,IPIAZ)                                  
C                                                                               
C-------------------------------------------------- inizio ---                  
C     ************************************************************              
C     QUESTA PARTE DEVE ESSERE SVOLTA SOLO SE  IL PROBLEMA TERMICO              
C     E' EFFETTIVAMENTE RICHIESTO.                                              
C     *************************************************************             
      IF (JEXIST('*TMP    ').EQ.1) THEN                                         
C     COSTRUZIONE MATRICI TERMINI NOTI TERMICI                                  
C                                                                               
C     MATRICE [Nt][D]{CDLTZ}{NTt}                                               
C                                                                               
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      DO 500 NPT=1,NPINT                                                        
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 510 I=1,NCONR                                                          
      DO 510 K=1,NCONR                                                          
      JK=(K-1)*NCS+1                                                            
      CALL CAP8(D,ENNE(1,K),ENNE(1,I),ELMAT(JK,I),NRELMT,CDLTZ)                 
510   CONTINUE                                                                  
500   CONTINUE                                                                  
      DO 530 I=1,NCONR                                                          
      CALL JWRITE(ISMTER,ELMAT(1,I),IELM1*2)                                    
c---     stampa risultati matrici                                               
c      write(iout,700)                                                          
c700   format(//'stampa matrice Nt D alfa Nt'//)                                
c710   format(5x,'elemento(',i2,',',i2,')=',e12.5)                              
c      do 77,j=1,ielm1                                                          
c      write(iout,710) j,i, elmat(j,i)                                          
c77    continue                                                                 
c-----                                                                          
530   CONTINUE                                                                  
C                                                                               
C     MATRICE [Bt][St][D]{CDLTZ}{NTt}                                           
C                                                                               
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      JI=IELM1+1                                                                
      DO 540 NPT=1,NPINT                                                        
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 550 I=1,NCONR                                                          
      DO 555 K=1,NCONR                                                          
      JK=(K-1)*NCS+1                                                            
      CALL CAP7(D,ENNE(1,K),ENNE(1,I),ELMAT(JK,I),NRELMT,CDLTZ)                 
555   CONTINUE                                                                  
C                                                                               
C     MATRICE [Zt][St][D]{CDLTZ}{NTt}                                           
C                                                                               
      CALL CAP9(D,ENNE(1,I),ELMAT(JI,I),NRELMT,CDLTZ,CPI)                       
550   CONTINUE                                                                  
540   CONTINUE                                                                  
      DO 560 I=1,NCONR                                                          
      CALL JWRITE(ISMTER,ELMAT(1,I),IELMAT*2)                                   
560   CONTINUE                                                                  
c                                                                               
c---- stampa delle matrici sopra calcolate ---                                  
c761   format(//5x,'stampa della matrice Bt S D alfa N',/,'NCONR=',i2)          
c765   format(5x,'riga(',i2,')=',8(2X,e12.5))                                   
c777   format(//5x,'stampa della matrice Zt S D alfa N')                        
c      write(iout,761) nconr                                                    
c      i2=0                                                                     
c      do 762,i1=1,ielm1                                                        
c      i2=i2+1                                                                  
c      write(iout,765) i1,(elmat(i1,i),i=1,nconr)                               
c      if(i2.eq.3) then                                                         
c      write(iout,*) ' '                                                        
c      i2=0                                                                     
c      endif                                                                    
c762   continue                                                                 
c      write(iout,777)                                                          
c      do 763,i1=ielm1+1,ielmat                                                 
c763   write(iout,765) (i1-ielm1),(elmat(i1,i),i=1,nconr)                       
c---  fine stampa                                                               
c                                                                               
C                                                                               
                                                                                
C                                                                               
C      CALL DZERO(ELMAT,NRELMT*NRELMT)                                          
C      CALL JPOSRL(ISLELS,-JUMP)                                                
C      DO 570 NPT=1,NPINT                                                       
C      CALL JREAD(ISLELS,ENNE,LREC)                                             
C      DO 580 I=1,NCONR                                                         
C      CALL CAP9(D,ENNE(1,I),ELMAT(1,I),NRELMT,CDLTZ,CPI)                       
C580   CONTINUE                                                                 
C570   CONTINUE                                                                 
C      DO 590 I=1,NCONR                                                         
C      CALL JWRITE(ISMTER,ELMAT(1,I),6*2)                                       
C590   CONTINUE                                                                 
                                                                                
C     FINE TERMINI NOTI TERMICI                                                 
      END IF                                                                    
C--------------------------------------------------------------------           
                                                                                
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      DO 90 NPT=1,NPINT                                                         
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 100 I=1,NCONR                                                          
      JI=(I-1)*NCS+1                                                            
      DO 110 K=I,NCONR                                                          
      JK=(K-1)*NCS+1                                                            
      DO 22 L=1,6                                                               
      DO 22 M=1,6                                                               
      DT(L,M)=D(L,M)                                                            
  22  CONTINUE                                                                  
      CALL CAP4(D,ENNE(1,I),ENNE(1,K),ELMAT(JI,JK),NRELMT                       
     1          ,DEL,COE,CDC,IGEOM,DT)                                          
 110  CONTINUE                                                                  
      JK=IELM1+1                                                                
      CALL CAP5(D,ENNE(1,I),CPI,ELMAT(JI,JK),NRELMT                             
     1          ,DEL,COE,CDC,IGEOM)                                             
 100  CONTINUE                                                                  
      JI=IELM1+1                                                                
      JK=IELM1+1                                                                
      CALL CAP6(D,CPI,ELMAT(JI,JK),NRELMT)                                      
 90   CONTINUE                                                                  
      DO 130 I=1,IELMAT                                                         
      if(elmat(i,i).lt.0) then                                                  
      write(iout,889) ITIP,IDEL,NCON                                            
889   format(' *** AVVISO (MATELP): TERMINE DIAGONALE NEGATIVO',                
     * ' PER L`ELEMENTO TIPO',i3,' NUMERO',i7,' CON ',i2,' NODI ***')           
      endif                                                                     
      DO 130 K=I,IELMAT                                                         
      ELMAT(K,I)=ELMAT(I,K)                                                     
 130  CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICE DI RIGIDEZZA                                     
C                                                                               
      RETURN                                                                    
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.MATSTR                                                        
C*************************          MATSTR          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATSTR(ELMAT1,ELMAT2,ELMAT3,NRELMT,VMASS,IPIAZ)                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3         
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELSTR1/ LREC,NPINT,ENNE(3,12),CPI(2),DA,BETA,TT1(3,3),            
     *                TT2(3,3),D(6,6),W,DEL,COE(2)                              
      COMMON /ELSTR2/ INT1,INT2,DEN,THICK,ANGLAM                                
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),IAUTCN,             
     *                IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                    
      DIMENSION PUNTI(2,25),PESI(25),ICON(12),XY(2,12),DG(6,6),                 
     3          ELMAT1(NRELMT,1),ELMAT2(NRELMT,1),ELMAT3(NRELMT,1),             
     1          VETT(2,12),P(2,25),Q(25),TEC(6,6),TMPEC(6,6),DT(6,6),           
     2          IPIAZ(1)                                                        
      DATA (ICON(N),N=1,12)/12*1/                                               
      DATA RAD/57.29578/                                                        
      LREC=196                                                                  
C                                                                               
C     LETTURA DEI DATI GENERALI DELL'ELEMENTO STRATIFICATO                      
C                                                                               
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,NCON)                                              
      CALL JREAD(ISLELM,VETT,NCON*4)                                            
      CALL JREAD(ISLELM,NSTRAT,1)                                               
C                                                                               
C      WRITE(IOUT,*)                                                            
C      WRITE(IOUT,*)'MATSTR  ITIP=',ITIP,IDEL,NCON,IFLMR                        
C      WRITE(IOUT,*)                                                            
C      WRITE(IOUT,*)'MATSTR  NSTRAT=',NSTRAT                                    
C      WRITE(IOUT,*)                                                            
C     AZZERAMENTO DELLE MATRICI ELMAT1,ELMAT2,ELMAT3                            
C                                                                               
      CALL DZERO(ELMAT1,NRELMT*NRELMT)                                          
      CALL DZERO(ELMAT2,NRELMT*NRELMT)                                          
      CALL DZERO(ELMAT3,NRELMT*NRELMT)                                          
      CALL JWRITE(ISLELS,LREC,1)                                                
C                                                                               
      CALL JREAD(ISLELM,INT1,2)                                                 
C      WRITE(IOUT,*)'MATSTR  INT1=',INT1,' INT2=',INT2                          
C      WRITE(IOUT,*)                                                            
      CALL DAT2D(NPINT,INT1,INT2,P,Q)                                           
C      WRITE(IOUT,*)'MATSTR  NPINT=',NPINT                                      
C      WRITE(IOUT,*)                                                            
      CALL JWRITE(ISLELS,NPINT,1)                                               
      YSEGN=1.                                                                  
      CALL JPOSRL(ISLELM,-2)                                                    
      DO 1000 IST=1,NSTRAT                                                      
      CALL JREAD(ISLELM,INT1,8)                                                 
C      WRITE(IOUT,*)'MATSTR  THICK=',THICK                                      
C      WRITE(IOUT,*)                                                            
C      WRITE(IOUT,*)'MATSTR  ANGLAM= ',ANGLAM                                   
C      WRITE(IOUT,*)                                                            
      DO 333 N=1,NPINT                                                          
      PUNTI(1,N)=P(1,N)                                                         
      PUNTI(2,N)=THICK*(P(2,N)-1)+YSEGN                                         
C      WRITE(IOUT,*)'MATSTR  YPINT=',PUNTI(2,N)                                 
C      WRITE(IOUT,*)                                                            
      PESI(N)=Q(N)*THICK                                                        
 333  CONTINUE                                                                  
      YSEGN=YSEGN-THICK*2                                                       
C                                                                               
      DO 5 K=1,6                                                                
      CALL JREAD(ISLELM,DG(1,K),12)                                             
  5   CONTINUE                                                                  
C       WRITE(IOUT,*)'DG LETTA IN ISLELM'                                       
C       WRITE(IOUT,1111)((DG(II,KK),KK=1,6),II=1,6)                             
 1111 FORMAT(/6(1X,6D12.5/))                                                    
  873 FORMAT(/3(8D9.3/))                                                        
      DO 999 NPT=1,NPINT                                                        
C                                                                               
C         DETERMINAZIONE DELLE FUNZIONI DI FORMA                                
C                                                                               
      CALL FORM2D(PUNTI(1,NPT),PUNTI(2,NPT),NCON,NCONR,ENNE,ICON)               
C      WRITE(IOUT,*)'FUNZIONI DI FORMA DOPO FORM2D IN MATSTR'                   
C      WRITE(IOUT,873)((ENNE(IOL,IOP),IOP=1,8),IOL=1,3)                         
                                                                                
CCC      NCON=NCONR                                                             
CCC      NNOD=NCONR                                                             
C                                                                               
C     COSTRUZIONE DELLO JACOBIANO                                               
C                                                                               
      CALL JAC2D(ENNE,NCONR,VETT,CPI,DA,BETA)                                   
C      WRITE(IOUT,*)'FUNZIONI DI FORMA DOPO JAC2D'                              
C      WRITE(IOUT,873)((ENNE(IOL,IOP),IOP=1,8),IOL=1,3)                         
C      WRITE(IOUT,*)                                                            
C      WRITE(IOUT,*)' CPI=    ',CPI                                             
C      WRITE(IOUT,*)                                                            
C      WRITE(IOUT,*)' DA=     ',DA                                              
C      WRITE(IOUT,*)                                                            
      DA=PESI(NPT)*DA                                                           
      DP=DA*DEN                                                                 
      DEL=CDC(3)+CDC(4)*CPI(2)-CDC(5)*CPI(1)                                    
      COE(1)=CDC(1)-CDC(6)*CPI(2)                                               
      COE(2)=CDC(2)+CDC(6)*CPI(1)                                               
      DP=DA*DEN*DEL                                                             
      W=DA/DEL                                                                  
C      WRITE(IOUT,*)' W=      ',W                                               
C      WRITE(IOUT,*)                                                            
C     ******************************************************************        
C     *****      INIZIO MODIFICA ************                                   
C     *****************************************************************         
      ALFA=BETA                                                                 
C      WRITE(IOUT,*)'ALFA=',ALFA                                                
C      WRITE(IOUT,*)                                                            
C                                                                               
      SINA=DSIN(ALFA)                                                           
      COSA=DCOS(ALFA)                                                           
      SINA2=SINA*SINA                                                           
      COSA2=COSA*COSA                                                           
      SIN2A=DSIN(2.D0*ALFA)                                                     
      COS2A=DCOS(2.D0*ALFA)                                                     
C                                                                               
      IF(ABS(BETA).LT.1.D-07)THEN                                               
      SINA=0.0D0                                                                
      COSA=1.0D0                                                                
      SINA2=0.0D0                                                               
      COSA2=1.0D0                                                               
      SIN2A=0.0D0                                                               
      COS2A=1.0D0                                                               
      END IF                                                                    
      CALL DZERO(TEC,36)                                                        
      TEC(1,1)= COSA                                                            
      TEC(1,2)=-SINA                                                            
      TEC(1,3)= 0.D0                                                            
      TEC(2,1)= SINA                                                            
      TEC(2,2)= COSA                                                            
      TEC(2,3)= 0.D0                                                            
      TEC(3,1)= 0.D0                                                            
      TEC(3,2)= 0.D0                                                            
      TEC(3,3)= 1.D0                                                            
      TEC(4,4)= COSA2                                                           
      TEC(4,5)= SINA2                                                           
      TEC(4,6)=-SIN2A                                                           
      TEC(5,4)= SINA2                                                           
      TEC(5,5)= COSA2                                                           
      TEC(5,6)= SIN2A                                                           
      TEC(6,4)= SIN2A/2.D0                                                      
      TEC(6,5)=-SIN2A/2.D0                                                      
      TEC(6,6)= COS2A                                                           
C                                                                               
C                                                                               
      DO 14 I=1,6                                                               
      DO 14 K=1,6                                                               
      D(I,K)=DG(I,K)*W                                                          
 14   CONTINUE                                                                  
C      WRITE(IOUT,*)'TEC'                                                       
C      WRITE(IOUT,1111)((TEC(KK,JJ),JJ=1,6),KK=1,6)                             
C      WRITE(IOUT,*)'D PESATA DA DG CON W'                                      
C      WRITE(IOUT,1111)((D(KK,JJ),JJ=1,6),KK=1,6)                               
      CALL DPROMM(TEC,6,6,6,D,6,TMPEC,6,6,0,0)                                  
      CALL DPROMM(TMPEC,6,6,6,TEC,6,D,6,6,0,1)                                  
C      WRITE(IOUT,*)'D RUOTATA CON BETA'                                        
C      WRITE(IOUT,1111)((D(KK,JJ),JJ=1,6),KK=1,6)                               
C                                                                               
C     *********************************************************                 
C     ***************   FINE MODIFICA  ************************                 
C     *********************************************************                 
      CALL ASMASS(VMASS,DP,CPI(1),CPI(2))                                       
C                                                                               
C     MODIFICA DELLA MATRICE ELASTICA PER MEZZO DEI PESI                        
C                                                                               
C                                                                               
C     SCRITTURA SU FILE DELLE FUNZIONI DI FORMA                                 
C                                                                               
C      WRITE(IOUT,*)'FUNZIONI DI FORMA PRIMA DI JWRITE SU ISLELS'               
C      WRITE(IOUT,873)((ENNE(IOL,IOP),IOP=1,8),IOL=1,3)                         
      CALL JWRITE(ISLELS,ENNE,LREC)                                             
C      WRITE(IOUT,*)'FUNZIONI DI FORMA DOPO JWRITE SU ISLELS'                   
CC      WRITE(IOUT,873)((ENNE(IOL,IOP),IOP=1,8),IOL=1,3)                        
      JUMP=JUMP+LREC                                                            
      IF(IFLMR.EQ.0)RETURN                                                      
      IELM1=NCONR*NCS                                                           
      IELMAT=NCONR*NCS+NPSI                                                     
      DO 801 K=1,NCONR                                                          
      JK=(K-1)*NCS+1                                                            
      DO 802 I=1,NCONR                                                          
      JI=(I-1)*NCS+1                                                            
C     ***   MATRICE H-CIRCONFLESSO   ***                                        
      CALL CAP1(D,ENNE(1,I),ENNE(1,K),ELMAT2(JI,JK),NRELMT,                     
     1              DEL,COE,CDC,IGEOM)                                          
802   CONTINUE                                                                  
      JI=IELM1+1                                                                
C     ***   MATRICE R-TILDE TRASPOSTA   ***                                     
      CALL CAP2(D,ENNE(1,K),CPI,ELMAT2(JI,JK),NRELMT)                           
801   CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICI H-CIRCONFLESSO ED R-TILDE TRASPOSTA              
C                                                                               
      DO 700 K=1,NCONR                                                          
      JK=(K-1)*NCS+1                                                            
      DO 700 I=K,NCONR                                                          
      JI=(I-1)*NCS+1                                                            
C     ***   MATRICE K-TILDE   ***                                               
      CALL CAP3(D,ENNE(1,I),ENNE(1,K),ELMAT3(JI,JK),NRELMT)                     
 700  CONTINUE                                                                  
      DO 720 K=1,IELM1                                                          
      DO 720 I=K,IELM1                                                          
      ELMAT3(K,I)=ELMAT3(I,K)                                                   
 720  CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICE K-TILDE                                          
C                                                                               
C                                                                               
C     ***   COSTRUZIONE MATRICE DI RIGIDEZZA   ***                              
C                                                                               
      DO 600 I=1,NCONR                                                          
      JI=(I-1)*NCS+1                                                            
      DO 610 K=I,NCONR                                                          
      JK=(K-1)*NCS+1                                                            
      DO 22 L=1,6                                                               
      DO 22 M=1,6                                                               
      DT(L,M)=D(L,M)                                                            
 22   CONTINUE                                                                  
C     ***   MATRICE K-CIRCONFLESSO   ***                                        
      CALL CAP4(D,ENNE(1,I),ENNE(1,K),ELMAT1(JI,JK),NRELMT,                     
     1           DEL,COE,CDC,IGEOM,DT)                                          
 610  CONTINUE                                                                  
      JK=IELM1+1                                                                
C     ***   MATRICE R-CIRCONFLESSO   ***                                        
      CALL CAP5(D,ENNE(1,I),CPI,ELMAT1(JI,JK),NRELMT,                           
     1           DEL,COE,CDC,IGEOM)                                             
 600  CONTINUE                                                                  
      JI=IELM1+1                                                                
      JK=IELM1+1                                                                
C     ***   MATRICE K-SEGNATA   ***                                             
      CALL CAP6(D,CPI,ELMAT1(JI,JK),NRELMT)                                     
      DO 630 I=1,IELMAT                                                         
      DO 630 K=I,IELMAT                                                         
      ELMAT1(K,I)=ELMAT1(I,K)                                                   
 630  CONTINUE                                                                  
CMT     WRITE(IOUT,222)                                                         
CMT222  FORMAT(/10X,'MATRICE DI RIGIDEZZA',/)                                   
CMT     WRITE(IOUT,223)((ELMAT1(I,K),K=1,IELMAT),I=1,IELMAT)                    
CMT223  FORMAT(/5X,(5(6D12.5/)/))                                               
C                                                                               
C     FINE COSTRUZIONE MATRICE DI RIGIDEZZA                                     
C                                                                               
C     CHIUSURA CICLO SUI PUNTI DI INTEGRAZIONE                                  
C                                                                               
 999  CONTINUE                                                                  
C                                                                               
C     CHIUSURA CICLO SUGLI STRATI                                               
C                                                                               
1000  CONTINUE                                                                  
      DO 400 I=1,IELM1                                                          
C                                                                               
C     SCRITTURA SU FILE DELLA MATRICE R-TILDE                                   
C                                                                               
      CALL JWRITE(ISLMTF,ELMAT2(1,I),IELMAT*2)                                  
 400  CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE FLESS',ELMAT2,                           
     *             NRELMT,IELMAT,IELM1,1,IPIAZ)                                 
C                                                                               
C     ANTISIMMETRIZAZIONE DELLA R-TILDE E SCRITTURA DELLA STESSA SU FILE        
C                                                                               
      DO 500 I1=1,IELM1                                                         
      DO 500 K1=I1,IELM1                                                        
      H=ELMAT2(I1,K1)-ELMAT2(K1,I1)                                             
      ELMAT2(I1,K1)=-H                                                          
      ELMAT2(K1,I1)= H                                                          
500   CONTINUE                                                                  
      DO 510 I=1,NPSI                                                           
      I1=IELM1+I                                                                
      DO 510 I2=1,IELM1                                                         
      ELMAT2(I2,I1)=ELMAT2(I1,I2)                                               
      ELMAT2(I1,I2)=-ELMAT2(I1,I2)                                              
510   CONTINUE                                                                  
      DO 550 I=1,IELMAT                                                         
      CALL JWRITE(ISLMTN,ELMAT2(1,I),IELMAT*2)                                  
550   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE TNOTO',ELMAT2,                           
     *             NRELMT,IELMAT,IELMAT,1,IPIAZ)                                
C                                                                               
C     FINE COSTRUZIONE MATRICE TERMINE NOTO                                     
C                                                                               
C                                                                               
C     SCRITTURA SU FILE DELLA MATRICE K-TILDE                                   
C                                                                               
      DO 450 I=1,IELM1                                                          
      CALL JWRITE(ISLMTF,ELMAT3(1,I),IELM1*2)                                   
 450  CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE MASSA',ELMAT3,                           
     *             NRELMT,IELM1,IELM1,1,IPIAZ)                                  
      RETURN                                                                    
      END                                                                       

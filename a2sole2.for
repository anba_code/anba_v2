C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MULV                                                          
C*************************          MULV          **********************        
      SUBROUTINE MULV(A,V,W,N,NA,ISLMAT,NW)                                     
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(1),V(1),W(1),NA(1),TEMP(1)                                    
      CALL DZERO(W,N)                                                           
      NEQ=N/2                                                                   
          CALL JPOSAB(ISLMAT,1)                                                 
          CALL JREAD(ISLMAT,A,NW)                                               
      CALL MSSKV(W(NEQ+1),A,V,NA,NEQ)                                           
          CALL JREAD(ISLMAT,A,NW)                                               
      CALL MASKV(W(NEQ+1),A,V(NEQ+1),NA,NEQ)                                    
          CALL JREAD(ISLMAT,A,NW)                                               
      CALL FORBAC(A,W(NEQ+1),NA,NEQ)                                            
      CALL SUBV(-1.D0,V(NEQ+1),W,NEQ)                                           
      RETURN                                                                    
      ENTRY MULVT(A,V,W,N,NA,TEMP,ISLMAT,NW)                                    
      CALL DZERO(W,N)                                                           
      NEQ=N/2                                                                   
      CALL COPV(V,TEMP,N)                                                       
          CALL JPOSAB(ISLMAT,NW*2+1)                                            
          CALL JREAD(ISLMAT,A,NW)                                               
      CALL FORBAC(A,TEMP(NEQ+1),NA,NEQ)                                         
          CALL JPOSAB(ISLMAT,1)                                                 
          CALL JREAD(ISLMAT,A,NW)                                               
      CALL MSSKV(W,A,TEMP(NEQ+1),NA,NEQ)                                        
          CALL JREAD(ISLMAT,A,NW)                                               
      CALL MASKTV(W(NEQ+1),A,TEMP(NEQ+1),NA,NEQ)                                
      CALL SUBV(-1.D0,V,W(NEQ+1),NEQ)                                           
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.ORDINA                                                        
C*************************          ORDINA          ********************        
      SUBROUTINE ORDINA(VATT,RR,RI,IANA,NREQ)                                   
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION VATT(1),RR(1),RI(1),IANA(1)                                     
      DO 10 I=1,NREQ                                                            
      VATT(I)=DSQRT(RR(I)*RR(I)+RI(I)*RI(I))                                    
 10   CONTINUE                                                                  
      DO 30 J=1,NREQ-1                                                          
      IMIN=J                                                                    
      DO 20 I=J+1,NREQ                                                          
      IF(VATT(IMIN).GE.VATT(I)) GOTO 20                                         
      IMIN=I                                                                    
 20   CONTINUE                                                                  
      S=VATT(J)                                                                 
      VATT(J)=VATT(IMIN)                                                        
      VATT(IMIN)=S                                                              
      S=RR(J)                                                                   
      RR(J)=RR(IMIN)                                                            
      RR(IMIN)=S                                                                
      S=RI(J)                                                                   
      RI(J)=RI(IMIN)                                                            
      RI(IMIN)=S                                                                
      IS=IANA(J)                                                                
      IANA(J)=IANA(IMIN)                                                        
      IANA(IMIN)=IS                                                             
 30   CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.ORDSLE                                                        
C*************************          ORDSLE          ********************        
      SUBROUTINE ORDSLE(AUTVT,NAUTOV,NEQV,COOR,NNODI,A,NPSI)                    
      IMPLICIT REAL*8   (A-H,O-Z)                                               
      LOGICAL DUMP                                                              
      COMPLEX*16 AUTVT(1),S(3),RLAM,A(3,6),X1,X2                                
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSFE          
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      DIMENSION COOR(2,1)                                                       
      DO 10 INAUTV=1,NAUTOV                                                     
      CALL JREAD(ISLSLE,RLAM,4)                                                 
      CALL JREAD(ISLSLE,AUTVT,NEQV*4)                                           
      CALL JPOSRL(ISLSLE,-NEQV*4)                                               
      RR=     RLAM                                                              
      IF(RR.LE.0.)GOTO 11                                                       
      DO 1040 L=1,3                                                             
      DO 1050 K=1,6                                                             
      A(L,K)=(0.,0.)                                                            
 1050 CONTINUE                                                                  
      A(L,L)=RLAM                                                               
 1040 CONTINUE                                                                  
      A(1,5)=-RLAM*RLAM                                                         
      A(2,4)=A(1,5)                                                             
      DO 1020 I=1,NNODI                                                         
      X1=DCMPLX(COOR(1,I))                                                      
      X2=DCMPLX(COOR(2,I))                                                      
      IPS=(I-1)*3                                                               
      DO 1030 K=1,3                                                             
      S(K)=AUTVT(IPS+K)                                                         
 1030 CONTINUE                                                                  
      A(1,6)=-X2*RLAM                                                           
      A(2,6)=X1*RLAM                                                            
      A(3,4)=-A(1,6)                                                            
      A(3,5)=-A(2,6)                                                            
      DO 1060 K=1,3                                                             
      DO 1060 L=1,6                                                             
      S(K)=S(K)+A(K,L)*AUTVT(NEQV-NPSI+L)                                       
 1060 CONTINUE                                                                  
 1020 CONTINUE                                                                  
 11   CALL JWRITE(ISLSLE,AUTVT,NEQV*4)                                          
      DO 20 K=1,NEQV                                                            
      AUTVT(K)=AUTVT(K)/RLAM                                                    
 20   CONTINUE                                                                  
      CALL JWRITE(ISLSLE,AUTVT,NEQV*4)                                          
 10   CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.ORTSCL                                                        
C*************************          ORTSCL          ********************        
      SUBROUTINE ORTSCL(VC,VL,NVEC,C,NTERM,NRVC,NRVL,TEMP,A,NA,                 
     *                  TEMP1,ORTERR,ZERTOL,ISLMAT,NW)                          
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(1),VC(NRVC,1),VL(NRVL,1),C(1),TEMP(1),TEMP1(1),NA(1)          
      NRTR=NVEC                                                                 
      IRAND=0                                                                   
      IT=0                                                                      
      IF(NVEC.GT.1)GOTO65                                                       
90    CONTINUE                                                                  
      CALL RANDOM(TEMP,NTERM)                                                   
      CALL MULV(A,TEMP,VC(1,NVEC),NTERM,NA,ISLMAT,NW)                           
      CALL RANDOM(TEMP,NTERM)                                                   
      CALL MULVT(A,TEMP,VL(1,NVEC),NTERM,NA,TEMP1,ISLMAT,NW)                    
      IF(NVEC.LE.1)GOTO 20                                                      
 10   CONTINUE                                                                  
      VL(NTERM+1,NVEC)=E                                                        
65    CONTINUE                                                                  
      IF(NRTR.GT.1) GOTO 35                                                     
      WRITE(6,110)NVEC,IT,IRAND                                                 
110   FORMAT('  VETTORE ',I4,'  ORTOGONALIZZATO DOPO',I4,'  ITERAZ',            
     1 'IONI E CON',I4,'  RIGENERAZIONI ')                                      
      RETURN                                                                    
35    N=NVEC-1                                                                  
60    IT=IT+1                                                                   
C***********************************************************************        
      DO40I=1,N                                                                 
      E=VL(NTERM+1,I)                                                           
      TEMP(I)=SCALAR(VC(1,NVEC),VL(1,I),NTERM)/E                                
      TEMP1(I)=SCALAR(VL(1,NVEC),VC(1,I),NTERM)/E                               
40    CONTINUE                                                                  
      E=0.                                                                      
      DO50K=1,NTERM                                                             
      S=0.                                                                      
      T=0.                                                                      
      DO55I=1,N                                                                 
      T=T+TEMP1(I)*VL(K,I)                                                      
55    S=S+TEMP(I)*VC(K,I)                                                       
      Q=DABS(T/VL(K,NVEC))                                                      
      IF(Q.GT.E)E=Q                                                             
      Q=DABS(S/VC(K,NVEC))                                                      
      IF(Q.GT.E)E=Q                                                             
      VL(K,NVEC)=VL(K,NVEC)-T                                                   
50    VC(K,NVEC)=VC(K,NVEC)-S                                                   
C***********************************************************************        
      IF(E.LT.ORTERR)GOTO20                                                     
      IF(IT.LT.16)GOTO60                                                        
      WRITE(6,140)ORTERR,E                                                      
140   FORMAT(' IMPOSSIBILE ORTOGONALIZZARE A',E15.8,' ACCETTATO',E15.8)         
20    E=SCALAR(VC(1,NVEC),VC(1,NVEC),NTERM)                                     
      IF(DABS(E).LT.ZERTOL)GOTO25                                               
      T=SCALAR(VL(1,NVEC),VL(1,NVEC),NTERM)                                     
      IF(DABS(T).LT.ZERTOL)GOTO25                                               
      E=DSQRT(E)                                                                
      C(NVEC)=E                                                                 
      E=1./E                                                                    
      T=1./DSQRT(T)                                                             
      DO30I=1,NTERM                                                             
      VC(I,NVEC)=VC(I,NVEC)*E                                                   
30    VL(I,NVEC)=VL(I,NVEC)*T                                                   
      E=SCALAR(VC(1,NVEC),VL(1,NVEC),NTERM)                                     
      IF(DABS(E).GT.ZERTOL)GOTO80                                               
      WRITE(6,100)NVEC,IRAND                                                    
100   FORMAT(' *** AVVISO (ORTSCL): I VETTORI',I4,' HANNO PRODOTTO SCAL'        
     1   ,'ARE NULLO (TENTATE',I4,'  RIGENERAZIONI)')                           
      GOTO80                                                                    
25    IRAND=IRAND+1                                                             
      IF(IRAND.LT.3)GOTO90                                                      
80    NRTR=1                                                                    
      GOTO10                                                                    
      END                                                                       
C*************************          PRTSLE          ********************        
      SUBROUTINE PRTSLE(NAUTOV,ISELEZ,ISLSLE,AUTVET,                            
     *                  LABEL,NNODI,NUME,NEQV,NUINT,NODSEL,NNSEL,NCS)           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMPLEX*16 AUTVET(*),RLAM                                                 
      INTEGER ISELEZ(NAUTOV),NUME(*),NUINT(*),NODSEL(*),LABEL(*)                
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
C                                                                               
C                                                                               
      DO 30 IS=1,NAUTOV                                                         
       Ia=ISELEZ(IS)                                                            
       JUMP=(4+6*4*2+NNODI*3*2*2*2)                                             
       jump=jump*(Ia-1)+1                                                       
       CALL JPOSAB(ISLSLE,JUMP)                                                 
       CALL JREAD(ISLSLE,RLAM,4)                                                
       CALL JREAD(ISLSLE,AUTVET,(NEQV)*4)                                       
400    FORMAT(//8X,'***   AUTOVETTORE N.',I8,10X,'AUTOVALORE (',E12.6,          
     * ',',E12.6,')   ***'//,4X,'ID.NODO',16X,'GX',31X,'GY',31X,'GZ'//)         
      NNS=NNSEL                                                                 
      i=0                                                                       
      IF(NNSEL.EQ.0) NNS=NNODI                                                  
      NNOP=45                                                                   
      NBL=(NNS+NNOP-1)/NNOP                                                     
      NN=NNOP                                                                   
      DO 50 IW=1,NBL                                                            
       CALL INTEST                                                              
       WRITE(IOUT,400) Ia,RLAM                                                  
       IF(IW.EQ.NBL) NN=NNS-(NBL-1)*NN                                          
       DO 51 LL=1,NN                                                            
 77     I=I+1                                                                   
        I2=NUINT(I)                                                             
        I7=(I2-1)*NCS+1                                                         
        I1=LABEL(I)                                                             
        IF(NNSEL.NE.0)THEN                                                      
         IPNOD=IPOSL(I1,NODSEL,NNSEL)                                           
         IF(IPNOD.EQ.0)GOTO 77                                                  
        END IF                                                                  
        IND=NUME(I7)-1                                                          
        WRITE(IOUT,440)I1,(AUTVET(IND+L),L=1,NCS)                               
 51    CONTINUE                                                                 
50    CONTINUE                                                                  
      CALL INTEST                                                               
      WRITE(IOUT,460)                                                           
      DO 80 I=NEQV-5,NEQV                                                       
       WRITE(IOUT,450) AUTVET(I)                                                
80    CONTINUE                                                                  
430   FORMAT(/53X,'***  SOLUZIONI : INGOBBAMENTI  ***'// )                      
460   FORMAT(/47X,'***  SOLUZIONI : PARAMETRI DI DEFORMAZIONE  ***'//)          
440   FORMAT(1X,I8,3(4X,'(',2E16.5,')'))                                        
441   FORMAT(9X,I6,2E15.8)                                                      
450   FORMAT(15X,2E15.8)                                                        
C                                                                               
30    CONTINUE                                                                  
204   FORMAT(//'  AUTOVETTORE N.',I8)                                           
206   FORMAT(1X,3(4X,'(',2E16.5,')')/)                                          
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.QR2TRD                                                        
C*************************          QR2TRD          ********************        
      SUBROUTINE QR2TRD(A,B,C,N,RR,RI,Z,NRZ,IANA)                               
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION Z(NRZ,1),IANA(1),A(1),B(1),C(1),RR(1),RI(1)                     
      N1=N-1                                                                    
      DO10I=1,N                                                                 
      DO10K=1,N                                                                 
10    Z(I,K)=0.                                                                 
      DO30I=1,N1                                                                
      Z(I,I)=A(I)                                                               
      Z(I,I+1)=B(I)                                                             
30    Z(I+1,I)=C(I+1)                                                           
      Z(N,N)=A(N)                                                               
      CALL DQR2(Z,RR,RI,IANA,N,NRZ)                                             
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.RANDOM                                                        
C*************************          RANDOM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RANDOM(X,N)                                                    
      REAL*8 X(1)                                                               
      DATA I/783637/                                                            
      DO 10 L=1,N                                                               
      I=125*I                                                                   
      I=I-(I/2796203)*2796203                                                   
      X(L)=FLOAT(I)/2796202                                                     
      X(L)=X(L)+X(L)-1.D0                                                       
10    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.RMOD                                                          
C*************************           RMOD           ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      DOUBLE PRECISION FUNCTION RMOD(A,B)                                       
      REAL*8 A,B                                                                
C                                                                               
      RMOD=A*A+B*B                                                              
      RETURN                                                                    
      END                                                                       
C*************************          SCALAR          ********************        
      DOUBLE PRECISION FUNCTION SCALAR(V,W,N)                                   
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION V(1),W(1)                                                       
      SCALA =0.                                                                 
      DO10I=1,N                                                                 
10    SCALA =SCALA +V(I)*W(I)                                                   
      SCALAR=SCALA                                                              
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.SUBV                                                          
C*************************          SUBV          **********************        
      SUBROUTINE SUBV(A,V,W,N)                                                  
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION W(1),V(1)                                                       
      DO10I=1,N                                                                 
10    W(I)=W(I)-A*V(I)                                                          
      RETURN                                                                    
      ENTRY COPV(V,W,N)                                                         
      DO20I=1,N                                                                 
20    W(I)=V(I)                                                                 
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.SFECOR                                                        
C*************************         SFECOR         **********************        
      SUBROUTINE SFECOR(NUME,NUINT,SOL,SIGMA,NSOL,NRSOL)                        
C                                                                               
C                                                                               
C     CALCOLA GLI SFORZI NEI CORRENTI                                           
C                                                                               
C                                                                               
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION SIGMA(1),NUME(1),NUINT(1),SOL(NRSOL,1)                          
C                                                                               
C                                                                               
      LOGICAL DUMP                                                              
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                        
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSFE          
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /CORR/ E,RO,AREA                                                   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      DATA RAD/57.29577951D0/                                                   
C                                                                               
C                                                                               
C                                                                               
      CALL INTEST                                                               
      WRITE(IOUT,110)                                                           
      LSUP=40                                                                   
      DO 500 IEL=1,NCORR                                                        
      IF(IEL.LE.LSUP)GOTO 50                                                    
      LSUP=LSUP+40                                                              
      CALL INTEST                                                               
      WRITE(IOUT,110)                                                           
C                                                                               
50    CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.NE.1) GOTO 900                                                    
      CALL JREAD(ISLELM,ICON,1)                                                 
      CALL JREAD(ISLELM,X,2)                                                    
      CALL JREAD(ISLELM,Y,2)                                                    
      CALL JREAD(ISLELM,E,2)                                                    
      CALL JPOSRL(ISLELM,4)                                                     
      IF(IFLMR.EQ.0) GOTO 50                                                    
      IND=NUINT(ICON)*NCS                                                       
      IND=NUME(IND)+NEQV                                                        
      IND1=NEQV-NPSI                                                            
      DO 10 I=1,NSOL                                                            
      SIGMA(I)=(SOL(IND,I)+SOL(IND1+3,I)+Y*SOL(IND1+4,I)                        
     *          -X*SOL(IND1+5,I))                                               
10    CONTINUE                                                                  
      CALL JWRITE(ISLSLE,SIGMA,4)                                               
      DO 40 I=1,NSOL                                                            
      SIGMA(I)=SIGMA(I)*E                                                       
 40   CONTINUE                                                                  
      RMOD=DSQRT(SIGMA(1)*SIGMA(1)+SIGMA(2)*SIGMA(2))                           
      FASE=1.570796327D0                                                        
      IF(SIGMA(1).NE.0.D0) FASE=DATAN2(SIGMA(2),SIGMA(1))                       
      FASE=FASE*RAD                                                             
      WRITE(IOUT,100)IDEL,SIGMA(1),SIGMA(2),RMOD,FASE                           
      CALL JWRITE(ISLSFE,SIGMA,4)                                               
      CALL JWRITE(ISLSFE,RMOD,2)                                                
      CALL JWRITE(ISLSFE,FASE,2)                                                
 500  CONTINUE                                                                  
      RETURN                                                                    
900   CALL JPOSRL(ISLELM,-4)                                                    
      RETURN                                                                    
100   FORMAT(9X,I6,3X,4E17.6)                                                   
110   FORMAT(//,40X,'*** SFORZI NEGLI ELEMENTI DI CORRENTE (SIGMA) ***'         
     1///,10X,'ID.ELEM.', 9X,'P.REALE',10X,'P.IMMAG.', 9X,'MODULO',             
     2  12X,'FASE'/)                                                            
      END                                                                       
C@ELT,I ANBA*ANBA.SFEGIU                                                        
C*************************         SFEGIU         **********************        
      SUBROUTINE SFEGIU(NUME,NUINT,SOL,SIGMA,ICON,XY,NSOL,NRSOL)                
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP                                                              
      DIMENSION NUME(1),NUINT(1),SOL(NRSOL,6),SIGMA(6),ICON(2),XY(4)            
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                        
      COMMON /GIUNZ/ G,RO,RL,T,DELTAX,DELTAY,OMEGA                              
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSFE          
      DATA RAD/57.29577951D0/                                                   
C                                                                               
C                                                                               
C     CALCOLA GLI SFORZI NELLE GIUNZIONI CON DUE NODI                           
C                                                                               
      CALL INTEST                                                               
      WRITE(IOUT,100)                                                           
      LSUP=40                                                                   
      DO 500 IEL=1,NGIUR                                                        
      IF(IEL.LE.LSUP) GOTO 50                                                   
      LSUP=LSUP+40                                                              
      CALL INTEST                                                               
      WRITE(IOUT,100)                                                           
50    CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.NE.2)GOTO 900                                                     
      CALL JREAD(ISLELM,ICON,2)                                                 
      CALL JREAD(ISLELM,XY,8)                                                   
      CALL JREAD(ISLELM,G,8)                                                    
      CALL JREAD(ISLELS,DELTAX,6)                                               
      IF(IFLMR.EQ.0) GOTO 50                                                    
      GSUL=G/RL                                                                 
      IND1=NUINT(ICON(1))*NCS                                                   
      IND2=NUINT(ICON(2))*NCS                                                   
      IND1=NUME(IND1)                                                           
      IND2=NUME(IND2)                                                           
      IND3=NEQV-NPSI                                                            
      DO 10 I=1,NSOL                                                            
      SIGMA(I)=SOL(IND2,I)-SOL(IND1,I)+DELTAX*SOL(IND3+1,I)+                    
     1         DELTAY*SOL(IND3+2,I)+2.*OMEGA*SOL(IND3+6,I)                      
10    CONTINUE                                                                  
      CALL JWRITE(ISLSFE,SIGMA,NSOL*2)                                          
      DO 30 I=1,NSOL                                                            
      SIGMA(I)=SIGMA(I)*GSUL                                                    
 30   CONTINUE                                                                  
      RMOD=DSQRT(SIGMA(1)*SIGMA(1)+SIGMA(2)*SIGMA(2))                           
      FASE=1.570796327D0                                                        
      IF(SIGMA(1).NE.0.D0) FASE=DATAN2(SIGMA(2),SIGMA(1))                       
      FASE=FASE*RAD                                                             
      WRITE(IOUT,100)IDEL,SIGMA(1),SIGMA(2),RMOD,FASE                           
      CALL JWRITE(ISLSFE,SIGMA,NSOL*2)                                          
      CALL JWRITE(ISLSFE,RMOD,2)                                                
      CALL JWRITE(ISLSFE,FASE,2)                                                
 500  CONTINUE                                                                  
      RETURN                                                                    
100   FORMAT(//,40X,'*** SFORZI NEGLI ELEMENTI DI GIUNZIONE (TAU) ***'          
     1///,10X,'ID.ELEM.',10X,'P.REALE',10X,'P.IMMAG.',10X,'MODULO',             
     2  14X,'FASE'/)                                                            
110   FORMAT(9X,I6,1X,4E17.6)                                                   
900   CALL JPOSRL(ISLELM,-3)                                                    
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.SFEELP                                                        
C*************************         SFEELP         **********************        
      SUBROUTINE SFEELP(NUME,NUINT,SOL,SIGMAG,SIGMAL,EPSI,D,ICON,XY,            
     1   NSOL,NRSOL)                                                            
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP                                                              
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                 
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELPIA/  LREC,NPINT,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),             
     1                T2(3,3),DG(6,6),W                                         
      COMMON /ELPIB/  INT1,INT2,TETA,DEN,alfort,IFLTET                          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSFE          
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      DIMENSION NUME(1),NUINT(1),SOL(NRSOL,1),SIGMAG(NPSI,1),                   
     1          SIGMAL(NPSI,1),ICON(1),XY(2,1),EPSI(NPSI,1),D(6,6),             
     2          RMOD(6),FASE(6)                                                 
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      NWORD=NPSI*NSOL*2                                                         
      DO 500 IEL=1,NELPR                                                        
 50   CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.NE.5)GOTO 900                                                     
      CALL JREAD(ISLELM,ICON,NCON)                                              
      CALL JREAD(ISLELM,XY,NCON*4)                                              
      CALL JREAD(ISLELM,nwr,1)                                                  
      CALL JREAD(ISLELM,INT1,nwr)                                               
      DO 5 I=1,6                                                                
      CALL JREAD(ISLELM,D(1,I),12)                                              
 5    CONTINUE                                                                  
      call jposrl(islelm,12)                                                    
      CALL JREAD(ISLELS,LREC,2)                                                 
      IN=0                                                                      
      NBL=(NPINT+5)/6                                                           
      NN=6                                                                      
      DO 10 IW=1,NBL                                                            
      IF(IFLMR.EQ.0) GOTO 11                                                    
      CALL INTEST                                                               
      WRITE(IOUT,100)IDEL                                                       
 11   IF(IW.EQ.NBL) NN=NPINT-(NBL-1)*NN                                         
      DO 10 LL=1,NN                                                             
      IN=IN+1                                                                   
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      IF(IFLMR.EQ.0) GOTO 10                                                    
      CALL DZERO(EPSI,36)                                                       
      DO 15 K1=1,6                                                              
      DO 15 K2=1,6                                                              
      DG(K1,K2)=DG(K1,K2)/W                                                     
 15   CONTINUE                                                                  
      DO 20 I=1,NSOL                                                            
      IND1=NEQV-NPSI                                                            
C                                                                               
C     CALCOLO EPSILON=EPSILON+ZETA*PSI                                          
C                                                                               
      EPSI(1,I)=EPSI(1,I)+SOL(IND1+1,I)-CPI(2)*SOL(IND1+6,I)                    
      EPSI(2,I)=EPSI(2,I)+SOL(IND1+2,I)+CPI(1)*SOL(IND1+6,I)                    
      EPSI(3,I)=EPSI(3,I)+SOL(IND1+3,I)+CPI(2)*SOL(IND1+4,I)                    
     1                                 -CPI(1)*SOL(IND1+5,I)                    
          M=0                                                                   
      DO 40 K=1,NCON                                                            
      IF(ICON(K).EQ.0) GOTO 40                                                  
          M=M+1                                                                 
      IND=(NUINT(ICON(K))-1)*NCS+1                                              
      IND=NUME(IND)-1                                                           
C                                                                               
C     CALCOLO EPSILON=EPSILON+ESSE*G DERIVATO/Z                                 
C                                                                               
      EPSI(1,I)=EPSI(1,I)+ENNE(1,M)*SOL(IND+NEQV+1,I)                           
      EPSI(2,I)=EPSI(2,I)+ENNE(1,M)*SOL(IND+NEQV+2,I)                           
      EPSI(3,I)=EPSI(3,I)+ENNE(1,M)*SOL(IND+NEQV+3,I)                           
C                                                                               
C                                                                               
C     CALCOLO EPSILON=EPSILON+G*OPERATORE DERIVATA                              
C                                                                               
      EPSI(1,I)=EPSI(1,I)+ENNE(2,M)*SOL(IND+3,I)                                
      EPSI(2,I)=EPSI(2,I)+ENNE(3,M)*SOL(IND+3,I)                                
      EPSI(4,I)=EPSI(4,I)+ENNE(2,M)*SOL(IND+1,I)                                
      EPSI(5,I)=EPSI(5,I)+ENNE(3,M)*SOL(IND+2,I)                                
      EPSI(6,I)=EPSI(6,I)+ENNE(3,M)*SOL(IND+1,I)+ENNE(2,M)*SOL(IND+2,I)         
 40   CONTINUE                                                                  
 20   CONTINUE                                                                  
C                                                                               
      CALL DPROMM(DG,6,6,6,EPSI,6,SIGMAG,6,2,0,0)                               
      CALL DPROMM(D(1,1),6,3,3,T1,3,DG(1,1),6,3,0,1)                            
      CALL DPROMM(D(4,1),6,3,3,T1,3,DG(4,1),6,3,0,1)                            
      CALL DPROMM(D(1,4),6,3,3,T2,3,DG(1,4),6,3,0,1)                            
      CALL DPROMM(D(4,4),6,3,3,T2,3,DG(4,4),6,3,0,1)                            
      CALL DPROMM(DG,6,6,6,EPSI,6,SIGMAL,6,2,0,0)                               
      DO 60 I=1,NPSI                                                            
      RMOD(I)=DSQRT(SIGMAL(I,1)*SIGMAL(I,1)+SIGMAL(I,2)*SIGMAL(I,2))            
      FASE(I)=1.5707963270D0                                                    
      IF(SIGMAL(I,1).NE.0.D0)FASE(I)=DATAN2(SIGMAL(I,2),SIGMAL(I,1))            
      FASE(I)=FASE(I)*RAD                                                       
 60   CONTINUE                                                                  
      WRITE(IOUT,111)    IN,(SIGMAL(1,K),SIGMAG(1,K),K=1,NSOL)                  
     1                  ,RMOD(1),FASE(1)                                        
      WRITE(IOUT,112)CPI(1),(SIGMAL(2,K),SIGMAG(2,K),K=1,NSOL)                  
     2                  ,RMOD(2),FASE(2)                                        
      WRITE(IOUT,113)CPI(2),(SIGMAL(3,K),SIGMAG(3,K),K=1,NSOL)                  
     3                  ,RMOD(3),FASE(3)                                        
      WRITE(IOUT,114)       (SIGMAL(4,K),SIGMAG(4,K),K=1,NSOL)                  
     4                  ,RMOD(4),FASE(4)                                        
      WRITE(IOUT,115)       (SIGMAL(5,K),SIGMAG(5,K),K=1,NSOL)                  
     5                  ,RMOD(5),FASE(5)                                        
      WRITE(IOUT,116)       (SIGMAL(6,K),SIGMAG(6,K),K=1,NSOL)                  
     6                  ,RMOD(6),FASE(6)                                        
      CALL JWRITE(ISLSFE,EPSI,NWORD)                                            
      CALL JWRITE(ISLSFE,SIGMAG,NWORD)                                          
      CALL JWRITE(ISLSFE,SIGMAL,NWORD)                                          
      CALL JWRITE(ISLSFE,RMOD,12)                                               
      CALL JWRITE(ISLSFE,FASE,12)                                               
 10   CONTINUE                                                                  
      IF(IFLMR.EQ.0) GOTO 50                                                    
 500  CONTINUE                                                                  
      RETURN                                                                    
 900  CALL JPOSRL(ISLELM,-4)                                                    
      RETURN                                                                    
100   FORMAT(/46X,'***  SFORZI NELL''ELEMENTO PIANO N.RO',I6,'  ***'///         
     1 22X,'COMPONENTE REALE',9X,'COMPONENTE IMMAGINARIA'//                     
     2 19X,2('   RIF LOC.    RIF GEN.  '),'  MODULO',7X,'FASE')                 
111   FORMAT(/' P.INT. N.',I4,'  TXZ',6(2X,E10.4))                              
112   FORMAT(' X =',E10.4,'  TYZ',6(2X,E10.4))                                  
113   FORMAT(' Y =',E10.4,'  SZ ',6(2X,E10.4))                                  
114   FORMAT(14X,'  SX ',6(2X,E10.4))                                           
115   FORMAT(14X,'  SY ',6(2X,E10.4))                                           
116   FORMAT(14X,'  TXY',6(2X,E10.4))                                           
      END                                                                       
C@ELT,I ANBA*ANBA.SFELAM                                                        
C*************************         SFELAM         **********************        
      SUBROUTINE SFELAM(NUME,NUINT,SOL,SIGMAG,SIGMAL,EPSI,D,ICON,XY,            
     1   NSOL,NRSOL)                                                            
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP                                                              
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                 
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELPIA/  LREC,NPINT,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),             
     1                T2(3,3),DG(6,6),W                                         
      COMMON /ELPIB/  INT1,INT2,TETA,DEN,alfort,IFLTET                          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSFE          
      DIMENSION NUME(1),NUINT(1),SOL(NRSOL,1),SIGMAG(NPSI,1),SPES(4),           
     1          SIGMAL(NPSI,1),ICON(1),XY(2,1),EPSI(NPSI,1),D(6,6),             
     2          RMOD(6),FASE(6)                                                 
      DATA RAD/57.29577951D0/                                                   
      NWORD=NSOL*NPSI*2                                                         
      DO 500 IEL=1,NLAMR                                                        
 50   CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.NE.4)GOTO 900                                                     
      CALL JREAD(ISLELM,ICON,NCON)                                              
      NCOPP=NCON/2                                                              
      CALL JREAD(ISLELM,SPES,NCOPP*2)                                           
      CALL JREAD(ISLELM,XY,NCON*4)                                              
      CALL JREAD(ISLELM,nwr,1)                                                  
      CALL JREAD(ISLELM,INT1,nwr)                                               
      DO 5 I=1,6                                                                
      CALL JREAD(ISLELM,D(1,I),12)                                              
 5    CONTINUE                                                                  
      CALL JREAD(ISLELS,LREC,2)                                                 
      IN=0                                                                      
      NBL=(NPINT+5)/6                                                           
      NN=6                                                                      
      DO 10 IW=1,NBL                                                            
      IF(IFLMR.EQ.0) GOTO 11                                                    
      CALL INTEST                                                               
      WRITE(IOUT,100)IDEL                                                       
 11   IF(IW.EQ.NBL) NN=NPINT-(NBL-1)*NN                                         
      DO 10 LL=1,NN                                                             
      IN=IN+1                                                                   
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      IF(IFLMR.EQ.0) GOTO 10                                                    
      CALL DZERO(EPSI,36)                                                       
      DO 15 K1=1,6                                                              
      DO 15 K2=1,6                                                              
      DG(K1,K2)=DG(K1,K2)/W                                                     
 15   CONTINUE                                                                  
      DO 20 I=1,NSOL                                                            
      IND1=NEQV-NPSI                                                            
C                                                                               
C     CALCOLO EPSILON=EPSILON+ZETA*PSI                                          
C                                                                               
      EPSI(1,I)=EPSI(1,I)+SOL(IND1+1,I)-CPI(2)*SOL(IND1+6,I)                    
      EPSI(2,I)=EPSI(2,I)+SOL(IND1+2,I)+CPI(1)*SOL(IND1+6,I)                    
      EPSI(3,I)=EPSI(3,I)+SOL(IND1+3,I)+CPI(2)*SOL(IND1+4,I)                    
     1                                 -CPI(1)*SOL(IND1+5,I)                    
      DO 40 K=1,NCON                                                            
      IND=(NUINT(ICON(K))-1)*NCS+1                                              
      IND=NUME(IND)-1                                                           
C                                                                               
C     CALCOLO EPSILON=EPSILON+ESSE*G DERIVATO/Z                                 
C                                                                               
      EPSI(1,I)=EPSI(1,I)+ENNE(1,K)*SOL(IND+NEQV+1,I)                           
      EPSI(2,I)=EPSI(2,I)+ENNE(1,K)*SOL(IND+NEQV+2,I)                           
      EPSI(3,I)=EPSI(3,I)+ENNE(1,K)*SOL(IND+NEQV+3,I)                           
C                                                                               
C                                                                               
C     CALCOLO EPSILON=EPSILON+G*OPERATORE DERIVATA                              
C                                                                               
      EPSI(1,I)=EPSI(1,I)+ENNE(2,K)*SOL(IND+3,I)                                
      EPSI(2,I)=EPSI(2,I)+ENNE(3,K)*SOL(IND+3,I)                                
      EPSI(4,I)=EPSI(4,I)+ENNE(2,K)*SOL(IND+1,I)                                
      EPSI(5,I)=EPSI(5,I)+ENNE(3,K)*SOL(IND+2,I)                                
      EPSI(6,I)=EPSI(6,I)+ENNE(3,K)*SOL(IND+1,I)+ENNE(2,K)*SOL(IND+2,I)         
 40   CONTINUE                                                                  
 20   CONTINUE                                                                  
C      CALL DWRITE('EPSI',EPSI,6,6,2,1)                                         
C                                                                               
      CALL DPROMM(DG,6,6,6,EPSI,6,SIGMAG,6,2,0,0)                               
      CALL DPROMM(D(1,1),6,3,3,T1,3,DG(1,1),6,3,0,1)                            
      CALL DPROMM(D(4,1),6,3,3,T1,3,DG(4,1),6,3,0,1)                            
      CALL DPROMM(D(1,4),6,3,3,T2,3,DG(1,4),6,3,0,1)                            
      CALL DPROMM(D(4,4),6,3,3,T2,3,DG(4,4),6,3,0,1)                            
      CALL DPROMM(DG,6,6,6,EPSI,6,SIGMAL,6,2,0,0)                               
      DO 60 I=1,NPSI                                                            
      RMOD(I)=DSQRT(SIGMAL(I,1)*SIGMAL(I,1)+SIGMAL(I,2)*SIGMAL(I,2))            
      FASE(I)=1.5707963270D0                                                    
      IF(SIGMAL(I,1).NE.0.D0)FASE(I)=DATAN2(SIGMAL(I,2),SIGMAL(I,1))            
      FASE(I)=FASE(I)*RAD                                                       
 60   CONTINUE                                                                  
      WRITE(IOUT,111)    IN,(SIGMAL(1,K),SIGMAG(1,K),K=1,NSOL)                  
     1                  ,RMOD(1),FASE(1)                                        
      WRITE(IOUT,112)CPI(1),(SIGMAL(2,K),SIGMAG(2,K),K=1,NSOL)                  
     2                  ,RMOD(2),FASE(2)                                        
      WRITE(IOUT,113)CPI(2),(SIGMAL(3,K),SIGMAG(3,K),K=1,NSOL)                  
     3                  ,RMOD(3),FASE(3)                                        
      WRITE(IOUT,114)       (SIGMAL(4,K),SIGMAG(4,K),K=1,NSOL)                  
     4                  ,RMOD(4),FASE(4)                                        
      WRITE(IOUT,115)       (SIGMAL(5,K),SIGMAG(5,K),K=1,NSOL)                  
     5                  ,RMOD(5),FASE(5)                                        
      WRITE(IOUT,116)       (SIGMAL(6,K),SIGMAG(6,K),K=1,NSOL)                  
     6                  ,RMOD(6),FASE(6)                                        
      CALL JWRITE(ISLSFE,EPSI,NWORD)                                            
      CALL JWRITE(ISLSFE,SIGMAG,NWORD)                                          
      CALL JWRITE(ISLSFE,SIGMAL,NWORD)                                          
      CALL JWRITE(ISLSFE,RMOD,12)                                               
      CALL JWRITE(ISLSFE,FASE,12)                                               
 10   CONTINUE                                                                  
      IF(IFLMR.EQ.0) GOTO 50                                                    
 500  CONTINUE                                                                  
      RETURN                                                                    
 900  CALL JPOSRL(ISLELM,-4)                                                    
      RETURN                                                                    
100   FORMAT(/41X,'***  SFORZI NELL''ELEMENTO DI LAMINA N.RO',I6,'  ***'        
     1 ///22X,'COMPONENTE REALE',9X,'COMPONENTE IMMAGINARIA'//                  
     2 19X,2('   RIF LOC.    RIF GEN.  '),'  MODULO',7X,'FASE')                 
111   FORMAT(/' P.INT. N.',I4,'  TXZ',6(2X,E10.4))                              
112   FORMAT(' X =',E10.4,'  TYZ',6(2X,E10.4))                                  
113   FORMAT(' Y =',E10.4,'  SZ ',6(2X,E10.4))                                  
114   FORMAT(14X,'  SX ',6(2X,E10.4))                                           
115   FORMAT(14X,'  SY ',6(2X,E10.4))                                           
116   FORMAT(14X,'  TXY',6(2X,E10.4))                                           
      END                                                                       
C@ELT,I ANBA*ANBA.SFEPAN                                                        
C*************************         SFEPAN         **********************        
      SUBROUTINE SFEPAN(NUME,NUINT,SOL,SIGMAG,SIGMAL,ICON,XY,NSOL,NRSOL,        
     1                  EPSI,D)                                                 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP                                                              
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODL,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSFE          
      COMMON /PANN/  TICK,DEN,NPINT                                             
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      DIMENSION B(1),ICON(8),SHAPE(1),XY(8),SIGMAL(2,6),SIGMAG(3,6),            
     1          NUME(1),NUINT(1),SOL(NRSOL,6),D(2,2),EPSI(2,6),Z(2,4),          
     2          RMOD(2),FASE(2)                                                 
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      NRIG=0                                                                    
      CALL INTEST                                                               
      WRITE(IOUT,100)                                                           
      DO 500 IEL=1,NPANR                                                        
50    CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.NE.3) GOTO 900                                                    
      CALL JREAD(ISLELM,ICON,NCON)                                              
      CALL JREAD(ISLELM,XY,NCON*4)                                              
      CALL JREAD(ISLELM,TICK,5)                                                 
      IF(IFLMR.EQ.0)GOTO 6                                                      
      NRIG=NRIG+NPINT*3+1                                                       
      IF(NRIG.LE.40) GOTO 6                                                     
      NRIG=NPINT*3+1                                                            
      CALL INTEST                                                               
      WRITE(IOUT,100)                                                           
 6    DO 5 I=1,2                                                                
      CALL JREAD(ISLELM,D(1,I),4)                                               
5     CONTINUE                                                                  
      D(2,1)=D(1,2)                                                             
      DO 10 IN=1,NPINT                                                          
      CALL JREAD(ISLELS,B,NCON*2)                                               
      CALL JREAD(ISLELS,SHAPE,NCON*2)                                           
      DO 15 I=1,NPSI                                                            
      CALL JREAD(ISLELS,Z(1,I),4)                                               
15    CONTINUE                                                                  
      IF(IFLMR.EQ.0)GOTO 10                                                     
      DO 20 I=1,NSOL                                                            
      IND1=NEQV-NPSI                                                            
      DO 30 L=1,2                                                               
      EPSI(L,I)=0.D0                                                            
      DO 30 K=1,NPSI                                                            
      EPSI(L,I)=EPSI(L,I)+Z(L,K)*SOL(IND1+K,I)                                  
30    CONTINUE                                                                  
      DO 40 K=1,NCON                                                            
      IND=NUINT(ICON(K))*NCS                                                    
      IND=NUME(IND)                                                             
      EPSI(1,I)=EPSI(1,I)+SHAPE(K)*SOL(IND+NEQV,I)                              
      EPSI(2,I)=EPSI(2,I)+B(K)*SOL(IND,I)                                       
40    CONTINUE                                                                  
      SIGMAL(1,I)=(D(1,1)*EPSI(1,I)+D(1,2)*EPSI(2,I))/TICK                      
      SIGMAL(2,I)=(D(2,1)*EPSI(1,I)+D(2,2)*EPSI(2,I))/TICK                      
      SIGMAG(1,I)=SIGMAL(1,I)                                                   
      SIGMAG(2,I)=SIGMAL(2,I)*Z(2,1)                                            
      SIGMAG(3,I)=SIGMAL(2,I)*Z(2,2)                                            
20    CONTINUE                                                                  
      DO 60 I=1,2                                                               
      RMOD(I)=DSQRT(SIGMAL(I,1)*SIGMAL(I,1)+SIGMAL(I,2)*SIGMAL(I,2))            
      FASE(I)=1.570796327D0                                                     
      IF(SIGMAL(I,1).NE.0.D0) FASE(I)=DATAN2(SIGMAL(I,2),SIGMAL(I,1))           
      FASE(I)=FASE(I)*RAD                                                       
 60   CONTINUE                                                                  
      IF(IN.EQ.1)WRITE(IOUT,110)IDEL,IN,(SIGMAL(1,I),SIGMAL(2,I),               
     1 I=1,NSOL),RMOD(1),FASE(1),RMOD(2),FASE(2),                               
     2         (SIGMAG(1,I),SIGMAG(2,I),I=1,NSOL),(SIGMAG(3,I),I=1,NSOL)        
      IF(IN.GT.1)WRITE(IOUT,111)IN,(SIGMAL(1,I),SIGMAL(2,I),I=1,NSOL),          
     1           RMOD(1),FASE(1),RMOD(2),FASE(2),                               
     2         (SIGMAG(1,I),SIGMAG(2,I),I=1,NSOL),(SIGMAG(3,I),I=1,NSOL)        
      CALL JWRITE(ISLSFE,EPSI,4*NSOL)                                           
      CALL JWRITE(ISLSFE,SIGMAG,6*NSOL)                                         
      CALL JWRITE(ISLSFE,SIGMAL,4*NSOL)                                         
      CALL JWRITE(ISLSFE,FASE,4)                                                
      CALL JWRITE(ISLSFE,RMOD,4)                                                
10    CONTINUE                                                                  
      IF(IFLMR.EQ.0)GOTO 50                                                     
 500  CONTINUE                                                                  
      RETURN                                                                    
900   CALL JPOSRL(ISLELM,-4)                                                    
      RETURN                                                                    
100   FORMAT(///40X,'***  SFORZI NEGLI ELEMENTI DI PANNELLO ***'///             
     1  1X,'ID.ELEM  P.INTEG',                                                  
     2 9X,'COMP. REALE',10X,'COMP. IMMAG.',//                                   
     2 22X,'SIGMA',6X,' TAU ',8X,'SIGMA',6X,' TAU ',6X,'MODULO',6X,             
     3 ' FASE ',6X,'MODULO',6X,' FASE '//)                                      
110   FORMAT(/(2I7,1X,'L',8(2X,E10.4))/16X,'G',4(2X,E10.4)/                     
     1                                 16X,'G',4(10X,2E10.4))                   
111   FORMAT(7X,I7,1X,'L',8(2X,E10.4)/16X,'G',4(2X,E10.4)/                      
     1                                 16X,'G',4(10X,2E10.4))                   
      END                                                                       
C@ELT,I ANBA*ANBA.WRSOLE                                                        
C*************************         WRSOLE         **********************        
      SUBROUTINE WRSOLE                                                         
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      DIMENSION DV(1)                                                           
      EQUIVALENCE (IV(1),DV(1))                                                 
      CHARACTER*12 STRNG                                                        
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
C                                                                               
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL READFF(ISYM,0,NAUTSL,*101,*101)                                      
      CALL CHARFF(*103,STRNG,0,LENFL)                                           
      IF(STRNG(1:4).EQ.'FINE' .OR. STRNG(1:5).EQ.'TUTTI') THEN                  
        CALL JOPEN('*SLE    ',ISLSLE,1)                                         
        CALL JREAD(ISLSLE,NAUTSL,1)                                             
        CALL JCLOSE(ISLSLE)                                                     
        CALL JALLOC(*2000,'ISELEZ  ','INT.',1,NAUTSL      ,JISELE)              
        DO 15 INAU=1,NAUTSL                                                     
          IV(JISELE+INAU-1)=INAU                                                
15      CONTINUE                                                                
      ELSE                                                                      
        CALL JALLOC(*2000,'ISELEZ  ','INT.',1,NAUTSL      ,JISELE)              
        IADV=1                                                                  
        DO 10 INAU=0,NAUTSL-1                                                   
          CALL INTGFF(*101,IV(JISELE+INAU),IADV,LENFL)                          
          IADV=0                                                                
10      CONTINUE                                                                
      END IF                                                                    
 900  FORMAT( )                                                                 
      CALL INTEST                                                               
      WRITE(IOUT,666)NAUTSL,(IV(JISELE+INAU),INAU=0,NAUTSL-1)                   
666   FORMAT(////////39X,'***  PARAMETRI SELEZIONE AUTOVETTORI   ***'//         
     128X,'N.RO AUTOVETTORI ',34('.'),' :',I12/                                 
     128X,'N.RO AUTOVALORE  ',34('.'),' :',10I3/                                
     110(80X,' :',10I3/))                                                       
      NEMPSI=NEQV-NPSI                                                          
      NEMPSI=NEQV                                                               
      NE2=NEMPSI+NEMPSI                                                         
      CALL JALLOC(*2000,'AUTVET  ','D.P.',1,NE2*2       ,JAUTVT)                
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI     ,JLABEL)                  
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JOPEN('*SNO    ',ISLSNO,1)                                           
      CALL JREAD(ISLSNO,NNSEL,1)                                                
      CALL JALLOC(*2000,'NODSEL  ','INT.',1,NNSEL     ,JNODSL)                  
      IF(NNSEL.NE.O) CALL JREAD(ISLSNO,IV(JNODSL),NNSEL)                        
      CALL JCLOSE(ISLSNO)                                                       
      CALL JOPEN('*SLE    ',ISLSLE,1)                                           
C     CALL JPOSRL(ISLSLE,1)                                                     
C                                                                               
      CALL PRTSLE(NAUTSL,IV(JISELE),ISLSLE,DV(JAUTVT),                          
     2            IV(JLABEL),NNODI,IV(JNUME),NEQV,IV(JNUINT),                   
     3            IV(JNODSL),NNSEL,NCS)                                         
C                                                                               
      CALL JCLOSE(ISLSLE)                                                       
      RETURN                                                                    
C                                                                               
103   WRITE(IOUT,104)                                                           
104   FORMAT(' ***   AVVISO (WRSOLE) : ERRORE NEI DATI PER LA STAMPA'           
     *      ,' DEGLI AUTOVETTORI   ***')                                        
      goto  2000                                                                
101   WRITE(IOUT,102)                                                           
102   FORMAT(' ***   AVVISO (WRSOLE) : ERRORE IN LETTURA SCHEDA DATI'           
     *      ,' DEGLI AUTOVETTORI   ***')                                        
 2000 CALL JHALT                                                                
      END                                                                       

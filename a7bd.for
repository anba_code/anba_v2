C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C----------------------    BLOCK DATA   --------------------------------        
      BLOCK DATA  GDBCDT                                                        
CC                                                                              
      IMPLICIT INTEGER (A-Z)                                                    
CC                                                                              
      PARAMETER (MAXMEM=4000000)                                                
      COMMON/JCRMNG/MAXDIM,IDAT,ITAB,NVAR,NWDEL,NWN,NWT(7),NDMAX,LUTAB,         
     *              IFMSG,IPRNT,LUPUN,IAPACK,LOCV,MAXWD                         
CC                                                                              
C-UNI COMMON IV(1)                                                              
      COMMON IV(MAXMEM)                                                         
CC                                                                              
C---- INITIALIZATION ---------------------------------------------------        
CC                                                                              
CC                                                                              
      DATA MAXDIM/MAXMEM/,IDAT/0/,ITAB/1/,NVAR/0/,NWDEL/0/,NWN/2/,              
     *     NWT/1,1,2,2,1,4,1/,NDMAX/3/,LUTAB/9/,IFMSG/6/,IPRNT/6/               
     *     ,LUPUN/7/,IAPACK/0/,MAXWD/0/                                         
CC                                                                              
CC    LUPUN = NWN + 2 + NDMAX                                                   
CC    LUTAB .GE. NWN + 4 + NDMAX                                                
CC                                                                              
      END                                                                       

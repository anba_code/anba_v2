C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MATGIU                                                        
C*************************          MATGIU          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATGIU(ELMAT,NRELMT,VMASS,IPIAZ)                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      logical dump,incore                                                       
      DIMENSION ELMAT(NRELMT,1),ICON(2),X(4),VMASS(1),IPIAZ(1)                  
      COMMON/JFTNIO/ INPT,IOUT,ISYM,dump(22),incore,iloca                       
      COMMON /GIUNZ/ G,RO,RL,T,DELTAX,DELTAY,OMEGA                              
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
C                                                                               
C                                                                               
C     CALCOLA LA MATRICE DI RIGIDEZZA DELLE GIUNZIONI CON DUE NODI              
C                                                                               
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,2)                                                 
      CALL JREAD(ISLELM,X,8)                                                    
      CALL JREAD(ISLELM,G,8)                                                    
      IF(ITIP.NE.2)GOTO 1200                                                    
C                                                                               
      IF(IFLMR.EQ.0) GOTO 20                                                    
C                                                                               
      OMEGA=.5*(X(1)*X(4)-X(2)*X(3))                                            
      OMEGA2=OMEGA*2.                                                           
      DELTAX=X(3)-X(1)                                                          
      DELTAY=X(4)-X(2)                                                          
      IELMAT=NCON*NCS+NPSI                                                      
      DO 10 I=1,IELMAT                                                          
      DO 10 J=1,IELMAT                                                          
10    ELMAT(I,J)=0.D0                                                           
      COST=(G*T)/RL                                                             
      ELMAT(3,3)=COST                                                           
      ELMAT(3,6)=-COST                                                          
      ELMAT(6,6)=COST                                                           
      ELMAT(7,7)=COST*DELTAX*DELTAX                                             
      ELMAT(7,8)=COST*DELTAX*DELTAY                                             
      ELMAT(8,8)=COST*DELTAY*DELTAY                                             
      ELMAT(7,12)=COST*DELTAX*OMEGA2                                            
      ELMAT(8,12)=COST*DELTAY*OMEGA2                                            
      ELMAT(12,12)=COST*OMEGA2*OMEGA2                                           
      ELMAT(3,7)=-COST*DELTAX                                                   
      ELMAT(3,8)=-COST*DELTAY                                                   
      ELMAT(3,12)=-COST*OMEGA2                                                  
      ELMAT(6,7)=COST*DELTAX                                                    
      ELMAT(6,8)=COST*DELTAY                                                    
      ELMAT(6,12)=COST*OMEGA2                                                   
      ELMAT(6,3)=ELMAT(3,6)                                                     
      ELMAT(8,7)=ELMAT(7,8)                                                     
      ELMAT(12,7)=ELMAT(7,12)                                                   
      ELMAT(12,8)=ELMAT(8,12)                                                   
      ELMAT(7,3)=ELMAT(3,7)                                                     
      ELMAT(8,3)=ELMAT(3,8)                                                     
      ELMAT(12,3)=ELMAT(3,12)                                                   
      ELMAT(7,6)=ELMAT(6,7)                                                     
      ELMAT(8,6)=ELMAT(6,8)                                                     
      ELMAT(12,6)=ELMAT(6,12)                                                   
      CALL JWRITE(ISLELS,DELTAX,6)                                              
C                                                                               
 20   XM=(X(1)+X(3))/2.                                                         
      YM=(X(2)+X(4))/2.                                                         
      DP=RO*RL*T                                                                
      CALL ASMASS(VMASS,DP,XM,YM)                                               
      RETURN                                                                    
1200   WRITE(IOUT,1300)ITIP,IDEL,NCON                                           
1300   FORMAT(/' *** AVVISO (MATGIU): I DATI RELATIVI ALL''ELEMENTO'            
     1 ,' NON SONO CONFORMI'/,24X,'ITIP= ',I6,'   IDEL= ',I6,                   
     1 'NCON=',I6,'   ***'/)                                                    
      CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C@ELT,I ANBA*ANBA.MATLAM                                                        
C*************************          MATLAM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATLAM(ELMAT,NRELMT,VMASS,IPIAZ)                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3         
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++              
c++++++++++++  31agosto +++++++++++++++++++++++++++++++++++++++++++             
c  Ho ripristinato il common ELPI1 originale [ togliendo CDLTZ(6) ]             
      COMMON /ELPI1/  LREC,NPINT,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),             
     1                T2(3,3),D(6,6),W,DEL,COE(2)                               
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
      COMMON /ELPI2/  INT1,INT2,TETA,DEN,alfort,IFLTET                          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),IAUTCN,             
     1                IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                    
      DIMENSION PUNTI(2,25),PESI(25),XY(2,8),ICON(8),ELMAT(NRELMT,1)            
     1         ,DG(6,6),SPES(4),XYLM(2,4),XYAD(4),ICORR(18),VMASS(1),           
     2          DT(6,6),IPIAZ(1)                                                
      DATA ICORR/3,4,2,1,3,6,4,2,5,1,3,7,8,4,2,6,5,1/                           
C                                                                               
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++             
c++++++++++++++   31agosto ++++++++++++++++++++++++++++++++++++++++             
c     Ripristino LREC originale [ tolgo + 12 ]                                  
      LREC=190                                                                  
C++++++++++++++++++++++                                                         
      CALL CAPPA(ELMAT,NRELMT)                                                  
C                                                                               
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,NCON)                                              
      NCOPP=NCON/2                                                              
      CALL JREAD(ISLELM,SPES,NCOPP*2)                                           
      CALL JREAD(ISLELM,XY,NCON*4)                                              
      CALL JREAD(ISLELM,nwr,1)                                                  
      CALL JREAD(ISLELM,INT1,nwr)                                               
      DO 5 K=1,6                                                                
      CALL JREAD(ISLELM,DG(1,K),12)                                             
 5    CONTINUE                                                                  
      CALL DAT2D(NPINT,INT1,INT2,PUNTI,PESI)                                    
      CALL JWRITE(ISLELS,LREC,2)                                                
      JUMP=0                                                                    
      IGOTO=NCOPP-1                                                             
      GOTO (101,102,103),IGOTO                                                  
 101  IBASE=0                                                                   
      XYAD(1)=-1.D0                                                             
      XYAD(2)= 1.D0                                                             
      GOTO 104                                                                  
 102  IBASE=4                                                                   
      XYAD(1)=-1.D0                                                             
      XYAD(2)= 0.D0                                                             
      XYAD(3)= 1.D0                                                             
      GOTO 104                                                                  
 103  IBASE=10                                                                  
      XYAD(1)=-1.D0                                                             
      XYAD(2)=-.3333333333D0                                                    
      XYAD(3)= .3333333333D0                                                    
      XYAD(4)= 1.D0                                                             
 104  CONTINUE                                                                  
      DO 7 I=1,NCOPP                                                            
      SPES(I)=SPES(I)/2.D0                                                      
      DO 7 K=1,2                                                                
      XYLM(K,I)=(XY(K,ICORR(IBASE+I))+XY(K,ICORR(IBASE+I+NCOPP)))/2.D0          
 7    CONTINUE                                                                  
      DO 8 I=1,NCOPP                                                            
      CALL LMAT(XYLM,NCOPP,XYAD(I),SINLM,COSLM)                                 
      XY(1,ICORR(IBASE+I))=XYLM(1,I)+SPES(I)*SINLM                              
      XY(2,ICORR(IBASE+I))=XYLM(2,I)-SPES(I)*COSLM                              
      XY(1,ICORR(IBASE+I+NCOPP))=XYLM(1,I)-SPES(I)*SINLM                        
      XY(2,ICORR(IBASE+I+NCOPP))=XYLM(2,I)+SPES(I)*COSLM                        
 8    CONTINUE                                                                  
      DO 10 NPT=1,NPINT                                                         
      CALL FORLAM(PUNTI(1,NPT),PUNTI(2,NPT),NCOPP,ENNE)                         
      CALL JAC2D(ENNE,NCON,XY,CPI,DA,BETA)                                      
ccc      W=PESI(NPT)*DA                                                         
ccc      DP=W*DEN                                                               
                                                                                
      DA=PESI(NPT)*DA                                                           
      DP=DA*DEN                                                                 
      DEL=CDC(3)+CDC(4)*CPI(2)-CDC(5)*CPI(1)                                    
      COE(1)=CDC(1)-CDC(6)*CPI(2)                                               
      COE(2)=CDC(2)+CDC(6)*CPI(1)                                               
      DP=DA*DEN*DEL                                                             
      W=DA/DEL                                                                  
                                                                                
      CALL ASMASS(VMASS,DP,CPI(1),CPI(2))                                       
      ALFA=BETA                                                                 
C                                                                               
      SINA=DSIN(ALFA)                                                           
      COSA=DCOS(ALFA)                                                           
      SINA2=SINA*SINA                                                           
      COSA2=COSA*COSA                                                           
      SIN2A=DSIN(2.D0*ALFA)                                                     
      COS2A=DCOS(2.D0*ALFA)                                                     
C                                                                               
      T1(1,1)= COSA                                                             
      T1(1,2)=-SINA                                                             
      T1(1,3)= 0.D0                                                             
      T1(2,1)= SINA                                                             
      T1(2,2)= COSA                                                             
      T1(2,3)= 0.D0                                                             
      T1(3,1)= 0.D0                                                             
      T1(3,2)= 0.D0                                                             
      T1(3,3)= 1.D0                                                             
C                                                                               
      T2(1,1)= COSA2                                                            
      T2(1,2)= SINA2                                                            
      T2(1,3)=-SIN2A                                                            
      T2(2,1)= SINA2                                                            
      T2(2,2)= COSA2                                                            
      T2(2,3)= SIN2A                                                            
      T2(3,1)= SIN2A/2.D0                                                       
      T2(3,2)=-SIN2A/2.D0                                                       
      T2(3,3)= COS2A                                                            
      DO 14 I=1,6                                                               
      DO 14 K=1,6                                                               
      D(I,K)=DG(I,K)*W                                                          
 14   CONTINUE                                                                  
C                                                                               
      CALL DABCT3(T1,D(1,1),6,T1)                                               
      CALL DABCT3(T1,D(1,4),6,T2)                                               
      CALL DABCT3(T2,D(4,1),6,T1)                                               
      CALL DABCT3(T2,D(4,4),6,T2)                                               
      CALL JWRITE(ISLELS,ENNE,LREC)                                             
      JUMP=JUMP+LREC                                                            
 10   CONTINUE                                                                  
      IF(IFLMR.EQ.0)RETURN                                                      
      IELM1=NCON*NCS                                                            
      IELMAT=NCON*NCS+NPSI                                                      
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      DO 20 NPT=1,NPINT                                                         
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 30 K=1,NCON                                                            
      JK=(K-1)*NCS+1                                                            
      DO 40 I=1,NCON                                                            
      JI=(I-1)*NCS+1                                                            
      CALL CAP1(D,ENNE(1,I),ENNE(1,K),ELMAT(JI,JK),NRELMT,DEL,COE,              
     1          CDC,IGEOM)                                                      
 40   CONTINUE                                                                  
      JI=IELM1+1                                                                
      CALL CAP2(D,ENNE(1,K),CPI,ELMAT(JI,JK),NRELMT)                            
 30   CONTINUE                                                                  
 20   CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICI H-CIRCONFLESSO ED R-CIRCONFLESSO TRASP.          
C                                                                               
      DO 250 I=1,IELM1                                                          
      CALL JWRITE(ISLMTF,ELMAT(1,I),IELMAT*2)                                   
 250  CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE FLESS',ELMAT,                            
     *             NRELMT,IELMAT,IELM1,1,IPIAZ)                                 
      DO 300 I1=1,IELM1                                                         
      DO 300 K1=I1,IELM1                                                        
      H=ELMAT(I1,K1)-ELMAT(K1,I1)                                               
      ELMAT(I1,K1)=-H                                                           
      ELMAT(K1,I1)= H                                                           
300   CONTINUE                                                                  
      DO 302 I=1,NPSI                                                           
      I1=IELM1+I                                                                
      DO 302 I2=1,IELM1                                                         
      ELMAT(I2,I1)=ELMAT(I1,I2)                                                 
      ELMAT(I1,I2)=-ELMAT(I1,I2)                                                
302   CONTINUE                                                                  
CC      WRITE(IOUT,1408)                                                        
      DO 350 I=1,IELMAT                                                         
CC      WRITE(IOUT,1406)(ELMAT(I,L),L=1,IELMAT)                                 
      CALL JWRITE(ISLMTN,ELMAT(1,I),IELMAT*2)                                   
350   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE TNOTO',ELMAT,                            
     *             NRELMT,IELMAT,IELMAT,1,IPIAZ)                                
C                                                                               
C     FINE COSTRUZIONE MATRICE TERMINE NOTO                                     
C                                                                               
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      DO 55 K=1,IELM1                                                           
      DO 55 I=K,IELM1                                                           
      ELMAT(I,K)=0.D0                                                           
 55   CONTINUE                                                                  
      DO 60 NPT=1,NPINT                                                         
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 70 K=1,NCON                                                            
      JK=(K-1)*NCS+1                                                            
      DO 70 I=K,NCON                                                            
      JI=(I-1)*NCS+1                                                            
      CALL CAP3(D,ENNE(1,I),ENNE(1,K),ELMAT(JI,JK),NRELMT)                      
 70   CONTINUE                                                                  
 60   CONTINUE                                                                  
      DO 80 K=1,IELM1                                                           
      DO 80 I=K,IELM1                                                           
      ELMAT(K,I)=ELMAT(I,K)                                                     
 80   CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICE K-TILDE                                          
C                                                                               
      DO 450 I=1,IELM1                                                          
      CALL JWRITE(ISLMTF,ELMAT(1,I),IELM1*2)                                    
450   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE MASSA',ELMAT,                            
     *             NRELMT,IELM1,IELM1,1,IPIAZ)                                  
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      CALL JPOSRL(ISLELS,-JUMP)                                                 
      DO 90 NPT=1,NPINT                                                         
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      DO 100 I=1,NCON                                                           
      JI=(I-1)*NCS+1                                                            
      DO 110 K=I,NCON                                                           
      JK=(K-1)*NCS+1                                                            
      CALL CAP4(D,ENNE(1,I),ENNE(1,K),ELMAT(JI,JK),NRELMT,                      
     1          DEL,COE,CDC,IGEOM,DT)                                           
 110  CONTINUE                                                                  
      JK=IELM1+1                                                                
      CALL CAP5(D,ENNE(1,I),CPI,ELMAT(JI,JK),NRELMT                             
     1          ,DEL,COE,CDC,IGEOM)                                             
 100  CONTINUE                                                                  
      JI=IELM1+1                                                                
      JK=IELM1+1                                                                
      CALL CAP6(D,CPI,ELMAT(JI,JK),NRELMT)                                      
 90   CONTINUE                                                                  
      DO 130 I=1,IELMAT                                                         
      DO 130 K=I,IELMAT                                                         
      ELMAT(K,I)=ELMAT(I,K)                                                     
 130  CONTINUE                                                                  
C                                                                               
C     FINE COSTRUZIONE MATRICE DI RIGIDEZZA                                     
C                                                                               
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MATPAN                                                        
C*************************          MATPAN          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATPAN(ELMAT,NRELMT,VMASS,IPIAZ)                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODL,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3             
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PANN/  TICK,DEN,INTORD                                            
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /PNTINT/ IFLPI                                                     
      DIMENSION PUNTI(21),PESI(21),B(4,5),ICON(4),SHAPE(4,5),IPIAZ(1),          
     1          Z(2,30),XY(8),D(2,2),ELMAT(NRELMT,1),DS(5),VMASS(1)
C                                                                               
C                                                                               
C     COEFF. PUNTI INTEGRAZIONE                                                 
C                                                                               
C                                                                               
      DATA PUNTI/                                                               
     *  0.D0,                                                                   
     * -.5773502691896D0,.5773502691896D0,                                      
     * -.7745966692415D0,0.D0,.7745966692415D0,                                 
     * -.8611363115941D0,-.3399810435849D0,.3399810435849D0,                    
     *  .8611363115941D0,                                                       
     * -.90617985D0,-.53846931D0,0.D0,.53846931D0,.90617985D0,                  
     * 0.D0,                                                                    
     * -1.D0,1.D0,                                                              
     * -1.D0,0.D0,1.D0/                                                         
      DATA  PESI/                                                               
     * 2.D0,                                                                    
     * 1.D0,1.D0,                                                               
     *.5555555555556D0,.8888888888889D0,.5555555555556D0,                       
     *.3478548451375D0,.6521451548625D0,.6521451548625D0,                       
     *.3478548451375D0,                                                         
     *.23692689D0,.47862867D0,.568888888889D0,.47862867D0,.23692689D0,          
     * 2.D0,                                                                    
     * 1.D0,1.D0,                                                               
     * .3333333333D0,1.3333333333D0,.3333333333D0/

      DATA MAXPUN/5/                                                            
C                                                                               
C                                                                               
C     SUBROUTINE PER IL CALCOLO DELLE MATRICI DI RIGIDEZZA                      
C     DEI PANNELLI CON 2,3 O 4 NODI                                             
C                                                                               
C                                                                               
C                                                                               
      CALL JREAD(ISLELM,ITIP,4)
      CALL JREAD(ISLELM,ICON,NCON)                                              
      CALL JREAD(ISLELM,XY,4*NCON)                                              
      CALL JREAD(ISLELM,TICK,5)                                                 
      DO 50 K=1,2                                                               
      CALL JREAD(ISLELM,D(1,K),4)                                               
50    CONTINUE
      CALL JREAD(ISLELM,Z0,2)

      write(iout,*) itip,idel,ncon
      write(iout,'(i8,2f8.3)')(icon(iw),xy(iw*2-1),xy(iw*2),iw=1,ncon)
      write(iout,*)TICK,DEN,INTORD
      write(iout,*) ((d(i,k),i=1,2),k=1,2)
      write(iout,*) ' z0',z0                                               

      call jread(islelm,nskip,1)                                                
      call jposrl(islelm,nskip)                                                 
      IF(INTORD.GT.MAXPUN) INTORD=MAXPUN                                        
      IP0=0                                                                     
      IF(IFLPI.NE.0) THEN                                                       
      IP0=IP0+15                                                                
      IF(INTORD.GT.3) IP0=0                                                     
      END IF                                                                    
      IOR=INTORD*(INTORD-1)/2+IP0                                               
      IELMAT=NCS*NCON+NPSI                                                      
      NCSCON=NCS*NCON                                                           
      IOR1=IOR                                                                  
      DO 20 IN=1,INTORD                                                         
      IOR1=IOR1+1                                                               
      IND=(IN-1)*NPSI+1               
cc      print*,'punti',PUNTI(IOR1),ind
      CALL BMAT(XY,NCON,PUNTI(IOR1),XX,YY,B(1,IN),SHAPE(1,IN),Z(1,IND)          
     1  ,DS(IN),DX,DY)                              
      PD=PESI(IOR1)*DEN*DS(IN)
      CALL ASMASS(VMASS,PD,XX,YY)                                               
      CALL JWRITE(ISLELS,B(1,IN),NCON*2)                                        
      CALL JWRITE(ISLELS,SHAPE(1,IN),NCON*2)                                    
cc      print*,(b(iol,in),iol=1,ncon)                                               
cc      print*,(shape(iol,in),iol=1,ncon)
      DO 10 IS=1,NPSI                                                           
      CALL JWRITE(ISLELS,Z(1,IND+IS-1),4)                                       
cc      print*,(z(iol,ind+is-1),iol=1,2)
10    CONTINUE     
                          
20    CONTINUE                                                                  
      IF(IFLMR.EQ.0)RETURN                                                      
      DO 60 I=1,IELMAT                                                          
      DO 60 J=1,IELMAT                                                          
      ELMAT(J,I)=0.D0                                                           
60    CONTINUE                                                                  
C                                                                               
C     COSTRUZIONE DELLE MATRICI H-CIRCONF. ED R-TILDE TRASP.                    
C                                                                               
      IOR1=IOR                                                                  
      DO 200 IN=1,INTORD                                                        
      IOR1=IOR1+1                                                               
      PESIDS=PESI(IOR1)*DS(IN)                                                  
      DO 201 K=1,NCON                                                           
      END1=SHAPE(K,IN)*D(1,1)*PESIDS                                            
      END2=SHAPE(K,IN)*D(2,1)*PESIDS                                            
C                                                                               
C     MATRICE R-TILDE TRASPOSTA                                                 
C                                                                               
      I1=NCON*NCS                                                               
      I2=K*NCS                                                                  
      DO 202 I=1,NPSI                                                           
      I1=I1+1                                                                   
      IND=(IN-1)*NPSI+I                                                         
      ELMAT(I1,I2)=ELMAT(I1,I2)+END1*Z(1,IND)+END2*Z(2,IND)                     
      ELMAT(I2,I1)=ELMAT(I1,I2)                                                 
202   CONTINUE                                                                  
C                                                                               
C     MATRICE H-CIRCONFL.                                                       
C                                                                               
      I1=K*NCS                                                                  
      DO 203 I=1,NCON                                                           
      I2=I*NCS                                                                  
      T=B(K,IN)*SHAPE(I,IN)*D(2,1)*PESIDS                                       
      ELMAT(I1,I2)=ELMAT(I1,I2)+T                                               
203   CONTINUE                                                                  
201   CONTINUE                                                                  
200   CONTINUE                                                                  
      DO 250 I=1,NCSCON                                                         
      CALL JWRITE(ISLMTF,ELMAT(1,I),IELMAT*2)                                   
250   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE FLESS',ELMAT,                            
     *             NRELMT,NCSCON,IELMAT,1,IPIAZ)                                
C                                                                               
C         CALCOLO DELLA MATRICE TERMINI NOTI                                    
C         SI MODIFICA LA PRECEDENTE                                             
C                                                                               
      DO 300 I=1,NCON                                                           
      I1=I*NCS                                                                  
      DO 301 K=I,NCON                                                           
      I2=K*NCS                                                                  
      H=ELMAT(I1,I2)-ELMAT(I2,I1)                                               
      ELMAT(I1,I2)=-H                                                           
      ELMAT(I2,I1)= H                                                           
301   CONTINUE                                                                  
300   CONTINUE                                                                  
      I1=NCON*NCS                                                               
      DO 302 I=1,NPSI                                                           
      I1=I1+1                                                                   
      DO 303 K=1,NCON                                                           
      I2=K*NCS                                                                  
      ELMAT(I1,I2)=-ELMAT(I1,I2)                                                
303   CONTINUE                                                                  
302   CONTINUE                                                                  
      DO 350 I=1,IELMAT                                                         
      CALL JWRITE(ISLMTN,ELMAT(1,I),IELMAT*2)                                   
350   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE TNOTO',ELMAT,                            
     *             NRELMT,IELMAT,IELMAT,1,IPIAZ)                                
C                                                                               
C         COSTRUZIONE MTRICE K-TILDE                                            
C                                                                               
      DO 460 I=1,NCSCON                                                         
      DO 460 J=1,NCSCON                                                         
      ELMAT(J,I)=0.D0                                                           
460   CONTINUE                                                                  
      IOR1=IOR                                             
      DO 400 IN=1,INTORD                                                        
      IOR1=IOR1+1                                                               
      PESIDS=PESI(IOR1)*DS(IN)                                                  
      DO 401 I=1,NCON                                                           
      I1=I*NCS                                                                  
      DO 402 J=1,NCON                                                           
      I2=J*NCS                                                                  
      ELMAT(I1,I2)=ELMAT(I1,I2)+SHAPE(I,IN)*SHAPE(J,IN)*PESIDS*D(1,1)           
C     ELMAT(I2,I1)=ELMAT(I1,I2)                                                 
402   CONTINUE                                                                  
401   CONTINUE                                                                  
400   CONTINUE                                                                  
      DO 450 I=1,NCSCON                                                         
      CALL JWRITE(ISLMTF,ELMAT(1,I),NCSCON*2)                                   
450   CONTINUE                                                                  
      IF(DUMP(1)) CALL DWRITM('MATRICE MASSA',ELMAT,                            
     *             NRELMT,NCSCON,NCSCON,1,IPIAZ)                                
C                                                                               
C     COSTRUZIONE MATRICE DELL'ELEMENTO                                         
C                                                                               
      DO 160 I=1,IELMAT                                                         
      DO 160 J=1,IELMAT                                                         
      ELMAT(J,I)=0.D0                                                           
160   CONTINUE                                                                  
      IOR1=IOR                    
      DO 100 IN=1,INTORD                                                        
      IOR1=IOR1+1                                                               
      PESIDS=PESI(IOR1)*DS(IN)                                                  
      DO 101 I=1,NCON                                                           
      PBE=B(I,IN)*D(2,2)*PESIDS                                                 
      I1=I*NCS                                                                  
C                                                                               
C     COSTRUZIONE MATRICE  K-CIRCONFL.                                          
C                                                                               
      DO 102 K=I,NCON                                                           
      I2=K*NCS                                                                  
      ELMAT(I1,I2)=ELMAT(I1,I2)+PBE*B(K,IN)                                     
      ELMAT(I2,I1)=ELMAT(I1,I2)                                                 
102   CONTINUE                                                                  
      BTD1=B(I,IN)*D(2,1)*PESIDS                                                
      BTD2=B(I,IN)*D(2,2)*PESIDS                                                
      I1=I*NCS                                                                  
C                                                                               
C     COSTRUZIONE MATRICE  R-CIRCONFL.                                          
C                                                                               
      DO 103 K=1,NPSI                                                           
      I2=NCON*NCS+K                                                             
      IND=(IN-1)*NPSI+K                                                         
      ELMAT(I2,I1)=ELMAT(I2,I1)+BTD1*Z(1,IND)+BTD2*Z(2,IND)                     
      ELMAT(I1,I2)=ELMAT(I2,I1)                                                 
103   CONTINUE                                                                  
101   CONTINUE                                                                  
C                                                                               
C     COSTRUZIONE MATRICE  K-SEGNATA                                            
C                                                                               
      DO 104 J=1,NPSI                                                           
      IND=(IN-1)*NPSI+J                                                         
      DZ1=(D(1,1)*Z(1,IND)+D(1,2)*Z(2,IND))*PESIDS                              
      DZ2=(D(2,1)*Z(1,IND)+D(2,2)*Z(2,IND))*PESIDS                              
      I1=NCON*NCS+J                                                             
      DO 105 I=J,NPSI                                                           
      I2=NCON*NCS+I                                                             
      IND=(IN-1)*NPSI+I                                                         
      ELMAT(I1,I2)=ELMAT(I1,I2)+DZ1*Z(1,IND)+DZ2*Z(2,IND)                       
      ELMAT(I2,I1)=ELMAT(I1,I2)                                                 
105   CONTINUE                                                                  
104   CONTINUE                                                                  
100   CONTINUE                                                                  
150   CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.OUCASB                                                        
C*************************          OUCASB          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
c      SUBROUTINE OUCASB                                                        
c      REAL*8 DV(1)                                                             
c      LOGICAL DUMP,INCORE                                                      
c      COMMON IV(1)                                                             
c      EQUIVALENCE (IV(1),DV(1))                                                
c      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,         
c     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQAS,NST       
c      COMMON /JCRMNG/ MAXDIM                                                   
c      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                     
c      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,        
c     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,        
c     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                      
c     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                           
C                                                                               
c      do 8513 iopl=1,maxdim                                                    
c8513  iv(iopl)=0                                                               
c      MEMDIS=MAXDIM-5*MAXLRG-2*MAXDEL-MAXDEL*MAXDEL*2-NELMAX-NELEM             
c     *       -14*7-3-3*898                                                     
c      NEQAS=MEMDIS/(1+MAXLRG*2)                                                
c      IF(NEQAS.GT.NEQV-NPSI)NEQAS=NEQV-NPSI                                    
c      NBLOC=(NEQV-NPSI+NEQAS-1)/NEQAS                                          
c      MEMDIS=MAXDIM-MAXDEL-MAXDEL-NELEM-9*10-3*898                             
c      INE=(MEMDIS-NBLOC)/(NBLOC+NBLOC)                                         
c      CALL JALLOC(*2000,'IPIAZ   ',1,1,MAXDEL      ,JIPIAZ)                    
c      CALL JALLOC(*2000,'IVBL    ',1,1,MAXDEL      ,JIVBL )                    
c      CALL JALLOC(*2000,'LNBL    ',1,1,NBLOC       ,JLNBL )                    
c      CALL JALLOC(*2000,'MTEL    ',1,1,NBLOC*INE   ,JMTEL )                    
c      CALL JALLOC(*2000,'MTEQ    ',1,1,NBLOC*INE   ,JMTEQ )                    
c      CALL JALLOC(*2000,'IVORD   ',1,1,NELEM       ,JIVORD)                    
c      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                          
c      CALL JOPEN('*TAB    ',ISLTAB,1)                                          
c      CALL JOPEN('*TEM    ',ISLTEM,1)                                          
cC                                                                              
cC                                                                              
c      JLIM=JIVORD+NELEM-1                                                      
c      KK=JLIM-JIPIAZ                                                           
cc      CALL IZERO(IV(JIPIAZ),KK)                                               
c      NNEQ=NEQAS                                                               
cC                                                                              
c      CALL PRETAB(IV(JIPIAZ),IV(JIVORD),IV(JIVBL),IV(JLNBL),IV(JMTEL),         
c     1     IV(JMTEQ),NNEQ,NBLOC,INE)                                           
c      CALL JCLOSE(ISLTEM)                                                      
c      CALL JCLOSE(ISLVPZ)                                                      
c      CALL JCLOSE(ISLTAB)                                                      
cC                                                                              
c      CALL JCRERS                                                              
cC                                                                              
c      do 8514 iopl=1,maxdim                                                    
c8514  iv(iopl)=0                                                               
c      CALL JALLOC(*2000,'IPELM   ',1,1,NELEM      ,JIPELM)                     
c      CALL JOPEN('*NWD    ',ISLNWD,1)                                          
c      CALL JREAD(ISLNWD,IV(JIPELM),NELEM)                                      
c      CALL JCLOSE(ISLNWD)                                                      
c      MAXPZ=MAXLRG                                                             
c      IF(MAXLRG.LT.MAXDEL) MAXPZ=MAXDEL                                        
c      CALL JALLOC(*2000,'IPIAZ   ',1,1,MAXPZ       ,JIPIAZ)                    
c      CALL JALLOC(*2000,'IELEM   ',1,1,NELMAX      ,JIELEM)                    
c      CALL JALLOC(*2000,'LHED    ',1,1,MAXLRG      ,JLHED )                    
c      CALL JALLOC(*2000,'KDEST   ',1,1,MAXDEL      ,JKDEST)                    
c      CALL JALLOC(*2000,'LDEST   ',1,1,MAXDEL      ,JLDEST)                    
c      CALL JALLOC(*2000,'IHED    ',1,1,MAXLRG       ,JIHED )                   
c      CALL JALLOC(*2000,'VETT    ',3,1,MAXLRG      ,JVETT )                    
c      CALL JALLOC(*2000,'ELMAT   ',3,1,MAXDEL*MAXDEL,JELMAT)                   
c      CALL JALLOC(*2000,'ASSMAT  ',3,1,MAXLRG*NEQAS,JASMAT)                    
c      CALL JALLOC(*2000,'KHED    ',1,1,NEQAS       ,JKHED )                    
c      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                          
c      CALL JOPEN('*ASM    ',ISLASM,1)                                          
c      CALL JOPEN('*TAB    ',ISLTAB,1)                                          
cC                                                                              
cC                                                                              
c      JLIM=JKHED+NEQAS-1                                                       
c      KK=JLIM-JIPIAZ                                                           
c      JASPSI=JASMAT                                                            
cc      CALL IZERO(IV(JIPIAZ),KK)                                               
c      NRASMT=NEQAS                                                             
cC                                                                              
c      CALL JPRMEM                                                              
c      CALL JPRTAB                                                              
c      CALL ASSFRO(IV(JIELEM),IV(JIPIAZ),IV(JKHED),IV(JLHED),                   
c     1 IV(JKDEST),IV(JLDEST),IV(JIHED),DV(JASMAT),NRASMT,DV(JVETT),            
c     2 DV(JELMAT),MAXDEL,DV(JASPSI),NPSI,NEQAS,IV(JIPELM))                     
cC                                                                              
c      CALL JCLOSE(ISLTAB)                                                      
c      CALL JCLOSE(ISLASM)                                                      
c      CALL JCLOSE(ISLVPZ)                                                      
cC                                                                              
cC                                                                              
c      RETURN                                                                   
cC                                                                              
c2000  CALL JHALT                                                               
c      STOP                                                                     
C                                                                               
c      END                                                                      
C@ELT,I ANBA*ANBA.MATRIX                                                        
C*************************          MATRIX          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATRIX(ITIP,IDEL,ELMAT,ELMAT1,ELMAT2,ELMAT3,                   
     1                  NRELMT,VMASS,IPIAZ)                                     
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      logical dump,incore                                                       
      DIMENSION ELMAT(NRELMT,1),VMASS(1),ELMAT1(NRELMT,1),                      
     1          ELMAT2(NRELMT,1),ELMAT3(NRELMT,1),IPIAZ(1)                      
       COMMON/JFTNIO/INPT,IOUT,ISYM,dump(22),incore,iloca                       
C                                                                               
C     SUBROUTINE PER LA CHIAMATA DELLE SUBROUTINE CHE                           
C     PREPARANO LE MATRICI DI RIGIDEZZA DEGLI ELEMENTI                          
C                                                                               
C                                                                               
C                                                                               
C     CORRENTE 100                                                              
C     GIUNZIONE 200                                                             
C     PANNELLI A 2,3 O 4 NODI 300                                               
C     LAMINE 400                                                                
C     ELEMENTI PIANI 500                                                        
C     ELEMENTI STRATIFICATI 600                                                 
C                                                                               
      IF (ITIP.GT.8)GOTO 1000                                                   
      GOTO(100,200,300,400,500,10,10,600),ITIP                                  
100   CALL MATCOR(ELMAT,NRELMT,VMASS)                                           
      RETURN                                                                    
200   CALL MATGIU(ELMAT,NRELMT,VMASS,IPIAZ)                                     
      RETURN                                                                    
300   CALL MATPAN(ELMAT,NRELMT,VMASS,IPIAZ)                                     
      RETURN                                                                    
400   CALL MATLAM(ELMAT,NRELMT,VMASS,IPIAZ)                                     
      RETURN                                                                    
500   CALL MATELP(ELMAT,NRELMT,VMASS,IPIAZ)                                     
      RETURN                                                                    
600   CALL MATSTR(ELMAT1,ELMAT2,ELMAT3,NRELMT,VMASS,IPIAZ)                      
      RETURN                                                                    
 10   RETURN                                                                    
C                                                                               
C                                                                               
1000  WRITE(IOUT,2000)IDEL,ITIP                                                 
2000  FORMAT(' *** AVVISO (MATRIX): NON RICONOSCIUTO L''ELEMENTO N.RO',         
     1  I8,'  DI TIPO',I8,'   ***')                                             
      CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C@ELT,I ANBA*ANBA.PREFRO                                                        
C*************************          PREFRO          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PREFRO(MINRIG,NEQV,ISLMRG,MAXLRG,NSTO,NPOS,MAXDEL,             
     * IGEOM)                                                                   
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      DIMENSION  MINRIG(1)                                                      
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON/JCRMNG/MAXDIM                                                      
C                                                                               
      CALL JWRITE(ISLMRG,MINRIG,NEQV)                                           
      CALL SHLSRT(NEQV,MINRIG,R,1,1,1)                                          
      K=1                                                                       
      MAXLRG=0                                                                  
      M=1                                                                       
      N=MINRIG(NEQV)                                                            
      DO 80 I=1,N                                                               
      L=0                                                                       
100   IF(MINRIG(K).GT.I)GOTO 90                                                 
      K=K+1                                                                     
      L=L+1                                                                     
      GOTO 100                                                                  
90    M=M+L-1                                                                   
      MINRIG(I)=M                                                               
      IF(M.GT.MAXLRG) MAXLRG=M                                                  
80    CONTINUE                                                                  
      IF(N.EQ.NEQV)GOTO 110                                                     
      N=N+1                                                                     
      DO 120 I=N,NEQV                                                           
      M=M-1                                                                     
      MINRIG(I)=M                                                               
120   CONTINUE                                                                  
110   CONTINUE                                                                  
      CALL INTEST                                                               
C                                                                               
      CALL JPOSAB(ISLMRG,1)                                                     
      CALL JREAD(ISLMRG,MINRIG,NEQV)                                            
      NSTO=1                                                                    
      DO 10 I=2,NEQV                                                            
      NSTO=NSTO+I-MINRIG(I)+1
      write(iout,*) i,minrig(i),nsto                                                   
      MINRIG(I)=NSTO                                                            
 10   CONTINUE                                                                  
C                                                                               
      MEMDIS=MAXDIM-NSTO*2-NEQV*9-MAXDEL*5                              
      inm=NSTO*2+NEQV*9+MAXDEL*5
      write(iout,*)'STIMA SPAZIO RICHIESTO PER SOLUZIONE SISTEMA'
      write(iout,*)'SOLUZIONE IN MEMORIA   ',INM                                   
      print*,'SOLUZIONE IN MEMORIA   ',INM                                   
      IF(IGEOM.EQ.1) then
         MEMDIS=MEMDIS+NSTO*2-NEQV*NEQV*4-NEQV*12
      inm=NEQV*9+MAXDEL*5+NEQV*NEQV*4+NEQV*12
      write(iout,*)'SOLUZIONE FUORI MEMORIA',INM                                   
      print*,'SOLUZIONE FUORI MEMORIA',INM                                   
      endif                            
      write(iout,*)'MEMORIA DISPONIBILE    ',MAXDIM,nsto,neqv,maxdel                                   
      print*,'MEMORIA',mAXDIM,nsto,neqv,maxdel                                   
      
      INCORE=.FALSE.                                                            
      IF(MEMDIS.GT.0.AND.NPOS.EQ.0)INCORE=.TRUE.                                
      IF(INCORE) THEN                                                           
      WRITE(IOUT,209)MAXLRG                                                     
 209  FORMAT(/10X,'***   AVVISO (PREFRO) : SCELTA LA SOLUZIONE IN'              
     *         ,' MEMORIA   ***'//                                              
     *  10x,'***',20X,'FRONTE MASSIMO DEL CALCOLO :',                           
     1    I8,'   ***'//)                                                        
      CALL JPOSAB(ISLMRG,1)                                                     
      CALL JWRITE(ISLMRG,MINRIG,NEQV)                                           
      ELSE                                                                      
      WRITE(IOUT,208)MAXLRG                                                     
      NSTO=-NSTO                                                                
      END IF                                                                    
      RETURN                                                                    
 208  FORMAT(/10X,'***   AVVISO (PREFRO): SCELTA LA SOLUZIONE FUORI ME',        
     *  'MORIA',6X,'***'//10X,'***',20X,'FRONTE MASSIMO DEL CALCOLO :',         
     1    I8,'   ***'//)                                                        
      END                                                                       
C@ELT,I ANBA*ANBA.PRETAB                                                        
C*************************          PRETAB          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PRETAB(IPIAZ,IVORD,IVBL,LNBL,MTEL,MTEQ,NNEQ,NBLOC,INE)         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      INTEGER SPCCOD                                                            
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),IVBL(1),LNBL(1),IVORD(1),MTEL(NBLOC,1),                
     1          MTEQ(NBLOC,1)                                                   
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      DATA MPCCOD/6/,SPCCOD/7/                                                  
      DO 10 I=1,NELES                                                           
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NTERM,1)                                                
      CALL JREAD(ISLVPZ,NT1,1)                                                  
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      JUMP=(NTERM-1)*NT1*2                                                      
      CALL JPOSRL(ISLVPZ,JUMP)                                                  
      IF(ITIP.EQ.MPCCOD) GOTO 5                                                 
      NTERM=NTERM-1                                                             
      IF(ITIP.NE.SPCCOD)NTERM=NTERM-NPSI                                        
 5    CALL SHLSRT(NTERM,IPIAZ,R,1,1,1)                                          
      NBL=(IPIAZ(1)+NNEQ-1)/NNEQ                                                
      IVBL(1)=NBL                                                               
      IF(NTERM.EQ.1) GOTO 21                                                    
      DO 20 L=2,NTERM                                                           
      NBL=(IPIAZ(L)+NNEQ-1)/NNEQ                                                
      LM1=L-1                                                                   
      DO 30 M=1,LM1                                                             
      IF(NBL.NE.IVBL(M)) GOTO 30                                                
      NBL=0                                                                     
      GOTO 31                                                                   
  30  CONTINUE                                                                  
  31  IVBL(L)=NBL                                                               
  20  CONTINUE                                                                  
  21  CONTINUE                                                                  
      DO 40 L=1,NTERM                                                           
      IF(IVBL(L).EQ.0) GOTO 40                                                  
      IND=IVBL(L)                                                               
      NEBL=LNBL(IND)                                                            
      NEBL=NEBL+1                                                               
      LNBL(IND)=NEBL                                                            
      IF(NEBL.GT.INE) GOTO 400                                                  
      MTEL(IND,NEBL)=I                                                          
      MTEQ(IND,NEBL)=IPIAZ(L)                                                   
      IVBL(L)=0                                                                 
  40  CONTINUE                                                                  
  10  CONTINUE                                                                  
      LINF=0                                                                    
      NEMAX=0                                                                   
      IEQ=0                                                                     
      IF(DUMP(3)) THEN                                                          
      CALL INTEST                                                               
      WRITE(IOUT,310)                                                           
      END IF                                                                    
      DO 50 I=1,NBLOC                                                           
      IF(DUMP(3)) WRITE(IOUT,320) I                                             
      DO 55 M=1,NNEQ                                                            
      NE=0                                                                      
      LBB=LNBL(I)                                                               
      DO 60 L=1,LBB                                                             
c                                                                               
c      IEQ=MTEQ(I,L)                                                            
c      IF(IEQ.NE.LINF+M) GOTO 60                                                
c                                                                               
      IF(MTEQ(I,L).NE.LINF+M) GOTO 60                                           
      NE=NE+1                                                                   
      IVORD(NE)=MTEL(I,L)                                                       
  60  CONTINUE                                                                  
      IEQ=IEQ+1                                                                 
      CALL JWRITE(ISLTAB,NE,1)                                                  
      CALL JWRITE(ISLTAB,IVORD,NE)                                              
      IF(DUMP(3))WRITE(IOUT,300)IEQ,NE,(IVORD(LK),LK=1,NE)                      
      CALL IZERO(IVORD,NE)                                                      
      IF(NE.GT.NEMAX) NEMAX=NE                                                  
  55  CONTINUE                                                                  
      LINF=LINF+NNEQ                                                            
      IF(I.EQ.(NBLOC-1)) NNEQ=NEQV-NPSI-I*NNEQ                                  
  50  CONTINUE                                                                  
      NELMAX=NEMAX                                                              
      RETURN                                                                    
400   WRITE(IOUT,301)                                                           
      CALL JHALT                                                                
      STOP                                                                      
301   FORMAT(' *** AVVISO (PRETAB): OVERFLOW,IMPLEMENTARE MERGE ***')           
 300   FORMAT(I5,2X,21I5)                                                       
 310  FORMAT(//10X,'***   TABELLA DI ASSEMBLAGGIO   ***'/)                      
 320  FORMAT(/10X,'BLOCCO DI ASSEMBLAGGIO N.   ',I8//                           
     *       'N.EQ',3X,'N.EL',' N. D''ORDINE DEGLI ELEMENTI'/)                  
      END                                                                       
                                                                                
                                                                                
C*************************          VTTPZ2          ********************        
      SUBROUTINE VTTPZ2(NUME,NUINT,IPIAZ,ICON,ICOMP,                            
     1                 MINRIG)                                                  
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      INTEGER SPCCOD                                                            
      DIMENSION NUINT(1),NUME(1),IPIAZ(1),ICON(1),                              
     1          ICOMP(1),MINRIG(1)                                              
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      DATA MPCCOD/6/,SPCCOD/7/                                                  
      NELES=NELEM                                                               
      IPOSE=1                                                                   
      IPOSF=1                                                                   
      IPOSN=1                                                                   
      IF(DUMP(9))WRITE(IOUT,203)                                                
                                                                                
      DO 5 I=1,NEQV                                                             
      MINRIG(I)=NEQV                                                            
5     CONTINUE                                                                  
      MINRIG(NEQV+1)=NEQV+1                                                     
          MM=NELEM-NMPC                                                         
      DO 10 I=1,NELEM                                                           
      MM=NEQV                                                                   
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,NCON)                                              
 300      FORMAT(12I8)                                                          
      IF(MPCCOD.EQ.ITIP) GOTO 15                                                
      IF(ITIP.EQ.SPCCOD) GOTO 15                                                
      CALL JPOSRL(ISLELM,-NCON-4)                                               
      IF(IFLMR.NE.0)GOTO 80                                                     
      NELES=NELES-1                                                             
      GOTO 10                                                                   
 80   LRID=0                                                                    
      DO 20 L=1,NCON                                                            
      NEST=ICON(L)                                                              
      IF(NEST.EQ.0) GOTO 310                                                    
      LRID=LRID+1                                                               
      NINT=NUINT(NEST)                                                          
      DO 30 K=1,NCS                                                             
      INDEQ=NUME((NINT-1)*NCS+K)                                                
      IPIAZ((LRID-1)*NCS+K)=INDEQ                                               
30    CONTINUE                                                                  
20    CONTINUE                                                                  
      NTERM=LRID*NCS                                                            
      IF(NPSI.EQ.0) GOTO 45                                                     
      DO 40 L=1,NPSI                                                            
      NTERM=NTERM+1                                                             
      IPIAZ(NTERM)=NUME((NNODI+NMPC+L-1)*NCS+1)                                 
40    CONTINUE                                                                  
45    CONTINUE                                                                  
      DO 61 K=1,NTERM                                                           
      IF(IPIAZ(K).LT.MM) MM=IPIAZ(K)                                            
 61   CONTINUE                                                                  
      DO 60 K=1,NTERM                                                           
      L=IPIAZ(K)                                                                
      IF(MINRIG(L).GT.MM) MINRIG(L)=MM                                          
60    CONTINUE                                                                  
      NT1=NTERM                                                                 
      NTERM=NTERM+1                                                             
      IPIAZ(NTERM)=0                                                            
35    CONTINUE                                                                  
      NWORDE=3+NTERM+(NTERM-1)*NT1*2                                            
      NWORDF=NT1*(NT1-NPSI)*2+(NT1-NPSI)*(NT1-NPSI)*2                           
      NWORDN=NT1*NT1*2                                                          
                                                                                
      NTERM1=NTERM-1                                                            
      IPOSE=IPOSE+NWORDE                                                        
      IPOSF=IPOSF+NWORDF                                                        
      IPOSN=IPOSN+NWORDN                                                        
55    CONTINUE                                                                  
      GOTO 10                                                                   
15    CONTINUE                                                                  
      CALL JREAD(ISLELM,ICOMP,NCON)                                             
      call jposrl(islelm,ncon*2+2)                                              
c     CALL JREAD(ISLELM,RCOMP,NCON*2)                                           
c     CALL JREAD(ISLELM,I1,1)                                                   
c     CALL JREAD(ISLELM,I1,1)                                                   
      IF(ITIP.EQ.SPCCOD) GOTO 50                                                
      NEST=NNODI+IDEL                                                           
      NINT=NUINT(NEST)                                                          
      NEQ=NUME((NINT-1)*NCS+1)                                                  
50    DO 25 L=1,NCON                                                            
      NEST=ICON(L)                                                              
      NINT=NUINT(NEST)                                                          
      INDEQ=NUME((NINT-1)*NCS+ICOMP(L))                                         
      IPIAZ(L)=INDEQ                                                            
      IF(IPIAZ(L).LT.MM)MM=IPIAZ(L)                                             
25    CONTINUE                                                                  
      NTERM=NCON+1                                                              
      IPIAZ(NTERM)=NEQ                                                          
      IF(ITIP.EQ.SPCCOD) IPIAZ(NTERM)=0                                         
      DO 70 K=1,NTERM                                                           
      L=IPIAZ(K)                                                                
      IF(L.EQ.0)GOTO 70                                                         
      IF(MINRIG(L).GT.MM)MINRIG(L)=MM                                           
70    CONTINUE                                                                  
      NT1=1                                                                     
      NWORDE=3+NTERM+(NTERM-1)*NT1*2                                            
      IPOSE=IPOSE+NWORDE                                                        
      IPOSF=IPOSF                                                               
      IPOSN=IPOSN                                                               
      ICOD=0                                                                    
      IF(ITIP.EQ.SPCCOD) ICOD=-1                                                
10    CONTINUE                                                                  
      IF(NELES.NE.NELRIG)WRITE(IOUT,208)NELES,NELRIG                            
      RETURN                                                                    
310   IF(ITIP.EQ.5) GOTO 20                                                     
      WRITE(IOUT,207)                                                           
      WRITE(IOUT,*)'ITIP IN VTTPZZ=',ITIP                                       
      CALL JHALT                                                                
      STOP                                                                      
200   FORMAT(/T10,'ELEM. N.RO',I4,' DI TIPO ',I4,'  N.RO DI TERMINI',I4)        
201   FORMAT(/T10,'VETTORE DI PIAZZAMENTO',/20(13I10/))                         
202   FORMAT(1X,12E10.4)                                                        
203   FORMAT(//' ***  STAMPE SUBROUTINE  "VTTPZZ"  ***')                        
205   FORMAT(/T10,'MATRICE DELL''ELEMENTO'/)                                    
206   FORMAT(T10,2I8)                                                           
207   FORMAT(' *** AVVISO (VTTPZZ): NUEST = 0    ***')                          
 212  FORMAT(' RIGA  N.RO',I8)                                                  
208   FORMAT(' *** AVVISO (VTTPZZ): NON CONCORDA IL NUMERO DI ELE',             
     1   'MENTI RIGIDI CALCOLATO CON QUELLO DI INPUT',2I8,'   ***')             
      END                                                                       
C@ELT,I ANBA*ANBA.VTTPZZ                                                        
C*************************          VTTPZZ          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE VTTPZZ(NUME,NUINT,IPIAZ,ICON,ICOMP,ELMAT,ELMAT2,               
     1                 ELMAT3,NRELMT,RCOMP,MINRIG,NWDEL,VMASS)                  
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      INTEGER SPCCOD                                                            
      DIMENSION NUINT(1),NUME(1),IPIAZ(1),ELMAT(NRELMT,1),ICON(1),              
     1          ICOMP(1),RCOMP(1),MINRIG(1),NWDEL(1),                           
     2          VMASS(1),ELMAT2(NRELMT,1),ELMAT3(NRELMT,1)                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      DATA MPCCOD/6/,SPCCOD/7/                                                  
      NELES=NELEM                                                               
      IPOSE=1                                                                   
      IPOSF=1                                                                   
      IPOSN=1                                                                   
      IF(DUMP(9))WRITE(IOUT,203)                                                
                                                                                
      DO 5 I=1,NEQV                                                             
      MINRIG(I)=NEQV                                                            
5     CONTINUE                                                                  
      MINRIG(NEQV+1)=NEQV+1                                                     
          MM=NELEM-NMPC                                                         
      DO 10 I=1,NELEM                                                           
      MM=NEQV                                                                   
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,ICON,NCON)                                              
 300      FORMAT(12I8)                                                          
      IF(MPCCOD.EQ.ITIP) GOTO 15                                                
      IF(ITIP.EQ.SPCCOD) GOTO 15                                                
       WRITE(IOUT,*)i,ITIP,IDEL                                                 
CCC       WRITE(IOUT,300)ITIP,IDEL,NCON,(ICON(L),L=1,NCON)                      
      CALL JPOSRL(ISLELM,-NCON-4)                                               
c     CALL MATRIX(ITIP,IDEL,ELMAT,ELMAT,ELMAT2,ELMAT3,NRELMT,VMASS)             
c     IF(IFLMR.NE.0)GOTO 80                                                     
c     NWDEL(I)=0                                                                
c     NELES=NELES-1                                                             
c     GOTO 10                                                                   
c80   LRID=0                                                                    
      LRID=0                                                                    
      DO 20 L=1,NCON                                                            
      NEST=ICON(L)                                                              
      IF(NEST.EQ.0) GOTO 310                                                    
      LRID=LRID+1                                                               
      NINT=NUINT(NEST)                                                          
      DO 30 K=1,NCS                                                             
      INDEQ=NUME((NINT-1)*NCS+K)                                                
      IPIAZ((LRID-1)*NCS+K)=INDEQ                                               
30    CONTINUE                                                                  
20    CONTINUE                                                                  
      NTERM=LRID*NCS                                                            
      IF(NPSI.EQ.0) GOTO 45                                                     
      DO 40 L=1,NPSI                                                            
      NTERM=NTERM+1                                                             
      IPIAZ(NTERM)=NUME((NNODI+NMPC+L-1)*NCS+1)                                 
40    CONTINUE                                                                  
45    CONTINUE                                                                  
      DO 61 K=1,NTERM                                                           
      IF(IPIAZ(K).LT.MM) MM=IPIAZ(K)                                            
 61   CONTINUE                                                                  
      DO 60 K=1,NTERM                                                           
      L=IPIAZ(K)                                                                
      IF(MINRIG(L).GT.MM) MINRIG(L)=MM                                          
60    CONTINUE                                                                  
      NT1=NTERM                                                                 
      NTERM=NTERM+1                                                             
      IPIAZ(NTERM)=0                                                            
35    CONTINUE                                                                  
      CALL MATRIX(ITIP,IDEL,ELMAT,ELMAT,ELMAT2,ELMAT3,NRELMT,VMASS,             
     *  IPIAZ)                                                                  
      IF(IFLMR.NE.0)GOTO 80                                                     
      NWDEL(I)=0                                                                
      NELES=NELES-1                                                             
      GOTO 10                                                                   
80    NWORDE=3+NTERM+(NTERM-1)*NT1*2                                            
      NWORDF=NT1*(NT1-NPSI)*2+(NT1-NPSI)*(NT1-NPSI)*2                           
      NWORDN=NT1*NT1*2                                                          
      NWDEL(I)=IPOSE                                                            
      NWDEL(I+NELEM)=IPOSF+NT1*(NT1-NPSI)*2                                     
      NWDEL(I+2*NELEM)=IPOSN                                                    
      CALL JWRITE(ISLVPZ,ITIP,1)                                                
      CALL JWRITE(ISLVPZ,NTERM,1)                                               
      CALL JWRITE(ISLVPZ,NT1,1)                                                 
      CALL JWRITE(ISLVPZ,IPIAZ,NTERM)                                           
      NTERM1=NTERM-1                                                            
      DO 37 IS=1,NTERM1                                                         
      CALL JWRITE(ISLVPZ,ELMAT(1,IS),NT1*2)                                     
37    CONTINUE                                                                  
      IPOSE=IPOSE+NWORDE                                                        
      IPOSF=IPOSF+NWORDF                                                        
      IPOSN=IPOSN+NWORDN                                                        
55    CONTINUE                                                                  
      IF(DUMP(9)) THEN                                                          
      WRITE(IOUT,200)I,ITIP,NTERM1                                              
      WRITE(IOUT,201)(IPIAZ(L),L=1,NTERM)                                       
      END IF                                                                    
      IF(DUMP(1)) CALL DWRITM('MATRICE RIGIDEZZA',ELMAT,                        
     *                        NRELMT,NT1,NT1,1,IPIAZ)                           
      GOTO 10                                                                   
15    CONTINUE                                                                  
      CALL JREAD(ISLELM,ICOMP,NCON)                                             
      CALL JREAD(ISLELM,RCOMP,NCON*2)                                           
      CALL JREAD(ISLELM,I1,1)                                                   
      CALL JREAD(ISLELM,I1,1)                                                   
      IF(ITIP.EQ.SPCCOD) GOTO 50                                                
      NEST=NNODI+IDEL                                                           
      NINT=NUINT(NEST)                                                          
      NEQ=NUME((NINT-1)*NCS+1)                                                  
50    DO 25 L=1,NCON                                                            
      NEST=ICON(L)                                                              
      NINT=NUINT(NEST)                                                          
      INDEQ=NUME((NINT-1)*NCS+ICOMP(L))                                         
      IPIAZ(L)=INDEQ                                                            
      IF(IPIAZ(L).LT.MM)MM=IPIAZ(L)                                             
25    CONTINUE                                                                  
      NTERM=NCON+1                                                              
      IPIAZ(NTERM)=NEQ                                                          
      IF(ITIP.EQ.SPCCOD) IPIAZ(NTERM)=0                                         
      DO 70 K=1,NTERM                                                           
      L=IPIAZ(K)                                                                
      IF(L.EQ.0)GOTO 70                                                         
      IF(MINRIG(L).GT.MM)MINRIG(L)=MM                                           
70    CONTINUE                                                                  
      NT1=1                                                                     
      NWORDE=3+NTERM+(NTERM-1)*NT1*2                                            
      NWDEL(I)=IPOSE                                                            
      NWDEL(I+NELEM)=IPOSF                                                      
      NWDEL(I+2*NELEM)=IPOSN                                                    
      CALL JWRITE(ISLVPZ,ITIP,1)                                                
      CALL JWRITE(ISLVPZ,NTERM,1)                                               
      CALL JWRITE(ISLVPZ,NT1,1)                                                 
      CALL JWRITE(ISLVPZ,IPIAZ,NTERM)                                           
      CALL JWRITE(ISLVPZ,RCOMP,(NTERM-1)*2)                                     
      IPOSE=IPOSE+NWORDE                                                        
      IPOSF=IPOSF                                                               
      IPOSN=IPOSN                                                               
      IF(DUMP(9)) THEN                                                          
      WRITE(IOUT,200)I,ITIP,NTERM                                               
      WRITE(IOUT,201)(IPIAZ(L),L=1,NTERM)                                       
      END IF                                                                    
      ICOD=0                                                                    
      IF(ITIP.EQ.SPCCOD) ICOD=-1                                                
      IF(DUMP(1)) CALL DWRITE('MATRICE DEL VINCOLO',RCOMP,                      
     *             NTERM,NTERM+ICOD,1,-1)                                       
10    CONTINUE                                                                  
      IF(NELES.NE.NELRIG)WRITE(IOUT,208)NELES,NELRIG                            
      RETURN                                                                    
 310  IF(ITIP.EQ.5) GOTO 20                                                     
      WRITE(IOUT,207)                                                           
      WRITE(IOUT,*)'ITIP IN VTTPZZ=',ITIP                                       
      CALL JHALT                                                                
      STOP                                                                      
200   FORMAT(/T10,'ELEM. N.RO',I4,' DI TIPO ',I4,'  N.RO DI TERMINI',I4)        
201   FORMAT(/T10,'VETTORE DI PIAZZAMENTO',/20(13I10/))                         
202   FORMAT(1X,12E10.4)                                                        
203   FORMAT(//' ***  STAMPE SUBROUTINE  "VTTPZZ"  ***')                        
205   FORMAT(/T10,'MATRICE DELL''ELEMENTO'/)                                    
206   FORMAT(T10,2I8)                                                           
207   FORMAT(' *** AVVISO (VTTPZZ): NUEST = 0    ***')                          
 212  FORMAT(' RIGA  N.RO',I8)                                                  
208   FORMAT(' *** AVVISO (VTTPZZ): NON CONCORDA IL NUMERO DI ELE',             
     1   'MENTI RIGIDI CALCOLATO CON QUELLO DI INPUT',2I8,'   ***')             
      END                                                                       
C@ELT,I ANBA*ANBA.WROUTE                                                        
C*************************          WROUTE          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE WROUTE(IHED,LHED,IPIAZ,VETT,ASSMAT,NRASMT,IRIGA,NCOL,          
     1                 IPLH,NPLH,IPRIG)                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),ASSMAT(NRASMT,1),VETT(1)                               
      DIMENSION LHED(1),IHED(1)                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NCDC,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn1,nn2,nn3             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      ICOL=0                                                                    
      DO 130 JK=1,NCOL                                                          
      IF(LHED(JK).LT.0) GOTO 130                                                
      ICOL=ICOL+1                                                               
      IHED(ICOL)=LHED(JK)                                                       
  130 CONTINUE                                                                  
      CALL SHLSRT(ICOL,IHED,R,1,1,1)                                            
      IF(IHED(1).NE.IRIGA) GOTO 160                                             
      DO 135 JK=1,ICOL                                                          
      IHED(JK)=IPOSL(IHED(JK),LHED,NCOL)                                        
  135 CONTINUE                                                                  
      NTERM=0                                                                   
      DO 150 LK=1,ICOL                                                          
      ICEQL=IHED(LK)                                                            
      NTERM=NTERM+1                                                             
      IPIAZ(NTERM)=LHED(ICEQL)                                                  
      VETT(NTERM)=ASSMAT(IPRIG,ICEQL)                                           
      ASSMAT(IPRIG,ICEQL)=0.D0                                                  
  150 CONTINUE                                                                  
      IF(MAXFRO.GE.NTERM)GOTO 170                                               
      MAXFRO=NTERM                                                              
      IF(MAXFRO.GT.MAXLRG) GOTO 200                                             
      WRITE(IOUT,789)MAXFRO                                                     
  789 FORMAT('  FRONTE ATTUALE :',I8)                                           
 170  CONTINUE                                                                  
      CALL JWRITE(ISLASM,NTERM,1)                                               
      CALL JWRITE(ISLASM,IPIAZ,NTERM)                                           
      CALL JWRITE(ISLASM,VETT,NTERM*2)                                          
       NWWRT=NTERM*3+2                                                          
       CALL JWRITE(ISLASM,NWWRT,1)                                              
      IPOS=IHED(1)                                                              
      LHED(IPOS)=-LHED(IPOS)                                                    
      NPLH=NPLH+1                                                               
      IPLH=2                                                                    
      IF(DUMP(9).OR.DUMP(5))WRITE(IOUT,904) IRIGA,(IPIAZ(L),L=1,ICOL)           
      IF(DUMP(5))            WRITE(IOUT,905)       (VETT(L),L=1,ICOL)           
 904  FORMAT(' EQUAZ. SCRITTA N.',I6,'  VETT. PIAZZ.'/20(20I6/))                
 905  FORMAT(' EQUAZ. COEFF.'/20(3X,10D12.6/))                                  
      RETURN                                                                    
  200 WRITE(IOUT,902)MAXFRO,MAXLRG                                              
  161 CALL JHALT                                                                
      STOP                                                                      
  160 WRITE(IOUT,903) IRIGA,(LHED(L),L=1,NCOL)                                  
      GOTO 161                                                                  
  902 FORMAT(' *** AVVISO (WROUTE): IL MASSIMO FRONTE EFFETTIVO (',I8,          
     1 ') SUPERA IL MASSIMO FATTORIZZABILE (',I8,')  ***')                      
  903 FORMAT(' *** AVVISO (WROUTE): NON E'' PRESENTE IL TERMINE DIAGONAL        
     1E DELLA RIGA ',I6/'  LHED',20(12I10/))                                    
      END                                                                       

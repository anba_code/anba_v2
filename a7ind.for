C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C  ------------------------------------------------------------------           
C  - NOTA: quarta riga del common /ISLOTS/ contiene slot utilizzati -           
C  -       per la risoluzione del problema termico                  -           
C  ------------------------------------------------------------------           
C@ELT,I ANBA*ANBA.MODINP                                                        
C*************************          MODINP          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MODINP                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
      common /outnas/ nasmod,naspch                                             
C                                                                               
      IERG=0                                                                    
      write(nasmod,'(a)')'$REF'                                                 
      write(nasmod,'(a)')'$END'                                                 
      write(nasmod,'(a)')'$GRI'                                                 
C                                                                               
      READ(ISYM,100)ISCHE                                                       
                                                                                
      IF(ISCHE(:4).NE.'NODI') GOTO 150                                          
C                                                                               
      CALL INPUTN(ISCHE,IERG,JLABEL,JCOOR)                                      
      write(nasmod,'(a)')'$END'                                                 
      write(nasmod,'(a)')'$CON'                                                 
      CALL JALLOC(*2000,'NODCNT  ','INT.',1,NNODI  ,JNDCNT)                     
      DO 8754 iopl=0,nnodi-1                                                    
8754  iv(jndcnt+iopl)=0                                                         
c      CALL IZERO(IV(JNDCNT),NNODI)                                             
C                                                                               
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
C                                                                               
      WRITE(IOUT,'(A)') ISCHE                                                   
      IF(ISCHE(:4).EQ.'RINU') READ(ISYM,100)ISCHE                               
      IF(ISCHE(:3).NE.'MPC')GOTO 199                                            
C                                                                               
      IER=0                                                                     
      CALL INPUTM(ISCHE,IER,JLABEL,JCOOR,JNDCNT)                                
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
 199  CONTINUE                                                                  
      IF(ISCHE(:4).EQ.'RINU'.OR.ISCHE(:3).EQ.'MPC')READ(ISYM,100)ISCHE          
C                                                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,12     ,JICON )                     
      CALL JALLOC(*2000,'VETT    ','D.P.',1,24     ,JVETT )                     
      WRITE(IOUT,'(A)') ISCHE                                                   
      IF(ISCHE(:4).EQ.'ELEM')GOTO 201                                           
      WRITE(IOUT,902)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 201  READ(ISYM,100,ERR=899)ISCHE                                               
      IF(ISCHE(:4).EQ.'CORR') THEN                                              
C                                                                               
      IER=0                                                                     
      CALL INPUTC(ISCHE,IER,JLABEL,JCOOR,JICON,JVETT,JNDCNT)                    
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'GIUN') THEN                                         
C                                                                               
      IER=0                                                                     
      CALL INPUTG(ISCHE,IER,JLABEL,JCOOR,JICON,JVETT,JNDCNT)                    
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'PANN') THEN                                         
C                                                                               
      IER=0                                                                     
      CALL INPUTP(ISCHE,IER,JLABEL,JCOOR,JICON,JVETT,JNDCNT)                    
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'LAMI') THEN                                         
C                                                                               
      IER=0                                                                     
      CALL INPUTL(ISCHE,IER,JLABEL,JCOOR,JICON,JVETT,JNDCNT)                    
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'STRA') THEN                                         
C                                                                               
      IER=0                                                                     
      CALL INPUTS(ISCHE,IER,JLABEL,JCOOR,JICON,JVETT,JNDCNT)                    
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'PIAN') THEN                                         
C                                                                               
      IER=0                                                                     
      CALL INPUTE(ISCHE,IER,JLABEL,JCOOR,JICON,JVETT,JNDCNT)                    
      IF(IER.NE.0) IERG=1                                                       
C                                                                               
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'VINC') THEN                                         
      IER=0                                                                     
      CALL INPVIN(IV(JLABEL),DV(JCOOR),NNODI,IV(JNDCNT),IER)                    
      IF(IER.NE.0) IERG=1                                                       
      GOTO 201                                                                  
      ELSE IF(ISCHE(:4).EQ.'CHIU') THEN                                         
      GOTO 899                                                                  
      END IF                                                                    
      WRITE(IOUT,904)ISCHE                                                      
      IERG=1                                                                    
 899  DO 300 I=1,NNODI                                                          
      IF(IV(JNDCNT+I-1).NE.0) GOTO 300                                          
      WRITE(IOUT,905) IV(JLABEL+I-1)                                            
      IERG=1                                                                    
 300  CONTINUE                                                                  
      NELTOT=NCOR+NGIU+NPAN+NLAM+NELP+NSPC+NELS                                 
      NELRIG=NCORR+NGIUR+NPANR+NLAMR+NELPR+NSPC+NELSR                           
      MAXDEL=MAXNOD*NCS+NPSI                                                    
      CALL JCLOSE(ISLELM)                                                       
      IF(IERG.EQ.0) GOTO 888                                                    
      CALL INTEST                                                               
      WRITE(IOUT,903)                                                           
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 888  NELEM=NELRIG+NMPC                                                         
      IF(MAXNOD.LT.LMXMPC)MAXNOD=LMXMPC                                         
      NEQV=NNODI*NCS+NMPC+NPSI                                                  
      NEQT=(NNODI+NMPC+NPSI)*NCS                                                
C                                                                               
      write(nasmod,'(a)')'$END'                                                 
      CALL INTEST                                                               
      WRITE(IOUT,509)NNODI,NEQV,NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,                
     1           NELS,NSPC,NMPC,NELEM                                           
c      write(iout,*) nelrig,nelem,nels,nelsr                                    
509   FORMAT(////////45X,'***   PARAMETRI  DI  CALCOLO   ***'///                
     128X,'NUMERO DI NODI ',35('.'),' :',I12/                                   
     228X,'NUMERO DI EQUAZIONI DEL SISTEMA .................. :',I12/           
     328X,'NUMERO DI ELEMENTI STRUTTURALI ',19('.'),' :',I12/                   
     428X,'   "      CORRENTI ',31('.'),' :',I12/                               
     528X,'   "      GIUNZIONI ',30('.'),' :',I12/                              
     628X,'   "      PANNELLI ',31('.'),' :',I12/                               
     728X,'   "      LAMINE ',33('.'),' :',I12/                                 
     828X,'   "      ELEMENTI PIANI ',25('.'),' :',I12/                         
     828X,'   "      ELEMENTI STRATIFICATI ',18('.'),' :',I12/                  
     928X,'   "      SPC ',36('.'),' :',I12/                                    
     A28X,'   "      MPC ',36('.'),' :',I12/                                    
     B28X,'NUMERO TOTALE DI ELEMENTI ',24('.'),' :',I12/)                       
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI+NMPC  ,JLABEL)                
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT        ,JNUME )                
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC  ,JNUINT)                
C                                                                               
      CALL JOPEN('*RIN    ',ISLRIN,1)                                           
      CALL JREAD(ISLRIN,IV(JNUINT),NNODI)                                       
      CALL JCLOSE(ISLRIN)                                                       
C                                                                               
      CALL JALLOC(*2000,'NUEST   ','INT.',1,NNODI+NMPC  ,JNUEST)                
      DO 8757 iopl=1,lmxmpc*4+nmpc*2                                            
8757  iv(jnuest+nnodi+nmpc+iopl)=0                                              
      CALL JALLOC(*2000,'NODMPC  ','INT.',1,LMXMPC      ,JNOMPC)                
      CALL JALLOC(*2000,'ICOMP   ','INT.',1,LMXMPC      ,JICOMP )               
      CALL JALLOC(*2000,'ICON    ','INT.',1,NMPC        ,JICON )                
      CALL JALLOC(*2000,'RCOMP   ','D.P.',1,LMXMPC      ,JRCOMP)                
      CALL JALLOC(*2000,'MINMPC  ','INT.',1,NMPC        ,JMIMPC)                
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
C                                                                               
c      JLIM=JMIMPC+NMPC-1                                                       
c      KK=JLIM-JNOMPC                                                           
c      CALL IZERO(IV(JNOMPC),KK)                                                
C                                                                               
      CALL PRNUME(IV(JNUME),IV(JNUINT),IV(JNUEST),IV(JNOMPC),IV(JICOMP),        
     1  IV(JICON),DV(JRCOMP),IV(JMIMPC),IV(JLABEL))                             
      CALL JCLOSE(ISLNUM)                                                       
      CALL JCLOSE(ISLELM)                                                       
C                                                                               
      CALL JCRERS                                                               
C                                                                               
C      DO 8758 iopl=1,maxdim                                                    
C8758  iv(iopl)=0                                                               
                                                                                
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT         ,JNUME )               
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC   ,JNUINT)               
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1     ,JIPIAZ)               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXNOD       ,JICON )               
      CALL JALLOC(*2000,'ICOMP   ','INT.',1,MAXNOD       ,JICOMP)               
      CALL JALLOC(*2000,'MINRIG  ','INT.',1,NEQV+1       ,JMNRIG)               
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
      CALL JOPEN('*ELS    ',ISLELS,1)                                           
C                                                                               
                                                                                
      CALL VTTPZ2(IV(JNUME),IV(JNUINT),IV(JIPIAZ),IV(JICON),IV(JICOMP),         
     2  IV(JMNRIG))                                                             
      CALL JCLOSE(ISLELS)                                                       
      CALL JCLOSE(ISLELM)                                                       
      CALL JCLOSE(ISLMTN)                                                       
      CALL JCLOSE(ISLMTF)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
C                                                                               
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      NPOS=NMPC+INCOU                                                           
C                                                                               
      CALL PREFRO(IV(JMNRIG),NEQV,ISLMRG,MAXLRG,NSTO,NPOS,MAXDEL,IGEOM)         
      CALL JCLOSE(ISLMRG)                                                       
                                                                                
      CALL CHKPNT                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 150  WRITE(IOUT,901)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 901  FORMAT(///10X,'***   AVVISO (MODINP): NON PRESENTE IL GRUPPO'             
     1   ,' NODI   ***')                                                        
 902  FORMAT(///10X,'***   AVVISO (MODINP): NON PRESENTE LA STRINGA'            
     1   ,' DI INTESTAZIONE GRUPPO ELEMENTI   ***')                             
 903  FORMAT(///10X,'***   AVVISO (MODINP): GLI ERRORI RISCONTRATI '            
     1 ,'SONO TALI DA NON GARANTIRE UNA CORRETTA ESECUZIONE   ***'//)           
 904  FORMAT(///10X,'***   AVVISO (MODINP): NON CHIUSO IL GRUPPO '              
     1 ,'ELEMENTI    ***'//)                                                    
 905  FORMAT(' ***   AVVISO (MODINP): IL NODO',I8,' NON E'' COLLEGATO',         
     1  ' A NESSUN ELEMENTO   ***'/)                                            
 100  FORMAT(A80)                                                               
      END                                                                       

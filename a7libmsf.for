C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.INTEST                                                        
C*************************          INTEST          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INTEST                                                         
      LOGICAL DUMP,IFIRST,incore                                                
      CHARACTER IDESC*80,oggi*9,ora*8                                           
C      save oggi,ora                                                            
cIBM      DIMENSION TIME(9)                                                     
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /TESTA/ IDESC(2)                                                   
      DATA IFIRST/.FALSE./                                                      
      NRIG=0                                                                    
      IF(IFIRST)GOTO 20                                                         
      NPAG=1                                                                    
      IFIRST=.TRUE.                                                             
c---> Seguono istruzioni per ambiente IBM JCL                                   
cIBM     CALL JOBNO(TIME)                                                       
cIBM20   WRITE(IOUT,10)IDESC(1),TIME(6),TIME(7),TIME(8),TIME(9),NPAG,           
cIBM    *              IDESC(2)                                                 
cIBM 10  FORMAT(1H1,2X,A80,10X,2A4,3X,'ANBA-2',2X,2A4,                          
cIBM    1    4X,'PAG.',I4/3X,A80//)                                             
c---> Seguono istruzioni per HP-720                                             
cc      call date(oggi)                                                         
cc      call time(ora)                                                          
       oggi=' '                                                                 
       ora=' '                                                                  
c---> Seguono istruzioni per PC MS Fortran                                      
cc      call date(oggi)                                                         
cc      call time(ora)                                                          
 20   WRITE(IOUT,10)IDESC(1),NPAG,                                              
     *              IDESC(2)                                                    
      NPAG=NPAG+1                                                               
      RETURN                                                                    
  10  FORMAT(1H1,2X,A80,10X,'ANBA-2',2X,  
     1    4X,'PAG.',I4/3X,A80//)                                                
      END

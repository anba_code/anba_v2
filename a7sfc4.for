C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.PRTEPS                                                        
C*************************          PRTEPS          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PRTEPS(IELSET,SIGMG,SIGML,NSOLL,                                
     *                  SIGM,SIGMAG,SIGMAL,SIGMAF,GPQ,nnsol)                    
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      REAL  SIGMAG,SIGMAL,SIGMAF,EPSI,EPSIL,EPSIF                               
      LOGICAL DUMP,INCORE                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3         
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++             
c++++++++++++  31agosto +++++++++++++++++++++++++++++++++++++++++++             
c  Ho ripristinato il common ELPI1 originale [ togliendo CDLTZ(6) ]             
      COMMON /ELPI1/  LREC,NPI,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),               
     1                T2(3,3),DG(6,6),W,DEL,COE(2)                              
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++             
      COMMON /ELPI2/  INT1,INT2,TETA,DEN,alfort,IFLTET                          
      COMMON /ELSTRA/ LRECD,NPINTG,ENNEST(3,12),CPINT(2),DAREA,BETA,            
     1                T11(3,3),T22(3,3),DGELAS(6,6),WGHT                        
      COMMON /ELSTRB/ INTG1,INTG2,DENSI,THICK,ANGLAM                            
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      common /sisrif/ xs,ys,ss,cc,iflsr,iflsro,iflsrs
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunstr    
      COMMON /CAZZO/ NSOL
C                                                                               
      DIMENSION IELSET(1)                                                       
      DIMENSION SIGMG(3,1),SIGML(2,1),EPSP(2,1)                                 
      DIMENSION SIGM(1),GPQ(3,6),EPSIL(NPSI,1)                                  
      DIMENSION SIGMAG(NPSI,1),SIGMAL(NPSI,1),EPSI(NPSI,1)                      
      DIMENSION SIGMAF(NPSI,1),EPSIF(NPSI,1)                                    
      NSOL = NSOLL
C                                                                               
      RETURN                                                                    
C                                                                               
      ENTRY EPSCOR(SIGM,IELSET,NESEL,ixsol)                                      
C                                                                               
C                                                                               
C     STAMPA LE DEFORMAZIONI DEGLI ELEMENTI DI CORRENTE                         
C                                                                               
C                                                                               
C     
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif
      
      LSUP=40                                                                   
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,1110)                                                          
      else
      WRITE(Iunstr,1111)                                                          
      endif
      
      DO 1500 IEL=1,NCORR                                                       

      IF(IEL.LE.LSUP) GOTO 150                                                  
      if(ixsol.gt.0) then
      CALL INTEST                                                               
      WRITE(IOUT,1110)   
      else
      WRITE(Iunstr,1111)                                                          
      endif                                                       
      LSUP=LSUP+40                                                              
C                                                                               
 150  CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,11)                                                    
      IF(IFLMR.EQ.0)GOTO 150                                                    
      IF(IPSEL.EQ.0) GOTO 1500                                                  
      CALL JREAD(ISLSFC,SIGM,6*2)                                            
      CALL JPOSRL(ISLSFC,6*2)
      if(ixsol.gt.0) then
       WRITE(IOUT,1100)IDEL,(SIGM(I),I=1,NSOL)                                   
      else
       WRITE(Iunstr,1101)IDEL,SIGM(NSOL)                                   
      endif
 1500 CONTINUE           

                                                             
      RETURN                                                                    
1100  FORMAT(9X,I6,1X,6E15.6)                                                   
1110  FORMAT(//,40X,'*** DEFORMAZIONI NEGLI ELEMENTI DI CORRENTE ***'           
     1///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                   
     2  T86,'MY',T101,'MZ'/)                                                    
1111  FORMAT(//,40X,'*** FORMATO NASTRAN ***'           
     1///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                   
     2  T86,'MY',T101,'MZ'/)                                                    
1101  FORMAT(9X,I6,1X,6E15.6)                                                   
C                                                                               
C                                                                               
      ENTRY EPSGIU(SIGM,IELSET,NESEL,ixsol)                                      
C                                                                               
C                                                                               
C     STAMPA LE DEFORMAZIONI NELLE GIUNZIONI CON DUE NODI                       
C                                                                               
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif

      LSUP=40                                                                   
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,2100)                                                          
      else
      WRITE(Iunstr,2101)                                                          
      endif

      DO 2500 IEL=1,NGIUR                                                       
      IF(IEL.LE.LSUP)GOTO 250                                                   
      if(ixsol.gt.0) then
      CALL INTEST                                                               
      WRITE(IOUT,2100)
      else
      WRITE(iunstr,2101)
      endif
                                                                
      LSUP=LSUP+40                                                              
250   CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,18)                                                    
      CALL JPOSRL(ISLELS,6)                                                     
      IF(IFLMR.EQ.0)GOTO 250                                                    
      IF(IPSEL.EQ.0) GOTO 2500                                                  
      CALL JREAD(ISLSFC,SIGM,6*2)                                            
      CALL JPOSRL(ISLSFC,6*2)                                                
      if(ixsol.gt.0) then
      WRITE(IOUT,2110) IDEL,(SIGM(I),I=1,NSOL)                                  
      else
      WRITE(iunstr,2111) IDEL,SIGM(NSOL)                                  
      endif
 2500 CONTINUE                                                                  
      
      RETURN                                                                    
2100  FORMAT(//,40X,'*** DEFORMAZIONI NEGLI ELEMENTI DI GIUNZIONE ***'          
     1   ///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                
     2   T86,'MX',T101,'MZ'/)                                                   
2110  FORMAT(9X,I6,1X,6E15.6)                                                   
2101  FORMAT(//,40X,'*** FORMATO NASTRAN ***'          
     1   ///,10X,'ID.ELEM.',T26,'TX',T41,'TY',T56,'TZ',T71,'MX',                
     2   T86,'MX',T101,'MZ'/)                                                   
2111  FORMAT(9X,I6,1X,6E15.6)                                                   
C                                                                               
C                                                                               
      ENTRY EPSPAN(SIGMG,SIGML,                                                 
     1                  EPSP,IELSET,NESEL,ixsol)                                 
C                                                                               

      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif

      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
      else
      WRITE(Iunstr,3101)                                                          
      endif

      NRIG=0                                                                    

      DO 3500 IEL=1,NPANR                                                       
350   CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,NCON*5+4)                                              
      CALL JREAD(ISLELM,NPINT,1)                                                
      IF(IFLMR.EQ.0.OR.IPSEL.EQ.0) GOTO 36                                      
      NRIG=NRIG+NPINT*3+1                                                       
      IF(NRIG.LE.40) GOTO 36                                                    
      NRIG=NPINT*ILOCA+1                                                        

      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
      else
      WRITE(Iunstr,3101)                                                          
      endif

 36   CALL JPOSRL(ISLELM,10)                                                     
      call jread(islelm,nskip,1)                                                
      call jposrl(islelm,nskip)                                                 
      DO 310 IN=1,NPINT                                                         
      CALL JPOSRL(ISLELS,4*NCON+12)                                             
      CALL JREAD(ISLELS,Y,2)                                                    
      CALL JREAD(ISLELS,X,2)                                                    
      CALL JREAD(ISLELS,X,2)                                                    
      X=-X                                                                      
      if(iflsrs.eq.1) then                                                      
      r=sqrt(x*x+y*y)                                                           
      y=atan2(y,x)*180./3.1415                                                  
      x=r                                                                       
      endif                                                                     
      CALL JPOSRL(ISLELS,6)                                                     
      IF(IFLMR.EQ.0)GOTO 310                                                    
      IF(IPSEL.EQ.0)GOTO 310                                                    
      if( dump(14)) CALL JREAD(ISLSFC,EPSP,4*6)                              
      CALL JPOSRL(ISLSFC,10*6)                                               

      if(ixsol.gt.0) then                   
      IF(IN.EQ.1)WRITE(IOUT,3310)IDEL,IN,                                       
     1   (EPSP(1,I),EPSP(2,I),I=1,NSOL)                                         
      IF(IN.GT.1)WRITE(IOUT,3320)IN,                                            
     1   (EPSP(1,I),EPSP(2,I),I=1,NSOL)                                         
      else
      IF(IN.EQ.1)WRITE(Iunstr,3311)IDEL,IN,                                       
     1   EPSP(1,nsol),EPSP(2,nsol)                                         
      IF(IN.GT.1)WRITE(Iunstr,3321)IN,                                            
     1   EPSP(1,nsol),EPSP(2,nsol)                                         
      endif
310   CONTINUE                                                                  
      IF(IFLMR.EQ.0) GOTO 350                                                   
 3500 CONTINUE                                                                  


      RETURN                                                                    
3100  FORMAT(///40X,'***  DEFORMAZIONI NEGLI ELEMENTI DI PANNELLO ***'//        
     1  1X,'ID.ELEM  P.INTEG',                                                  
     2 9X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'//                   
     2 17X,6('    EZ      GXY  '))                                              
3310  FORMAT(/(2I7,1X,'L',6(1X,2E9.3)))                                         
 3320 FORMAT(7X,I7,1X,'L',6(1X,2E9.3))                                          

3101  FORMAT(///40X,'***  FORMATO NASTRAN ***'//        
     1  1X,'ID.ELEM  P.INTEG',                                                  
     2 9X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'//                   
     2 17X,6('    EZ      GXY  '))                                              
3311  FORMAT(/(2I7,1X,'L',6(1X,2E9.3)))                                         
 3321 FORMAT(7X,I7,1X,'L',6(1X,2E9.3))                                          
C                                                                               
C                                                                               
      ENTRY EPSEPL(SIGMAG,SIGMAL,EPSI,                                          
     1   ITY,IELSET,NESEL,ixsol)                                                 
C                                                                               
C         STAMPA DELLE DEFORMAZIONI NEGLI ELEMENTI PIANI(ITY=5)                 
C                                    E DI LAMINA(ITY=4)                         
C                                                                               
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif



      NWORD=NSOL*6                                                              
      NELI=NLAMR                                                                
      IF(ITY.EQ.5) NELI=NELPR                                                   
      DO 4500 IEL=1,NELI                                                        
c       do 7774 k=1,6                                                           
c       write(60+k,7778) iel                                                    
c       write(70+k,7778) iel                                                    
c7774    continue                                                               
                                                                                
 450  CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
C      CALL JPOSRL(ISLELM,NCON*5+7+72)                                          
C-----nuova istruzione: tiene conto scrittura in ISLELM dei coeff.termici       
      IF(ITY.EQ.5) THEN                                                         
      CALL JPOSRL(ISLELM,NCON*5)                                                
      call jread(islelm,nwr,1)                                                  
      CALL JPOSRL(ISLELM,nwr+72+6*2)                                            
      ELSE                                                                      
      CALL JPOSRL(ISLELM,NCON*5)                                                
      call jread(islelm,nwr,1)                                                  
      CALL JPOSRL(ISLELM,nwr+72)                                                
      ENDIF                                                                     
C-----                                                                          
      IF(ITY.EQ.4) THEN                                                         
      CALL JPOSRL(ISLELM,NCON)                                                  
      END IF                                                                    
      CALL JREAD(ISLELS,LREC,2)                                                 
      IN=0                                                                      
      NBL=(NPI+5)/6                                                             
      NN=6                                                                      
      DO 410 IW=1,NBL                                                           
      IF(IFLMR.EQ.0.OR.IPSEL.EQ.0)GOTO 411                                      

      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      IF(ITY.EQ.4) THEN                                                         
        WRITE(IOUT,4100)IDEL                                                    
      ELSE                                                                      
        WRITE(IOUT,5100)IDEL                                                    
      END IF                                                                    
      WRITE(IOUT,4101)                                                          
      else
       WRITE(Iunstr,4100)IDEL                                                    
       WRITE(Iunstr,4101)                                                          
      endif

 411  IF(IW.EQ.NBL) NN=NPI-(NBL-1)*NN                                           
      DO 410 LL=1,NN                                                            
      IN=IN+1                                                                   
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      IF(IFLMR.EQ.0)GOTO 410                                                    
      IF(IPSEL.EQ.0) GOTO 410                                                   
C                                                                               
      CALL JREAD(ISLSFC,EPSI,NWORD)                                             
      CALL JPOSRL(ISLSFC,NWORD*2)                                               
      if(iflsrs.eq.1) then                                                      
      r=sqrt(cpi(1)**2+cpi(2)**2)                                               
      y=atan2(cpi(2),cpi(1))*180./3.1415                                        
      cpi(1)=r                                                                  
      cpi(2)=y                                                                  
      endif                                                                     
      if(ixsol.gt.0) then                   
      WRITE(IOUT,4211)    IN,(EPSI(1,K),K=1,NSOL)                               
      WRITE(IOUT,4212)       (EPSI(2,K),K=1,NSOL)                               
      WRITE(IOUT,4213)CPI(1),(EPSI(3,K),K=1,NSOL)                               
      WRITE(IOUT,4214)CPI(2),(EPSI(4,K),K=1,NSOL)                               
      WRITE(IOUT,4215)       (EPSI(5,K),K=1,NSOL)                               
      WRITE(IOUT,4216)       (EPSI(6,K),K=1,NSOL)                               
      else
      WRITE(Iunstr,4211)    IN,EPSI(1,NSOL)                               
      WRITE(Iunstr,4212)       EPSI(2,NSOL)                               
      WRITE(Iunstr,4213)CPI(1),EPSI(3,NSOL)                               
      WRITE(Iunstr,4214)CPI(2),EPSI(4,NSOL)                               
      WRITE(Iunstr,4215)       EPSI(5,NSOL)                               
      WRITE(Iunstr,4216)       EPSI(6,NSOL)                               
      endif                                                                                
c     ipo=ipo+1                                                                 
c     if(ipo.eq.ipi) then                                                       
c       ipi=ipi+3                                                               
c       do 7767 k=1,6                                                           
c       write(60+k,7778) cpi(1),cpi(2),(epsi(isfo,k),isfo=1,6)                  
c       write(70+k,7778) cpi(1),cpi(2),(epsi(isfo,k),isfo=1,6)                  
c7767    continue                                                               
c7778    format(8e14.6)                                                         
c     endif                                                                     
                                                                                
 410  CONTINUE                                                                  
      IF(IFLMR.EQ.0) GOTO 450                                                   
 4500 CONTINUE                                                                  


      RETURN                                                                    
4100  FORMAT(/41X,'***  DEFORMAZIONI NELL''ELEMENTO DI LAMINA N.RO',I6,         
     1 '  ***',///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'         
     1 /)                                                                       
4101  FORMAT(16X,6('    RIFER.  LOCALE '))                                      
 5100 FORMAT(/46X,'***  DEFORMAZIONI NELL''ELEMENTO PIANO N.RO',I6,             
     1 '  ***'///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)        
4211  FORMAT(/' P.INT. N.',I4,' GXZ',6(1X,E14.6,4X))                            
4212  FORMAT(14X,' GYZ',6(1X,E14.6,4X))                                         
4213  FORMAT(' X =',E10.4,' EZ ',6(1X,E14.6,4X))                                
4214  FORMAT(' Y =',E10.4,' EX ',6(1X,E14.6,4X))                                
4215  FORMAT(14X,' EY ',6(1X,E14.6,4X))                                         
4216  FORMAT(14X,' GXY',6(1X,E14.6,4X))                                         
C                                                                               
      ENTRY EPSSTR(SIGMAG,SIGMAL,SIGMAF,EPSI,EPSIL,EPSIF,                       
     1   ITY,IELSET,NESEL,ixsol)                                                 
C                                                                               
C         STAMPA DELLE DEFORMAZIONI NEGLI ELEMENTI STRATIFICATI(ITY=8)          
C                                                                               
      if(ixsol.gt.0) then                   
      nsol=ixsol
      else
      nsol=-ixsol
      endif

      NWORD=NSOL*NPSI                                                           
      NELI=NELSR                                                                
      DO 8500 IEL=1,NELI                                                        
c       do 7775 k=1,6                                                           
c       write(60+k,7778) iel                                                    
c       write(70+k,7778) iel                                                    
7775    continue                                                                
 850  CALL JREAD(ISLELM,ITIP,4)                                                 
      IPSEL=1                                                                   
      IF(NESEL.NE.0) IPSEL=IPOSL(IDEL,IELSET,NESEL)                             
      CALL JPOSRL(ISLELM,NCON*5)                                                
      CALL JREAD(ISLELM,NSTRAT,1)                                               
      NUMPOS=NSTRAT*80                                                          
      CALL JPOSRL(ISLELM,NUMPOS)                                                
      CALL JREAD(ISLELS,LRECD,2)                                                
      IN=0                                                                      
      NBL=(NPINTG*NSTRAT+5)/6                                                   
      NN=6                                                                      
      ipo=0                                                                     
      ipi=2                                                                     
      DO 810 IW=1,NBL                                                           
      IF(IFLMR.EQ.0.OR.IPSEL.EQ.0)GOTO 811                                      
      if(ixsol.gt.0) then                   
      CALL INTEST                                                               
      WRITE(IOUT,8100)IDEL                                                      
      else
       WRITE(Iunstr,8101)                                                          
      endif


 811  IF(IW.EQ.NBL) NN=NPINTG*NSTRAT-(NBL-1)*NN                                 
      DO 810 LL=1,NN                                                            
      IN=IN+1                                                                   
      CALL JREAD(ISLELS,ENNEST,LRECD)                                           
      IF(IFLMR.EQ.0)GOTO 810                                                    
      IF(IPSEL.EQ.0) GOTO 810                                                   
C                                                                               
      CALL JREAD(ISLSFC,EPSI,NWORD)                                             
      CALL JREAD(ISLSFC,EPSIL,NWORD)                                            
      CALL JPOSRL(ISLSFC,NWORD*2)                                               
      IF(DUMP(20)) CALL JREAD(ISLSFC,EPSIF,NWORD)                               
CMT   MODIFICA 7-4-89 :AGGIUNTA DI IF(DUMP(19))                                 
      IF(DUMP(19)) CALL JPOSRL(ISLSFC,NWORD)                                    
C                                                                               
      if(iflsrs.eq.1) then                                                      
      r=sqrt(cpint(1)**2+cpint(2)**2)                                           
      y=atan2(cpint(2),cpint(1))*180./3.1415                                    
      cpint(1)=r                                                                
      cpint(2)=y                                                                
      endif                                                                     
                                                                                
c     ipo=ipo+1                                                                 
c     if(ipo.eq.ipi) then                                                       
c       ipi=ipi+3                                                               
c       do 7777 k=1,6                                                           
c       write(60+k,7778) cpint(1),cpint(2),(epsil(isfo,k),isfo=1,6)             
c       write(70+k,7778) cpint(1),cpint(2),(epsi(isfo,k),isfo=1,6)              
7777    continue                                                                
c     endif                                                                     
      if(ixsol.gt.0) then                                                                                
      IF(ILOCA.NE.1) THEN                                                       
      WRITE(IOUT,8101)                                                          
      WRITE(IOUT,8211)      IN,(EPSI(1,K),K=1,NSOL)                             
      WRITE(IOUT,8212)         (EPSI(2,K),K=1,NSOL)                             
      WRITE(IOUT,8213)CPINT(1),(EPSI(3,K),K=1,NSOL)                             
      WRITE(IOUT,8214)CPINT(2),(EPSI(4,K),K=1,NSOL)                             
      WRITE(IOUT,8215)         (EPSI(5,K),K=1,NSOL)                             
      WRITE(IOUT,8216)         (EPSI(6,K),K=1,NSOL)                             
      END IF                                                                    
C                                                                               
      IF(ILOCA.NE.2) THEN                                                       
      WRITE(IOUT,8901)                                                          
      WRITE(IOUT,8211)      IN,(EPSIL(1,K),K=1,NSOL)                            
      WRITE(IOUT,8212)         (EPSIL(2,K),K=1,NSOL)                            
      WRITE(IOUT,8213)CPINT(1),(EPSIL(3,K),K=1,NSOL)                            
      WRITE(IOUT,8214)CPINT(2),(EPSIL(4,K),K=1,NSOL)                            
      WRITE(IOUT,8215)         (EPSIL(5,K),K=1,NSOL)                            
      WRITE(IOUT,8216)         (EPSIL(6,K),K=1,NSOL)                            
      END IF                                                                    
C                                                                               
      IF(DUMP(21)) THEN                                                         
      WRITE(IOUT,8911)                                                          
      WRITE(IOUT,8211)      IN,(EPSIF(1,K),K=1,NSOL)                            
      WRITE(IOUT,8212)         (EPSIF(2,K),K=1,NSOL)                            
      WRITE(IOUT,8213)CPINT(1),(EPSIF(3,K),K=1,NSOL)                            
      WRITE(IOUT,8214)CPINT(2),(EPSIF(4,K),K=1,NSOL)                            
      WRITE(IOUT,8215)         (EPSIF(5,K),K=1,NSOL)                            
      WRITE(IOUT,8216)         (EPSIF(6,K),K=1,NSOL)                            
      END IF             
      else
      WRITE(Iunstr,8101)                                                          
      WRITE(Iunstr,8211)      IN,(EPSI(1,K),K=1,NSOL)                             
      WRITE(Iunstr,8212)         (EPSI(2,K),K=1,NSOL)                             
      WRITE(Iunstr,8213)CPINT(1),(EPSI(3,K),K=1,NSOL)                             
      WRITE(Iunstr,8214)CPINT(2),(EPSI(4,K),K=1,NSOL)                             
      WRITE(Iunstr,8215)         (EPSI(5,K),K=1,NSOL)                             
      WRITE(Iunstr,8216)         (EPSI(6,K),K=1,NSOL)                             
      endif                                                       
 810  CONTINUE                                                                  
      IF(IFLMR.EQ.0) GOTO 850                                                   
 8500 CONTINUE                                                                  

      
      RETURN                                                                    
8100  FORMAT(41X,'***  DEFORMAZIONI NELL''ELEMENTO STRATIFICATO N.RO',          
     1 I6,'  ***',///26X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,          
     1 'MZ',/)                                                                  
8101  FORMAT(/16X,6('    RIFER.  GENER. '))                                     
8911  FORMAT(/16X,6('    RIFER.  FIBRE '))                                      
8901  FORMAT(/16X,6('    RIFER.  LOCALE'))                                      
8211  FORMAT(/' P.INT. N.',I4,' GXZ',6(1X,E14.6,4X))                            
8212  FORMAT(14X,' GYZ',6(1X,E14.6,4X))                                         
8213  FORMAT(' X =',E10.4,' EZ ',6(1X,E14.6,4X))                                
8214  FORMAT(' Y =',E10.4,' EX ',6(1X,E14.6,4X))                                
8215  FORMAT(14X,' EY ',6(1X,E14.6,4X))                                         
8216  FORMAT(14X,' GXY',6(1X,E14.6,4X))                                         
      END                                                                       
                                                                                
                                                                                

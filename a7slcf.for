C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MDSLCO                                                
C*************************          MDSLCO          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE MDSLCO                                                 
      IMPLICIT REAL*8(A-H,O-Z)                                          
      REAL*8 DV(1)                                                      
      LOGICAL DUMP,INCORE                                               
      COMMON IV(1)                                                      
      EQUIVALENCE (IV(1),DV(1))                                         
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,  
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO
      COMMON /JCRMNG/ MAXDIM                                            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                     
C                                                                       
C                                                                       
      CALL JCRERS                                                       
      NDIMEQ=MAXFRO*(MAXFRO+1)/2                                        
      CALL JALLOC(*2000,'EQ      ','D.P.',1,NDIMEQ      ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
      CALL JALLOC(*2000,'IPIAZH  ','INT.',1,MAXFRO      ,JPIAZH)        
      CALL JALLOC(*2000,'ILOC    ','INT.',1,MAXFRO      ,JILOC )        
      call izero(iv(jipiaz),maxfro)                                     
      call izero(iv(jpiazh),maxfro)                                     
      call izero(iv(jiloc),maxfro)                                      
      call dzero(dv(jeq),ndimeq)                                        
      call jprmem                                                       
      call jprtab                                                       
C>>>  CALL JOPEN('*FCT    ',ISLFCT,1)                                   
C>>>  CALL JOPEN('*ASM    ',ISLASM,1)                                   
      CALL JOPEN('*ASM    ',ISLFCT,1)                                   
C                                                                       
c      JLIM=JILOC+MAXFRO-1                                              
c      CALL IZERO(IV(JIPIAZ),MAXFRO)                                    
c      CALL IZERO(IV(JIPIAZH),MAXFRO)                                   
c      CALL IZERO(IV(JILOC),MAXFRO)                                     
C                                                                       
      PRINT*,' ***   INIZIO FASE DI FATTORIZZAZIONE FUORI MEMORIA DEL SI
     1STEMA REALE  ***'                                                 
      CALL FATFRO(DV(JEQ),IV(JIPIAZ),IV(JPIAZH),IV(JILOC),NEQV,MAXFRO)  
      CALL JCLOSE(ISLFCT)                                               
C>>>  CALL JCLOSE(ISLASM)                                               
C                                                                       
      CALL JCRERS                                                       
      PRINT*,' ***   FINE FASE DI FATTORIZZAZIONE FUORI MEMORIA DEL SIST
     1EMA REALE  ***'                                                   
C                                                                       
      CALL JALLOC(*2000,'EQ      ','D.P.',1,MAXFRO      ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
C>>>  CALL JOPEN('*FCT    ',ISLFCT,1)                                   
      CALL JOPEN('*ASM    ',ISLFCT,1)                                   
      CALL JOPEN('*SL1    ',ISLSL1,1)                                   
      CALL JALLOC(*2000,'IFITT   ','INT.',1,1         ,JIFITT)          
      CALL JFREE(*2000,'IFITT   ')                                      
      MEMDIS=MAXDIM-JIFITT-5*7-2                                        
      NTNOTI=MEMDIS/(NEQV*2)                                            
      IF(NTNOTI.LE.0)GOTO40                                             
      IF(NTNOTI.GT.6)NTNOTI=6                                           
      NCICLI=(6/NTNOTI)+1                                               
C                                                                       
30    CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*NTNOTI ,JTNOTO)        
C                                                                       
C                                                                       
      PRINT*,' ***   INIZIO SOLUZIONE PRIMO PASSO  ***'                 
      CALL SLSTP1(DV(JTNOTO),DV(JEQ),IV(JIPIAZ),NTNOTI,NCICLI)          
      PRINT*,' ***   FINE                          ***'                 
      CALL JCLOSE(ISLSL1)                                               
      CALL JCLOSE(ISLFCT)                                               
C                                                                       
      CALL JCRERS                                                       
C                                                                       
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV         ,JTNOTO)       
      CALL JALLOC(*2000,'TNOTH   ','D.P.',1,NEQV         ,JTNOTH)       
      CALL JALLOC(*2000,'TNMAT   ','D.P.',1,MAXDEL*MAXDEL,JTNMAT)       
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL       ,JIPIAZ)       
C                                                                       
      CALL JOPEN('*MTN    ',ISLMTN,1)                                   
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                   
      CALL JOPEN('*SL1    ',ISLSL1,1)                                   
      CALL JOPEN('*TN2    ',ISLTN2,1)                                   
C                                                                       
      PRINT*,' ***   INIZIO COSTRUZIONE TERMINE NOTO PRIMO PASSO  ***'  
      CALL TNSTP2(DV(JTNOTH),DV(JTNOTO),DV(JTNMAT),IV(JIPIAZ))          
      PRINT*,' ***   FINE                                         ***'  
C                                                                       
      CALL JCLOSE(ISLTN2)                                               
      CALL JCLOSE(ISLSL1)                                               
      CALL JCLOSE(ISLVPZ)                                               
      CALL JCLOSE(ISLMTN)                                               
      CALL JCRERS                                                       
C                                                                       
      CALL JALLOC(*2000,'EQ      ','D.P.',1,MAXFRO      ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
      CALL JOPEN('*TN2    ',ISLTN2,1)                                   
      CALL JOPEN('*SL2    ',ISLSL2,1)                                   
      CALL JOPEN('*ASM    ',ISLFCT,1)                                   
      CALL JALLOC(*2000,'IFITT   ','INT.',1,1            ,JIFITT)       
      CALL JFREE(*2000,'IFITT   ')                                      
      MEMDIS=MAXDIM-JIFITT-5*7-2                                        
      NTNOTI=MEMDIS/(NEQV*2)                                            
      IF(NTNOTI.LE.0)GOTO40                                             
      IF(NTNOTI.GT.6)NTNOTI=6                                           
      NCICLI=(6/NTNOTI)+1                                               
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*NTNOTI ,JTNOTO)        
C                                                                       
      JLIM=JIPIAZ+MAXFRO-1                                              
      NTTN=NTNOTI                                                       
C                                                                       
      PRINT*,' ***   INIZIO SOLUZIONE SECONDO PASSO  ***'               
      CALL SLSTP2(DV(JTNOTO),DV(JEQ),IV(JIPIAZ),NEQV,NTNOTI,NCICLI)     
      PRINT*,' ***   FINE                            ***'               
C                                                                       
      CALL JCLOSE(ISLFCT)                                               
      CALL JCLOSE(ISLSL2)                                               
      CALL JCLOSE(ISLTN2)                                               
C                                                                       
      CALL JCRERS                                                       
C                                                                       
C                                                                       
C   ********************* PARTE PER TRAVE CURVA *******************     
C                                                                       
C                                                                       
      IF(IGEOM.EQ.1)THEN                                                
      NDIMEQ=MAXFRO*(MAXFRO+1)                                          
      CALL JALLOC(*2000,'EQ      ','D.P.',1,NDIMEQ*2    ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
      CALL JALLOC(*2000,'IPIAZH  ','INT.',1,MAXFRO      ,JPIAZH)        
      CALL JALLOC(*2000,'ILOC    ','INT.',1,MAXFRO      ,JILOC )        
      CALL JOPEN('*ASC    ',ISLCFC,1)                                   
C                                                                       
      JLIM=JILOC+MAXFRO-1                                               
      CALL IZERO(IV(JIPIAZ),JLIM-JIPIAZ+1)                              
      PRINT*,' ***   INIZIO FASE DI FATTORIZZAZIONE FUORI MEMORIA DEL ',
     1'SISTEMA COMPLESSO  ***'                                          
      CALL CFTFRO(DV(JEQ),IV(JIPIAZ),IV(JPIAZH),IV(JILOC),NEQV,MAXFRO)  
      CALL JCLOSE(ISLCFC)                                               
C                                                                       
      CALL JCRERS                                                       
C                                                                       
      PRINT*,' ***   FINE FASE DI FATTORIZZAZIONE FUORI MEMORIA DEL SIST
     1EMA COMPLESSO  ***'                                               
      CALL JALLOC(*2000,'EQ      ','D.P.',1,MAXFRO*2,JEQ)               
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO,  JIPIAZ)            
      CALL JOPEN('*ASC    ',ISLCFC,1)                                   
      CALL JOPEN('*SC1    ',ISLSC1,1)                                   
      CALL JALLOC(*2000,'IFITT   ','INT.',1,1        ,JIFITT)           
      CALL JFREE(*2000,'IFITT   ')   
      call jalmax(1,memdisp)                                            
cx      MEMDIS=MAXDIM-JIFITT-3*8                                        
      NTNOTI=MEMDISp/(NEQV*4)                                           
      IF(NTNOTI.LE.0)GOTO40                                             
      IF(NTNOTI.GT.6)NTNOTI=6                                           
cz      NCICLI=(6/NTNOTI)+1
      ncicli=nint(6/ntnoti+.49)                                         
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*NTNOTI*2 ,JTNOTO)      
C                                                                       
      PRINT*,' ***   INIZIO SOLUZIONE CMPLX PRIMO PASSO  ***'          
      CALL CSLTP1(DV(JTNOTO),DV(JEQ),IV(JIPIAZ),NTNOTI,NCICLI)          
      PRINT*,' ***   FINE                                         ***'  
      CALL JCLOSE(ISLSC1)                                               
      CALL JCLOSE(ISLCFC)                                               
C                                                                       
      CALL JCRERS                                                       
C                                                                       
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*2       ,JTNOTO)       
      CALL JALLOC(*2000,'TNOTH   ','D.P.',1,NEQV*2       ,JTNOTH)       
      CALL JALLOC(*2000,'TNMAT   ','D.P.',1,MAXDEL*MAXDEL*2,JTNMAT)     
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL       ,JIPIAZ)       
C                                                                       
      CALL JOPEN('*MTN    ',ISLMTN,1)                                   
      CALL JOPEN('*MTF    ',ISLMTF,1)                                   
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                   
      CALL JOPEN('*SC1    ',ISLSC1,1)                                   
      CALL JOPEN('*TC2    ',ISLTC2,1)                                   
C                                                                       
      PRINT*,' ***   INIZIO COSTRUZIONE T.NOTO CMPLX II PASSO  ***'     
      CALL CTNSTP(DV(JTNOTH),DV(JTNOTO),DV(JTNMAT),IV(JIPIAZ))          
      PRINT*,' ***   FINE                                      ***'     
C                                                                       
      CALL JCLOSE(ISLTC2)                                               
      CALL JCLOSE(ISLSC1)                                               
      CALL JCLOSE(ISLVPZ)                                               
      CALL JCLOSE(ISLMTF)                                               
      CALL JCLOSE(ISLMTN)                                               
C                                                                       
      CALL JCRERS                                                       
C                                                                       
      CALL JALLOC(*2000,'EQ      ','D.P.',1,MAXFRO*2    ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
      CALL JOPEN('*TC2    ',ISLTC2,1)                                   
      CALL JOPEN('*SC2    ',ISLSC2,1)                                   
      CALL JOPEN('*ASC    ',ISLCFC,1)                                   
      CALL JALLOC(*2000,'IFITT   ','INT.',1,1            ,JIFITT)       
      CALL JFREE(*2000,'IFITT   ')                                      
      MEMDIS=MAXDIM-JIFITT-3*8                                          
      NTNOTI=MEMDIS/(NEQV*4)                                            
      IF(NTNOTI.LE.0)GOTO40                                             
      IF(NTNOTI.GT.6)NTNOTI=6                                           
      NCICLI=(6/NTNOTI)+1                                               
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*NTNOTI*2 ,JTNOTO)      
C                                                                       
      JLIM=JIPIAZ+MAXFRO-1                                              
      NTTN=NTNOTI                                                       
C                                                     
      PRINT*,' ***   INIZIO SOLUZIONE CMPLX PRIMO PASSO  ***'          
      CALL CSLTP2(DV(JTNOTO),DV(JEQ),IV(JIPIAZ),NEQV,NTNOTI,NCICLI)     
      PRINT*,' ***   FINE                                ***'          
C                                                                       
      CALL JCLOSE(ISLCFC)                                               
      CALL JCLOSE(ISLSC2)                                               
      CALL JCLOSE(ISLTC2)                                               
C                                                                       
      CALL JCRERS                                                       
C                                                                       
      CALL JALLOC(*2000,'UO      ','D.P.',1,NEQV*2     ,JUO)            
      CALL JALLOC(*2000,'VO      ','D.P.',1,NEQV*2     ,JVO)            
      CALL RISULT(NEQV,DV(JUO),DV(JVO))                                 
      ENDIF                                                             
      RETURN                                                            
C                                                                       
C                                                                       
420   FORMAT(' *** AVVISO (MDSLCO): LA MEMORIA DISONIBILE NON E'' ',    
     1 'SUFFICIENTE PER CONTENERE UN TERMINE NOTO COMPLETO ***')        
40    WRITE(IOUT,420)                                                   
2000  CALL JHALT                                                        
      STOP                                                              
C                                                                       
      END                                                               
C@ELT,I ANBA*ANBA.MDSCTO                                                
C*************************          MDSCTO          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE MDSCTO                                                 
      IMPLICIT REAL*8(A-H,O-Z)                                          
      REAL*8 DV(1)                                                      
      LOGICAL DUMP,INCORE                                               
      COMMON IV(1)                                                      
      EQUIVALENCE (IV(1),DV(1))                                         
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,  
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO
      COMMON /JCRMNG/ MAXDIM                                            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISTEMP,ISMTER,ISTNOT                              
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                     
C                                                                       
C                                                                       
      CALL JCRERS                                                       
C                                                                       
      if(MODEX(3).eq.1) then                                            
      PRINT*,' ***   INIZIO FATTORIZZAZIONE FUORI MEMORIA DEL SIST'     
     1,'EMA  ***'                                                       
      NDIMEQ=MAXFRO*(MAXFRO+1)/2                                        
      DO 8754 iopl=1,maxfro*3+ndimeq*2                                  
8754  iv(iopl)=0                                                        
      CALL JALLOC(*2000,'EQ      ','D.P.',1,NDIMEQ      ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
      CALL JALLOC(*2000,'IPIAZH  ','INT.',1,MAXFRO      ,JPIAZH)        
      CALL JALLOC(*2000,'ILOC    ','INT.',1,MAXFRO      ,JILOC )        
C>>>  CALL JOPEN('*FCT    ',ISLFCT,1)                                   
C>>>  CALL JOPEN('*ASM    ',ISLASM,1)                                   
      CALL JOPEN('*ASM    ',ISLFCT,1)                                   
C                                                                       
      CALL FATFRO(DV(JEQ),IV(JIPIAZ),IV(JPIAZH),IV(JILOC),NEQV,MAXFRO)  
      CALL JCLOSE(ISLFCT)                                               
C>>>  CALL JCLOSE(ISLASM)                                               
      PRINT*,' ***   FINE FASE DI FATTORIZZAZIONE FUORI MEMORIA DEL SIST
     1EMA  ***'                                                         
      endif                                                             
      call jcrers                                                       
                                                                        
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV        ,JTNOTO)        
      CALL JALLOC(*2000,'TNOTH   ','D.P.',1,NEQV        ,JTNOTH)        
      CALL JALLOC(*2000,'TNMAT   ','D.P.',1,MAXDEL*MAXDEL,JTNMAT)       
      CALL JALLOC(*2000,'EQ      ','D.P.',1,MAXFRO*2    ,JEQ   )        
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXFRO      ,JIPIAZ)        
C                                                                       
C                                                                       
C  ----------------------------------------------------------           
C  INIZIO SOLUZIONE TERMICA                                             
C                                                                       
C     apertura slot: temperature assegnate (ISTEMP), matrici termiche (I
C     vettore termine noto termico (ISTNOT), vettore di piazzamento (ISL
C                                                                       
      CALL JOPEN('*TNT    ',ISTNOT,1)                                   
      CALL JOPEN('*TMP    ',ISTEMP,1)                                   
      CALL JOPEN('*MTT    ',ISMTER,1)                                   
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                   
C     chiama costruzione del vettore termine noto termico               
      CALL TNSLTR(DV(JTNOTH),DV(JTNOTO),DV(JTNMAT),IV(JIPIAZ))          
C                                                                       
      CALL JCLOSE(ISLVPZ)                                               
      CALL JCLOSE(ISMTER)                                               
      CALL JCLOSE(ISTEMP)                                               
      CALL JCLOSE(ISTNOT)                                               
      CALL JOPEN('*ASM    ',ISLFCT,1)                                   
      CALL FORBCK(DV(JTNOTO),DV(JEQ),IV(JIPIAZ),NEQV,1)                 
      CALL JCLOSE(ISLFCT)                                               
C                                                                       
      CALL JOPEN('*SLTERM ',ISLSLT,1)                                   
      CALL JWRITE(ISLSLT,DV(JTNOTO),NEQV*2)                             
      CALL JCLOSE(ISLSLT)                                               
C                                                                       
C  FINE SOLUZIONE TERMICA                                               
C  ---------------------------------------------------------            
C                                                                       
C                                                                       
      RETURN                                                            
C                                                                       
C                                                                       
420   FORMAT(' *** AVVISO (MDSLCO): LA MEMORIA DISONIBILE NON E'' ',    
     1 'SUFFICIENTE PER CONTENERE UN TERMINE NOTO COMPLETO ***')        
40    WRITE(IOUT,420)                                                   
2000  CALL JHALT                                                        
      STOP                                                              
C                                                                       
      END                                                               
C@ELT,I ANBA*ANBA.CFTFRO                                                
C*************************          CFTFRO          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE CFTFRO (EQ,IPIAZ,IPIAZH,ILOC,NEQ,MAXFRO)               
C                                                                       
C     ROUTINE DI FATTORIZZAZIONE DELLA MATRICE DEI COEFFICIENTI DI      
C     UN SISTEMA LINEARE SIMMETRICO COMPLESSO CON TECNICA FRONTALE      
C                                                                       
C         SIGNIFICATO DELLE VARIABILI                                   
C                                                                       
C     EQ     =   VETTORE DI LAVORO CORRISPONDENTE AD UNA MATRICE TRIANGO
C     IPIAZ  =   VETTORE CONTENENTE GLI INDICI DI PIAZZAMENTO DEI TERMIN
C     IPIAZH =   VETTORE MEMORIA DEI TERMINI DI PIAZZAMENTO E LORO POSIZ
C     ILOC   =   VETTORE PUNTATORE DELLA DIAGONALE                      
C                                                                       
C     MAXFRO =   MASSIMO FRONTE                                         
C     NEQ    =   NUMERO DI EQUAZIONI DEL SISTEMA                        
C     NT     =   NUMERO DI INCOGNITE DELL'EQUAZIONE CONSIDERATA         
C                                                                       
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      COMPLEX*16 EQ,Q,PIV,FAC                                           
      LOGICAL DUMP,incore                                               
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                islasc,islcfc,islsc1,isltc2,islsc2                
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                     
      DIMENSION EQ(1),IPIAZ(1),IPIAZH(1),ILOC(1)                        
      IERLOC=1                                                          
      ILOC(1)=0                                                         
      DO 2 K=2,MAXFRO                                                   
      ILOC(K)=ILOC(K-1)+MAXFRO-K+2                                      
2     CONTINUE                                                          
      I=1                                                               
C                                                                       
C     CICLO DI LETTURA FATTORIZZAZIONE E SCRITTURA FUORI MEMORIA        
C     DELLA PRIMA EQUAZIONE                                             
C                                                                       
      CALL JREAD(ISLCFC,NT,1)                                           
      CALL JREAD(ISLCFC,IPIAZ,NT)                                       
      IF(IPIAZ(1).NE.I)GOTO 1000                                        
      CALL JREAD(ISLCFC,EQ,NT*4)                                        
          CALL JREAD(ISLCFC,NW,1)                                       
          CALL JPOSRL(ISLCFC,-NW)                                       
      IF(DUMP(6))WRITE(IOUT,904)I,NT,(IPIAZ(KZ),KZ=1,NT)                
  904 FORMAT(' EQUAZ. N.RO ',I6,' DI ',I6,' TERMINI'/                   
     1  ' V.PIAZ',12I10/20(7X,12I10/))                                  
  905 FORMAT(' COEF.'/40(7X,12D10.4/))                                  
C                                                                       
C     FATTORIZZAZIONE DELLA PRIMA EQUAZIONE ED INVIO DEI CONTRIBUTI     
C                                                                       
C                                                                       
C     CONTROLLO SUL VALORE DEL TERMINE DIAGONALE                        
C                                                                       
CCC   IF(DABS(EQ(1)).GE.ERR) GOTO 25                                    
      Q=EQ(1)                                                           
      IF(CDABS(Q).GE.ERR) GOTO 25                                       
      WRITE(IOUT,205)I,EQ(1),ERR                                        
      EQ(1)=EQ(1)+CONST                                                 
      IERLOC=1                                                          
25    CONTINUE                                                          
      PIV = 1./EQ(1)                                                    
C     WRITE(6,3434)PIV                                                  
      IF(NT.LT.2) GOTO 17                                               
      DO 10 K=2,NT                                                      
      FAC=DCONJG(EQ(K))*PIV                                             
      NLIM=NT-K+1                                                       
      DO 20 L=1,NLIM                                                    
      EQ(ILOC(K)+L)=-EQ(L+K-1)*FAC                                      
20    CONTINUE                                                          
      EQ(K)=DCONJG(FAC)                                                 
10    CONTINUE                                                          
17    CONTINUE                                                          
C                                                                       
C     SCRITTURA SU FILE DELLA PRIMA EQUAZIONE                           
C                                                                       
      NWORD=2+NT*5                                                      
      CALL JWRITE(ISLCFC,NT,1)                                          
      CALL JWRITE(ISLCFC,IPIAZ,NT)                                      
      CALL JWRITE(ISLCFC,EQ,NT*4)                                       
      CALL JWRITE(ISLCFC,NWORD,1)                                       
      IF(DUMP(6))WRITE(IOUT,905)(EQ(KZ),KZ=1,NT)                        
C                                                                       
C     CICLO DI LETTURA FATTORIZZAZIONE E SCRITTURA FUORI MEMORIA        
C     DELLE RESTANTI NEQ-1 EQUAZIONI                                    
C                                                                       
      DO 30 I=2,NEQ                                                     
      NTH=NT-1                                                          
C                                                                       
C     MEMORIZZAZIONE DEL VETTORE DI PIAZZAMENTO DELL'EQUAZIONE  I-1     
C                                                                       
      IF(NTH.EQ.0) GOTO 40                                              
      DO 190 IS=1,NTH                                                   
      IPIAZH(IS)=IPIAZ(IS+1)                                            
190   CONTINUE                                                          
C                                                                       
C     LETTURA DELLA  I-ESIMA EQUAZIONE                                  
C                                                                       
 40   CALL JREAD(ISLCFC,NT,1)                                           
      CALL JREAD(ISLCFC,IPIAZ,NT)                                       
C                                                                       
C     TEST PER LA VERIFICA DELLA PRESENZA DEL TERMINE DIAGONALE         
C                                                                       
      CALL JREAD(ISLCFC,EQ,NT*4)                                        
         CALL JREAD(ISLCFC,NW,1)                                        
         CALL JPOSRL(ISLCFC,-NW)        
      IF(DUMP(6))WRITE(IOUT,904)I,NT,(IPIAZ(KZ),KZ=1,NT) 
      IF(IPIAZ(1).NE.I)GOTO 1000
C
C      CARICAMENTO IN -IPIAZH- DELLE POSIZIONI CHE LE INCOGNITE DELLA  
C     EQUAZIONE PRECEDENTE ASSUMONO IN QUELLA ATTUALE 
C
801   CONTINUE
      IF(NTH.EQ.0)GOTO 65
      DO 35 M=1,NTH
      IPIAZH(M)=IPOSZ(IPIAZH(M),IPIAZ,NT)
35    CONTINUE
      IF(IPIAZH(1).NE.1)GOTO 65
      ILL=ILOC(2)
C                                                                       
C     SOMMA ALL'EQUAZIONE I-ESIMA DEI CONTRIBUTI AD ESSA DESTINATI      
C     E CHE SI TROVANO NELLA SECONDA RIGA DELLA MATRICE                 
C                                                                       
      DO 50 K=1,NTH                                                     
      IP=IPIAZH(K)                                                      
      EQ(IP)=EQ(IP)+EQ(ILL+K)                                           
50    CONTINUE                                                          
 907  FORMAT(24I5)                                                      
      KK=2                                                              
      GOTO 66                                                           
65    KK=1                                                              
C                                                                       
C     FATTORIZZAZIONE DELLA EQUAZIONE I-ESIMA                           
C                                                                       
66    CONTINUE                                                          
CCC   IF(DABS(EQ(1)).GE.ERR) GOTO 28                                    
      Q=EQ(1)                                                           
      IF(CDABS(Q).GE.ERR) GOTO 28                                       
      WRITE(IOUT,205)I,EQ(1),ERR                                        
      EQ(1)=EQ(1)+CONST                                                 
      IERLOC=1                                                          
28    CONTINUE                                                          
      PIV=1./EQ(1)                                                      
      IF(KK.GT.NTH) GOTO 105
      DO 70 L=KK,NTH                                                    
      IR=IPIAZH(L)                                                      
      IF(IR.GT.L)GOTO 75                                                
      FAC=DCONJG(EQ(IR))*PIV                                            
      ISUM=L-1                                                          
      ILR=ILOC(IR)-IR+1                                                 
      ILL=ILOC(L+1)                                                     
C                                                                       
C     CICLO DI INVIO DEI CONTRIBUTI DI TUTTE LE INCOGNITE DELLA NUOVA   
C     EQUAZIONE FINO A QUELLO CHE COMPARE PER LA PRIMA VOLTA            
C                                                                       
      NTS=NTH-ISUM                                                      
      IF(NTS.LT.1) GOTO 70                                              
      DO 80 K3=1,NTS                                                    
      IC=IPIAZH(K3+ISUM)                                                
      EQ(ILR+IC)=EQ(ILL+K3)-EQ(IC)*FAC                                  
80    CONTINUE                                                          
 70   CONTINUE                                                          
      GOTO 105                                                          
C                                                                       
C     CICLO DI INVIO DEI CONTRIBUTI DELLE INCOGNITE GIA' PRESENTI NELLA 
C     EQUAZIONE PRECEDENTE A PARTIRE DALL'ULTIMO                        
C                                                                       
75    IF(NTH.LT.L)GOTO 91                                               
      DO 90 IM=L,NTH                                                    
      M=NTH-IM+L                                                        
      IR=IPIAZH(M)                                                      
      FAC=DCONJG(EQ(IR))*PIV                                            
      ISUM=M-1                                                          
      ILR=ILOC(IR)-IR+1                                                 
      ILL=ILOC(M+1)                                                     
      NLIM=NTH-ISUM                                                     
      IF(NLIM.LT.1)GOTO 101                                             
      DO 100 K4=1,NLIM                                                  
      K3=NLIM-K4+1                                                      
                                                                        
      IC=IPIAZH(K3+ISUM)                                                
      EQ(ILR+IC)=EQ(ILL+K3)-EQ(IC)*FAC                                  
100   CONTINUE                                                          
101   CONTINUE                                                          
90    CONTINUE                                                          
91    CONTINUE                                                          
105   IND2=NTH                                                          
C                                                                       
C     CARICAMENTO NELLE POSIZIONI DI  IPIAZH  SUCCESSIVE ALLE           
C     PRIME  NTH  DELLE POSIZIONI OCCUPATE IN  IPIAZ  DALLE INCOGNITE   
C     DI NUOVO INSERIMENTO                                              
C                                                                       
501   IF(NT.LT.2) GOTO 601                                              
      DO 110 K=2,NT                                                     
      IP=IPOSZ(K,IPIAZH,NTH)                                            
      IF(IP.NE.0)GOTO 110                                               
      IND2=IND2+1                                                       
      IPIAZH(IND2)=K                                                    
110   CONTINUE                                                          
      IDO=1                                                             
      IF(IPIAZH(1).EQ.1)IDO=2                                           
C                                                                       
C     CICLO DI INVIO DEI CONTRIBUTI ALLE RIGHE CORRISPONDENTI           
C     ALLE INCOGNITE DI NUOVO INSERIMENTO                               
C                                                                       
      IF(IDO.GT.NTH)GOTO 121                                            
      DO 120 L=IDO,NTH                                                  
      IND=NTH+1                                                         
      IR=IPIAZH(L)                                                      
      FAC=DCONJG(EQ(IR))*PIV                                            
125   CONTINUE                                                          
      IF(IR.LE.IPIAZH(IND))GOTO 130                                     
      IND=IND+1                                                         
      GOTO 125                                                          
130   JINF=IND                                                          
      ILR=ILOC(IR)-IR+1                                                 
      IF(JINF.GT.IND2)GOTO 120                                          
      DO 140 K=JINF,IND2                                                
      ILL=IPIAZH(K)                                                     
      EQ(ILR+ILL)=-EQ(ILL)*FAC                                          
140   CONTINUE                                                          
120   CONTINUE                                                          
121   CONTINUE                                                          
C                                                                       
C     CICLO DI INVIO DEI CONTRIBUTI NELLE COLONNE CORRISPONDENTI        
C     ALLE INCOGNITE INSERITI CON QUESTA EQUAZIONE                      
C                                                                       
      IF(IND2.EQ.NTH) GOTO 601                                          
      NLIM=1+NTH                                                        
      IF(NLIM.GT.IND2)GOTO 168                                          
      DO 165 K=NLIM,IND2                                                
      IR=IPIAZH(K)                                                      
      FAC=DCONJG(EQ(IR))*PIV                                            
      ISUM=IR-1                                                         
      ILR=ILOC(IR)-ISUM                                                 
      IF(IR.GT.NT)GOTO 171                                              
      DO 170 L=IR,NT                                                    
      EQ(ILR+L)=-EQ(L)*FAC                                              
170   CONTINUE                                                          
171   CONTINUE                                                          
165   CONTINUE                                                          
168   CONTINUE                                                          
C                                                                       
C     RIDUZIONE DELLE INCOGNITE DELLA I-ESIMA EQUAZIONE                 
C                                                                       
 601  CONTINUE                                                          
      IF(NT.LT.2)GOTO 176                                               
      DO 175 MI=2,NT                                                    
      EQ(MI)=EQ(MI)*PIV                                                 
175   CONTINUE                                                          
176   CONTINUE                                                          
C                                                                       
C     SCRITTURA SU FILE DELLA EQUAZIONE I-ESIMA                         
C                                                                       
      NWORD=2+5*NT                                                      
      CALL JWRITE(ISLCFC,NT,1)                                          
      CALL JWRITE(ISLCFC,IPIAZ,NT)                                      
      CALL JWRITE(ISLCFC,EQ,NT*4)                                       
      CALL JWRITE(ISLCFC,NWORD,1)                                       
      IF(DUMP(6))WRITE(IOUT,905)(EQ(KZ),KZ=1,NT)                        
30    CONTINUE                                                          
      IF(IERLOC.NE.0.AND.IAUTO.EQ.1) THEN                               
      WRITE(IOUT,1300)                                                  
      WRITE(IOUT,*)' IERLOC',IERLOC,'    IAUTO',IAUTO                   
      CALL JCLOSE(ISLCFC)                                               
C+    CALL JCLOSE(ISLASC)                                               
      CALL JHALT                                                        
      STOP                                                              
 1300 FORMAT(/' ***   AVVISO (CFTFRO) : IL CALCOLO NON PROSEGUE',       
     * ' A CAUSA DELLE LABILITA'' RISCONTRATE   ***'/)                  
      END IF                                                            
      RETURN                                                            
1000  WRITE(IOUT,1200)I,IPIAZ(1)                                        
1200  FORMAT(/' ***   AVVISO (CFTFRO): NON PRESENTE IL TERMINE ',       
     1 'DIAGONALE DELL''EQUAZIONE N.RO',I8,'   ***'/)                   
      CALL JHALT                                                        
      STOP                                                              
205   FORMAT(/' *** AVVISO (CFTFRO): ESEGUITO AUTOSPC SU'               
     1 ,' EQ. N.RO',I6,' IL CUI TERM. DIAG.',E12.6,'  E'' MINORE DI'    
     2   ,E12.6,'  ***')                                                
      END                                                               
C@ELT,I ANBA*ANBA                                                       
C*************************          CFRBCK          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE CFRBCK(TNOTO,EQ,IPIAZ,NEQ,NTNOTI)                      
C                                                                       
C                                                                       
C     SOLUZIONE DI UN SISTEMA LINEARE LA CUI MATRICE DI COEFFICIENTI    
C     E' STATA FATTORIZZATA CON TECNICA FRONTALE DALLA ROUTINE -FATFRO- 
C                                                                       
C     LA SOLUZIONE VIENE ESEGUITA CONTEMPORANEAMENTE PER -NTNOTI-       
C     TERMINI NOTI                                                      
C                                                                       
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      COMPLEX*16 TNOTO,RHS,ACC,EQ                                       
      DIMENSION EQ(1),IPIAZ(1),TNOTO(1)                                 
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
C                                                                       
C     SOSTITUZIONE FRONTALE IN AVANTI                                   
C                                                                       
      DO 10 I=1,NEQ                                                     
      CALL JREAD(ISLCFC,NT,1)                                           
      CALL JREAD(ISLCFC,IPIAZ,NT)                                       
      CALL JREAD(ISLCFC,EQ,NT*4)                                        
      CALL JREAD(ISLCFC,NWORD,1)                                        
      DO 15 J=1,NTNOTI                                                  
      JJ=NEQ*(J-1)                                                      
      RHS=TNOTO(IPIAZ(1)+JJ)                                            
      IF(NT.EQ.1)GOTO 25                                                
      DO 20 K=2,NT                                                      
      IND=IPIAZ(K)+JJ                                                   
      TNOTO(IND)=TNOTO(IND)-DCONJG(EQ(K))*RHS                           
20    CONTINUE                                                          
25    TNOTO(JJ+I)=TNOTO(JJ+I)/EQ(1)                                     
15    CONTINUE                                                          
10    CONTINUE                                                          
C                                                                       
C     SOSTITUZIONE FRONTALE ALL'INDIETRO                                
C                                                                       
      CALL JPOSRL(ISLCFC,-1)                                            
      CALL JREAD(ISLCFC,NWORD,1)                                        
      CALL JPOSRL(ISLCFC,-NWORD-1)                                      
      DO 50 I=2,NEQ                                                     
      CALL JREAD(ISLCFC,NWORD,1)                                        
      CALL JPOSRL(ISLCFC,-NWORD)                                        
      CALL JREAD(ISLCFC,NT,1)                                           
      CALL JREAD(ISLCFC,IPIAZ,NT)                                       
      CALL JREAD(ISLCFC,EQ,NT*4)                                        
      CALL JPOSRL(ISLCFC,-NWORD)                                        
      DO 55 J=1,NTNOTI                                                  
      ACC=DCMPLX(0,0)                                                   
      JJ=NEQ*(J-1)                                                      
      IF(NT.EQ.1)GOTO 65                                                
      DO 60 K=2,NT                                                      
      IND=IPIAZ(K)+JJ                                                   
      ACC=ACC+TNOTO(IND)*EQ(K)                                          
60    CONTINUE                                                          
65    IND=IPIAZ(1)+JJ                                                   
      TNOTO(IND)=TNOTO(IND)-ACC                                         
55    CONTINUE                                                          
50    CONTINUE                                                          
      RETURN                                                            
      END                                                               
C@ELT,I ANBA*ANBA.SLSTP1                                                
C*************************          SLSTP1          *************MOD90  
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE SLSTP1(TNOTO,EQ,IPIAZ,NTNOTI,NCICLI)                   
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      LOGICAL DUMP,incore                                               
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,   
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                     
      DIMENSION TNOTO(1),EQ(1),IPIAZ(1)                                 
      DIMENSION CBO(6,6),TEMPO(6,6),ELLE(6,6),ELLE1(6,6),ELLE2(6,6)     
      CALL DZERO(TEMPO,36)                                              
      CALL DZERO(CBO,36)                                                
      CALL ITELLE(1,CDC,ELLE,1.D0)                                      
      DO 6 I=1,6                                                        
      TEMPO(I,I)=TEMPO(I,I)+1.D0                                        
 6    CONTINUE                                                          
      IF(IGEOM.EQ.1)THEN                                                
      CALL ITELLE(1,CDC,ELLE,C)                                         
      CALL ITELLE(2,CDC,ELLE1,C)                                        
      CALL ITELLE(4,CDC,ELLE2,C)                                        
      DO 5 I=1,6                                                        
      DO 5 K=1,6                                                        
      TEMPO(I,K)=TEMPO(I,K)+2*ELLE1(I,K)+ELLE2(I,K)                     
 5    CONTINUE                                                          
      ENDIF                                                             
      DO 7 I=1,6                                                        
      DO 7 K=1,6                                                        
      DO 7 L=1,6                                                        
      CBO(I,K)=CBO(I,K)+ELLE(I,L)*TEMPO(L,K)                            
 7    CONTINUE                                                          
      IF(DUMP(4))THEN                                                   
          WRITE(IOUT,'(/,A)')' MATRICE DEL TNOTO DEL PRIMO PASSO IN SLST
     *P1'                                                               
       DO 40 I=1,6                                                      
          WRITE(IOUT,'(1X,6E12.6)')(CBO(I,K),K=1,6)                     
40     CONTINUE                                                         
      ENDIF                                                             
      IWW=NNODI*NCS+NMPC                                                
      LI=0                                                              
      IK=0                                                              
      DO 10 K=1,NCICLI                                                  
      IF(K.EQ.NCICLI)NTNOTI=6-(NCICLI-1)*NTNOTI                         
      CALL DZERO(TNOTO,NTNOTI*NEQV)                                     
      DO 20 I=1,NTNOTI                                                  
      LI=LI+1                                                           
      DO 30 L=1,6                                                       
      IW=IWW+(I-1)*NEQV+L                                               
      TNOTO(IW)=TNOTO(IW)+CBO(L,LI)                                     
 30   CONTINUE                                                          
 20   CONTINUE                                                          
      CALL FORBCK(TNOTO,EQ,IPIAZ,NEQV,NTNOTI)                           
      IF(DUMP(2))THEN                                                   
      DO 50 J=1,NTNOTI                                                  
      NN=NEQV*(J-1)                                                     
      IK=IK+1                                                           
      NINF=NN+1                                                         
      NSUP=NEQV+NN                                                      
      WRITE(IOUT,100)IK,(TNOTO(JJ),JJ=NINF,NSUP)                        
50    CONTINUE                                                          
      ENDIF                                                             
      CALL JWRITE(ISLSL1,TNOTO,NTNOTI*NEQV*2)                           
10    CONTINUE                                                          
100   FORMAT(' SOLUZIONE DEL PRIMO PASSO CON TNOTO N.RO: ',I4,/,        
     * (T5,10E12.5/))                                                   
      RETURN                                                            
      END                                                               
C@ELT,I ANBA*ANBA.SLSTP2                                                
C*************************          SLSTP2          *************MOD90  
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE SLSTP2(TNOTO,EQ,IPIAZ,NEQV,NTNOTI,NCICLI)              
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      LOGICAL DUMP,incore                                               
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      DIMENSION TNOTO(1),EQ(1),IPIAZ(1)                                 
      NT=NTNOTI                                                         
      IK=0                                                              
      DO 10 K=1,NCICLI                                                  
      IF(K.EQ.NCICLI) NTNOTI=6-(NCICLI-1)*NTNOTI                        
      CALL JREAD(ISLTN2,TNOTO,NEQV*NTNOTI*2)                            
      CALL FORBCK(TNOTO,EQ,IPIAZ,NEQV,NTNOTI)                           
      CALL JWRITE(ISLSL2,TNOTO,NEQV*NTNOTI*2)                           
      IF(DUMP(2)) THEN                                                  
          DO 80 J=1,NTNOTI                                              
          NN=NEQV*(J-1)	                                                
          IK=IK+1                                                       
          NINF=NN+1                                                     
          NSUP=NEQV+NN                                                  
          WRITE(IOUT,100)IK,(TNOTO(IO),IO=NINF,NSUP)                    
80        CONTINUE                                                      
      END IF                                                            
100   FORMAT('  SOLUZIONE DEL SECONDO PASSO PARTE REALE',/,             
     1  '   TNOTO N.RO:',I4,/,(T6,10E12.5/))                            
10    CONTINUE                                                          
      RETURN                                                            
      END                                                               
C@ELT,I ANBA*ANBA.CSLTP1                                                
C*************************          CSLTP1          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
C                                                                       
      SUBROUTINE CSLTP1(TNOTO,EQ,IPIAZ,NTNOTI,NCICLI)                   
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      COMPLEX*16 TNOTO(1),EQ(1),B1(6,6)                                 
      LOGICAL DUMP,INCORE                                               
      DIMENSION ELLE(6,6),ELLE1(6,6),IPIAZ(1)                           
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,   
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C     
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLAC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                 
C                                                                       
      do i=1,6
      do l=1,6
      b1(i,l)=dcmplx(0.d0,0.d0)
      enddo
      enddo             
      write(iout,'(a,6f8.5,e12.5)') ' cdc ',cdc,c             
      write(iout,*) 'ncicli',ncicli,'     ntnoti',ntnoti
c      CALL DZERO(B1,72)                                                
      CALL ITELLE(3,CDC,ELLE,C)  
      write(iout,*) ' elle 3'          
      write(iout,'(6e12.5)') elle                                     
      CALL ITELLE(2,CDC,ELLE1,C)                                        
      write(iout,*) ' elle 2'          
      write(iout,'(6e12.5)') elle1                                     
      DO 30 I=1,6                                                       
      DO 30 K=1,6                                                       
      B1(I,K)=DCMPLX(0.25*ELLE(I,K),0.25*ELLE1(I,K))                    
  30  CONTINUE                                                          
      CALL ITELLE(5,CDC,ELLE,C)   
      write(iout,*) ' elle 5'          
      write(iout,'(6e12.5)') elle                                       
      CALL ITELLE(4,CDC,ELLE1,C)                                        
      write(iout,*) ' elle 4'          
      write(iout,'(6e12.5)') elle1                                     
      DO 40 I=1,6                                                       
      DO 40 K=1,6                                                       
      B1(I,K)=B1(I,K)+DCMPLX(.25*ELLE(I,K),.25*ELLE1(I,K))              
  40  CONTINUE                                                          
CC    WRITE(IOUT,'(/,A)') ' B1 IN CSLTP1 PARTE REALE'                   
CC    WRITE(IOUT,'(1X,6E12.6)') ((DREAL(B1(IO,IC)),IC=1,6),IO=1,6)      
CC    WRITE(IOUT,'(/,A)') ' B1 IN CSLTP1 PARTE IMMAGINARIA'             
CC    WRITE(IOUT,'(1X,6E12.6)') ((DIMAG(B1(IO,IC)),IC=1,6),IO=1,6)      
CC    WRITE(IOUT,'(/,A)')' MATRICE K-I*C*H+C**2*M 30*30',               
CC   1' PARTE REALE IN CSLTP1'                                          
CC    WRITE(IOUT,'(1X,6E12.6)')((DREAL(RK(IO,IC)),IC=1,NEQV),IO=1,NEQV) 
CC    WRITE(IOUT,'(/,A)')' MATRICE K-I*C*H+C**2*M 30*30 PARTE'          
CC   1' IMMAGINARIA IN CSLTP1'                                          
CC    WRITE(IOUT,'(1X,6E12.6)')((DIMAG(RK(IO,IC)),IC=1,NEQV),IO=1,NEQV) 
cx      IF(DUMP(4))THEN                                                 
          WRITE(IOUT,'(/,A)')' MATRICE DEL TERMINE NOTO REALE IN CSLTP1'
          DO 43 IR=1,6                                                  
          WRITE(IOUT,200)(DREAL(B1(IR,IO)),IO=1,6)                      
43        CONTINUE                                                      
          WRITE(IOUT,'(/,A)')' MATRICE DEL TERMINE NOTO IMMAGINARIO IN  
     *CSLTP1'                                                           
          DO 44 IR=1,6                                                  
          WRITE(IOUT,200)(DIMAG(B1(IR,IO)),IO=1,6)                      
44        CONTINUE                                                      
cx      ENDIF                                                           
      IWW=NNODI*NCS                                                     
      LI=0                                                              
      IK=0                                                              
      DO 50 K=1,NCICLI                                                  
      IF(K.EQ.NCICLI)NTNOTI=6-(NCICLI-1)*NTNOTI                         
      CALL DZERO(TNOTO,NTNOTI*NEQV*2)                                   
      DO 60 I=1,NTNOTI                                                  
      LI=LI+1                                                           
      DO 70 L=1,6                                                       
      IW=IWW+(I-1)*NEQV+L                                               
      TNOTO(IW)=TNOTO(IW)+B1(L,LI)                                      
70    CONTINUE                                                          
60    CONTINUE                           
      print*,' call cfrbck'                                       
      CALL CFRBCK(TNOTO,EQ,IPIAZ,NEQV,NTNOTI)  
      print*,' exit cfrbck'                                 
      CALL JWRITE(ISLSC1,TNOTO,NTNOTI*NEQV*4)                           
      IF(DUMP(2)) THEN                                                  
          DO 80 J=1,NTNOTI                                              
          NN=NEQV*(J-1)	                                                
          IK=IK+1                                                       
          NINF=NN+1                                                     
          NSUP=NEQV+NN                                                  
          WRITE(IOUT,100)IK,(TNOTO(IO),IO=NINF,NSUP)                    
80        CONTINUE                                                      
      END IF                                                            
 50   CONTINUE                                                          
100   FORMAT('  SOLUZIONE DEL PRIMO PASSO PARTE COMPLESSA',/,           
     1  '   TNOTO N.RO:',I4,/,(T5,10E12.5/))                            
200   FORMAT(6(4X,6E12.5))                                              
      RETURN                                                            
C                                                                       
      END                                                               
C                                                                       
C@ELT,I ANBA*ANBA.CSLTP2                                                
C*************************          CSLTP2          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
C                                                                       
      SUBROUTINE CSLTP2(TNOTO,EQ,IPIAZ,NEQV,NTNOTI,NCICLI)              
C                                                                       
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      COMPLEX*16 TNOTO,EQ                                               
      LOGICAL DUMP,INCORE                                               
      DIMENSION EQ(1),IPIAZ(1),TNOTO(1)                                 
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA              
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLAC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                 
      IK=0                                                              
      DO 10 K=1,NCICLI                                                  
      IF(K.EQ.NCICLI) NTNOTI=6-(NCICLI-1)*NTNOTI                        
      CALL JREAD(ISLTC2,TNOTO,NEQV*NTNOTI*4)                            
      CALL CFRBCK(TNOTO,EQ,IPIAZ,NEQV,NTNOTI)                           
      CALL JWRITE(ISLSC2,TNOTO,NTNOTI*NEQV*4)                           
      IF(DUMP(2)) THEN                                                  
          DO 80 J=1,NTNOTI                                              
          NN=NEQV*(J-1)	                                                
          IK=IK+1                                                       
          NINF=NN+1                                                     
          NSUP=NEQV+NN                                                  
          WRITE(IOUT,100)IK,(TNOTO(IO),IO=NINF,NSUP)                    
80        CONTINUE                                                      
      END IF                                                            
10    CONTINUE                                                          
100   FORMAT('  SOLUZIONE DEL SECONDO PASSO PARTE COMPLESSA',/,         
     1  '   TNOTO N.RO:',I4,/,(T5,10E12.5/))                            
      RETURN                                                            
C                                                                       
      END                                                               
C                                                                       
C@ELT,I ANBA*ANBA.RISULT                                                
C*************************          RISULT          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE RISULT(NEQV,UO,VO)                                     
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      COMPLEX*16 CMPLXU,CMPLXV                                          
      LOGICAL DUMP,INCORE                                               
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C     
      COMMON /JFTNIO / INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA             
      DIMENSION UO(NEQV),VO(NEQV)                                       
C                                                                       
      CALL JOPEN('*SL1   ',ISLSL1,1)                                    
      CALL JOPEN('*SL2   ',ISLSL2,1)                                    
      CALL JOPEN('*SC1   ',ISLSC1,1)                                    
      CALL JOPEN('*SC2   ',ISLSC2,1)                                    
      DO 20 I=1,6                                                       
      CALL DZERO(UO,NEQV)                                               
      CALL DZERO(VO,NEQV)                                               
      DO 30 K=1,NEQV                                                    
      CALL JREAD(ISLSC1,CMPLXV,4)                                       
      CALL JREAD(ISLSC2,CMPLXU,4)                                       
      CALL JREAD(ISLSL1,VO(K),2)                                        
      CALL JREAD(ISLSL2,UO(K),2)                                        
      VO(K)=VO(K)+2*C*DREAL(CMPLXV)-2*C*DIMAG(CMPLXU)                   
      UO(K)=UO(K)+2*DREAL(CMPLXU)                                       
 30   CONTINUE                                                          
      CALL JPOSRL(ISLSL1,-NEQV*2)                                       
      CALL JWRITE(ISLSL1,VO,NEQV*2)                                     
      CALL JPOSRL(ISLSL2,-NEQV*2)                                       
      CALL JWRITE(ISLSL2,UO,NEQV*2)                                     
 20   CONTINUE                                                          
      CALL JCLOSE(ISLSC1)                                               
      CALL JCLOSE(ISLSC2)                                               
      CALL JCLOSE(ISLSL1)                                               
      CALL JCLOSE(ISLSL2)                                               
C                                                                       
      RETURN                                                            
      END                                                               

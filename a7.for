C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MAINA2                                                        
C*************************           MAIN           ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C **********************************************************************        
C **********************************************************************        
C                                                                               
C                                                                               
C                         PROGRAMMA "ANBA-2"                                    
C                                                                               
C                                                                               
C **********************************************************************        
C **********************************************************************        
C                                                                               
C     PROGRAMMA PRINCIPALE  -  MAIN                                             
C                                                                               
C **********   LIBRERIA   **********************************************        
C                                                                               
C     AZZERA  CHKPNT  DPROMM  DWRITE  FACTOR  FEMADS  FORBAC  INTEST            
C     INVERT  IPOSL   IPOSZ   MOVE    PRIPAG  RCPSOL  RUFCT   RUSOL             
C     SHLSRT  WRSOLC                                                            
C                                                                               
C **********   INGRESSO DATI   *****************************************        
C                                                                               
C     MODINP                                                                    
C                                                                               
C     INPCOR  INPELP  INPGIU  INPLAM  INPMPC  INPNOD  INPPAN  INPRIN            
C     INPVIN  INPUTC  INPUTE  INPUTG  INPUTL  INPUTM  INPUTN  INPUTP            
C     INPUT0  MATMUL  MAXIM   MEDCAR  MPCORD  PRNUME  RCPCON  RCPNOD            
C     READFL  TRASF   TRASFM                                                    
C                                                                               
C ***********   COSTRUZIONE MATRICI ED ASSEMBLAGGIO   ******************        
C                                                                               
C     MODASS                                                                    
C                                                                               
C     ASMASS  ASSELM  ASSFRO  ASSMPC  BMAT    CAPPA   DABCT3  DAT2D             
C     FORLAM  FORM2D  INCASB  JAC2D   LMAT    MATCOR  MATELP  MATGIU            
C     MATLAM  MATPAN  MATRIX  OUCASB  PREFRO  PRETAB  VTTPZZ  WROUTE            
C                                                                               
C ***********   FATTORIZZAZIONE E SOLUZIONE DEI SISTEMI   **************        
C                                                                               
C     MDSLCI  MDSLCO          FATFRO  FORBCK  SLSTP1  SLSTP2  TNSTP2            
C                                                                               
C ***********   CALCOLO CARATTERISTICHE DI SEZIONE   *******************        
C                                                                               
C     MODCAR          CHANGE  COSFLX  CRSTIF          SOLMDF                    
C                                                                               
C ***********   RECUPERO SFORZI PER SOLUZIONI CENTRALI   ***************        
C                                                                               
C     MODSFC          CALSFC  PRTSFC  RIGEOM                                    
C                                                                               
C ***********   SOLUZIONI DI ESTREMITA'   ******************************        
C                                                                               
C     MD1SLE  MD2SLE                                                            
C                                                                               
C     ADDT    AUTVET  AUVALR  AUVLAS  AUVETM  AUTVET  DCUSOL  DCUFCT            
C     DHSN    DQR2    FEMADA  LANCZO  MATDMP  MSKYV   MULV    ORDINA            
C     ORTSCL  QR2TRD  RANDOM  SCALAR  SUBV                                      
C                                                                               
C ***********   RECUPERO SFORZI SOLUZIONI DI ESTREMITA'   **************        
C                                                                               
C     MODSFE          SFECOR  SFEELP  SFEGIU  SFELAM  SFEPAN                    
C                                                                               
C **********************************************************************        
C                                                                               
C                                                                               
      PROGRAM ANBA2                                                             
chp      PROGRAM ANBA2(nomefile)                                                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      character*64 nomefile                                                     
      LOGICAL DUMP,INCORE                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /SISRIF/ XS,YS,SS,CS,sclx,scly,IFLSR                               
      COMMON /AUTCON/ ERR,CONST,Xz,Yz,Sz,Cz,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      data nomefile/'none'/                                                     
C                                                                               
C                                                                               
      CALL ERRSET(208,256,-1,1,1,1)                                             
      CALL ERRSET(209,256,-1,1,1,1)                                             
C                                                                               
      CALL RDCASE(nomefile,*800)                                                
      PRINT*,' ***   TERMINATA LETTURA DATI DI CONTROLLO   ***'                 
C                                                                               
      CALL INPUT0                                                               
      PRINT*,' ***   TERMINATA FASE DI COPIATURA DATI   ***'                    
C                                                                               
      IF(MODEX(1).EQ.0) then                                                    
      CALL MODINP                                                               
      PRINT*,' ***   TERMINATA FASE INPUT DATI   ***'                           
      endif                                                                     
C                                                                               
      IF(MODEX(2).EQ.0) then                                                    
      CALL MODASS                                                               
      PRINT*,' ***   TERMINATA FASE ASSEMBLAGGIO  ***'                          
      endif                                                                     
C                                                                               
C      CALL JPRFIL                                                              
      IF(MODEX(3).EQ.0) THEN                                                    
C                                                                               
       IF(INCORE) THEN                                                          
        CALL MDSLCI                                                             
       ELSE                                                                     
        CALL MDSLCO                                                             
       END IF                                                                   
      PRINT*,' ***   TERMINATA FASE FATTORIZZAZIONE E SOLUZIONE  ***'           
C      CALL JPRFIL                                                              
C                                                                               
      IF((DUMP(7) .AND. MODEX(4).EQ.1) .OR. DUMP(2)) THEN                       
	CALL WRSOLC                                                                    
      ENDIF                                                                     
C                                                                               
      END IF                                                                    
C                                                                               
      IF(DUMP(7)) THEN                                                          
c      CALL PCHSOL('*SL2    ',6)                                                
      call wrsolnas                                                             
      ENDIF                                                                     
      IF(MODEX(4).EQ.0) then                                                    
      CALL MODCAR                                                               
      PRINT*,' ***   TERMINATA FASE DI CALCOLO MATRICI SEZIONE  ***'            
      endif                                                                     
C                                                                               
      IF(MODEX(5).EQ.0)then                                                     
      CALL MODSFC                                                               
      PRINT*,' ***   TERMINATA FASE DI CALCOLO DEGLI SFORZI  ***'               
      endif                                                                     
C                                                                               
      IF(MODEX(8).EQ.0) THEN                                                    
       IF(INCORE) THEN                                                          
        CALL MDSCTI                                                             
       ELSE                                                                     
        CALL MDSCTO                                                             
       END IF                                                                   
      PRINT*,' ***   TERMINATA FASE CALCOLO SOLUZIONE TERMICA  ***'             
	CALL MODSFT                                                                    
      PRINT*,' ***   TERMINATA FASE DI CALCOLO SFORZI TERMICI  ***'             
        IF(DUMP(7)) THEN                                                        
       	  CALL PCHSOL('*SLTERM ',1)                                             
        ENDIF                                                                   
      ENDIF                                                                     
C                                                                               
       IF(MODEX(6).EQ.0) CALL MD1SLE                                            
C                                                                               
       IF(MODEX(6).EQ.2) CALL MD2SLE                                            
C                                                                               
       IF(MODEX(6).EQ.3 .OR. MODEX(6).EQ.5) CALL MD1INS                         
C                                                                               
       IF(MODEX(6).EQ.4 .OR. MODEX(6).EQ.5) CALL MD2INS                         
C                                                                               
       IF(DUMP(11) .AND. (MODEX(6).NE.1.OR.MODEX(1).EQ.1) ) CALL WRSOLE         
C                                                                               
C+++  IF(MODEX(7).EQ.0 .AND. MODEX(6).EQ.0) CALL MODSFE                         
C                                                                               
      CALL CHKPNT                                                               
C+    CALL JPRFIL                                                               
C                                                                               
      CLOSE(INPT)                                                               
      CLOSE(ISYM)                                                               
 700  WRITE(IOUT,904)                                                           
 904  FORMAT(//////////40X,'***   FINE  DELL'' ELABORAZIONE   ***')             
C                                                                               
      STOP                                                                      
 800  WRITE(IOUT,999)                                                           
 999  FORMAT(//' *** AVVISO (MAIN) : ESECUZIONE TERMINATA PER ',                
     *         'ERRORI NELLE ISTRUZIONI DI CONTROLLO   ***'//)                  
      GOTO 700                                                                  
      END                                                                       
                                                                                
c**********************************************************************         
      SUBROUTINE ERRSET(I,J,K,L,M,N)                                            
      RETURN                                                                    
      END                                                                       
      SUBROUTINE OVERFL(LL)                                                     
      RETURN                                                                    
      END                                                                       
      subroutine trovap(stringa,ip)                                             
      character*(*) stringa                                                     
      lung=len(stringa)                                                         
      do 10 ip=1,lung                                                           
        if(stringa(ip:ip).eq.'.' .or. stringa(ip:ip).eq.' ') return             
10    continue                                                                  
      ip=lung-4                                                                 
      return                                                                    
      end                                                                       
C*************************   JSPV BLOCK-DATA         ******************         
      BLOCK DATA                                                                
C                                                                               
C                                                                               
      COMMON/JFTNIO/ CRD,PRT,SYM,IVO(22),INCORE,iloca                           
C                                                                               
C *** INITIALIZATION *****************************************                  
C                                                                               
C      DATA CRD,PRT,SYM/5,6,4/                                                  
C                                                                               
C                                                                               
      END                                                                       
C*************************          DEFLT          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DEFLT                                                          
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /FILEOU/ NFR                                                       
      COMMON /PARAM/ IVE(17)                                                    
      COMMON /PARSTR/ NMXSTR                                                    
      COMMON/JCRMNG/MAXDIM,izz(14),ifmsg,iprnt                                  
      COMMON /SISRIF/ XS,YS,SS,CS,IFLSR                                         
      COMMON /AUTCON/ ERR,CONST,Xz,Yz,Sz,Cz,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),                                         
     *                MODEX(8),INCOU,IAUTO,IGEOM,C                              
      COMMON /FILEOU/ NRF                                                       
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV,NREQ           
      COMMON /PARELM/ INELE(19)                                                 
      common /outnas/ nasmod,naspch                                             
C                                                                               
C                                                                               
      DO 10 I=1,8                                                               
      MODEX(I)=1                                                                
 10   CONTINUE                                                                  
C                                                                               
      NMXSTR=20                                                                 
C                                                                               
      MODEX(1)=0                                                                
C                                                                               
      DO 17 I=1,17                                                              
      IVE(I)=0                                                                  
 17   CONTINUE                                                                  
      IVE(3)=6                                                                  
      IVE(2)=3                                                                  
      IVE(9)=MAXDIM                                                             
C                                                                               
      XS=0.D0                                                                   
      YS=0.D0                                                                   
      ALFA=0.D0                                                                 
      SS=0.                                                                     
      CS=1.D0                                                                   
C                                                                               
      DO 20 I=1,22                                                              
      DUMP(I)=.FALSE.                                                           
 20   CONTINUE                                                                  
C                                                                               
      DO 30 I=1,6                                                               
      PGCON(I) =0.D0                                                            
      IPGCON(I)=0                                                               
 30   CONTINUE                                                                  
C                                                                               
      DO 40 I=1,19                                                              
      INELE(I) =0                                                               
 40   CONTINUE                                                                  
C                                                                               
      INPT=5                                                                    
C                                                                               
      IOUT=8                                                                    
      nasmod=9                                                                  
      naspch=10                                                                 
      iprnt=8                                                                   
      ifmsg=8                                                                   
C                                                                               
      ISYM=4                                                                    
C                                                                               
      ILOCA=3                                                                   
C                                                                               
      NFR=0                                                                     
C                                                                               
      INCOU=0                                                                   
C                                                                               
      CONST=1.E+15                                                              
C                                                                               
      ERR=1.E-8                                                                 
C                                                                               
      IAUTCN=0                                                                  
C                                                                               
      TOLL =1.E-4                                                               
C                                                                               
      ORTER=1.E-8                                                               
C                                                                               
      ZERTOL=1.E-8                                                              
C                                                                               
      IGEOM=0                                                                   
      DO 50 L=1,6                                                               
      CDC(L)=0.D0                                                               
 50   CONTINUE                                                                  
      CDC(3)=1.D0                                                               
      C=0.D0                                                                    
C                                                                               
C                                                                               
C                                                                               
      RETURN                                                                    
      END                                                                       

C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.INPNOD                                                        
C*************************          INPNOD          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPNOD(*,*,LABEL,COOR,MAXNOD,NNODI)                            
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION LABEL(*),COOR(2,*)                                              
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /SISRIF/ XS,YS,SS,CS,sclx,scly,IFLSR,iflsro,iflsrs                          
      COMMON /AUTCON/ ERR,CONST,Xz,Yz,Sz,Cz,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat                            
      common /outnas/ nasmod,naspch                                             
      data pigre/3.14159265d0/                                                  
C                                                                               
904   FORMAT(/48X,'***   NODI  DELLO  SCHEMA   ***'//                           
     1   12X,' N.RO',9X,'ID.NODO',15X,'COORDINATE SEZIONE',                     
     2   25X,'COORDINATE GENERALI'//)                                           
 5    NSUP=MAXNOD+1                                                             
      isalt=0                                                                   
      xmin=1.e6                                                                 
      ymin=1.e6                                                                 
      xmax=-1.e6                                                                
      ymax=-1.e6                                                                
                                                                                
      if(iflsr.eq.1) then
       write(iout,*)' Coordinate cilindriche',iflsr
      else
       write(iout,*)' Coordinate csrtesiane',iflsr
      endif                                                                                

      DO 10 I=1,NSUP                                                            
       CALL READFF(ISYM,0,NF,*100,*20)                                          
       CALL INTGFF(*101,LABEL(I),0,LENFL)                                       
	 if(nf.eq.3) then                                                              
         CALL DOUBFF(*101,COOR(1,I),0,LENFL)                                    
         CALL DOUBFF(*101,COOR(2,I),0,LENFL)                                    
	   if( isalt.eq.1 ) then                                                       
	     coor(1,i-1)=(coor(1,i-2)+coor(1,i))/2                                     
	     coor(2,i-1)=(coor(2,i-2)+coor(2,i))/2                                     
	   endif                                                                       
	   isalt=0                                                                     
	 else                                                                          
	   isalt=1                                                                     
	 endif                                                                         
10    CONTINUE                                                                  
      WRITE(IOUT,200)MAXNOD                                                     
      CALL JHALT                                                                
20    NNODI=I-1                                                                 
      CALL SHLSRT(NNODI,LABEL,COOR,2,2,2)                                       

                                                                                
      NNOPP=50                                                                  
      NCICLI=(NNODI+49)/NNOPP                                                   
      I=0                                                                       
      DO 25 IN=1,NCICLI                                                         
      IF(IN.EQ.NCICLI)NNOPP=NNODI-(NCICLI-1)*NNOPP                              
      CALL INTEST                                                               
      WRITE(IOUT,904)                                                           
      DO 30 IM=1,NNOPP                                                          
        I=I+1                                                                   
        xz=COOR(1,I)                                                            
        yz=COOR(2,I)                                                            
        if (iflsr.eq.1) then                                                    
          cz=cos(yz*pigre/180.)                                                 
          sz=sin(yz*pigre/180.)                                                 
          COOR(2,I)=sz*xz                                                       
          COOR(1,I)=cz*xz                                                       
        endif                                                                   
        XR=sclx*(XS+CS*COOR(1,I)-SS*COOR(2,I))                                  
        YR=scly*(YS+SS*COOR(1,I)+CS*COOR(2,I))                                  

c     nodi modello fem nastran
        WRITE(IOUT,905)I,LABEL(I),xz,yz,XR,YR                                   

        COOR(1,I)=XR                                                            
        COOR(2,I)=YR                                                            
        if(xmin.gt.coor(1,i)) xmin=coor(1,i)                                           
        if(ymin.gt.coor(2,i)) ymin=coor(2,i)                                           
        if(xmax.lt.coor(1,i)) xmax=coor(1,i)                                           
        if(ymax.lt.coor(2,i)) ymax=coor(2,i)                                           
30    CONTINUE                                                                  
25    CONTINUE                                                                  

      if(concio.eq.0.0) concio=dmin1(xmax-xmin,ymax-ymin)/5                     
      print*,xmin,xmax                                                          
      print*,ymin,ymax                                                          
      write(iout,*)' DIMENSIONE CONCIO MOD NASTRAN:',concio,nstrat             

      DO 125 I=1,NNODI
        xr=COOR(1,I)                                                            
        yr=COOR(2,I)                                                            
      
c     nodi modello fem nastran
        write(nasmod,1) 'GRID*   ',label(i),xr,yr                               
        write(nasmod,2) '*',0.                                                  
        nnxc=10000000                                                           
        do ii=1,nstrat                                                            
           write(nasmod,1) 'GRID*   ',label(i)+nnxc*ii,xr,yr                        
           write(nasmod,2) '*',-concio*ii                                           
           write(iout,*) label(i)+nnxc*ii,xr,yr,-concio*ii                                           
        enddo                                                                     
1     format(a8,i16,16x,2e16.6)                                                 
2     format(a1,7x,e16.6)                                                       


125   CONTINUE                                                                  


      CALL JWRITE(ISLNOD,LABEL,NNODI)                                           
      CALL JWRITE(ISLNOD,COOR,NNODI*4)                                          
905   FORMAT(2I16,11X,2E12.5,21X,2E12.5)                                        
      RETURN                                                                    
100   RETURN 1                                                                  
101   RETURN 2                                                                  
200   FORMAT(' *** AVVISO (INPNOD): IL NUMERO DI NODI ECCEDE IL'                
     * ,' MASSIMO CONSENTITO',I8,' ***')                                        
      END                                                                       
                                                                                
C@ELT,I ANBA*ANBA.INPNOD                                                        
C*************************          INPNOD          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPNODold(*,*,LABEL,COOR,MAXNOD,NNODI)                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION LABEL(*),COOR(2,*)                                              
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /SISRIF/ XS,YS,SS,CS,sclx,scly,IFLSR,iflsro,iflsrs                 
      COMMON /AUTCON/ ERR,CONST,Xz,Yz,Sz,Cz,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      common /outnas/ nasmod,naspch                                             
      data pigre/3.14159265d0/                                                  
C                                                                               
904   FORMAT(/48X,'***   NODI  DELLO  SCHEMA   ***'//                           
     1   12X,' N.RO',9X,'ID.NODO',15X,'COORDINATE SEZIONE',                     
     2   25X,'COORDINATE GENERALI'//)                                           
 5    NSUP=MAXNOD+1                                                             
      isalt=0                                                                   

      DO 10 I=1,NSUP                                                            
       CALL READFF(ISYM,0,NF,*100,*20)                                          
       CALL INTGFF(*101,LABEL(I),0,LENFL)                                       
	   if(nf.eq.3) then                                                            
       CALL DOUBFF(*101,COOR(1,I),0,LENFL)                                      
       CALL DOUBFF(*101,COOR(2,I),0,LENFL)                                      
	   if( isalt.eq.1 ) then                                                       
	   coor(1,i-1)=(coor(1,i-2)+coor(1,i))/2                                       
	   coor(2,i-1)=(coor(2,i-2)+coor(2,i))/2                                       
	   endif                                                                       
	   isalt=0                                                                     
	   else                                                                        
	   isalt=1                                                                     
	   endif                                                                       
10    CONTINUE                                                                  
      WRITE(IOUT,200)MAXNOD                                                     
      CALL JHALT                                                                
20    NNODI=I-1                                                                 
      CALL SHLSRT(NNODI,LABEL,COOR,2,2,2)                                       
                                                                                
      NNOPP=50                                                                  
      NCICLI=(NNODI+49)/NNOPP                                                   
      I=0           
      DO 25 IN=1,NCICLI                                                         
      IF(IN.EQ.NCICLI)NNOPP=NNODI-(NCICLI-1)*NNOPP                              
      CALL INTEST                                                               
      WRITE(IOUT,904)                                                           
      DO 30 IM=1,NNOPP                                                          
        I=I+1                                                                   
        xz=COOR(1,I)                                                            
        yz=COOR(2,I)                                                            
        if (iflsr.eq.1) then                                                    
          cz=cos(yz*pigre/180.)                                                 
          sz=sin(yz*pigre/180.)                                                 
          COOR(2,I)=sz*xz                                                       
          COOR(1,I)=cz*xz                                                       
        endif                                                                   
        XR=sclx*(XS+CS*COOR(1,I)-SS*COOR(2,I))                                  
        YR=scly*(YS+SS*COOR(1,I)+CS*COOR(2,I))                                  
        WRITE(IOUT,905)I,LABEL(I),xz,yz,XR,YR                                   
        write(nasmod,1) 'GRID*   ',label(i),xr,yr,'+G',label(i)                 
        write(nasmod,2) '+G',label(i),0.                                        
1     format(a8,i16,16x,2e16.6,a,i6)                                            
2     format(a2,i6,e16.6)                                                       
        COOR(1,I)=XR                                                            
        COOR(2,I)=YR                                                            
30    CONTINUE                                                                  
25    CONTINUE                                                                  
      CALL JWRITE(ISLNOD,LABEL,NNODI)                                           
      CALL JWRITE(ISLNOD,COOR,NNODI*4)                                          
905   FORMAT(2I16,11X,2E12.5,21X,2E12.5)                                        
      RETURN                                                                    
100   RETURN 1                                                                  
101   RETURN 2                                                                  
200   FORMAT(' *** AVVISO (INPNOD): IL NUMERO DI NODI ECCEDE IL'                
     * ,' MASSIMO CONSENTITO',I8,' ***')                                        
      END                                                                       
                                                                                
C@ELT,I ANBA*ANBA.INPPAN                                                        
C*************************          INPPAN          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPPAN                                                         
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE E' FITTIZIA E CONTIENE GLI ENTRY RELATIVI               
C     AGLI ELEMENTI DI  "PANNELLO"                                              
C                                                                               
C     INMATP - LETTURA  MATERIALI                                               
C     INPROP - LETTURA  PROPRIETA'                                              
C     INCONP - LETTURA  CONNESSIONI                                             
C     WRMATP - STAMPA MATERIALI                                                 
C     WRPROP - STAMPA PROPRIETA'                                                
C                                                                               
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      logical dump,incore                                                       
      character*8 continua,sz0*32                                                      
      DIMENSION IDMAT(*),CAR(6,*),IDPRO(*),ELAS(5,*),LABEL(*),COOR(2,*)         
     1   ,DEN(2,*),VETT(*),ICON(*),CAR1(6)                                      
     2   ,VEL(6),NODCNT(*)                                                      
      DIMENSION IDLAM(30,1),SPES(30,1),ROTAZ(30,1),nlami(1)                     

      DIMENSION PUNTI(9),B(4),SHAPE(4),          
     1          Z(2,24),xy(8),
     2          dex(4),dey(4)             


      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,dump(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /PANN/   TICK,RO,INTOR                                             
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat                            
      common /outnas/ nasmod,naspch                                             

      DATA PUNTI/                                                               
     * -1.D0,1.D0,                                                              
     * -1.D0,0.D0,1.D0,
     * -1.d0,-.3333333333D0,.3333333333D0,1.d0/                                                         

      data icont/0/                                                             
C                                                                               
C                                                                               
C                                                                               
      ENTRY INMATP(*,*,MAXMAT,IDMAT,CAR,NMAT)                                   
C                                                                               
C                                                                               
      DO 10 I=1,MAXMAT                                                          
      CALL READFF(ISYM,0,NF,*501,*20)                                           
      CALL INTGFF(*502,IDMAT(I),0,LENFL)                                        
      DO 10 K=1,6                                                               
      CALL DOUBFF(*502,CAR(K,I),0,LENFL)                                        
 10   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*501,*20)                                           
      WRITE(IOUT,900)MAXMAT                                                     
      CALL JHALT                                                                
      STOP                                                                      
 20   NMAT=I-1                                                                  
      RETURN                                                                    
 501  RETURN 1                                                                  
 502  RETURN 2                                                                  
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY INPROP(*,*,MAXPRO,IDPRO,ELAS,DEN,NPRO,IDMAT,CAR,NMAT,               
     *                  idlam,spes,rotaz,nlami,IER)                             
C                                                                               
C                                                                               
      CALL DZERO(ELAS,4*MAXPRO)                                                 
      CALL INTEST                                                               
      WRITE(IOUT,600)                                                           
      WRITE(IOUT,610)                                                           
      NR=0                                                                      
      DO 30 I=1,MAXPRO                                                          
c      CALL IZERO(IDLAM,30)                                                     
c      CALL DZERO(SPES,30)                                                      
c      CALL DZERO(ROTAZ,30)
      spet=0.                                                     
      CALL DZERO(VEL,6)                                                         
      KADD=1                                                                    
      NNL=0                                                                     
 150  CALL READFF(ISYM,0,NF,*503,*40)                                           
      IF(KADD.EQ.1) then
        CALL INTGFF(*504,IDPRO(I),0,LENFL) 
C        CALL CHARFF(*504,sZ0,0,LENFL) 
C        nf=nf-1
C        elas(5,i)=-99.
C        if(lenfl.gt.0.and.sz0(1:1).ne.' ') 
C     *     CALL DOUBFF(*504,ELAS(5,i),1,LENFL)
C        print*,'z0:',lenfl,sz0,elas(5,i)                     
      endif
      IF(KADD.EQ.2) then                                                        
      CALL INTGFF(*504,Itemp,0,LENFL)                                           
C      kadd=1                                                                    
      endif                                                                     
      NL=(NF-KADD)/3
      DO 100 K=1,NL                                                             
       CALL INTGFF(*504,IDLAM(NNL+K,i),0,LENFL)                                 
       CALL DOUBFF(*504,SPES(NNL+K,i),0,LENFL)                                  
       CALL DOUBFF(*504,ROTAZ(NNL+K,i),0,LENFL)
C       spet=spet+spes(NNL+K,i)                                 
 100  CONTINUE                             
 
C      if(elas(5,i).eq.-99) elas(5,i)=spet/2
                                                    
      NNL=NNL+NL                                                                
      IF(NNL.GT.30)THEN                                                         
      WRITE(IOUT,*) ' MA CHE RAZZA DI LAMINATO STAI FACENDO????',               
     *              '   (MAX 30 LAMINE PER PANNELLO)'                           
      CALL JHALT                                                                
      STOP                                                                      
      END IF                                                                    
      IF(NL*3+KADD.NE.NF) then                                                  
       call charff(*504,continua,0,lenfl)                                       
       if(continua(1:1).eq.'C'.or.continua(1:1).eq.'c') then 
       kadd=0                                                                   
       else                                                                     
       kadd=2                                                                   
       endif
       GOTO 150                                                                 
      endif                                                                     
      NR=NR+NNL              
      IF(NR.LE.50) GOTO 35                                                      
      NR=NNL                                                                    
      CALL INTEST    
      WRITE(IOUT,610)                                                           
 35   WRITE(IOUT,620)I,IDPRO(I),elas(5,i),
     *               IDLAM(1,i),SPES(1,i),ROTAZ(1,i)                 
      IF(NNL.GT.1) THEN                                                         
        WRITE(IOUT,621)(IDLAM(LL,i),SPES(LL,i),ROTAZ(LL,i),LL=2,NNL)            
      ENDIF                                                                     
 620  FORMAT(19X,I8,5X,I8,3x,E10.3,14X,I8,8X,E12.5,2X,E12.5)                             
 621  FORMAT(5(60X,I8,8X,E12.5,2X,f12.5/))                                      
 600  FORMAT(//30X,'***   PROPRIETA'' DEGLI ELEMENTI DI PANNELLO ',             
     1   '(COMPOSIZIONE)   ***'/)                                               
 610  FORMAT(//24X,'N.RO',8X,'ID.PROP.',5X,'Z0',7x,                                     
     1   10X,'ID. LAMINATO      SPESSORE    ORIENTAZIONE'/)                     
 44   DEN(2,I)=0.                                                               
      nlami(i)=nnl                                                              
      DO 50 ILAM=1,NNL                                                          
      IF(IDLAM(ILAM,i).EQ.0)GOTO 51                                             
      IF(IDLAM(ILAM,i).GT.0)GOTO 52                                             
      CALL READFL(IDLAM(ILAM,i),IERL,ITIP,CAR1,IFL,D)                           
      IF(IERL.EQ.0)GOTO 54                                                      
      WRITE(IOUT,904)IDLAM(ILAM,i),IDPRO(I)                                     
      IER=1                                                                     
 54   CALL MEDCAR(VEL,CAR1,DEN(1,I),SPES(ILAM,i),ROTAZ(ILAM,i))                 
      GOTO 53                                                                   
 52   IPM=IPOSL(IDLAM(ILAM,i),IDMAT,NMAT)                                       
      IF(IPM.NE.0)GOTO 803                                                      
      WRITE(IOUT,903)IDLAM(ILAM,i),IDPRO(I)                                     
      IER=1                                                                     
      GOTO 50                                                                   
 803  continue                                                                  
      CALL MEDCAR(VEL,CAR(1,IPM),DEN(1,I),SPES(ILAM,i),ROTAZ(ILAM,i))           
 53   DEN(2,I)=DEN(2,I)+SPES(ILAM,i)                                            
 50   CONTINUE                                                                  
 51   CONTINUE                                                                  
 70   ELAS(1,I)=VEL(1)                                                          
      ELAS(4,I)=VEL(6)                                                          
      ELAS(3,I)=VEL(4)                                                          
       IF(VEL(3).GT.1.E-8)THEN                                                  
        ELAS(1,I)=ELAS(1,I)-VEL(2)*VEL(2)/VEL(3)                                
        ELAS(4,I)=ELAS(4,I)-VEL(5)*VEL(5)/VEL(3)                                
        ELAS(3,I)=ELAS(3,I)-VEL(2)*VEL(5)/VEL(3)                                
       END IF                                                                   
      ELAS(2,I)=ELAS(3,I)                                                       
 30   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*503,*40)                                           
      WRITE(IOUT,901)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 40   NPRO=I-1                                                                  
c      write(iout,*)' stampe inprop'                                             
c      do i=1,npro                                                               
c      write(iout,*)' prop ',i,idpro(i),nlami(i)                                 
c      write(iout,'(8e12.5)')(elas(iol,i),iol=1,4),den(1,i),den(2,i)             
c      do iol=1,nlami(i)                                                         
c      write(iout,*)idlam(iol,i),spes(iol,i),rotaz(iol,i)*180/3.1415             
c      enddo                                                                     
c      enddo                                                                     
      RETURN                                                                    
 503  RETURN 1                                                                  
 504  RETURN 2                                                                  
 41   WRITE(IOUT,906) IDPRO(I)                                                  
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY INCONP(*,*,IDPRO,ELAS,DEN,NPRO,LABEL,COOR,VETT,ICON,                
     *          NODCNT,idlam,spes,rotaz,nlami,idmat,car,nmat,IER)               
C                                                                               
C                                                                               
CNEW   write(iout,*)' stampe inconp'                                            
CNEW   do i=1,nmat                                                              
CNEW   write(iout,*)' mate ',i,idmat(i)                                         
CNEW   write(iout,*)' exx=',car(1,i)                                            
CNEW   write(iout,*)' exx=',car(2,i)                                            
CNEW   write(iout,*)' nxy=',car(3,i)                                            
CNEW   write(iout,*)' nyx=',car(4,i)                                            
CNEW   write(iout,*)' g  =',car(5,i)                                            
CNEW   write(iout,*)' rho=',car(6,i)                                            
CNEW   enddo                                                                    
CNEW  do i=1,npro                                                               
CNEW  write(iout,*)' prop ',i,idpro(i)                                          
CNEW  write(iout,'(4e12.5)')(elas(iol,i),iol=1,4)                               
CNEW  write(iout,'(4e12.5)')den(1,i),den(2,i)                                   
CNEW  write(iout,*)' nlam ',nlami(i)                                            
CNEW  do iol=1,nlami(i)                                                         
CNEW  write(iout,*) ' idlam=',idlam(iol,i), ' spes=',spes(iol,i),               
CNEW *      ' rotaz=',rotaz(iol,i)*180/3.1415                                   
CNEW  enddo                                                                     
CNEW  enddo                                                                     
                                                                                
                                                                                
      ITIP=3                                                                    
      IFLMR=1                                                                   
      NEL=0                                                                     
      NPAN=0                                                                    
      NPANR=0                                                                   
      NWP=0                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 90   CALL IZERO(ICON,4)                                                        
      CALL READFF(ISYM,0,NF,*505,*80)                                           
      CALL INTGFF(*506,IDEL,0,LENFL)                                            
      CALL INTGFF(*506,IPROP,0,LENFL)                                           
      CALL INTGFF(*506,INTOR,0,LENFL)                                           
      NCON=NF-3                                                                 
      DO 95 I=1,NCON                                                            
       CALL INTGFF(*506,ICON(I),0,LENFL)                                        
 95   CONTINUE                                                                  
      NPAN=NPAN+1                                                               
      IF(IFLMR.NE.0)NPANR=NPANR+1                                               
      NEL=NEL+1                                                                 
      IF(NEL.LE.50) GOTO 91                                                     
      NEL=1                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
 91   CONTINUE                                                                  
      IF(ICON(1).EQ.0.OR.ICON(2).EQ.0) GOTO 802                                 
      IF(MAXNOD.LT.NCON)MAXNOD=NCON                                             
      IPP=IPOSL(IPROP,IDPRO,NPRO)                                               
      IF(IPP.EQ.0)GOTO 800                                                      
      RO=DEN(1,IPP)                                                             
      TICK=DEN(2,IPP)                                                           
38    WRITE(IOUT,607)NPAN,IDEL,IPROP,INTOR,(ICON(L),L=1,NCON)                   
      IER=0                                                                     
                                                  
c -----------------------------------------------------------                   
c     inizializzazione di icont                                                 
      if(nstrat.gt.0) then
      do iok=1,nstrat                                                           
      icont=icont+1                                                             
      iux=10000000                                                              
      nt1=iux*(iok-1)                                                           
      nt2=iux*iok                                                               
      icont=icont+1                                                             
      if(ncon.eq.2) then                                                        
      WRITE(nasmod,721)IDEL+nt1,IPROP,ICON(1)+nt1,icon(2)+nt1,                  
     +                 ICON(2)+nt2,icon(1)+nt2                                  
      elseif(ncon.eq.3) then                                                    
      WRITE(nasmod,722)IDEL+nt1,IPROP,ICON(1)+nt1,icon(2)+nt1,                  
     + ICON(2)+nt2,                                                             
     + icon(1)+nt2,icon(3)+nt1,0,icont,icont,icon(3)+nt2,0                            
       icont=icont+1                                                            
      else                                                                      
c      do ippa=1,ncon-1                                                         
c      WRITE(nasmod,721)IDEL,IPROP,ICON(ippa),icon(ippa+1),                     
c     +                 ICON(ippa+1)+nnodi,icon(ippa)+nnodi                     
c      enddo                                                                    
      endif                                                                     
      enddo                                                                     
721   format('CQUAD4  ',6i8)                                                    
722   format('CQUAD8  ',8i8,'+CQ',i5/'+CQ',i5,2i8)
      else
      do ippa=1,ncon-1                                                         
      WRITE(nasmod,821)IDEL,IPROP,ICON(ippa),icon(ippa+1)                      
      enddo                                                                    
821   format('CROD    ',6i8)
      endif                              
c -----------------------------------------------------------                   
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,xy,NODCNT,IER)                   
      IF(IER.EQ.0)GOTO 86                                                       
      IERG=1                                                                    
      WRITE(IOUT,907)IDEL                                                       
      GOTO 90                                                                   
 86   continue  

      z0=elas(5,ipp)
c      if(abs(z0).ne.0.0d0) then            
      I0=0                                               
      if(ncon.eq.3) i0=i0+2
      if(ncon.eq.4) i0=i0+5
c      print'(2e12.5)',(xy(iop),xy(iop+1),iop=1,ncon*2,2)
c      print'(2(a,f8.4))',' z0',z0,'    t',tick
c      print*,i0

      DO  In=1,ncon
      Io=i0+in                                               
c      print*,in,io,punti(io)       
                                                        
      IND=(IN-1)*6+1                                                         
      CALL Bmat(xy,NCON,PUNTI(IO),XX,YY,B,SHAPE,Z          
     1  ,DS,DX,DY)                       
     
      vett((in-1)*2+1)=xy((in-1)*2+1)+(z0-tick/2.d0)*dy
      vett((in-1)*2+2)=xy((in-1)*2+2)-(z0-tick/2.d0)*dx

c      print*,+(z0-tick/2.d0)*dy,-(z0-tick/2.d0)*dx                       
      write(iout,'(50x,i10,2(2e12.5,5x))')icon(in),
     * xy((in-1)*2+1),xy((in-1)*2+2),
     * vett((in-1)*2+1),vett((in-1)*2+2)
c      vy=vett(3)-vett(1)
c      vx=vett(4)-vett(2)
c      ab=sqrt(vx*vx+vy*vy)
c      vy=vy/ab
c      vx=vx/ab
c      print*,vx,vy
      enddo
      
      
c      endif
      
 
      CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,NCON)                                             
      CALL JWRITE(ISLELM,VETT,4*NCON)                                           
      CALL JWRITE(ISLELM,TICK,5)                                                
      CALL JWRITE(ISLELM,ELAS(1,IPP),5*2)                                         
      print*,'INCONP'
      print*,'itip,idel,ncon',itip,idel,ncon
      print'(i8,2f8.3)',(icon(iw),vett(iw*2-1),vett(iw*2),iw=1,ncon)
      print*,'TICK,ro,INTOR',TICK,ro,INTOR
      print'(a,4e12.5)','ELAS',(elas(kw,ipp),kw=1,4)
      print*,'Z0',elas(5,ipp)                                               

c      CALL JWRITE(ISLELM,ELAS(1,IPP),4*2)                                         
      nskip=1+(1+7*2)*nlami(ipp)                                                
      CALL JWRITE(ISLELM,nskip,1)                                               
      CALL JWRITE(ISLELM,nlami(ipp),1)                                          
      do iii=1,nlami(ipp)                                                       
      call jwrite(islelm,idlam(iii,ipp),1)                                      
      call jwrite(islelm,spes (iii,ipp),2  )                                    
      call jwrite(islelm,rotaz(iii,ipp),2  )                                    
      ipm=iposl(idlam(iii,ipp),idmat,nmat)                                      
      call jwrite(islelm,car(1,ipm),5*2  )                                      
      enddo                                                                     
CNEW  write(iout,*) ' stampe di controllo dati elemento'                        
CNEW  write(iout,*) itip,idel,ncon,iflmr                                        
CNEW  write(iout,*) (icon(iop),iop=1,ncon)                                      
CNEW  write(iout,'(2e12.5)') (vett(iop),iop=1,2*ncon)                           
CNEW  write(iout,'(f8.2,i8,e12.5)') tick,intor,ro                               
CNEW  write(iout,'(4e12.5)') (elas(iop,ipp),iop=1,4)                            
CNEW  WRITE(iout,*)' parole da saltare:',nskip                                  
CNEW  WRITE(iout,*)'Prop N.',ipp,' N. lamine=',nlami(ipp)                       
CNEW  do iii=1,nlami(ipp)                                                       
CNEW  write(iout,'(i4,i8,2f8.2)')iii,idlam(iii,ipp),spes(iii,ipp),              
CNEW *      rotaz(iii,ipp)*180/3.1415                                           
CNEW  ipm=iposl(idlam(iii,ipp),idmat,nmat)                                      
CNEW  write(iout,'(i8,5e12.5)')ipm,(car(iop,ipm),iop=1,5)                       
CNEW  enddo                                                                     
                                                                                
      NWP=NWP+19+5*NCON+nskip+1                                                 
      GOTO 90                                                                   
 80   CONTINUE                                                                  
      RETURN                                                                    
 505  RETURN 1                                                                  
 506  RETURN 2                                                                  
 800  WRITE(IOUT,902)IPROP,IDEL                                                 
      GOTO 99                                                                   
 802  WRITE(IOUT,905)IDEL,IPROP,INTOR,(ICON(I),I=1,4)                           
 99   IER=1                                                                     
      GOTO 90                                                                   
C                                                                               
 607  FORMAT(20X,4I10,5X,4I10)                                                  
 700  FORMAT(//40X,'***   ELEMENTI DI PANNELLO   ***')                          
 710  FORMAT(//23X,'    N.RO    ID.ELEM.  ID.PROP.  N.PT.INT.',                 
     1   11X,'CONNESSIONI'/)                                                    
 900  FORMAT(' ***   AVVISO (INMATP) : OVERFLOW, N. MASSIMO DI'                 
     1,' MATERIALI AMMESSO E''',I8,'   ***')                                    
 901  FORMAT(' ***   AVVISO (INPROP) : OVERFLOW, N. MASSIMO DI'                 
     1,' PROPRIETA'' AMMESSO E''',I8,'   ***')                                  
 902  FORMAT(' ***   AVVISO (INCONP) : PROPRIETA'' NON DICHIARATA'              
     1 ,I8,'  PER L''ELEMENTO N.RO',I8,'   ***')                                
 903  FORMAT(' ***   AVVISO (INPROP) : NON DATO IL MATERIALE N.RO',I8,          
     1     ' ( PROPRIETA'' N.RO',I8,' )   ***')                                 
 904  FORMAT(' ***   AVVISO (INPROP) : MATERIALE N.RO',I8,                      
     1     ' ( PROPRIETA'' N.RO',I8,' )  ***')                                  
 905  FORMAT(' ***   AVVISO (INCONP) : DATI NON CONGRUENTI :'                   
     1 ,7I8,'   ***'/)                                                          
 906  FORMAT(' ***   AVVISO (INPROP) : DATI PROPRIETA'' N.RO',I8                
     1 ,' NON CONGRUENTI CON REQUISITI    ***'/)                                
 907  FORMAT(' ***   AVVISO (INCONP) : NON RECUPERATI NODI DELL''',             
     1  'ELEMENTO N.RO',I8,'   ***')                                            
C                                                                               
C                                                                               
C                                                                               
      ENTRY WRMATP(IDMAT,CAR,NMAT)                                              
C                                                                               
C                                                                               
      IMAT=0                                                                    
      NMPAG=50                                                                  
      NPAG=(NMAT+49)/NMPAG                                                      
      DO 2000 I=1,NPAG                                                          
      IF(I.EQ.NPAG)NMPAG=NMAT-(NPAG-1)*NMPAG                                    
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      DO 2100 L=1,NMPAG                                                         
      IMAT=IMAT+1                                                               
      WRITE(IOUT,3100)IMAT,IDMAT(IMAT),(CAR(K,IMAT),K=1,6)                      
 2100 CONTINUE                                                                  
 2000 CONTINUE                                                                  
 3000 FORMAT(/40X,'***   MATERIALI ELEMENTI DI PANNELLO   ***'//                
     1 12X,'   N.RO  ID.MAT.',7X,'E11',13X,'E22',12X,'NU12',12X,'NU21',         
     2 14X,'G',12X,'DENSITA'''/)                                                
 3100 FORMAT(10X,2I8,6E16.6)                                                    
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
      ENTRY WRPROP(IDPRO,ELAS,DEN,NPRO)                                         
C                                                                               
C                                                                               
      IPRO=0                                                                    
      NPPAG=50                                                                  
      NPAG=(NPRO+49)/NPPAG                                                      
      DO 2200 I=1,NPAG                                                          
      IF(I.EQ.NPAG)NPPAG=NPRO-(NPAG-1)*NPPAG                                    
      CALL INTEST                                                               
      WRITE(IOUT,3200)                                                          
      DO 2300 L=1,NPPAG                                                         
      IPRO=IPRO+1                                                               
      WRITE(IOUT,3300)IPRO,IDPRO(IPRO),(ELAS(K,IPRO),K=1,4),                    
     1                DEN(2,IPRO),DEN(1,IPRO)                                   
 2300 CONTINUE                                                                  
 2200 CONTINUE                                                                  
 3200 FORMAT(/40X,'***   PROPRIETA'' ELEMENTI DI PANNELLO   ***'//              
     1 10X,'   N.RO  ID.PROP.',24X,'MATRICE ELASTICA',30X,'SPESSORE',8X,        
     1    'DENSITA'''/)                                                         
 3300 FORMAT(8X,2I8,2X,6E16.6)                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.INPUTP                                                        
C*************************          INPUTP          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTP(ISCHE,IERG,JLABEL,JCOOR,JICON,JVETT,JNDCNT)             
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
C                                                                               
      NMAT=1                                                                    
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).NE.'MATE')GOTO 400                                           
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXMAT=(MEMDIS-11-9*7-JIFITT)/13                                          
      call jalmax(2,memdisp)                                                    
      maxmat=(memdisp-11)/13                                                    
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,MAXMAT     ,JIDMAT)                 
      CALL JALLOC(*2000,'CAR2    ','D.P.',1,MAXMAT*6   ,JCAR2 )                 
      CALL INMATP(*500,*501,MAXMAT,IV(JIDMAT),DV(JCAR2),NMAT)                   
      JFITT=JCAR2                                                               
      CALL JFREE(*2000,'CAR2    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,NMAT       ,JIDMAT)                 
      CALL JALLOC(*2000,'CAR2    ','D.P.',1,NMAT*6     ,JCAR2 )                 
      CALL DMOVE(DV(JFITT),DV(JCAR2),NMAT*6)                                    
      CALL WRMATP(IV(JIDMAT),DV(JCAR2),NMAT)                                    
      READ(ISYM,100)ISCHE                                                       
 400  IF(ISCHE(:4).EQ.'PROP')GOTO 410                                           
      WRITE(IOUT,721)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 410  CONTINUE                                                                  
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXPRO=(MEMDIS-13-12*7-JIFITT)/14                                         
      call jalmax(7,memdisp)                                                    
      maxlam=30                                                                 
      MAXPRO=(memdisp-13)/(2+7*2  +1+maxlam+maxlam*2*2  )                       
CNEW  print*, ' maxpro ',maxpro,maxlam                                          
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,MAXPRO*2   ,JIDPRO)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,MAXPRO*5   ,JELAS )                 
      CALL JALLOC(*2000,'DEN     ','D.P.',1,MAXPRO*2   ,JDEN  )                 
      CALL JALLOC(*2000,'NLAM    ','INT.',1,MAXPRO     ,JNLAMI)                 
      CALL JALLOC(*2000,'IDLAM   ','INT.',1,MAXPRO*maxlam,JIDLAM)               
      CALL JALLOC(*2000,'SPES    ','D.P.',1,MAXPRO*maxlam,JSPES )               
      CALL JALLOC(*2000,'ROTAZ   ','D.P.',1,MAXPRO*maxlam,JROTAZ)               
c     call jprtab                                                               
      IER=0                                                                     
      CALL INPROP(*500,*501,MAXPRO,IV(JIDPRO),DV(JELAS),DV(JDEN),NPRO,          
     1            IV(JIDMAT),DV(JCAR2),NMAT,                                    
     2      iv(jidlam),dv(jspes),dv(jrotaz),iv(jnlami),IER)                     
C                                                                               
      IF(IER.NE.0)IERG=1                                                        
      JFITT=JELAS                                                               
      JFIT1=JDEN                                                                
      JFIT2=JNLAMI                                                              
      JFIT3=JIDLAM                                                              
      JFIT4=JSPES                                                               
      JFIT5=JROTAZ                                                              
c     call jprtab                                                               
      CALL JFREE(*2000,'ROTAZ   ')                                              
      CALL JFREE(*2000,'SPES    ')                                              
      CALL JFREE(*2000,'IDLAM   ')                                              
      CALL JFREE(*2000,'NLAM    ')                                              
      CALL JFREE(*2000,'DEN     ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
CNEW  write(iout,*)' npro ',npro,'   maxlam ',maxlam                            
c     call jprtab                                                               
                                                                                
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,NPRO*2     ,JIDPRO)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,NPRO*5     ,JELAS )                 
      CALL JALLOC(*2000,'DEN     ','D.P.',1,NPRO*2     ,JDEN  )                 
      CALL JALLOC(*2000,'NLAM    ','INT.',1,NPRO       ,JNLAM )                 
      CALL JALLOC(*2000,'IDLAM   ','INT.',1,NPRO  *maxlam,JIDLAM)               
      CALL JALLOC(*2000,'SPES    ','D.P.',1,NPRO  *maxlam,JSPES )               
      CALL JALLOC(*2000,'ROTAZ   ','D.P.',1,NPRO  *maxlam,JROTAZ)               
c     call jprtab                                                               
      CALL IMOVE(IV(JIDPRO+MAXPRO),IV(JIDPRO+NPRO),NPRO)                        
      CALL DMOVE(DV(JFITT),DV(JELAS),NPRO*5)                                    
      CALL DMOVE(DV(JFIT1),DV(JDEN),NPRO*2)                                     
      CALL IMOVE(IV(JFIT2),IV(Jnlami),NPRO)                                     
      CALL IMOVE(IV(JFIT3),IV(Jidlam),NPRO*maxlam)                              
      CALL DMOVE(DV(JFIT4),DV(Jspes),NPRO*maxlam)                               
      CALL DMOVE(DV(JFIT5),DV(Jrotaz),NPRO*maxlam)                              
      CALL WRPROP(IV(JIDPRO),DV(JELAS),DV(JDEN),NPRO)                           
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).EQ.'CONN')GOTO 420                                           
      WRITE(IOUT,722)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 420  CONTINUE                                                                  
      IER=0                                                                     
c     call jprtab                                                               
      CALL INCONP(*500,*501,IV(JIDPRO),DV(JELAS),DV(JDEN),NPRO,                 
     1     IV(JLABEL),DV(JCOOR),DV(JVETT),IV(JICON),IV(JNDCNT),                 
     2     iv(jidlam),dv(jspes),dv(jrotaz),iv(jnlami),                          
     1            IV(JIDMAT),DV(JCAR2),NMAT,IER)                                
C                                                                               
      IF(IER.NE.0)IERG=1                                                        
c     call jprtab                                                               
      CALL JFREE(*2000,'ROTAZ   ')                                              
      CALL JFREE(*2000,'SPES    ')                                              
      CALL JFREE(*2000,'IDLAM   ')                                              
      CALL JFREE(*2000,'NLAM    ')                                              
      CALL JFREE(*2000,'DEN     ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JFREE(*2000,'CAR2    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
C                                                                               
      RETURN                                                                    
C                                                                               
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTP): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTP): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 100  FORMAT(A80)                                                               
 721  FORMAT(' ***   AVVISO (INPUTP): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO PROPRIETA'' PANNELLI   ***')                 
 722  FORMAT(' ***   AVVISO (INPUTP): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO CONNESSIONI PANNELLI   ***')                 
      END                                                                       
C@ELT,I ANBA*ANBA.INPRIN                                                        
C*************************          INPRIN          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPRIN(*,*,NUMER,NUNEW,LABEL,NNODI,IFL,IER)                    
C                                                                               
C     LETTURA SCHEDE RINUMERAZIONE NODI                                         
C                                                                               
      DIMENSION NUNEW(*),NUMER(2,*)                                             
      logical dump,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      IF(IFL.EQ.0)GOTO 25                                                       
      I=0                                                                       
      NRIN=0                                                                    
 10   CALL READFF(ISYM,0,NNODG,*500,*40)                                        
      NNODG=NNODG/2                                                             
      NRIN=NRIN+NNODG                                                           
      DO 50 K=1,NNODG                                                           
      CALL INTGFF(*501,NUMER(1,K),0,LENFL)                                      
      CALL INTGFF(*501,NUMER(2,K),0,LENFL)                                      
      IP=IPOSZ(NUMER(1,K),LABEL,NNODI)                                          
      IF(IP.GT.0)GOTO 21                                                        
      WRITE(IOUT,160)NUMER(1,K),NUMER(2,K)                                      
      IER=1                                                                     
 21   NUNEW(IP)=NUMER(2,K)                                                      
 50   CONTINUE                                                                  
      IF(NRIN.LT.NNODI) GOTO 10                                                 
      CALL READFF(ISYM,0,NF,*500,*40)                                           
      WRITE(IOUT,150)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 25   DO 30 I=1,NNODI                                                           
      NUNEW(I)=I                                                                
 30   CONTINUE                                                                  
 40   IF(IER.NE.0)WRITE(IOUT,170)                                               
      RETURN                                                                    
 500  RETURN 1                                                                  
 501  RETURN 2                                                                  
 150  FORMAT(//' ***   AVVISO (INPRIN): NON CORRETTO IL N.RO ',                 
     1    'DI NODI RINUMERATI   ***')                                           
 160  FORMAT(//' ***   AVVISO (INPRIN): RINUMERAZIONE ERRATA ',                 
     1    'DEL NODO N.RO',I8,'  :',I8,'   ***')                                 
 170  FORMAT(//' ***   AVVISO (INPRIN): LA RINUMERAZIONE ',                     
     1    'VIENE IGNORATA   ***')                                               
      END                                                                       
C@ELT,I ANBA*ANBA.INPVIN                                                        
C*************************          INPVIN          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPVIN(LABEL,COOR,NNODI,NODCNT,IERG)                           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      logical dump,incore                                                       
      DIMENSION LABEL(*),COOR(2,*),ICON(40),ICOMP(3),VETT(2),NODCNT(*)          
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      NCON=1                                                                    
      ITIP=7                                                                    
      NN=0                                                                      
      NEL=0                                                                     
      NRIG=0                                                                    
      NSPC=0                                                                    
      IDEL=0                                                                    
      CALL INTEST                                                               
      NWV=0                                                                     
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 20   CONTINUE                                                                  
      CALL IZERO(ICON,40)                                                       
      CALL READFF(ISYM,0,NF,*500,*200)                                          
      CALL INTGFF(*501,IDGL,0,LEN)                                              
      NNF=NF                                                                    
      DO 100 I=1,NNF-1                                                          
      CALL INTGFF(*501,ICON(I),0,LEN)                                           
      IF(ICON(I).EQ.0) NF=NF-1                                                  
 100  CONTINUE                                                                  
      IDEL=IDEL+1                                                               
      NODV=NF-1                                                                 
      ICOMP(1)=IDGL-(IDGL/10)*10                                                
      ICOMP(2)=(IDGL-(IDGL/100)*100-ICOMP(1))/10                                
      ICOMP(3)=(IDGL-ICOMP(1)-ICOMP(2)*10)/100                                  
      NCOMP=3                                                                   
      IF(ICOMP(3).EQ.0)NCOMP=2                                                  
      IF(ICOMP(2).EQ.0)NCOMP=1                                                  
      IF(ICOMP(1).EQ.0) GOTO 800                                                
      NN=NN+1                                                                   
      IF(NRIG.LE.50) GOTO 30                                                    
      NRIG=0                                                                    
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
 30   CONTINUE                                                                  
      NNO=NODV                                                                  
      IF(NODV.GT.10) NNO=10                                                     
      WRITE(IOUT,608) NN,IDGL,(ICON(I),I=1,NNO)                                 
      NNRES=NODV-10                                                             
      NRIG=NRIG+1                                                               
      DO 35 I=11,NODV,10                                                        
      WRITE(IOUT,609) (ICON(L),L=I,I+9)                                         
      NRIG=NRIG+1                                                               
 35   CONTINUE                                                                  
      DO 90 II=1,NODV                                                           
      IER=0                                                                     
      CALL RCPCON(ICON(II),NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)               
      IF(IER.EQ.0)GOTO 86                                                       
      IERG=1                                                                    
      WRITE(IOUT,901)ICON(II),IDEL                                              
      GOTO 20                                                                   
 86   DO 10 I=1,NCOMP                                                           
          NSPC=NSPC+1                                                           
      CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON(II),1)                                            
      CALL JWRITE(ISLELM,ICOMP(I),1)                                            
      CALL JWRITE(ISLELM,CONST,2)                                               
      CALL JWRITE(ISLELM,I,2)                                                   
      NWV=NWV+10                                                                
 10   CONTINUE                                                                  
 90   CONTINUE                                                                  
      GOTO 20                                                                   
 800  WRITE(IOUT,900)                                                           
      IERG=1                                                                    
      GOTO 20                                                                   
 200  RETURN                                                                    
 500  WRITE(IOUT,902)                                                           
      GOTO 2000                                                                 
 501  WRITE(IOUT,903)                                                           
 2000 CALL JHALT                                                                
 900  FORMAT(/' ***   AVVISO (INPVIN): SCHEDA VINCOLO ERRATA   ***')            
 901  FORMAT(/' ***   AVVISO (INPVIN): NON RECUPERATO IL NODO N.RO',            
     1  I8,'DELL''ELEMENTO N.RO',I8,'     ***')                                 
 902  FORMAT(/' ***   AVVISO (INPVIN): TERMINATE LE SCHEDE DATI   ***')         
 903  FORMAT(/' ***   AVVISO (INPVIN): ERRORE IN LETTURA DATI   ***')           
 608  FORMAT(18X,2I8,8X,10I8)                                                   
 609  FORMAT(36X,10I8)                                                          
 700  FORMAT(//50X,'***   ELEMENTI DI VINCOLO (SPC)   ***')                     
 710  FORMAT(//19X,'    N.RO   COMPON. ',10X,                                   
     1   'NODI VINCOLATI'/)                                                     
      END                                                                       
C@ELT,I ANBA*ANBA.INPUTC                                                        
C*************************          INPUTC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTC(ISCHE,IERG,JLABEL,JCOOR,JICON,JVETT,JNDCNT)             
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC, ISLSC1,ISLTC2,ISLSC2,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
C                                                                               
      NMAT=1                                                                    
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).NE.'MATE')GOTO 200                                           
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXMAT=(MEMDIS-9-9*7-JIFITT)/5                                            
ccc   call prcor(' entro inputc', nnodi,dv(jcoor))                              
ccc   call jprmem                                                               
ccc   call jprtab                                                               
      call javlbl(memdi)                                                        
      call jalmax(2,memdisp)                                                    
      maxmat=(memdisp-9)/5                                                      
ccc   write(iout,*)memdisp,memdi,maxmat                                         
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,MAXMAT     ,JIDMAT)                 
      CALL JALLOC(*2000,'CAR1    ','D.P.',1,MAXMAT*2   ,JCAR1 )                 
C                                                                               
      CALL INMATC(*500,*501,MAXMAT,IV(JIDMAT),DV(JCAR1),NMAT)                   
      JFITT=JCAR1                                                               
ccc   call prcor(' dopo inmatc ', nnodi,dv(jcoor))                              
      CALL JFREE(*2000,'CAR1    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,NMAT       ,JIDMAT)                 
      CALL JALLOC(*2000,'CAR1    ','D.P.',1,NMAT*2     ,JCAR1 )                 
      CALL DMOVE(DV(JFITT),DV(JCAR1),NMAT*2)                                    
      CALL WRMATC(IV(JIDMAT),DV(JCAR1),NMAT)                                    
ccc   call prcor(' dopo wrmatc ', nnodi,dv(jcoor))                              
      READ(ISYM,100)ISCHE                                                       
 200  IF(ISCHE(:4).EQ.'PROP')GOTO 210                                           
      WRITE(IOUT,701)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 210  CONTINUE                                                                  
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXPRO=(MEMDIS-9-12*7-JIFITT)/4                                           
      call jalmax(2,memdisp)                                                    
      maxpro=(memdisp-9)/4                                                      
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,MAXPRO*2   ,JIDPRO)                 
      CALL JALLOC(*2000,'SEZI    ','D.P.',1,MAXPRO     ,JSEZI )                 
ccc   call prcor(' prima inproc', nnodi,dv(jcoor))                              
      CALL INPROC(*500,*501,MAXPRO,IV(JIDPRO),DV(JSEZI),NPRO)                   
      JFITT=JSEZI                                                               
      CALL JFREE(*2000,'SEZI    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,NPRO*2     ,JIDPRO)                 
      CALL JALLOC(*2000,'SEZI    ','D.P.',1,NPRO       ,JSEZI )                 
      CALL IMOVE(IV(JIDPRO+MAXPRO),IV(JIDPRO+NPRO),NPRO)                        
      CALL DMOVE(DV(JFITT),DV(JSEZI),NPRO)                                      
CCC       CALL JPRTAB                                                           
      CALL WRPROC(IV(JIDPRO),DV(JSEZI),NPRO)                                    
ccc   call prcor(' dopo  wrproc', nnodi,dv(jcoor))                              
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).EQ.'CONN') GOTO 220                                          
      WRITE(IOUT,702)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 220  CONTINUE                                                                  
      IER=0                                                                     
ccc   call prcor(' prima inconc', nnodi,dv(jcoor))                              
      CALL INCONC(*500,*501,IV(JIDMAT),DV(JCAR1),NMAT,IV(JIDPRO),               
     1  DV(JSEZI),NPRO,IV(JLABEL),DV(JCOOR),DV(JVETT),IV(JICON),                
     2  IV(JNDCNT),IER)                                                         
C                                                                               
      IF(IER.NE.0) IERG=1                                                       
      CALL JFREE(*2000,'SEZI    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JFREE(*2000,'CAR1    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTC): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTC): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 100  FORMAT(A80)                                                               
 701  FORMAT(' ***   AVVISO (INPUTC): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO PROPRIETA'' CORRENTI   ***')                 
 702  FORMAT(' ***   AVVISO (INPUTC): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO CONNESSIONI CORRENTI   ***')                 
      END                                                                       
      subroutine prcor(st,n,x)                                                  
      character*(*) st                                                          
      dimension x(2,1)                                                          
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      write(iout,'(a)')st                                                       
      do i=1,n                                                                  
      write(iout,*)x(1,i),x(2,i)                                                
      enddo                                                                     
      return                                                                    
      end                                                                       
C                                                                               
C-----------------in INPUTE e' stata aggiunta l'acquisizione   ----             
C-----------------dei coefficienti termici                     ----             
C                                                                               
C*************************          INPUTE          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTE(ISCHE,IERG,JLABEL,JCOOR,JICON,JVETT,JNDCNT)             
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*(*)                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
      INTEGER JFIT2                                                             
C                                                                               
C     CDTER=  VETTORE DEI COEFFICIENTI DI DILATAZIONE TERMICA                   
C                                                                               
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).NE.'MATE') GOTO 800                                          
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXMAT=(MEMDIS-84-7*15-JIFITT)/(75+12)                                    
      call jalmax(6,memdisp)                                                    
      maxmat=(memdisp-84)/(75+12)                                               
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,MAXMAT     ,JIDMAT)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,MAXMAT*36  ,JELAS )                 
      CALL JALLOC(*2000,'DENS    ','D.P.',1,MAXMAT     ,JDENS )                 
C                                                                               
      CALL JALLOC(*2000,'CDTER   ','D.P.',1,6*MAXMAT   ,JCDTER)                 
C                                                                               
      CALL JALLOC(*2000,'TMPMT   ','D.P.',1,36         ,JTMPMT)                 
      CALL JALLOC(*2000,'PERM    ','D.P.',1,6          ,JPERM )                 
c-----                                                                          
C     CALL JPRTAB                                                               
c-----                                                                          
                                                                                
      CALL INMATE(*500,*501,MAXMAT,IV(JIDMAT),DV(JELAS),DV(JDENS),              
     1           DV(JCDTER),NMAT,DV(JPERM),DV(JTMPMT),IER)                      
C                                                                               
      IF(IER.NE.0) IERG=1                                                       
      JFITT=JELAS                                                               
      JFIT1=JDENS                                                               
C                                                                               
      JFIT2=JCDTER                                                              
C                                                                               
      CALL JFREE(*2000,'PERM    ')                                              
      CALL JFREE(*2000,'TMPMT   ')                                              
C                                                                               
      CALL JFREE(*2000,'CDTER   ')                                              
C                                                                               
      CALL JFREE(*2000,'DENS    ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,NMAT       ,JIDMAT)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,NMAT*36    ,JELAS )                 
      CALL JALLOC(*2000,'DENS    ','D.P.',1,NMAT       ,JDENS )                 
C                                                                               
      CALL JALLOC(*2000,'CDTER   ','D.P.',1,6*NMAT     ,JCDTER)                 
C                                                                               
      CALL DMOVE(DV(JFITT),DV(JELAS),NMAT*36)                                   
      CALL DMOVE(DV(JFIT1),DV(JDENS),NMAT)                                      
C                                                                               
      CALL DMOVE(DV(JFIT2),DV(JCDTER),6*NMAT)                                   
C                                                                               
      CALL WRMATE(IV(JIDMAT),DV(JELAS),DV(JDENS),DV(JCDTER),NMAT)               
C                                                                               
      NR6=6                                                                     
      READ(ISYM,100)ISCHE                                                       
 800  IF(ISCHE(:4).EQ.'PROP') GOTO 810                                          
      WRITE(IOUT,751)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 810  CONTINUE                                                                  
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXPRO=(MEMDIS-10-15*7-JIFITT)/7                                          
      call jalmax(3,memdisp)                                                    
      maxpro=(memdisp-10)/(7)                                                   
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,MAXPRO*3   ,JIDPRO)                 
      CALL JALLOC(*2000,'ALFA    ','D.P.',1,MAXPRO     ,JALFA )                 
      CALL JALLOC(*2000,'TETA    ','D.P.',1,MAXPRO     ,JTETA )                 
C                                                                               
      CALL INPROE(*500,*501,MAXPRO,IV(JIDPRO),DV(JALFA),DV(JTETA),NPRO)         
C                                                                               
      JFITT=JALFA                                                               
      JFIT1=JTETA                                                               
      CALL JFREE(*2000,'TETA    ')                                              
      CALL JFREE(*2000,'ALFA    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,NPRO*3     ,JIDPRO)                 
      CALL JALLOC(*2000,'ALFA    ','D.P.',1,NPRO       ,JALFA )                 
      CALL JALLOC(*2000,'TETA    ','D.P.',1,NPRO       ,JTETA )                 
      CALL IMOVE(IV(JIDPRO+MAXPRO),IV(JIDPRO+NPRO),NPRO)                        
      CALL IMOVE(IV(JIDPRO+2*MAXPRO),IV(JIDPRO+2*NPRO),NPRO)                    
      CALL DMOVE(DV(JFITT),DV(JALFA),NPRO)                                      
      CALL DMOVE(DV(JFIT1),DV(JTETA),NPRO)                                      
      CALL WRPROE(IV(JIDPRO),DV(JALFA),DV(JTETA),NPRO,                          
     *iv(jidmat),dv(jelas),nmat)                                                
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).EQ.'CONN')GOTO 820                                           
      WRITE(IOUT,752)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 820  CONTINUE                                                                  
      CALL JALLOC(*2000,'SCOMP   ','D.P.',1,36         ,JSCOMP)                 
      CALL JALLOC(*2000,'TRSMT   ','D.P.',1,36         ,JTRSMT)                 
      CALL JALLOC(*2000,'TMPMT   ','D.P.',1,36         ,JTMPMT)                 
C                                                                               
      IER=0                                                                     
      CALL INCONE(*500,*501,IV(JIDMAT),DV(JELAS),DV(JDENS),DV(JCDTER),          
     1          NMAT,IV(JIDPRO),DV(JALFA),DV(JTETA),NPRO,                       
     2          IV(JICON),DV(JVETT),IV(JLABEL),DV(JCOOR),                       
     3         DV(JSCOMP),DV(JTRSMT),DV(JTMPMT),IV(JNDCNT),IER)                 
      IF(IER.NE.0) IERG=1                                                       
      CALL JFREE(*2000,'TMPMT   ')                                              
      CALL JFREE(*2000,'TRSMT   ')                                              
      CALL JFREE(*2000,'SCOMP   ')                                              
      CALL JFREE(*2000,'TETA    ')                                              
      CALL JFREE(*2000,'ALFA    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
C                                                                               
      CALL JFREE(*2000,'CDTER   ')                                              
C                                                                               
      CALL JFREE(*2000,'DENS    ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTE): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTE): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 100  FORMAT(A80)                                                               
 751  FORMAT(' ***   AVVISO (INPUTE): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO PROPRIETA'' ELEMENTI PIANI   ***')           
 752  FORMAT(' ***   AVVISO (INPUTE): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO CONNESSIONI ELEMENTI PIANI   ***')           
      END                                                                       
C----------------------------------------- fine -------                         
C@ELT,I ANBA*ANBA.INPUTG                                                        
C*************************          INPUTG          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTG(ISCHE,IERG,JLABEL,JCOOR,JICON,JVETT,JNDCNT)             
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
C                                                                               
          NMAT=1                                                                
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).NE.'MATE')GOTO 300                                           
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXMAT=(MEMDIS-9-9*7-JIFITT)/5                                            
      call jalmax(2,memdisp)                                                    
      maxmat=(memdisp-9)/(5)                                                    
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,MAXMAT     ,JIDMAT)                 
      CALL JALLOC(*2000,'CAR1    ','D.P.',1,MAXMAT*2   ,JCAR1 )                 
C                                                                               
      CALL INMATG(*500,*501,MAXMAT,IV(JIDMAT),DV(JCAR1),NMAT)                   
      JFITT=JCAR1                                                               
      CALL JFREE(*2000,'CAR1    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,NMAT       ,JIDMAT)                 
      CALL JALLOC(*2000,'CAR1    ','D.P.',1,NMAT*2     ,JCAR1 )                 
      CALL DMOVE(DV(JFITT),DV(JCAR1),NMAT*2)                                    
      CALL WRMATG(IV(JIDMAT),DV(JCAR1),NMAT)                                    
      READ(ISYM,100)ISCHE                                                       
 300  IF(ISCHE(:4).EQ.'PROP')GOTO 310                                           
      WRITE(IOUT,711)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 310  CONTINUE                                                                  
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXPRO=(MEMDIS-9-11*7-JIFITT)/6                                           
      call jalmax(2,memdisp)                                                    
      maxpro=(memdisp-9)/(6)                                                    
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,MAXPRO*2   ,JIDPRO)                 
      CALL JALLOC(*2000,'SPLU    ','D.P.',1,MAXPRO*2   ,JSPLU )                 
      CALL INPROG(*500,*501,MAXPRO,IV(JIDPRO),DV(JSPLU),NPRO)                   
      JFITT=JSPLU                                                               
      CALL JFREE(*2000,'SPLU    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,NPRO*2     ,JIDPRO)                 
      CALL JALLOC(*2000,'SPLU    ','D.P.',1,NPRO*2     ,JSPLU )                 
      CALL IMOVE(IV(JIDPRO+MAXPRO),IV(JIDPRO+NPRO),NPRO)                        
      CALL DMOVE(DV(JFITT),DV(JSPLU),NPRO*2)                                    
      CALL WRPROG(IV(JIDPRO),DV(JSPLU),NPRO)                                    
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).EQ.'CONN')GOTO 320                                           
      WRITE(IOUT,712)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 320      CONTINUE                                                              
      IER=0                                                                     
      CALL INCONG(*500,*501,IV(JIDMAT),DV(JCAR1),NMAT,IV(JIDPRO),               
     1     DV(JSPLU),NPRO,IV(JLABEL),DV(JCOOR),DV(JVETT),IV(JICON),             
     2     IV(JNDCNT),IER)                                                      
C                                                                               
      IF(IER.NE.0) IERG=1                                                       
      CALL JFREE(*2000,'SPLU    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JFREE(*2000,'CAR1    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTG): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTG): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 100  FORMAT(A80)                                                               
 711  FORMAT(' ***   AVVISO (INPUTG): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO PROPRIETA'' GIUNZIONI   ***')                
 712  FORMAT(' ***   AVVISO (INPUTG): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO CONNESSIONI GIUNZIONI   ***')                
      END                                                                       
C@ELT,I ANBA*ANBA.INPUTL                                                        
C*************************          INPUTL          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTL(ISCHE,IERG,JLABEL,JCOOR,JICON,JVETT,JNDCNT)             
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
C                                                                               
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).NE.'MATE') GOTO 600                                          
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXMAT=(MEMDIS-84-7*12-JIFITT)/75                                         
      call jalmax(5,memdisp)                                                    
      maxmat=(memdisp-84)/75                                                    
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,MAXMAT     ,JIDMAT)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,MAXMAT*36  ,JELAS )                 
      CALL JALLOC(*2000,'DENS    ','D.P.',1,MAXMAT     ,JDENS )                 
      CALL JALLOC(*2000,'TMPMT   ','D.P.',1,36         ,JTMPMT)                 
      CALL JALLOC(*2000,'PERM    ','D.P.',1,6          ,JPERM )                 
C                                                                               
      CALL INMATL(*500,*501,MAXMAT,IV(JIDMAT),DV(JELAS),DV(JDENS),NMAT,         
     1         DV(JPERM),DV(JTMPMT),IER)                                        
C                                                                               
      IF(IER.NE.0) IERG=1                                                       
      JFITT=JELAS                                                               
      JFIT1=JDENS                                                               
      CALL JFREE(*2000,'PERM    ')                                              
      CALL JFREE(*2000,'TMPMT   ')                                              
      CALL JFREE(*2000,'DENS    ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,NMAT       ,JIDMAT)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,NMAT*36    ,JELAS )                 
      CALL JALLOC(*2000,'DENS    ','D.P.',1,NMAT       ,JDENS )                 
C                                                                               
      CALL DMOVE(DV(JFITT),DV(JELAS),NMAT*36)                                   
      CALL DMOVE(DV(JFIT1),DV(JDENS),NMAT)                                      
      CALL WRMATL(IV(JIDMAT),DV(JELAS),DV(JDENS),NMAT)                          
      NR6=6                                                                     
      READ(ISYM,100)ISCHE                                                       
 600  IF(ISCHE(:4).EQ.'PROP') GOTO 610                                          
      WRITE(IOUT,741)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 610  CONTINUE                                                                  
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXPRO=(MEMDIS-15*7-JIFITT)/12                                            
      call jalmax(3,memdisp)                                                    
      maxpro=(memdisp-3)/12                                                     
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,MAXPRO*2   ,JIDPRO)                 
      CALL JALLOC(*2000,'SPES    ','D.P.',1,MAXPRO*4   ,JSPES )                 
      CALL JALLOC(*2000,'ALFA    ','D.P.',1,MAXPRO     ,JALFA )                 
C                                                                               
      CALL INPROL(*500,*501,MAXPRO,IV(JIDPRO),DV(JSPES),DV(JALFA),NPRO)         
      JFITT=JSPES                                                               
      JFIT1=JALFA                                                               
      CALL JFREE(*2000,'ALFA    ')                                              
      CALL JFREE(*2000,'SPES    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,NPRO*2     ,JIDPRO)                 
      CALL JALLOC(*2000,'SPES    ','D.P.',1,NPRO*4     ,JSPES )                 
      CALL JALLOC(*2000,'ALFA    ','D.P.',1,NPRO       ,JALFA )                 
      CALL IMOVE(IV(JIDPRO+MAXPRO),IV(JIDPRO+NPRO),NPRO)                        
      CALL DMOVE(DV(JFITT),DV(JSPES),NPRO*4)                                    
      CALL DMOVE(DV(JFIT1),DV(JALFA),NPRO)                                      
C                                                                               
      IER=0                                                                     
      CALL WRPROL(IV(JIDPRO),DV(JSPES),DV(JALFA),NPRO,IER)                      
      IF(IER.NE.0)IERG=1                                                        
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).EQ.'CONN')GOTO 620                                           
      WRITE(IOUT,742)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 620  CONTINUE                                                                  
      CALL JALLOC(*2000,'SCOMP   ','D.P.',1,36         ,JSCOMP)                 
      CALL JALLOC(*2000,'TRSMT   ','D.P.',1,36         ,JTRSMT)                 
      CALL JALLOC(*2000,'TMPMT   ','D.P.',1,36         ,JTMPMT)                 
C                                                                               
      IER=0                                                                     
      CALL INCONL(*500,*501,IV(JIDMAT),DV(JELAS),DV(JDENS),NMAT,                
     1          IV(JIDPRO),DV(JSPES),DV(JALFA),NPRO,                            
     2          IV(JICON),DV(JVETT),IV(JLABEL),DV(JCOOR),                       
     3          DV(JSCOMP),DV(JTRSMT),DV(JTMPMT),IV(JNDCNT),IER)                
C                                                                               
      IF(IER.NE.0)IERG=1                                                        
      CALL JFREE(*2000,'TMPMT   ')                                              
      CALL JFREE(*2000,'TRSMT   ')                                              
      CALL JFREE(*2000,'SCOMP   ')                                              
      CALL JFREE(*2000,'ALFA    ')                                              
      CALL JFREE(*2000,'SPES    ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JFREE(*2000,'DENS    ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTL): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTL): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 100  FORMAT(A80)                                                               
 741  FORMAT(' ***   AVVISO (INPUTL): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO PROPRIETA'' LAMINE   ***')                   
 742  FORMAT(' ***   AVVISO (INPUTL): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO CONNESSIONI LAMINE   ***')                   
      END                                                                       
C@ELT,I ANBA*ANBA.INPUTM                                                        
C*************************          INPUTM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTM(ISCHE,IERG,JLABEL,JCOOR,JNDCNT)                         
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
C                                                                               
      CALL JOPEN('*MPC    ',ISLMPC,1)                                           
      CALL JOPEN('*NWD    ',ISLNWD,1)                                           
      CALL JALLOC(*2000,'IFITT   ','INT.',1,1      ,JIFITT)                     
      CALL JFREE(*2000,'IFITT   ')                                              
C                                                                               
      MAXL=(MEMDIS-4-11*7-JIFITT)/8                                             
C                                                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXL        ,JICON )                
      CALL JALLOC(*2000,'RCOMP   ','D.P.',1,MAXL        ,JRCOMP)                
      CALL JALLOC(*2000,'VETT    ','D.P.',1,MAXL*2      ,JVETT )                
      CALL JALLOC(*2000,'ICOMP   ','INT.',1,MAXL        ,JICOMP)                
C                                                                               
      JLIM=JICOMP+MAXL-1                                                        
C                                                                               
      CALL INPMPC(*500,*501,IV(JICON),IV(JICOMP),DV(JRCOMP),MAXL,NMPC,          
     1     LMXMPC,IV(JLABEL),NNODI,DV(JCOOR),DV(JVETT),IV(JNDCNT),IER)          
C                                                                               
      CALL JFREE (*2000,'ICOMP   ')                                             
      CALL JFREE (*2000,'VETT    ')                                             
      CALL JFREE (*2000,'RCOMP   ')                                             
      CALL JFREE (*2000,'ICON    ')                                             
C                                                                               
      IF(NMPC.EQ.0)GOTO 30                                                      
C                                                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,LMXMPC      ,JICON )                
      CALL JALLOC(*2000,'ICOMP   ','INT.',1,LMXMPC      ,JICOMP )               
      CALL JALLOC(*2000,'RCOMP   ','D.P.',1,LMXMPC      ,JRCOMP)                
      CALL JALLOC(*2000,'IPOS    ','INT.',1,NMPC        ,JIPOS  )               
      CALL JALLOC(*2000,'INOD    ','INT.',1,NMPC        ,JINOD  )               
      CALL JALLOC(*2000,'IDOF    ','INT.',1,NMPC        ,JIDOF )                
      CALL JALLOC(*2000,'IPFIL   ','INT.',1,NMPC        ,JIPFIL)                
C                                                                               
C                                                                               
      CALL MPCORD(IV(JICON),IV(JICOMP),DV(JRCOMP),IV(JIPOS),IV(JINOD),          
     1   IV(JIDOF),IV(JIPFIL))                                                  
C                                                                               
      CALL JFREE (*2000,'IPFIL   ')                                             
      CALL JFREE (*2000,'IDOF    ')                                             
      CALL JFREE (*2000,'INOD    ')                                             
      CALL JFREE (*2000,'IPOS    ')                                             
      CALL JFREE (*2000,'RCOMP   ')                                             
      CALL JFREE (*2000,'ICOMP   ')                                             
      CALL JFREE (*2000,'ICON    ')                                             
C                                                                               
 30   CONTINUE                                                                  
      CALL JCLOSE(ISLMPC)                                                       
C     CALL JDELET(ISLMPC)                                                       
      CALL JCLOSE(ISLNWD)                                                       
C                                                                               
      RETURN                                                                    
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTM): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTM): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 2000 CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C@ELT,I ANBA*ANBA.INPUTN                                                        
C*************************          INPUTN          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTN(ISCHE,IERG,JLABEL,JCOOR)                                
      REAL*8 DV(1),dummy,temper,dertem                                          
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO            
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
C                                                                               
C     COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
C    *IAUTCN,IPGCON(6),MODEX(8),ICOU,IAUTO,IGEOM,C                              
C                                                                               
      COMMON /AUTCON/ DUMMY(18),IDUMMY(7),MODEX(8),IDUMM2(3),C                  
C                                                                               
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2 ,                      
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
      common /tempdf/ temper,dertem                                             
C                                                                               
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JALLOC(*2000,'IFITT   ','INT.',1,1      ,JIFITT)                     
      CALL JFREE(*2000,'IFITT   ')                                              
      NNODMX=(MEMDIS-1-6*7-JIFITT)/5                                            
C                                                                               
      CALL JALLOC(*2000,'COOR    ','D.P.',1,2*NNODMX    ,JCOOR )                
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODMX      ,JLABEL)                
C                                                                               
      JLIM=JLABEL+NNODMX-1                                                      
C                                                                               
C                                                                               
      CALL INPNOD(*500,*501,IV(JLABEL),DV(JCOOR),NNODMX,NNODI)                  
C                                                                               
      CALL JCLOSE(ISLNOD)                                                       
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI       ,JLABEL)                
      CALL JALLOC(*2000,'COOR    ','D.P.',1,2*NNODI     ,JCOOR )                
C                                                                               
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
C+    CALL RCPNOD(IV(JLABEL),DV(JCOOR),NNODI)                                   
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JREAD(ISLNOD,DV(JCOOR),4*NNODI)                                      
      CALL JCLOSE(ISLNOD)                                                       
C                                                                               
C     -------- eventuale lettura temperature nodali e loro derivate ----        
C     -- NOTA: le temperature nella scheda di ingresso vanno assegnate -        
C     --       subito dopo l'assegnazione dei nodi                     -        
C                                                                               
      LEGGO=0                                                                   
      READ(ISYM,100) ISCHE                                                      
      CALL JALLOC(*2000,'TMPR    ','D.P.',1,NNODI*3,JTMPR)                      
      CALL JALLOC(*2000,'TDER    ','D.P.',1,NNODI*3,JTDER)                      
      IER=0                                                                     
      if(ische(:4).eq.'TEMP') then                                              
      ilet=1                                                                    
      else                                                                      
      ilet=0                                                                    
      endif                                                                     
      CALL INPTEM(*500,*501,IV(JLABEL),DV(JTMPR),DV(JTDER),NNODI,IER,           
     *            ilet)                                                         
      CALL JOPEN('*TMP    ',ISTEMP,1)                                           
      CALL JWRITE(ISTEMP,DV(JTMPR),NNODI*3*2)                                   
      CALL JWRITE(ISTEMP,DV(JTDER),NNODI*3*2)                                   
      CALL JCLOSE(ISTEMP)                                                       
      CALL JFREE(*2000,'TDER    ')                                              
      CALL JFREE(*2000,'TMPR    ')                                              
C     ------------------- fine ----------------------                           
C                                                                               
      IF (ilet.eq.1) READ(ISYM,100)ISCHE                                        
      IFL=0                                                                     
      IF(ISCHE(:4).EQ.'RINU')IFL=1                                              
      CALL JOPEN('*RIN    ',ISLRIN,1)                                           
      CALL JALLOC(*2000,'NUMER   ','INT.',1,NNODI*2    ,JNUMER)                 
      CALL JALLOC(*2000,'NUNEW   ','INT.',1,NNODI      ,JNUNEW)                 
C                                                                               
      IER=0                                                                     
      CALL INPRIN(*500,*501,IV(JNUMER),IV(JNUNEW),IV(JLABEL),NNODI,IFL,         
     *            IER)                                                          
C                                                                               
      CALL JWRITE(ISLRIN,IV(JNUNEW),NNODI)                                      
      CALL JFREE(*2000,'NUNEW   ')                                              
      CALL JFREE(*2000,'NUMER   ')                                              
      CALL JCLOSE(ISLRIN)                                                       
C                                                                               
      RETURN                                                                    
                                                                                
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTN): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTN): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
 2000 CALL JHALT                                                                
      STOP                                                                      
 100  FORMAT(A80)                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.INPUT0                                                        
C*************************          INPUT0          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUT0                                                         
C                                                                               
      LOGICAL DUMP,incore                                                       
      CHARACTER ILETT*120,lett*120                                                     
      COMMON /JFTNIO/  INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                     
C                                                                               
      IF(DUMP(15)) THEN                                                         
        CALL INTEST                                                             
        WRITE(IOUT,300)                                                         
 300    FORMAT(48X,'***    FILE  DATI    ***'//)                                
      END IF                                                                    
      NRIG=0                                                                    
      IRIGA=0                                                                   
      N=2                                                                       
10      DO IO=1,120                                                           
          ILETT(IO:IO)=' '                                                      
        ENDDO                                                                
      READ(INPT,100,END=31)ILETT        

      if(ilett(1:7).eq.'include'.or.ilett(1:7).eq.'INCLUDE') then

        call intest
        WRITE(IOUT,101) 0,ILETT                                             
        do kol=9,120
          if(ilett(kol:kol).eq.'''') goto 333
        enddo                            
 333    continue     
        lett=ilett(9:kol-1)
        open(88,file=lett,err=339)
 334    DO IO=1,120                                                           
          ILETT(IO:IO)=' '                                                      
        enddo                                                                

        read(88,'(a)',end=338,err=338) ilett     

          call trims(ilett,lett,ifl)                                            
          if(ifl.ge.0) then                                                      
          IRIGA=IRIGA+1                                                             
          IF(DUMP(15)) THEN                                                         
            WRITE(IOUT,101) -IRIGA,ILETT                                             
            NRIG=NRIG+1                                                             
            IF(NRIG.GT.50) THEN                                                     
              CALL INTEST                                                           
              WRITE(IOUT,300)                                                       
              NRIG=0                                                                
            END IF                                                                  
          END IF                                                                    
          IF(ILETT(1:1).EQ.'=') GOTO 334                                          
          IF(ILETT(1:1).EQ.'$') GOTO 334                                         
          WRITE(ISYM,100)LETT
          endif                                                      
        goto 334            
 338    close(88)                    
        write(iout,*) 'Chiuso file include'
        goto10     
      else                                                
        IRIGA=IRIGA+1                                                             
        IF(DUMP(15)) THEN                                                         
          WRITE(IOUT,101) IRIGA,ILETT                                             
          NRIG=NRIG+1                                                             
          IF(NRIG.GT.50) THEN                                                     
            CALL INTEST                                                           
            WRITE(IOUT,300)                                                       
            NRIG=0                                                                
          END IF                                                                  
        END IF                                                                    

        IF(ILETT(1:1).EQ.'=') GOTO 10                                          
        IF(ILETT(1:1).EQ.'$') GOTO 10                                          
 20     IF(ILETT(1:4).EQ.'FINE') GOTO 30
        call trims(ilett,lett,ifl)                                            
        if(ifl.ge.0) WRITE(ISYM,100)LETT                                                      
        endif
      GOTO 10                                                                   
 31   WRITE(IOUT,702)                                                           
 702  FORMAT(/10X,'***   AVVISO (INPUT0) :',                                    
     *            ' NON PRESENTE SCHEDA FINEDATI, INSERITA   ***'/)             
 30   WRITE(ISYM,'(A8)')'FINEDATI'                                              
      REWIND ISYM                                                               
      RETURN                                                                    
 100  FORMAT(A)                                                              
 101  FORMAT(20X,I5,' :',5X,A) 
 339  write(iout,703) ilett                                              
 703  FORMAT(/10X,'***   AVVISO (INPUT0) :',                                    
     *            ' INCLUDE FILE (',A,') NON PRESENTE  ***'/)
      call jhalt             
      END            
	  subroutine trims(si,so,is)
	  character*(*) si,so
	  ls=len(si)
	  is=0
	  do i=1,ls
	  is=is+1
	  if(si(i:i).ne.' ') goto 33
      enddo
      is=-1
      return       
33    so(1:ls-is+1)=si(is:ls)
      return	  
      end	                                                             
C@ELT,I ANBA*ANBA.MATMUL                                                        
C*************************          MATMUL          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ�)                                         
      SUBROUTINE MATMUL(NR,NC,NRC,A,B,C)                                        
      REAL*8 A(*),B(*),C(*)                                                     
      NN=NR*NC                                                                  
      DO 1 I=1,NN                                                               
   1  C(I)=0.D0                                                                 
      LK=1                                                                      
      KJ=0                                                                      
      KK=0                                                                      
      DO 12 I=1,NR                                                              
      DO 11 J=1,NC                                                              
      DO 10 K=1,NRC                                                             
      C(LK)=C(LK)+A(K+KJ*NRC)*B(K+KK*NRC)                                       
  10  CONTINUE                                                                  
      LK=LK+1                                                                   
      KK=KK+1                                                                   
  11  CONTINUE                                                                  
      KJ=KJ+1                                                                   
      KK=0                                                                      
  12  CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MEDCAR                                                        
C*************************          MEDCAR          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MEDCAR(ELAS,CAR,DEN,SPES,ALFA)                                 
C                                                                               
C     AGGIORNA LA MATRICE ELAS CON I CONTRIBUTI DELLE VARIE LAMINE              
C     COSTITUENTI IL LAMINATO                                                   
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION VETTOR(9),T(9),DL(9),CAR(*),ELAS(*)                             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      DATA RAD/57.29577951D0/   
      RDIV=1.D0/(1.D0-CAR(4)*CAR(3))                                            
      DL(1)=CAR(1)*RDIV                                                         
      DL(4)=CAR(2)*CAR(4)*RDIV                                                  
      DL(7)=0.D0                                                                
      DL(5)=CAR(2)*RDIV                                                         
      DL(8)=0.D0                                                                
      DL(9)=CAR(5)                                                              
      DL(2)=DL(4)                                                               
      DL(3)=DL(7)                                                               
      DL(6)=DL(8)                                                               
C                       
      ALFA=ALFA/RAD                                                             
      CALL TRASF(ALFA,T)                                                        
      CALL MATMUL(3,3,3,T,DL,VETTOR)                                            
      CALL MATMUL(3,3,3,VETTOR,T,DL)                                            
      DO 40 K=1,9                                                               
      DL(K)=DL(K)*SPES                                                          
 40   CONTINUE                                                                  
      DEN=DEN+SPES*CAR(6)                                                       
      ELAS(1)=ELAS(1)+DL(1)                                                     
      ELAS(2)=ELAS(2)+DL(4)                                                     
      ELAS(3)=ELAS(3)+DL(5)                                                     
      ELAS(4)=ELAS(4)+DL(7)                                                     
      ELAS(5)=ELAS(5)+DL(8)                                                     
      ELAS(6)=ELAS(6)+DL(9)                                                     
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MPCORD                                                        
C*************************          MPCORD          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MPCORD(ICON,ICOMP,RCOMP,IPOS,INOD,IDOF,IPFIL)                  
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION ICON(*),ICOMP(*),RCOMP(*),IPOS(*),INOD(*),IDOF(*),              
     1          IPFIL(*)                                                        
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /ELGEN/ ITIP,IDMPC,LMPC,IVAR                                       
      CALL JPOSAB(ISLNWD,1)                                                     
      CALL JREAD(ISLNWD,IPFIL,NMPC)                                             
      DO 110 I=1,NMPC                                                           
      CALL JPOSAB(ISLMPC,IPFIL(I))                                              
      CALL JREAD(ISLMPC,ITIP,3)                                                 
      CALL JREAD(ISLMPC,ICON,LMPC)                                              
      CALL JREAD(ISLMPC,ICOMP,LMPC)                                             
      CALL JREAD(ISLMPC,RCOMP,LMPC*2)                                           
      IPOS(I)=I                                                                 
      INOD(I)=ICON(1)                                                           
      IDOF(I)=ICOMP(1)                                                          
110   CONTINUE                                                                  
C                                                                               
C     SORT DEI VETTORI IPOS,INOD,IDOF PER INOD CRESCENTE                        
C                                                                               
      M=NMPC                                                                    
10    M=M/2                                                                     
      IF(M.EQ.0) GOTO 60                                                        
      K=NMPC-M                                                                  
      J=1                                                                       
20    I=J                                                                       
30    N=I+M                                                                     
      IF(INOD(I).LE.INOD(N))GOTO 40                                             
      L=INOD(I)                                                                 
      INOD(I)=INOD(N)                                                           
      INOD(N)=L                                                                 
      L=IPOS(I)                                                                 
      IPOS(I)=IPOS(N)                                                           
      IPOS(N)=L                                                                 
      L=IDOF(I)                                                                 
      IDOF(I)=IDOF(N)                                                           
      IDOF(N)=L                                                                 
      I=I-M                                                                     
      IF(I.GE.1)GOTO 30                                                         
40    J=J+1                                                                     
      IF(J.GT.K) GOTO 10                                                        
      GOTO 20                                                                   
C                                                                               
C     SORT DEI VETTORI IPOS E IDOF PER IDOF CRESCENTE                           
C                                                                               
60    DO 120 I=1,NMPC                                                           
      IGRD=INOD(I)                                                              
      ICOM=IDOF(I)                                                              
      NSUP=NCS-1                                                                
      DO 130 K=1,NSUP                                                           
        L=K+I                                                                   
        IF(L.GT.NMPC)GOTO 125                                                   
        IF(INOD(L).NE.IGRD)GOTO 120                                             
        IF(ICOM.LT.IDOF(L))GOTO 130                                             
        IF(ICOM.EQ.IDOF(L))GOTO 70                                              
        IS=IPOS(L)                                                              
        IPOS(L)=IPOS(I)                                                         
        IPOS(I)=IS                                                              
        ICOM=IDOF(L)                                                            
        IDOF(L)=IDOF(I)                                                         
        IDOF(I)=ICOM                                                            
130   CONTINUE                                                                  
120   CONTINUE                                                                  
C                                                                               
C     SCRIVE SU FILE I DATI DEGLI MPC A PARTIRE DALL'ULTIMO                     
C                                                                               
125   DO 140 I=1,NMPC                                                           
      NN=NMPC-I+1                                                               
      IND=IPOS(NN)                                                              
      IND=IPFIL(IND)                                                            
      CALL JPOSAB(ISLMPC,IND)                                                   
      CALL JREAD(ISLMPC,ITIP,3)                                                 
      CALL JREAD(ISLMPC,ICON,LMPC)                                              
      CALL JREAD(ISLMPC,ICOMP,LMPC)                                             
      CALL JREAD(ISLMPC,RCOMP,LMPC*2)                                           
      IDMPC=I                                                                   
      CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,LMPC)                                             
      CALL JWRITE(ISLELM,ICOMP,LMPC)                                            
      CALL JWRITE(ISLELM,RCOMP,LMPC*2)                                          
      CALL JWRITE(ISLELM,INOD(NN),1)                                            
      CALL JWRITE(ISLELM,IDOF(NN),1)                                            
140   CONTINUE                                                                  
      RETURN                                                                    
70    WRITE(IOUT,200)                                                           
200   FORMAT(' *** AVVISO (MPCORD): DUE MPC ELIMINANO LO STESSO GDL ',          
     1  '***')                                                                  
      CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C@ELT,I ANBA*ANBA.RCPCON                                                        
C*************************          RCPCON          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)             
C                                                                               
C     SOSTITUISCE NEI VETTORI DI CONNESSIONE DEGLI ELEMENTI -ICON-              
C     LE LABEL DELLA NUMERAZIONE ESTERNA CON QUELLA INTERNA                     
C                                                                               
      REAL*8 COOR(2,*),VETT(*)                                                  
      DIMENSION NODCNT(*)                                                       
      DIMENSION ICON(*),LABEL(*)                                                
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      K=0                                                                       
      DO 10 KK=1,NCON                                                           
C       PRINT*,' NODO N.',KK                                                    
      IF(ICON(KK).EQ.0)GOTO 10                                                  
      K=K+1                                                                     
      IP=IPOSZ(ICON(KK),LABEL,NNODI)                                            
      IF(IP.NE.0) GOTO 20                                                       
      IER=1                                                                     
      GOTO 10                                                                   
 20   ICON(KK)=IP                                                               
      IK=K*2-1                                                                  
      VETT(IK)=COOR(1,IP)                                                       
      VETT(IK+1)=COOR(2,IP)                                                     
C       PRINT6,K,IK,VETT(IK),VETT(IK+1),IP                                      
C6    FORMAT(2I8,2E12.5,I8)                                                     
      NODCNT(IP)=1                                                              
10    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.RCPNOD                                                        
C*************************          RCPNOD          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RCPNOD(LABEL,COOR,NNODI)                                       
C                                                                               
C     RECUPERA DA FILE I VETTORI -LABEL- E -COOR-                               
C                                                                               
      REAL*8 COOR                                                               
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      DIMENSION LABEL(*),COOR(2,*)                                              
      CALL JREAD(ISLNOD,LABEL,NNODI)                                            
      CALL JREAD(ISLNOD,COOR,4*NNODI)                                           
      RETURN                                                                    
      END                                                                       
C                                                                               
C------------------inizio subroutine lettura e stampa temperature -------       
C@ELT,I ANBA*ANBA.INPTEM                                                        
C     ************************* INPTEM ********************************         
C                                                                               
C-UNI                                                                           
      SUBROUTINE INPTEM(*,*,LABEL,TEMP,TEMPD,NNODI,IER,ILET)                    
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      DIMENSION LABEL(*),TEMP(3,*),TEMPD(3,*)                                   
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      common /tempdf/ temper,dertem                                             
C                                                                               
      DO 10,I=1,NNODI                                                           
      DO 10,J=1,3                                                               
      TEMP(J,I)=temper                                                          
      TEMPD(J,I)=dertem                                                         
10    CONTINUE                                                                  
      if(ilet.eq.1) then                                                        
      DO 20,I=1,NNODI+1                                                         
      CALL READFF(ISYM,0,NF,*100,*120)                                          
      CALL INTGFF(*101,LAB,0,LENFL)                                             
      CALL DOUBFF(*101,T,0,LENFL)                                               
      CALL DOUBFF(*101,TD,0,LENFL)                                              
      IP=IPOSL(LAB,LABEL,NNODI)                                                 
      IF (IP.EQ.0) THEN                                                         
      WRITE(IOUT,500)                                                           
      IER=1                                                                     
      END IF                                                                    
      DO 30,J=1,3                                                               
      TEMP(J,IP)=T                                                              
      TEMPD(J,IP)=TD                                                            
30    CONTINUE                                                                  
20    CONTINUE                                                                  
      endif                                                                     
120   CONTINUE                                                                  
C     ***** inizio  stampa  temperature ****                                    
      WRITE(IOUT,510)                                                           
      WRITE(IOUT,515)                                                           
      DO 50,I=1,NNODI                                                           
      if( temp(3,i).ne.0. .or. tempd(3,i).ne.0.) then                           
      WRITE(IOUT,520) I,LABEL(I),TEMP(3,I),TEMPD(3,I)                           
      endif                                                                     
50    CONTINUE                                                                  
      RETURN                                                                    
100   RETURN1                                                                   
101   RETURN2                                                                   
C                                                                               
500   FORMAT(///,'*** AVVISO : INPTEM : ASSEGNATA TEMPERATURA PER NODO ',       
     1      'NON ESISTENTE')                                                    
510   FORMAT(//T45,' *** TEMPERATURE NODALI E LORO DERIVATE ***'/)              
515   FORMAT(T10,'N.RO',T20,'ID.NODO',T35,'TEMPERATURA',T50,'DERIVATA TE        
     +MP.'//)                                                                   
520   FORMAT(T8,I4,T20,I4,T33,E12.5,T48,E12.5)                                  
      END                                                                       
C                                                                               
C------------ fine subroutine lettura temperature e derivate --*--*-            

C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.RDCASE                                                        
C*************************          RDCASE          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RDCASE(nomefile,*)                                             
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      character*64 nomefile,nomeprt,filfit*32,filstr*32                         
      CHARACTER PARAM*8,MODAL*6,NAMFIL*4,IDESC*80,IMAGE*120                     
      character FINO*4,MODA*8                                                   
      CHARACTER*6 IMOD(9),IPRT(22),ISOL(2),IPDG(6),YES*2,NO*2                   
      CHARACTER METODO(2)*6,NEWFIL*5,NEW*5,RIFGEN*6,RIFLOC*6                    
      COMMON IV(1)                                                              
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat                            
      COMMON /PNTINT/ IFLPI                                                     
      COMMON /TESTA/  IDESC(2)                                                  
      COMMON /FILEOU/ NRF,ircfit,filfit,filstr,iunstr                           
      COMMON /SISRIF/ XS,YS,SS,CS,sclx,scly,IFLSR,iflsro,iflsrs                 
      COMMON /AUTCON/ ERR,CONST,Xz,Yz,Sz,Cz,PGCON(6),CDC(6),IAUTCN,             
     *IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                                    
      common /outnas/ nasmod,naspch                                             
      common /tempdf/ temper,dertem                                             
C                                                                               
C                                                                               
      DATA IMOD/'INDATI','ASSEMB','SOLCEN','CARSEZ',                            
     *          'SFOCEN','SOLEST','SFOEST','SOLTER','TUTTI '/                   
      DATA IPDG/'PATX  ','PATY  ','PATZ  ',                                     
     *          'PAMX  ','PAMY  ','PAMZ  '/                                     
      DATA YES/'SI'/,NO/'NO'/,ISOL/'INMEMO','FUOMEM'/,FINO/'FINO'/              
      DATA METODO/'LANCZO','SHIFT '/,NEW/'NUOVA'/                               
      DATA RIFLOC/'RIFLOC'/,RIFGEN/'RIFGEN'/                                    
      DATA IPRT/'MATELM','SOLPPA','TABELM','TERNOT',                            
     *          'MATASS','MATFAT','SOLCEN','SFOCEN',                            
     *          'ASSEMB','RIGEOM','SOLEST','SFOEST','NUMEQN',                   
     *          'CMPDEF','ECHO  ','HANBA1','SOLVIN','STPDEF','STPSFF',          
     *          'CMPDFF','STPDFF','TUTTI'/                                      
C                                                                               
      DATA IFL/1/,IER/0/,NSK/0/,NESEL/0/,NNSEL/0/,IP/-1/                        
      DATA IFLAG/0/,IFLOG/1/,rl/0.d0/                                           
      DATA sclx/1.d0/,scly/1.d0/                                                
      data nstrat/0/                                                           
C                                                                               
C                                                                               
      IFLPI=0                                                                   
      iunstr=0                                                                  
      CALL DEFLT                                                                
      if( nomefile(1:4).eq.'none'.or.nomefile(1:4).eq.'    ') then              
      print'(a)',' Nome file dati (max 32 char):'                               
      read'(a)',nomefile                                                        
      endif                                                                     
      print'(a,a)',' Ingresso dati da file: ',nomefile                          
      open(unit=inpt,file=nomefile,access='SEQUENTIAL',status='OLD')            
      open(unit=isym,file='symfile',access='SEQUENTIAL')                        
      call trovap(nomefile,ipop)                                                
      nomeprt=nomefile(1:ipop-1)//'.f06'                                        
      print'(a,a)',' Stampe su file       : ',nomeprt                           
      open(unit=iout,file=nomeprt,access='SEQUENTIAL')                          
      nomeprt=nomefile(1:ipop-1)//'.nas'                                        
      open(unit=nasmod,file=nomeprt,access='SEQUENTIAL')                        
      nomeprt=nomefile(1:ipop-1)//'.pch'                                        
      open(unit=naspch,file=nomeprt,access='SEQUENTIAL')                        
C                                                                               
      CALL READFF(INPT,ISYM,NF,*800,*800)                                       
      NSK=NSK+1                                                                 
      PARAM(1:6)='      '                                                       
      CALL CHARFF(*800,PARAM,IFLAG,LENFL)                                       
      IF (PARAM.EQ.'NOMEBD') THEN                                               
C                                                                               
       NAMFIL='    '                                                            
       NEWFIL='     '                                                           
       CALL CHARFF(*800,NAMFIL,IFLAG,LENFL)                                     
       IF(NF.EQ.3) CALL CHARFF(*800,NEWFIL,IFLAG,LENFL)                         
       IF(NEWFIL.EQ.NEW) THEN                                                   
        CALL JPREP(NAMFIL)                                                      
c        CALL DEFLT                                                             
       END IF                                                                   
        CALL JSTMMS(NAMFIL)                                                     
       CALL JOPEN('*SNO    ',ISLSNO,1)                                          
       CALL JOPEN('*SEL    ',ISLSEL,1)                                          
       CALL JALLOC(*2000,'IP      ','INT.',1,1,JIP)                             
C                                                                               
       IEX=JEXIST('*CHK    ')                                                   
       IF (IEX.NE.0) CALL RSTART                                                
      ELSE                                                                      
       WRITE(IOUT,777)                                                          
  777  FORMAT(/' ***   AVVISO (RDCASE) : OMESSO IL NOME DELLA BASE DATI'        
     *      ,'   ***'//)                                                        
       GOTO 999                                                                 
      END IF                                                                    
 10   CALL READFF(INPT,ISYM,NF,*800,*800)                                       
      NSK=NSK+1                                                                 
C                                                                               
C                                                                               
      PARAM(1:6)='      '                                                       
      CALL CHARFF(*800,PARAM,IFLAG,LENFL)                                       
      if(param(1:1).eq.'=') goto 10                                             
C                                                                               
C                                                                               
      IF(PARAM(1:6).EQ.'TITOLO') THEN                                           
        CALL TEXTFF(*800,IDESC(1))                                              
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'SOTTIT') THEN                                      
C                                                                               
        CALL TEXTFF(*800,IDESC(2))                                              
C                                                                               
C********* dati per output su programma di fitting per AGUSTA *****             
C                                                                               
C                                                                               
      ELSE IF(PARAM(1:8).EQ.'FITHANBA') THEN                                    
        CALL CHARFF(*800,FILFIT,IFLAG,LENFL)                                    
        CALL INTGFF(*800,IRCFIT,IFLAG,LENFL)                                    
C                                                                               
C********* modifica per inserire i dati della curvatura************             
C                                                                               
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'DTCUSV') THEN                                      
      IGEOM=1                                                                   
      DO 25 L=1,6                                                               
        CDC(L)=0.D0                                                             
        CALL DOUBFF(*800,CDC(L),IFLAG,LENFL)                                    
 25   CONTINUE                                                                  
C                                                                               
      C=DSQRT(CDC(4)**2+CDC(5)**2+CDC(6)**2) 
      if(dabs(cdc(3)-1.d0).gt.0.0001) igeom=0.d0                                   
C                                                                               
C***************** fine modifica **********************************             
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'MODESE') THEN                                      
C                                                                               
        DO 20 I=1,7                                                             
          MODEX(I)=1                                                            
 20     CONTINUE                                                                
C                                                                               
        DO 40 I=1,NF-1                                                          
          MODAL='      '                                                        
          CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                   
          DO 42 L=1,8                                                           
            IF(MODAL.EQ.IMOD(L)) THEN                                           
              MODEX(L)=0                                                        
              GOTO 40                                                           
            END IF                                                              
 42       CONTINUE                                                              
          WRITE(IOUT,1000) PARAM,MODAL                                          
          IER=1                                                                 
 40     CONTINUE                                                                
 130  CONTINUE                                                                  
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'TEMPDF') THEN                                      
       tmpdef=0                                                                 
       dertem=0                                                                 
       nf=(nf-1)/2                                                              
       do inf=1,nf                                                              
       CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                      
       IF(MODAL(1:6).EQ.'TEMPER')THEN                                           
       CALL DOUBFF(*800,temper,IFLAG,LENFL)                                     
       ELSE IF(MODAL(1:6).EQ.'DERTEM') THEN                                     
       CALL DOUBFF(*800,dertem,IFLAG,LENFL)                                     
       END IF                                                                   
       enddo                                                                    
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'TIPRIF') THEN                                      
       do ll=1,(nf-1)/2                                                         
       CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                      
       IF(MODAL(1:6).EQ.'COORDI')THEN                                           
         CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                    
         IF(MODAL(1:6).EQ.'RETTAN')THEN                                         
           IFLSR=0                                                              
         ELSE IF(MODAL(1:6).EQ.'CILIND') THEN                                   
           IFLSR=1                                                              
         ELSE                                                                   
          IFLSR=0                                                               
         ENDIF                                                                  
       ELSE IF(MODAL(1:6).EQ.'SPOSTA')THEN                                      
         CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                    
         IF(MODAL(1:6).EQ.'RETTAN')THEN                                         
           IFLSRO=0                                                             
         ELSE IF(MODAL(1:6).EQ.'CILIND') THEN                                   
           IFLSRO=1                                                             
         ELSE                                                                   
          IFLSRO=0                                                              
         ENDIF                                                                  
       ELSE IF(MODAL(1:6).EQ.'SFORZI')THEN                                      
         CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                    
         IF(MODAL(1:6).EQ.'RETTAN')THEN                                         
           IFLSRS=0                                                             
         ELSE IF(MODAL(1:6).EQ.'CILIND') THEN                                   
           IFLSRS=1                                                             
         ELSE                                                                   
          IFLSRS=0                                                              
         ENDIF                                                                  
       END IF                                                                   
       ENDDO                                                                    
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'PINTEG') THEN                                      
       CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                      
       IF(MODAL.EQ.'GAUSS')THEN                                                 
       IFLPI=0                                                                  
       ELSE IF(MODAL.EQ.'BORDO') THEN                                           
        IFLPI=1                                                                 
       ELSE                                                                     
        IFLPI=0                                                                 
       END IF                                                                   
C                                                                               
      ELSE IF(PARAM(1:7).EQ.'NASTRAN') THEN                                      
        CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                    
        IF(MODAL(1:6).EQ.'CONCIO') THEN                                      
         concio=0                                                                 
         nstrat=1                                                                 
         do ll=1,(nf-1)/2                                                         
           CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                      
           IF(MODAL(1:6).EQ.'DELTAZ')THEN                                           
             CALL DOUBFF(*800,concio,IFLAG,LENFL)                                    
           ELSE IF(MODAL(1:6).EQ.'STRATI')THEN                                      
             CALL intgFF(*800,nstrat,IFLAG,LENFL)                                    
           END IF                                                                   
         ENDDO                                                                    
         if(nstrat.gt.8) nstrat=8                                                  
         print*,' concio ',concio                                                  
         print*,' nstrat ',nstrat
        ELSE                                                  
         concio=0                                                                 
         nstrat=0                                                                 
        ENDIF
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'TRAVE3') THEN                                      
        CALL DOUBFF(*800,RL,IFLAG,LENFL)                                        
	do il=1,6                                                                      
        CALL DOUBFF(*800,RVe(IL),IFLAG,LENFL)                                   
	enddo                                                                          
        CALL intgff(*800,NELTRa,IFLAG,LENFL)                                    
	rl=rl/neltra                                                                   
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'SISRIF') THEN                                      
        CALL DOUBFF(*800,XS,IFLAG,LENFL)                                        
        CALL DOUBFF(*800,YS,IFLAG,LENFL)                                        
        CALL DOUBFF(*800,ALFA,IFLAG,LENFL)                                      
        if(nf.ge.5) CALL DOUBFF(*800,SCLX,IFLAG,LENFL)                          
        if(nf.eq.6) CALL DOUBFF(*800,SCLy,IFLAG,LENFL)                          
                                                                                
        ALFB=ALFA/57.295779513D0                                                
        SS=DSIN(ALFB)                                                           
        CS=DCOS(ALFB)                                                           
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'STAMPA') THEN                                      
      MODAL='      '                                                            
      CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                       
       IF(MODAL.EQ.IPRT(22)) THEN                                               
       DO 50 I=1,22                                                             
        DUMP(I)=.TRUE.                                                          
 50    CONTINUE                                                                 
C                                                                               
C                                                                               
       ELSE                                                                     
       DO 60 I=1,NF-1                                                           
C                                                                               
       DO 65 KPRT=1,21                                                          
         NCHAR=LEN(MODAL)                                                       
         IF(MODAL(1:NCHAR).NE.IPRT(KPRT)(1:NCHAR)) GOTO 65                      
         DUMP(KPRT)=.TRUE.                                                      
         GOTO 66                                                                
  65   CONTINUE                                                                 
C                                                                               
       WRITE(IOUT,1000) PARAM(1:6),MODAL                                        
       IER=2                                                                    
 66    MODAL='      '                                                           
        IF(I.LT.NF-1) CALL CHARFF(*800,MODAL,IFLAG,LENFL)                       
 60    CONTINUE                                                                 
      END IF                                                                    
C                                                                               
 120  CONTINUE                                                                  
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'SYMFIL') THEN                                      
      CALL INTGFF(*800,ISOM,IFLAG,LENFL)                                        
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'UNILET') THEN                                      
      CALL INTGFF(*800,INPT,IFLAG,LENFL)                                        
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'UNISTP') THEN                                      
      CALL INTGFF(*800,IOUT,IFLAG,LENFL)                                        
C                                                                               
      ELSE IF(PARAM(1:7).EQ.'FITTING') THEN                                     
      CALL CHARFF(*800,FILFIT,IFLAG,LENFL)                                      
      CALL INTGFF(*800,NRF,IFLAG,LENFL)                                         
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'OUTSFO') THEN                                      
      CALL CHARFF(*800,FILSTR,IFLAG,LENFL)                                      
      CALL CHARFF(*800,MODAL,IFLAG,LENFL)
      iunstr=66                                       
      open(iunstr,file=filstr)
      close(iunstr,status='DELETE')                                                                
      if(modal(1:1).eq.'F') then
      open(iunstr,file=filstr,form='formatted')
      else                               
      open(iunstr,file=filstr,form='unformatted')
      iunstr=-iunstr                               
      endif
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'TIPSOL') THEN                                      
      MODAL='      '                                                            
      CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                       
C                                                                               
      IF(MODAL.EQ.'INMEMO') THEN                                                
      INCOU=0                                                                   
      ELSE IF(MODAL.EQ.'FUOMEM') THEN                                           
      INCOU=1                                                                   
      ELSE                                                                      
      WRITE(IOUT,1000) PARAM,MODAL                                              
      IER=3                                                                     
      END IF                                                                    
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'VLVINC') THEN                                      
      CALL DOUBFF(*800,CONST,IFLAG,LENFL)                                       
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'VLSING') THEN                                      
      CALL DOUBFF(*800,ERR,IFLAG,LENFL)                                         
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'AUTSPC') THEN                                      
      MODAL='      '                                                            
      CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                       
C                                                                               
       IF(MODAL(1:2).EQ.YES) THEN                                               
       IAUTO=0                                                                  
       ELSE IF(MODAL(1:2).EQ.NO) THEN                                           
       IAUTO=1                                                                  
       ELSE                                                                     
       WRITE(IOUT,1000) PARAM,MODAL                                             
       IER=4                                                                    
       END IF                                                                   
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'VINPDG') THEN                                      
C                                                                               
        DO 70 I=1,(NF-1)/2                                                      
         MODAL='      '                                                         
         CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                    
         IF(LENFL.EQ.0)GOTO 140                                                 
C                                                                               
         DO 71 L=1,6                                                            
           IF(MODAL.EQ.IPDG(L)) THEN                                            
             IPGCON(L)=1                                                        
             PGCON(L)=0.D0                                                      
             CALL DOUBFF(*800,PGCON(L),IFLAG,LENFL)                             
             GOTO 70                                                            
           END IF                                                               
 71      CONTINUE                                                               
         WRITE(IOUT,1000) PARAM,MODAL                                           
         IER=5                                                                  
 70     CONTINUE                                                                
 140  CONTINUE                                                                  
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'CARICO') THEN                                      
C                                                                               
C     DO 91 L=1,6                                                               
C       CDC(L)=0.D0                                                             
C       CALL DOUBFF(*800,CDC(L),IFLAG,LENFL)                                    
C91   CONTINUE                                                                  
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'SETELE') THEN                                      
        IF(IP.EQ.-1) IFL=1                                                      
        ICOUNT=0                                                                
        DO 700 M=1,NF-1                                                         
          IF(ICOUNT.EQ.NF-1) GOTO 700                                           
          MODA='        '                                                       
          CALL CHARFF(*800,MODA,IFLAG,LENFL)                                    
          ICOUNT=ICOUNT+1                                                       
          IF(MODA(1:4).EQ.FINO) THEN                                            
            IINF=IV(JIP+IP)                                                     
            ISUP=0                                                              
            CALL INTGFF(*800,ISUP,IFLAG,LENFL)                                  
            ICOUNT=ICOUNT+1                                                     
            DO 720 I=IINF+1,ISUP                                                
              NESEL=NESEL+1                                                     
              IP=IP+1                                                           
              IV(JIP+IP)=I                                                      
 720        CONTINUE                                                            
          ELSE                                                                  
            NESEL=NESEL+1                                                       
            IP=IP+1                                                             
            CALL INTGFF(*800,IV(JIP+IP),IFLOG,LENFL)                            
           END IF                                                               
 700    CONTINUE                                                                
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'SETNOD') THEN                                      
        IF(IP.EQ.-1) IFL=-1                                                     
        ICOUNT=0                                                                
        DO 600 M=1,NF-1                                                         
          IF(ICOUNT.EQ.NF-1) GOTO 600                                           
            MODA='        '                                                     
            CALL CHARFF(*800,MODA,IFLAG,LENFL)                                  
            ICOUNT=ICOUNT+1                                                     
            IF(MODA(1:4).EQ.FINO) THEN                                          
              IINF=IV(JIP+IP)                                                   
              ISUP=0                                                            
              CALL INTGFF(*800,ISUP,IFLAG,LENFL)                                
              ICOUNT=ICOUNT+1                                                   
              DO 620 I=IINF+1,ISUP                                              
                NNSEL=NNSEL+1                                                   
                IP=IP+1                                                         
                IV(JIP+IP)=I                                                    
 620        CONTINUE                                                            
          ELSE                                                                  
            NNSEL=NNSEL+1                                                       
            IP=IP+1                                                             
            CALL INTGFF(*800,IV(JIP+IP),IFLOG,LENFL)                            
          END IF                                                                
 600    CONTINUE                                                                
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'RIFSFO') THEN                                      
       MODAL='      '                                                           
       CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                      
       ILOCA=3                                                                  
       IF(MODAL.EQ.'RIFLOC') ILOCA=1                                            
       IF(MODAL.EQ.'RIFGEN') ILOCA=2                                            
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'NCSPOS') THEN                                      
C                                                                               
      CALL INTGFF(*800,NCS,IFLAG,LENFL)                                         
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'METODO') THEN                                      
       CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                      
       IF(MODAL.EQ.'INSTAB') THEN                                               
         IF(MODEX(6).EQ.0) MODEX(6)=5                                           
           IF(NF.EQ.3) THEN                                                     
            CALL CHARFF(*800,MODAL,IFLAG,LENFL)                                 
            IF(MODAL(1:6).EQ.'INIZIA') MODEX(6)=3                               
            IF(MODAL(1:6).EQ.'CALCOL') MODEX(6)=4                               
           END IF                                                               
       ELSE                                                                     
         IF(MODAL(1:6).NE.'LANCZO' .AND. MODAL(1:5).NE.'SHIFT') THEN            
           WRITE(IOUT,1200)PARAM,MODAL                                          
           IER=6                                                                
         ELSE                                                                   
           IF(MODAL(1:5).EQ.'SHIFT'.AND.MODEX(6).EQ.0) MODEX(6)=2               
         END IF                                                                 
       END IF                                                                   
C                                                                               
      ELSE IF(PARAM(1:6).EQ.'FINECN')THEN                                       
        GOTO 200                                                                
      ELSE                                                                      
      WRITE(IOUT,1100)PARAM                                                     
      IER=7                                                                     
      END IF                                                                    
      GOTO 10                                                                   
C                                                                               
 200  CONTINUE                                                                  
      REWIND ISYM                                                               
      CALL PRIPAG                                                               
      CALL INTEST                                                               
      WRITE(IOUT,1400)                                                          
 1400 FORMAT(/////50X,'***   A N B A - 2   ***'                                 
     *              //50X,'ISTRUZIONI DI CONTROLLO'////)                        
      DO 400 I=1,NSK                                                            
        READ(ISYM,'(A)') IMAGE                                                  
        WRITE(IOUT,'(30X,A)')IMAGE                                              
 400  CONTINUE                                                                  
      REWIND ISYM                                                               
      CALL INTEST                                                               
C                                                                               
      WRITE(IOUT,902)NAMFIL,MODEX,IPGCON,ERR,CONST,IAUTO,MAXDIM,INPT,           
     1            IOUT,ISYM,INCOU,NRF                                
      WRITE(IOUT,903) ALFA,XS,YS,SCLX,SCLY                                
902   FORMAT(////////39X,'***  PRINCIPALI PARAMETRI DEL CALCOLO  ***'///        
     128X,'NOME BASE DATI ',35('.'),' :',8X,A4//                                
     A28X,'MODALITA'' DI ESECUZIONE ',26('.'),' :',4X,8I1/                      
     228X,'VINCOLI ATTIVI SUI PARAMETRI DI DEFORMAZIONE ..... :',6X,6I1/        
     328X,'VALORE PER TEST DI LABILITA'' ',21('.'),' :',E12.3/                  
     428X,'VALORE DEL COEFFICIENTE DI AUTOSPC ',15('.'),' :',E12.3/             
     428X,'PARAMETRO DI ATTIVAZIONE AUTOSPC ',17('.'),' :',I12/                 
     528X,'DIMENSIONE DEL VETTORE DI LAVORO ',17('.'),' :',I12/                 
     628X,'UNITA'' DI INPUT ',34('.'),' :',I12/                                 
     728X,'UNITA'' DI OUTPUT ',33('.'),' :',I12/                                
     828X,'UNITA'' DI SYM DEI DATI ',27('.'),' :',I12/                          
     928X,'SOLUZIONE RICHIESTA',31('.'),' :',I12/                               
     D28X,'RECORD DI POSIZIONAMENTO FILE OUTPUT',14('.'),' :',I12)              
903   FORMAT(
     A28X,'ROTAZIONE DEL SISTEMA DI RIFERIMENTO ',13('.'),' :',F12.4/           
     B28X,'ASCISSA DEL SISTEMA DI RIFERIMENTO   ',13('.'),' :',F12.4/             
     C28X,'ORDINATA      "             "        ',13('.'),' :',F12.4/             
     D28X,'COEFFICIENTE SCALA X                 ',13('.'),' :',F12.4/
     E28X,'COEFFICIENTE SCALA Y                 ',13('.'),' :',F12.4/
     X )              
C                                                                               
 999  CONTINUE                                                                  
      CALL JWRITE(ISLSNO,NNSEL,1)                                               
      PRINT*,NESEL,ifl,JIP,ip                                                   
      PRINT'(24I4)',(IV(JIP+IPOL),IPOL=0,IP-1)                                  
      CALL JWRITE(ISLSEL,NESEL,1)                                               
      IF(IFL.EQ.1) THEN                                                         
      JPE=JIP                                                                   
      JPN=JPE+NESEL                                                             
      ELSE                                                                      
      JPN=JIP                                                                   
      JPE=JPN+NNSEL                                                             
      END IF                                                                    
      IF(NESEL.NE.0) CALL JWRITE(ISLSEL,IV(JPE),NESEL)                          
      IF(NNSEL.NE.0) CALL JWRITE(ISLSNO,IV(JPN),NNSEL)                          
      CALL JCLOSE(ISLSEL)                                                       
      CALL JCLOSE(ISLSNO)                                                       
      CALL JCRERS                                                               
C                                                                               
      IF(IER.EQ.0) RETURN                                                       
 800      WRITE(IOUT,1200) IER,param                                            
          RETURN 1                                                              
 2000 CALL JHALT                                                                
      STOP                                                                      
 1200 FORMAT(/' *** AVVISO(RDCASE):DATI CONT ERRATI ',i2/'>',a,'< ***'/)        
 1000 FORMAT(/' ***   AVVISO (RDCASE): PAREMETRO ',A,' , NON RI',               
     *  'CONOSCIUTA L''OPZIONE ',A,'   ***'/)                                   
 1100 FORMAT(/' ***   AVVISO (RDCASE): PAREMETRO  -',A,'-  NON RI',             
     *  'CONOSCIUTO   ***'/)                                                    
          END                                                                   
C@ELT,I ANBA*ANBA.READFF                                                        
C*************************          READFF          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE READFF(INPT,ISYM,NF,*,*)                                       
C                                                                               
C     READ FREE FORMAT                                                          
C                                                                               
      IMPLICIT INTEGER (A-Z)                                                    
      logical dump,incore                                                       
      CHARACTER IMAST*121,IMAGE*120,IMAGC(121)*1,FORMAT*7,FRMINT*5
      character ich*4              
      CHARACTER TEXT*(*)                                                        
      CHARACTER CHAR*(*)                                                        
      DOUBLE PRECISION DOUB                                                     
      REAL REAL      
      integer intge                                                           
      COMMON /JFTNIO/ IPT,IOUT,IS,DUMP(22),INCORE,ILOCA                         
      EQUIVALENCE (IMAGE,IMAST,IMAGC)                                           
      EQUIVALENCE (ICH,INTGe)                                           
      DATA DLMT/3/,FORMAT/'(G  .0)'/,FRMINT/'(I  )'/,IMAGC(121)/'@'/            
      DATA IFLD,JCOL,EFLD/0,0,0/                                                
C                                                                               
c      IF(DLMT.EQ.0) GO TO 191                                                  
      do 1010 i=1,120                                                           
1010  image(i:i)=' '                                                            
99    READ(INPT,'(A)',END=190) IMAGE
      if(image(1:1).eq.'$') goto 99                                            
      if(image(1:1).eq.'=') goto 99                                            
      JCOL=0                                                                    
      DLMT=1                                                                    
      IF(IMAGE(1:1).EQ.'*'.AND.ISYM.EQ.0) RETURN 2                              
      IF(ISYM.NE.0) WRITE(ISYM,'(A)')IMAGE                                      
cc      if(inpt.eq.4) print*,image                                              
C                                                                               
      NF=0                                                                      
      MF=0                                                                      
      DO 10 I=1,120                                                             
       IF(IMAGC(I).EQ.' '.OR.IMAGC(I).EQ.',') THEN                              
        IF(I.GT.1) THEN                                                         
         IF(IMAGC(I).EQ.','.AND.IMAGC(I-1).EQ.',') NF=NF+1                      
        END IF                                                                  
       MF=0                                                                     
      ELSE                                                                      
       IF(MF.NE.0) GOTO 10                                                      
       MF=1                                                                     
       NF=NF+1                                                                  
      END IF                                                                    
  10  CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
  190 DLMT=0                                                                    
      PRINT*,'  READFF : FINE DATI'                                             
      RETURN 1                                                                  
  191 PRINT*,'  READFF : RDLMT=0'                                               
      RETURN 1                                                                  
C                                                                               
C                                                                               
C     PARAMETER-TYPE ENTRIES:                                                   
C                                                                               
      ENTRY TEXTFF(*,TEXT)                                                      
      IF(DLMT.NE.1.OR.JCOL.GE.120) GO TO 900                                    
      TEXT=IMAGE(JCOL+1:120)                                                    
      JCOL=121                                                                  
      DLMT=3                                                                    
      GO TO 900                                                                 
C                                                                               
      ENTRY CHARFF(*,CHAR,IFL,LFLD)                                             
      IF (IFL.EQ.0) CALL FIELD(DLMT,IFLD,JCOL,IMAGC,LFLD,EFLD,*900)             
      IF(LFLD.GT.LEN(CHAR)) GO TO 901
      do ilo=ifld,efld           
       ich(1:1)=image(ilo:ilo)
       if(intge.ge.97.and.intge.le.112) then
        intge=intge-32        
        image(ilo:ilo)=ich(1:1)
       endif  
      enddo          
      CHAR=IMAGE(IFLD:EFLD)                                                     
      GO TO 900                                                                 
C                                                                               
      ENTRY INTGFF(*,INTG,IFL,LFLD)                                             
      IF (IFL.EQ.0) CALL FIELD(DLMT,IFLD,JCOL,IMAGC,LFLD,EFLD,*900)             
      WRITE(FRMINT(3:4),'(I2)') LFLD                                            
      READ(IMAGE(IFLD:EFLD),FRMINT,ERR=901) INTG                                
      GO TO 900                                                                 
C                                                                               
      ENTRY REALFF(*,REAL,IFL,LFLD)                                             
      IF (IFL.EQ.0) CALL FIELD(DLMT,IFLD,JCOL,IMAGC,LFLD,EFLD,*900)             
      WRITE(FORMAT(3:4),'(I2)') LFLD                                            
      READ(IMAGE(IFLD:EFLD),FORMAT,ERR=901) REAL                                
      GO TO 900                                                                 
C                                                                               
      ENTRY DOUBFF(*,DOUB,IFL,LFLD)                                             
      IF (IFL.EQ.0) CALL FIELD(DLMT,IFLD,JCOL,IMAGC,LFLD,EFLD,*900)             
      WRITE(FORMAT(3:4),'(I2)') LFLD                                            
      READ(IMAGE(IFLD:EFLD),FORMAT,ERR=901) DOUB                                
C                                                                               
  900 RETURN                                                                    
  901 write(*,*) image,ifld,efld,image(ifld:efld)                               
      RETURN 1                                                                  
      END                                                                       
C@ELT,I ANBA*ANBA.FIELD                                                         
C*************************          FIELD           ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
C                                                                               
C                                                                               
      SUBROUTINE FIELD(DLMT,IFLD,JCOL,IMAGC,LFLD,EFLD,*)                        
      IMPLICIT INTEGER (A-Z)                                                    
      CHARACTER * 1 IMAGC(121)                                                  
C                                                                               
C     SET A FIELD                                                               
C                                                                               
      IF(DLMT.NE.1) RETURN 1                                                    
 130  IFLD=JCOL+1                                                               
          DO 110 JCOL=IFLD,120,1                                                
          IF(IMAGC(JCOL).EQ.','.OR.IMAGC(JCOL).EQ.' ') GOTO 120                 
 110      CONTINUE                                                              
          DLMT=0                                                                
  120 LFLD=JCOL-IFLD                                                            
      IF(LFLD.EQ.0) THEN                                                        
       IF(IMAGC(JCOL).EQ.',') RETURN 1                                          
       GOTO 130                                                                 
      ELSE                                                                      
       EFLD=JCOL-1                                                              
      END IF                                                                    
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.READFL                                                        
C*************************          READFL          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE READFL(ID,IERL,ITIP,CAR,IFL,D)                                 
      REAL*8 CAR(6,*),D                                                         
      REAL*4 BAR(6,6),B                                                         
      DATA IFL10,IFL11,ILF12,IFL13/0,0,0,0/                                     
      IERL=0                                                                    
      ID=-ID                                                                    
      GOTO (10,20,30,40,40),ITIP                                                
 10   IF(IFL10.EQ.0) THEN                                                       
       OPEN(10,ACCESS='DIRECT',FORM='UNFORMATTED',RECL=44,                      
     *      STATUS='UNKNOWN')                                                   
       IFL10=1                                                                  
      END IF                                                                    
      READ(10,REC=ID)IDM,BAR(1,1),B                                             
      CAR(1,1)=BAR(1,1)                                                         
      D=B                                                                       
      GOTO 50                                                                   
 20   IF(IFL11.EQ.0) THEN                                                       
       OPEN(11,ACCESS='DIRECT',FORM='UNFORMATTED',RECL=44,                      
     *      STATUS='UNKNOWN')                                                   
       IFL11=1                                                                  
      END IF                                                                    
      READ(11,REC=ID)IDM,BAR(1,1),B                                             
      CAR(1,1)=BAR(1,1)                                                         
      D=B                                                                       
      GOTO 50                                                                   
 30   IF(IFL12.EQ.0) THEN                                                       
       OPEN(12,ACCESS='DIRECT',FORM='UNFORMATTED',RECL=52,                      
     *      STATUS='UNKNOWN')                                                   
       IFL12=1                                                                  
      END IF                                                                    
      READ(12,REC=ID)IDM,(BAR(L,1),L=1,6)                                       
      DO 70 I=1,6                                                               
 70   CAR(I,1)=BAR(I,1)                                                         
      GOTO 50                                                                   
 40   IF(IFL13.EQ.0) THEN                                                       
       OPEN(13,ACCESS='DIRECT',FORM='UNFORMATTED',RECL=188,                     
     *      STATUS='UNKNOWN')                                                   
       IFL13=1                                                                  
      END IF                                                                    
      READ(13,REC=ID)IDM,IFL,((BAR(I,J),I=1,6),J=1,6),B                         
      DO 80 I=1,6                                                               
      DO 80 L=1,6                                                               
 80   CAR(I,J)=BAR(I,J)                                                         
      D=B                                                                       
 50   IF(IDM.NE.ID)IERL=1                                                       
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.TRASF                                                         
C*************************          TRASF          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE  TRASF(ALFA,T)                                                 
      REAL*8 ALFA,T,A,B                                                         
      DIMENSION T(*)                                                            
      A=DCOS(ALFA)                                                              
      B=DSIN(ALFA)                                                              
      T(1)=A**2                                                                 
      T(2)=B**2                                                                 
      T(3)=-2.*A*B                                                              
      T(4)=B**2                                                                 
      T(5)=A**2                                                                 
      T(6)=2.*A*B                                                               
      T(7)=A*B                                                                  
      T(8)=-T(7)                                                                
      T(9)=(A**2)-(B**2)                                                        
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.TRASFM                                                        
C*************************          TRASFM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TRASFM(RCOMP,TRSMT,SCOMP,NR6,ANGLE)                            
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION  RCOMP(NR6,*),SCOMP(NR6,*),TRSMT(NR6,*)                         
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      IF(ANGLE.EQ.0.D0) RETURN                                                  
      CALL DZERO(TRSMT,NR6*NR6)                                                 
      ALFA=ANGLE/RAD                                                            
      COSA=DCOS(ALFA)                                                           
      SINA=DSIN(ALFA)                                                           
      COS2=COSA*COSA                                                            
      SIN2=SINA*SINA                                                            
      SICO=SINA*COSA                                                            
      IF(ANGLE.EQ.90.D0) THEN                                                   
      COSA=0.D0                                                                 
      SINA=1.D0                                                                 
      COS2=0.D0                                                                 
      SIN2=1.D0                                                                 
      SICO=0.D0                                                                 
      ENDIF                                                                     
      TRSMT(1,1)=COS2-SIN2                                                      
      TRSMT(1,3)=SICO                                                           
      TRSMT(1,4)=-SICO                                                          
      TRSMT(2,2)=COSA                                                           
      TRSMT(2,6)=-SINA                                                          
      TRSMT(3,1)=-2.D0*SICO                                                     
      TRSMT(3,3)=COS2                                                           
      TRSMT(3,4)=SIN2                                                           
      TRSMT(4,1)=2.D0*SICO                                                      
      TRSMT(4,3)=SIN2                                                           
      TRSMT(4,4)=COS2                                                           
      TRSMT(5,5)=1.D0                                                           
      TRSMT(6,2)=SINA                                                           
      TRSMT(6,6)=COSA                                                           
      CALL DPROMM(TRSMT,NR6,6,6,RCOMP,NR6,SCOMP,NR6,6,0,0)                      
      CALL DPROMM(SCOMP,NR6,6,6,TRSMT,NR6,RCOMP,NR6,6,0,1)                      
C                                                                               
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.PRNUME                                                        
C*************************          PRNUME          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PRNUME(NUME,NUINT,NUEST,NODMPC,ICOMP,ICON,RCOMP,MINMPC,        
     1                  LABEL)                                                  
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      DIMENSION NUME(*),NUINT(*),NUEST(*),NODMPC(*),ICOMP(*),ICON(*),           
     1  RCOMP(*),MINMPC(*),LABEL(*)                                             
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,nn67,nn80,nn87          
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /ELGEN/ ITIP,IDMPC,LMPC,IVAR                                       
      DATA MPCCOD/6/                                                            
C                                                                               
      CALL JPOSAB(ISLELM,1)                                                     
      NODI=NNODI                                                                
      IF(NMPC.EQ.0)GOTO 30                                                      
      DO 40 I=1,NMPC                                                            
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,NODMPC,LMPC)                                            
      CALL JREAD(ISLELM,ICOMP,LMPC)                                             
      CALL JREAD(ISLELM,RCOMP,2*LMPC)                                           
      CALL JREAD(ISLELM,MIN,1)                                                  
      CALL JREAD(ISLELM,ICON(I),1)                                              
      IF(ITIP.NE.MPCCOD)  GOTO 251                                              
      DO 50 L=1,NODI                                                            
      IF(MIN.EQ.NUINT(L)) MINMPC(I)=L                                           
      IF(MIN.LT.NUINT(L))NUINT(L)=NUINT(L)+1                                    
50    CONTINUE                                                                  
      NODI=NODI+1                                                               
      NUINT(NODI)=MIN+1                                                         
40    CONTINUE                                                                  
      WRITE(IOUT,208)(MINMPC(LK),LK=1,NMPC)                                     
      WRITE(IOUT,206)(ICON(LK),LK=1,NMPC)                                       
30    DO 20 K=1,NODI                                                            
      K2=NUINT(K)                                                               
      NUEST(K2)=K                                                               
20    CONTINUE                                                                  
      IF(NODI.EQ.NNODI+NMPC) GOTO 140                                           
      WRITE(IOUT,200) NODI,NNODI                                                
      CALL JHALT                                                                
      STOP                                                                      
140   NEQ=0                                                                     
      DO 60 I=1,NODI                                                            
      IPUN=(I-1)*NCS                                                            
      IF(NUEST(I).LE.NNODI)GOTO 80                                              
      IF(I.EQ.1) GOTO 250                                                       
      IND=NUEST(I)-NNODI                                                        
      JNOD=MINMPC(IND)                                                          
      JNOD=NUINT(JNOD)                                                          
      JDOF=ICON(IND)                                                            
      IPNOD=(JNOD-1)*NCS                                                        
      IF(JDOF.GE.NCS)GOTO 65                                                    
      DO 90 M=1,NCS                                                             
      IF(M.LE.JDOF) GOTO 90                                                     
      NUME(IPNOD+M)=NUME(IPNOD+M)+1                                             
90    CONTINUE                                                                  
      NEQ=NUME(IPNOD+NCS)                                                       
      NUME(IPUN+1)=NEQ-NCS+JDOF                                                 
68    DO 95 M=2,NCS                                                             
      NUME(IPUN+M)=0                                                            
95    CONTINUE                                                                  
      GOTO 60                                                                   
65    NEQ=NEQ+1                                                                 
      NUME(IPUN+1)=NEQ                                                          
      GOTO 68                                                                   
80    DO 70 K=1,NCS                                                             
      NEQ=NEQ+1                                                                 
      NUME(IPUN+K)=NEQ                                                          
70    CONTINUE                                                                  
60    CONTINUE                                                                  
      IF(NPSI.EQ.0) GOTO 130                                                    
      DO 110 I=1,NPSI                                                           
      IPUN=(NNODI+NMPC+I-1)*NCS                                                 
      NEQ=NEQ+1                                                                 
      NUME(IPUN+1)=NEQ                                                          
      IF(NCS.EQ.1) GOTO 110                                                     
      DO 120 K=2,NCS                                                            
      NUME(IPUN+K)=0                                                            
120   CONTINUE                                                                  
110   CONTINUE                                                                  
130   CONTINUE                                                                  
      IF(.NOT.DUMP(13)) GOTO 150                                                
      CALL INTEST                                                               
      WRITE(IOUT,202)                                                           
      ICONT=0                                                                   
      WRITE(IOUT,207)                                                           
      NSUP=NNODI+NMPC                                                           
      DO 555 JK=1,NSUP                                                          
      JJ=NUINT(JK)                                                              
      NN=(JJ-1)*NCS+1                                                           
      ICONT=ICONT+1                                                             
      IF(ICONT.LE.50)GOTO 160                                                   
      CALL INTEST                                                               
      WRITE(IOUT,207)                                                           
      ICONT=0                                                                   
 160  NLIM=NN+NCS-1                                                             
      WRITE(IOUT,205)JK,LABEL(JK),NUINT(JK),NUEST(JK)                           
     1     ,(NUME(L),L=NN,NLIM)                                                 
555   CONTINUE                                                                  
      ICONT=ICONT+1                                                             
      IF(ICONT.GT.50)CALL INTEST                                                
      IND=(NNODI+NMPC)*NCS+1                                                    
      NSUP=IND+NPSI*NCS-1                                                       
      WRITE(IOUT,211)(NUME(K),K=IND,NSUP,NCS)                                   
 211  FORMAT(//15X,'EQUAZIONI CORRISPONDENTI AI PARAMETRI DI ',                 
     1  'DEFORMAZIONE :',6I8)                                                   
      IF(NEQ.EQ.NEQV)GOTO 150                                                   
      WRITE(IOUT,201)NEQ,NEQV                                                   
      CALL JHALT                                                                
      STOP                                                                      
150   CALL JPOSAB(ISLNUM,1)                                                     
      CALL JWRITE(ISLNUM,NUME,NEQT)                                             
      CALL JWRITE(ISLNUM,NUINT,NODI)                                            
      CALL JWRITE(ISLNUM,NUEST,NODI)                                            
      RETURN                                                                    
250   WRITE(IOUT,209)                                                           
      CALL JHALT                                                                
      STOP                                                                      
251   WRITE(IOUT,210)                                                           
      CALL JHALT                                                                
      STOP                                                                      
201   FORMAT(' *** AVVISO (PRNUME): -NEQ- ED -NEQV- NON CORRISPONDONO',         
     1  2I8,'  ***')                                                            
202   FORMAT(//20X,'***   CORRISPONDENZE NUMERAZIONE ESTERNA/INTERNA',          
     1    ' E NODI/EQUAZIONI   ***'/)                                           
205   FORMAT(T10,4I8,4X,6I6)                                                    
206   FORMAT(/T20,'VETTORE COMPONENTI MPC'/18I6)                                
207   FORMAT(//T12,'  N.NODO  LABEL   NUINT   NUEST',                           
     1            '    NUMERAZIONE EQUAZIONI'/)                                 
208   FORMAT(/T20,'VETT. NODO MINIMO PER MPC'/18I6)                             
209   FORMAT(' *** AVVISO (PRNUME): LA PRIMA EQUAZIONE E'' UN MPC',             
     1  '  ***')                                                                
210   FORMAT(' *** AVVISO (PRNUME): UN ELEMENTO LETTO NEL - DO 40 -',           
     1  ' NON E'' UN MPC  ***')                                                 
200   FORMAT(' *** AVVISO (PRNUME): NON CORRISPONDONO I VALORI DI',             
     1  ' -NODI- ED -NNODI+NMPC-',2I8,' ***')                                   
      END                                                                       
C*****************************************************************              
C@ELT,I ANBA*ANBA.INPSTR                                                        
C************************         INPSTR        ************************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPSTR                                                         
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE E' FITTIZIA E CONTIENE GLI ENTRY RELATIVI               
C     AGLI ELEMENTI STRATIFICATI                                                
C                                                                               
C     INMATS - LETTURA  MATERIALI                                               
C     INPROS - LETTURA  PROPRIETA'                                              
C     INCONS - LETTURA  CONNESSIONI                                             
C     WRMATS - STAMPA MATERIALI                                                 
C     WRPROS - STAMPA PROPRIETA'                                                
C                                                                               
C                                                                               
      IMPLICIT REAL*8  (A-H,O-Z)                                                
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /PARSTR/ NMXSTR                                                    
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3         
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELSTR/  INT1,INT2,DEN,THICK,ANGLAM                                
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstratnas 
      
                           
      common /outnas/ nasmod,naspch                                             
                                                                                
      DIMENSION ELAS(6,6),DENS(*),PERM(6),TMPMT(6,6),IDPRO(*),                  
     1          SPES(*),ORIEN(*),ICON(*),VETT(*),IMATER(*),                     
     2          LABEL(*),COOR(2,*),SCOMP(6,1),TRSMT(6,1),POMP(6,1),             
     3          IVP(6),VOTT(24),NODCNT(*),IDMAT(*),ZMMM(6,6),idnt(12)                    
C                                                                               
c     DATA IVP/6,5,3,1,2,4/                                                     
      DATA IVP/4,5,1,2,3,6/                                                     
C                                                                               
      ENTRY INMATS(*,*,MAXMAT,IDMAT,ELAS,DENS,NMAT,PERM,TMPMT,IER)              
C                                                                               
      IER=0                                                                     
      DO 10 I=1,MAXMAT                                                          
      IB=(I-1)*6                                                                
      NS=0                                                                      
 101  CALL READFF(ISYM,0,NF,*107,*40)                                           
      NI=NS+1                                                                   
      NS=NI+NF-1                                                                
      DO 100 K=NI,NS                                                            
      CALL DOUBFF(*108,VOTT(K),0,LENFL)                                         
 100  CONTINUE                                                                  
       IDMAT(I)=VOTT(1)                                                         
  31  FORMAT(/10X,'  ID. MATER    ',I8/)                                        
       IFLAG=VOTT(2)                                                            
      IF((NS.LT.24.AND.IFLAG.NE.0) . OR .                                       
     *   (NS.LT.12.AND.IFLAG.EQ.0))  GOTO 101                                   
      IF(NS.GT.24) THEN                                                         
       WRITE(IOUT,906) IDMAT(I)                                                 
 906   FORMAT(' ***   AVVISO (INMATS) : N.RO DI CAMPI INESATTO PER ',           
     *        'MATERIALE N.RO',I8,'   ***')                                     
       IER=1                                                                    
       GOTO 10                                                                  
      ELSE                                                                      
       IF(IFLAG.EQ.0) THEN                                                      
        IFLAG=-1                                                                
        EX=VOTT(3)                                                              
        EY=VOTT(4)                                                              
        EZ=VOTT(5)                                                              
        RNUXY=VOTT(6)                                                           
        RNUYZ=VOTT(7)                                                           
        RNUXZ=VOTT(8)                                                           
        GXY=VOTT(9)                                                             
        GYZ=VOTT(10)                                                            
        GZX=VOTT(11)                                                            
        DEN=VOTT(12)                                                            
        CALL DZERO(VOTT(3),22)                                                  
        VOTT(3)=1./EX                                                           
        VOTT(4)=-RNUXY/EX                                                       
        VOTT(5)=-RNUXZ/EX                                                       
        VOTT(9)=1./EY                                                           
        VOTT(10)=-RNUYZ/EY                                                      
        VOTT(14)=1./EZ                                                          
        VOTT(18)=1./GXY                                                         
        VOTT(21)=1./GYZ                                                         
        VOTT(23)=1./GZX                                                         
        VOTT(24)=DEN                                                            
       END IF                                                                   
       IND=2                                                                    
       DO 103 K=1,6                                                             
       DO 103 J=K,6                                                             
        IND=IND+1                                                               
        ELAS(K,J+IB)=VOTT(IND)                                                  
 103   CONTINUE                                                                 
       DENS(I)=VOTT(24)                                                         
      END IF                                                                    
      DO 20 KR=2,6                                                              
      NSUP=KR-1                                                                 
      DO 20 KC=1,NSUP                                                           
      ELAS(KR,KC+IB)=ELAS(KC,KR+IB)                                             
 20   CONTINUE                                                                  
      IF(IFLAG.GT.0)GOTO 55                                                     
      CALL INVERT(ELAS(1,IB+1),6,TMPMT,6,6,PERM)                                
      DO 30 J=1,6                                                               
      DO 30 K=1,6                                                               
      ELAS(J,K+IB)=TMPMT(J,K)                                                   
 30   CONTINUE                                                                  
 55   CONTINUE                                                                  
 10   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*107,*40)                                           
      WRITE(IOUT,900)MAXMAT                                                     
      CALL JHALT                                                                
      STOP                                                                      
 107  RETURN 1                                                                  
 108  RETURN 2                                                                  
 40   NMAT=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY INPROS(*,*,MAXPRO,IDPRO,IMATER,SPES,ORIEN,NPRO)                     
C                                                                               
      DO 50 I=1,MAXPRO                                                          
      CALL READFF(ISYM,0,NF,*80,*80)                                            
      CALL INTGFF(*108,IDPRO(I),0,IL)                                           
      NF=NF-1                                                                   
      NSTRAT=NF/3                                                               
      ICNTST=0                                                                  
      DO 60 L=1,NSTRAT                                                          
      ICNTST=ICNTST+1                                                           
      IF(ICNTST.GT.NMXSTR) THEN                                                 
      WRITE(IOUT,904)NMXSTR                                                     
      END IF                                                                    
      CALL INTGFF(*108,IMATER((I-1)*NMXSTR+ICNTST),0,IL)                        
      CALL DOUBFF(*108,SPES((I-1)*NMXSTR+ICNTST),0,IL)                          
      CALL DOUBFF(*108,ORIEN((I-1)*NMXSTR+ICNTST),0,IL)                         
60    CONTINUE                                                                  
64    IF(NSTRAT*3.NE.NF)THEN                                                    
      CALL READFF(ISYM,0,NF,*80,*80)                                            
      NSTRAT=NF/3                                                               
      DO 66 L=1,NSTRAT                                                          
      ICNTST=ICNTST+1                                                           
      IF(ICNTST.GT.NMXSTR) THEN                                                 
      WRITE(IOUT,904)NMXSTR                                                     
      END IF                                                                    
      CALL INTGFF(*108,IMATER((I-1)*NMXSTR+ICNTST),0,IL)                        
      CALL DOUBFF(*108,SPES((I-1)*NMXSTR+ICNTST),0,IL)                          
      CALL DOUBFF(*108,ORIEN((I-1)*NMXSTR+ICNTST),0,IL)                         
66    CONTINUE                                                                  
      GOTO 64                                                                   
      END IF                                                                    
      IDPRO(I+MAXPRO)=ICNTST                                                    
50    CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*80,*80)                                            
      WRITE(IOUT,901) MAXPRO                                                    
      CALL JHALT                                                                
80    NPRO=I-1                                                                  
      IDPRO(2*MAXPRO+1)=0                                                       
      DO 99 I=2,NPRO                                                            
      IDPRO(2*MAXPRO+I)=IDPRO(2*MAXPRO+I-1)+IDPRO(MAXPRO+I-1)                   
99    CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY INCONS(*,*,IDMAT,ELAS,DENS,NMAT,IDPRO,IMATER,SPES,ORIEN,NPRO        
     1            ,ICON,VETT,LABEL,COOR,SCOMP,TRSMT,TMPMT,NODCNT,IER)           
C                                                                               
C     LA SUBROUTINE INCONS LEGGE I DATI RELATIVI ALLA CONNESSIONI DALLA         
C     SCHEDA DI DATI                                                            
C                                                                               
      IER=0                                                                     
      NEL=0                                                                     
      ITIP=8                                                                    
      IFLMR=1                                                                   
      NR6=6                                                                     
      NELS=0                                                                    
      NELSR=0                                                                   
      NWE=0                                                                     
      EMXSTR=0                                                                  
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 70   CALL IZERO(ICON,NCON)                                                     
      CALL READFF(ISYM,0,NF,*88,*89)                                            
      CALL INTGFF(*89,IDEL,0,IL)                                                
      CALL INTGFF(*89,IPROP,0,IL)                                               
      CALL INTGFF(*89,INT1,0,IL)                                                
      CALL INTGFF(*89,INT2,0,IL)                                                
      NCON=NF-4                                                                 
      IF(MAXNOD.LT.NCON) MAXNOD=NCON                                            
      DO 71 L=1,NCON                                                            
      CALL INTGFF(*89,ICON(L),0,IL)                                             
  71  CONTINUE                                                                  
      NEL=NEL+1                                                                 
      IF(NEL.LE.50)GOTO 90                                                      
      NEL=1                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
  90  CONTINUE                                                                  
      NELS=NELS+1                                                               
      IF(IFLMR.NE.0)NELSR=NELSR+1                                               
      WRITE(IOUT,720)NELS,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)               

c      if (ncon.eq.4) then                                                      
c      WRITE(nasmod,721)IDEL,IPROP,(ICON(L),L=1,NCON)                           
c721   format('CQUAD4  ',6i8)                                                   
c      elseif (ncon.eq.8) then                                                  
c      WRITE(nasmod,722)IDEL,IPROP,(ICON(L),L=1,6),idel                         
c      WRITE(nasmod,723)IDEL,(ICON(L),L=7,8)                                    
c722   format('CQUAD8  ',8i8,'+Q',i6)                                           
c723   format('+Q',i6,2i8)                                                      
c      endif                                                                    
c -----------------------------------------------------------                   
c     inizializzazione di icont                                                 
      if(nstratnas.gt.0) then
      do iok=1,nstratnas                                                           
      icont=icont+1                                                             
      iux=10000000                                                              
      nt1=iux*(iok-1)                                                           
      nt2=iux*iok                                                               
      icont=icont+1                                                             
      if (ncon.eq.4) then                                                       
                                                                                
      WRITE(nasmod,921)IDEL+nt1,IPROP,(ICON(L)+nt1,L=1,4),                      
     + (ICON(L)+nt2,L=1,2),icont,icont,                                         
     + (ICON(L)+nt2,L=3,4)                                                      
                                                                                
921   format('CHEXA   ',8i8,'+CH',i5/'+CH',i5,2i8)                              
      elseif (ncon.eq.8) then                                                   
                                                                                
      WRITE(nasmod,721)IDEL+nt1,IPROP,(ICON(L)+nt1,L=1,4),                      
     + (ICON(L)+nt2,L=1,2),                                                     
     + icont                                                                    
      WRITE(nasmod,722)icont,(ICON(L)+nt2,L=3,4),(ICON(L)+nt1,L=5,8),           
     +  0,0,icont+1                                                             
      WRITE(nasmod,723)icont+1,0,0,(ICON(L)+nt2,L=5,8)                          
                                                                                
      icont=icont+1                                                             
721   format('CHEXA   ',8i8,'+CH',i5)                                           
722   format('+CH',i5,8i8,'+CH',i5)                                             
723   format('+CH',i5,6i8)                                                      
      endif                                                                     
      enddo               
      else 
      if (ncon.eq.4) then
      WRITE(nasmod,821)IDEL,IPROP,(ICON(L),L=1,NCON)
821   format('CQUAD4  ',6i8)
      elseif (ncon.eq.8) then
      WRITE(nasmod,822)IDEL,IPROP,(ICON(L),L=1,6),idel
      WRITE(nasmod,823)IDEL,(ICON(L),L=7,8)
822   format('CQUAD8  ',8i8,'+Q',i6)
823   format('+Q',i6,2i8)
      endif
      
      endif                                                      
c -----------------------------------------------------------                   
      do i=1,ncon
      idnt(i)=icon(i)
      enddo 
      IER=0                                                                     
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      dx1=vett(3)-vett(1)
      dy1=vett(4)-vett(2)
      dx2=vett(7)-vett(1)
      dy2=vett(8)-vett(2)
      prv=dx1*dy2-dx2*dy1 
c      print*,idel,dx1,dy1,dx2,dy2
c      print*,prv
      if(prv.lt.0) then
      do i=1,ncon
      icon(i)=idnt(i)
      enddo 

      call iswap(icon(1),icon(2))
      call iswap(icon(3),icon(4))
      if(ncon.eq.8) then
        call iswap(icon(6),icon(8))
      endif
      if(ncon.eq.12) then
        call iswap(icon(5),icon(6))
        call iswap(icon(9),icon(10))
        call iswap(icon(11),icon(8))
        call iswap(icon(7),icon(12))
      endif
      write(iout,*) 'Il verso di numerazione dell''elemento e'' stato',
     * ' invertito',ncon
      WRITE(IOUT,720)NELP,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)               
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      endif      

      IF(IER.NE.0)THEN                                                          
        IERG=1                                                                  
        WRITE(IOUT,905)IDEL                                                     
        GOTO 70                                                                 
      END IF                                                                    
C      WRITE(IOUT,400)                                                          
      CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,NCON)                                             
      CALL JWRITE(ISLELM,VETT,NCON*4)                                           
      IPP=IPOSL(IPROP,IDPRO,NPRO)                                               
      IF(IPP.EQ.0) THEN                                                         
        WRITE(IOUT,902)IPROP,IDEL                                               
        IER=1                                                                   
        GOTO 70                                                                 
      END IF                                                                    
      NSTRAT=IDPRO(IPP+NPRO)                                                    
C     IF(NSTRAT.GT.EMXSTR)THEN                                                  
C     EMXSTR=NSTRAT                                                             
C     END IF                                                                    
      CALL JWRITE(ISLELM,NSTRAT,1)                                              
      DO 777 IST=1,NSTRAT                                                       
      IDM=IMATER(IDPRO(IPP+2*NPRO)+IST)                                         
      IF(IDM.LT.0)THEN                                                          
        CALL READFL(IDM,IER,ITIP,TMPMT,ifl,DEN)                                 
        CALL INVERT(TMPMT,6,SCOMP,6,6,PERM)                                     
        DO 1010 J=1,6                                                           
        DO 1010 K=1,6                                                           
          TMPMT(J,K)=SCOMP(J,K)                                                 
1010    CONTINUE                                                                
      ELSE                                                                      
        IPM=IPOSL(IDM,IDMAT,NMAT)                                               
        IF(IPM.EQ.0) THEN                                                       
          WRITE(IOUT,903)IDEL,IDM,IPROP                                         
          IER=1                                                                 
          GOTO 70                                                               
        END IF                                                                  
      END IF                                                                    
      DEN=DENS(IPM)                                                             
      THICK=SPES(IDPRO(2*NPRO+IPP)+IST)                                         
      ANGLAM=ORIEN(IDPRO(2*NPRO+IPP)+IST)                                       
      CALL DMOVE(ELAS(1,(IPM-1)*6+1),TMPMT,36)                                  
C                                                                               
      CALL JWRITE(ISLELM,INT1,8)                                                
      DO 888 II=1,6                                                             
      DO 888 IL=1,6                                                             
      TRSMT(II,IL)=TMPMT(II,IVP(IL))                                            
 888  CONTINUE                                                                  
      DO 778 II=1,6                                                             
      DO 778 IL=1,6                                                             
      TMPMT(IL,II)=TRSMT(IVP(IL),II)                                            
 778  CONTINUE                                                                  
                                                                                
c       WRITE(IOUT,*)'DG PRIMA          '                                       
c       WRITE(IOUT,1111)((tmpmt(II,KK),KK=1,6),II=1,6)                          
c 1111 FORMAT(/6(1X,6D12.5/))                                                   
                                                                                
      IF(ORIEN(IST+IDPRO(2*NPRO+IPP)).EQ.0.D0)GOTO 75                           
      CALL TRASFM(TMPMT,TRSMT,SCOMP,6,ORIEN(IST+IDPRO(IPP+2*NPRO)))             
c       WRITE(IOUT,*)'DG dopo           '                                       
c       WRITE(IOUT,1111)((tmpmt(II,KK),KK=1,6),II=1,6)                          
       DO 8888 II=1,6                                                           
      DO 8888 IL=1,6                                                            
      ZMMM(II,IL)=TMPMT(II,IL)                                                  
8888  CONTINUE                                                                  
 75   CONTINUE                                                                  
      CALL JWRITE(ISLELM,TMPMT,72)                                              
 777   CONTINUE                                                                 
      NWE=NWE+5*NCON+5+NSTRAT*(72+8)                                            
      GOTO 70                                                                   
 88   RETURN 1                                                                  
 89   RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY WRMATS(IDMAT,ELAS,DENS,NMAT)                                        
C                                                                               
      NR=0                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      WRITE(IOUT,3001)                                                          
      DO 2000 IMAT=1,NMAT                                                       
      NR=NR+6                                                                   
      IF(NR.LE.54)GOTO 2010                                                     
      NR=6                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3001)                                                          
 2010 CONTINUE                                                                  
      IB=(IMAT-1)*6                                                             
      WRITE(IOUT,3010)IMAT,IDMAT(IMAT),(ELAS(1,K+IB),K=1,6),DENS(IMAT),         
     1               ((ELAS(L,K+IB),K=1,6),L=2,6)                               
 2000 CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY WRPROS(IDPRO,IMATER,SPES,ORIEN,NPRO)                                
C                                                                               
      NR=0                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
      WRITE(IOUT,3101)                                                          
      DO 2100 IPRO=1,NPRO                                                       
      NR=NR+1                                                                   
      IF(NR.GT.50)THEN                                                          
      NR=1                                                                      
      WRITE(IOUT,3101)                                                          
      END IF                                                                    
      WRITE(IOUT,3110)IPRO,IDPRO(IPRO),1,                                       
     1    IMATER(IDPRO(2*NPRO+IPRO)+1),                                         
     2    SPES(1+IDPRO(NPRO*2+IPRO)),ORIEN(1+IDPRO(NPRO*2+IPRO))                
      DO 2101 L=2,IDPRO(IPRO+NPRO)                                              
      WRITE(IOUT,3111)L,IMATER(IDPRO(2*NPRO+IPRO)+L),                           
     1    SPES(L+IDPRO(NPRO*2+IPRO)),ORIEN(L+IDPRO(NPRO*2+IPRO))                
 2101 CONTINUE                                                                  
 2100 CONTINUE                                                                  
      RETURN                                                                    
C400  FORMAT(/35X,'***   PARAMETRI RELATIVI ALLE CONNESSIONI   ***'/)           
C401  FORMAT(/35X,'TIPO ELEM.   ID.ELEM.   N.RO CONN.   ',                      
C    *       'FLAG RIGIDITA'''/)                                                
C410  FORMAT(31X,I8,5X,I8,4X,I8,6X,I8)                                          
C411  FORMAT(//48X,'***   CONNESSIONI   ***')                                   
C420  FORMAT(/32X,8I8)                                                          
C421  FORMAT(//43X,'***   COORDINATE CONNESSIONI   ***'/)                       
C422  FORMAT(47X,'NODO       X             Y')                                  
C430  FORMAT(/41X,I8,5X,F8.4,6X,F8.4)                                           
 900  FORMAT(' ***   AVVISO (INMATS) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI MATERIALI E'' =',I8,'   ***'/)                                    
 901  FORMAT(' ***   AVVISO (INPROS) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI PROPRIETA'' E'' =',I8,'   ***'/)                                  
 902  FORMAT(' ***   AVVISO (INCONS) : NON RECUPERATA LA PROPRIETA'''           
     1   ,' N.RO',I8,' DELL''ELEMENTO N.RO',I8,'   ***'/)                       
 903  FORMAT(' ***   AVVISO (INCONS) : PER L''ELEMENTO N.RO',I8,                
     1   '  NON RECUPERATO IL MATERIALE N.RO',I8,'  INDICATO ',                 
     2   'DALLA PROPRIETA'' N.RO',I8,'   ***'/)                                 
 904  FORMAT(' ***   AVVISO (INPROS) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI LAMINE PER STRATIFICATO E'' =',I8,'   ***'/)                      
 905  FORMAT(' ***   AVVISO (INCONS) : NON RECUPERATI NODI'                     
     1 ,' DELL''ELEMENTO N.RO',I8,'   ***')                                     
C                                                                               
 700  FORMAT(/40X,'***   ELEMENTI STRATIFICATI   ***'/)                         
 710  FORMAT(//12X,'   N.RO  ID.ELEM  ID.PROP   N.PUNTI INTEG. ',               
     1  4X,'CONNESSIONI'/)                                                      
 720  FORMAT(10X,3I8,6X,2I4,8X,12I8)                                            
 3000 FORMAT(/40X,'***   MATERIALI ELEMENTI STRATIFICATI  ***'/)                
 3001 FORMAT(//9X,'   N.RO  ID.MAT  ',T30,'MATRICE ELASTICA',T114,              
     1    'DENSITA'''//)                                                        
 3010 FORMAT(6X,2I8,T30,6E12.5,T110,E12.5,5(/T30,6E12.5)/)                      
 3100 FORMAT(/40X,'***   PROPRIETA'' ELEMENTI STRATIFICATI   ***'/)             
 3101 FORMAT(//19X,'   N.RO    ID.PROP     STRATO   ID.MAT',                    
     1      '       SPESSORE       ORIENT. FIBRE'/)                             
 3110 FORMAT(/15X,4I10,7X,D12.5,4X,D12.5)                                       
 3111 FORMAT(35X,2I10,7X,D12.5,4X,D12.5)                                        
      END                                                                       
C@ELT,I ANBA*ANBA.INPUTS                                                        
C*************************          INPUTS          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPUTS(ISCHE,IERG,JLABEL,JCOOR,JICON,JVETT,JNDCNT)             
      REAL*8 DV(1)                                                              
      LOGICAL DUMP,incore                                                       
      CHARACTER ISCHE*80                                                        
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON/ PARSTR/ NMXSTR                                                    
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1           NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3              
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP,ISMTER,ISTNOT, ISLSLT                              
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,              
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
C                                                                               
c     call jprmem                                                               
c     call jprtab                                                               
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).NE.'MATE') GOTO 800                                          
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXMAT=(MEMDIS-84-7*15-JIFITT)/75                                         
      call jalmax(5,memdisp)                                                    
      maxmat=(memdisp-84)/75                                                    
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,MAXMAT     ,JIDMAT)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,MAXMAT*36  ,JELAS )                 
      CALL JALLOC(*2000,'DENS    ','D.P.',1,MAXMAT     ,JDENS )                 
      CALL JALLOC(*2000,'TMPMT   ','D.P.',1,36         ,JTMPMT)                 
      CALL JALLOC(*2000,'PERM    ','D.P.',1,6          ,JPERM )                 
C                                                                               
      CALL INMATS(*500,*501,MAXMAT,IV(JIDMAT),DV(JELAS),DV(JDENS),NMAT,         
     1          DV(JPERM),DV(JTMPMT),IER)                                       
      IF(IER.NE.0) IERG=1                                                       
      JFITT=JELAS                                                               
      JFIT1=JDENS                                                               
      CALL JFREE(*2000,'PERM    ')                                              
      CALL JFREE(*2000,'TMPMT   ')                                              
      CALL JFREE(*2000,'DENS    ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
      CALL JALLOC(*2000,'IDMAT   ','INT.',1,NMAT       ,JIDMAT)                 
      CALL JALLOC(*2000,'ELAS    ','D.P.',1,NMAT*36    ,JELAS )                 
      CALL JALLOC(*2000,'DENS    ','D.P.',1,NMAT       ,JDENS )                 
C                                                                               
      CALL DMOVE(DV(JFITT),DV(JELAS),NMAT*36)                                   
      CALL DMOVE(DV(JFIT1),DV(JDENS),NMAT)                                      
      CALL WRMATS(IV(JIDMAT),DV(JELAS),DV(JDENS),NMAT)                          
      NR6=6                                                                     
      READ(ISYM,100)ISCHE                                                       
 800  IF(ISCHE(:4).EQ.'PROP') GOTO 810                                          
      WRITE(IOUT,751)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 810  CONTINUE                                                                  
c     CALL JALLOC(*2000,'IFITT   ',1,1,1      ,JIFITT)                          
c     CALL JFREE(*2000,'IFITT   ')                                              
c     MAXPRO=(MEMDIS-10-16*7-JIFITT)/(NMXSTR*5+3)                               
      call jalmax(4,memdisp)                                                    
      maxpro=(memdisp-5)/(nmxstr*5+3)                                           
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,MAXPRO*3        ,JIDPRO)            
      CALL JALLOC(*2000,'IMATER  ','INT.',1,MAXPRO*NMXSTR   ,JMATER)            
      CALL JALLOC(*2000,'SPES    ','D.P.',1,MAXPRO*NMXSTR   ,JSPES )            
      CALL JALLOC(*2000,'ORIEN   ','D.P.',1,MAXPRO*NMXSTR   ,JORIEN)            
C                                                                               
      CALL INPROS(*500,*501,MAXPRO,IV(JIDPRO),IV(JMATER),DV(JSPES),             
     1                             DV(JORIEN),NPRO)                             
      CALL JFREE(*2000,'IDPRO   ')                                              
      JFITT=JMATER                                                              
      JFIT1=JSPES                                                               
      JFIT2=JORIEN                                                              
      CALL JFREE(*2000,'IMATER  ')                                              
      CALL JFREE(*2000,'SPES    ')                                              
      CALL JFREE(*2000,'ORIEN   ')                                              
      CALL JALLOC(*2000,'IDPRO   ','INT.',1,NPRO*3      ,JIDPRO)                
      CALL IMOVE(IV(JIDPRO+MAXPRO),IV(JIDPRO+NPRO),NPRO)                        
      CALL IMOVE(IV(JIDPRO+2*MAXPRO),IV(JIDPRO+2*NPRO),NPRO)                    
      NRSTRA=IV(JIDPRO+3*NPRO-1)+IV(JIDPRO+2*NPRO-1)                            
      CALL JALLOC(*2000,'IMATER  ','INT.',1,NRSTRA  ,JMATER)                    
      CALL JALLOC(*2000,'SPES    ','D.P.',1,NRSTRA*3,JSPES )                    
      CALL JALLOC(*2000,'ORIEN   ','D.P.',1,NRSTRA*3,JORIEN)                    
      DO 111 I=1,NPRO                                                           
      CALL IMOVE(IV(JFITT+(I-1)*NMXSTR),                                        
     1           IV(JMATER+IV(JIDPRO+2*NPRO+I-1)),IV(JIDPRO+NPRO+I-1))          
      CALL DMOVE(DV(JFIT1+(I-1)*NMXSTR),                                        
     1           DV(JSPES+IV(JIDPRO+2*NPRO+I-1)),IV(JIDPRO+NPRO+I-1))           
      CALL DMOVE(DV(JFIT2+(I-1)*NMXSTR),                                        
     1           DV(JORIEN+IV(JIDPRO+2*NPRO+I-1)),IV(JIDPRO+NPRO+I-1))          
111   CONTINUE                                                                  
C                                                                               
      CALL WRPROS(IV(JIDPRO),IV(JMATER),DV(JSPES),DV(JORIEN),NPRO)              
      READ(ISYM,100)ISCHE                                                       
      IF(ISCHE(:4).EQ.'CONN')GOTO 820                                           
      WRITE(IOUT,752)                                                           
      CALL JHALT                                                                
      STOP                                                                      
 820  CONTINUE                                                                  
c     call jprmem                                                               
c     call jprtab                                                               
      CALL JALLOC(*2000,'SCOMP   ','D.P.',1,36         ,JSCOMP)                 
      CALL JALLOC(*2000,'TRSMT   ','D.P.',1,36         ,JTRSMT)                 
      CALL JALLOC(*2000,'TMPMT   ','D.P.',1,36         ,JTMPMT)                 
C                                                                               
      IER=0                                                                     
      CALL INCONS(*500,*501,IV(JIDMAT),DV(JELAS),DV(JDENS),NMAT,                
     1          IV(JIDPRO),IV(JMATER),DV(JSPES),DV(JORIEN),NPRO,                
     2          IV(JICON),DV(JVETT),IV(JLABEL),DV(JCOOR),                       
     3         DV(JSCOMP),DV(JTRSMT),DV(JTMPMT),IV(JNDCNT),IER)                 
      IF(IER.NE.0) IERG=1                                                       
      CALL JFREE(*2000,'TMPMT   ')                                              
      CALL JFREE(*2000,'TRSMT   ')                                              
      CALL JFREE(*2000,'SCOMP   ')                                              
      CALL JFREE(*2000,'SPES    ')                                              
      CALL JFREE(*2000,'IMATER  ')                                              
      CALL JFREE(*2000,'IDPRO   ')                                              
      CALL JFREE(*2000,'DENS    ')                                              
      CALL JFREE(*2000,'ELAS    ')                                              
      CALL JFREE(*2000,'IDMAT   ')                                              
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
 500  WRITE(IOUT,510)                                                           
 510  FORMAT(' ***   AVVISO (INPUTS): FINE DEL FILE DI DATI   ***')             
      CALL JHALT                                                                
      STOP                                                                      
 501  WRITE(IOUT,511)                                                           
 511  FORMAT(' ***   AVVISO (INPUTS): RECORD TROPPO CORTO   ***')               
      CALL JHALT                                                                
      STOP                                                                      
C                                                                               
 100  FORMAT(A80)                                                               
 751  FORMAT(' ***   AVVISO (INPUTS): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO PROPRIETA'' ELEMENTI STRATIFICATI',          
     2 '   ***')                                                                
 752  FORMAT(' ***   AVVISO (INPUTS): NON PRESENTE LA STRINGA',                 
     1 ' DI IDENTIFICAZIONE GRUPPO CONNESSIONI ELEMENTI STRATIFICATI',          
     2 '   ***')                                                                
      END                                                                       

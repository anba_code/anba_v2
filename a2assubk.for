C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
                                                                                
C@ELT,I ANBA*ANBA.ASSUBK                                                        
C*************************          ASSUBK          ********************        
      SUBROUTINE ASSUBK(UK,UD,UM,IPIAZ,ELMAT,NA,NRELMT,VETT,COEF,               
     *                  ISLVPZ,ISLMBK)                                          
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      INTEGER SPCCOD                                                            
      LOGICAL DUMP,incore                                                       
      DIMENSION IPIAZ(1),ELMAT(NRELMT,1),NA(1),UK(1),UD(1),UM(1),VETT(1)        
     *          ,COEF(1)                                                        
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                 
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ELGEN/  ITIP,NTERM,NT1,IFLMR                                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO                                     
      DATA MPCCOD/6/,SPCCOD/7/                                                  
C                                                                               
          CALL JPOSAB(ISLVPZ,1)                                                 
C                                                                               
      DO 300 K=1,NELEM                                                          
C                                                                               
      CALL JREAD(ISLVPZ,ITIP,3)                                                 
      PRINT*,' ELEMENTO $',K,ITIP,NTERM,NT1                                     
      IF(ITIP.EQ.MPCCOD) GOTO 500                                               
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      NTERM=NTERM-1                                                             
C                                                                               
      CALL JPOSRL(ISLVPZ,NTERM*NT1*2)                                           
C                                                                               
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      DO 340 I=1,NTERM-NPSI                                                     
       DO 340 L=1,NPSI                                                          
        CALL JREAD(ISLMBK,VETT,(NTERM-NPSI)*2)                                  
       DO 340 M=1,NTERM-NPSI                                                    
        ELMAT(M,I)=ELMAT(M,I)+VETT(M)*COEF(L)                                   
 340  CONTINUE                                                                  
C:    CALL DWRITE(' MAT K ',ELMAT,NRELMT,NTERM-NPSI,NTERM-NPSI,1)               
      CALL FEMADS(UK,NA,ELMAT,IPIAZ,NTERM-NPSI,NRELMT)                          
C                                                                               
C                                                                               
      DO 400 IL=1,NPSI                                                          
      IF(IPGCON(IL).EQ.0) GOTO 400                                              
      IN=NEQV-NPSI+IL                                                           
      IN=NA(IN)                                                                 
      UK(IN)=UK(IN)+CONST                                                       
 400  CONTINUE                                                                  
C                                                                               
      IF(ITIP.EQ.SPCCOD) GOTO 300                                               
C                                                                               
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      DO 341 I=1,NTERM-NPSI                                                     
       DO 341 L=1,NPSI                                                          
        CALL JREAD(ISLMBK,VETT,(NTERM-NPSI)*2)                                  
       DO 341 M=1,NTERM-NPSI                                                    
        ELMAT(M,I)=ELMAT(M,I)+VETT(M)*COEF(L)                                   
 341  CONTINUE                                                                  
C:    CALL DWRITE(' MAT D ',ELMAT,NRELMT,NTERM-NPSI,NTERM-NPSI,1)               
      CALL FEMADA(UD,NA,ELMAT,IPIAZ,NTERM-NPSI,NRELMT)                          
C                                                                               
C                                                                               
      IF(ITIP.EQ.SPCCOD) GOTO 300                                               
C                                                                               
C                                                                               
      CALL DZERO(ELMAT,NRELMT*NRELMT)                                           
      DO 342 I=1,NTERM-NPSI                                                     
       DO 342 L=1,NPSI                                                          
        CALL JREAD(ISLMBK,VETT,(NTERM-NPSI)*2)                                  
       DO 342 M=1,NTERM-NPSI                                                    
        ELMAT(M,I)=ELMAT(M,I)+VETT(M)*COEF(L)                                   
 342  CONTINUE                                                                  
C:    CALL DWRITE(' MAT M ',ELMAT,NRELMT,NTERM-NPSI,NTERM-NPSI,1)               
      CALL FEMADS(UM,NA,ELMAT,IPIAZ,NTERM-NPSI,NRELMT)                          
C                                                                               
 300  CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
 500  WRITE(IOUT,700)                                                           
 700  FORMAT(' ***   AVVISO (ASSUBK): NON PREVISTI MPC NEL CALCOLO',            
     *      ' DEGLI AUTOVALORI DI INSTABILITA'' ***')                           
      CALL JHALT                                                                
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.VTPZBK                                                        
C*************************          VTPZBK          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE VTPZBK(ICON,ELMBK,NREBK,                                       
     *                  ISLELM,ISLELS,ISLSFC,ISLVPZ,ISLMBK)                     
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      INTEGER SPCCOD                                                            
      DIMENSION ELMBK(NREBK,NREBK,6,3),ICON(1)                                  
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      DATA MPCCOD/6/,SPCCOD/7/                                                  
C                                                                               
C                                                                               
      DO 10 I=1,NELEM                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      IF(MPCCOD.EQ.ITIP) GOTO 15                                                
      IF(ITIP.EQ.SPCCOD) GOTO 15                                                
C                                                                               
      CALL MATRBK(ITIP,IDEL,                                                    
     *            ELMBK(1,1,1,1),ELMBK(1,1,1,2),ELMBK(1,1,1,3),NREBK,           
     *            ISLELM,ISLELS,ISLSFC)                                         
C=    CALL JREAD(ISLVPZ,ITIP,1)                                                 
C=    CALL JREAD(ISLVPZ,NTERM,1)                                                
C=    CALL JREAD(ISLVPZ,NT1,1)                                                  
C=    CALL JPOSRL(ISLVPZ,NTERM)                                                 
      NT=NCON*NCS+NPSI                                                          
C=    CALL JPOSRL(ISLVPZ,NT1*2*NTERM1)                                          
      DO 36 ISSS=1,3                                                            
      DO 36 IS=1,NT-NPSI                                                        
      DO 36 ISS=1,NPSI,1                                                        
      CALL JWRITE(ISLMBK,ELMBK(1,IS,ISS,ISSS),(NT-NPSI)*2)                      
36    CONTINUE                                                                  
      GOTO 10                                                                   
C                                                                               
15    CONTINUE                                                                  
      CALL JPOSRL(ISLELM,NCON*4+6)                                              
      DO 100 IO=1,18                                                            
        CALL JWRITE(ISLMBK,0.D0,2)                                              
  100 CONTINUE                                                                  
C=    CALL JREAD(ISLVPZ,ITIP,1)                                                 
C=    CALL JREAD(ISLVPZ,NTERM,1)                                                
C=    CALL JREAD(ISLVPZ,NT1,1)                                                  
C=    CALL JPOSRL(ISLVPZ,NTERM)                                                 
C=    CALL JPOSRL(ISLVPZ,(NTERM-1)*2)                                           
C                                                                               
10    CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.MATRBK                                                        
C*************************          MATRBK          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATRBK(ITIP,IDEL,                                              
     *                  ELBKE,ELBKH,ELBKM,NRELMT,                               
     *             ISLELM,ISLELS,ISLSFC)                                        
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      DIMENSION ELBKE(NRELMT,NRELMT,6),ELBKH(NRELMT,NRELMT,6)                   
     *               ,ELBKM(NRELMT,NRELMT,6)                                    
       COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                       
C     SUBROUTINE PER LA CHIAMATA DELLE SUBROUTINE CHE                           
C     PREPARANO LE MATRICI DI RIGIDEZZA DEGLI ELEMENTI                          
C                                                                               
C     CORRENTE 100                                                              
C     GIUNZIONE 200                                                             
C     PANNELLI A 2 3 O 4NODI 300                                                
C     LAMINE 400                                                                
C     ELEMENTI PIANI 500                                                        
C                                                                               
      IF (ITIP.NE.5)GOTO 1000                                                   
C     IF (ITIP.GT.7)GOTO 1000                                                   
C     GOTO(100,200,300,400,500),ITIP                                            
C100   CALL MATCOR(ELMAT,NRELMT,VMASS)                                          
C      RETURN                                                                   
C200   CALL MATGIU(ELMAT,NRELMT,VMASS)                                          
C      RETURN                                                                   
C300   CALL MATPAN(ELMAT,NRELMT,VMASS)                                          
C      RETURN                                                                   
C400   CALL MATLAM(ELMAT,NRELMT,VMASS)                                          
C      RETURN                                                                   
500   CALL MBKELP(ELBKE,ELBKH,ELBKM,NRELMT,                                     
     *            ISLELM,ISLELS,ISLSFC)                                         
      RETURN                                                                    
C                                                                               
C                                                                               
1000  WRITE(IOUT,2000)IDEL,ITIP                                                 
2000  FORMAT(' *** AVVISO (MATRBK): NON RICONOSCIUTO L''ELEMENTO N.RO',         
     1  I8,'  DI TIPO',I8,'   ***')                                             
      CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C@ELT,I ANBA*ANBA.MBKELP                                                        
C*************************          MBKELP          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MBKELP(ELBKE,ELBKH,ELBKM,NRELMT,                               
     *                  ISLELM,ISLELS,ISLSFC)                                   
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION ELBKE(NRELMT,NRELMT,6),ELBKH(NRELMT,NRELMT,6)                   
      DIMENSION ELBKM(NRELMT,NRELMT,6)                                          
      REAL*4 SIGMAG(6,6)                                                        
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                 
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELPI1/  LREC,NPINT,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),             
     1                T2(3,3),D(6,6),W                                          
C                                                                               
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,NCON*5+79)                                             
      CALL JREAD(ISLELS,LREC,2)                                                 
      CALL DZERO(ELBKE,NRELMT*NRELMT*6)                                         
      CALL DZERO(ELBKH,NRELMT*NRELMT*6)                                         
      CALL DZERO(ELBKM,NRELMT*NRELMT*6)                                         
      DO 20 NPT=1,NPINT,1                                                       
      CALL JREAD(ISLELS,ENNE,LREC)                                              
      CALL JREAD(ISLSFC,SIGMAG,6*NPSI*1)                                        
      CALL JPOSRL(ISLSFC,6*NPSI*1)                                              
      DO 30 K=1,NCON,1                                                          
      JK=(K-1)*NCS+1                                                            
      DO 40 I=1,NCON,1                                                          
      JI=(I-1)*NCS+1                                                            
      DO 50 L=1,NPSI,1                                                          
      CALL CAPBK(W,SIGMAG(4,L),SIGMAG(1,L),ENNE(1,I),ENNE(1,K),                 
     *           ELBKE(JI,JK,L),ELBKH(JI,JK,L),ELBKM(JI,JK,L),NRELMT)           
 50   CONTINUE                                                                  
 40   CONTINUE                                                                  
 30   CONTINUE                                                                  
 20   CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.CAPNK                                                         
**************************          CAPBK           ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CAPBK(W,F,P,ENR,ENC,RIGE,RIGH,RIGM,NRIG)                       
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      REAL*4    F(3),P(3)                                                       
      DIMENSION ENR(3),ENC(3)                                                   
      DIMENSION RIGE(NRIG,3),RIGH(NRIG,3),RIGM(NRIG,3)                          
C                                                                               
      R11=W*(F(1)*ENR(2)*ENC(2)+F(2)*ENR(3)*ENC(3)                              
     *       +F(3)*(ENR(2)*ENC(3)+ENR(3)*ENC(2)))                               
      R12=W*(P(1)*ENR(2)+P(2)*ENR(3))*ENC(1)                                    
      R21=W*ENR(1)*(P(1)*ENC(2)+P(2)*ENC(3))                                    
      R22=W*P(3)*ENR(1)*ENC(1)                                                  
                                                                                
      DO 10 I=1,3                                                               
      RIGE(I,I)=RIGE(I,I)+R11                                                   
      RIGH(I,I)=RIGH(I,I)+R12-R21                                               
      RIGM(I,I)=RIGM(I,I)+R22                                                   
   10 CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.TTTRRR                                                        
C*************************     TTTRRR     *****************************         
      SUBROUTINE TTTRRR(X,NODI,TRA)                                             
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION X(2,NODI),TRA(3,NODI,3)                                         
      PGR=3.141592654                                                           
      B=48.                                                                     
      DO 10 K=1,3                                                               
      DO 10 J=1,NODI                                                            
      DO 10 I=1,3                                                               
  10  TRA(I,J,K)=0.                                                             
      DO 100 J=1,NODI                                                           
      ARG=PGR*X(1,J)/B                                                          
      TRA(1,J,1)=PGR*X(2,J)*DSIN(ARG)/B                                         
      TRA(2,J,1)=DCOS(ARG)                                                      
      TRA(3,J,2)=PGR*X(2,J)*DCOS(ARG)/B                                         
      TRA(3,J,3)=1.                                                             
 100  CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.CONDEN                                                        
C*************************      CONDEN       **************************         
      SUBROUTINE CONDEN                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION A(1),NA(1),TRA(120,3),TEM(120,3),CON(3,3)                       
      ENTRY CONDES(A,NA,TRA,TEM,CON)                                            
      CALL DZERO(TEM,120*3)                                                     
      DO 100 K=1,3                                                              
      CALL MSSKV(TEM(1,K),A,TRA(1,K),NA,120)                                    
 100  CONTINUE                                                                  
      DO 110 J=1,3                                                              
      DO 110 I=1,3                                                              
      CON(I,J)=0.                                                               
      DO 110 K=1,120                                                            
      CON(I,J)=CON(I,J)+TRA(K,I)*TEM(K,J)                                       
 110  CONTINUE                                                                  
      RETURN                                                                    
      ENTRY CONDEA(A,NA,TRA,TEM,CON)                                            
      CALL DZERO(TEM,120*3)                                                     
      DO 200 K=1,3                                                              
      CALL MASKV(TEM(1,K),A,TRA(1,K),NA,120)                                    
 200  CONTINUE                                                                  
      DO 210 J=1,3                                                              
      DO 210 I=1,3                                                              
      CON(I,J)=0.                                                               
      DO 210 K=1,120                                                            
      CON(I,J)=CON(I,J)+TRA(K,I)*TEM(K,J)                                       
 210  CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       

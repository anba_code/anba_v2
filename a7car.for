C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MODCAR                                                        
C*************************          MODCAR          ********************        
C     COMPILER (ARGCHK=OFF)                                                     
      SUBROUTINE MODCAR                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      COMMON /TRAVE3/ rl,rve(6),neltra                                          
      CALL JCRERS                                                               
C                                                                               
C     INIZIALIZZAZIONE DELLE VARIABILI                                          
C                                                                               
      NSOL=NPSI                                                                 
      NRZT=MAXDEL                                                               
      NRTNM1=MAXDEL                                                             
      NDVX=NEQV+NEQV-NPSI                                                       
      NDVXK=(NEQV-NPSI)*2                                                       
C                                                                               
C         ALLOCAZIONE DELLE VARIABILI E LETTURA DELLE SOLUZIONI                 
C                                                                               
      CALL JALLOC(*2000,'FLEX    ','D.P.',1,NSOL*NSOL    ,JFLEX )               
      CALL JALLOC(*2000,'WJX     ','D.P.',1,6*6        ,JWJX  )                 
      CALL JALLOC(*2000,'VX      ','D.P.',1,NDVX*NSOL  , JVX   )                
      CALL JOPEN('*SL1    ',ISLSL1,1)                                           
      CALL JOPEN('*SL2    ',ISLSL2,1)                                           
C                                                                               
      CALL RCPSOL(DV(JVX),NDVX,NSOL)                                            
C                                                                               
      CALL JCLOSE(ISLSL2)                                                       
      CALL JCLOSE(ISLSL1)                                                       
C                                                                               
      CALL JALLOC(*2000,'TNM1    ','D.P.',1,MAXDEL*MAXDEL,JTNM1 )               
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1     ,JIPIAZ)               
      CALL JALLOC(*2000,'ZT2     ','D.P.',1,NRZT*NSOL  ,JZT2  )                 
      CALL JALLOC(*2000,'ZT      ','D.P.',1,NRZT*NSOL  ,JZT   )                 
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
C     CALL JOPEN('*FCT    ',ISLFCT,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
C                                                                               
C     COSTRUZIONE DELLA MATRICE DI FLESSIBILITA'                                
C                                                                               
C      WRITE(IOUT,*)' CHIAMA COSFLX'                                            
C      CALL JPRFIL                                                              
      CALL COSFLX(IV(JIPIAZ),DV(JZT),NRZT,DV(JFLEX),NSOL,DV(JTNM1),             
     1            NRTNM1,DV(JVX),NDVX,DV(JZT2),DV(JWJX))                        
C      WRITE(IOUT,*)' ESCO   COSFLX'                                            
      CALL JCLOSE(ISLMTF)                                                       
C     CALL JCLOSE(ISLFCT)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JFREE(*2000,'ZT      ')                                              
      CALL JFREE(*2000,'ZT2     ')                                              
      CALL JFREE(*2000,'IPIAZ   ')                                              
      CALL JFREE(*2000,'TNM1    ')                                              
C                                                                               
      CALL JALLOC(*2000,'RIGID   ','D.P.',1,NSOL*NSOL    ,JRIGID)               
      CALL JALLOC(*2000,'VXK     ','D.P.',1,NDVXK*9    ,JVXK  )                 
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI    ,JLABEL)                   
      CALL JALLOC(*2000,'XYZ     ','D.P.',1,NNODI*2  ,JXYZ  )                   
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JREAD(ISLNOD,DV(JXYZ),NNODI*4)                                       
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JOPEN('*SNO    ',ISLSNO,1)                                           
      CALL JREAD(ISLSNO,NNSEL,1)                                                
      IF(NNSEL.EQ.0)GOTO 50                                                     
      CALL JALLOC(*2000,'NODSEL  ','INT.',1,NNSEL   ,JNODSL)                    
      CALL JREAD(ISLSNO,IV(JNODSL),NNSEL)                                       
 50   CALL JCLOSE(ISLSNO)                                                       
      CALL SOLMDF(IV(JLABEL),IV(JNUME),IV(JNUINT),                              
     %   DV(JRIGID),DV(JFLEX),DV(JWJX),                                         
     %   DV(JVX),NDVX,DV(JVXK),NDVXK,DV(JXYZ),IV(JNODSL),NNSEL)                 
      CALL JFREE(*2000,'NUINT   ')                                              
      CALL JFREE(*2000,'NUME    ')                                              
      CALL JFREE(*2000,'XYZ     ')                                              
      CALL JFREE(*2000,'LABEL   ')                                              
      CALL JOPEN('*SL2    ',ISLSL2,1)                                           
      DO 60 I=1,NSOL                                                            
      IX=NDVX*(I-1)                                                             
      CALL JWRITE(ISLSL2,DV(JVX+IX),NEQV*2)                                     
 60   CONTINUE                                                                  
      CALL JCLOSE(ISLSL2)                                                       
      CALL JOPEN('*SL1    ',ISLSL1,1)                                           
      IPUN=JVX+NEQV                                                             
      DO 70 I=1,NSOL                                                            
      CALL JWRITE(ISLSL1,DV(IPUN),NDVXK)                                        
      CALL JPOSRL(ISLSL1,NPSI+NPSI)                                             
      IPUN=IPUN+NDVX                                                            
 70   CONTINUE                                                                  
      CALL JCLOSE(ISLSL1)                                                       
      CALL JFREE(*2000,'VXK     ')                                              
C                                                                               
      CALL JALLOC(*2000,'VMASS   ','D.P.',1,10            ,JVMASS)               
      CALL JOPEN('*MAS    ',ISLMAS,1)                                           
      CALL JREAD(ISLMAS,DV(JVMASS),20)                                          
      CALL JCLOSE(ISLMAS)                                                       
      nter=(neltra+1)*6                                                         
      CALL JALLOC(*2000,'FLEXS   ','D.P.',1,NSOL*NSOL    ,JFLEXS)               
      CALL JALLOC(*2000,'RIGIDS  ','D.P.',1,NSOL*NSOL    ,JRIGIS)               
      CALL JALLOC(*2000,'TI      ','D.P.',1,NSOL*NSOL    ,JTI   )               
      CALL JALLOC(*2000,'TEMP    ','D.P.',1,9*9          ,JTEMP )               
      CALL JALLOC(*2000,'TEMQ    ','D.P.',1,nter         ,JTEMq )               
      nt=max(nsol*2,nter)                                                       
      CALL JALLOC(*2000,'PERM    ','D.P.',1,nt           ,JPERM )               
      CALL JALLOC(*2000,'RIGIE   ','D.P.',1,144          ,JRIGIE)               
      CALL JALLOC(*2000,'MATTR   ','D.P.',1,nter*nter    ,JRMATR)               
C                                                                               
C     CALCOLO DELLE CARATTERISTICHE DELLA SEZIONE                               
C                                                                               
      CALL CRSTIF(DV(JFLEX),DV(JRIGID),DV(JTI),DV(JTEMP),DV(JRIGIS),            
     1  DV(JFLEXS),NSOL,DV(JPERM),DV(JVMASS),DV(JRIGIE),DV(JWJX),               
     2  dv(jrmatr),nter,dv(jtemq))                                              
                                                                                
C                                                                               
      CALL JOPEN('*CSZ    ',ISLCSZ,1)                                           
      CALL JWRITE(ISLCSZ,DV(JRIGID),72)                                         
      CALL JWRITE(ISLCSZ,DV(JFLEX),72)                                          
      CALL JWRITE(ISLCSZ,DV(JWJX),72)                                           
      CALL JCLOSE(ISLCSZ)                                                       
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
C                                                                               
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.CHANGE                                                        
C*************************          CHANGE          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CHANGE(VX,VD,NDVX,XY,WJX,ELLE,I2,I3)                           
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION VX(NDVX,1),VD(NDVX,1),XY(2),WJX(6,6)                            
      DIMENSION ELLE(6,6)                                                       
C                                                                               
      DO 12 I=1,6                                                               
      VX(1,I)=VX(1,I)-WJX(1,I)+XY(2)*WJX(6,I)                                   
      VX(1+I2,I)=VX(1+I2,I)-WJX(2,I)-XY(1)*WJX(6,I)                             
      VX(1+I3,I)=VX(1+I3,I)-WJX(3,I)-XY(2)*WJX(4,I)+XY(1)*WJX(5,I)              
      VD(1,I)=0.D0                                                              
      VD(1+I2,I)=0.D0                                                           
      VD(1+I3,I)=0.D0                                                           
 12   CONTINUE                                                                  
      DO 14 I=1,6                                                               
      DO 14 K=1,6                                                               
      VD(1,I)=VD(1,I)+VX(1,K)*ELLE(K,I)                                         
      VD(1+I2,I)=VD(1+I2,I)+VX(1+I2,K)*ELLE(K,I)                                
      VD(1+I3,I)=VD(1+I3,I)+VX(1+I3,K)*ELLE(K,I)                                
 14   CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
C                                                                               
C                                                                               
       END                                                                      
C@ELT,I ANBA*ANBA.COSFLX                                                        
C*************************          COSFLX          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE COSFLX(IPIAZ,ZT,NRZT,FLEX,NSOL,TNM1,NRTNM1,X,NRX,              
     1                  ZT2,W)                                                  
C                                                                               
C     COSTRUZIONE DELLA MATRICE DI FLESSIBILITA' DELLA SEZIONE                  
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      INTEGER SPCCOD                                                            
      LOGICAL DUMP                                                              
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      DIMENSION IPIAZ(1),ZT(NRZT,1),FLEX(NSOL,1),X(NRX,1)                       
     1   ,TNM1(NRTNM1,1),ZT2(NRZT,1),W(NSOL,1)                                  
      DATA MPCCOD,SPCCOD/6,7/                                                   
C                                                                               
      CALL DZERO(FLEX,NSOL*NSOL)                                                
      CALL DZERO(W,NSOL*NSOL)                                                   
C                                                                               
      DO 30 IEL=1,NELRIG                                                        
C                                                                               
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT,1)                                                   
      CALL JREAD(ISLVPZ,NT1,1)                                                  
      CALL JREAD(ISLVPZ,IPIAZ,NT)                                               
      DO 10 I=1,NT-1                                                            
      CALL JREAD(ISLVPZ,TNM1(1,I),NT1*2)                                        
 10   CONTINUE                                                                  
      IF(ITIP.EQ.MPCCOD) GOTO 30                                                
      IF(ITIP.EQ.SPCCOD) GOTO 30                                                
      IF(ITIP.EQ.2) GOTO 30                                                     
      NT=NT-1                                                                   
      NTERM=NT                                                                  
      DO 20 I=1,NTERM                                                           
      DO 20 K=1,NSOL                                                            
      ZT(I,K)=0.D0                                                              
      DO 20 L=1,NTERM                                                           
      L1=IPIAZ(L)                                                               
      ZT(I,K)=ZT(I,K)+TNM1(I,L)*X(L1,K)                                         
 20   CONTINUE                                                                  
      DO 25 I=1,NSOL                                                            
      DO 25 K=1,NSOL                                                            
      DO 25 L=1,NTERM                                                           
      L1=IPIAZ(L)                                                               
      FLEX(I,K)=FLEX(I,K)+ZT(L,K)*X(L1,I)                                       
 25   CONTINUE                                                                  
C                                                                               
      NTERM=NT-NPSI                                                             
      DO 80 I=1,NTERM                                                           
      CALL JREAD(ISLMTF,TNM1(1,I),NT*2)                                         
80    CONTINUE                                                                  
      DO 60 I=1,NT                                                              
      DO 60 K=1,NSOL                                                            
      ZT(I,K)=0.D0                                                              
      ZT2(I,K)=0.D0                                                             
      DO 60 L=1,NTERM                                                           
      L1=IPIAZ(L)                                                               
      ZT2(I,K)=ZT2(I,K)+TNM1(I,L)*X(L1,K)                                       
      L1=L1+NEQV                                                                
      ZT(I,K)=ZT(I,K)+TNM1(I,L)*X(L1,K)                                         
60    CONTINUE                                                                  
      DO 70 I=1,NSOL                                                            
      DO 70 K=1,NSOL                                                            
      DO 70 L=1,NT                                                              
      L1=IPIAZ(L)                                                               
      ADD=ZT(L,K)*X(L1,I)                                                       
      FLEX(I,K)=FLEX(I,K)+ADD                                                   
      FLEX(K,I)=FLEX(K,I)+ADD                                                   
      W(I,K)=W(I,K)+ZT2(L,K)*X(L1,I)                                            
70    CONTINUE                                                                  
C                                                                               
      DO 75 IS=1,NTERM                                                          
      CALL JREAD(ISLMTF,TNM1(1,IS),NTERM*2)                                     
75    CONTINUE                                                                  
      DO 40 I=1,NTERM                                                           
      DO 40 K=1,NSOL                                                            
      ZT(I,K)=0.D0                                                              
      ZT2(I,K)=0.D0                                                             
      DO 40 L=1,NTERM                                                           
      L1=IPIAZ(L)                                                               
      ZT2(I,K)=ZT2(I,K)+TNM1(I,L)*X(L1,K)                                       
      L1=L1+NEQV                                                                
      ZT(I,K)=ZT(I,K)+TNM1(I,L)*X(L1,K)                                         
40    CONTINUE                                                                  
      DO 50 I=1,NSOL                                                            
      DO 50 K=1,NSOL                                                            
      DO 50 L=1,NTERM                                                           
      L1=IPIAZ(L)+NEQV                                                          
      FLEX(I,K)=FLEX(I,K)+ZT(L,K)*X(L1,I)                                       
      W(I,K)=W(I,K)+ZT2(L,K)*X(L1,I)                                            
 50   CONTINUE                                                                  
C                                                                               
C                                                                               
C                                                                               
30    CONTINUE                                                                  
C     
      
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.CRSTIF                                                        
C*************************          CRSTIF          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CRSTIF(FLEX,RIGID,TI,TEMP,RIGIDS,FLEXS,NSOL,PERM,VMASS,        
     *                 RIGIE,WJX,rmatr,nd,TEMQ)                                 
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP                                                              
      character*32 filfit,filstr                                                
      DIMENSION FLEX(NSOL,1),RIGID(NSOL,6),TI(NSOL,1),TEMP(NSOL,1)              
      DIMENSION TTT(3,3),RIGIE(12,12),WJX(6,6),rmatr(nd,*)                      
      DIMENSION RIGIDS(NSOL,1),FLEXS(NSOL,1),PERM(1),VMASS(1),RMASS(6,6)        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunst                            
      common /trave3/ rl,rve(6),neltra                                          
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),IAUTCN,             
     1                IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                    
      real*8 rml(12,6),ff(12,12),ltff(6,12),ltffl(6,6),fti(6,6)                 
      real*8 lfti(12,6),temq(*),rigidr(6,6) 
      real*8 alfmat(6,6)                                                
      DATA PIGRE/3.141592653589793D0/                                           
      DATA TTT/0.D0,1.D0,0.D0,-1.D0,5*0.D0/                                     
C                                                                               
C                                                                               
C                                                                               
      IF(DUMP(10)) THEN                                                         
      DO 10 I=1,6                                                               
      DO 10 L=1,6                                                               
      RIGIE(I,L)=RIGID(I,L)                                                     
      RIGIE(I,L+6)=0                                                            
      RIGIE(I+6,L)=0                                                            
      RIGIE(I+6,L+6)=0                                                          
  10  CONTINUE                                                                  
      DO 20 I=1,6                                                               
      DO 20 L=1,3                                                               
      DO 21 K=1,3                                                               
      RIGIE(I,L+9)=RIGIE(I,L+9)+RIGID(I,K)*TTT(K,L)                             
  21  CONTINUE                                                                  
      RIGIE(L+9,I)=RIGIE(I,L+9)                                                 
 20   CONTINUE                                                                  
      DO 30 I=1,3                                                               
      DO 30 L=1,3                                                               
      DO 30 K=1,3                                                               
      RIGIE(I+9,L+9)=RIGIE(I+9,L+9)-TTT(I,K)*RIGIE(K,L+9)                       
 30   CONTINUE                               
      CALL INTEST                                                               
      WRITE(IOUT,701)((RIGIE(IR,IC),IC=1,12),IR=1,12)                           
 701  FORMAT(///10X,'MATRICE DI RIGIDEZZA GENERALE'/                            
     *   10X,30('*')//12(1X,12E11.5//))                                         
C                                                                               
      WRITE(IOUT,702)((WJX(IR,IC),IC=1,6),IR=1,6)                               
 702  FORMAT(///10X,'MATRICE DI SPOSTAMENTO RIGIDO'/                            
     *   10X,30('*')//6(20X,6D15.6//))                                          
      END IF                                                                    
C                                                                               

      do i=1,6
      do l=1,6 
      alfmat(i,j)=0.d0
      enddo
      enddo                                  
      alfmat(1,1)=cdc(1)
      alfmat(2,1)=cdc(2)
      alfmat(3,1)=cdc(3)
      alfmat(1,3)=cdc(3)
      alfmat(2,3)=cdc(2)
      alfmat(3,3)=-cdc(1)
      alfmat(2,2)=1.d0
      alfmat(4,4)=cdc(1)
      alfmat(5,4)=cdc(2)
      alfmat(6,4)=cdc(3)
      alfmat(4,6)=cdc(3)
      alfmat(5,6)=cdc(2)
      alfmat(6,6)=-cdc(1)
      alfmat(5,5)=1.d0
      
C                                                                               
      CALL INTEST                                                               
      WRITE(IOUT,107)                                                           
      WRITE(IOUT,200)((FLEX(I,J),J=1,NSOL),I=1,NSOL)                            
      WRITE(IOUT,100)                                                           
      WRITE(IOUT,200)((RIGID(I,J),J=1,NSOL),I=1,NSOL)                           

      if(dabs(cdc(3)-1.d0).gt.0.0d0) then
      CALL DPROMM(rigid,6,6,6,alfmat,6,fti,6,6,0,0)                          
      CALL DPROMM(alfmat,6,6,6,fti,6,rigidr,6,6,1,0)                          
      WRITE(IOUT,200)((alfmat(I,J),J=1,6),I=1,6)                           
      WRITE(IOUT,105)                                                           
      WRITE(IOUT,200)((RIGIDr(I,J),J=1,NSOL),I=1,NSOL)                           
      endif

C                                                                               
      XSN=-RIGID(3,5)/RIGID(3,3)                                                
      YSN=RIGID(3,4)/RIGID(3,3)                                                 
      G1=-2.*(RIGID(3,4)*XSN+RIGID(4,5))                                        
      G2=RIGID(3,5)*XSN+RIGID(5,5)+RIGID(3,4)*YSN-RIGID(4,4)                    
      RR=((RIGID(4,4)+RIGID(5,5))/2.)*1.E-5                                     
      ALFA=0.D0                                                                 
      IF(DABS(G2).GT.RR) ALFA=.5*DATAN2(G1,G2)                                  
      SINA=DSIN(ALFA)                                                           
      COSA=DCOS(ALFA)                                                           
      ALFA=ALFA*180./PIGRE                                                      
      DIVIS=RIGID(1,1)*RIGID(2,2)-RIGID(1,2)*RIGID(1,2)                         
      XCT=(RIGID(2,6)*RIGID(1,1)-RIGID(1,6)*RIGID(2,1))/DIVIS                   
      YCT=(RIGID(1,2)*RIGID(2,6)-RIGID(2,2)*RIGID(1,6))/DIVIS                   
      RIGTOR=RIGID(1,6)*YCT-RIGID(2,6)*XCT+RIGID(6,6)                           
C                                                                               
      CALL SETTRS(RIGID,NSOL,TI,COSA,SINA,XSN,YSN,TEMP,RIGIDS)                  
      WRITE(IOUT,101)                                                           
      WRITE(IOUT,200)((RIGIDS(I,J),J=1,NSOL),I=1,NSOL)                          
C                                                                               
      RIFLX=RIGIDS(4,4)                                                         
      RIFLY=RIGIDS(5,5)                                                         
      RIASS=RIGIDS(3,3)                                                         
C                                                                               
      CALL INVERT6(RIGIDS,NSOL,FLEXS,NSOL,NSOL,PERM)                             
C                                                                               
      WRITE(IOUT,102)                                                           
      WRITE(IOUT,200)((FLEXS(I,J),J=1,NSOL),I=1,NSOL)                           
C                                                                               
      ETAXS=-FLEXS(4,6)/FLEXS(4,4)                                              
      ETAYS=-FLEXS(5,6)/FLEXS(5,5)                                              
      ETAX=ETAXS*COSA-ETAYS*SINA                                                
      ETAY=ETAYS*COSA+ETAXS*SINA                                                
      AEXS=DATAN(ETAXS)                                                         
      AEYS=DATAN(ETAYS)                                                         
      AEX=DATAN(ETAX)                                                           
      AEY=DATAN(ETAY)                                                           
      GAMXS=-FLEXS(6,4)/FLEXS(6,6)                                              
      GAMYS=-FLEXS(6,5)/FLEXS(6,6)                                              
      AFLXS=DATAN(GAMXS)                                                        
      AFLYS=DATAN(GAMYS)                                                        
      AEXS=AEXS*180./PIGRE                                                      
      AEYS=AEYS*180./PIGRE                                                      
      AEX=AEX*180./PIGRE                                                        
      AEY=AEY*180./PIGRE                                                        
      AFLXS=AFLXS*180./PIGRE                                                    
      AFLYS=AFLYS*180./PIGRE                                                    
C                                                                               
      CALL INTEST                                                               
C                                                                               
      WRITE(IOUT,1000)XSN,YSN,ALFA,RIFLX,RIFLY,RIASS                            
     1 ,XCT,YCT,RIGTOR,AEXS,AEYS,AEX,AEY,ETAXS,ETAYS,ETAX,ETAY,                 
     2 AFLXS,AFLYS,GAMXS,GAMYS                                                  
C                                                                               
      CALL DZERO(RMASS,36)
      write(iout,*)' 1 ',vmass(1)
      write(iout,*)' x ',vmass(2)
      write(iout,*)' y ',vmass(3)
      write(iout,*)' xx',vmass(4)
      write(iout,*)' yy',vmass(5)
      write(iout,*)' xy',vmass(6)
      write(iout,*)' z ',VMASS(7) 
      write(iout,*)' xz',VMASS(8) 
      write(iout,*)' yz',VMASS(9) 
      write(iout,*)' zz',VMASS(10) 
      RMASS(1,1)= VMASS(1)                                                      
      RMASS(1,6)=-VMASS(3)                                                      
      RMASS(2,2)= VMASS(1)                                                      
      RMASS(2,6)= VMASS(2)                                                      
      RMASS(3,3)= VMASS(1)                                                      
      RMASS(3,4)= VMASS(3)                                                      
      RMASS(3,5)=-VMASS(2)                                                      
      RMASS(4,3)= VMASS(3)                                                      
      RMASS(4,4)= VMASS(5)                                                      
      RMASS(4,5)=-VMASS(6)                                                      
      RMASS(5,3)=-VMASS(2)                                                      
      RMASS(5,4)=-VMASS(6)                                                      
      RMASS(5,5)= VMASS(4)                                                      
      RMASS(6,1)=-VMASS(3)                                                      
      RMASS(6,2)= VMASS(2)                                                      
      RMASS(6,6)= VMASS(4)+VMASS(5)                                             

      RMASS(1,4)= VMASS(7)                                                      
      RMASS(4,1)= -VMASS(7)                                                      
      RMASS(4,4)= RMASS(4,4)+VMASS(10)                                                      
      RMASS(5,5)= RMASS(5,5)+VMASS(10)                                                      
      RMASS(4,6)= -VMASS(8)                                                      
      RMASS(6,4)= -VMASS(8)                                                      
      RMASS(5,6)= -VMASS(9)                                                      
      RMASS(6,5)= -VMASS(9)                                                      



      XCG=-RMASS(3,5)/RMASS(1,1)                                                
      YCG= RMASS(3,4)/RMASS(1,1)                                                
      RJXX=RMASS(5,5)-RMASS(1,1)*XCG*XCG                                        
      RJYY=RMASS(4,4)-RMASS(1,1)*YCG*YCG                                        
      RJXY=-RMASS(4,5)-RMASS(1,1)*XCG*YCG                                       
      G1=2.D0*RJXY                                                              
      G2=RJXX-RJYY                                                              
      RR=((RJXX+RJYY)/2.D0)*1.D-5                                               
      ALFAm=0.D0                                                                
      IF(DABS(G2).GT.RR) ALFAm=.5D0*DATAN2(G1,G2)                               
      CS=DCOS(ALFAm)                                                            
      SS=DSIN(ALFAm)                                                            
      CS2=CS*CS                                                                 
      SS2=SS*SS                                                                 
      SSCS=SS*CS                                                                
      Ry=RJXX*CS2+RJYY*SS2+2.D0*RJXY*SSCS                                       
      Rx=RJXX*SS2+RJYY*CS2-2.D0*RJXY*SSCS                                       
      RP=RJXX+RJYY                                                              
      ALFAm=ALFAm*180.D0/PIGRE                                                  
      CALL SETTRS(RMASS,NSOL,TI,CS,SS,XCG,YCG,TEMP,RIGIDS)                      
      CALL INTEST                                                               
      WRITE(IOUT,103)                                                           
      WRITE(IOUT,200)((RMASS(I,J),J=1,NSOL),I=1,NSOL)                           
      WRITE(IOUT,104)                                                           
      WRITE(IOUT,200)((RIGIDS(I,J),J=1,NSOL),I=1,NSOL)                          
      WRITE(IOUT,1001)RMASS(1,1),XCG,YCG,ALFAm,RX,RY,RP                         
C                                                                               
C spostate indietro di 20 char tutte le stringhe dei format 1000 e 1001         
C                                                                               
1000  FORMAT(////10X,'CARATTERISTICHE DI RIGIDEZZA'/10X,28('*')///              
     * 43X,'*  ASSI PRINCIPALI FLESSIONE  *',                                   
     *     '*    ASSI  DEL  DISEGNO       *',//                                 
     * 39X,2(15X,'X',15X,'Y'),//                                                
     A  5X,'COORDINATE DEGLI SFORZI NORMALI',8X,2E15.4//                        
     B  5X,'ROTAZIONE ASSI PRINCIPALI (GRADI)',6X,E15.4//                       
     C  5X,'RIGIDEZZE FLESSIONALI',18X,2E15.4//                                 
     D  5X,'RIGIDEZZA ASSIALE',22X,E15.4//                                      
     E  5X,'COORDINATE CENTRO DI TAGLIO',12X,2E15.4//                           
     F  5X,'RIGIDEZZA TORSIONALE',19X,E15.4//                                   
     G  5X,'INCLINAZIONE DEL MOMENTO TORCENTE'/                                 
     H  5X,'(CHE DA PURA TORSIONE - GRADI)',9X,4E15.4//                         
     I  5X,'RAPPORTI MX/MZ E MY/MZ            '/                                
     L  5X,'CORRISPONDENTI         ',16X,4E15.4//                               
     M  5X,'INCLINAZIONI DEI MOMENTI FLETTENTI '/                               
     N  5X,'(PURE FLESSIONI PRINCIPALI - GRADI)',4X,2E15.4//                    
     O  5X,'RAPPORTI MZ/MX E MZ/MY'/                                            
     P  5X,'CORRISPONDENTI          ',15X,2E15.4)                               
1001  FORMAT(//10X,'CARATTERISTICHE DI MASSA    '/10X,24('*')///                
     A  5X,'MASSA DELLA SEZIONE            ',8X,E15.4//                         
     A  5X,'COORDINATE DEL BARICENTRO      ',8X,2E15.4//                        
     B  5X,'ROTAZIONE ASSI PRINCIPALI        ',6X,E15.4//                       
     C  5X,'MOMENTI PRINCIPALI DI INERZIA',10X,2E15.4//                         
     D  5X,'MOMENTO POLARE   ',22X,E15.4)                                       
C200   FORMAT(6(6D15.6//))                                                      
200   FORMAT(6(6D17.10/))                                                       
107   FORMAT(//28X,'***   MATRICE DI FLESSIBILITA` (ORDINATA SECONDO',          
     1 ' TX,TY,TZ,MX,MY,MZ)   ***'//)                                           
100   FORMAT(//30X,'***   MATRICE DI RIGIDEZZA (ORDINATA SECONDO',              
     1 ' TX,TY,TZ,MX,MY,MZ)   ***'//)                                           
105   FORMAT(//30X,'***   MATRICE DI RIGIDEZZA RUOTATA (ORDINATA '
     * 'SECONDO TX,TY,TZ,MX,MY,MZ)   ***'//)                                           
101   FORMAT(///26X,'***   MATRICE DI RIGIDEZZA RIFERITA AGLI ASSI',            
     1 ' PRINCIPALI DELLA FLESSIONE   ***'//)                                   
102   FORMAT(///25X,'***   MATRICE DI FLESSIBILITA'' RIFERITA AGLI ASSI'        
     1 ,' PRINCIPALI DELLA FLESSIONE   ***'//)                                  
103   FORMAT(///40X,'***   MATRICE DI MASSA DELLA SEZIONE   ',                  
     *                   '(ASSI DISEGNO)   ***'//)                              
104   FORMAT(//32X,'***   MATRICE DI MASSA DELLA SEZIONE  ',                    
     *         '(ASSI PRINCIPALI DI INERZIA)   ***'//)                          
C                                                                               
C                                                                               
      IF(NFR.gt.0) THEN                                                         
        OPEN(15,file=filfit,STATUS='UNKNOWN',ACCESS='DIRECT',                   
     *       FORM='UNFORMATTED',RECL=576)                                       
        write(iout,*) ' record: ',nfr                                                  
c       WRITE(15,REC=NFR) RIGID,RMASS                                           
        WRITE(15,REC=NFR)((RIGID(I,J),I=1,6),J=1,6),                            
     1                   ((RMASS(I,J),I=1,6),J=1,6)
	  close(15)                                    
	elseif(nfr.lt.0)then
        OPEN(15,file=filfit,STATUS='UNKNOWN',ACCESS='APPEND',                   
     *       FORM='FORMATTED')                                       
        write(iout,*) ' Matrici su file formattato: '                                                  
        WRITE(15,'(6e16.9)')((RIGID(I,J),I=1,6),J=1,6)                            
        if(nfr.lt.-1) then
          WRITE(15,'(6e16.9)')((RMASS(I,J),I=1,6),J=1,6)                            
        endif                             
        close(15)                                                                      
      END IF                                                                    
      IF(ircfit.NE.0) THEN                                                      
        OPEN(66,file=filfit,STATUS='UNKNOWN',ACCESS='DIRECT',                   
     *       FORM='UNFORMATTED',RECL=176)                                       
        WRITE(66,REC=ircfit)                                                    
     1  real(rmass(1,1)),real(xcg),real(ycg),real(0.),real(0.),                 
     2  real(0.),real(alfam),real(rx),real(ry),real(rp),                        
     3  real(riass),real(xsn),real(ysn),real(0.),real(0.),real(0.),             
     4  real(alfa),real(riflx),real(rifly),real(rigtor),real(xct),              
     5  real(yct)                                                               
        close(66)                                                               
      END IF                                                                    
c     real*8 rml(12,6),ff(12,12),ltff(6,12),ltffl(6,6),fti(6,6)                 
C                                                                               
      if(rl.gt.0) then                                                          
	call dzero(rml,72)                                                             
	call invert6(rigid,6,flex,6,6,perm)                                             
	do i=1,6                                                                       
         rml(i,i)=-1                                                            
         rml(i+6,i)=1                                                           
        enddo                                                                   
	rml(4,2)=rl                                                                    
	rml(5,1)=-rl                                                                   
	 do i=1,6                                                                      
	 do l=1,6                                                                      
	 ff(i,l)=2*flex(i,l)*rl/6                                                      
	 ff(i+6,l)=-flex(i,l)*rl/6                                                     
	 ff(i,l+6)=-flex(i,l)*rl/6                                                     
	 ff(i+6,l+6)=2*flex(i,l)*rl/6                                                  
	 enddo                                                                         
	 enddo                                                                         
	 call DPROMM(rml ,12,12,6,ff, 12,ltff,  6,12,1,0)                              
	 call DPROMM(ltff, 6,6,12,rml,12,ltffl, 6, 6,0,0)                              
	 call invert6(ltffl,6,fti,6,6,perm)                                             
	 call DPROMM(rml ,12,12, 6,fti, 6,lfti, 12, 6,0,0)                             
	 call DPROMM(lfti,12,12, 6,rml,12,rigie,12,12,0,1)                             
	 write(iout,*)' flexsez '                                                      
	 do i=1,6                                                                      
	 write(iout,'(6e12.5)') (flex(i,l),l=1,6)                                      
	 enddo                                                                         
	 call invert6(flex,6,fti,6,6,perm)                                              
	 write(iout,*)' rigisez '                                                      
	 do i=1,6                                                                      
	 write(iout,'(6e12.5)') (fti(i,l),l=1,6)                                       
	 enddo                                                                         
	 call invert6(fti,6,flex,6,6,perm)                                              
	 write(iout,*)' flexsez '                                                      
	 do i=1,6                                                                      
	 write(iout,'(6e12.5)') (flex(i,l),l=1,6)                                      
	 enddo                                                                         
c write(iout,*)' TRAVE3D '                                                      
c do i=1,12                                                                     
c write(iout,'(12e20.9)') (rigie(i,l),l=1,12)                                   
c enddo                                                                         
	 call dzero(rmatr,nd*nd)                                                       
	 print*,neltra                                                                 
	 do i=1,neltra                                                                 
	 ib=(i-1)*6                                                                    
	 do ir=1,12                                                                    
	 do ic=1,12                                                                    
	 rmatr(ib+ir,ib+ic)=rmatr(ib+ir,ib+ic)+rigie(ir,ic)                            
	 enddo                                                                         
	 enddo                                                                         
	 enddo                                                                         
	 call rufct(rmatr(7,7),perm,neltra*6,nd,ier)                                   
	 call dzero(temq,neltra*6)                                                     
	 s=0.                                                                          
	 do i=1,6                                                                      
	 s=s+abs(rve(i))                                                               
	 temq((neltra-1)*6+i)=rve(i)                                                   
	 enddo                                                                         
	 if(s.lt.0.01) then                                                            
	 write(iout,*)' matrice spostamenti per carichi unitari'                       
	 do i=1,6                                                                      
	 call dzero(temq,neltra*6)                                                     
	 temq((neltra-1)*6+i)=1.                                                       
	 call rusol(rmatr(7,7),temq,nd,neltra*6,perm)                                  
	 write(iout,'(6e12.5)') (temq(io),io=1,neltra*6)                               
	 write(iout,'(6e12.5)')                                                        
	 enddo                                                                         
	 else                                                                          
	 write(iout,*)' Carichi '                                                      
	 write(iout,'(6e12.5)') rve                                                    
	 call rusol(rmatr(7,7),temq,nd,neltra*6,perm)                                  
	 write(iout,*)' spostam '                                                      
	 write(iout,'(6e12.5)') (temq(io),io=1,neltra*6)                               
	 endif                                                                         
                                                                                
                                                                                
ccall dzero(rml,72)                                                             
crigid(1,2)=0.                                                                  
crigid(1,3)=0.                                                                  
crigid(1,4)=0.                                                                  
crigid(1,5)=0.                                                                  
crigid(2,1)=0.                                                                  
crigid(3,1)=0.                                                                  
crigid(4,1)=0.                                                                  
crigid(5,1)=0.                                                                  
crigid(2,3)=0.                                                                  
crigid(2,4)=0.                                                                  
crigid(2,5)=0.                                                                  
crigid(3,2)=0.                                                                  
crigid(4,2)=0.                                                                  
crigid(5,2)=0.                                                                  
ccall invert(rigid,6,flex,6,6,perm)                                             
cdo i=1,6                                                                       
c        rml(i,i)=-1                                                            
c        rml(i+6,i)=1                                                           
c       enddo                                                                   
crml(4,2)=rl                                                                    
crml(5,1)=-rl                                                                   
c do i=1,6                                                                      
c do l=1,6                                                                      
c ff(i,l)=2*flex(i,l)*rl/6                                                      
c ff(i+6,l)=-flex(i,l)*rl/6                                                     
c ff(i,l+6)=-flex(i,l)*rl/6                                                     
c ff(i+6,l+6)=2*flex(i,l)*rl/6                                                  
c enddo                                                                         
c enddo                                                                         
c call DPROMM(rml ,12,12,6,ff, 12,ltff,  6,12,1,0)                              
c call DPROMM(ltff, 6,6,12,rml,12,ltffl, 6, 6,0,0)                              
c call invert(ltffl,6,fti,6,6,perm)                                             
c call DPROMM(rml ,12,12, 6,fti, 6,lfti, 12, 6,0,0)                             
c call DPROMM(lfti,12,12, 6,rml,12,rigie,12,12,0,1)                             
c write(iout,*)' flexsez '                                                      
c do i=1,6                                                                      
c write(iout,'(6e12.5)') (flex(i,l),l=1,6)                                      
c enddo                                                                         
c call invert(flex,6,fti,6,6,perm)                                              
c write(iout,*)' rigisez '                                                      
c do i=1,6                                                                      
c write(iout,'(6e12.5)') (fti(i,l),l=1,6)                                       
c enddo                                                                         
c call invert(fti,6,flex,6,6,perm)                                              
c write(iout,*)' flexsez '                                                      
c do i=1,6                                                                      
c write(iout,'(6e12.5)') (flex(i,l),l=1,6)                                      
c enddo                                                                         
c write(iout,*)' TRAVE3D '                                                      
c do i=1,12                                                                     
c write(iout,'(12e20.9)') (rigie(i,l),l=1,12)                                   
c enddo                                                                         
c call dzero(rmatr,nd*nd)                                                       
c call jprmem                                                                   
c call jprtab                                                                   
c print*,neltra                                                                 
c do i=1,neltra                                                                 
c ib=(i-1)*6                                                                    
c do ir=1,12                                                                    
c do ic=1,12                                                                    
c rmatr(ib+ir,ib+ic)=rmatr(ib+ir,ib+ic)+rigie(ir,ic)                            
c enddo                                                                         
c enddo                                                                         
c enddo                                                                         
c call rufct(rmatr(7,7),perm,neltra*6,nd,ier)                                   
c call dzero(temq,neltra*6)                                                     
c s=0.                                                                          
c do i=1,6                                                                      
c s=s+abs(rve(i))                                                               
c temq((neltra-1)*6+i)=rve(i)                                                   
c enddo                                                                         
c if(s.lt.0.01) then                                                            
c write(iout,*)' matrice spostamenti per carichi unitari'                       
c do i=1,6                                                                      
c call dzero(temq,neltra*6)                                                     
c temq((neltra-1)*6+i)=1.                                                       
c call rusol(rmatr(7,7),temq,nd,neltra*6,perm)                                  
c write(iout,'(6e12.5)') (temq(io),io=1,neltra*6)                               
c write(iout,'(6e12.5)')                                                        
c enddo                                                                         
c else                                                                          
c write(iout,*)' Carichi '                                                      
c write(iout,'(6e12.5)') rve                                                    
c call rusol(rmatr(7,7),temq,nd,neltra*6,perm)                                  
c write(iout,*)' spostam '                                                      
c write(iout,'(6e12.5)') (temq(io),io=1,neltra*6)                               
c endif                                                                         
      endif                                                                     
C                                                                               
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.SETTRS                                                        
C*************************          SETTRS          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE SETTRS(RM,NM,TRS,COSA,SINA,XSN,YSN,TEMP,RIG)                   
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION RM(NM,1),TRS(NM,1),TEMP(NM,1),RIG(NM,1)                         
C                                                                               
      CALL DZERO(TRS,NM*NM)                                                     
      TRS(1,1)=COSA                                                             
      TRS(2,2)=COSA                                                             
      TRS(3,3)=1.                                                               
      TRS(4,4)=COSA                                                             
      TRS(5,5)=COSA                                                             
      TRS(6,6)=1.                                                               
      TRS(1,2)=SINA                                                             
      TRS(2,1)=-SINA                                                            
      TRS(6,1)=YSN                                                              
      TRS(6,2)=-XSN                                                             
      TRS(4,3)=-YSN*COSA+XSN*SINA                                               
      TRS(4,5)=SINA                                                             
      TRS(5,3)=XSN*COSA+YSN*SINA                                                
      TRS(5,4)=-SINA                                                            
C                                                                               
      CALL DZERO(TEMP,NM*NM)                                                    
      CALL DPROMM(RM,NM,NM,NM,TRS,NM,TEMP,NM,NM,0,1)                            
      CALL DZERO(RIG,NM*NM)                                                     
      CALL DPROMM(TRS,NM,NM,NM,TEMP,NM,RIG,NM,NM,0,0)                           
C                                                                               
      RETURN                                                                    
      END                                                                       
                                                                                
                                                                                
C@ELT,I ANBA*ANBA.SOLMDF                                                        
C*************************          SOLMDF          ********************        
C     COMPILER (ARGCHK=OFF)                                                     
      SUBROUTINE SOLMDF(LABEL,NUME,NUINT,RIGID,FLEX,WJX,                        
     *     VX,NDVX,VXK,NDVXK,XY,NODSEL,NNSEL)                                   
C                                                                               
      IMPLICIT REAL*8  (A-H,O-Z)                                                
      LOGICAL DUMP,INCORE                                                       
      DIMENSION FLEX(6,6),RIGID(6,6),PERM(6),NODSEL(1),FLEXLOC(6,6)                          
      common /sisrif/ xs,ys,ss,cs,iflsr,iflsro,iflsrs                           
      DIMENSION NUINT(1),NUME(1),XY(2,1),LABEL(1),WJX(6,6)                      
      DIMENSION VX(NDVX,1),VXK(NDVXK,1),ELLE(6,6)                               
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /AUTCON/ ERR,CONST,Xc,Yc,Sc,Cc,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
                                                                                
      CALL ITELLE(1,CDC,ELLE,1.D0)                                              
      print'(6e12.5)',elle                                                      
C      PRINT*,' VALORI DI CDC'                                                  
C      WRITE(*,'(1X,6E12.5)')(CDC(I),I=1,6)                                     
C                                                                               
c      PRINT*,NPSI                                                              
c      WRITE(IOUT,'(A)')' MATRICE VX PRIMA'                                     
c      DO 30 I=1,NDVX                                                           
c      WRITE(IOUT,450)(VX(I,K),K=1,6)                                           
c 30   CONTINUE                                                                 
      DO 10 K=1,NNODI                                                           
      IND=(NUINT(K)-1)*NCS+1                                                    
      IND1=NUME(IND)                                                            
      IND2=NUME(IND+1)-IND1                                                     
      IND3=NUME(IND+2)-IND1                                                     
c      WRITE(IOUT,'(/,A,/,1X,4I6)')' STAMPA DA IND A IND3',IND,IND1             
c    1                             ,IND2,IND3                                   
c      DO 3 I=1,6                                                               
c      WRITE(IOUT,'(/,A,6E12.5)')' PRIMA',VX(IND1,I),VX(IND1+IND2,I)            
c    1                  ,VX(IND1+IND3,I),VX(IND1+NEQV,I),                       
c    1                   VX(IND1+NEQV+IND2,I),VX(IND1+NEQV+IND3,I)              
c  3   CONTINUE                                                                 
      CALL CHANGE(VX(IND1,1),VX(IND1+NEQV,1),NDVX,XY(1,K),WJX,                  
     1          ELLE,IND2,IND3)                                                 
c      DO 4 I=1,6                                                               
c      WRITE(IOUT,'(/,A,6E12.5)')' DOPO ',VX(IND1,I),VX(IND1+IND2,I),           
c    1                   VX(IND1+IND3,I),VX(IND1+NEQV,I),                       
c    1                   VX(IND1+NEQV+IND2,I),VX(IND1+NEQV+IND3,I)              
c 4    CONTINUE                                                                 
 10    CONTINUE                                                                 


      DO 20 I=1,6                                                               
      CALL DMOVE(FLEX(1,I),VX(NEQV-6+1,I),6)                                    
 20    CONTINUE                                                                 
C                                                                               

      IF(.NOT.DUMP(7))GOTO 220                                                  
C                                                                               
      I=0                                                                       
      NNS=NNSEL                                                                 
      IF(NNSEL.EQ.0) NNS=NNODI                                                  
      NNOP=45/(NCS+1)                                                           
      NBL=(NNS+NNOP-1)/NNOP                                                     
      NN=NNOP                                                                   
      DO 50 IW=1,NBL                                                            
      CALL INTEST                                                               
      WRITE(IOUT,430)                                                           
      IF(IW.EQ.NBL) NN=NNS-(NBL-1)*NN                                           
      DO 51 LL=1,NN                                                             
 77   I=I+1                                                                     
      I2=NUINT(I)                                                               
      I7=(I2-1)*NCS+1                                                           
      I1=LABEL(I)                                                               
      IF(NNSEL.NE.0)THEN                                                        
      IPNOD=IPOSL(I1,NODSEL,NNSEL)                                              
      IF(IPNOD.EQ.0)GOTO 77                                                     
      END IF                                                                    
      IND=NUME(I7)-1                                                            
      if(iflsro.eq.1) then                                                      
       write(iout,*) ' spost in rif cil'                                        
        a=atan2(xy(2,i),xy(1,i))                                                
        do  l=1,6                                                               
          x=vx(ind+1,l)                                                         
          INDy=nume(i7+1)                                                       
          y=vx(indy,l)                                                          
          rigid(1,l)=x                                                          
          rigid(2,l)=y                                                          
          r=sqrt(x*x+y*y)                                                       
          if(x.ne.0) then                                                       
            b=atan2(y,x)                                                        
          else                                                                  
            b=0                                                                 
          endif                                                                 
          amb=a-b                                                               
          write(iout,899)i,r,b,x,y,a,amb,xy(1,i),xy(2,i)                        
899       format(i3,8e12.5)                                                     
          b=r*sin(amb)                                                          
          r=r*cos(amb)                                                          
          vx(ind+1,l)=r                                                         
          vx(indy,l)=b*180/3.1415                                               
        enddo                                                                   
      endif                                                                     
      WRITE(IOUT,440)I1,1,(VX(IND+1,L),L=1,6)                                   
      IF(NCS.EQ.1)GOTO 51                                                       
      DO 60 M=2,NCS                                                             
       I7=I7+1                                                                  
       IND=NUME(I7)                                                             
      WRITE(IOUT,441) M,(VX(IND,L),L=1,6)                                       
 60    CONTINUE                                                                 
      if(iflsro.eq.1) then                                                      
      do  l=1,6                                                                 
      vx(ind+1,l)=rigid(1,l)                                                    
      vx(indy,l)=rigid(2,l)                                                     
      enddo                                                                     
      endif                                                                     
 51   CONTINUE                                                                  
 50    CONTINUE                                                                 
      IF(NMPC.EQ.0) GOTO 75                                                     
      CALL INTEST                                                               
      WRITE(IOUT,445)                                                           
        DO 70 I=1,NMPC                                                          
          I1=NUINT(NNODI+I)                                                     
          I2=NUME((I1-1)*NCS+1)                                                 
          WRITE(IOUT,450)(VX(I2,L),L=1,6)                                       
 70     CONTINUE                                                                
 75    CONTINUE                                                                 

      CALL INTEST                                                               
      WRITE(IOUT,460)                                                           
      DO 80 I=NEQV-NPSI+1,NEQV                                                  
      WRITE(IOUT,450)(VX(I,L),L=1,6)                                            
 80    CONTINUE                                                                 
430   FORMAT(/,53X,'***  SOLUZIONI : INGOBBAMENTI  ***'//                       
     1  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
460   FORMAT(/47X,'***  SOLUZIONI : PARAMETRI DI DEFORMAZIONE  ***'//           
     1  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ')                  
445   FORMAT(/46X,'***  SOLUZIONI : MOLTIPLICATORI DI LAGRANGE  ***'//          
     1  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ')                  
440   FORMAT(/1X,I8,I6,6E19.10)                                                 
441   FORMAT(9X,I6,6E19.10)                                                     
450   FORMAT(15X,6E19.10)     
                                                  
  220 CONTINUE
      CALL INVERT6(FLEX,6,RIGID,6,6,PERM)                                       

c                                                                               
       DO 90 K=1,6                                                              
       DO 91 I=1,NEQV-6                                                         
      VXK(I,K)=0.D0                                                             
      DO 91 L=1,6                                                               
      VXK(I,K)=VXK(I,K)+VX(I,L)*RIGID(L,K)                                      
 91   CONTINUE                                                                  
                                                                                
      DO 92 I=NEQV-6+1,NDVXK                                                    
      VXK(I,K)=0.D0                                                             
      DO 92 L=1,6                                                               
      VXK(I,K)=VXK(I,K)+VX(I+6,L)*RIGID(L,K)                                    
 92   CONTINUE                                                                  
 90   CONTINUE                                                                  
      DO 93 I=1,NDVXK                                                           
      VXK(I,7)= VXK(I,2)                                                        
      VXK(I,8)=-VXK(I,1)                                                        
      VXK(I,9)= 0.D0                                                            
 93   CONTINUE                                                                  
C                                                                               

      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      END                                                                       

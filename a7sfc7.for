C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MODSFT                                                        
C*************************          MODSFT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MODSFT                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      INTEGER SPCCOD                                                            
      DIMENSION DV(1)                                                           
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3          
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      DATA MPCCOD/6/,SPCCOD/7/                                                  
C                                                                               
      write(iout,490)                                                           
490   format(/////T40,' ****  CALCOLO SFORZI TERMICI  ****'///)                 
C                                                                               
      CALL JCRERS                                                               
                                                                                
      CALL WRSOLT                                                               
      CALL JCRERS                                                               
                                                                                
C     nsol=6   e' istruzione per soluzione normale (non termica)                
C      NSOL=6                                                                   
C     NSOL=1 per soluzione termica                                              
      NSOL=1                                                                    
      NRSOL=NEQV+NEQV-NPSI                                                      
      CALL JALLOC(*2000,'GPQ     ','D.P.',1, 3*6      ,JGPQ  )                  
      CALL JALLOC(*2000,'SOL     ','D.P.',1,NRSOL*NSOL   ,JSOL  )               
C######                                                                         
      CALL JALLOC(*2000,'TEMP    ','D.P.',1,NRSOL*NSOL   ,JTEMP)                
C#####                                                                          
      CALL JOPEN('*TMP    ',ISTEMP,1)                                           
      CALL JREAD(ISTEMP,DV(JTEMP),NNODI*NCS*2)                                  
      CALL JCLOSE(ISTEMP)                                                       
                                                                                
      CALL JOPEN('*ST1    ',ISLSL1,1)                                           
C------------------------------------------------------------------             
C     NOTA: lo slot ST1 non e' stato ancora calcolato. Viene per                
C           il momento riempito di zeri di comodo. In futuro le                 
C           istruzioni che riempiono di zeri ST1 dovranno essere                
C           cancellate.                                                         
C                                                                               
      DO 11,I=1,(NSOL*NEQV)                                                     
      CALL JWRITE(ISLSL1,0.0D0,2)                                               
11    CONTINUE                                                                  
      CALL JPOSAB(ISLSL1,1)                                                     
C-------------------------------------------------------------------            
                                                                                
      CALL JOPEN('*SLTERM ',ISLSL2,1)                                           
      CALL RCPSOL(DV(JSOL),NRSOL,NSOL)                                          
C         DO 33,I=1,NEQV                                                        
C         WRITE(IOUT,*) 'PIPPO(',I,')=',DV(JSOL+I-1)                            
C33    CONTINUE                                                                 
      CALL JCLOSE(ISLSL2)                                                       
      CALL JCLOSE(ISLSL1)                                                       
C                                                                               
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
C                                                                               
      CALL DZERO(DV(JGPQ), 3*6)                                                 
C                                                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXNOD    ,JICON )                  
      CALL JALLOC(*2000,'SIGMAG  ','D.P.',1,NSOL*NPSI ,JSIGMG)                  
      CALL JALLOC(*2000,'SIGMAL  ','D.P.',1,NSOL*NPSI ,JSIGML)                  
      CALL JALLOC(*2000,'SIGMAF  ','D.P.',1,NSOL*NPSI ,JSIGMF)                  
      CALL JALLOC(*2000,'EPSI    ','D.P.',1,NSOL*NPSI ,JEPSI )                  
      CALL JALLOC(*2000,'EPSIG   ','D.P.',1,NSOL*NPSI ,JEPSIG)                  
      CALL JALLOC(*2000,'EPSIL   ','D.P.',1,NSOL*NPSI ,JEPSIL)                  
      CALL JALLOC(*2000,'D       ','D.P.',1,6*6       ,JD    )                  
C                                                                               
      CALL JOPEN('*SEL    ',ISLSEL,1)                                           
      CALL JREAD(ISLSEL,NESEL,1)                                                
      CALL JALLOC(*2000,'IELSET  ','INT.',1,NESEL   ,JIELST)                    
      IF(NESEL.NE.0) CALL JREAD(ISLSEL,IV(JIELST),NESEL)                        
      CALL JCLOSE(ISLSEL)                                                       
C-----                                                                          
      CALL JOPEN('*SFT    ',ISLSFC,1)                                           
C-----                                                                          
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
      CALL JOPEN('*ELS    ',ISLELS,1)                                           
C                                                                               
C     OPEN(20,ACCESS='SEQUENTIAL',FORM='FORMATTED',STATUS='UNKNOWN ')           
C                                                                               
c      CALL CALSFC(IV(JICON),NRSOL,IV(JNUME),IV(JNUINT),DV(JSOL),               
c     *            DV(JSIGMG),DV(JSIGML),DV(JSIGMF),DV(JEPSI),                  
c     *            DV(JEPSIG),DV(JEPSIL),                                       
c     *            DV(JD),DV(JSIGMG),DV(JSIGMG),DV(JSIGML),DV(JEPSI),           
c     *            DV(JD),IV(JIELST),DV(JGPQ))                                  
C                                                                               
C     CLOSE(20)                                                                 
C                                                                               
C-----nsolt= numero di soluzioni termiche   -----                               
      NSOLT=1                                                                   
C------                                                                         
500   FORMAT(//,T15,' **** AVVISO: MODSFT: RICHIESTO CALCOLO SFORZI TERMICI     
     * PER ELEMENTI NON IMPLEMENTATI:',A12,' ***'//)                            
C                                                                               
      NE=0                                                                      
6     CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      IF(ITIP.EQ.MPCCOD.OR.ITIP.EQ.SPCCOD) THEN                                 
      JUMP=NCON*4+6                                                             
      CALL JPOSRL(ISLELM,JUMP)                                                  
      NE=NE+1                                                                   
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.1) THEN                                                   
C                                                                               
      WRITE(IOUT,500) '  CORRENTI  '                                            
C                                                                               
      DO 10,I=1,NCORR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.1) THEN                                                        
      CALL JPOSRL(ISLELM,(1+2+2+6))                                             
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
10    CONTINUE                                                                  
                                                                                
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.2) THEN                                                   
C                                                                               
      WRITE(IOUT,500) ' GIUNZIONI  '                                            
      DO 20,I=1,NGIUR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.2) THEN                                                        
      CALL JPOSRL(ISLELM,(2+8+8))                                               
      CALL JPOSRL(ISLELS,6)                                                     
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
20    CONTINUE                                                                  
                                                                                
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.3) THEN                                                   
C                                                                               
      WRITE(IOUT,500) '  PANNELLI  '                                            
      DO 30,I=1,NPANR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.3) THEN                                                        
      CALL JPOSRL(ISLELM,(NCON+NCON*4+2+2))                                     
      CALL JREAD(ISLELM,NPINT,1)                                                
      CALL JPOSRL(ISLELM,4*2)                                                   
      CALL JPOSRL(ISLELS,(NCON*2+NCON*2+4*NPSI)*NPINT)                          
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
30    CONTINUE                                                                  
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.4) THEN                                                   
C                                                                               
c++++++++++++++++++  31 agosto   +++++++++++++++++++++++++++++++++++            
c     ho inserito l' ultimo indice [index=2]                                    
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      CALL SFCEPL(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
     1            DV(JEPSI),DV(JD),IV(JICON),NSOLT,NRSOL,4,                     
     *           IV(JIELST),NESEL,DV(JGPQ),DV(JTEMP),2)                         
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.5) THEN                                                   
C                                                                               
c     ********************************                                          
C      print*,' call sfcepl'                                                    
c########################################################                       
c      write(iout,*) '###### entro in SFCEPL #######'                           
c++++++++++++++++++  31 agosto   +++++++++++++++++++++++++++++++++++            
c     ho inserito l' ultimo indice [index=2]                                    
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      CALL SFCEPL(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
     1            DV(JEPSI),DV(JD),IV(JICON),NSOLT,NRSOL,5,                     
     *           IV(JIELST),NESEL,DV(JGPQ),DV(JTEMP),2)                         
c      write(iout,*) '###### esco da SFCEPL #######'                            
c##########################################################                     
c     **************************************                                    
      NE=NE+NELP                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.8) THEN                                                   
C                                                                               
      WRITE(IOUT,500) 'STRATIFICATI'                                            
      DO 40,I=1,NELS                                                            
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.8) THEN                                                        
      CALL JPOSRL(ISLELM,(NCON+NCON*4))                                         
      CALL JREAD(ISLELM,NSTRAT,1)                                               
      CALL JREAD(ISLELS,LRECD,1)                                                
      CALL JREAD(ISLELS,NPINTG,1)                                               
      CALL JPOSRL(ISLELM,(12*6+8)*NSTRAT)                                       
      CALL JPOSRL(ISLELS,LRECD*NPINTG*NSTRAT)                                   
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
40    CONTINUE                                                                  
C                                                                               
      NE=NE+NELS                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
      ELSE                                                                      
      WRITE(IOUT,818)ITIP                                                       
 818  FORMAT(/' ***   AVVISO (MODSFT) : NON RICONOSCIUTO ELEMENTO ',            
     *        ' N.RO',I8,'   ***'/)                                             
      END IF                                                                    
      GOTO 6                                                                    
7     CONTINUE                                                                  
C                                                                               
277   IF(DUMP(18)) THEN                                                         
      NE=0                                                                      
C                                                                               
510   FORMAT(//,T30,' **** AVVISO: MODSFT: RICHIESTI ALLUNGAMENTI               
     *TERMICI PER ELEMENTI NON IMPLEMENTATI:',A12,' ***'//)                     
C                                                                               
      CALL PRTEPS(IV(JIELST),DV(JSIGMG),DV(JSIGML),NSOL,DV(JSIGMG),             
     *            DV(JSIGMG),DV(JSIGML),DV(JSIGMF),DV(JGPQ),nsolt)              
      CALL JPOSAB(ISLSFC,1)                                                     
      CALL JPOSAB(ISLELM,1)                                                     
      CALL JPOSAB(ISLELS,1)                                                     
C                                                                               
C                                                                               
256   CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      IF(ITIP.EQ.MPCCOD.OR.ITIP.EQ.SPCCOD) THEN                                 
      JUMP=NCON*4+6                                                             
      CALL JPOSRL(ISLELM,JUMP)                                                  
      NE=NE+1                                                                   
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.1) THEN                                                   
C                                                                               
      WRITE(IOUT,510) ' CORRENTI  '                                             
      DO 50,I=1,NCORR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.1) THEN                                                        
      CALL JPOSRL(ISLELM,11)                                                    
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
50    CONTINUE                                                                  
C                                                                               
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.2) THEN                                                   
C                                                                               
      WRITE(IOUT,510) '  GIUNZIONI  '                                           
      DO 60,I=1,NGIUR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.2) THEN                                                        
      CALL JPOSRL(ISLELM,18)                                                    
      CALL JPOSRL(ISLELS,6)                                                     
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
60    CONTINUE                                                                  
C                                                                               
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.3) THEN                                                   
C                                                                               
      WRITE(IOUT,510) '  PANNELLI  '                                            
      DO 70,I=1,NPANR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.3) THEN                                                        
      CALL JPOSRL(ISLELM,NCON*5+4)                                              
      CALL JREAD(ISLELM,NPINT,1)                                                
      CALL JPOSRL(ISLELM,8)                                                     
      CALL JPOSRL(ISLELS,(4*NCON+12+2*3+6)*NPINT)                               
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
70    CONTINUE                                                                  
C                                                                               
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.4) THEN                                                   
C                                                                               
      CALL EPSEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),4,                                                  
     *            IV(JIELST),NESEL,nsol)                                        
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.5) THEN                                                   
C  ****************************                                                 
c      write(iout,*) ' ###### entro in EPSEPL ######'                           
C                                                                               
      CALL EPSEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),5,                                                  
     *            IV(JIELST),NESEL,nsol)                                        
C  ******************************                                               
c      write(iout,*) ' ******* esco da EPSEPL *******'                          
      NE=NE+NELP                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.8) THEN                                                   
C                                                                               
      WRITE(IOUT,510) 'STRATIFICATI'                                            
      DO 80,I=1,NELSR                                                           
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.8) THEN                                                        
      CALL JPOSRL(ISLELM,NCON*5)                                                
      CALL JREAD(ISLELM,NSTRAT,1)                                               
      CALL JPOSRL(ISLELM,NSTRAT*80)                                             
      CALL JPOSRL(ISLELS,1)                                                     
      CALL JREAD(ISLELS,NPINTG,1)                                               
C     verificare linea seguente                                                 
      CALL JPOSRL(ISLELS,(NPINTG*NSTRAT+5)/6)                                   
      ELSE                                                                      
      CALL  JPOSRL(ISLELM,-4)                                                   
      ENDIF                                                                     
80    CONTINUE                                                                  
C                                                                               
      NE=NE+NELS                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
      END IF                                                                    
      GOTO 256                                                                  
257   CONTINUE                                                                  
C                                                                               
      END IF                                                                    
C                                                                               
 77   IF(DUMP(8)) THEN                                                          
C                                                                               
520   FORMAT(//,T30,' *** AVVISO: MODSFT: RICHIESTA STAMPA DI SFORZO            
     *TERMICO PER ELEMENTO NON IMPLEMENTATO: ',A12,' ***'//)                    
C                                                                               
      NE=0                                                                      
C                                                                               
      CALL PRTSFC(IV(JIELST),DV(JSIGMG),DV(JSIGML),NSOLT,                       
     *              DV(JSIGMG),DV(JSIGMG),DV(JSIGML),                           
     *              DV(JSIGMF),DV(JGPQ))                                        
      CALL JPOSAB(ISLSFC,1)                                                     
      CALL JPOSAB(ISLELM,1)                                                     
      CALL JPOSAB(ISLELS,1)                                                     
C                                                                               
C                                                                               
56    CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      IF(ITIP.EQ.MPCCOD.OR.ITIP.EQ.SPCCOD) THEN                                 
      JUMP=NCON*4+6                                                             
      CALL JPOSRL(ISLELM,JUMP)                                                  
      NE=NE+1                                                                   
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.1) THEN                                                   
C                                                                               
      WRITE(IOUT,520) '  CORRENTI  '                                            
      DO 100,I=1,NCORR                                                          
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.1) THEN                                                        
      CALL JPOSRL(ISLELM,11)                                                    
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
100   CONTINUE                                                                  
C                                                                               
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.2) THEN                                                   
C                                                                               
      WRITE(IOUT,520) '  GIUNZIONI '                                            
      DO 120,I=1,NGIUR                                                          
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.2) THEN                                                        
      CALL JPOSRL(ISLELM,18)                                                    
      CALL JPOSRL(ISLELS,6)                                                     
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
120   CONTINUE                                                                  
C                                                                               
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.3) THEN                                                   
C                                                                               
      WRITE(IOUT,520) '  PANNELLI  '                                            
      DO 130,I=1,NPANR                                                          
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.3) THEN                                                        
      CALL JPOSRL(ISLELM,NCON*5+4)                                              
      CALL JREAD(ISLELM,NPINT,1)                                                
      CALL JPOSRL(ISLELM,8)                                                     
      CALL JPOSRL(ISLELS,(NCON*4+12+2*3+6)*NPINT)                               
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
130   CONTINUE                                                                  
C                                                                               
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.4) THEN                                                   
C                                                                               
      CALL PRTEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),4,                                                  
     *            IV(JIELST),NESEL,nsolt)                                       
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.5) THEN                                                   
C                                                                               
C  ****************************                                                 
      CALL PRTEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),5,                                                  
     *            IV(JIELST),NESEL,nsolt)                                       
C  ******************************                                               
      NE=NE+NELP                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
      ELSE IF(ITIP.EQ.8) THEN                                                   
C                                                                               
      WRITE(IOUT,520) 'STRATIFICATI'                                            
      DO 140,I=1,NELSR                                                          
      CALL JREAD(ISLELM,ITIP,4)                                                 
      IF(ITIP.EQ.8) THEN                                                        
      CALL JPOSRL(ISLELM,NCON*5)                                                
      CALL JREAD(ISLELM,NSTRAT,1)                                               
      CALL JPOSRL(ISLELM,NSTRAT*80)                                             
      CALL JPOSRL(ISLELS,1)                                                     
      CALL JREAD(ISLELS,NPINTG,1)                                               
      CALL JPOSRL(ISLELS,(NPINTG*NSTRAT+5)/6)                                   
      ELSE                                                                      
      CALL JPOSRL(ISLELM,-4)                                                    
      ENDIF                                                                     
140   CONTINUE                                                                  
C                                                                               
      NE=NE+NELS                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
      END IF                                                                    
      GOTO 56                                                                   
57    CONTINUE                                                                  
C                                                                               
      END IF                                                                    
C                                                                               
      CALL JCLOSE(ISLELS)                                                       
      CALL JCLOSE(ISLELM)                                                       
      CALL JCLOSE(ISLSFC)                                                       
      write(*,480)                                                              
480   format(' ****    FINE FASE CALCOLO SFORZI TERMICI   ***')                 
      RETURN                                                                    
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C                                                                               

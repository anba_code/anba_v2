C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.ADDT                                                          
C*************************           ADDT           ********************        
      SUBROUTINE ADDT  (A,NRDA,B,NRDB,NRA,NCA,IB,COE)                           
C                                                                               
      REAL*8 A,B,COE                                                            
      DIMENSION A(NRDA,1),B(NRDB,1)                                             
      IGO=IB+1                                                                  
      GOTO(10,20),IGO                                                           
10    DO 11 I=1,NRA                                                             
      DO 11 K=1,NCA                                                             
      A(I,K)=A(I,K)+COE*B(I,K)                                                  
11    CONTINUE                                                                  
      RETURN                                                                    
20    DO 12 I=1,NRA                                                             
      DO 12 K=1,NCA                                                             
      A(I,K)=A(I,K)+COE*B(K,I)                                                  
12    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.AUVALR                                                        
C*************************          AUVALR          ********************        
      SUBROUTINE AUVALR(RMAT,RK,RR,RI,NEQ,NEQ2,IANA,ISLTMP,PERM)                
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP,INCORE                                                       
      COMPLEX*16 AU,AD,AS                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV                
      DIMENSION RMAT(NEQ2,1),RK(NEQ,1),RR(1),RI(1),IANA(1),PERM(1)              
      CALL DZERO(RMAT,NEQ2*NEQ2)                                                
      DO 10 I=1,NEQ                                                             
      RMAT(I,NEQ+I)=1.D0                                                        
 10   CONTINUE                                                                  
      CALL JREAD(ISLTMP,RK,NEQ*NEQ*2)                                           
      DO 50 I=1,NEQ                                                             
      CALL JREAD(ISLTMP,RMAT(NEQ+1,NEQ+I),NEQ*2)                                
 50   CONTINUE                                                                  
      DO 51 I=1,NEQ                                                             
      CALL JREAD(ISLTMP,RMAT(NEQ+1,I),NEQ*2)                                    
 51   CONTINUE                                                                  
      CALL ADDT(RK,NEQ,RMAT(NEQ+1,1),NEQ2,NEQ,NEQ,0,-SHIFT*SHIFT)               
      CALL ADDT(RK,NEQ,RMAT(NEQ+1,NEQ+1),NEQ2,NEQ,NEQ,0,-SHIFT)                 
      CALL ADDT(RMAT(NEQ+1,NEQ+1),NEQ2,RMAT(NEQ+1,1),NEQ2,NEQ,NEQ,0,            
     *          2.D0*SHIFT)                                                     
       IF(DUMP(5)) THEN                                                         
          CALL DWRITE('CAPP',RK,NEQ,NEQ,NEQ,1)                                  
          CALL DWRITE('MASS',RMAT(NEQ+1,1),NEQ2,NEQ,NEQ,1)                      
          CALL DWRITE('DUMP',RMAT(NEQ+1,NEQ+1),NEQ2,NEQ,NEQ,1)                  
       END IF                                                                   
      CALL RUFCT(RK,PERM,NEQ,NEQ,INDER)                                         
      IF(INDER.NE.0) WRITE(IOUT,881)INDER                                       
 881  FORMAT(' ***   AVVISO (AUVALR): CODICE ERRORE RUFCT =',I8,'  ***')        
      DO 30 I=1,NEQ2                                                            
      CALL RUSOL(RK,RMAT(NEQ+1,I),NEQ,NEQ,PERM)                                 
 30   CONTINUE                                                                  
      CALL JPOSAB(ISLTMP,1)                                                     
      NWORD=NEQ2*NEQ2*2                                                         
      CALL JWRITE(ISLTMP,RMAT,NWORD)                                            
      CALL DHSN(RMAT,NEQ2,NEQ2)                                                 
      CALL DQR2(RMAT,RR,RI,IANA,NEQ2,NEQ2)                                      
      WRITE(IOUT,100)                                                           
      DO 41 I=1,NEQ2                                                            
       AU=CMPLX(RR(I),RI(I))                                                    
       AD=(1.D0,0.D0)+SHIFT*AU                                                  
       AS=AU/AD                                                                 
       RR(I)= DBLE(AS)                                                          
       RI(I)=DIMAG(AS)                                                          
 41   CONTINUE                                                                  
      CALL AVLSRT(NEQ2,RR,RI)                                                   
      DO 42 I=1,NEQ2                                                            
       RSR=RR(I)                                                                
       RSI=RI(I)                                                                
       RMOD=DSQRT(RSR*RSR+RSI*RSI)                                              
       FASE=1.570796327D0                                                       
       IF(RSR.NE.0.D0)FASE=DATAN2(RSI,RSR)                                      
       FASE=FASE*57.29577951D0                                                  
       WRITE(IOUT,110)I,RSR,RSI,RMOD,FASE                                       
 42   CONTINUE                                                                  
 100  FORMAT(/////40X,'  AUTOVALORI DEL SISTEMA'///11X,'N.RO',10X,'P. R'        
     1  ,'EALE',12X,'P. IMMAG',12X,'  MODULO',9X,'   FASE'/)                    
 110  FORMAT(2X,I10,4E18.8)                                                     
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.AUVLAS                                                        
C*************************          AUVLAS          ********************        
      SUBROUTINE AUVASK(RM,IPIAZ,ELMAT,NA,NRELMT)                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      INTEGER SPCCOD                                                            
      LOGICAL DUMP,INCORE                                                       
      DIMENSION RM(1),IPIAZ(1),ELMAT(NRELMT,1),NA(1)                            
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1                NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                 
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2          
      COMMON /ELGEN/  ITIP,NTERM,NT1,IFLMR                                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO                     
      DATA MPCCOD/6/,SPCCOD/7/                                                  
C                                                                               
          CALL JPOSAB(ISLVPZ,1)                                                 
C                                                                               
      DO 300 K=1,NELEM                                                          
C                                                                               
      CALL JREAD(ISLVPZ,ITIP,3)                                                 
      IF(ITIP.EQ.MPCCOD) GOTO 500                                               
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      NTERM=NTERM-1                                                             
C                                                                               
      DO 310 I=1,NTERM                                                          
      CALL JREAD(ISLVPZ,ELMAT(1,I),NT1*2)                                       
 310  CONTINUE                                                                  
      CALL FEMADS(RM,NA,ELMAT,IPIAZ,NTERM,NRELMT)                               
C                                                                               
C                                                                               
 300  CONTINUE                                                                  
C                                                                               
      DO 400 IL=1,NPSI                                                          
      IF(IPGCON(IL).EQ.0) GOTO 400                                              
      IN=NEQV-NPSI+IL                                                           
      IN=NA(IN)                                                                 
      RM(IN)=RM(IN)+CONST                                                       
 400  CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
 500  WRITE(IOUT,600)                                                           
 600  FORMAT(' *** AVVISO (AUVLAS): NON PREVISTI MPC NEL CALCOLO '              
     1  ,'DELLE SOLUZIONI DI ESTREMITA'' ***')                                  
      CALL JHALT                                                                
C                                                                               
C                                                                               
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY AUVASD(RM,IPIAZ,ELMAT,NA,NRELMT)                                    
          CALL JPOSAB(ISLVPZ,1)                                                 
          CALL JPOSAB(ISLMTN,1)                                                 
      DO 311 K=1,NELEM                                                          
C                                                                               
      CALL JREAD(ISLVPZ,ITIP,3)                                                 
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      NTERM=NTERM-1                                                             
C                                                                               
      CALL JPOSRL(ISLVPZ,NTERM*NT1*2)                                           
      IF(ITIP.EQ.SPCCOD) GOTO 311                                               
C                                                                               
      DO 321 I=1,NTERM                                                          
      CALL JREAD(ISLMTN,ELMAT(1,I),NT1*2)                                       
 321  CONTINUE                                                                  
      CALL FEMADA(RM,NA,ELMAT,IPIAZ,NTERM,NRELMT)                               
C                                                                               
C                                                                               
 311  CONTINUE                                                                  
          RETURN                                                                
C                                                                               
      ENTRY AUVASM(RM,IPIAZ,ELMAT,NA,NRELMT)                                    
          CALL JPOSAB(ISLVPZ,1)                                                 
          CALL JPOSAB(ISLMTF,1)                                                 
C                                                                               
      DO 322 K=1,NELEM                                                          
C                                                                               
      CALL JREAD(ISLVPZ,ITIP,3)                                                 
c     write(iout,*)itip,nterm,nt1                                               
      CALL JREAD(ISLVPZ,IPIAZ,NTERM)                                            
      NTERM=NTERM-1                                                             
C                                                                               
      CALL JPOSRL(ISLVPZ,NTERM*NT1*2)                                           
      IF(ITIP.EQ.SPCCOD) GOTO 322                                               
C                                                                               
C                                                                               
      CALL JPOSRL(ISLMTF,NTERM*(NTERM-NPSI)*2)                                  
      DO 332 I=1,NTERM-NPSI                                                     
      CALL JREAD(ISLMTF,ELMAT(1,I),(NTERM-NPSI)*2)                              
 332  CONTINUE                                                                  
      CALL FEMADS(RM,NA,ELMAT,IPIAZ,(NTERM-NPSI),NRELMT)                        
C                                                                               
 322  CONTINUE                                                                  
C                                                                               
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.AUVETM                                                        
C*************************          AUVETM          ********************        
      SUBROUTINE AUVETM(CEIGEN,ND,NEQ,RR,RI,AUTVET,PERM,ISLTMP,ISLSLE)          
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMPLEX*16 CEIGEN(ND,1),AUTVET(NEQ),CTEMP                                 
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      DIMENSION PERM(NEQ),RI(NEQ),RR(NEQ)                                       
      PRINT*,' AUVETM NEQ=',NEQ                                                 
      CALL JWRITE(ISLSLE,NEQ,1)                                                 
      DO 29 KS=1,NEQ                                                            
       CALL JWRITE(ISLSLE,RR(KS),2)                                             
       CALL JWRITE(ISLSLE,RI(KS),2)                                             
       CALL RANDOM(RAND,1)                                                      
       RR(KS)=RR(KS)*(1.0+RAND*1.E-6)                                           
       CALL RANDOM(RAND,1)                                                      
       RI(KS)=RI(KS)*(1.0+RAND*1.E-6)                                           
       CALL JPOSAB(ISLTMP,1)                                                    
       DO 10 J=1,NEQ                                                            
        CALL JREAD(ISLTMP,PERM,NEQ*2)                                           
        DO 10 I=1,NEQ                                                           
         CEIGEN(I,J)=PERM(I)                                                    
 10    CONTINUE                                                                 
       DO 31 I=1,NEQ                                                            
        AUTVET(I)=(1.D0,0.D0)                                                   
        DO 32 J=1,NEQ                                                           
         IF(I.EQ.J)CEIGEN(I,I)=CEIGEN(I,I)-CMPLX(RR(Ks),RI(Ks))                 
32      CONTINUE                                                                
31     CONTINUE                                                                 
       CALL DCUFCT(CEIGEN,PERM,NEQ,ND,INDER)                                    
       IF(INDER.EQ.1) CALL JHALT                                                
       CALL DCUSOL(CEIGEN,AUTVET,ND,NEQ,PERM)                                   
       CALL DCUSOL(CEIGEN,AUTVET,ND,NEQ,PERM)                                   
       CTEMP=(0.D0,0.D0)                                                        
       DO 33 I=1,NEQ                                                            
        CTEMP=CTEMP+AUTVET(I)*AUTVET(I)                                         
33     CONTINUE                                                                 
       CTEMP=CDSQRT(CTEMP)                                                      
       DO 34 I=1,NEQ                                                            
        AUTVET(I)=AUTVET(I)/CTEMP                                               
34     CONTINUE                                                                 
C                                                                               
      CALL JWRITE(ISLSLE,AUTVET,NEQ*4)                                          
      CALL JPOSRL(ISLSLE,24)                                                    
      CALL JPOSRL(ISLSLE,NEQ*4)                                                 
 29   CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.AUTVET                                                        
C*************************          AUTVET          ********************        
C     COMPILER (ARGCHK=OFF)                                                     
      SUBROUTINE AUTVET(A,B,C,W,VET,IN,IANA,RR,RI,NVEC,N,NRVET,VC,NEQ)          
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL FIRST,IN(1),DUMP,INCORE                                           
      COMPLEX*16 VET(1),W(N,1),MULT,T,S,RMAX                                    
      DIMENSION A(1),B(1),C(1),RR(1),RI(1),VC(NEQ,1),IANA(1)                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSL2          
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1             NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                    
      DATA RAD/57.29577951D0/                                                   
      CALL INTEST                                                               
      NEQD2=(NEQ-1)/2                                                           
      WRITE(IOUT,100)                                                           
      DO 42 I=1,NVEC                                                            
      RMOD=DSQRT(RR(I)*RR(I)+RI(I)*RI(I))                                       
      FASE=1.570796327D0                                                        
      IF(RR(I).NE.0.D0)FASE=DATAN2(RI(I),RR(I))                                 
      FASE=FASE*RAD                                                             
          RA=RR(I)/(RMOD*RMOD)                                                  
          RB=-RI(I)/(RMOD*RMOD)                                                 
      WRITE(IOUT,110)I,RR(I),RI(I),RMOD,FASE,RA,RB,IANA(I)                      
 42   CONTINUE                                                                  
      NM1=N-1                                                                   
      DO10I=1,NM1                                                               
      C(I)=C(I+1)                                                               
10    CONTINUE                                                                  
      DO82I=1,NVEC                                                              
      DO44J=1,N                                                                 
       W(J,2)=(0.,0.)                                                           
      W(J,3)=B(J)                                                               
      W(J,4)=DCMPLX(A(J)-RR(I),-RI(I))                                          
      W(J,5)=(1.,0.)                                                            
44    CONTINUE                                                                  
      W(N+1,5)=(0.,0.)                                                          
      W(N+2,5)=(0.,0.)                                                          
      FIRST=.TRUE.                                                              
      DO50J=1,NM1                                                               
       IF(CDABS(W(J,4)).LT.DABS(C(J)))GOTO46                                    
      MULT=C(J)/W(J,4)                                                          
      IN(J)=.FALSE.                                                             
      GOTO48                                                                    
46    MULT=W(J,4)/C(J)                                                          
      IN(J)=.TRUE.                                                              
      W(J,4)=C(J)                                                               
      T=W(J+1,4)                                                                
      W(J+1,4)=W(J,3)                                                           
      W(J,3)=T                                                                  
      W(J,2)=W(J+1,3)                                                           
      W(J+1,3)=(0.,0.)                                                          
48    W(J,1)=MULT                                                               
      W(J+1,3)=W(J+1,3)-MULT*W(J,2)                                             
      W(J+1,4)=W(J+1,4)-MULT*W(J,3)                                             
      IF(CDABS(W(J,4)).EQ.0.)W(J,4)=(1.E-15,1.E-15)                             
50    CONTINUE                                                                  
      IF(CDABS(W(N,4)).EQ.0.)W(N,4)=(1.E-15,1.E-15)                             
54    CALL OVERFL(II)                                                           
      DO66JI=1,N                                                                
      K=N-JI+1                                                                  
      T=W(K,5)                                                                  
62    W(K,5)=(T-W(K+1,5)*W(K,3)-W(K+2,5)*W(K,2))/W(K,4)                         
      CALL OVERFL(II)                                                           
      IF(II.NE.1)GOTO66                                                         
      DO64J=1,N                                                                 
64    W(J,5)=W(J,5)*1.E-5                                                       
      T=T*1.E-5                                                                 
      GOTO62                                                                    
66    CONTINUE                                                                  
      IF(.NOT.FIRST)GOTO74                                                      
       FIRST=.FALSE.                                                            
       DO70J=1,NM1                                                              
      IF(IN(J))GOTO68                                                           
      W(J+1,5)=W(J+1,5)-W(J,1)*W(J,5)                                           
       GOTO70                                                                   
68    T=W(J,5)                                                                  
      W(J,5)=W(J+1,5)                                                           
      W(J+1,5)=T-W(J,1)*W(J+1,5)                                                
70    CONTINUE                                                                  
      GOTO54                                                                    
 74   CONTINUE                                                                  
      RMAX=(0.D0,0.D0)                                                          
      DO 101 J=1,NEQD2                                                          
      S=(0.D0,0.D0)                                                             
      DO 102 K=1,NRVET                                                          
      S=S+VC((J+NEQD2),K)*W(K,5)                                                
 102  CONTINUE                                                                  
      IF(CDABS(S).GT.CDABS(RMAX)) RMAX=S                                        
      VET(J)=S                                                                  
 101  CONTINUE                                                                  
      RMAX=(1.D0,0.D0)/RMAX                                                     
      DO 103 J=1,NEQD2                                                          
      VET(J)=VET(J)*RMAX                                                        
 103  CONTINUE                                                                  
      CALL JWRITE(ISLSLE,RR(I),2)                                               
      CALL JWRITE(ISLSLE,RI(I),2)                                               
      CALL JWRITE(ISLSLE,VET,NEQD2*4)                                           
      CALL JPOSRL(ISLSLE,NEQD2*4)                                               
82    CONTINUE                                                                  
 100  FORMAT(/////40X,'  AUTOVALORI DEL SISTEMA'///11X,'N.RO',10X,'P. R'        
     1  ,'EALE',6X,'P. IMMAG',6X,'  MODULO',9X,'   FASE',26X,'IANA'/)           
 110  FORMAT(5X,I10,6E14.6,I10)                                                 
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.AVLSRT                                                        
C*************************          AVLSRT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE AVLSRT(NT,AVLR,AVLI)                                           
C                                                                               
C     ROUTINE DI ORDINAMENTO DEGLI AUTOVALORI PER MODULO DECRESCENTE            
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION AVLR(*),AVLI(*)                                                 
      M=NT                                                                      
10    M=M/2                                                                     
      IF(M.EQ.0)RETURN                                                          
      K=NT-M                                                                    
      J=1                                                                       
20    I=J                                                                       
30    N=I+M                                                                     
      IF(RMOD(AVLR(I),AVLI(I)).GE.RMOD(AVLR(N),AVLI(N)))   GOTO 40              
      R=AVLR(I)                                                                 
      AVLR(I)=AVLR(N)                                                           
      AVLR(N)=R                                                                 
      R=AVLI(I)                                                                 
      AVLI(I)=AVLI(N)                                                           
      AVLI(N)=R                                                                 
1     I=I-M                                                                     
      IF(I.GE.1)GOTO 30                                                         
40    J=J+1                                                                     
      IF(J.GT.K) GOTO 10                                                        
      GOTO 20                                                                   
      END                                                                       
C@ELT,I ANBA*ANBA.DHSN                                                          
C*************************          DHSN          **********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DHSN(A,N,NR)                                                   
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(NR,1)                                                         
      DOUBLE PRECISION S                                                        
      IF(N.LE.2)RETURN                                                          
      NM2=N-2                                                                   
      DO15K=1,NM2                                                               
      KP1=K+1                                                                   
      KP2=K+2                                                                   
      ABIG=ABS(A(KP1,K))                                                        
      J=KP1                                                                     
      DO5 I=KP2,N                                                               
      TEMP=ABS(A(I,K))                                                          
      IF(TEMP.LE.ABIG)GOTO5                                                     
      J=I                                                                       
      ABIG=TEMP                                                                 
 5    CONTINUE                                                                  
      IF(ABIG.EQ.0.)GOTO15                                                      
      IF(J.EQ.KP1)GOTO8                                                         
      DO6I=K,N                                                                  
      TEMP=A(KP1,I)                                                             
      A(KP1,I)=A(J,I)                                                           
      A(J,I)=TEMP                                                               
6     CONTINUE                                                                  
      DO7I=1,N                                                                  
      TEMP=A(I,KP1)                                                             
      A(I,KP1)=A(I,J)                                                           
      A(I,J)=TEMP                                                               
7     CONTINUE                                                                  
8     DO9I=KP2,N                                                                
      A(I,K)=A(I,K)/A(KP1,K)                                                    
9     CONTINUE                                                                  
      DO11I=1,KP1                                                               
      S=0.                                                                      
      DO10J=KP2,N                                                               
      S=S+A(I,J)*A(J,K)                                                         
10    CONTINUE                                                                  
      A(I,KP1)=A(I,KP1)+S                                                       
11    CONTINUE                                                                  
      DO13I=KP2,N                                                               
      S=0.                                                                      
      DO12J=KP2,N                                                               
      S=S+A(I,J)*A(J,K)                                                         
12    CONTINUE                                                                  
      A(I,KP1)=A(I,KP1)+S-A(I,K)*A(KP1,KP1)                                     
13    CONTINUE                                                                  
      DO14I=KP2,N                                                               
      DO14J=KP2,N                                                               
      A(I,J)=A(I,J)-A(I,K)*A(KP1,J)                                             
14    CONTINUE                                                                  
15    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.DQR2                                                          
C*************************          DQR2          **********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DQR2(A,RR,RI,IANA,M,NRA)                                       
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(NRA,1),RR(1),RI(1),PRR(2),PRI(2),IANA(1)                      
      INTEGER P,P1,Q                                                            
      E7=1.E-7                                                                  
      E6=1.E-6                                                                  
      E10=1.E-10                                                                
      DELTA=0.50                                                                
      MAXIT=30                                                                  
      N=M                                                                       
20    N1=N-1                                                                    
      IF(N1.EQ.0)GOTO1300                                                       
30    NP=N+1                                                                    
      IT=0                                                                      
      DO40I=1,2                                                                 
      PRR(I)=0.0                                                                
40    PRI(I)=0.0                                                                
      PAN=0.0                                                                   
      PAN1=0.0                                                                  
      R=0.0                                                                     
      S=0.0                                                                     
      N2=N1-1                                                                   
60    T=A(N1,N1)-A(N,N)                                                         
      U=T*T                                                                     
      V=4.0*A(N1,N)*A(N,N1)                                                     
      IF(ABS(V).LE.U*E7)GOTO100                                                 
      T=U+V                                                                     
      IF(DABS(T).GT.DMAX1(U,DABS(V))*E6)GOTO68                                  
      T=0.0                                                                     
68    U=(A(N1,N1)+A(N,N))*0.50                                                  
      V=DSQRT(ABS(T))*0.50                                                      
      IF(T.LT.0.0)GOTO140                                                       
      IF(U.LT.0.0)GOTO80                                                        
      RR(N1)=U+V                                                                
      RR(N)=U-V                                                                 
      GOTO130                                                                   
80    RR(N1)=U-V                                                                
      RR(N)=U+V                                                                 
      GOTO130                                                                   
100   IF(T.LT.0.0)GOTO120                                                       
      RR(N1)=A(N1,N1)                                                           
      RR(N)=A(N,N)                                                              
      GOTO130                                                                   
120   RR(N1)=A(N,N)                                                             
      RR(N)=A(N1,N1)                                                            
130   RI(N)=0.0                                                                 
      RI(N1)=0.0                                                                
      GOTO160                                                                   
140   RR(N1)=U                                                                  
      RR(N)=U                                                                   
      RI(N1)=V                                                                  
      RI(N)=-V                                                                  
160   IF(N2.LE.0)GOTO1280                                                       
180   RMOD=RR(N1)*RR(N1)+RI(N1)*RI(N1)                                          
      EPS=E10*DSQRT(RMOD)                                                       
      IF(ABS(A(N1,N2)).LE.EPS)GOTO1280                                          
      IF(ABS(A(N,N1)).LE.E10*ABS(A(N,N)))GOTO1300                               
      IF(ABS(PAN1-A(N1,N2)).LE.ABS(A(N1,N2))*E6)GOTO1240                        
      IF(ABS(PAN-A(N,N1)).LE.ABS(A(N,N1))*E6)GOTO1240                           
      IF(IT.GE.MAXIT)GOTO1240                                                   
      J=1                                                                       
      DO360I=1,2                                                                
      K=NP-I                                                                    
      IF(ABS(RR(K)-PRR(I))+ABS(RI(K)-PRI(I))-DELTA*(ABS(RR(K))+ABS(RI(K)        
     *)))340,360,360                                                            
340   J=J+I                                                                     
360   CONTINUE                                                                  
      GOTO(440,460,460,480),J                                                   
440   R=0.0                                                                     
      S=0.0                                                                     
      GOTO500                                                                   
460   J=N+2-J                                                                   
      R=RR(J)*RR(J)                                                             
      S=RR(J)+RR(J)                                                             
      GOTO500                                                                   
480   R=RR(N)*RR(N1)-RI(N)*RI(N1)                                               
      S=RR(N)+RR(N1)                                                            
500   PAN=A(N,N1)                                                               
      PAN1=A(N1,N2)                                                             
      DO520I=1,2                                                                
      K=NP-I                                                                    
      PRR(I)=RR(K)                                                              
520   PRI(I)=RI(K)                                                              
      P=N2                                                                      
      IF(N.LE.3)GOTO600                                                         
525   DO580K=2,N2                                                               
      J=N2+2-K                                                                  
      IF(ABS(A(J,J-1)).LE.EPS)GOTO600                                           
      D=A(J,J)*(A(J,J)-S)+A(J,J+1)*A(J+1,J)+R                                   
      IF(D.EQ.0.0)GOTO560                                                       
      IF(ABS(A(J,J-1)*A(J+1,J))*(ABS(A(J,J)+A(J+1,J+1)-S)+ABS(A(J+2,J+1)        
     *))-ABS(D)*EPS)620,620,560                                                 
560   P=N1-K                                                                    
580   CONTINUE                                                                  
600   Q=P                                                                       
      GOTO680                                                                   
620   P1=P-1                                                                    
      Q=P1                                                                      
      IF(P1.LE.1)GOTO680                                                        
      DO660K=2,P1                                                               
      I=J+1-K                                                                   
      IF(ABS(A(I,I-1)).LE.EPS)GOTO680                                           
660   Q=Q-1                                                                     
680   DO1220I=P,N1                                                              
      IF(I.NE.P)GOTO720                                                         
      G1=A(P,P)*(A(P,P)-S)+A(P,P+1)*A(P+1,P)+R                                  
      G2=A(P+1,P)*(A(P,P)+A(P+1,P+1)-S)                                         
      G3=A(P+1,P)*A(P+2,P+1)                                                    
      A(P+2,P)=0.0                                                              
      GOTO780                                                                   
720   G1=A(I,I-1)                                                               
      G2=A(I+1,I-1)                                                             
      IF(I.GT.N2)GOTO760                                                        
      G3=A(I+2,I-1)                                                             
      GOTO780                                                                   
760   G3=0.0                                                                    
780   CAP=DSQRT(G1*G1+G2*G2+G3*G3)                                              
      IF(CAP.EQ.0.0)GOTO860                                                     
      IF(G1.GE.0.0)GOTO840                                                      
      CAP=-CAP                                                                  
840   T=G1+CAP                                                                  
      PSI1=G2/T                                                                 
      PSI2=G3/T                                                                 
      ALPHA=2.0/(1.0+PSI1*PSI1+PSI2*PSI2)                                       
      GOTO880                                                                   
860   ALPHA=2.0                                                                 
      PSI1=0.0                                                                  
      PSI2=0.0                                                                  
880   IF(I.EQ.Q)GOTO960                                                         
      IF(I.EQ.P)GOTO940                                                         
      A(I,I-1)=-CAP                                                             
      GOTO960                                                                   
940   A(I,I-1)=-A(I,I-1)                                                        
960   DO1040J=I,N                                                               
      T=PSI1*A(I+1,J)                                                           
      IF(I.GE.N1)GOTO1000                                                       
      T=T+PSI2*A(I+2,J)                                                         
1000  ETA=ALPHA*(T+A(I,J))                                                      
      A(I,J)=A(I,J)-ETA                                                         
      A(I+1,J)=A(I+1,J)-PSI1*ETA                                                
      IF(I.GE.N1)GOTO1040                                                       
1020  A(I+2,J)=A(I+2,J)-PSI2*ETA                                                
1040  CONTINUE                                                                  
      IF(I.LT.N1)GOTO1080                                                       
      K=N                                                                       
      GOTO1100                                                                  
1080  K=I+2                                                                     
1100  DO1180J=Q,K                                                               
      T=PSI1*A(J,I+1)                                                           
      IF(I.GE.N1)GOTO1140                                                       
      T=T+PSI2*A(J,I+2)                                                         
1140  ETA=ALPHA*(T+A(J,I))                                                      
      A(J,I)=A(J,I)-ETA                                                         
      A(J,I+1)=A(J,I+1)-ETA*PSI1                                                
      IF(I.GE.N1)GOTO1180                                                       
      A(J,I+2)=A(J,I+2)-ETA*PSI2                                                
1180  CONTINUE                                                                  
      IF(I.GE.N2)GOTO1220                                                       
      ETA=ALPHA*PSI2*A(I+3,I+2)                                                 
      A(I+3,I)=-ETA                                                             
      A(I+3,I+1)=-PSI1*ETA                                                      
      A(I+3,I+2)=A(I+3,I+2)-PSI2*ETA                                            
1220  CONTINUE                                                                  
      IT=IT+1                                                                   
      GOTO60                                                                    
1240  IF(ABS(A(N,N1)).LT.ABS(A(N1,N2)))GOTO1300                                 
1280   IANA(N)=0                                                                
      IANA(N1)=2                                                                
      N=N2                                                                      
      IF(N2.LE.0)RETURN                                                         
      GOTO20                                                                    
1300  RR(N)=A(N,N)                                                              
      RI(N)=0.0                                                                 
      IANA(N)=1                                                                 
      IF(N1.LE.0)RETURN                                                         
1320  N=N1                                                                      
      GOTO20                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.FEMADA                                                        
C*************************          FEMADA          ********************        
      SUBROUTINE FEMADA(A,NA,S,LM,ND,NRS)                                       
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP,INCORE                                                       
      DIMENSION A(1),NA(1),S(NRS,1),LM(1)                                       
      COMMON/JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                       
      DO 20 I=1,ND                                                              
      II=LM(I)                                                                  
      IF(II.LE.0)GOTO20                                                         
      DO 30 J=I,ND                                                              
      JJ=LM(J)                                                                  
      IF(JJ.LE.0)GOTO30                                                         
      IJ=II-JJ                                                                  
      IF(IJ.LT.0)GOTO40                                                         
      JJ=II                                                                     
 40   KK=NA(JJ)                                                                 
      IF(IJ.LT.0)GOTO 45                                                        
      KK=KK-IJ                                                                  
      A(KK)=A(KK)-S(I,J)                                                        
      GOTO 30                                                                   
 45   KK=KK+IJ                                                                  
      A(KK)=A(KK)+S(I,J)                                                        
30    CONTINUE                                                                  
20    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.LANCZO                                                        
C*************************          LANCZO          ********************        
      SUBROUTINE LANCZO(RK,VC,VL,A,B,C,N,NA,TEMP,TEMP1,NMAX,VOLD,               
     *                  NRVC,NRVL,Z,NRZ,RR,RI,IANA,NREQ,VATT,ISLMAT,NW)         
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP,INDCNV,INCORE                                                
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV                
      DIMENSION A(1),B(1),C(1),VC(NRVC,1),VL(NRVL,1),RK(1),                     
     1  VATT(1),VOLD(1),NA(1),TEMP(1),TEMP1(1),RR(1),RI(1),IANA(1),Z(1)         
      CALL DZERO(VOLD,NMAX)                                                     
      CALL DZERO(VATT,NMAX)                                                     
      NAUTIN=NAUTOV                                                             
      NITER=1                                                                   
      LINF=1                                                                    
      LSUP=NREQ                                                                 
 45   CONTINUE                                                                  
      DO 10 I=LINF,LSUP                                                         
      CALL ORTSCL(VC,VL,I,C,N,NRVC,NRVL,TEMP,RK,NA,TEMP1,ORTER,                 
     1            ZERTOL,ISLMAT,NW)                                             
      CALL MULV(RK,VC(1,I),TEMP,N,NA,ISLMAT,NW)                                 
      T=SCALAR(VL(1,I),TEMP,N)                                                  
      S=VL(N+1,I)                                                               
      A(I)=T/S                                                                  
      IF(I.EQ.1)GOTO15                                                          
      T=VL(N+1,I-1)                                                             
      B(I-1)=SCALAR(VL(1,I-1),TEMP,N)/T                                         
      BASTER=C(I)*S/T                                                           
 15   CONTINUE                                                                  
      CALL SUBV(A(I),VC(1,I),TEMP,N)                                            
      IF(I.EQ.1)GOTO20                                                          
      CALL SUBV(B(I-1),VC(1,I-1),TEMP,N)                                        
20    IF(I.EQ.NMAX)GOTO30                                                       
      CALL COPV(TEMP,VC(1,I+1),N)                                               
 30   CONTINUE                                                                  
      CALL MULVT(RK,VL(1,I),TEMP,N,NA,TEMP1,ISLMAT,NW)                          
      CALL SUBV(A(I),VL(1,I),TEMP,N)                                            
      IF(I.EQ.1)GOTO40                                                          
      CALL SUBV(BASTER,VL(1,I-1),TEMP,N)                                        
40    IF(I.EQ.NMAX)GOTO10                                                       
      CALL COPV(TEMP,VL(1,I+1),N)                                               
10    CONTINUE                                                                  
 120  CONTINUE                                                                  
      CALL QR2TRD(A,B,C,NREQ,RR,RI,Z,NRZ,IANA)                                  
      CALL ORDINA(VATT,RR,RI,IANA,NREQ)                                         
      INDCNV=.TRUE.                                                             
      DO 50 I=1,NREQ                                                            
      RMUL=1.D0                                                                 
      IF(DABS(VATT(I)-DABS(VOLD(I))).LT.TOLL) GOTO 55                           
      IF(I.LE.NAUTIN) INDCNV=.FALSE.                                            
      RMUL=-1.D0                                                                
 55   VOLD(I)=VATT(I)*RMUL                                                      
C     IF(RMUL.GT.0.D0.AND.I.GT.NAUTIN)NAUTOV=NAUTOV+1                           
 50   CONTINUE                                                                  
      IF(INDCNV) GOTO 60                                                        
      IF(LSUP.EQ.NMAX)GOTO 60                                                   
      LINF=LSUP+1                                                               
      LSUP=LSUP+INCR                                                            
      IF(LSUP.GT.NMAX)LSUP=NMAX                                                 
      NREQ=LSUP                                                                 
      NITER=NITER+1                                                             
      GOTO 45                                                                   
 60   CONTINUE                                                                  
c     WRITE(IOUT,100)                                                           
c     DO 42 I=1,LSUP                                                            
c     RMOD=DSQRT(RR(I)*RR(I)+RI(I)*RI(I))                                       
c     FASE=1.570796327D0                                                        
c     IF(RR(I).NE.0.D0)FASE=DATAN2(RI(I),RR(I))                                 
c     FASE=FASE*57.297579                                                       
c     WRITE(IOUT,110)I,RR(I),RI(I),RMOD,FASE,IANA(I)                            
c42   CONTINUE                                                                  
 100  FORMAT(/////10X,'  AUTOVALORI DEL SISTEMA'///11X,'N.RO',10X,'P. R'        
     1  ,'EALE',12X,'P. IMMAG',12X,'  MODULO',9X,'   FASE',10X,'IANA'/)         
 110  FORMAT(5X,I10,4E20.8,I10)                                                 
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MATDMP                                                        
C*************************          MATDMP          ********************        
      SUBROUTINE MATDMP(RMAT,RM,NA,NEQV,IFL,RMUL,IDES)                          
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
          CHARACTER*4 IDES                                                      
      LOGICAL DUMP,INCORE                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLMRG,ISLTEM,ISLTN2,ISLSL2          
      DIMENSION RMAT(NEQV,1),RM(1),NA(1)                                        
      CALL DZERO(RMAT,NEQV*NEQV)                                                
      IF(IFL.EQ.1)GOTO 80                                                       
      RMAT(1,1)=RM(1)                                                           
      DO 20 I=2,NEQV                                                            
      ICOL=I                                                                    
      IR1=NA(I-1)+1                                                             
      IR2=NA(I)                                                                 
      IRIGA=ICOL+IR1-IR2                                                        
      DO 25 K=IR1,IR2-1                                                         
      RMAT(IRIGA,ICOL)=RM(K)                                                    
      RMAT(ICOL,IRIGA)=RMUL*RM(K)                                               
      IRIGA=IRIGA+1                                                             
 25   CONTINUE                                                                  
      RMAT(IRIGA,ICOL)=RM(IR2)                                                  
 20   CONTINUE                                                                  
          GOTO 99                                                               
80        DO 30 I=1,NEQV                                                        
          CALL JREAD(ISLASM,NTERM,1)                                            
          CALL JREAD(ISLASM,NA,NTERM)                                           
          CALL JREAD(ISLASM,RM,NTERM*2)                                         
          DO 31 IL=1,NTERM                                                      
          IK=NA(IL)                                                             
          RMAT(I,IK)=RM(IL)                                                     
          RMAT(IK,I)=RM(IL)                                                     
 31       CONTINUE                                                              
 30       CONTINUE                                                              
1000      FORMAT(/' NODO RIGA:',I6,'        NODO COLONNA:',I6)                  
1100      FORMAT(/' NODO RIGA:',I6)                                             
1200      FORMAT(/' NODO COLONNA:',I6)                                          
  99      NODI=(NEQV-6)/3                                                       
          DO 50 IRIGA=1,NODI                                                    
          INDR=(IRIGA-1)*3+1                                                    
          DO 50 ICOL=1,NODI                                                     
C         WRITE(IOUT,1000)IRIGA,ICOL                                            
          INDC=(ICOL-1)*3+1                                                     
 50       CONTINUE                                                              
          DO 51 IRIGA=1,NODI                                                    
          INDR=(IRIGA-1)*3+1                                                    
C         WRITE(IOUT,1100)IRIGA                                                 
51        CONTINUE                                                              
          DO 52 ICOL=1,NODI                                                     
          INDC=(ICOL-1)*3+1                                                     
C         WRITE(IOUT,1200)ICOL                                                  
52        CONTINUE                                                              
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MSKYV                                                         
C*************************          MSKYV          *********************        
      SUBROUTINE MSKYV                                                          
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION W(1),A(1),V(1),NA(1)                                            
      ENTRY MSSKV(W,A,V,NA,NEQ)                                                 
      W(1)=W(1)+A(1)*V(1)                                                       
      IPOS=1                                                                    
      DO 40 I=2,NEQ                                                             
      IDIF=NA(I)-NA(I-1)                                                        
      IM=I-NA(I)+NA(I-1)+1                                                      
      IP=IM                                                                     
      IPOS=NA(I-1)                                                              
      VI=V(I)                                                                   
      DO 20 K=1,IDIF                                                            
      W(IM)=W(IM)+A(IPOS+K)*VI                                                  
      IM=IM+1                                                                   
 20   CONTINUE                                                                  
      IF(IDIF.EQ.1) GOTO 40                                                     
      ACCU=0.D0                                                                 
      IFIN=IDIF-1                                                               
      DO 30 K=1,IFIN                                                            
      ACCU=ACCU+A(IPOS+K)*V(IP)                                                 
      IP=IP+1                                                                   
 30   CONTINUE                                                                  
      W(I)=W(I)+ACCU                                                            
 40   CONTINUE                                                                  
      RETURN                                                                    
      ENTRY MASKV(W,A,V,NA,NEQ)                                                 
      W(1)=W(1)+A(1)*V(1)                                                       
      IPOS=1                                                                    
      DO 140 I=2,NEQ                                                            
      IDIF=NA(I)-NA(I-1)                                                        
      IM=I-NA(I)+NA(I-1)+1                                                      
      IP=IM                                                                     
      IPOS=NA(I-1)                                                              
      VI=V(I)                                                                   
      DO 120 K=1,IDIF                                                           
      W(IM)=W(IM)+A(IPOS+K)*VI                                                  
      IM=IM+1                                                                   
 120  CONTINUE                                                                  
      IF(IDIF.EQ.1) GOTO 140                                                    
      ACCU=0.D0                                                                 
      IFIN=IDIF-1                                                               
      DO 130 K=1,IFIN                                                           
      ACCU=ACCU+A(IPOS+K)*V(IP)                                                 
      IP=IP+1                                                                   
 130  CONTINUE                                                                  
      W(I)=W(I)-ACCU                                                            
 140  CONTINUE                                                                  
      RETURN                                                                    
      ENTRY MASKTV(W,A,V,NA,NEQ)                                                
      W(1)=W(1)+A(1)*V(1)                                                       
      IPOS=1                                                                    
      DO 240 I=2,NEQ                                                            
      IDIF=NA(I)-NA(I-1)                                                        
      IM=I-NA(I)+NA(I-1)+1                                                      
      IP=IM                                                                     
      IPOS=NA(I-1)                                                              
      VI=V(I)                                                                   
      DO 220 K=1,IDIF                                                           
      W(IM)=W(IM)-A(IPOS+K)*VI                                                  
      IM=IM+1                                                                   
 220  CONTINUE                                                                  
      IF(IDIF.EQ.1) GOTO 240                                                    
      ACCU=0.D0                                                                 
      IFIN=IDIF-1                                                               
      DO 230 K=1,IFIN                                                           
      ACCU=ACCU+A(IPOS+K)*V(IP)                                                 
      IP=IP+1                                                                   
 230  CONTINUE                                                                  
      W(I)=W(I)+ACCU                                                            
 240  CONTINUE                                                                  
 320  FORMAT(' W1 MASKTV',10E12.5,100(/10X,10E12.5))                            
      RETURN                                                                    
      END                                                                       

#----------------------------------------------------------------------
#
#    This file is part of Anba.
#
#    Anba is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Hanba is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
#
#----------------------------------------------------------------------
#
rm -f *.o make.log anba
for i in *.for; do 
	gfortran  -m32 -g -O2 -c -finit-local-zero -fno-automatic -fno-underscoring $i >> make.log 2>&1; 
done
g++ -c -O2 -m32 -g GestoreMemoria.C
gfortran -o anba -O2 -m32 -finit-local-zero -fno-automatic -g \
a7bd.o \
a2assubk.o \
a2sole.o \
a2sole1.o \
a2sole2.o \
a7ass.o \
a7ass1.o \
a7ass2.o \
a7ass3.o \
a7.o \
a7car.o \
a7ind.o \
a7ind1.o \
a7ind2.o \
a7ind3.o \
a7sfc1.o \
a7sfc2.o \
a7sfc3.o \
a7sfc4.o \
a7sfc5.o \
a7sfc6.o \
a7sfc7.o \
a7slcf.o \
a7solc.o \
bdgfbd-f.o \
gdbc.o \
a7lib.o \
a7libmsf.o \
GestoreMemoria.o \
-lstdc++

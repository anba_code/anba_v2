C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.CALSFC                                                
C*************************          CALSFC          ********************
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                 
      SUBROUTINE CALSFC                                                 
CNEW  SUBROUTINE CALSFC(ICON,NRSOL,NUME,NUINT,SOL,SIGMAG,SIGMAL,EPSI,   
CNEW *                  D,SIGM,SIGMG,SIGML,EPSP,DP,IELSET,GPQ)          
C                                                                       
C                                                                       
C                                                                       
      RETURN                                                            
      END                                                               
C                                                                       
      SUBROUTINE SFCCOR                                                 
     +(icon,NUME,NUINT,SOL,SIGM,NSOL,NRSOL,IELSET,NESEL,GPQ)            
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      REAL RMSP(6,6)                                                    
      LOGICAL DUMP,INCORE                                               
      character*32 filfit,filstr                                        
C                                                                       
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunstr                   
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,   
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,             
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                              
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C     
      COMMON /CORR/ E,ROC,AREA                                          
C                                                                       
      DIMENSION ICON(1),NUME(1),NUINT(1),SOL(NRSOL,1),IELSET(1)         
c      DIMENSION SIGMAG(NPSI,1),                                        
c     1          SIGMAL(NPSI,1),EPSI(NPSI,1),D(6,6),EPSIL(6,6)          
                                                                        
                                                                        
C+++++++++++++++++++++++++++++++++++++++++++++++++++                    
C  ho inserito TEMP(*) e COEFT(6)                                       
c      DIMENSION B(4),SHAPE(4),SIGMG(3,1),SIGML(2,1),DIDE(3,3),         
c     1          DP(2,1),EPSP(2,1),Z(2,6),  TEMP(*),COEFT(6)            
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      DIMENSION SIGM(1),GPQ(3,6),BTA(3,3),BTB(3,3),RIS(6,6)             
c      DATA DIDE/1.d0,0.d0,0.d0,0.d0,1.d0,0.d0,0.d0,0.d0,1.d0/          
C                                                                       
C                                                                       
C     CALCOLA GLI SFORZI NEI CORRENTI                                   
C                                                                       
                                                                        
                                                                        
      DO 1500 IEL=1,NCORR                                               
C                                                                       
 150  CALL JREAD(ISLELM,ITIP,4)                                         
      IF(ITIP.NE.1) GOTO 1900                                           
      CALL JREAD(ISLELM,ICON(1),1)                                      
      CALL JREAD(ISLELM,X,2)                                            
      CALL JREAD(ISLELM,Y,2)                                            
      CALL JREAD(ISLELM,E,6)                                            
      IPEL=1                                                            
      IF(NESEL.NE.0) IPEL=IPOSL(IDEL,IELSET,NESEL)                      
      IF(IFLMR.EQ.0.OR.IPEL.EQ.0)GOTO 150                               
      IND=NUINT(ICON(1))*NCS                                            
      IND=NUME(IND)+NEQV                                                
      IND1=NEQV-NPSI                                                    
      DO 110 I=1,NSOL                                                   
      SIGM(I)=(SOL(IND,I)+SOL(IND1+3,I)+Y*SOL(IND1+4,I)                 
     *          -X*SOL(IND1+5,I))                                       
110   CONTINUE                                                          
      IF(DUMP(14))  CALL JWRITE(ISLSFC,SIGM,NSOL*2)                     
      IF(DUMP(16)) THEN                                                 
        WRITE(IOUT,*) ITIP,IDEL,1                                       
        WRITE(IOUT,*) X,Y                                               
        WRITE(IOUT,*) (SIGM(I),I=1,NSOL)                                
      END IF                                                            
      DO 140 I=1,NSOL                                                   
      SIGM(I)=SIGM(I)*E                                                 
 140  CONTINUE                                                          
      IF(DUMP(16)) WRITE(IOUT,*) (SIGM(I),I=1,NSOL)                     
      CALL JWRITE(ISLSFC,SIGM,NSOL*2)                                   
      if(iabs(iunstr).gt.0) then                                              
        call dzero(ris,36)                                              
        do i=1,nsol                                                     
          ris(i,3)=sigm(i)                                              
        enddo                                                           
        do i=1,nsol
        if(iunstr.gt.0) then                                                     
          write(iunstr,7) idel,itip,iflmr,x,y,(ris(i,l),l=1,6)          
        else
          write(-iunstr) idel,itip,iflmr,x,y,(ris(i,l),l=1,6)            
        endif
        enddo                                                           
      endif                                                             
C                                                                       
C         CALCOLO TERMINI B                                             
C                                                                       
      GPQ(1,I)=GPQ(1,I)+AREA*SIGM(I)*Y*Y                                
      GPQ(2,I)=GPQ(2,I)+AREA*SIGM(I)*X*X                                
      GPQ(3,I)=GPQ(3,I)-AREA*SIGM(I)*X*Y                                
C                                                                       
 1500 CONTINUE                                                          
      RETURN                                                            
 1900 CALL JPOSRL(ISLELM,-4)                                            
      RETURN                                                            
C                                                                       
7     format(3i8,8E12.5)                                                
C                                                                       
      END                                                               
C                                                                       
      SUBROUTINE SFCGIU                                                 
     +(NUME,NUINT,SOL,SIGM,ICON,NSOL,NRSOL,IELSET,NESEL,GPQ)            
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      REAL RMSP(6,6)                                                    
      LOGICAL DUMP,INCORE                                               
      character*32 filfit,filstr                                        
C                                                                       
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunstr                   
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,   
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,             
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                              
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C     
      COMMON /GIUNZ/ G,ROG,RL,T,DELTAX,DELTAY,OMEGA                     
C                                                                       
      DIMENSION ICON(1),NUME(1),NUINT(1),SOL(NRSOL,1),IELSET(1)         
c      DIMENSION SIGMAG(NPSI,1),                                        
c     1          SIGMAL(NPSI,1),EPSI(NPSI,1),D(6,6),EPSIL(6,6)          
                                                                        
c      dimension vel(6),idlam(30),spes(30),rotaz(30),car(6,30),         
c     1          elas(3,3),epsy(3,6),elasl(3,3),t(9),epsl(3),           
c     2          sigmy(3,6)                                             
                                                                        
C+++++++++++++++++++++++++++++++++++++++++++++++++++                    
C  ho inserito TEMP(*) e COEFT(6)                                       
      DIMENSION B(4),SHAPE(4),SIGMG(3,1),SIGML(2,1),                    
     1          DP(2,1),EPSP(2,1),Z(2,6)                                
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      DIMENSION SIGM(1),GPQ(3,6),BTA(3,3),BTB(3,3),RIS(6,6)             
c      DATA DIDE/1.d0,0.d0,0.d0,0.d0,1.d0,0.d0,0.d0,0.d0,1.d0/          
C                                                                       
C                                                                       
C     CALCOLA GLI SFORZI NELLE GIUNZIONI CON DUE NODI                   
C                                                                       
      DO 2500 IEL=1,NGIUR                                               
 250  CALL JREAD(ISLELM,ITIP,4)                                         
      IF(ITIP.NE.2)GOTO 2900                                            
      CALL JREAD(ISLELM,ICON,2)                                         
      CALL JREAD(ISLELM,B,8)                                            
      CALL JREAD(ISLELM,G,8)                                            
      CALL JREAD(ISLELS,DELTAX,6)                                       
      IPEL=1                                                            
      IF(NESEL.NE.0) IPEL=IPOSL(IDEL,IELSET,NESEL)                      
      IF(IFLMR.EQ.0.OR.IPEL.EQ.0)GOTO 250                               
      GSUL=G/RL                                                         
      IND1=NUINT(ICON(1))*NCS
      IND2=NUINT(ICON(2))*NCS
      IND1=NUME(IND1)   
      IND2=NUME(IND2)                                                   
      IND3=NEQV-NPSI                                                    
      DO 210 I=1,NSOL                                                   
      SIGM(I)=SOL(IND2,I)-SOL(IND1,I)+DELTAX*SOL(IND3+1,I)+
     1         DELTAY*SOL(IND3+2,I)+2.*OMEGA*SOL(IND3+6,I)
 210  CONTINUE                                                          
      IF(DUMP(16)) THEN                                                 
        B(1)=(B(1)+B(3))/2.                                             
        B(2)=(B(2)+B(4))/2.
        WRITE(IOUT,*) ITIP,IDEL,1                                       
        WRITE(IOUT,*) B(1),B(2)                                         
        WRITE(IOUT,*) (SIGM(I),I=1,NSOL)                                
      END IF                                                            
      IF(DUMP(14))  CALL JWRITE(ISLSFC,SIGM,NSOL*2)                     
      DO 230 I=1,NSOL                                                   
      SIGM(I)=SIGM(I)*GSUL                                              
 230  CONTINUE                                                          
      if(iunstr.gt.0) then                                              
        call dzero(ris,36)                                              
        do i=1,nsol                                                     
          ris(i,6)=sigm(i)                                              
        enddo                                                           
        do i=1,nsol                                                     
        if(iunstr.gt.0) then                                                     
          write(iunstr,7) idel,itip,iflmr,b(1),b(2),(ris(i,l),l=1,6)    
        else
          write(-iunstr) idel,itip,iflmr,b(1),b(2),(ris(i,l),l=1,6)      
        endif
        enddo                                                           
      endif                                                             
      IF(DUMP(16)) WRITE(IOUT,*) (SIGM(I),I=1,NSOL)                     
      CALL JWRITE(ISLSFC,SIGM,NSOL*2)                                   
 2500 CONTINUE                                                          
      RETURN                                                            
 2900 CALL JPOSRL(ISLELM,-4)                                            
7     format(3i8,8E12.5)                                                
      RETURN                                                            
C                                                                       
C                                                                       
      END                                                               
C                                                                       
      SUBROUTINE SFCPAN(NUME,NUINT,SOL,SIGMG,SIGML,ICON,NSOL,NRSOL,     
     1                  EPSP,DP,IELSET,NESEL,GPQ)                       
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      REAL RMSP(6,6)                                                    
      LOGICAL DUMP,INCORE                                               
      character*32 filfit,filstr                                        
C                                                                       
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunstr                   
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,   
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,             
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                              
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C     
      COMMON /PANN/  TICK,DEN,NPINT                                     
C                                                                       
                                                                        
      dimension vel(6),idlam(30),spes(30),rotaz(30),car(6,30),          
     1          elas(3,3),epsy(3,6),elasl(3,3),t(9),epsl(3),            
     2          sigmy(3,6)                                              
                                                                        
      DIMENSION ICON(1),NUME(1),NUINT(1),SOL(NRSOL,1),IELSET(1)         
C+++++++++++++++++++++++++++++++++++++++++++++++++++                    
C  ho inserito TEMP(*) e COEFT(6)                                       
      DIMENSION B(5),SHAPE(5),SIGMG(3,1),SIGML(2,1),                    
     1          DP(2,1),EPSP(2,1),Z(2,6)                                
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      DIMENSION SIGM(1),GPQ(3,6),BTA(3,3),BTB(3,3),RIS(6,6)             
C                
      print*,'entro in sfcpan'                                                       
      DO 3500 IEL=1,NPANR                                               
 350    CALL JREAD(ISLELM,ITIP,4)                                         
        IF(ITIP.NE.3) GOTO 3900                                           
        CALL JREAD(ISLELM,ICON,NCON)                                      
        CALL JPOSRL(ISLELM,NCON*4)                                        
        CALL JREAD(ISLELM,TICK,5)                                         
        DO 35 I=1,2                                                       
          CALL JREAD(ISLELM,DP(1,I),4)                                      
 35     CONTINUE                                                          
        CALL JREAD(ISLELM,Z0,2)                                         
      print*,itip,idel,ncon
      print'(4i8)',(icon(iw),iw=1,ncon)
      print*,'TICK,DEN,npint',TICK,DEN,npint
      print*,'dp',((dp(i,k),i=1,2),k=1,2)
      print*,'z0',z0                                               

        DP(2,1)=DP(1,2)                                                   
        call jread(islelm,nskip,1)                                        
c+++      call jposrl(islelm,nskip)                                     
                                                                        
        call jread(islelm,nlami,1)
        print*,' nlami',nlami                                           
        if(nlami.gt.30)then                                             
          write(iout,*)' troppe lamine per recupero sforzi '            
          stop                                                          
        endif                                                           
        call dzero(vel,6)                                               
        do iii=1,nlami                                                  
cc      print*,iii                                                          
          call jread(islelm,idlam(iii),1)                               
          call jread(islelm,spes(iii),2)                                
          call jread(islelm,rotaz(iii),2)                               
	    rotaz(iii)=rotaz(iii)*180./3.141592654d0                         
cc      print*,'lam',idlam(iii),spes(iii),rotaz(iii)                           
          call jread(islelm,car(1,iii),5*2)
          car(6,iii)=0.                                 
cc      print*,'car',(car(io,iii),io=1,5)                                     
          call medcar(vel,car(1,iii),dent,spes(iii),rotaz(iii))         
cc      print*,'dop',idlam(iii),spes(iii),rotaz(iii)                           
        enddo                                                           
        elas(1,1)=vel(1)                                                
        elas(1,2)=vel(2)                                                
        elas(1,3)=vel(4)                                                
        elas(2,1)=vel(2)                                                
        elas(2,2)=vel(3)                                                
        elas(2,3)=vel(5)                                                
        elas(3,1)=vel(4)                                                
        elas(3,2)=vel(5)                                                
        elas(3,3)=vel(6)                                                
                                                                        
      DO 310 IN=1,NPINT                                                 
cc      print*,' do 310 in=',in           
        CALL JREAD(ISLELS,B,NCON*2)
        CALL JREAD(ISLELS,SHAPE,NCON*2)                                   
cc      print*,'b',(b(iol),iol=1,ncon)                                       
cc      print*,'n',(shape(iol),iol=1,ncon)
        DO 15 I=1,NPSI                                                    
          CALL JREAD(ISLELS,Z(1,I),4)                                       
15      CONTINUE             
        WRITE(*,*) -Z(1,5),Z(1,4)                                             
        IPEL=1                                                            
        IF(NESEL.NE.0) IPEL=IPOSL(IDEL,IELSET,NESEL)                      
        IF(IFLMR.EQ.0.OR.IPEL.EQ.0)GOTO 310                               
        IF(IN.EQ.1 .AND. DUMP(16))  WRITE(IOUT,*) ITIP,IDEL,NPINT         
      IF(DUMP(16)) WRITE(IOUT,*) -Z(1,5),Z(1,4)                         
        DO 320 I=1,NSOL                                                   
          IND1=NEQV-NPSI                                                    
          DO 330 L=1,2                                                      
            EPSP(L,I)=0.D0                                                    
            DO 330 K=1,NPSI                                                   
            EPSP(L,I)=EPSP(L,I)+Z(L,K)*SOL(IND1+K,I)                          
 330      CONTINUE                                                          
          DO 340 K=1,NCON                                                   
            IND=NUINT(ICON(K))*NCS                                            
            IND=NUME(IND)                                                     
            EPSP(1,I)=EPSP(1,I)+SHAPE(K)*SOL(IND+NEQV,I)                      
            EPSP(2,I)=EPSP(2,I)+B(K)*SOL(IND,I)                               
 340      CONTINUE                                                          
                                                                        
          epsy(1,i)=epsp(1,i)                                               
          epsy(3,i)=epsp(2,i)                                               
          if(elas(2,2).ne.0.0)then                                          
            epsy(2,i)=-(elas(2,1)*epsy(1,i)+  elas(2,3)*                      
     *                            epsy(3,i))/elas(2,2)                      
          else                                                              
            epsy(2,i)=0.                                                      
          endif                                                             
cc>      SIGML(1,I)=(DP(1,1)*EPSP(1,I)+DP(1,2)*EPSP(2,I))                  
cc>      SIGML(2,I)=(DP(2,1)*EPSP(1,I)+DP(2,2)*EPSP(2,I))                  
cc>      SIGMG(1,I)=SIGML(1,I)                                             
cc>      SIGMG(2,I)=SIGML(2,I)*Z(2,1)                                      
cc>      SIGMG(3,I)=SIGML(2,I)*Z(2,2)                                      
 320    CONTINUE                                                          
c>>cc      IF(DUMP(14))  CALL JWRITE(ISLSFC,EPSP,4*NSOL)                     
      IF(DUMP(14))  CALL JWRITE(ISLSFC,EPSY,6*NSOL)                     

c>ccc      if(iabs(iunstr).gt.0) then                                              
                                                                        
        do ilam=1,nlami                                                 
          print*,ilam
          call dzero(vel,6)          
          sp=1.d0  
          alfa=0.d0
          call medcar(vel,car(1,ilam),sp,1.d0,alfa)                     
          elasl(1,1)=vel(1)                                             
          elasl(1,2)=vel(2)                                             
          elasl(1,3)=vel(4)                                             
          elasl(2,1)=vel(2)                                             
          elasl(2,2)=vel(3)                                             
          elasl(2,3)=vel(5)                                             
          elasl(3,1)=vel(4)                                             
          elasl(3,2)=vel(5)                                             
          elasl(3,3)=vel(6)                                             
          call trasf(rotaz(ilam),t)                                     
          call dzero(ris,36)                                              
      write(iout,77) 'el',idel,ilam,rotaz(ilam)  
          do i=1,nsol 
c          ris(i,3)=sigml(1,i)                                          
c          ris(i,6)=sigml(2,i)                                          
            call dpromm(t,3,3,3,epsy(1,i),3,epsl,3,1,0,0)               
            call dpromm(elasl,3,3,3,epsl,3,sigmy(1,i),3,1,0,0)          

            call dpromm(t,3,3,3,sigmy(1,i),3,epsl,3,1,1,0)              
            call dpromm(epsl,3,3,3,t,3,sigmy(1,i),3,1,0,1)              

            ris(i,3)=sigmy(1,i)                                         
            ris(i,4)=sigmy(2,i)                                         
            ris(i,1)=sigmy(3,i)                                         
ccc<      if(i.eq.3)then                                                   
                                    
ccc<      write(iout,'(3e12.5)')t                                          
      write(iout,77) 'epsy',in,i,epsy(1,i),epsy(2,i),epsy(3,i)              
      write(iout,77) 'epsl',in,i,epsl(1),epsl(2),epsl(3)                    
c      write(iout,77) 'sigm',in,i,sigmy(1,i),sigmy(2,i),sigmy(3,i)           
ccc<      endif                                                            
          enddo                
      IF(DUMP(14))      CALL JWRITE(ISLSFC,epsl,6*3*2)                                  
          CALL JWRITE(ISLSFC,sigmy,6*3*2)
          do i=1,nsol                                                     
 77    format(a,2i4,8E12.5)                                               
                                                                        
            if(iunstr.gt.0) then
              write(iunstr,7) idel,itip,idlam(ilam),                          
     *                     -z(1,5),z(1,4),(ris(i,l),l=1,6)              
            elseif(iunstr.lt.0)  then
              write(-iunstr) idel,itip,idlam(ilam),                          
     *                     -z(1,5),z(1,4),(ris(i,l),l=1,6)              
            endif                                                               
          enddo      
        enddo                                                            
c>ccc      endif                                                             
cc>      IF(DUMP(16)) THEN                                                 
cc>        WRITE(IOUT,2) ((EPSP(I,K),I=1,2),K=1,NSOL)                      
cc>        WRITE(IOUT,2) ((SIGML(I,K),I=1,2),K=1,NSOL)                     
cc>        WRITE(IOUT,1) ((SIGMG(I,K),I=1,3),K=1,NSOL)                     
cc>      END IF                                                            
1     format(3e12.5)                                                    
2     format(2e12.5)                                                    
cc      CALL JWRITE(ISLSFC,SIGMG,6*NSOL)                                  
cc      CALL JWRITE(ISLSFC,SIGML,4*NSOL)                                  
C                                                                       
C         CALCOLO TERMINI B                                             
C                                                                       
      GPQ(1,I)=GPQ(1,I)+W*SIGMG(1,I)*Z(1,4)*Z(1,4)                      
      GPQ(2,I)=GPQ(2,I)+W*SIGMG(1,I)*Z(1,5)*Z(1,5)                      
      GPQ(3,I)=GPQ(3,I)-W*SIGMG(1,I)*Z(1,4)*Z(1,5)                      
C                                                                       
 310  CONTINUE                                                          
      IF(IFLMR.EQ.0) GOTO 350                                           
 3500 CONTINUE                                                          
      print*,'esco da sfcpan'                                                       
      RETURN                                                            
 3900 CALL JPOSRL(ISLELM,-4)                                            
      RETURN                                                            
C                                                                       
C                                                                       
c++++++++++++++++++  1settembre  +++++++++++++++++++++++++++++          
c  Inserisco index per distinguere il caso in cui si debba              
c  svolgere o meno il pb termico                                        
c  index=1 senza pb. termico [svolgo solo il primo blocco]              
c  index=2 con pb. termico [svolgo ambedue i blocchi]                   
                                                                        
7     format(3i8,8E12.5)                                                
                                                                        
      END                                                               
C                                                                       
      SUBROUTINE SFCEPL(NUME,NUINT,SOL,SIGMAG,SIGMAL,EPSI,D,ICON,       
     1   NSOL,NRSOL,ITY,IELSET,NESEL,GPQ,TEMP,index)                    
      IMPLICIT REAL*8 (A-H,O-Z)                                         
      LOGICAL DUMP,INCORE                                               
      character*32 filfit,filstr                                        
C                                                                       
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunstr                   
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,   
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ, 
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2, 
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,             
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                              
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
c++++++++++++  31agosto +++++++++++++++++++++++++++++++++++++++++++     
c  Ho ripristinato il common ELPI1 originale [ togliendo CDLTZ(6) ]     
      COMMON /ELPI1/  LREC,NPIN,ENNE(3,12),CPI(2),DA,ALFA,T1(3,3),      
     1                T2(3,3),DG(6,6),W,DEL,COE(2)                      
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++             
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),            
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C     
      COMMON /ELPI2/  INT1,INT2,TETA,DENS,alfort,IFLTET                 
C                                                                       
      real*4 RMSP(6,6)                                                  
      real*8 EPSIL(6,6)                                                 
      DIMENSION ICON(1),NUME(1),NUINT(1),SOL(NRSOL,1),IELSET(1)         
      DIMENSION SIGMAG(NPSI,1),                                         
     1          SIGMAL(NPSI,1),EPSI(NPSI,1),D(6,6)                      
                                                                        
c      dimension vel(6),idlam(30),spes(30),rotaz(30),car(6,30),         
c     1          elas(3,3),epsy(3,6),elasl(3,3),t(9),epsl(3),           
c     2          sigmy(3,6)                                             
                                                                        
C+++++++++++++++++++++++++++++++++++++++++++++++++++                    
C  ho inserito TEMP(*) e COEFT(6)                                       
      DIMENSION  TEMP(*),COEFT(6)                                       
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      DIMENSION GPQ(3,6),BTA(3,3),BTB(3,3),RIS(6,6),DIDE(3,3)           
      DATA DIDE/1.d0,0.d0,0.d0,0.d0,1.d0,0.d0,0.d0,0.d0,1.d0/
                                                                        
                                                                        
C                                                                       
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++        
      CALL DZERO(RIS,36)                                                
      NWORD=NSOL*NPSI*2                                                 
      NELI=NLAMR                                                        
      IF(ITY.EQ.5)NELI=NELPR                                            
      DO 4500 IEL=1,NELI                                                
 450  CALL JREAD(ISLELM,ITIP,4)                                         
      IF(ITIP.NE.ITY)GOTO 4900                                          
      CALL JREAD(ISLELM,ICON,NCON)                                      
      IF(ITIP.EQ.4) CALL JPOSRL(ISLELM,NCON)                            
      CALL JPOSRL(ISLELM,NCON*4)                                        
      CALL JREAD(ISLELM,nwr,1)                                          
      CALL JPOSRL(ISLELM,nwr)                                           
      DO 45 I=1,6                                                       
      CALL JREAD(ISLELM,D(1,I),12)                                      
 45   CONTINUE                                                          
C------                                                                 
      IF(ITIP.EQ.5) CALL JREAD(ISLELM,COEFT,6*2)                        
C------                                                                 
      CALL JREAD(ISLELS,LREC,2)                                         
      DO 410 IN=1,NPIN                                                  
      CALL JREAD(ISLELS,ENNE,LREC)                                      
      IPEL=1                                                            
      IF(NESEL.NE.0) IPEL=IPOSL(IDEL,IELSET,NESEL)                      
      IF(IFLMR.EQ.0.OR.IPEL.EQ.0)GOTO 410                               
C***** prima modifica                                                   
      CALL DZERO(EPSI,6*NSOL)                                           
      DO 415 K1=1,6                                                     
      DO 415 K2=1,6                                                     
      DG(K1,K2)=DG(K1,K2)/W                                             
 415  CONTINUE                                                          
c++++++++++++++++ 1settembre +++++++++++++++++++++++++++                
c  primo blocco [sia per index=1 che per index=2]                       
c++++++++++++++++++++++++++++++++++++++++++++++++++++                   
      DO 420 I=1,NSOL                                                   
      IND1=NEQV-NPSI                                                    
C                                                                       
C     CALCOLO EPSILON=EPSILON+ZETA*PSI                                  
C                                                                       
c     ***************************                                       
      EPSI(1,I)=EPSI(1,I)+(SOL(IND1+1,I)-CPI(2)*SOL(IND1+6,I))/DEL      
      EPSI(2,I)=EPSI(2,I)+(SOL(IND1+2,I)+CPI(1)*SOL(IND1+6,I))/DEL      
      EPSI(3,I)=EPSI(3,I)+(SOL(IND1+3,I)+CPI(2)*SOL(IND1+4,I)           
     1                                 -CPI(1)*SOL(IND1+5,I))/DEL       
      KK=0                                                              
      DO 440 K=1,NCON                                                   
      IF(ICON(K).EQ.0) GOTO 440                                         
      KK=KK+1                                                           
      IND=(NUINT(ICON(K))-1)*NCS+1                                      
      IND=NUME(IND)-1                                                   
C                                                                       
C     CALCOLO EPSILON=EPSILON+ESSE*G DERIVATO/Z                         
C                                                                       
      EPSI(1,I)=EPSI(1,I)+(ENNE(1,KK)*SOL(IND+NEQV+1,I))/DEL            
      EPSI(2,I)=EPSI(2,I)+(ENNE(1,KK)*SOL(IND+NEQV+2,I))/DEL            
      EPSI(3,I)=EPSI(3,I)+(ENNE(1,KK)*SOL(IND+NEQV+3,I))/DEL            
C                                                                       
C     CALCOLO EPSILON=EPSILON+G*OPERATORE DERIVATA                      
C                                                                       
      CALL BTABUP(DEL,ENNE(1,KK),BTA,COE,CDC(4))                        
      CALL BTBBUP(DEL,ENNE(1,KK),BTB)                                   
      DO 430 L=1,3                                                      
      DO 430 M=1,3                                                      
      EPSI(L,I)=EPSI(L,I)+BTA(M,L)*SOL(IND+M,I)/DEL                     
      EPSI(L+3,I)=EPSI(L+3,I)+BTB(M,L)*SOL(IND+M,I)/DEL                 
 430  CONTINUE                                                          
 440  CONTINUE                                                          
 420  CONTINUE                                                          
c++++++++   1settembre +++++++++++++++++++++++++++++                    
c  fine primo blocco                                                    
      if(index.eq.1) goto 19                                            
c  secondo blocco                                                       
C++++++++++++++++++++++++++++++++++++++++++++++++++                     
C+++++++++++++++++++++++++++++++++++++++++++++++++++                    
C  INTRODUCO MODIFICA SOLO PER CALCOLO TERMICO                          
C                                                                       
c      CALL DWRITE('EPSIT',EPSI,6,6,nsol,1)                             
                                                                        
C     CALCOLO [N][Tnodo]                                                
      TPI=0.                                                            
      DO 17,I1=1,NCON                                                   
      IND=(NUINT(ICON(I1))-1)*NCS+1                                     
      IND=NUME(IND)                                                     
      TPI=TPI+ENNE(1,I1)*TEMP(IND)                                      
17    CONTINUE                                                          
C     CALCOLO {E}-{ALFA}{E}termica                                      
      DO 27,I2=1,6                                                      
      EPSI(I2,1)=EPSI(I2,1)-COEFT(I2)*TPI                               
27    CONTINUE                                                          
C++++++++++++++++++++++++++++++++++++++++++++++++++                     
C++++++++++++++++++++++++++++++++++++++++++++++++++                     
c------ seconda modifica                                                
c****                                                                   
c+++++++++++++ 1sett +++++++++++++++++++++++++++++++++++++++++          
c  Commento le parti che servono per controllare le deformaz            
c      write(iout,*) ' ******* stampa di controllo di EPSI *****'       
c      CALL DWRITE('EPSIV',EPSI,6,6,nsol,1)                             
c      write(iout,*) '******* fine stampa di controllo *****'           
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
c****                                                                   
                                                                        
c      END IF                                                           
c+++++++++  1settembre  ++++++++++++++++++++++++                        
19    continue                                                          
c+++++++++++++++++++++++++++++++++++++++++++++++                        
C     FINE MODIFICA                                                     
c+++++++++++++ 1sett +++++++++++++++++++++++++++++++++++++++++          
c  Commento le parti che servono per controllare le deformaz            
c      IF(DUMP(16))  WRITE(IOUT,9) ((EPSI(I,K),I=1,6),K=1,nsol)         
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         
C      CALL DWRITE('dg',dg,6,6,6,1)                                     
                                                                        
      CALL DPROMM(DG,6,6,6,EPSI,6,SIGMAG,6,nsol,0,0)                    
      CALL DPROMM(D(1,1),6,3,3,T1,3,DG(1,1),6,3,0,1)                    
      CALL DPROMM(D(4,1),6,3,3,T1,3,DG(4,1),6,3,0,1)                    
      CALL DPROMM(D(1,4),6,3,3,T2,3,DG(1,4),6,3,0,1)                    
      CALL DPROMM(D(4,4),6,3,3,T2,3,DG(4,4),6,3,0,1)                    
      CALL DPROMM(DG,6,6,6,EPSI,6,SIGMAL,6,nsol,0,0)                    
c     calcolo deformazioni nel riferimento locale                       
      CALL DZERO(DG,6*NSOL)                                             
      CALL DPROMM(DIDE,3,3,3,T1,3,DG(1,1),6,3,0,1)                      
      CALL DPROMM(DIDE,3,3,3,T2,3,DG(4,4),6,3,0,1)                      
      CALL DPROMM(DG,6,6,6,EPSI,6,EPSIL,6,nsol,0,0)                     
c+++++++++++++ 1sett +++++++++++++++++++++++++++++++++++++++++          
c  Commento le parti che servono per controllare le deformaz            
c      IF(DUMP(16)) THEN                                                
c      write(iout,*) '+++++ SIGMAL(6,nsol) +++++++'                     
c      WRITE(IOUT,9) ((SIGMAL(I,K),I=1,6),K=1,nsol)                     
c      write(iout,*) '+++++ SIGMAG(6,nsol) +++++++'                     
c      WRITE(IOUT,9) ((SIGMAG(I,K),I=1,6),K=1,nsol)                     
c      END IF                                                           
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         
      if(iabs(iunstr).gt.0) then                                              
        call dzero(ris,36)                                              
c        do i=1,nsol                                                    
c        do l=1,6                                                       
c          ris(i,l)=sigmal(i,l)                                         
c        enddo                                                          
c        enddo                                                          
c           write(iout,*)' alfa=',alfa                                  
            write(iout,*) ' alfa= ', alfort                             
cwrite(iout,*)' sl   '                                                  
cwrite(iout,'(6e12.5)') ((sigmal(ioo,iop),iop=1,6),ioo=1,6)             
        call trasf4(sigmal,alfort,ris)                                  
c       call trasf4(sigmal,5.d0,ris)                                    
        do l=1,nsol                                                     
c         write(iout,7) idel,itip,iflmr,cpi(1),cpi(2),(ris(i,l),i=1,6)  
         if(iunstr.gt.0) then
          write(iunstr,7) idel,itip,iflmr,cpi(1),cpi(2),(ris(i,l),i=1,6)
         else
          write(-iunstr) idel,itip,iflmr,cpi(1),cpi(2),(ris(i,l),i=1,6)  
         endif
        enddo                                                           
      endif                                                             
          DO 444 IR1=1,6                                                
            DO 444 IC1=1,nsol                                           
444         RMSP(IR1,IC1)=EPSIL(IR1,IC1)                                
      IF(DUMP(14)) CALL JWRITE(ISLSFC,RMSP,nsol*6)                      
          DO 445 IR1=1,6                                                
            DO 445 IC1=1,nsol                                           
445         RMSP(IR1,IC1)=SIGMAG(IR1,IC1)                               
      CALL JWRITE(ISLSFC,RMSP,nsol*6)                                   
          DO 446 IR1=1,6                                                
            DO 446 IC1=1,nsol                                           
446         RMSP(IR1,IC1)=SIGMAL(IR1,IC1)                               
      CALL JWRITE(ISLSFC,RMSP,nsol*6)                                   
C                                                                       
C         CALCOLO TERMINI B                                             
C                                                                       
c++++++++++++++   31 agosto   ++++++++++++++++++++++++                  
c     caso senza pb termico [index=1]                                   
      if (index.eq.1) then                                              
c++++++++++++++++++++++++++++++++++++++++++++++++++++                   
      DO 78 K=1,nsol                                                    
      RIS(1,K)=RIS(1,K)+DA*SIGMAG(1,K)                                  
      RIS(2,K)=RIS(2,K)+DA*SIGMAG(2,K)                                  
      RIS(3,K)=RIS(3,K)+DA*SIGMAG(3,K)                                  
      RIS(4,K)=RIS(4,K)+DA*SIGMAG(3,K)*CPI(2)                           
      RIS(5,K)=RIS(5,K)-DA*SIGMAG(3,K)*CPI(1)                           
      RIS(6,K)=RIS(6,K)-DA*SIGMAG(1,K)*CPI(2)+DA*SIGMAG(2,K)*CPI(1)     
 78   CONTINUE                                                          
      DO 80 I=1,NSOL                                                    
      GPQ(1,I)=GPQ(1,I)+DA*(DEL**2)*SIGMAG(3,I)*CPI(2)*CPI(2)           
      GPQ(2,I)=GPQ(2,I)+DA*(DEL**2)*SIGMAG(3,I)*CPI(1)*CPI(1)           
      GPQ(3,I)=GPQ(3,I)-DA*(DEL**2)*SIGMAG(3,I)*CPI(1)*CPI(2)           
 80   CONTINUE                                                          
      end if                                                            
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
C                                                                       
C                                                                       
 410  CONTINUE                                                          
                                                                        
      IF(IFLMR.EQ.0) GOTO 450                                           
 4500 CONTINUE                                                          
                                                                        
      WRITE(IOUT,'(/,A)') ' RISULTANTI DEGLI SFORZI'                    
      WRITE(IOUT,'(1X,6E12.5)')((RIS(I,K),K=1,6),I=1,6)                 
      RETURN                                                            
 4900 CALL JPOSRL(ISLELM,-4)                                            
      RETURN                                                            
9     format(6e12.5)                                                    
7     format(3i8,8E12.5)                                                
      END                                                               

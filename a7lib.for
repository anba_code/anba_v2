C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.IZERO                                                         
C*************************          IZERO          ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE IZERO(IV,N)                                                    
      DIMENSION IV(*)                                                           
C                                                                               
C     AZZERA UN VETTORE INTERO DI -N- TERMINI                                   
C                                                                               
      DO 10 I=1,N                                                               
      IV(I)=0                                                                   
10    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.RZERO                                                         
C*************************          RZERO          ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RZERO(RV,N)                                                    
      DIMENSION RV(*)                                                           
      DO 20 I=1,N                                                               
      RV(I)=0.                                                                  
20    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.DZERO                                                         
C*************************          DZERO          ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DZERO(DV,N)                                                    
      REAL*8 DV                                                                 
      DIMENSION DV(*)                                                           
C                                                                               
C     AZZERA UN VETTORE DOPPIA PRECISIONE DI -N- TERMINI                        
C                                                                               
      DO 30 I=1,N                                                               
      DV(I)=0.                                                                  
30    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.CHKPNT                                                        
C*************************          CHKPNT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CHKPNT                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      CHARACTER*80 IDESC                                                        
c      COMMON /TESTA/ IDESC(2)                                                  
      common /testa/ id(160)                                                    
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO                     
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      CALL JOPEN('*CHK',ISLCHK,1)                                               
c      CALL JWRITE(ISLCHK,IDESC,40)                                             
      CALL JWRITE(ISLCHK,id,40)                                                 
      CALL JWRITE(ISLCHK,NELEM,15)                                              
      CALL JWRITE(ISLCHK,ERR,43)                                                
      CALL JWRITE(ISLCHK,NELTOT,15)                                             
      CALL JCLOSE(ISLCHK)                                                       
      RETURN                                                                    
      ENTRY RSTART                                                              
      CALL JOPEN('*CHK',ISLCHK,1)                                               
c      CALL JREAD(ISLCHK,IDESC,40)                                              
      CALL JREAD(ISLCHK,ID,40)                                                  
      CALL JREAD(ISLCHK,NELEM,15)                                               
      CALL JREAD(ISLCHK,ERR,43)                                                 
      CALL JREAD(ISLCHK,NELTOT,15)                                              
      CALL JCLOSE(ISLCHK)                                                       
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.DPROMM                                                        
C*************************          DPROMM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DPROMM(A,NRDA,NRA,NCA,B,NRDB,C,NRDC,NCC,IA,IB)                 
C                                                                               
C     ESEGUE IL PRODOTTO DELLA MATRICE "A" PER "B"                              
C     A    = PRIMA MATRICE                                                      
C     NRDA = N.RO DI RIGHE PER IL DIMENSIONAMENTO DI "A"                        
C     NRA  = N.RO DI RIGHE UTILIZZATE PER IL PRODOTTO DI "A"                    
C     NCA  = N.RO DI COLONNE DI "A"                                             
C     B    = SECONDA MATRICE                                                    
C     NRDB = N.RO DI RIGHE PER IL DIMENSIONAMENTO DI "B"                        
C     C    = MATRICE PRODOTTO                                                   
C     NRDC = N.RO DI RIGHE PER IL DIMENSIONAMENTO DI "C"                        
C     NCB  = N.RO DI COLONNE DI "C"                                             
C     IA   = INDICE DI TRASPOSIZIONE DI "A"  0 = NO                             
C                                            1 = SI                             
C     IB   = INDICE DI TRASPOSIZIONE DI "B"                                     
C                                                                               
      REAL*8 A,B,C                                                              
      DIMENSION A(NRDA,1),B(NRDB,1),C(NRDC,1)                                   
      IGO=2*IA+IB+1                                                             
      GOTO(10,20,30,40),IGO                                                     
10    DO 11 I=1,NRA                                                             
      DO 11 K=1,NCC                                                             
      C(I,K)=0.                                                                 
      DO 11 L=1,NCA                                                             
      C(I,K)=C(I,K)+A(I,L)*B(L,K)                                               
11    CONTINUE                                                                  
      RETURN                                                                    
20    DO 12 I=1,NRA                                                             
      DO 12 K=1,NCC                                                             
      C(I,K)=0.                                                                 
      DO 12 L=1,NCA                                                             
      C(I,K)=C(I,K)+A(I,L)*B(K,L)                                               
12    CONTINUE                                                                  
      RETURN                                                                    
30    DO 13 I=1,NCA                                                             
      DO 13 K=1,NCC                                                             
      C(I,K)=0.                                                                 
      DO 13 L=1,NRA                                                             
      C(I,K)=C(I,K)+A(L,I)*B(L,K)                                               
13    CONTINUE                                                                  
      RETURN                                                                    
40    DO 14 I=1,NCA                                                             
      DO 14 K=1,NCC                                                             
      C(I,K)=0.                                                                 
      DO 14 L=1,NRA                                                             
      C(I,K)=C(I,K)+A(L,I)*B(K,L)                                               
14    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.DWRITM                                                        
C*************************          DWRITM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DWRITM(NOME,ZMAT,NRD,NR,NC,IFLAG,IPIAZ)                        
C                                                                               
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      CHARACTER NOME*(*)                                                        
C                                                                               
      REAL*8 ZMAT                                                               
      DIMENSION ZMAT(NRD,1),IPIAZ(1),m(30),ipt(30)                              
      if(nr.gt.30) return                                                       
      do 777 i=1,nr                                                             
      ipt(i)=ipiaz(i)                                                           
      m(i)=i                                                                    
777   continue                                                                  
      write(iout,'(a,30i3)')' ipiaz ',(ipiaz(i),i=1,nr)                         
      CALL SHLSRi(NR,IPT,M,30,1,3)                                              
      write(iout,'(a,30i3)')' ipt   ',(ipt(i),i=1,nr)                           
      write(iout,'(a,30i3)')' m     ',(m(i),i=1,nr)                             
      WRITE(IOUT,100) NOME,IFLAG                                                
      IF(IFLAG.EQ.-1) GOTO1                                                     
      DO 10 J=1,NR                                                              
      WRITE(IOUT,300) J                                                         
      WRITE(IOUT,200) (ZMAT(m(J),m(K)),K=1,NC)                                  
10    CONTINUE                                                                  
      RETURN                                                                    
1     DO 20 K=1,NC                                                              
      WRITE(IOUT,400) K                                                         
      WRITE(IOUT,200) (ZMAT(m(J),m(K)),J=1,NR)                                  
20    CONTINUE                                                                  
      RETURN                                                                    
100   FORMAT(//10X,A,I9/)                                                       
200   FORMAT(5X,12E10.4)                                                        
300   FORMAT(/'  RIGA  N. :',I8)                                                
400   FORMAT(/'  COLONNA  N. :',I8)                                             
      END                                                                       
C@ELT,I ANBA*ANBA.DWRITE                                                        
C*************************          DWRITE          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DWRITE(NOME,ZMAT,NRD,NR,NC,IFLAG)                              
C                                                                               
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      CHARACTER NOME*(*)                                                        
C                                                                               
      REAL*8 ZMAT                                                               
      DIMENSION ZMAT(NRD,1)                                                     
      WRITE(IOUT,100) NOME,IFLAG                                                
      IF(IFLAG.EQ.-1) GOTO1                                                     
      DO 10 J=1,NR                                                              
      WRITE(IOUT,300) J                                                         
      WRITE(IOUT,200) (ZMAT(J,K),K=1,NC)                                        
10    CONTINUE                                                                  
      RETURN                                                                    
1     DO 20 K=1,NC                                                              
      WRITE(IOUT,400) J                                                         
      WRITE(IOUT,200) (ZMAT(J,K),J=1,NR)                                        
20    CONTINUE                                                                  
      RETURN                                                                    
100   FORMAT(//10X,A,I9/)                                                       
200   FORMAT(5X,12E10.4)                                                        
300   FORMAT(/'  RIGA  N. :',I8)                                                
400   FORMAT(/'  COLONNA  N. :',I8)                                             
      END                                                                       
C@ELT,I ANBA*ANBA.FACTOR                                                        
C*************************          FACTOR          ********************        
      SUBROUTINE FACTOR(A,NA,NEQ)                                               
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(1),NA(1)                                                      
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO                     
C                                                                               
      IERLOC =1                                                                 
      J=1                                                                       
C     IF(A(1).LE.0.D0)GOTO1000                                                  
      IF(DABS(A(1)).LE.ERR)THEN                                                 
      WRITE(IOUT,205)J,A(1),ERR                                                 
      A(1)=A(1)+CONST                                                           
      IERLOC=1                                                                  
      END IF                                                                    
      A(1)=1./A(1)                                                              
      IL=1                                                                      
      NAJP=NA(1)                                                                
      DO 90 J=2,NEQ                                                             
      NAJ=NA(J)                                                                 
      JK=NAJ-J                                                                  
      IF=1-JK+NAJP                                                              
      IF(IF.GE.J)GOTO80                                                         
      IF1=IF+1                                                                  
      IF(IF1.GT.IL)GOTO60                                                       
      JIA=2+NAJP                                                                
      NAIP=NA(IF)                                                               
      KL=IF+JK                                                                  
      DO 50 I=IF1,IL                                                            
      NAI=NA(I)                                                                 
      IK=NAI-I                                                                  
      II=I+1-NAI+NAIP                                                           
      IF(II.GE.I)GOTO40                                                         
      KF=MAX0(II,IF)+JK                                                         
      JJ=IK-JK                                                                  
      AA=0.D0                                                                   
      DO 30 K=KF,KL                                                             
30    AA=AA+A(K)*A(JJ+K)                                                        
      A(JIA)=A(JIA)-AA                                                          
40    JIA=JIA+1                                                                 
      KL=KL+1                                                                   
50    NAIP=NAI                                                                  
60    KF=JK+IF                                                                  
      KL=NAJ-1                                                                  
      AA=0.D0                                                                   
      DO 70 K=KF,KL                                                             
      NAI=NA(IF)                                                                
      CC=A(K)*A(NAI)                                                            
      AA=AA+A(K)*CC                                                             
      A(K)=CC                                                                   
70    IF=IF+1                                                                   
      A(NAJ)=A(NAJ)-AA                                                          
80    CONTINUE                                                                  
C     IF(A(NAJ).LE.0.) GO TO 1000                                               
      IF(DABS(A(NAJ)).LE.ERR)THEN                                               
      WRITE(IOUT,205)J,A(NAJ),ERR                                               
      A(NAJ)=A(NAJ)+CONST                                                       
      IERLOC=1                                                                  
      END IF                                                                    
      A(NAJ)=1./A(NAJ)                                                          
      IL=IL+1                                                                   
      NNN=NA(NEQ)                                                               
90    NAJP=NAJ                                                                  
      IF(IERLOC.NE.0.AND.IAUTO.EQ.1) THEN                                       
      WRITE(IOUT,1300)                                                          
      CALL JHALT                                                                
      STOP                                                                      
 1300 FORMAT(/' ***   AVVISO (FACTOR) : IL CALCOLO NON PROSEGUE',               
     * ' A CAUSA DELLE LABILITA'' RISCONTRATE   ***'/)                          
      END IF                                                                    
205   FORMAT(/' *** AVVISO (FACTOR): ESEGUITO AUTOSPC SU'                       
     1 ,' EQ. N.RO',I6,' IL CUI TERM. DIAG.',E12.6,'  E'' MINORE DI'            
     2   ,E12.6,'  ***')                                                        
      RETURN                                                                    
C+ 1000 WRITE(IOUT,10000)J                                                      
C+10000 FORMAT(' *** NEGATIVE OR ZERO PIVOT ENCOUNTERED AT EQUATION ',          
C+     1       I5,' ***')                                                       
C+      WRITE(IOUT,16000) (A(I),I=1,200)                                        
C+      NEQ=-NEQ                                                                
C+      RETURN                                                                  
C+16000  FORMAT(T20,'A   '/(10E12.3))                                           
      END                                                                       
C@ELT,I ANBA*ANBA.FEMADS                                                        
C*************************          FEMADS          ********************        
      SUBROUTINE FEMADS(A,NA,S,LM,ND,NRS)                                       
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(1),NA(1),S(NRS,1),LM(1)                                       
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      DO 20 I=1,ND                                                              
      II=LM(I)                                                                  
      IF(II.LE.0)GOTO20                                                         
      DO 30 J=I,ND                                                              
      JJ=LM(J)                                                                  
      IF(JJ.LE.0)GOTO30                                                         
      IJ=II-JJ                                                                  
      IF(IJ.LT.0)GOTO40                                                         
      JJ=II                                                                     
40    KK=NA(JJ)-IABS(IJ)                                                        
      A(KK)=A(KK)+S(I,J)                                                        
30    CONTINUE                                                                  
20    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.FORBAC                                                        
C*************************          FORBAC          ********************        
      SUBROUTINE FORBAC(A,B,NA,NEQ)                                             
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      DIMENSION A(1),B(1),NA(1)                                                 
      NEQQ=NEQ-1                                                                
      DO 10 N=1,NEQQ                                                            
      IF(B(N).NE.0.D0)GOTO20                                                    
10    CONTINUE                                                                  
      N=NEQQ                                                                    
20    N1=N+1                                                                    
      I1=N1+1                                                                   
      KL=N                                                                      
      NAIP=NA(N)                                                                
      DO 50 I=N1,NEQ                                                            
      NAI=NA(I)                                                                 
      II=I1-NAI+NAIP                                                            
      IF(II.GE.I)GOTO40                                                         
      KF=MAX0(II,N)                                                             
      IK=NAI-I                                                                  
      IKA=IK+KF                                                                 
      BB=0.D0                                                                   
      DO 30 K=KF,KL                                                             
      BB=BB+A(IKA)*B(K)                                                         
30    IKA=IKA+1                                                                 
      B(I)=B(I)-BB                                                              
  40  I1=I1+1                                                                   
      KL=KL+1                                                                   
50    NAIP=NAI                                                                  
      DO 60 I=N,NEQ                                                             
      NAI=NA(I)                                                                 
60    B(I)=B(I)*A(NAI)                                                          
      J=NEQ                                                                     
      NAJ=NA(NEQ)                                                               
      DO 90 I=1,NEQQ                                                            
      NAJP=NA(J-1)                                                              
      JKA=NAJP+1                                                                
      II=J-NAJ+JKA                                                              
      IF(II.GE.J)GOTO 80                                                        
      KL=J-1                                                                    
      BB=B(J)                                                                   
      DO 70 K=II,KL                                                             
      B(K)=B(K)-A(JKA)*BB                                                       
70    JKA=JKA+1                                                                 
80    J=J-1                                                                     
90    NAJ=NAJP                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.INVERT                                                        
C*************************          INVERT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INVERT(A,NDIMA,B,NDIMB,NEQ,PERM)                               
      REAL*8 A,B,PERM                                                           
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      DIMENSION A(NDIMA,1),B(NDIMB,1),PERM(NDIMA)                                   
      CALL RUFCT(A,PERM,NEQ,NDIMA,INDER)                                        
      IF(INDER.EQ.0) GOTO 3                                                     
      WRITE(IOUT,100) INDER                                                     
100   FORMAT(//' *** AVVISO (INVERT):LA MATRICE NON E'' INVERTI'                
     1  ,'BILE  ,INDER =',I6,'  ***')                                           
      CALL JHALT                                                                
      STOP                                                                      
3     NN=NDIMB*NEQ                                                              
      CALL DZERO(B,NN)                                                          
      DO 1 I=1,NEQ                                                              
      B(I,I)=1.                                                                 
1     CONTINUE                                                                  
      DO 2 I=1,NEQ                                                              
      CALL RUSOL(A,B(1,I),NDIMA,NEQ,PERM)                                       
2     CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.INVERT                                                        
C*************************          INVERT6          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INVERT6(A,NDIMA,B,NDIMB,NEQ,PERM)                               
      REAL*8 A,B,PERM,TMP 
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      DIMENSION A(NDIMA,1),B(NDIMB,1),PERM(NDIMA),TMP(6,6)                                   
      DO I=1,6                                                            
      DO l=1,6                                                            
      tmp(i,l)=a(i,l) 
      enddo  
      enddo  
      CALL RUFCT(tmp,PERM,NEQ,NDIMA,INDER)                                        
      IF(INDER.EQ.0) GOTO 3                                                     
      WRITE(IOUT,100) INDER                                                     
100   FORMAT(//' *** AVVISO (INVERT6):LA MATRICE NON E'' INVERTI'                
     1  ,'BILE  ,INDER =',I6,'  ***')                                           
      CALL JHALT                                                                
      STOP                                                                      
3     continue
      do i=1,6
      do l=1,6                                                              
      b(i,l)=0.d0
      enddo                                                          
      B(I,I)=1.                                                                 
      enddo                                                                  

      DO 2 I=1,NEQ                                                              
      CALL RUSOL(tmp,B(1,I),NDIMA,NEQ,PERM)                                       
2     CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C*************************          IPOSL          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      INTEGER FUNCTION IPOSL(IDEL,IDE,NELM)                                     
C                                                                               
C     ESEGUE UNA RICERCA LINEARE DEL VALORE DI -IDEL- NEL VETTORE -IDE-         
C     COMPOSTO DA -NELM- TERMINI                                                
C                                                                               
C     SE LA RICERCA FALLISCE IPOSL=0                                            
C                                                                               
      DIMENSION IDE(*)                                                          
      DO 10 IPO=1,NELM                                                          
      IF(IDEL.EQ.IDE(IPO)) THEN                                                 
        IPOSL=IPO                                                               
        RETURN                                                                  
      END IF                                                                    
10    CONTINUE                                                                  
      IPOSL=0                                                                   
      RETURN                                                                    
      END                                                                       
C*************************          IPOSZ          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      INTEGER FUNCTION IPOSZ(IDEL,IDE,NELM)                                     
C                                                                               
C     ESEGUE UNA RICERCA BINARIA DEL VALORE DI -IDEL- NEL VETTORE -IDE-         
C     COMPOSTO DA -NELM- TERMINI                                                
C                                                                               
C     SE LA RICERCA FALLISCE IPOSZ=0                                            
C                                                                               
      DIMENSION IDE(1)                                                          
      IVAL=IDEL                                                                 
      K=0                                                                       
      IPOSZ=0                                                                   
      N=NELM                                                                    
10    L=K+1                                                                     
20    IF(N.LT.L)RETURN                                                          
      K=(L+N)/2                                                                 
      M=IDE(K)                                                                  
      IF(IVAL.EQ.M)GOTO 30                                                      
      IF(IVAL.GT.M)GOTO 10                                                      
      N=K-1                                                                     
      GOTO 20                                                                   
30    IPOSZ=K                                                                   
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.MATPRT                                                        
C*************************          MATPRT         *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MATPRT(NA,RK,NEQV,NAME)                                        
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      CHARACTER*(*) NAME                                                        
      LOGICAL DUMP,INCORE                                                       
      DIMENSION NA(1),RK(1)                                                     
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
C                                                                               
C                                                                               
        CALL INTEST                                                             
          WRITE(IOUT,*) NAME                                                    
          WRITE(IOUT,500)1,1,1                                                  
          WRITE(IOUT,200)RK(1)                                                  
          DO 323 IO=2,NEQV                                                      
          WRITE(IOUT,500)IO,IO-NA(IO)+NA(IO-1)+1,IO                             
          WRITE(IOUT,200)(RK(IK),IK=NA(IO-1)+1,NA(IO))                          
 323      CONTINUE                                                              
C                                                                               
200       FORMAT(12(1X,E10.4))                                                  
500       FORMAT(' COLONNA N.RO ',I6,';  RIGHE DA',I6,'  A',I6)                 
C                                                                               
      RETURN                                                                    
                                                                                
      END                                                                       
C@ELT,I ANBA*ANBA.MOVE                                                          
C*************************          MOVE          **********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MOVE                                                           
      REAL*8 DVI,DVO                                                            
      DIMENSION IVI(1),IVO(1),RVI(1),RVO(1),DVI(1),DVO(1)                       
C                                                                               
C                                                                               
      ENTRY IMOVE(IVI,IVO,N)                                                    
C                                                                               
      DO 10 I=1,N                                                               
      IVO(I)=IVI(I)                                                             
 10   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY RMOVE(RVI,RVO,N)                                                    
C                                                                               
      DO 20 I=1,N                                                               
      RVO(I)=RVI(I)                                                             
 20   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY DMOVE(DVI,DVO,N)                                                    
C                                                                               
      DO 30 I=1,N                                                               
      DVO(I)=DVI(I)                                                             
 30   CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.PRIPAG                                                        
C*************************          PRIPAG          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PRIPAG                                                         
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      WRITE(IOUT,10)                                                            
      WRITE(IOUT,28)                                                            
      WRITE(IOUT,40)                                                            
      WRITE(IOUT,11)                                                            
      WRITE(IOUT,27)                                                            
      WRITE(IOUT,29)                                                            
      WRITE(IOUT,30)                                                            
      WRITE(IOUT,31)                                                            
      WRITE(IOUT,32)                                                            
      WRITE(IOUT,33)                                                            
      WRITE(IOUT,34)                                                            
      WRITE(IOUT,35)                                                            
      WRITE(IOUT,36)                                                            
      WRITE(IOUT,37)                                                            
      WRITE(IOUT,38)                                                            
      WRITE(IOUT,39)                                                            
      WRITE(IOUT,29)                                                            
      WRITE(IOUT,27)                                                            
      WRITE(IOUT,12)                                                            
      WRITE(IOUT,9)                                                             
    9 FORMAT( //,46X,'''AN''ISOTROPIC     ''B''EAM     ''A''NALYSIS')           
   10 FORMAT(1H1,///,15X,'COSTRUZIONI AERONAUTICHE',51X,'POLITECNICO DI'        
     1,' MILANO')                                                               
   11 FORMAT(56X,17('M'),/,50X,29('M'),/,47X,35('M'),/,44X,41('M'),/,42X        
     1,45('M'),/,40X,49('M'),/,38X,53('M'),/,36X,57('M'),/,35X,59('M'),/        
     2 ,34X,61('M'),/,33X,63('M'),/,32X,65('M'),T114,'ANBA     2'/              
     3  31X,67('M'),/,31X,67('M'),T114,'VERSIONE 5'/                            
     4  30X,69('M'),T114,'LIVELLO  02/3/87'/)                                   
 12   FORMAT(/30X,69('M'),                                                      
     *       /31X,67('M'),                                                      
     *       /31X,67('M'),                                                      
     *       /32X,65('M'),                                                      
     *       /33X,63('M'),                                                      
     *       /34X,61('M'),                                                      
     *       /35X,59('M'),                                                      
     *       /36X,57('M'),                                                      
     *       /38X,53('M'),                                                      
     *       /40X,49('M'),                                                      
     *       /42X,45('M'),                                                      
     *       /44X,41('M'),                                                      
     *       /47X,35('M'),                                                      
     *       /50X,29('M'),                                                      
     *       /56X,17('M'))                                                      
   27 FORMAT(26X,77('O'))                                                       
   28 FORMAT(23X,'G. AGUSTA',62X,'DIPARTIMENTO')                                
   29 FORMAT(26X,'OOO',71X,'OOO')                                               
   30 FORMAT(26X,'OOO',7X,'AA',7X,'NNNN',4X,'NNNN',                             
     1 2X,9('B'),9X,'AA',7X,3X,6('2'),3X,2X,'OOO')                              
   31 FORMAT(26X,'OOO',6X,'AAAA',7X,'NNN',5X,'NN',4X,                           
     1 9('B'),7X,'AAAA',6X,1X,3('2'),4X,3('2'),1X,2X,'OOO')                     
   32 FORMAT(26X,'OOO',5X,'AA',2X,'AA',6X,'NNNN',4X,'NN'                        
     1,4X,'BB',5X,'BBB',5X,'AA',2X,'AA',5X,3('2'),6X,3('2'),2X,'OOO')           
   33 FORMAT(26X,'OOO',4X,'AA',4X,'AA',5X,5('N'),3X,'NN'                        
     1,4X,'BB',5X,'BBB',4X,'AA',4X,'AA',4X,3('2'),6X,3('2'),2X,'OOO')           
   34 FORMAT(26X,'OOO',4X,'AA',4X,'AA',5X,'NN NNN  NN',4X,                      
     1 9('B'),5X,'AA',4X,'AA',4X,7X,4('2'),1X,2X,'OOO')                         
   35 FORMAT(26X,'OOO',3X,10('A'),4X,'NN  NNN NN',4X,9('B'),                    
     1 4X,10('A'),7X,4('2'),6X,'OOO')                                           
   36 FORMAT(26X,'OOO',3X,10('A'),4X,'NN',3X,'NNNNN',4X,                        
     1'BB',5X,'BBB',3X,10('A'),3X,2X,3('2'),5X,4X,'OOO')                        
   37 FORMAT(26X,'OOO',3X,'AA',6X,'AA',4X,'NN',4X,'NNNN'                        
     1,4X,'BB',5X,'BBB',3X,'AA',6X,'AA',4X,3('2'),8X,2X,'OOO')                  
   38 FORMAT(26X,'OOO',3X,'AA',6X,'AA',4X,'NN',5X,'NNN',                        
     14X,9('B'),4X,'AA',6X,'AA',3X,3('2'),6X,3('2'),2X,'OOO')                   
   39 FORMAT(26X,'OOO',2X,'AAAA',4X,'AAAA',2X,'NNNN'                            
     1,4X,'NNNN',2X,9('B'),4X,'AAAA',4X,'AAAA',2X,12('2'),2X,'OOO')             
   40 FORMAT(13X,'DIREZIONE RICERCHE E PROGETTI',46X,'DI INGEGNERIA AERO        
     1SPAZIALE')                                                                
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.RCPSOL                                                        
C*************************          RCPSOL          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RCPSOL(X,NRX,NSOL)                                             
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION  X(NRX,1)                                                       
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1             NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                    
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      DO 10 I=1,NSOL                                                            
      CALL JREAD(ISLSL2,X(1,I),NEQV+NEQV)                                       
      CALL JREAD(ISLSL1,X(NEQV+1,I),(NEQV-NPSI)*2)                              
      CALL JPOSRL(ISLSL1,NPSI+NPSI)                                             
10    CONTINUE                                                                  
      RETURN                                                                    
      ENTRY RCPSLE(X,NRX,NSOL,APREA,APIMM,ITIPO,INAUTV)                         
      NW=NEQV*4-NPSI*2                                                          
      CALL JREAD(ISLSLE,APREA,2)                                                
      CALL JREAD(ISLSLE,APIMM,2)                                                
      CALL JREAD(ISLSLE,ITIPO,1)                                                
      DO 30 I=1,NSOL                                                            
      CALL JREAD(ISLSLE,X(1,I),NW)                                              
      CALL JPOSRL(ISLSLE,NPSI*2)                                                
 30   CONTINUE                                                                  
      WRITE(IOUT,100)INAUTV,APREA,APIMM                                         
 100  FORMAT(////10X,'***  AUTOVALORE   N.RO ',I6,10X,'PARTE '                  
     1 ,'REALE=',E15.6,10X,'PARTE IMMAG.=',E15.6)                               
      WRITE(IOUT,110)(X(L,1),L=1,NEQV)                                          
      WRITE(IOUT,120)(X(L,2),L=1,NEQV)                                          
 110  FORMAT(///10X,'PARTE REALE DELL''AUTOVETTORE'///,20(12E10.4/))            
 120  FORMAT(///10X,'PARTE IMMAGINARIA DELL''AUTOVETTORE'///,20(12E10.4/        
     1))                                                                        
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.RUFCT                                                         
C*************************          RUFCT          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RUFCT(A,PERM,N,NR,INDER)                                       
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION A(NR,1),PERM(1)                                                 
      DO10I=1,N                                                                 
      X=0.D0                                                                    
      DO20K=1,N                                                                 
      IF(DABS(A(I,K)).LT.X)GOTO20                                               
      X=DABS(A(I,K))                                                            
20    CONTINUE                                                                  
      IF(X.EQ.0.D0)GOTO110                                                      
      PERM(I)=1./X                                                              
10    CONTINUE                                                                  
      DO100 I=1,N                                                               
      IM1=I-1                                                                   
      IP1=I+1                                                                   
      IPVT=I                                                                    
      X=0.D0                                                                    
      DO50K=I,N                                                                 
      DP=A(K,I)                                                                 
      IF(I.EQ.1)GOTO40                                                          
      DO30J=1,IM1                                                               
      DP=DP-A(K,J)*A(J,I)                                                       
30    CONTINUE                                                                  
      A(K,I)=DP                                                                 
40    IF(X.GT.(DABS(DP)*PERM(K)))GOTO50                                         
      IPVT=K                                                                    
      X=DABS(DP)*PERM(K)                                                        
50    CONTINUE                                                                  
      IF(X.LE.0.)GOTO110                                                        
      IF(IPVT.EQ.I)GOTO70                                                       
      DO60J=1,N                                                                 
      X=A(IPVT,J)                                                               
      A(IPVT,J)=A(I,J)                                                          
      A(I,J)=X                                                                  
60    CONTINUE                                                                  
      PERM(IPVT)=PERM(I)                                                        
70    PERM(I)=IPVT                                                              
      IF(I.EQ.N)GOTO100                                                         
      X=A(I,I)                                                                  
      DO90K=IP1,N                                                               
      A(K,I)=A(K,I)/X                                                           
      IF(I.EQ.1)GOTO90                                                          
      DP=A(I,K)                                                                 
      DO80J=1,IM1                                                               
      DP=DP-A(I,J)*A(J,K)                                                       
80    CONTINUE                                                                  
      A(I,K)=DP                                                                 
90    CONTINUE                                                                  
100   CONTINUE                                                                  
      INDER=0                                                                   
      RETURN                                                                    
110   INDER=I                                                                   
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.DCUFCT                                                        
C*************************          DCUFCT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DCUFCT(A,PERM,N,NRDIM,INDER)                                   
      DOUBLE PRECISION PERM,X                                                   
      COMPLEX*16 A,DP,C                                                         
      DIMENSION A(NRDIM,1),PERM(1)                                              
      INDER=0                                                                   
      IF(N.GT.1)GOTO2                                                           
      IF(CDABS(A(1,1)).EQ.0.D0)INDER=1                                          
      RETURN                                                                    
2     CONTINUE                                                                  
      DO10I=1,N                                                                 
      X=0.                                                                      
      DO20K=1,N                                                                 
      IF(CDABS(A(I,K)).LT.X)GOTO20                                              
      X=CDABS(A(I,K))                                                           
20    CONTINUE                                                                  
      IF(X.EQ.0.)GOTO110                                                        
      PERM(I)=1./X                                                              
10    CONTINUE                                                                  
      DO100 I=1,N                                                               
      IM1=I-1                                                                   
      IP1=I+1                                                                   
      IPVT=I                                                                    
      X=0.                                                                      
      DO50K=I,N                                                                 
      DP=A(K,I)                                                                 
      IF(I.EQ.1)GOTO40                                                          
      DO30J=1,IM1                                                               
      DP=DP-A(K,J)*A(J,I)                                                       
30    CONTINUE                                                                  
      A(K,I)=DP                                                                 
40    IF(X.GT.(CDABS(A(K,I))*PERM(K)))GOTO50                                    
      IPVT=K                                                                    
      X=CDABS(A(K,I))*PERM(K)                                                   
50    CONTINUE                                                                  
      IF(X.LE.0.)GOTO110                                                        
      IF(IPVT.EQ.I)GOTO70                                                       
      DO60J=1,N                                                                 
      C=A(IPVT,J)                                                               
      A(IPVT,J)=A(I,J)                                                          
      A(I,J)=C                                                                  
60    CONTINUE                                                                  
      PERM(IPVT)=PERM(I)                                                        
70    PERM(I)=IPVT                                                              
      IF(I.EQ.N)GOTO100                                                         
      C=A(I,I)                                                                  
      DO90K=IP1,N                                                               
      A(K,I)=A(K,I)/C                                                           
      IF(I.EQ.1)GOTO90                                                          
      DP=A(I,K)                                                                 
      DO80J=1,IM1                                                               
      DP=DP-A(I,J)*A(J,K)                                                       
80    CONTINUE                                                                  
      A(I,K)=DP                                                                 
90    CONTINUE                                                                  
100   CONTINUE                                                                  
      RETURN                                                                    
110   INDER=1                                                                   
      RETURN                                                                    
      END                                                                       
                                                                                
C@ELT,I ANBA*ANBA.RUSOL                                                         
C*************************          RUSOL          *********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RUSOL(A,B,NR,N,PERM)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION A(NR,1),B(1),PERM(1)                                            
      IF(N.GT.1)GOTO 2                                                          
      B(1)=B(1)/A(1,1)                                                          
      RETURN                                                                    
2     DO10I=1,N                                                                 
      K=PERM(I)                                                                 
      IF(K.EQ.I)GOTO10                                                          
      X=B(K)                                                                    
      B(K)=B(I)                                                                 
      B(I)=X                                                                    
10    CONTINUE                                                                  
      DO20I=2,N                                                                 
      IM1=I-1                                                                   
      DP=B(I)                                                                   
      DO40K=1,IM1                                                               
      DP=DP-A(I,K)*B(K)                                                         
40    CONTINUE                                                                  
      B(I)=DP                                                                   
20    CONTINUE                                                                  
      B(N)=B(N)/A(N,N)                                                          
      DO60I=2,N                                                                 
      IM1=N-I+1                                                                 
      INF=IM1+1                                                                 
      DP=B(IM1)                                                                 
      DO70K=INF,N                                                               
      DP=DP-A(IM1,K)*B(K)                                                       
70    CONTINUE                                                                  
      B(IM1)=DP/A(IM1,IM1)                                                      
60    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C*************************          DCUSOL          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE DCUSOL(A,B,NR,N,PERM)                                          
      COMPLEX*16 DP,A,B                                                         
      DOUBLE PRECISION PERM                                                     
      DIMENSION A(NR,1),B(1),PERM(1)                                            
      IF(N.GT.1)GOTO2                                                           
      B(1)=B(1)/A(1,1)                                                          
      RETURN                                                                    
2     CONTINUE                                                                  
      DO10I=1,N                                                                 
      K=PERM(I)                                                                 
      IF(K.EQ.I)GOTO10                                                          
      DP=B(K)                                                                   
      B(K)=B(I)                                                                 
      B(I)=DP                                                                   
10    CONTINUE                                                                  
      DO20I=2,N                                                                 
      IM1=I-1                                                                   
      DP=B(I)                                                                   
      DO40K=1,IM1                                                               
      DP=DP-A(I,K)*B(K)                                                         
40    CONTINUE                                                                  
      B(I)=DP                                                                   
20    CONTINUE                                                                  
      B(N)=B(N)/A(N,N)                                                          
      DO60I=2,N                                                                 
      IM1=N-I+1                                                                 
      INF=IM1+1                                                                 
      DP=B(IM1)                                                                 
      DO70K=INF,N                                                               
      DP=DP-A(IM1,K)*B(K)                                                       
70    CONTINUE                                                                  
      B(IM1)=DP/A(IM1,IM1)                                                      
60    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.SHLSRT                                                        
C*************************          SHLSRT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE SHLSRT(NT,IV,RM,ND,NROC,IFL)                                   
C                                                                               
C     ROUTINE DI ORDINAMENTO DEL VETTORE INTERO -IV- DI -NT- TERMINI            
C                                                                               
C     SE  IFL=1  VIENE ORDINATO SOLO IL VETTORE -IV-                            
C         IFL=2  SULLA STESSA BASE VENGONO ORDINATE -NROC- RIGHE DELLA          
C                MATRICE -RM-                                                   
C         IFL=3  SULLA STESSA BASE VENGONO ORDINATE -NROC- COLONNE DELLA        
C                MATRICE -RM-                                                   
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION IV(1),RM(ND,1)                                                  
      IF(IFL.EQ.1) ASSIGN 1 TO JUMP                                             
      IF(IFL.EQ.2) ASSIGN 2 TO JUMP                                             
      IF(IFL.EQ.3) ASSIGN 3 TO JUMP                                             
      M=NT                                                                      
10    M=M/2                                                                     
      IF(M.EQ.0)RETURN                                                          
      K=NT-M                                                                    
      J=1                                                                       
20    I=J                                                                       
30    N=I+M                                                                     
      IF(IV(I).LE.IV(N))GOTO 40                                                 
      L=IV(I)                                                                   
      IV(I)=IV(N)                                                               
      IV(N)=L                                                                   
      GOTO JUMP,(1,2,3)                                                         
2     DO 50 IS=1,NROC                                                           
      R=RM(IS,I)                                                                
      RM(IS,I)=RM(IS,N)                                                         
      RM(IS,N)=R                                                                
50    CONTINUE                                                                  
      GOTO 1                                                                    
3     DO 60 IS=1,NROC                                                           
      R=RM(I,IS)                                                                
      RM(I,IS)=RM(N,IS)                                                         
      RM(N,IS)=R                                                                
60    CONTINUE                                                                  
1     I=I-M                                                                     
      IF(I.GE.1)GOTO 30                                                         
40    J=J+1                                                                     
      IF(J.GT.K) GOTO 10                                                        
      GOTO 20                                                                   
      END                                                                       
C@ELT,I ANBA*ANBA.SHLSRi                                                        
C*************************          SHLSRi          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE SHLSRi(NT,IV,MM,ND,NROC,IFL)                                   
C                                                                               
C     ROUTINE DI ORDINAMENTO DEL VETTORE INTERO -IV- DI -NT- TERMINI            
C                                                                               
C     SE  IFL=1  VIENE ORDINATO SOLO IL VETTORE -IV-                            
C         IFL=2  SULLA STESSA BASE VENGONO ORDINATE -NROC- RIGHE DELLA          
C                MATRICE -M-                                                    
C         IFL=3  SULLA STESSA BASE VENGONO ORDINATE -NROC- COLONNE DELLA        
C                MATRICE -M-                                                    
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION IV(1),MM(ND,1)                                                  
      IF(IFL.EQ.1) ASSIGN 1 TO JUMP                                             
      IF(IFL.EQ.2) ASSIGN 2 TO JUMP                                             
      IF(IFL.EQ.3) ASSIGN 3 TO JUMP                                             
      M=NT                                                                      
10    M=M/2                                                                     
      IF(M.EQ.0)RETURN                                                          
      K=NT-M                                                                    
      J=1                                                                       
20    I=J                                                                       
30    N=I+M                                                                     
      IF(IV(I).LE.IV(N))GOTO 40                                                 
      L=IV(I)                                                                   
      IV(I)=IV(N)                                                               
      IV(N)=L                                                                   
      GOTO JUMP,(1,2,3)                                                         
2     DO 50 IS=1,NROC                                                           
      LR=MM(IS,I)                                                               
      MM(IS,I)=MM(IS,N)                                                         
      MM(IS,N)=LR                                                               
50    CONTINUE                                                                  
      GOTO 1                                                                    
3     DO 60 IS=1,NROC                                                           
      LR=MM(I,IS)                                                               
      MM(I,IS)=MM(N,IS)                                                         
      MM(N,IS)=LR                                                               
60    CONTINUE                                                                  
1     I=I-M                                                                     
      IF(I.GE.1)GOTO 30                                                         
40    J=J+1                                                                     
      IF(J.GT.K) GOTO 10                                                        
      GOTO 20                                                                   
      END                                                                       
C@ELT,I ANBA*ANBA.WRSOLC                                                        
C*************************          WRSOLC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE WRSOLC                                                         
      REAL*8 DV(1),xs,ys,ss,cs                                                  
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JCRMNG/ MAXDIM                                                    
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      common /sisrif/ xs,ys,ss,cs,iflsr,iflsro,iflsrs                           
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI     ,JLABEL)                  
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'X       ','D.P.',1,NEQV*NPSI ,JX    )                  
      CALL JOPEN('*SL2    ',ISLSL2,1)                                           
      CALL JREAD(ISLSL2,DV(JX),NEQV*NPSI*2)                                     
      CALL JCLOSE(ISLSL2)                                                       
C                                                                               
      CALL JOPEN('*SNO    ',ISLSNO,1)                                           
      CALL JREAD(ISLSNO,NNSEL,1)                                                
      CALL JALLOC(*2000,'NODSEL  ','INT.',1,NNSEL     ,JNODSL)                  
      IF(NNSEL.NE.0) CALL JREAD(ISLSNO,IV(JNODSL),NNSEL)                        
      CALL JCLOSE(ISLSNO)                                                       
      I=0                                                                       
      IUNO=1                                                                    
      NNS=NNSEL                                                                 
      IF(NNSEL.EQ.0) NNS=NNODI                                                  
      NEQVM1=NEQV-1                                                             
      NBL=(NNS+11)/12                                                           
      NN=12                                                                     
      DO 50 IW=1,NBL                                                            
      CALL INTEST                                                               
      WRITE(IOUT,430)                                                           
      IF(IW.EQ.NBL) NN=NNS-(NBL-1)*NN                                           
      DO 51 LL=1,NN                                                             
  77  I=I+1                                                                     
      I1=IV(JLABEL+I-1)                                                         
      IF(NNSEL.NE.0) THEN                                                       
       IPNOD=IPOSL(I1,IV(JNODSL),NNSEL)                                         
       IF(IPNOD.EQ.0) GOTO 77                                                   
      END IF                                                                    
      I2=IV(JNUINT+I-1)                                                         
      I7=JNUME+(I2-1)*NCS                                                       
      IF(IV(I7+1).EQ.0) GOTO 50                                                 
      I3=IV(I7)                                                                 
      IND=JX-1+I3                                                               
      WRITE(IOUT,440) I1,IUNO,(DV(IND+(L-1)*NEQV),L=1,NPSI)                     
      DO 60 M=2,NCS                                                             
       I7=I7+1                                                                  
       I3=IV(I7)                                                                
       IND=JX+I3-1                                                              
       WRITE(IOUT,441)  M,(DV(IND+(L-1)*NEQV),L=1,NPSI)                         
  60  CONTINUE                                                                  
  51  CONTINUE                                                                  
  50  CONTINUE                                                                  
      IF(NMPC.EQ.0) GOTO 75                                                     
       CALL INTEST                                                              
       WRITE(IOUT,445)                                                          
       DO 70 I=1,NMPC                                                           
        I1=IV(JNUINT+NNODI+I-1)                                                 
        I2=IV(JNUME+(I1-1)*NCS)                                                 
        IND=JX+I2-1                                                             
        WRITE(IOUT,450) (DV(IND+(L-1)*NEQV),L=1,NPSI)                           
  70   CONTINUE                                                                 
  75   CONTINUE                                                                 
      CALL INTEST                                                               
      WRITE(IOUT,460)                                                           
      IL=NNODI*NCS+NMPC                                                         
      DO 80 I=IL,NEQVM1                                                         
      IND=JX+I                                                                  
      WRITE(IOUT,450) (DV(IND+(L-1)*NEQV),L=1,NPSI)                             
 80   CONTINUE                                                                  
 430  FORMAT(/53X,'***  SOLUZIONI : INGOBBAMENTI  ***'//                        
     *  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
 460  FORMAT(/47X,'***  SOLUZIONI : PARAMETRI DI DEFORMAZIONE  ***'//           
     *  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
 445  FORMAT(/46X,'***  SOLUZIONI : MOLTIPLICATORI DI LAGRANGE  ***'//          
     *  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
 440  FORMAT(/1X,I8,I6,6E19.10)                                                 
 441  FORMAT(9X,I6,6E19.10)                                                     
 450  FORMAT(15X,6E19.10)                                                       
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.WRSOLNAS                                                      
C*************************          WRSOLNAS          ********************      
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE WRSOLNAS                                                       
      implicit real*8 (a-h,o-z)                                                 
      REAL*8 DV(1)                                                              
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JCRMNG/ MAXDIM                                                    
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI     ,JLABEL)                  
      CALL JALLOC(*2000,'COOR    ','D.P.',1,2*NNODI     ,JCOOR )                
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JREAD(ISLNOD,DV(JCOOR),4*NNODI)                                      
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'X       ','D.P.',1,NEQV*NPSI ,JX    )                  
      CALL JALLOC(*2000,'XP      ','D.P.',1,NEQV*NPSI ,JXP   )                  
      CALL JOPEN('*SL2    ',ISLSL2,1)                                           
      CALL JOPEN('*SL1    ',ISLSL1,1)                                           
C                                                                               
      do idsol=1,6                                                              
      CALL JREAD(ISLSL2,DV(JX),NEQV*2)                                          
      if(idsol.le.2) then                                                       
       CALL JREAD(ISLSL1,DV(JXP),NEQV*2)                                        
      else                                                                      
       do iol=0,neqv-1                                                          
       dv(jxp+iol)=0.d0                                                         
       enddo                                                                    
      endif                                                                     
      call prsolnas(nnodi,iv(jlabel),dv(jcoor),iv(jnuint),iv(jnume),            
     +              dv(jx),dv(jxp),idsol)                                       
      enddo                                                                     
      CALL JCLOSE(ISLSL2)                                                       
      CALL JCLOSE(ISLSL1)                                                       
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
                                                                                
      subroutine prsolnas(nodi,label,coor,nuint,nume,x,xp,idsol)                
      implicit real*8 (a-h,o-z)                                                 
      CHARACTER IDESC*80                                                        
      dimension label(1),coor(2,1),nuint(1),nume(1),x(1),xp(1)                  
      dimension ind(3),sol(3),sold(3),t(3,3)                                    
      character*20 nomecaso(6)                                                  
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /TESTA/  IDESC(2)                                                  
      common /sisrif/ xs,ys,ss,cs,iflsr,iflsro,iflsrs                           
      common /outnas/ nasmod,naspch                                             
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat                            
      data nomecaso/'TAGLIO            TX',                                     
     *              'TAGLIO            TY',                                     
     *              'AZIONE ASSIALE    NZ',                                     
     *              'MOMENTO FLETTENTE MX',                                     
     *              'MOMENTO FLETTENTE MY',                                     
     *              'MOMENTO TORCENTE  MZ'/                                     
                                                                                
      ishlab=0                                                                  
                                                                                
c      do i=1,nnodi                                                             
c      Ilab =LABEL(I)                                                           
c      Inod =NUINT(I)                                                           
c      Ieq  =(Inod-1)*NCS                                                       
c      DO  M=1,NCS                                                              
c       ipn=NUME(ieq+m)                                                         
c       sol(m)=x(ipn)                                                           
c       sold(m)=xp(ipn)                                                         
c       enddo                                                                   
c       WRITE(iout,'(i8,6e12.5)')Ilab,(sol(lo),lo=1,3),(sold(lo),lo=1,3)        
c      enddo                                                                    
                                                                                
      do ifaccia=1,nstrat+1                                                     
                                                                                
      I=0                                                                       
      NNS=NNodi                                                                 
      NBL=(NNS+49)/50                                                           
      NN=50                                                                     
      DO 50 IW=1,NBL      
      if(iw.eq.1) then                                                     
       WRITE(naspch,105) idesc(1)(1:70),iw                                      
       WRITE(naspch,103) idesc(2)(1:70),idsol                                   
       WRITE(naspch,104) nomecaso(idsol)                                        
       WRITE(naspch,102)                                                        
       WRITE(naspch,101)  1,2,3,1,2,3 
      endif                                          
      IF(IW.EQ.NBL) NN=NNS-(NBL-1)*NN                                           
      DO 51 LL=1,NN                                                             
  77  I=I+1                                                                     
      Ilab =LABEL(I)                                                            
      Inod =NUINT(I)                                                            
      Ieq  =(Inod-1)*NCS                                                        
      IF(nume(Ieq+1).EQ.0) GOTO 50                                              
      DO 60 M=1,NCS                                                             
       ipn=NUME(ieq+m)                                                          
       sol(m)=x(ipn)                                                            
       sold(m)=xp(ipn)                                                          
  60  CONTINUE                                                                  
                                                                                
       if( ifaccia.gt.1) then                                                   
        do ir=1,3                                                               
        do ic=1,3                                                               
          t(ir,ic)=0.d0                                                         
        enddo                                                                   
        enddo                                                                   
        t(1,2)=0                                                                
        t(1,3)=coor(2,i)                                                        
        t(2,3)=-coor(1,i)                                                       
        t(2,1)=-t(1,2)                                                          
        t(3,1)=-t(1,3)                                                          
        t(3,2)=-t(2,3)                                                          
        dx=concio*(ifaccia-1)                                                   
        do m=1,ncs                                                              
         sol(m)=sol(m)+sold(m)*dx                                               
        enddo                                                                   
        IL=NNODI*NCS+NMPC                                                       
        do m=1,3                                                                
          sol(m)=sol(m)-x(il+m)*dx                                              
        enddo                                                                   
        il=il+3                                                                 
        sol(1)=sol(1)-x(il+3)*dx*t(1,3)                                         
        sol(2)=sol(2)-x(il+1)*dx*t(2,1)-x(il+3)*dx*t(2,3)                       
        sol(3)=sol(3)-x(il+1)*dx*t(3,1)-x(il+2)*dx*t(3,2)                       
       endif                                                                    
                                                                                
                                                                                
                                                                                
                                                                                
       WRITE(naspch,66)  Ilab+ishlab,(sol(lo),lo=1,3)                           
  51  CONTINUE                                                                  
  50  CONTINUE                                                                  
      ishlab=ishlab+10000000                                                    
      enddo                                                                     
      WRITE(naspch,*)  ' '                           
                                                                                
105   format('',5x,a70,4x,'JULY  16, 1997   MSC/NASTRAN 11/14/95    ',         
     +       'PAGE',i7)                                                         
103   format(5x,a70,33x,'SUBCASE',i3)                                           
104   format(5x,a)                                                              
                                                                                
102   format(t47,'D I S P L A C E M E N T   V E C T O R')                       
101   format(t6,'POINT ID.',t18,'TYPE',3(10x,'T',i1,3x),                        
     *     3(10x,'R',i1,3x))                                                    
66    format(1x,i12,6x,'G',3x,6e15.6)                                           
                                                                                
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.PCHSLC                                                        
C*************************          PCHSLC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE PCHSOL(file,nsol)                                              
      character*(*) file                                                        
      REAL*8 DV(1)                                                              
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JCRMNG/ MAXDIM                                                    
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      common /outnas/ nasmod,naspch                                             
      dimension vsol(6)                                                         
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI     ,JLABEL)                  
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'X       ','D.P.',1,NEQV ,JX    )                       
C                                                                               
      CALL JOPEN('*SNO    ',ISLSNO,1)                                           
      CALL JREAD(ISLSNO,NNSEL,1)                                                
      CALL JALLOC(*2000,'NODSEL  ','INT.',1,NNSEL     ,JNODSL)                  
      IF(NNSEL.NE.0) CALL JREAD(ISLSNO,IV(JNODSL),NNSEL)                        
      CALL JCLOSE(ISLSNO)                                                       
                                                                                
                                                                                
      CALL JOPEN(file,ISLSL2,1)                                                 
      DO 7778 icdc=1,nsol                                                       
      WRITE(naspch,'(A,i8)') '$ SUBCASE ',icdc                                  
      CALL JREAD(ISLSL2,DV(JX),NEQV*2)                                          
      I=0                                                                       
      IUNO=1                                                                    
      DO 51 I=1,NNODI                                                           
      I1=IV(JLABEL+I-1)                                                         
      I2=IV(JNUINT+I-1)                                                         
      I7=JNUME+(I2-1)*NCS                                                       
      IF(IV(I7+1).EQ.0) GOTO 51                                                 
      DO 60 M=1,NCS                                                             
       I3=IV(I7)                                                                
       IND=JX+I3-1                                                              
       VSOL(M)=DV(IND)                                                          
       I7=I7+1                                                                  
  60  CONTINUE                                                                  
      write(naspch,722)i1,(vsol(iop),iop=1,3)                                   
      write(naspch,723)0.,0.,0.                                                 
722   format(i10,'       G',3e18.6)                                             
723   format('-CONT-',12x,3e18.6)                                               
  51  CONTINUE                                                                  
7778  CONTINUE                                                                  
      call jclose(islsl2)                                                       
                                                                                
 440  FORMAT(/1X,I8,I6,6E19.10)                                                 
 441  FORMAT(9X,I6,6E19.10)                                                     
 450  FORMAT(15X,6E19.10)                                                       
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C                                                                               
C                                                                               
C@ELT,I ANBA*ANBA.PCHSLC                                                        
C*************************          F06SLC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE F06SOL(file,nsol)                                              
      character*(*) file                                                        
      REAL*8 DV(1)                                                              
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JCRMNG/ MAXDIM                                                    
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      common /outnas/ nasmod,naspch                                             
      dimension vsol(6)                                                         
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI     ,JLABEL)                  
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'X       ','D.P.',1,NEQV ,JX    )                       
C                                                                               
      CALL JOPEN('*SNO    ',ISLSNO,1)                                           
      CALL JREAD(ISLSNO,NNSEL,1)                                                
      CALL JALLOC(*2000,'NODSEL  ','INT.',1,NNSEL     ,JNODSL)                  
      IF(NNSEL.NE.0) CALL JREAD(ISLSNO,IV(JNODSL),NNSEL)                        
      CALL JCLOSE(ISLSNO)                                                       
                                                                                
                                                                                
      CALL JOPEN(file,ISLSL2,1)                                                 
      DO 7778 icdc=1,nsol                                                       
	  WRITE(IOUT,105) icdc                                                         
	  WRITE(IOUT,103) icdc                                                         
	  WRITE(IOUT,102)                                                              
	  WRITE(IOUT,101)  1,2,3,1,2,3                                                 
105   format('',79x,'JULY  16, 1997   CSA/NASTRAN 11/14/95    PAGE',i7)        
104   format(4x,a)                                                              
103   format(108x,'SUBCASE',i3)                                                 
102   format(t47,'D I S P L A C E M E N T   V E C T O R')                       
101   format(t6,'POINT ID.',t18,'TYPE',3(10x,'T',i1,3x),                        
     *     3(10x,'R',i1,3x))                                                    
      WRITE(naspch,'(A,i8)') '$ SUBCASE ',icdc                                  
      CALL JREAD(ISLSL2,DV(JX),NEQV*2)                                          
      I=0                                                                       
      IUNO=1                                                                    
      DO 51 I=1,NNODI                                                           
      I1=IV(JLABEL+I-1)                                                         
      I2=IV(JNUINT+I-1)                                                         
      I7=JNUME+(I2-1)*NCS                                                       
      IF(IV(I7+1).EQ.0) GOTO 51                                                 
      DO 60 M=1,NCS                                                             
       I3=IV(I7)                                                                
       IND=JX+I3-1                                                              
       VSOL(M)=DV(IND)                                                          
       I7=I7+1                                                                  
  60  CONTINUE                                                                  
      write(naspch,722)i1,(vsol(iop),iop=1,3)                                   
      write(naspch,723)0.,0.,0.                                                 
722   format(i10,'       G',3e18.6)                                             
723   format('-CONT-',12x,3e18.6)                                               
                                                                                
      WRITE(IOUT,100) i1,( VSOL(L),L=1,3)                                       
100   format(1x,i12,6x,'G',3x,6e15.6)                                           
                                                                                
  51  CONTINUE                                                                  
7778  CONTINUE                                                                  
      call jclose(islsl2)                                                       
                                                                                
 440  FORMAT(/1X,I8,I6,6E19.10)                                                 
 441  FORMAT(9X,I6,6E19.10)                                                     
 450  FORMAT(15X,6E19.10)                                                       
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C                                                                               
C                                                                               
C@ELT,I ANBA*ANBA.WRSOLT                                                        
C*************************          WRSOLT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE WRSOLT                                                         
      REAL*8 DV(1),xs,ys,ss,cs                                                  
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD                  
      COMMON /JCRMNG/ MAXDIM                                                    
      common /sisrif/ xs,ys,ss,cs,iflsr,iflsro,iflsrs                           
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'LABEL   ','INT.',1,NNODI     ,JLABEL)                  
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JREAD(ISLNOD,IV(JLABEL),NNODI)                                       
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
      CALL JALLOC(*2000,'X       ','D.P.',1,NEQV ,JX    )                       
      CALL JOPEN('*SLTERM ',ISLSL2,1)                                           
      CALL JREAD(ISLSL2,DV(JX),NEQV*2)                                          
      CALL JCLOSE(ISLSL2)                                                       
C                                                                               
      CALL JOPEN('*SNO    ',ISLSNO,1)                                           
      CALL JREAD(ISLSNO,NNSEL,1)                                                
      CALL JALLOC(*2000,'NODSEL  ','INT.',1,NNSEL     ,JNODSL)                  
      IF(NNSEL.NE.0) CALL JREAD(ISLSNO,IV(JNODSL),NNSEL)                        
      CALL JCLOSE(ISLSNO)                                                       
      I=0                                                                       
      IUNO=1                                                                    
      NNS=NNSEL                                                                 
      IF(NNSEL.EQ.0) NNS=NNODI                                                  
      NEQVM1=NEQV-1                                                             
      NBL=(NNS+11)/12                                                           
      NN=12                                                                     
      DO 50 IW=1,NBL                                                            
      CALL INTEST                                                               
      WRITE(IOUT,430)                                                           
      IF(IW.EQ.NBL) NN=NNS-(NBL-1)*NN                                           
      DO 51 LL=1,NN                                                             
  77  I=I+1                                                                     
      I1=IV(JLABEL+I-1)                                                         
      IF(NNSEL.NE.0) THEN                                                       
       IPNOD=IPOSL(I1,IV(JNODSL),NNSEL)                                         
       IF(IPNOD.EQ.0) GOTO 77                                                   
      END IF                                                                    
      I2=IV(JNUINT+I-1)                                                         
      I7=JNUME+(I2-1)*NCS                                                       
      IF(IV(I7+1).EQ.0) GOTO 50                                                 
      I3=IV(I7)                                                                 
      IND=JX-1+I3                                                               
      WRITE(IOUT,440) I1,IUNO,(DV(IND+(L-1)*NEQV),L=1,1)                        
      DO 60 M=2,NCS                                                             
       I7=I7+1                                                                  
       I3=IV(I7)                                                                
       IND=JX+I3-1                                                              
       WRITE(IOUT,441)  M,(DV(IND+(L-1)*NEQV),L=1,1)                            
  60  CONTINUE                                                                  
  51  CONTINUE                                                                  
  50  CONTINUE                                                                  
      IF(NMPC.EQ.0) GOTO 75                                                     
       CALL INTEST                                                              
       WRITE(IOUT,445)                                                          
       DO 70 I=1,NMPC                                                           
        I1=IV(JNUINT+NNODI+I-1)                                                 
        I2=IV(JNUME+(I1-1)*NCS)                                                 
        IND=JX+I2-1                                                             
        WRITE(IOUT,450) (DV(IND+(L-1)*NEQV),L=1,1)                              
  70   CONTINUE                                                                 
  75   CONTINUE                                                                 
      CALL INTEST                                                               
      WRITE(IOUT,460)                                                           
      IL=NNODI*NCS+NMPC                                                         
      DO 80 I=IL,NEQVM1                                                         
      IND=JX+I                                                                  
      WRITE(IOUT,450) (DV(IND+(L-1)*NEQV),L=1,1)                                
 80   CONTINUE                                                                  
 430  FORMAT(/53X,'***  SOLUZIONI : INGOBBAMENTI TERMICI  ***'//                
     *  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
 460  FORMAT(/47X,'***  SOLUZIONI : PARAMETRI DI DEFORMAZIONE  ***'//           
     *  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
 445  FORMAT(/46X,'***  SOLUZIONI : MOLTIPLICATORI DI LAGRANGE  ***'//          
     *  27X,'TX',17X,'TY',17X,'TZ',17X,'MX',17X,'MY',17X,'MZ'/)                 
 440  FORMAT(/1X,I8,I6,6E19.10)                                                 
 441  FORMAT(9X,I6,6E19.10)                                                     
 450  FORMAT(15X,6E19.10)                                                       
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.TRASF2                                                        
C*************************          TRASF2         ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TRASF2(RCOP,N,ANGLE)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION  RCOP(6,6),SCOP(6,6),TRSMT(6,6)                                 
      DIMENSION  TRSINV(6,6),PERM(6,6),TCOP(6,6),TRSTSP(6,6)                    
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      IF(ANGLE.EQ.0.D0) RETURN                                                  
      CALL DZERO(TRSMT,6*6)                                                     
      ALFA=ANGLE/RAD                                                            
      COSA=DCOS(ALFA)                                                           
      SINA=DSIN(ALFA)                                                           
      COS2=COSA*COSA                                                            
      SIN2=SINA*SINA                                                            
      SICO=SINA*COSA                                                            
      TRSMT(1,1)=COS2-SIN2                                                      
      TRSMT(1,3)=SICO                                                           
      TRSMT(1,4)=-SICO                                                          
      TRSMT(2,2)=COSA                                                           
      TRSMT(2,6)=-SINA                                                          
      TRSMT(3,1)=-2.D0*SICO                                                     
      TRSMT(3,3)=COS2                                                           
      TRSMT(3,4)=SIN2                                                           
      TRSMT(4,1)=2.D0*SICO                                                      
      TRSMT(4,3)=SIN2                                                           
      TRSMT(4,4)=COS2                                                           
      TRSMT(5,5)=1.D0                                                           
      TRSMT(6,2)=SINA                                                           
      TRSMT(6,6)=COSA                                                           
      CALL INVMAU(TRSMT,TRSINV)                                                 
      CALL DPMMAU(TRSINV,TRSMT,TCOP)                                            
      CALL DPMMAU(TRSINV,RCOP,SCOP)                                             
      CALL TSPMAU(TRSINV,TRSTSP)                                                
      CALL DPMMAU(SCOP,TRSTSP,RCOP)                                             
C                                                                               
      RETURN                                                                    
      END                                                                       
C *******************************************************************           
C@ELT,I ANBA*ANBA.TRASF3                                                        
C*************************          TRASF3         ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TRASF3(RCOP,ANGLE,SCOP)                                        
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION  RCOP(6,6),SCOP(6,6),TRSMT(6,6)                                 
      DIMENSION  TRSINV(6,6),PERM(6,6),TCOP(6,6),TRSTSP(6,6)                    
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      IF(DABS(ANGLE).LE.1D-7) THEN                                              
      DO 33 I=1,6                                                               
      DO 32 J=1,6                                                               
      SCOP(I,J)=RCOP(I,J)                                                       
  32  CONTINUE                                                                  
  33  CONTINUE                                                                  
      RETURN                                                                    
      END IF                                                                    
      CALL DZERO(TRSMT,6*6)                                                     
      ALFA=ANGLE/RAD                                                            
      COSA=DCOS(ALFA)                                                           
      SINA=DSIN(ALFA)                                                           
      COS2=COSA*COSA                                                            
      SIN2=SINA*SINA                                                            
      SICO=SINA*COSA                                                            
      TRSMT(1,1)=COS2-SIN2                                                      
      TRSMT(1,3)=SICO                                                           
      TRSMT(1,4)=-SICO                                                          
      TRSMT(2,2)=COSA                                                           
      TRSMT(2,6)=-SINA                                                          
      TRSMT(3,1)=-2.D0*SICO                                                     
      TRSMT(3,3)=COS2                                                           
      TRSMT(3,4)=SIN2                                                           
      TRSMT(4,1)=2.D0*SICO                                                      
      TRSMT(4,3)=SIN2                                                           
      TRSMT(4,4)=COS2                                                           
      TRSMT(5,5)=1.D0                                                           
      TRSMT(6,2)=SINA                                                           
      TRSMT(6,6)=COSA                                                           
      CALL TSPMAU(TRSMT,TRSTSP)                                                 
      CALL DPMMAU(TRSTSP,RCOP,SCOP)                                             
C                                                                               
      RETURN                                                                    
      END                                                                       
C *******************************************************************           
C@ELT,I ANBA*ANBA.TRASF4                                                        
C*************************          TRASF4         ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TRASF4(RCOP,ANGLE,SCOP)                                        
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION  RCOP(6,6),SCOP(6,6),TRSMT(6,6),TEC(6,6)                        
      DIMENSION  TRSINV(6,6),PERM(6,6),TCOP(6,6),TRSTSP(6,6)                    
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      IF(DABS(ANGLE).LE.1D-7) THEN                                              
      DO 33 I=1,6                                                               
      DO 32 J=1,6                                                               
      SCOP(I,J)=RCOP(I,J)                                                       
  32  CONTINUE                                                                  
  33  CONTINUE                                                                  
      RETURN                                                                    
      END IF                                                                    
      CALL DZERO(TRSMT,6*6)                                                     
      CALL DZERO(TEC,6*6)                                                       
      ALFA=ANGLE                                                                
      COSA=DCOS(ALFA)                                                           
      SINA=DSIN(ALFA)                                                           
      COSA2=COSA*COSA                                                           
      COS2A=DCOS(2.D0*ALFA)                                                     
      SIN2A=DSIN(2.D0*ALFA)                                                     
      SINA2=SINA*SINA                                                           
      SICO=SINA*COSA                                                            
C                                                                               
      TEC(1,1)= COSA                                                            
      TEC(1,2)=-SINA                                                            
      TEC(1,3)= 0.D0                                                            
      TEC(2,1)= SINA                                                            
      TEC(2,2)= COSA                                                            
      TEC(2,3)= 0.D0                                                            
      TEC(3,1)= 0.D0                                                            
      TEC(3,2)= 0.D0                                                            
      TEC(3,3)= 1.D0                                                            
      TEC(4,4)= COSA2                                                           
      TEC(4,5)= SINA2                                                           
      TEC(4,6)=-SIN2A                                                           
      TEC(5,4)= SINA2                                                           
      TEC(5,5)= COSA2                                                           
      TEC(5,6)= SIN2A                                                           
      TEC(6,4)= SIN2A/2.D0                                                      
      TEC(6,5)=-SIN2A/2.D0                                                      
      TEC(6,6)= COS2A                                                           
C     WRITE(6,*)'TEC IN TRASF4'                                                 
C     WRITE(6,1111)((TEC(KK,JJ),JJ=1,6),KK=1,6)                                 
C1111 FORMAT(/1X,6(6D12.6/))                                                    
      CALL TSPMAU(TEC,TRSTSP)                                                   
      CALL DPMMAU(TRSTSP,RCOP,SCOP)                                             
C     WRITE(6,*)'SCOP IN TRASF4'                                                
C     WRITE(6,1111)((SCOP(KK,JJ),JJ=1,6),KK=1,6)                                
C                                                                               
      RETURN                                                                    
      END                                                                       
C *******************************************************************           
C@ELT,I ANBA*ANBA.TRASF5                                                        
C*************************          TRASF5         ********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TRASF5(RCOP,N,ANGLE)                                           
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION  RCOP(6,6),SCOP(6,6),TRSMT(6,6)                                 
      DIMENSION  TRSINV(6,6),PERM(6,6),TCOP(6,6),TRSTSP(6,6)                    
      DATA RAD/57.29577951D0/                                                   
C                                                                               
      IF(ANGLE.EQ.0.D0) RETURN                                                  
      CALL DZERO(TRSMT,6*6)                                                     
      ALFA=ANGLE/RAD                                                            
      COSA=DCOS(ALFA)                                                           
      SINA=DSIN(ALFA)                                                           
      COS2=COSA*COSA                                                            
      SIN2=SINA*SINA                                                            
      SICO=SINA*COSA                                                            
      TRSMT(1,1)=COS2-SIN2                                                      
      TRSMT(1,3)=SICO                                                           
      TRSMT(1,4)=-SICO                                                          
      TRSMT(2,2)=COSA                                                           
      TRSMT(2,6)=-SINA                                                          
      TRSMT(3,1)=-2.D0*SICO                                                     
      TRSMT(3,3)=COS2                                                           
      TRSMT(3,4)=SIN2                                                           
      TRSMT(4,1)=2.D0*SICO                                                      
      TRSMT(4,3)=SIN2                                                           
      TRSMT(4,4)=COS2                                                           
      TRSMT(5,5)=1.D0                                                           
      TRSMT(6,2)=SINA                                                           
      TRSMT(6,6)=COSA                                                           
      CALL DPMMAU(TRSMT,RCOP,SCOP)                                              
      CALL TSPMAU(TRSMT,TRSTSP)                                                 
      CALL DPMMAU(SCOP,TRSTSP,RCOP)                                             
C                                                                               
      RETURN                                                                    
      END                                                                       
C************************************************************************       
      SUBROUTINE INVMAU(ZZ,ZINV)                                                
      IMPLICIT REAL*8(A-H,O-Z)                                                  
      DIMENSION  ZINV(6,6),Z(6,12),ZZ(6,6)                                      
      RAD=57.29577951D0                                                         
      N=6                                                                       
C                                                                               
      DO 1000 I=1,6                                                             
      DO 900 J=1,6                                                              
      Z(I,J)=ZZ(I,J)                                                            
  900 CONTINUE                                                                  
 1000 CONTINUE                                                                  
      NP1=N+1                                                                   
      NX2=N*2                                                                   
      DO 20 I=1,N                                                               
      DO 10 J=NP1,NX2                                                           
      Z(I,J)=0.0                                                                
   10 CONTINUE                                                                  
   20 CONTINUE                                                                  
      J=N                                                                       
      DO 30 I=1,N                                                               
      J=J+1                                                                     
      Z(I,J)=1.0                                                                
   30 CONTINUE                                                                  
      DO 40 K=1,N                                                               
      KM1=K-1                                                                   
      KP1=K+1                                                                   
      AKK=Z(K,K)                                                                
C                                                                               
      IF(AKK.NE.0.0) GOTO 500                                                   
      WRITE(6,600)                                                              
  600 FORMAT(1H ,///T3,                                                         
     #'                  ATTENZIONE    '/T3,                                    
     #'                ==============  '//T3,                                   
     #'LA ROUTINE DI INVERSIONE MATRICIALE RISCONTRA UNA DIVISIONE'/T3,         
     #'PER ZERO. QUESTO NON DOVREBBE MAI ACCADERE.'//T3,                        
     #'RICHIEDERE L''INTERVENTO DEL PROGRAMMATORE.'//T3,                        
     #'ELABORAZIONE TERMINATA IN SUBROUTINE INVERS'////)                        
      STOP 99                                                                   
  500 CONTINUE                                                                  
C                                                                               
      DO 50 J=1,NX2                                                             
      Z(K,J)=Z(K,J)/AKK                                                         
   50 CONTINUE                                                                  
      IF(K.EQ.1) GOTO 100                                                       
      DO 61 I=1,KM1                                                             
      ZIK=Z(I,K)                                                                
      DO 62 J=1,NX2                                                             
      Z(I,J)=Z(I,J)-ZIK*Z(K,J)                                                  
   62 CONTINUE                                                                  
   61 CONTINUE                                                                  
  100 CONTINUE                                                                  
      IF(K.EQ.N) GOTO 200                                                       
      DO 71 I=KP1,N                                                             
      ZIK=Z(I,K)                                                                
      DO 72 J=1,NX2                                                             
      Z(I,J)=Z(I,J)-ZIK*Z(K,J)                                                  
   72 CONTINUE                                                                  
   71 CONTINUE                                                                  
  200 CONTINUE                                                                  
   40 CONTINUE                                                                  
C                                                                               
      DO 1234 I=1,6                                                             
      DO 1233 J=1,6                                                             
      ZINV(I,J)=Z(I,J+6)                                                        
 1233 CONTINUE                                                                  
 1234 CONTINUE                                                                  
C     WRITE(6,2)((Z(I,J),J=1,6),I=1,6)                                          
C  2  FORMAT(/3X,'DA Z(1,1) A Z(6,6)',//,6(6E12.6/))                            
C     WRITE(6,3)((Z(I,J),J=7,12),I=1,6)                                         
C  3  FORMAT(/3X,'DA Z(1,7) A Z(6,12)',//,6(6E12.6/))                           
C     STOP                                                                      
      RETURN                                                                    
      END                                                                       
C***********************************************************************        
      SUBROUTINE TSPMAU(A,B)                                                    
C     SUBROUTINE DI TRASPOSIZIONE DI UNA MATRICE 6X6                            
      IMPLICIT REAL*8(A-H,O-Z)                                                  
      DIMENSION A(6,6),B(6,6)                                                   
      DO 11 I=1,6                                                               
      DO 10 J=1,6                                                               
      B(I,J)=0.                                                                 
   10 CONTINUE                                                                  
   11 CONTINUE                                                                  
      DO 21 II=1,6                                                              
      DO 20 JJ=1,6                                                              
      B(II,JJ)=A(JJ,II)                                                         
   20 CONTINUE                                                                  
   21 CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C***********************************************************************        
      SUBROUTINE DPMMAU(A,B,C)                                                  
C     SUBROUTINE DI MOLTIPLICAZIONE DI 2  MATRICI 6X6                           
      IMPLICIT REAL*8(A-H,O-Z)                                                  
      DIMENSION A(6,6),B(6,6),C(6,6)                                            
      DO 100 I=1,6                                                              
      DO 99 J=1,6                                                               
      C(I,J)=0.0                                                                
      DO 98 K=1,6                                                               
      C(I,J)=C(I,J)+A(I,K)*B(K,J)                                               
   98 CONTINUE                                                                  
   99 CONTINUE                                                                  
  100 CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
C *******************************************************************           
C     ***********************  BTBBUP     ************************** MOD        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE BTBBUP(DEL,ENNE,BTB)                                           
C                                                                               
C                                                                               
      REAL*8 DEL,ENNE,BTB                                                       
      DIMENSION BTB(3,3),ENNE(3)                                                
                                                                                
      BTB(1,1)=DEL*ENNE(2)                                                      
      BTB(1,2)=0.D0                                                             
      BTB(1,3)=DEL*ENNE(3)                                                      
      BTB(2,1)=0.D0                                                             
      BTB(2,2)=BTB(1,3)                                                         
      BTB(2,3)=BTB(1,1)                                                         
      BTB(3,1)=0.D0                                                             
      BTB(3,2)=0.D0                                                             
      BTB(3,3)=0.D0                                                             
                                                                                
      RETURN                                                                    
      END                                                                       
C     *******************     BTABUP     ***************************  MOD       
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE BTABUP(DEL,ENNE,BTA,COE,CUR)                                   
C                                                                               
C     CALCOLA IL PRODOTTO DELLA MATRICE BTA (SECONDA PARTE DERIVATE)            
C     PER LA MATRICE DELLE FUNZIONI DI FORMA                                    
C                                                                               
      REAL*8 DEL,ENNE,BTA,COE,CUR                                               
      DIMENSION BTA(3,3),ENNE(3),COE(2),CUR(3)                                  
                                                                                
      BTA(1,1)=-(COE(1)*ENNE(2)+COE(2)*ENNE(3))                                 
      BTA(1,2)= CUR(3)*ENNE(1)                                                  
      BTA(1,3)=-CUR(2)*ENNE(1)                                                  
      BTA(2,1)=-BTA(1,2)                                                        
      BTA(2,2)= BTA(1,1)                                                        
      BTA(2,3)= CUR(1)*ENNE(1)                                                  
      BTA(3,1)= DEL*ENNE(2)+CUR(2)*ENNE(1)                                      
      BTA(3,2)= DEL*ENNE(3)-CUR(1)*ENNE(1)                                      
      BTA(3,3)= BTA(1,1)                                                        
                                                                                
      RETURN                                                                    
      END                                                                       
                                                                                
C@ELT,I ANBA*ANBA.ITELLE                                                        
C*************************          ITELLE          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE ITELLE(NIT,CDC,ELLEIT,C)                                       
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      DIMENSION ELLEIT(6,6),CDC(6),TEMP(6),CAP(6)                               
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
C                                                                               
      CALL DZERO(ELLEIT,36)
      if(abs(c).gt.0.d0) then                                                     
      DO 6 I=1,6                                                                
      CAP(I)=CDC(I)/C                                                           
  6   CONTINUE                
      else
      DO  I=1,6                                                                
      CAP(I)=CDC(I)                                                           
      enddo                
      endif                                                  
      DO 10 I=1,6                                                               
      ELLEIT(I,I)=1.D0                                                          
  10  CONTINUE                                                                  
      IF (NIT.EQ.0) RETURN                                                      
      IP=-1                                                                     
      DO 40 IT=1,NIT                                                            
      DO 30 K=1,6                                                               
      DO 20 I=1,6                                                               
      TEMP(I)=ELLEIT(I,K)                                                       
  20  CONTINUE                                                                  
      ELLEIT(1,K)=IP*(-CAP(6)*TEMP(2)+CAP(5)*TEMP(3))                           
      ELLEIT(2,K)=IP*(CAP(6)*TEMP(1)-CAP(4)*TEMP(3))                            
      ELLEIT(3,K)=IP*(-CAP(5)*TEMP(1)+CAP(4)*TEMP(2))                           
      ELLEIT(4,K)=IP*(-CAP(3)*TEMP(2)+CAP(2)*TEMP(3)-CAP(6)*TEMP(5)             
     1          +CAP(5)*TEMP(6))                                                
      ELLEIT(5,K)=IP*(CAP(3)*TEMP(1)-CAP(1)*TEMP(3)+CAP(6)*TEMP(4)              
     1          -CAP(4)*TEMP(6))                                                
      ELLEIT(6,K)=IP*(-CAP(2)*TEMP(1)+CAP(1)*TEMP(2)-CAP(5)*TEMP(4)             
     1          +CAP(4)*TEMP(5))                                                
 30   CONTINUE                                                                  
 40   CONTINUE                                                                  
                                                                                
      RETURN                                                                    
      END                                                                       
C                                                                               

C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MDSLCI                                                        
C*************************          MDSLCI          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MDSLCI                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLAC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                         
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
C                                                                               
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV        ,JNA   )                
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
C                                                                               
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*2,JTNOTO)                      
      CALL JALLOC(*2000,'TNOTH   ','D.P.',1,NEQV*2       ,JTNOTH)               
      CALL JALLOC(*2000,'TNMAT   ','D.P.',1,MAXDEL*MAXDEL*2,JTNMAT)             
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL       ,JIPIAZ)               
      CALL JALLOC(*2000,'RK      ','D.P.',1,NSTO        ,JRK   )                
      CALL JOPEN('*ASM    ',ISLASM,1)                                           
      CALL JREAD(ISLASM,DV(JRK),NSTO*2)                                         
      CALL JCLOSE(ISLASM)                                                       
      PRINT*,' ***   INIZIO FASE DI FATTORIZZAZIONE IN MEMORIA SISTEMA R        
     1EALE ***'                                                                 
C                                                                               
      CALL FACTOR(DV(JRK),IV(JNA),NEQV)                                         
C                                                                               
      PRINT*,' ***   FINE                                                       
     1     ***'                                                                 
      IF(DUMP(6)) THEN                                                          
      WRITE(IOUT,*)' MATRICE FATTORIZZATA'                                      
      WRITE(IOUT,300) 1,1,1                                                     
      WRITE(IOUT,200)DV(JRK)                                                    
      DO 323 IO=2,NEQV                                                          
      WRITE(IOUT,300) IO,IO-IV(JNA+IO-1)+IV(JNA+IO-2)+1,IO                      
      WRITE(IOUT,200)(DV(JRK+IK-1),IK=IV(JNA+IO-2)+1,IV(JNA+IO-1))              
 323  CONTINUE                                                                  
 200  FORMAT(10(1X,E12.4))                                                      
 300  FORMAT(/'  COLONNA N.',I8,5X,'  RIGHE DALLA',I8,'  ALLA',I8)              
      END IF                                                                    
      CALL JOPEN('*FCT    ',ISLFCT,1)                                           
      CALL JWRITE(ISLFCT,DV(JRK),NSTO*2)                                        
      CALL JCLOSE(ISLFCT)                                                       
                                                                                
C                                                                               
C                                                                               
      CALL JOPEN('*SL1    ',ISLSL1,1)                                           
C                                                                               
C                                                                               
      PRINT*,' ***   INIZIO SOLUZIONE PRIMO PASSO SISTEMA REALE   ***'          
           CALL INCSL1(DV(JRK),DV(JTNOTO),IV(JNA),ISLSL1 )                      
      PRINT*,' ***   FINE                                         ***'          
            CALL JCLOSE(ISLSL1)                                                 
C                                                                               
      CALL JOPEN('*TN2    ',ISLTN2,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*SL1    ',ISLSL1,1)                                           
C                                                                               
      PRINT*,' ***   INIZIO COSTRUZIONE TERMINE NOTO SECONDO PASSO  ***'        
            CALL TNSTP2(DV(JTNOTH),DV(JTNOTO),DV(JTNMAT),IV(JIPIAZ))            
      PRINT*,' ***   FINE                                           ***'        
C                                                                               
      CALL JCLOSE(ISLSL1)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JCLOSE(ISLMTN)                                                       
C                                                                               
      CALL JPOSAB(ISLTN2,1)                                                     
      CALL JOPEN('*SL2    ',ISLSL2,1)                                           
C                                                                               
      CALL INCSL2(DV(JRK),DV(JTNOTO),IV(JNA),NEQV,ISLTN2,ISLSL2)                
C                                                                               
      CALL JCLOSE(ISLSL2)                                                       
      CALL JCLOSE(ISLTN2)                                                       
C                                                                               
C                                                                               
      IF(IGEOM.EQ.1)THEN                                                        
C                                                                               
      CALL JFREE(*2000,'RK      ')                                              
      CALL JALLOC(*2000,'RK      ','D.P.',1,NEQV*NEQV*2 ,JRK   )                
      CALL JOPEN('*ASC    ',ISLASC,1)                                           
      CALL JREAD(ISLASC,DV(JRK),NEQV*NEQV*4)                                    
      CALL JCLOSE(ISLASC)                                                       
      CALL JALLOC(*2000,'PERM    ','D.P.',1,NEQV*2,JP)                          
      PRINT*,' ***   INIZIO FASE DI FATTORIZZAZIONE IN MEMORIA SISTEMA C        
     1OMPLESSO  ***'                                                            
C                                                                               
      CALL DCUFCT(DV(JRK),DV(JP),NEQV,NEQV,INDER)                               
C                                                                               
      PRINT*,' ***   FINE FASE DI FATTORIZZAZIONE IN MEMORIA SISTEMA COM        
     1PLESSO  ***'                                                              
      IF(INDER.NE.0) THEN                                                       
      WRITE(IOUT,'(/,A,I2)')' ERRORE FATT. RK IN MDSLCI =',INDER                
      ENDIF                                                                     
      IF(DUMP(6)) THEN                                                          
      WRITE(IOUT,*)' MATRICE FATTORIZZATA'                                      
      DO 324 IO=0,NEQV-1                                                        
      WRITE(IOUT,310) IO                                                        
      WRITE(IOUT,210)(DV(JRK+IO*NEQV+IK),IK=1,NEQV)                             
 324  CONTINUE                                                                  
 210  FORMAT(10(1X,E12.4))                                                      
 310  FORMAT(/'  RIGA N.',I8)                                                   
      ENDIF                                                                     
      CALL JOPEN('*CFC    ',ISLCFC,1)                                           
      CALL JWRITE(ISLCFC,DV(JRK),NEQV*NEQV*4)                                   
      CALL JCLOSE(ISLCFC)                                                       
C                                                                               
      CALL JOPEN('*SC1    ',ISLSC1,1)                                           
C                                                                               
      PRINT*,' ***   INIZIO SOLUZIONE COMPLESSA PRIMO PASSO  ***'               
      CALL CINCS1(DV(JRK),DV(JTNOTO),DV(JP),IV(JNA),NEQV,ISLSC1,NPSI)           
      PRINT*,' ***   FINE                                    ***'               
      CALL JCLOSE(ISLSC1)                                                       
C                                                                               
C                                                                               
      CALL JOPEN('*TC2    ',ISLTC2,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*SC1    ',ISLSC1,1)                                           
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
C                                                                               
      PRINT*,' ***   INIZIO COSTRUZIONE TERMINE NOTO SECONDO PASSO  ***'        
      CALL CTNSTP(DV(JTNOTH),DV(JTNOTO),DV(JTNMAT),IV(JIPIAZ))                  
      PRINT*,' ***   FINE                                           ***'        
C                                                                               
      CALL JCLOSE(ISLSC1)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JCLOSE(ISLMTN)                                                       
      CALL JCLOSE(ISLMTF)                                                       
C                                                                               
          CALL JPOSAB(ISLTC2,1)                                                 
      CALL JOPEN('*SC2    ',ISLSC2,1)                                           
C                                                                               
      PRINT*,' ***   INIZIO SOLUZIONE COMPLESSA SECONDO PASSO  ***'             
      CALL CINCS2(DV(JRK),DV(JTNOTO),DV(JP),IV(JNA),NEQV,ISLTC2,ISLSC2)         
      PRINT*,' ***   FINE                                      ***'             
C                                                                               
      CALL JCLOSE(ISLSC2)                                                       
      CALL JCLOSE(ISLTC2)                                                       
      CALL JALLOC(*2000,'UO      ','D.P.',1,NEQV*2       ,JUO)                  
      CALL JALLOC(*2000,'VO      ','D.P.',1,NEQV*2       ,JVO)                  
      CALL RISULT(NEQV,DV(JUO),DV(JVO))                                         
      ENDIF                                                                     
      RETURN                                                                    
C                                                                               
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.MDSLCI                                                        
C*************************          MDSCTI          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MDSCTI                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION DV(1)                                                           
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
C                                                                               
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV        ,JNA   )                
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
C                                                                               
      CALL JALLOC(*2000,'TNOTO   ','D.P.',1,NEQV*2,JTNOTO)                      
      CALL JALLOC(*2000,'TNOTH   ','D.P.',1,NEQV*2       ,JTNOTH)               
      CALL JALLOC(*2000,'TNMAT   ','D.P.',1,MAXDEL*MAXDEL*2,JTNMAT)             
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL       ,JIPIAZ)               
      CALL JALLOC(*2000,'RK      ','D.P.',1,NSTO        ,JRK   )                
                                                                                
      IF (JEXIST('*FCT    ').EQ.0) THEN                                         
C                                                                               
      CALL JOPEN('*ASM    ',ISLASM,1)                                           
      CALL JREAD(ISLASM,DV(JRK),NSTO*2)                                         
      CALL JCLOSE(ISLASM)                                                       
C                                                                               
      CALL FACTOR(DV(JRK),IV(JNA),NEQV)                                         
C                                                                               
      PRINT*,' ***   FINE FASE DI FATTORIZZAZIONE IN MEMORIA  SISTEMA RE        
     1ALE ***'                                                                  
      IF(DUMP(6)) THEN                                                          
      WRITE(IOUT,*)' MATRICE FATTORIZZATA'                                      
      WRITE(IOUT,300) 1,1,1                                                     
      WRITE(IOUT,200)DV(JRK)                                                    
      DO 323 IO=2,NEQV                                                          
      WRITE(IOUT,300) IO,IO-IV(JNA+IO-1)+IV(JNA+IO-2)+1,IO                      
      WRITE(IOUT,200)(DV(JRK+IK-1),IK=IV(JNA+IO-2)+1,IV(JNA+IO-1))              
 323  CONTINUE                                                                  
 200  FORMAT(10(1X,E12.4))                                                      
 300  FORMAT(/'  COLONNA N.',I8,5X,'  RIGHE DALLA',I8,'  ALLA',I8)              
      END IF                                                                    
      CALL JOPEN('*FCT    ',ISLFCT,1)                                           
      CALL JWRITE(ISLFCT,DV(JRK),NSTO*2)                                        
      CALL JCLOSE(ISLFCT)                                                       
      ELSE                                                                      
      ENDIF                                                                     
                                                                                
C                                                                               
C  ----------------------------------------------------------                   
C  INIZIO SOLUZIONE TERMICA                                                     
C                                                                               
C     apertura slot: temperature assegnate (ISTEMP), matrici termiche (ISMTER)  
C     vettore termine noto termico (ISTNOT), vettore di piazzamento (ISLVPZ)    
C                                                                               
      CALL JOPEN('*TNT    ',ISTNOT,1)                                           
      CALL JOPEN('*TMP    ',ISTEMP,1)                                           
      CALL JOPEN('*MTT    ',ISMTER,1)                                           
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
C     chiama costruzione del vettore termine noto termico                       
      CALL TNSLTR(DV(JTNOTH),DV(JTNOTO),DV(JTNMAT),IV(JIPIAZ))                  
C                                                                               
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JCLOSE(ISMTER)                                                       
      CALL JCLOSE(ISTEMP)                                                       
C                                                                               
      CALL JPOSAB(ISTNOT,1)                                                     
c     apre slot della soluzione termica ISLSLT                                  
      CALL JOPEN('*SLTERM ',ISLSLT,1)                                           
C     chiama soluzione termica  (incore)                                        
      CALL INCSLT(DV(JRK),DV(JTNOTO),IV(JNA),NEQV,ISTNOT,ISLSLT)                
C                                                                               
      CALL JCLOSE(ISLSLT)                                                       
      CALL JCLOSE(ISTNOT)                                                       
C                                                                               
C  FINE SOLUZIONE TERMICA                                                       
C  ---------------------------------------------------------                    
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
C                                                                               
2000  CALL JHALT                                                                
      STOP                                                                      
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.FATFRO                                                        
C*************************          FATFRO          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE FATFRO (EQ,IPIAZ,IPIAZH,ILOC,NEQ,MAXFRO)                       
C                                                                               
C     ROUTINE DI FATTORIZZAZIONE DELLA MATRICE DEI COEFFICIENTI DI              
C     UN SISTEMA LINEARE SIMMETRICO CON TECNICA FRONTALE                        
C                                                                               
C         SIGNIFICATO DELLE VARIABILI                                           
C                                                                               
C     EQ     =   VETTORE DI LAVORO CORRISPONDENTE AD UNA MATRICE TRIANGO        
C     IPIAZ  =   VETTORE CONTENENTE GLI INDICI DI PIAZZAMENTO DEI TERMIN        
C     IPIAZH =   VETTORE MEMORIA DEI TERMINI DI PIAZZAMENTO E LORO POSIZ        
C     ILOC   =   VETTORE PUNTATORE DELLA DIAGONALE                              
C                                                                               
C     MAXFRO =   MASSIMO FRONTE                                                 
C     NEQ    =   NUMERO DI EQUAZIONI DEL SISTEMA                                
C     NT     =   NUMERO DI INCOGNITE DELL'EQUAZIONE CONSIDERATA                 
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                islasc,islcfc,islsc1,isltc2,islsc2                        
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      DIMENSION EQ(1),IPIAZ(1),IPIAZH(1),ILOC(1)                                
      IERLOC=1                                                                  
      ILOC(1)=0                                                                 
      DO 2 K=2,MAXFRO                                                           
      ILOC(K)=ILOC(K-1)+MAXFRO-K+2                                              
2     CONTINUE                                                                  
      I=1                                                                       
C                                                                               
C     CICLO DI LETTURA FATTORIZZAZIONE E SCRITTURA FUORI MEMORIA                
C     DELLA PRIMA EQUAZIONE                                                     
C                                                                               
      CALL JREAD(ISLFCT,NT,1)                                                   
      CALL JREAD(ISLFCT,IPIAZ,NT)                                               
      IF(IPIAZ(1).NE.I)GOTO 1000                                                
      CALL JREAD(ISLFCT,EQ,NT+NT)                                               
          CALL JREAD(ISLFCT,NW,1)                                               
          CALL JPOSRL(ISLFCT,-NW)                                               
      IF(DUMP(10))WRITE(IOUT,904)I,NT,(IPIAZ(KZ),KZ=1,NT)                       
  904 FORMAT(' EQUAZ. N.RO ',I6,' DI ',I6,' TERMINI'/                           
     1  ' V.PIAZ',12I10/20(7X,12I10/))                                          
  905 FORMAT(' COEF.'/20(7X,12D10.4/))                                          
C                                                                               
C     FATTORIZZAZIONE DELLA PRIMA EQUAZIONE ED INVIO DEI CONTRIBUTI             
C                                                                               
C                                                                               
C     CONTROLLO SUL VALORE DEL TERMINE DIAGONALE                                
C                                                                               
CCC   IF(DABS(EQ(1)).GE.ERR) GOTO 25                                            
      Q=EQ(1)                                                                   
      IF(DABS(Q).GE.ERR) GOTO 25                                                
      WRITE(IOUT,205)I,EQ(1),ERR                                                
      EQ(1)=EQ(1)+CONST                                                         
      IERLOC=1                                                                  
25    CONTINUE                                                                  
      PIV = 1./EQ(1)                                                            
C     WRITE(6,3434)PIV                                                          
      IF(NT.LT.2) GOTO 17                                                       
      DO 10 K=2,NT                                                              
      FAC=EQ(K)*PIV                                                             
      NLIM=NT-K+1                                                               
      DO 20 L=1,NLIM                                                            
      EQ(ILOC(K)+L)=-EQ(L+K-1)*FAC                                              
20    CONTINUE                                                                  
      EQ(K)=FAC                                                                 
10    CONTINUE                                                                  
17    CONTINUE                                                                  
C                                                                               
C     SCRITTURA SU FILE DELLA PRIMA EQUAZIONE                                   
C                                                                               
      NWORD=2+NT*3                                                              
      CALL JWRITE(ISLFCT,NT,1)                                                  
      CALL JWRITE(ISLFCT,IPIAZ,NT)                                              
      CALL JWRITE(ISLFCT,EQ,NT+NT)                                              
      CALL JWRITE(ISLFCT,NWORD,1)                                               
      IF(DUMP(10))WRITE(IOUT,905)(EQ(KZ),KZ=1,NT)                               
C                                                                               
C     CICLO DI LETTURA FATTORIZZAZIONE E SCRITTURA FUORI MEMORIA                
C     DELLE RESTANTI NEQ-1 EQUAZIONI                                            
C                                                                               
      DO 30 I=2,NEQ                                                             
      NTH=NT-1                                                                  
C                                                                               
C     MEMORIZZAZIONE DEL VETTORE DI PIAZZAMENTO DELL'EQUAZIONE  I-1             
C                                                                               
      IF(NTH.EQ.0) GOTO 40                                                      
      DO 190 IS=1,NTH                                                           
      IPIAZH(IS)=IPIAZ(IS+1)                                                    
190   CONTINUE                                                                  
C                                                                               
C     LETTURA DELLA  I-ESIMA EQUAZIONE                                          
C                                                                               
 40   CALL JREAD(ISLFCT,NT,1)                                                   
      CALL JREAD(ISLFCT,IPIAZ,NT)                                               
C                                                                               
C     TEST PER LA VERIFICA DELLA PRESENZA DEL TERMINE DIAGONALE                 
C                                                                               
      CALL JREAD(ISLFCT,EQ,NT+NT)                                               
         CALL JREAD(ISLFCT,NW,1)                                                
         CALL JPOSRL(ISLFCT,-NW)                                                
      IF(DUMP(10))WRITE(IOUT,904)I,NT,(IPIAZ(KZ),KZ=1,NT)                       
      IF(IPIAZ(1).NE.I)GOTO 1000                                                
C                                                                               
C      CARICAMENTO IN -IPIAZH- DELLE POSIZIONI CHE LE INCOGNITE DELLA           
C     EQUAZIONE PRECEDENTE ASSUMONO IN QUELLA ATTUALE                           
C                                                                               
801   CONTINUE                                                                  
      IF(NTH.EQ.0)GOTO 65                                                       
      DO 35 M=1,NTH                                                             
      IPIAZH(M)=IPOSZ(IPIAZH(M),IPIAZ,NT)                                       
35    CONTINUE                                                                  
      IF(IPIAZH(1).NE.1)GOTO 65                                                 
      ILL=ILOC(2)
C                                                                               
C     SOMMA ALL'EQUAZIONE I-ESIMA DEI CONTRIBUTI AD ESSA DESTINATI              
C     E CHE SI TROVANO NELLA SECONDA RIGA DELLA MATRICE                         
C                                                                               
      DO 50 K=1,NTH                                                             
      IP=IPIAZH(K)                                                              
      EQ(IP)=EQ(IP)+EQ(ILL+K)                                                   
50    CONTINUE                                                                  
 907  FORMAT(24I5)                                                              
      KK=2                                                                      
      GOTO 66                                                                   
65    KK=1                                                                      
C                                                                               
C     FATTORIZZAZIONE DELLA EQUAZIONE I-ESIMA                                   
C                                                                               
66    CONTINUE                                                                  
CCC   IF(DABS(EQ(1)).GE.ERR) GOTO 28                                            
      Q=EQ(1)                                                                   
      IF(DABS(Q).GE.ERR) GOTO 28                                                
      WRITE(IOUT,205)I,EQ(1),ERR                                                
      EQ(1)=EQ(1)+CONST                                                         
      IERLOC=1                                                                  
28    CONTINUE                                                                  
      PIV=1./EQ(1)                                                              
      IF(KK.GT.NTH) GOTO 105                                                    
      DO 70 L=KK,NTH                                                            
      IR=IPIAZH(L)                                                              
      IF(IR.GT.L)GOTO 75                                                        
      FAC=EQ(IR)*PIV                                                            
      ISUM=L-1                                                                  
      ILR=ILOC(IR)-IR+1                                                         
      ILL=ILOC(L+1)                                                             
C                                                                               
C     CICLO DI INVIO DEI CONTRIBUTI DI TUTTE LE INCOGNITE DELLA NUOVA           
C     EQUAZIONE FINO A QUELLO CHE COMPARE PER LA PRIMA VOLTA                    
C                                                                               
      NTS=NTH-ISUM                                                              
      IF(NTS.LT.1) GOTO 70                                                      
      DO 80 K3=1,NTS                                                            
      IC=IPIAZH(K3+ISUM)                                                        
      EQ(ILR+IC)=EQ(ILL+K3)-EQ(IC)*FAC                                          
80    CONTINUE                                                                  
 70   CONTINUE                                                                  
      GOTO 105                                                                  
C                                                                               
C     CICLO DI INVIO DEI CONTRIBUTI DELLE INCOGNITE GIA' PRESENTI NELLA         
C     EQUAZIONE PRECEDENTE A PARTIRE DALL'ULTIMO                                
C                                                                               
75    IF(NTH.LT.L)GOTO 91                                                       
      DO 90 IM=L,NTH                                                            
      M=NTH-IM+L                                                                
      IR=IPIAZH(M)                                                              
      FAC=EQ(IR)*PIV                                                            
      ISUM=M-1                                                                  
      ILR=ILOC(IR)-IR+1                                                         
      ILL=ILOC(M+1)                                                             
      NLIM=NTH-ISUM                                                             
      IF(NLIM.LT.1)GOTO 101                                                     
      DO 100 K4=1,NLIM                                                          
      K3=NLIM-K4+1                                                              
                                                                                
      IC=IPIAZH(K3+ISUM)                                                        
      EQ(ILR+IC)=EQ(ILL+K3)-EQ(IC)*FAC                                          
100   CONTINUE                                                                  
101   CONTINUE                                                                  
90    CONTINUE                                                                  
91    CONTINUE                                                                  
105   IND2=NTH                                                                  
C                                                                               
C     CARICAMENTO NELLE POSIZIONI DI  IPIAZH  SUCCESSIVE ALLE                   
C     PRIME  NTH  DELLE POSIZIONI OCCUPATE IN  IPIAZ  DALLE INCOGNITE           
C     DI NUOVO INSERIMENTO                                                      
C                                                                               
501   IF(NT.LT.2) GOTO 601                                                      
      DO 110 K=2,NT                                                             
      IP=IPOSZ(K,IPIAZH,NTH)                                                    
      IF(IP.NE.0)GOTO 110                                                       
      IND2=IND2+1                                                               
      IPIAZH(IND2)=K                                                            
110   CONTINUE                                                                  
      IDO=1                                                                     
      IF(IPIAZH(1).EQ.1)IDO=2                                                   
C                                                                               
C     CICLO DI INVIO DEI CONTRIBUTI ALLE RIGHE CORRISPONDENTI                   
C     ALLE INCOGNITE DI NUOVO INSERIMENTO                                       
C                                                                               
      IF(IDO.GT.NTH)GOTO 121                                                    
      DO 120 L=IDO,NTH                                                          
      IND=NTH+1                                                                 
      IR=IPIAZH(L)                                                              
      FAC=EQ(IR)*PIV                                                            
125   CONTINUE                                                                  
      IF(IR.LE.IPIAZH(IND))GOTO 130                                             
      IND=IND+1                                                                 
      GOTO 125                                                                  
130   JINF=IND                                                                  
      ILR=ILOC(IR)-IR+1                                                         
      IF(JINF.GT.IND2)GOTO 120                                                  
      DO 140 K=JINF,IND2                                                        
      ILL=IPIAZH(K)                                                             
      EQ(ILR+ILL)=-EQ(ILL)*FAC                                                  
140   CONTINUE                                                                  
120   CONTINUE                                                                  
121   CONTINUE                                                                  
C                                                                               
C     CICLO DI INVIO DEI CONTRIBUTI NELLE COLONNE CORRISPONDENTI                
C     ALLE INCOGNITE INSERITI CON QUESTA EQUAZIONE                              
C                                                                               
      IF(IND2.EQ.NTH) GOTO 601                                                  
      NLIM=1+NTH                                                                
      IF(NLIM.GT.IND2)GOTO 168                                                  
      DO 165 K=NLIM,IND2                                                        
      IR=IPIAZH(K)                                                              
      FAC=EQ(IR)*PIV                                                            
      ISUM=IR-1                                                                 
      ILR=ILOC(IR)-ISUM                                                         
      IF(IR.GT.NT)GOTO 171                                                      
      DO 170 L=IR,NT                                                            
      EQ(ILR+L)=-EQ(L)*FAC                                                      
170   CONTINUE                                                                  
171   CONTINUE                                                                  
165   CONTINUE                                                                  
168   CONTINUE                                                                  
C                                                                               
C     RIDUZIONE DELLE INCOGNITE DELLA I-ESIMA EQUAZIONE                         
C                                                                               
 601  CONTINUE                                                                  
      IF(NT.LT.2)GOTO 176                                                       
      DO 175 MI=2,NT                                                            
      EQ(MI)=EQ(MI)*PIV                                                         
175   CONTINUE                                                                  
176   CONTINUE                                                                  
C                                                                               
C     SCRITTURA SU FILE DELLA EQUAZIONE I-ESIMA                                 
C                                                                               
      NWORD=2+3*NT                                                              
      CALL JWRITE(ISLFCT,NT,1)                                                  
      CALL JWRITE(ISLFCT,IPIAZ,NT)                                              
      CALL JWRITE(ISLFCT,EQ,NT+NT)                                              
      CALL JWRITE(ISLFCT,NWORD,1)                                               
      IF(DUMP(10))WRITE(IOUT,905)(EQ(KZ),KZ=1,NT)                               
30    CONTINUE                                                                  
      IF(IERLOC.NE.0.AND.IAUTO.EQ.1) THEN                                       
      WRITE(IOUT,1300)                                                          
      WRITE(IOUT,*)' IERLOC',IERLOC,'    IAUTO',IAUTO                           
      CALL JCLOSE(ISLFCT)                                                       
C+    CALL JCLOSE(ISLASM)                                                       
      CALL JHALT                                                                
      STOP                                                                      
 1300 FORMAT(/' ***   AVVISO (FATFRO) : IL CALCOLO NON PROSEGUE',               
     * ' A CAUSA DELLE LABILITA'' RISCONTRATE   ***'/)                          
      END IF                                                                    
      RETURN                                                                    
1000  WRITE(IOUT,1200)I,IPIAZ(1)                                                
1200  FORMAT(/' ***   AVVISO (FATFRO): NON PRESENTE IL TERMINE ',               
     1 'DIAGONALE DELL''EQUAZIONE N.RO',I8,'   ***'/)                           
      CALL JHALT                                                                
      STOP                                                                      
205   FORMAT(/' *** AVVISO (FATFRO): ESEGUITO AUTOSPC SU'                       
     1 ,' EQ. N.RO',I6,' IL CUI TERM. DIAG.',E12.6,'  E'' MINORE DI'            
     2   ,E12.6,'  ***')                                                        
      END                                                                       
C@ELT,I ANBA*ANBA.FORBCK                                                        
C*************************          FORBCK          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE FORBCK(TNOTO,EQ,IPIAZ,NEQ,NTNOTI)                              
C                                                                               
C                                                                               
C     SOLUZIONE DI UN SISTEMA LINEARE LA CUI MATRICE DI COEFFICIENTI            
C     E' STATA FATTORIZZATA CON TECNICA FRONTALE DALLA ROUTINE -FATFRO-         
C                                                                               
C     LA SOLUZIONE VIENE ESEGUITA CONTEMPORANEAMENTE PER -NTNOTI-               
C     TERMINI NOTI                                                              
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION EQ(1),IPIAZ(1),TNOTO(1)                                         
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                islasc,islcfc,islsc1,isltc2,islsc2                        
C                                                                               
C     SOSTITUZIONE FRONTALE IN AVANTI                                           
C                                                                               
      DO 10 I=1,NEQ                                                             
      CALL JREAD(ISLFCT,NT,1)                                                   
      CALL JREAD(ISLFCT,IPIAZ,NT)                                               
      CALL JREAD(ISLFCT,EQ,NT+NT)                                               
      CALL JREAD(ISLFCT,NWORD,1)                                                
      DO 15 J=1,NTNOTI                                                          
      JJ=NEQ*(J-1)                                                              
      RHS=TNOTO(IPIAZ(1)+JJ)                                                    
      IF(NT.EQ.1)GOTO 25                                                        
      DO 20 K=2,NT                                                              
      IND=IPIAZ(K)+JJ                                                           
      TNOTO(IND)=TNOTO(IND)-EQ(K)*RHS                                           
20    CONTINUE                                                                  
25    TNOTO(JJ+I)=TNOTO(JJ+I)/EQ(1)                                             
15    CONTINUE                                                                  
10    CONTINUE                                                                  
C                                                                               
C     SOSTITUZIONE FRONTALE ALL'INDIETRO                                        
C                                                                               
      CALL JPOSRL(ISLFCT,-1)                                                    
      CALL JREAD(ISLFCT,NWORD,1)                                                
      CALL JPOSRL(ISLFCT,-NWORD-1)                                              
      DO 50 I=2,NEQ                                                             
      CALL JREAD(ISLFCT,NWORD,1)                                                
      CALL JPOSRL(ISLFCT,-NWORD)                                                
      CALL JREAD(ISLFCT,NT,1)                                                   
      CALL JREAD(ISLFCT,IPIAZ,NT)                                               
      CALL JREAD(ISLFCT,EQ,NT+NT)                                               
      CALL JPOSRL(ISLFCT,-NWORD)                                                
      DO 55 J=1,NTNOTI                                                          
      ACC=0.                                                                    
      JJ=NEQ*(J-1)                                                              
      IF(NT.EQ.1)GOTO 65                                                        
      DO 60 K=2,NT                                                              
      IND=IPIAZ(K)+JJ                                                           
      ACC=ACC+TNOTO(IND)*EQ(K)                                                  
60    CONTINUE                                                                  
65    IND=IPIAZ(1)+JJ                                                           
      TNOTO(IND)=TNOTO(IND)-ACC                                                 
55    CONTINUE                                                                  
50    CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
                                                                                
C@ELT,I ANBA*ANBA.INCSL1                                                        
C*************************          INCSL1          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE INCSL1(RK,TN,NA,ISLSL1)                                        
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP                                                              
      DIMENSION NA(1),RK(1),TN(1),ELLE(6,6),ELLE1(6,6),ELLE2(6,6),              
     *          TEMPO(6,6),CBO(6,6)                                             
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NELES,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     1                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
C                                                                               
       print*,' incsl1'
      CALL DZERO(TEMPO,36)                                                      
      CALL DZERO(CBO,36)                                                        
      CALL ITELLE(1,CDC,ELLE,1.D0)                                              
      DO 6 I=1,6                                                                
      TEMPO(I,I)=TEMPO(I,I)+1.D0                                                
 6    CONTINUE                                                                  
      IF(IGEOM.EQ.1)THEN                                                        
       print*,' Itelle',c
      CALL ITELLE(1,CDC,ELLE,C)                                                 
      CALL ITELLE(2,CDC,ELLE1,C)                                                
      CALL ITELLE(4,CDC,ELLE2,C)                                                
       print*,' Itelle'
      DO 5 I=1,6                                                                
      DO 5 K=1,6                                                                
      TEMPO(I,K)=TEMPO(I,K)+2*ELLE1(I,K)+ELLE2(I,K)                             
 5    CONTINUE                                                                  
      ENDIF                                                                     
      DO 7 I=1,6                                                                
      DO 7 K=1,6                                                                
      DO 7 L=1,6                                                                
      CBO(I,K)=CBO(I,K)+ELLE(I,L)*TEMPO(L,K)                                    
 7    CONTINUE                                                                  
      DO 20 I=1,6                                                               
      CALL DZERO(TN,NEQV)                                                       
      DO 30 L=1,6                                                               
      IW=NEQV-NPSI+L                                                            
      TN(IW)=TN(IW)+CBO(L,I)                                                    
 30   CONTINUE                                                                  
      IF(DUMP(4))THEN                                                           
          WRITE(IOUT,'(/,A)')' TNOTO DEL PRIMO PASSO IN INCSL1'                 
          WRITE(IOUT,'(1X,6E12.6)')(TN(IO),IO=1,NEQV)                           
      ENDIF                                                                     
      CALL FORBAC(RK,TN,NA,NEQV)                                                
      CALL JWRITE(ISLSL1,TN,NEQV*2)                                             
      IF(DUMP(2))THEN                                                           
          WRITE(IOUT,'(/,A)')' SOLUZIONI DEL PRIMO PASSO IN INCSL1'             
          WRITE(IOUT,'(1X,6E12.6)')(TN(IO),IO=1,NEQV)                           
      END IF                                                                    
 20   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C@ELT,I ANBA*ANBA.CINCS1                                                        
C*************************          CINCS1          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE CINCS1(RK,TN,PERM,NA,NEQV,ISLSC1,NPSI)                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 RK(NEQV,1),TN(1),B1(6,6)                                       
      LOGICAL DUMP                                                              
      DIMENSION NA(1),ELLE(6,6),ELLE1(6,6),PERM(NEQV)                           
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
C                                                                               
      CALL DZERO(B1,72)                                                         
      CALL ITELLE(3,CDC,ELLE,C)                                                 
      CALL ITELLE(2,CDC,ELLE1,C)                                                

      
      write(iout,'(a,6f8.5,e12.5)') ' cdc ',cdc,c       
      write(iout,*) ' elle 3'          
      write(iout,'(6e12.5)') elle                                     
      write(iout,*) ' elle 2'          
      write(iout,'(6e12.5)') elle1                                     

      DO 30 I=1,6                                                               
      DO 30 K=1,6                                                               
      B1(I,K)=DCMPLX(0.25*ELLE(I,K),0.25*ELLE1(I,K))                            
  30  CONTINUE                                                                  
      CALL ITELLE(5,CDC,ELLE,C)                                                 
      CALL ITELLE(4,CDC,ELLE1,C)                                                

      write(iout,*) ' elle 5'          
      write(iout,'(6e12.5)') elle                                     
      write(iout,*) ' elle 4'          
      write(iout,'(6e12.5)') elle1                                     

      DO 40 I=1,6                                                               
      DO 40 K=1,6                                                               
      B1(I,K)=B1(I,K)+DCMPLX(.25*ELLE(I,K),.25*ELLE1(I,K))                      
  40  CONTINUE                                                                  
      WRITE(IOUT,'(/,A)') ' B1 IN CINCS1 PARTE REALE'                           
      WRITE(IOUT,'(1X,6E12.6)') ((DREAL(B1(IO,IC)),IC=1,6),IO=1,6)              
      WRITE(IOUT,'(/,A)') ' B1 IN CINCS1 PARTE IMMAGINARIA'                     
      WRITE(IOUT,'(1X,6E12.6)') ((DIMAG(B1(IO,IC)),IC=1,6),IO=1,6)              
CC    WRITE(IOUT,'(/,A)')' MATRICE K-I*C*H+C**2*M 30*30',                       
CC   1' PARTE REALE IN CINCS1'                                                  
CC    WRITE(IOUT,'(1X,6E12.6)')((DREAL(RK(IO,IC)),IC=1,NEQV),IO=1,NEQV)         
CC    WRITE(IOUT,'(/,A)')' MATRICE K-I*C*H+C**2*M 30*30 PARTE'                  
CC   1' IMMAGINARIA IN CINCS1'                                                  
CC    WRITE(IOUT,'(1X,6E12.6)')((DIMAG(RK(IO,IC)),IC=1,NEQV),IO=1,NEQV)         
      DO 50 L=1,6                                                               
      CALL DZERO(TN,NEQV*2)                                                     
      DO 60 I=1,6                                                               
      IW=NEQV-NPSI+I                                                            
      TN(IW)=TN(IW)+B1(I,L)                                                     
  60  CONTINUE                                                                  
      IF(DUMP(4))THEN                                                           
          WRITE(IOUT,'(/,A)')' TERMINE NOTO REALE IN CINCS1'                    
          WRITE(IOUT,'(1X,6E12.6)')(DREAL(TN(IO)),IO=1,NEQV)                    
          WRITE(IOUT,'(/,A)')' TERMINE NOTO IMMAGINARIO IN CINCS1'              
          WRITE(IOUT,'(1X,6E12.6)')(DIMAG(TN(IO)),IO=1,NEQV)                    
      ENDIF                                                                     
      CALL DCUSOL(RK,TN,NEQV,NEQV,PERM)                                         
      CALL JWRITE(ISLSC1,TN,NEQV*4)                                             
      IF(DUMP(2)) THEN                                                          
          WRITE(IOUT,'(/,A)')' SOLUZIONI DEL PRIMO PASSO CINCS1'                
          WRITE(IOUT,'(1X,6E12.6)')(TN(IO),IO=1,NEQV)                           
      END IF                                                                    
 50   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.INCSL2                                                        
C*************************          INCSL2          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE INCSL2(RK,TN,NA,NEQV,ISLTN2,ISLSL2)                            
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP                                                              
      DIMENSION NA(1),RK(1),TN(1)                                               
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
C                                                                               
      IF(DUMP(4).or.DUMP(2)) THEN                                                          
        WRITE(IOUT,'(/,A)')' SECONDO PASSO IN INCSL2'          
      END IF                                                                    
      DO 10 ISOL=1,6                                                            
      CALL JREAD(ISLTN2,TN,NEQV*2)                                              
      IF(DUMP(4)) THEN                                                          
        WRITE(IOUT,'(/,A,I8)') 'T.Noto ',ISOL          
        WRITE(IOUT,'(1X,6E13.6)')(TN(IO),IO=1,NEQV)                             
      END IF
      CALL FORBAC(RK,TN,NA,NEQV)                                                
      CALL JWRITE(ISLSL2,TN,NEQV*2)                                             
      IF (DUMP(2))THEN                                                          
        WRITE(IOUT,'(/,A,I8)') 'Soluzione ',ISOL          
        WRITE(IOUT,'(1X,6E13.6)')(TN(IO),IO=1,NEQV)                          
      ENDIF                                                                     
 10   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C------------------------incore soluzione termica ------- inizio-------         
C@ELT,I ANBA*ANBA.INCSLT                                                        
C*************************          INCSLT          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE INCSLT(RK,TN,NA,NEQV,ISTNOT,ISLSLT)                            
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP                                                              
      DIMENSION NA(1),RK(1),TN(1)                                               
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
C                                                                               
      CALL JPOSAB(ISTNOT,1)                                                     
      DO 10 ISOL=1,1                                                            
      CALL JREAD(ISTNOT,TN,NEQV*2)                                              
c---                                                                            
c      do 20,j=1,neqv                                                           
c      write(iout,*) 'Tnoto(',j,')=',tn(j)                                      
c20    continue                                                                 
c-------                                                                        
      IF(DUMP(4)) THEN                                                          
       WRITE(IOUT,'(/,A)')' TERMINI NOTI DEL SECONDO PASSO IN INCSLT'           
       WRITE(IOUT,'(1X,6E12.6)')(TN(IO),IO=1,NEQV)                              
      END IF                                                                    
      CALL FORBAC(RK,TN,NA,NEQV)                                                
      CALL JWRITE(ISLSLT,TN,NEQV*2)                                             
      IF (DUMP(2))THEN                                                          
      WRITE(IOUT,'(/,A)')' SOLUZIONI TERMICHE DEL SECONDO PASSO IN              
     + INCSLT'                                                                  
      WRITE(IOUT,'(1X,6E12.6)') (TN(IO),IO=1,NEQV)                              
      ENDIF                                                                     
 10   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C-------------------------------------------------------------------            
C@ELT,I ANBA*ANBA.CINCS2                                                        
C*************************          CINCS2          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
C                                                                               
      SUBROUTINE CINCS2(RK,TN,PERM,NA,NEQV,ISLTC2,ISLSC2)                       
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 RK(NEQV,1),TN(1)                                               
      LOGICAL DUMP                                                              
      DIMENSION NA(1),PERM(NEQV)                                                
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
C                                                                               
      DO 10 ISOL=1,6                                                            
      CALL JREAD(ISLTC2,TN,NEQV*4)                                              
      IF(DUMP(4)) THEN                                                          
        WRITE(IOUT,'(/,A)')' TERMINI NOTI DEL SECONDO PASSO'                    
        WRITE(IOUT,'(6E12.5)')(TN(IO),IO=1,NEQV)                                
      END IF                                                                    
      CALL DCUSOL(RK,TN,NEQV,NEQV,PERM)                                         
      IF (DUMP(2))THEN                                                          
       WRITE(IOUT,'(/,A)')' SOLUZIONE REALE SECONDO PASSO ANBA7 (U1)'           
       WRITE(IOUT,'(1X,6E12.6)')(DREAL(TN(IO)),IO=1,NEQV)                       
       WRITE(IOUT,'(/,A)')' SOLUZIONE IMMAGIN SECONDO PASSO ANBA7 (U1)'         
       WRITE(IOUT,'(1X,6E12.6)')(DIMAG(TN(IO)),IO=1,NEQV)                       
      ENDIF                                                                     
      CALL JWRITE(ISLSC2,TN,NEQV*4)                                             
 10   CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.TNSTP2                                                        
C*************************          TNSTP2          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TNSTP2(TNOTH,TNOTO,TNMAT,IPIAZ)                                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP                                                              
      INTEGER SPCCOD                                                            
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,neqs,nsto        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      DIMENSION TNOTO(1),TNOTH(1),TNMAT(MAXDEL,1),IPIAZ(1),                     
     *          ELLE(6,6),ELLE1(6,6),AO(6,6)                                    
      DATA MPCCOD,SPCCOD/6,7/                                                   
      CALL JPOSAB(ISLSL1,1)                                                     
      CALL DZERO(AO,36)                                                         
      DO 6 I=1,6                                                                
      AO(I,I)=1.D0                                                              
 6    CONTINUE                                                                  
      IF(IGEOM.EQ.1)THEN                                                        
      CALL ITELLE(2,CDC,ELLE,C)                                                 
      CALL ITELLE(4,CDC,ELLE1,C)                                                
      DO 5 I=1,6                                                                
      DO 5 K=1,6                                                                
      AO(I,K)=AO(I,K)+2*ELLE(I,K)+ELLE1(I,K)                                    
 5    CONTINUE                                                                  
CC    WRITE(IOUT,'(/,A)') ' AO DEL SECONDO PASSO ANBA2'                         
CC    WRITE(IOUT,'(1X,6E12.6)') ((AO(IO,IC),IC=1,6),IO=1,6)                     
      ENDIF                                                                     
      DO 10 I=1,6                                                               
      CALL JREAD(ISLSL1,TNOTH,NEQV*2)                                           
      CALL DZERO(TNOTO,NEQV)                                                    
      CALL JPOSAB(ISLVPZ,1)                                                     
      CALL JPOSAB(ISLMTN,1)                                                     
      DO 20 K=1,NELEM                                                           
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT,1)                                                   
      CALL JREAD(ISLVPZ,NT1,1)                                                  
      CALL JREAD(ISLVPZ,IPIAZ,NT-1)                                             
      JUMP=(NT-1)*NT1*2+1                                                       
      CALL JPOSRL(ISLVPZ,JUMP)                                                  
      IF(ITIP.EQ.MPCCOD)GOTO 20                                                 
      IF(ITIP.EQ.SPCCOD)GOTO 20                                                 
      IF(ITIP.EQ.2)GOTO 20                                                      
      NT=NT-1                                                                   
      DO 70 IS=1,NT                                                             
      CALL JREAD(ISLMTN,TNMAT(1,IS),NT1*2)                                      
 70   CONTINUE                                                                  
      DO 30 M=1,NT                                                              
      MS=IPIAZ(M)                                                               
      DO 40 N=1,NT                                                              
      MT=IPIAZ(N)                                                               
      TNOTO(MS)=TNOTO(MS)+TNMAT(M,N)*TNOTH(MT)                                  
040    CONTINUE                                                                 
030    CONTINUE                                                                 
020    CONTINUE                                                                 
      DO 25 K=1,6                                                               
      IW=NNODI*NCS+NMPC+K                                                       
      TNOTO(IW)=TNOTO(IW)+AO(K,I)                                               
 25   CONTINUE                                                                  
      iw=neqv-6+i                                                               
      IF(IPGCON(I).EQ.1.AND.PGCON(I).NE.0.)TNOTO(IW)=CONST*PGCON(I)             
      IF(DUMP(4))THEN                                                           
      WRITE(IOUT,'(/,A)') ' TNOTO DEL SECONDO PASSO TNSTP2'                     
      WRITE(IOUT,'(1X,6E12.6)') (TNOTO(IO),IO=1,NEQV)                           
      ENDIF                                                                     
      CALL JWRITE(ISLTN2,TNOTO,NEQV*2)                                          
10    CONTINUE                                                                  
                                                                                
      RETURN                                                                    
      END                                                                       
C                                                                               
C                                                                               
C----------------------------------------------- INIZIO -------------           
C  AGGIUNTA SUBROUTINE PER IL CALCOLO DEL TERMINE NOTO FINALE DEL               
C  VETTORE DELLE TEMPERATURE                                                    
C  TERMINE NOTO DI NNODI*(NCS=3)+(NPSI=6) TERMINI                               
C                                                                               
C*************************          TNSLTR          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE TNSLTR(TNOTH,TNOTO,TNMAT,IPIAZ)                                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP                                                              
      INTEGER SPCCOD                                                            
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,neqs,nsto        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
                                                                                
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      DIMENSION TNOTO(1),TNOTH(1),TNMAT(MAXDEL,1),IPIAZ(1)                      
      DATA MPCCOD,SPCCOD/6,7/                                                   
      CALL JPOSAB(ISTEMP,1)                                                     
C------                                                                         
C      verificare numero di termini da annullare in TNOTH                       
      CALL DZERO(TNOTH,NEQV)                                                    
C     legge temperature salvate sullo slot ISTEMP                               
      CALL JREAD(ISTEMP,TNOTH,(NEQV-NPSI)*2)                                    
c----------------------------------------------                                 
c     verifica sulla lettura di TNOTH()                                         
c      do 99,i=1,(neqv-npsi)                                                    
c      write(iout,*) 'tnoth(',i,')=',tnoth(i)                                   
c99    continue                                                                 
c-----------------------------------------------                                
      CALL DZERO(TNOTO,NEQV)                                                    
      CALL JPOSAB(ISLVPZ,1)                                                     
      CALL JPOSAB(ISMTER,1)                                                     
C                                                                               
C     PER TUTTI GLI ELEMENTI                                                    
C                                                                               
      DO 20 K=1,NELEM                                                           
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT,1)                                                   
      CALL JREAD(ISLVPZ,NT1,1)                                                  
      CALL JREAD(ISLVPZ,IPIAZ,NT-1)                                             
      JUMP=(NT-1)*NT1*2+1                                                       
      CALL JPOSRL(ISLVPZ,JUMP)                                                  
c-----                                                                          
C     solo per gli elementi tipo 5                                              
C     IF(ITIP.NE.5)GOTO20                                                       
C                                                                               
      IF(ITIP.EQ.MPCCOD)GOTO 20                                                 
      IF(ITIP.EQ.SPCCOD)GOTO 20                                                 
      IF(ITIP.EQ.2)GOTO 20                                                      
      NT=NT-1                                                                   
C     NWTRAS= NUMERO WORD DI TRASLAZIONE SULLO   SLOT ISMTER                    
C     (ISMTER CONTIENE PRIMA MATRICE 3NxN NON UTILE QUI)                        
C     6 o NPSI ?  ( ricorda: NT=3N+NPSI )                                       
      NWTRAS=2*(NT-6)*(NT-6)/3                                                  
      CALL JPOSRL(ISMTER,NWTRAS)                                                
      DO 70 IS=1,(NT-6)/3                                                       
      CALL JREAD(ISMTER,TNMAT(1,IS),NT1*2)                                      
c-----verifica che sono stati letti numeri voluti                               
c777   format(5x,'tnmat(',i2,',',i2,')=',e12.5)                                 
c      do 70,i1=1,nt1                                                           
c      write(iout,777) i1,is,tnmat(i1,is)                                       
c-----                                                                          
 70   CONTINUE                                                                  
C                                                                               
C                                                                               
      DO 30,M=1,NT                                                              
      MS=IPIAZ(M)                                                               
      DO 40,N=1,(NT-6)/3                                                        
      MT=IPIAZ(3*(N-1)+1)                                                       
      TNOTO(MS)=TNOTO(MS)+TNMAT(M,N)*TNOTH(MT)                                  
40    CONTINUE                                                                  
c     verificato che per un elemento tnoto e' giusto                            
c      write(iout,*) 'tnoto(ms=',m,')=',tnoto(ms)                               
30    CONTINUE                                                                  
20    CONTINUE                                                                  
                                                                                
      IF(DUMP(4))THEN                                                           
      WRITE(IOUT,'(/,A)') ' TNOTO TERMICO DEL SECONDO PASSO TNSLTR'             
      WRITE(IOUT,'(1X,6E12.6)') (TNOTO(IO),IO=1,NEQV)                           
      ENDIF                                                                     
      CALL JWRITE(ISTNOT,TNOTO,NEQV+NEQV)                                       
                                                                                
      RETURN                                                                    
      END                                                                       
C                                                                               
C     FINE ROUTINE COSTRUZIONE T.NOTO TERMICO                                   
C--------------------------------------------------------------------           
C                                                                               
C                                                                               
C@ELT,I ANBA*ANBA.CTNSTP                                                        
C*************************          CTNSTP          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE CTNSTP(TNOTH,TNOTO,TNMAT,IPIAZ)                                
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      COMPLEX*16 A1,TNOTH,TNOTO,TNMAT                                           
      LOGICAL DUMP                                                              
      INTEGER SPCCOD                                                            
      REAL*8 TEMP(6)                                                            
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,neqs,nsto        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      DIMENSION TNOTO(1),TNOTH(1),TNMAT(MAXDEL,1),IPIAZ(1),A1(6,6),             
     *          ELLE(6,6),ELLE1(6,6)                                            
      DATA MPCCOD,SPCCOD/6,7/                                                   
      CALL DZERO(A1,72)                                                         
      CALL JPOSAB(ISLSC1,1)                                                     
      CALL ITELLE(2,CDC,ELLE,C)                                                 
      CALL ITELLE(3,CDC,ELLE1,C)                                                
      DO 3 I=1,6                                                                
      DO 3 K=1,6                                                                
      A1(I,K)=DCMPLX(-ELLE(I,K),1.25*ELLE1(I,K))                                
  3   CONTINUE                                                                  
      CALL ITELLE(4,CDC,ELLE,C)                                                 
      CALL ITELLE(5,CDC,ELLE1,C)                                                
      DO 4 I=1,6                                                                
      DO 4 K=1,6                                                                
      A1(I,K)=A1(I,K)+DCMPLX(-0.5*ELLE(I,K),0.75*ELLE1(I,K))                    
  4   CONTINUE                                                                  
CC    WRITE(IOUT,'(/,A)') ' A1 REALE DEL SECONDO PASSO CTNSTP'                  
CC    WRITE(IOUT,'(1X,6E12.6)') ((DREAL(A1(IO,IC)),IC=1,6),IO=1,6)              
CC    WRITE(IOUT,'(/,A)') ' A1 IMMAGINARIA DEL SECONDO PASSO CTNSTP'            
CC    WRITE(IOUT,'(1X,6E12.6)') ((DIMAG(A1(IO,IC)),IC=1,6),IO=1,6)              
CC    WRITE(IOUT,'(/,A)') ' K 30*30'                                            
CC    CALL JPOSAB(ISLVPZ,1)                                                     
CC    DO 6 K=1,NELEM                                                            
CC    CALL JREAD(ISLVPZ,ITIP,1)                                                 
CC    CALL JPOSRL(ISLVPZ,1)                                                     
CC    CALL JREAD(ISLVPZ,NT,1)                                                   
CC    CALL JPOSRL(ISLVPZ,NT+1)                                                  
CC    IF(ITIP.NE.6.AND.ITIP.NE.7)THEN                                           
CC    DO 5 I=1,30*5                                                             
CC    CALL JREAD(ISLVPZ,TEMP,6*2)                                               
CC    WRITE(IOUT,'(1X,6(E12.6))') TEMP                                          
CC5   CONTINUE                                                                  
CC    ELSE                                                                      
CC    CALL JPOSRL(ISLVPZ,2)                                                     
CC    ENDIF                                                                     
CC6   CONTINUE                                                                  
CC    WRITE(IOUT,'(/,A)') ' H 30*30'                                            
CC    CALL JPOSAB(ISLMTN,1)                                                     
CC    DO 7 I=1,30*5                                                             
CC    CALL JREAD(ISLMTN,TEMP,6*2)                                               
CC    WRITE(IOUT,'(1X,6(E12.6))') TEMP                                          
CC7   CONTINUE                                                                  
CC    WRITE(IOUT,'(/,A)') ' M 24*24'                                            
CC    CALL JPOSAB(ISLMTF,1)                                                     
CC    CALL JPOSRL(ISLMTF,30*24*2)                                               
CC    DO 8 I=1,24*4                                                             
CC    CALL JREAD(ISLMTF,TEMP,6*2)                                               
CC    WRITE(IOUT,'(1X,6(E12.6))') TEMP                                          
CC8   CONTINUE                                                                  
      DO 10 I=1,6                                                               
      CALL JREAD(ISLSC1,TNOTH,NEQV*4)                                           
      CALL DZERO(TNOTO,NEQV*2)                                                  
      CALL JPOSAB(ISLVPZ,1)                                                     
      CALL JPOSAB(ISLMTN,1)                                                     
      CALL JPOSAB(ISLMTF,1)                                                     
      DO 20 K=1,NELEM                                                           
      CALL JREAD(ISLVPZ,ITIP,1)                                                 
      CALL JREAD(ISLVPZ,NT,1)                                                   
      CALL JREAD(ISLVPZ,NT1,1)                                                  
      CALL JREAD(ISLVPZ,IPIAZ,NT)                                               
      JUMP=(NT-1)*NT1*2                                                         
      CALL JPOSRL(ISLVPZ,JUMP)                                                  
CC    IF(ITIP.EQ.MPCCOD)GOTO 20                                                 
CC    IF(ITIP.EQ.SPCCOD)GOTO 20                                                 
CC    IF(ITIP.EQ.2)GOTO 20                                                      
      NT=NT-1                                                                   
      IF (ITIP.NE.6.AND.ITIP.NE.7) CALL JPOSRL(ISLMTF,NT1*(NT1-NPSI)*2)         
      DO 70 IR=1,NT                                                             
      DO 70 IS=1,NT1                                                            
      RR=0.D0                                                                   
      RI=0.D0                                                                   
      RH=0.D0                                                                   
      RM=0.D0                                                                   
      IF (ITIP.NE.6.AND.ITIP.NE.7) CALL JREAD(ISLMTN,RH,2)                      
      IF(IS.LE.NT1-NPSI.AND.IR.LE.NT1-NPSI) CALL JREAD(ISLMTF,RM,2)             
C                                                                               
      RR=RH*C                                                                   
      RI=RM*2*C**2                                                              
      TNMAT(IS,IR)=DCMPLX(RR,RI)                                                
70    CONTINUE                                                                  
CC    IF (I.EQ.1) THEN                                                          
CC    WRITE(IOUT,'(/,A)')' MATRICE C*H+I*2*C**2*M 30*30 PARTE REALE'            
CC    WRITE(IOUT,'(1X,6E12.6)')((DREAL(TNMAT(IO,IC)),IC=1,NT),IO=1,NT)          
CC    WRITE(IOUT,'(/,A)')' MATRICE C*H+I*2*C**2*M 30*30 PARTE',                 
CC   /' IMMAGINARIA'                                                            
CC    WRITE(IOUT,'(1X,6E12.6)')((DIMAG(TNMAT(IO,IC)),IC=1,NT),IO=1,NT)          
CC    ENDIF                                                                     
      DO 30 M=1,NT                                                              
      MS=IPIAZ(M)                                                               
      DO 30 N=1,NT                                                              
      MT=IPIAZ(N)                                                               
      TNOTO(MS)=TNOTO(MS)+TNMAT(M,N)*TNOTH(MT)                                  
30    CONTINUE                                                                  
20    CONTINUE                                                                  
      DO 80 K=1,6                                                               
      IW=NNODI*NCS+NMPC+K                                                       
      TNOTO(IW)=TNOTO(IW)+A1(K,I)                                               
80    CONTINUE                                                                  
      IF(DUMP(4))THEN                                                           
        WRITE(IOUT,'(/,A)')' TERMINE NOTO PARTE REALE IN CTNSTP'                
        WRITE(IOUT,'(1X,6E12.6)')(DREAL(TNOTO(IO)),IO=1,NEQV)                   
        WRITE(IOUT,'(1X,A)')' TERMINE NOTO PARTE IMMAGINARIA IN CTNSTP'         
        WRITE(IOUT,'(1X,6E12.6)')(DIMAG(TNOTO(IO)),IO=1,NEQV)                   
      ENDIF                                                                     
      CALL JWRITE(ISLTC2,TNOTO,NEQV*4)                                          
10    CONTINUE                                                                  
      write(iout,*)' Vincoli PDG2'                                              
      IF(IPGCON(I).EQ.1.AND.PGCON(I).NE.0.)TNOTO(IW)=CONST*PGCON(I)             
      RETURN                                                                    
      END                                                                       

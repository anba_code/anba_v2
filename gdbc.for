C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C                                G D B C
C
C                'G'ESTIONE  'D'ATI  'B'LANK  'C'OMMON
C                    VERSIONE ORIGINALE DI M. LANZ
C             AGGIORNATA NEL GENNAIO 1990  DA G. BINDOLINO
C
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
C
C
C     SUBROUTINE PER LA GESTIONE DATI IN BLANK COMMON
C
C     LE CHIAMATE PREVISTE SONO:
C
C        CALL JCRERS = INIZIALIZZAZIONE TABELLA DEI VETTORE ALLOCATI
C        CALL JAVLBL = FORNISCE IL NUMERO DI PAROLE DISPONIBILI
C        CALL JALMAX = FORNISCE IL NUMERO DI PAROLE DISPONIBILI
C                      TENENDO CONTO DEL NUMERO DI TABELLE DA ALLOCARE
C        CALL JWDMAX(N1,N2) = FORNISCE OCCUPAZIONE MASSIMA DEL COMMON
C                             N1 = MASSIMA OCCUPAZIONE ATTUALE
C                             N2 = MASSIMA OCCUPAZIONE DA INIZIO RUN
C        CALL JGETCO = ALLOCCA IN MANIERA DINAMICA AREA DI LAVORO
C                      SOLO IN AMBIENTE UNISYS
C        CALL JALLOC(*,VAR,TIP,ND,ID,INDIR) = ALLOCCA UN VETTORE
C                    *     = RETURN ALTERNATIVO IN CASO DI ERRORE
C                    VAR   = NOME VETTORE ( STRINGA CARATTERE DI NWN
C                            PAROLE
C                    TIP   = TIPO VETTORE. I TIPI AMMESSI SONO :
C                            'INT.' = INTERO
C                            'REAL' = REALE
C                            'D.P.' = DOPPIA PRECISIONE
C                            'CMPL' = COMPLESSO
C                            'ALFA' = ALFANUMERICO
C                            'CMPD' = COMPLESSO DOPPIA PRECISIONE
C                            'LOG.' = LOGICO
C                    ND    = NUMERO DIMENSIONI VETTORE
C                    ID    = VETTORE CONTENTE DIMENSIONI DEL VETTORE
C                            ALLOCATO
C                    INDIR = PUNTATORE DI INIZIO DEL VETTORE NELLA AREA
C                            LAVORO DEFINITO SECONDO IN TIPO DEL VETTORE
C        CALL JREQIN(*,NAME,ITIP,ND,ID,INDIR) = RESTITUISCE IL TIPO,
C                    NUMERO DI DIMENSIONI, LE DIMENSIONI E IL PUNTATORE
C                    DEL VETTORE NAME
C        CALL JFREE(*,NAME) = DELETA DALLA TABELLA IL VETTORE NAME
C        CALL JPACK(*) = IMPACCA LA AREA DI LAVORO NEL CASO DI MATRICI
C                        DELETATE
C        CALL JPRMEM = STAMPA INFORMAZIONI OCCUPAZIONE MEMORIA
C        CALL JPRTAB = STAMPA TABELLA VETTORI ALLOCATI
C        CALL JPRDAT = STAMPA INFORMAZIONI OCCUPAZIONE MEMORIA +
C                      STAMPA TABELLA VETTORI ALLOCATI +
C                      TUTTA LA MEMORIA
C        CALL JPREL(*,NAME) = STAMPA VETTORE NAME
C
C
C     *** ATTENZIONE IN UNISYS SCOMMENTARE LE ISTRUZIONI COMMENTATE
C     *** CON LA SIGLA C-UNI
C
C-UNI COMPILER (ARGCHK=OFF)
      SUBROUTINE GDBC
C
      DIMENSION NAME(2),ID(1),NTYPE(7)
      CHARACTER CNAME*(*),CITIP*(*)
      COMMON IV(1)
      COMMON /JCRMNG/ LCOM,IDAT,ITAB,NVAR,NWDEL,NWN,NWT(7),NDMAX,LUTAB,
     *                IFMSG,IPRNT,LUPUN,IAPACK,LOCV,MAXWD
      CHARACTER TYPE(7)*4
      EQUIVALENCE(NTYPE,TYPE)
      DATA TYPE/'INT.','REAL','D.P.','CMPL','ALFA','CMDP','LOG.'/
C
C
C
C     LCOM   = LUNGHEZZA BLANK COMMON
C     IDAT   = NUMERO PAROLE OCCUPATE
C     ITAB   = PUNTATORE INIZIO TABELLA
C     NVAR   = NUMERO VARIABILI
C     NWDEL  = NUMERO PAROLE DELETATE
C     NWN    = NUMERO DI PAROLE RISERVATE AL NOME DELLA VARIABILE
C     NWT    = VETTORE CONTENENTE NUMERO DI PAROLE PER TIPO :
C           (1)  INTERO
C           (2)  REALE
C           (3)  DOPPIA PRECISIONE
C           (4)  COMPLESSO
C           (5)  ALFANUMERICO
C           (6)  COMPLESSO DOPPIA PRECISIONE
C           (7)  LOGICO
C     NDMAX  = NUMERO MASSIMO DI DIMENSIONI
C     LUTAB  = LUNGHEZZA TABELLA PER OGNI SINGOLA VARIABILE:
C              LUTAB = NWN+2+NDMAX+1+1
C     IFMSG  = FILE STAMPA MESSAGGI DIAGNOSTICA
C     IPRNT  = FILE STAMPA TABELLA E/O DATI
C     LUPUN  = NWN+1+NDMAX+1
C     IAPACK = FLAG DI RICHIESTA DI PACK CON AGGIORNAMENTO
C              AUTOMATICO DELL'INDICE DELLA VARIABILE
C              ATTIVO SOLO IN AMBIENTE USISYS E IBM 4381
C     LOCV   = INDIRIZZO ASSOLUTO PRIMA PAROLA BLANK COMMON
C     MAXWD  = NUMERO MASSIMO DI PAROLE DA INIZIO RUN
C     STRUTTURA DEL DESCRITTORE DELLA MATRICE. TALE DESCRITTORE SI
C     TROVA NEL BLANK COMMON PRIMA DELLA MATRICE A CUI SI RIFERISCE
C     IV(JTAB)             = NOME
C     IV(JTAB+NWN-1)       = NOME
C     IV(JTAB+NWN  )       = INDIRIZZO VARIABILE NEL VETTORE TIPO
C     IV(JTAB+NWN+1)       = 100*ITIPO+ND
C     IV(JTAB+NWN+2)       = VETTORE DIMENSIONE
C     IV(JTAB+NWN+1+NDMAX) = VETTORE DIMENSIONE
C     IV(JTAB+NWN+2+NDMAX) = PUNTATORE POSIZIONE TABELLA SUCCESSIVA
C     IV(JTAB+NWN+3+NDMAX) = INDIRIZZO RELATIVO DELLA VARIABILE
C                            PUNTATORE DEFINITO DA JALLOC RISPETTO
C                            ALLA PRIMA PAROLA BLANK COMMON
C
C     LA INIZIALIZZAZIONE DEL BLANK COMMON E DEL COMMON /JCRMNG/ DEVE
C     AVVENIRE IN UN BLOCK DATA
C     I VALORI ATTUALMENTE DA UTILIZZARSI SONO:
C     DATA LCOM/LCOM/, IDAT/0/, ITAB/1/, NVAR/0/, NWDEL/0/, NWN/2/,
C    *     NWT/1,1,2,2,1,4,1/, NDMAX/3/, LUTAB/9/, IPRNT/6/, IFMSG/6/
C    *     LUPUN/7/, IAPACK/0/, LOCV/1/, MAXWD/0/
C
C
C     NOTA: IN AMBIENTE UNSYS MEDIANTE LA ROUTINE JGETCO E' POSSIBILE
C           ALLOCARE IN ESECUZIONE L'AREA DI LAVORO (E TRAMITE LA
C           JFRECO RILASCIARLA). IN QUESTO MODO TUTTE LE MATRICI
C           VENGONO ALLOCATE ALL'INTERNO DI QUESTA AREA DI LAVORO E IL
C           LORO INDIRIZZO VIENE DEFINITO RELATIVAMENTE ALLA PRIMA
C           POSIZIONE DI MEMORIA DEL VETTORE IV DEL BLANK COMMON.
C           TALE VETTORE DEVE AVERE DIMENSIONE UNITARIA E DEVE ESSERE
C           PRESENTE IN TUTTE LE ROUTINE. NEL CASO IN CUI NON SIA
C           POSSIBILE O NON SI VOGLIA UTILIZZARE L'ALLOCAMENTO DELLA
C           AREA DI LAVORO, SI DEVE DIMENSIONARE IL BLANK COMMON NEL
C           BLOCK DATA BDGFC MEDIANTE IL PARAMETRO MAXMEM DELLA
C           LUNGHEZZA CHE SI DESIRA E NON DI LUNGHEZZA UNITARIA E
C           NELLA JGETCO AVERE COME UNICA ISTRUZIONE ITAB=1
C           PER USARE TALE ROUTINE IN AMBIENTE UNISYS OCCORE RIVEDERE LA
C           FUNCTION MCORE E LA SUBROUTINE LCORE
C
C     NOTA: IN AMBIENTE UNISYS E IBM 4381 E' POSSIBILE ATTIVARE IL PACK
C           AUTOMATICO. IN QUESTO CASO VENGONO AUTOMATICAMENTE AGGIOR-
C           NATI I PUNTATORI CON LA NUOVA POSIZIONE DELLA MATRICE NELLO
C           AMBITO DEL BLANK COMMON.
C           PER USARE TALE OPZIONE OCCORRE RIVEDERE LA FUNCTIONE LOCVAR
C           UTILIZZANDO LE ISTRUZIONI RELATIVE AL CALCOLATORE IN ESAME
C
C
C-----------------------------------------------------------------------
      ENTRY JCRERS
C-----------------------------------------------------------------------
C     INIZIALIZZA TABELLA E DATI
C
      NVAR = 0
      IDAT = 0
      NWDEL = 0
      LUPUN = NWN + 2 + NDMAX
      MAXWDP = 0
C
C     DETERMINA INDIRIZZO ASSOLUTO DI IV(1)
C
      LOCV = 1
      IF(IAPACK.EQ.1) LOCV = LOCVAR(IV(1))
C
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JGETCO(ISIZE)
C-----------------------------------------------------------------------
C     ALLOCA AREA DI LAVORO IN MANIERA DINAMICA (SOLO SU UNISYS)
      IF(ISIZE .NE. -1) THEN
      LCOM = ISIZE
      IBASE = MCORE(ISIZE) - LOCVAR(IV(1)) + 1
      ITAB = (IBASE/2)*2 + 1
C      WRITE(*,*) ' IV ',IV(1),LOCVAR(IV(1))
      ELSE
      ITAB = 1
      ENDIF
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JFRECO(ISIZE)
C-----------------------------------------------------------------------
C     RILASCIA AREA DI LAVORO
      IF(ISIZE .NE. -1) THEN
      CALL LCORE(IBASE+ISIZE-LOCVAR(IV(1))-1)
      ENDIF
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JAVLBL(NWAV)
C-----------------------------------------------------------------------
C     FORNISCE NUMERO PAROLE LIBERE
C
      NWAV = LCOM - IDAT - 1
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JALMAX(NTAB,NWAV)
C-----------------------------------------------------------------------
C     FORNISCE NUMERO PAROLE LIBERE A MENO DI NTAB TABELLE
C
      NWAV = LCOM - IDAT - NTAB*(LUTAB+1)
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JWDMAX(NWAV1,NWAV2)
C-----------------------------------------------------------------------
C     FORNISCE OCCUPAZIONE MASSIMA
C
      NWAV1 = MAXWDP
      NWAV2 = MAXWD
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JALLOC(*,CNAME,CITIP,ND,ID,INDIRI)
C-----------------------------------------------------------------------
C     ALLOCA UNA VARIABILE
C
C --- INPUT
C     NAME   = NOME VARIABILE
C     ITIPO  = TIPO(NUMERO O TIPO SECONDO IL VETTORE TYPE)
C     ND     = NUMERO DI DIMENSIONI
C     ID     = DIMENSIONI
C --- OUTPUT
C     INDIRI = INDIRIZZO (NEL VETTORE DEL TIPO)
C
C     CERCA IL NOME IN TABELLA
C
      CALL JCGDBC(CNAME,NAME,NWN) 
      CALL JCGDBC(CITIP,ITIP,1)
 29   ITIPO = JTIPO(NAME,ITIP,NTYPE)
      JPOS = IPOS(NAME,NWN,IV(1),NVAR,LUPUN,ITAB)
C      WRITE(*,*) ' JPOS ',JPOS
      IF(JPOS .GT. 0) GO TO 9
C
C     NON TROVATO (JPOS.LT.0)
C
C     CALCOLA IL NUMERO DI PAROLE NECESSARIE
C
      NWORD = NWT(ITIPO)
      I = 0
 1    I = I + 1
      IF(I .GT. ND) GOTO 2
C***      IF(ID(I) .GT. 1) NWORD = NWORD*ID(I)
      NWORD = NWORD*ID(I)
      GO TO 1
 2    CONTINUE
C
C     CALCOLA IL NUMERO DI PAROLE DISPONIBILI
C
      NWFRE = LCOM - LUTAB - IDAT - NWT(ITIPO)
C
C     TEST SE MEMORIA SUFFICIENTE
C
      IF(NWORD .GT. NWFRE) GOTO 7
C
C     MEMORIA SUFFICIENTE: CALCOLA PUNTATORE
C
      JTAB = IABS(JPOS)
      DO 70 I = 1,NDMAX
 70   IV(JTAB+NWN+1+I) = 0
      INDIR = (IDAT + ITAB - 1 + LUTAB + NWT(ITIPO) -1)/NWT(ITIPO) + 1
      IDAT = (INDIR - 1)*NWT(ITIPO) + NWORD - ITAB + 1
      IF(IDAT .GT. MAXWDP) MAXWDP = IDAT
      IF(MAXWDP .GT. MAXWD) MAXWD = MAXWDP
C
C     INCREMENTA NUMERO VARIABILI
C
      NVAR = NVAR + 1
C
C     RIEMPIE LA TABELLA
C
      DO 5 I = 1,NWN
      IV(JTAB+I-1) = NAME(I)
 5    CONTINUE
C
C     PUNTATORE INIZIO NUOVA TABELLA
C
      IV(JTAB+LUPUN) = IDAT + ITAB
 4    JTAB = JTAB + NWN
      IV(JTAB) = INDIR
      IV(JTAB+1) = 100*ITIPO + ND
C***      IV(JTAB+2) = 1
      I = 0
 6    I = I + 1
      IF(I .GT. ND) GOTO 31
      IV(JTAB+I+1) = ID(I)
C***      IF(ID(I) .LE. 0) IV(JTAB+I+1) = 1
      GOTO 6
 31   CONTINUE
C
C     DETERMINA INDIRIZZO DI INDIRI (IAPACK = 1)
C
      IV(JTAB+LUTAB-1-NWN) = 0
      IF(IAPACK .EQ. 1) THEN
      LOCIN = LOCVAR(INDIRI)
      LOCIN = LOCIN - LOCV + 1
      IV(JTAB+LUTAB-1-NWN) = LOCIN
      ENDIF
C
      INDIRI = INDIR
C
      RETURN
C
C     MEMORIA NON SUFFICIENTE
C
C     TEST SE POSSIBILE IMPACCAMENTO ( SE IAPACK = 1)
C
 7    CONTINUE
      NAV = NWFRE + NWDEL
      IF(NAV .LT. NWORD)GO TO 8
      IF(IAPACK .EQ. 1) THEN
      IGO = 1
      GOTO 17
C
C     VA NELLA JPACK
C
      ELSE
      IER = 11
      CALL JCRERR(IER,NAME,NAV,NWORD,ID)
      CALL JPRMEM
      CALL JPRTAB
      RETURN 1
      ENDIF
C
C     IMPOSSIBILE ALLOCARE ANCHE DOPO POSSIBILE IMPACCAMENTO
C
 8    CONTINUE
      IER = 1
      CALL JCRERR(IER,NAME,ITIPO,NAV,NWORD)
      CALL JPRMEM
      CALL JPRTAB
      RETURN 1
C
C     NOME TROVATO IN TABELLA
C
 9    JTAB = JPOS + NWN
      ITYPO = IV(JTAB+1)/100
      INDIR = IABS(IV(JTAB))
      NDIM = IV(JTAB+1) -100*ITYPO
      IF(ITIPO .NE. ITYPO .OR. ND .NE. NDIM) GOTO 11
      DO 10 I = 1,ND
      IF(ID(I) .NE. IV(JTAB+I+1)) GOTO 11
 10   CONTINUE
C
C     GIA' ALLOCATO - IDENTICO
C     VERIFICA SE DELETATO
C
      IF(IV(JTAB) .GT. 0) GOTO 55
C     DELETATO
      JTAB = JTAB - NWN
      JPOS = IV(JTAB+LUPUN)
      NWORD = JPOS - JTAB
      NWDEL = NWDEL - NWORD
      JTAB = JTAB + NWN
      IV(JTAB) = -IV(JTAB)
      INDIR = IV(JTAB)
      RETURN
 55   IER = 2
      CALL JCRERR(IER,NAME,NTYPE(ITIPO),ND,ID)
      RETURN
C
C     GIA' ALLOCATO - DIVERSO
C     VERIFICA SE DELETATO
C
 11   IF(IV(JTAB) .GT. 0) GOTO34
      NWORD = NWT(ITIPO)
      I = 0
 32   I = I + 1
      IF(I .GT. ND) GOTO 33
C***      IF(ID(I) .GT. 1) NWORD = NWORD*ID(I)
      NWORD = NWORD*ID(I)
      GOTO 32
 33   CONTINUE
      JTAB = JTAB - NWN
      INDIRP = (JTAB+LUTAB+NWT(ITIPO)-2)/NWT(ITIPO)+1
      INDIRP = (INDIRP-1)*NWT(ITIPO)+1
      NWDISP = IV(JTAB+LUPUN)-INDIRP
      IF(NWORD .GT. NWDISP) GOTO 35
      JPOS = JTAB
      INDIR = INDIRP
      GOTO 4
 34   IER = 3
      CALL JCRERR(IER,NAME,NTYPE(ITIPO),ND,ID)
      RETURN 1
 35   IER = 12
      CALL JCRERR(IER,NAME,NWORD,NWDISP,ID)
      CALL JPRMEM
      CALL JPRTAB
      RETURN 1
C
C
C-----------------------------------------------------------------------
      ENTRY JREQIN(*,CNAME,IITIP,ND,ID,INDIRI)
C-----------------------------------------------------------------------
C     FORNISCE TIPO,DIMENSIONI E INDIRIZZO DELLA VARIABILE NAME
C
C     CERCA NOME IN TABELLA
C
      CALL JCGDBC(CNAME,NAME,NWN)
      JPOS = IPOS(NAME,NWN,IV(1),NVAR,LUPUN,ITAB)
      IF(JPOS .LE. 0) GO TO 13
      JTAB = JPOS + NWN
      IF(IV(JTAB) .LE. 0) GO TO 13
      ITIPO = IV(JTAB+1)/100
      IITIP = ITIPO
      ND = IV(JTAB+1) -100*ITIPO
      INDIRI = IV(JTAB)
      DO 12 I = 1,ND
      ID(I) = IV(JTAB+I+1)
 12   CONTINUE
      RETURN
C
 13   IER = 4
      CALL JCRERR(IER,NAME,ITIPO,ND,ID)
      RETURN 1
C
C
C-----------------------------------------------------------------------
      ENTRY JFREE(*,CNAME)
C-----------------------------------------------------------------------
C     DELETA VARIABILE DALLA TABELLA
C
      CALL JCGDBC(CNAME,NAME,NWN)
 54   JPOS = IPOS(NAME,NWN,IV(1),NVAR,LUPUN,ITAB)
      IF(JPOS .LE. 0) GO TO 16
      IC = 1
      JTAB = JPOS + NWN
C
C     TEST SE VARIABILE DELETATA
C
      IF(IV(JTAB) .LE. 0) RETURN
C
C     VARIABILE NON DELETATA
C
      IV(JTAB) = -IV(JTAB)
 56   NWORD = IV(JPOS+LUPUN) - JPOS
      NWDEL = NWDEL + IC*NWORD
C
C     RISTABILISCE PUNTATORI SE ULTIMA VARIABILE E' DELETATA
C
      IF(IV(JPOS+LUPUN) .NE. IDAT+ITAB) RETURN
      IDAT = IDAT - NWORD
      NWDEL = NWDEL - NWORD
      NVAR = NVAR - 1
C
C     VERIFICA CHE NON CI SIANO ALTRE VARIABILI DELETATE
C
      JTAB = ITAB
      DO 57 I = 1,NVAR
      JPOS = JTAB
      JTAB = IV(JTAB+LUPUN)
 57   CONTINUE
      JTAB = JPOS + NWN
      IF(IV(JTAB) .GT. 0) RETURN
      IC = 0
      GOTO 56
 16   IER = 5
      CALL JCRERR(IER,NAME,ITIPO,ND,ID)
      RETURN
C
C
C-----------------------------------------------------------------------
      ENTRY JPACK(*)
C-----------------------------------------------------------------------
C     IMPACCA TABELLA E DATI
C
      IGO = 2
      IF(NWDEL .LE. 0) GO TO 28
 17   NEL = 0
      JTAB = ITAB
      JTABV = ITAB
      IDAT = 0
      IDATV = 0
      JVAR = 0
      JVARV = 0
 18   JVARV = JVARV + 1
C
C     VERIFICA CHE NON SI SUPERI NUMERO MASSIMO DI VARIABILI
C
      IF(JVARV .GT. NVAR) GOTO 27
      JTABV = JTABV + NWN
C
C     VERIFICA SE VARIABILE E' STATA DELETATA
C
      IDATV = IV(JTABV+NDMAX+2) - ITAB
      IF(IV(JTABV) .LE. 0) GOTO 22
      JVAR = JVAR + 1
      IF(JVAR .EQ. JVARV) GOTO 21
      DO 19 I = 0,LUTAB-1
      IV(JTAB+I) = IV(JTABV+I-NWN)
C      WRITE(*,*) ' JTAB,IV(JTAB+I) ',IV(JTAB+I)
 19   CONTINUE
      ITIPO = IV(JTABV+1)/100
      INDIRV = IV(JTABV)
C      WRITE(*,*) ' ITIPO JTABV,INDIRV ',ITIPO,JTABV,INDIRV
      LOCOLD = INDIRV
      INDIRV = (INDIRV-1)*NWT(ITIPO) + 1
      INDIR = (IDAT+ITAB-1+LUTAB+NWT(ITIPO)-1)/NWT(ITIPO) + 1
      IV(JTAB+NWN) = INDIR
      INDIR = (INDIR-1)*NWT(ITIPO) +  1
      NWORD = IV(JTABV+NDMAX+2) - INDIRV
      DO 20 I = 0,NWORD-1
      IV(INDIR+I) = IV(INDIRV+I)
 20   CONTINUE
      IDAT = INDIR+NWORD - ITAB
      IV(JTAB+LUPUN) = IDAT+ITAB
C      WRITE(*,*) ' PACK: INDIR,INDIRV,NWORD,IDAT,ITAB,IV(JTAB+LUPUN)'
C      WRITE(*,*)  INDIR,INDIRV,NWORD,IDAT,ITAB,IV(JTAB+LUPUN)
C
C     PACK AUTOMATICO
C
      IF(IAPACK .EQ. 1) THEN
      INDIR = IV(JTAB+NWN)
      LOCIN = IV(JTAB+LUTAB-1)
      INDIRV = IV(LOCIN)
      IF(INDIRV .NE. LOCOLD) THEN
      IER = 13
      CALL JCRERR(IER,IV(JTAB),INDIRV,LOCOLD,IDUMMY)
      RETURN 1
      ELSE
      IV(LOCIN) = INDIR
      ENDIF
      ENDIF
C
C     FINE PACK AUTOMATICO
C
 21   IDAT = IV(JTAB+LUPUN) - ITAB
      JTAB = IDAT + ITAB
 22   CONTINUE
      JTABV = IDATV + ITAB
C      WRITE(*,*) ' PACK18: IDAT,JTAB,IDATV,JTABV'
C      WRITE(*,*)  IDAT,JTAB,IDATV,JTABV
      GOTO 18
 27   NVAR = JVAR
      NWDEL = 0
      IF(JVAR .EQ. JVARV-1) GOTO 28
      GOTO (29,30) IGO
 28   IER = 7
      CALL JCRERR(IER,NAME,ITIPO,ND,ID)
      RETURN
C
 30   RETURN
      END
C
C
C-----------------------------------------------------------------------
C-------------------------          IPOS          ----------------------
C-----------------------------------------------------------------------
      FUNCTION IPOS(NAME,NPN,NOMI,NVAR,LUPUN,ITAB)
C
C     LA FUNCTION CERCA NELLA TABELLA DELLE VARIABILI LA VARIABILE
C     'NAME' E NE FORNISCE LA POSIZIONE NEL VETTORE IV
C     (IPOS>0) SE 'NAME' ESISTE,(IPOS<0) SE NON ESISTE
C
      DIMENSION NAME(NPN),NOMI(1)
      IPOS = -ITAB
      IF(NVAR .LE. 0) RETURN
      JTAB = ITAB
      JVAR = 0
 1    JVAR = JVAR + 1
      DO 2 I = 1,NPN
      IF(NAME(I)-NOMI(JTAB+I-1)) 3,2,3
 2    CONTINUE
      IPOS = JTAB
      RETURN
 3    JTAB = NOMI(JTAB+LUPUN)
      IF(JVAR .LT. NVAR) GOTO 1
      IPOS = -JTAB
      RETURN
      END
C
C
C-----------------------------------------------------------------------
C------------------------           JITIPO          --------------------
C-----------------------------------------------------------------------
      FUNCTION JTIPO(NAME,ITIP,NTYPE)
C
C     FORNISCE IL TIPO DELLA VARIBILE ALLOCATA IN NUMERO SIA CHE SIA
C     DEFINITA IN NUMERO O STRINGA CARATTERE
C
      DIMENSION NTYPE(7)
      DO 10 J = 1,7
      IF(ITIP .NE. NTYPE(J)) GOTO 10
      JTIPO = J
      RETURN
  10  CONTINUE
      JTIPO = ITIP
      IF(JTIPO .GT. 0 .AND. JTIPO .LE. 7) RETURN
      IER = 10
      CALL JCRERR(IER,NAME,ITIP,IDUMMY,IDUMMY)
      STOP
      END
C
C
C-----------------------------------------------------------------------
C------------------------           JCGDBC          --------------------
C-----------------------------------------------------------------------
C-UNI COMPILER (ARGCHK=OFF)
      SUBROUTINE JCGDBC(CAR,ICAR,NC)
C
C     TRASFERISCE STRINGA CARATTERE IN VARIABILE INTERA
C
      CHARACTER CAR*(*),CP8*8
      DIMENSION ICAR(*),ICP(2)
      EQUIVALENCE (ICP,CP8)
C
      LCAR = LEN(CAR)
      IF(LCAR.GT.8) THEN
      WRITE(*,*) ' GDBC : NOME VARIABILE PIU'' LUNGO DI 8 CARATTERI'
      WRITE(*,*) ' VARIABILE ',CAR
      STOP
      ENDIF
      CP8=CAR
      DO 100 I = 1,NC
      ICAR(I) = ICP(I)
 100  CONTINUE
C
      RETURN
      END
C
C
C-----------------------------------------------------------------------
C-------------------------          JCRERR          --------------------
C-----------------------------------------------------------------------
C
C-UNI COMPILER (ARGCHK=OFF)
      SUBROUTINE JCRERR(IER,NAME,NTYPE,ND,ID)
C
C     SUBROUTINE PER LA DIAGNOSTICA DELLA SUBROUTINE GDBC
C
      COMMON /JCRMNG/ LCOM,IDAT,ITAB,NVAR,NWDEL,NWN,NWT(7),NDMAX,LUTAB,
     *                IFMSG,IPRNT,LUPUN,IAPACK,LOCV,MAXWD
      DIMENSION NAME(2),ID(1)
C
      GOTO (10,20,30,40,50,60,70,80,90,100,110,120,130) IER
C
 10   WRITE(IFMSG,101) NAME(1),NAME(2),ND,ID(1)
 101  FORMAT(//1X,10('-'),' GDBC ERROR 100'/
     *         12X,'MAXIMUM STORAGE EXCEEDED BY VARIABLE ',2A4/
     *         12X,'PACK IS USELESS AS THE AVAILABLE LOCATIONS',I9,
     *         12X,'ARE LESS THAN',I9)
      GOTO 2000
C
 20   WRITE(IFMSG,200) NAME(1),NAME(2),NTYPE,(ID(I),I=1,ND)
 200  FORMAT(//1X,10('-'),' GDBC WARNING 200'/
     *      12X,'VARIABLE ALREADY ALLOCATED WITH SAME TYPE AND/OR',
     *     ' DIMENSIONS'/12X,'NAME ',2A4,' - TYPE ',A4,' - DIMENSIONS ',
     *      10I5)
      GOTO 2000
C
 30   WRITE(IFMSG,300) NAME(1),NAME(2),NTYPE,(ID(I),I=1,ND)
 300  FORMAT(//1X,10('-'),' GDBC  ERROR  300'/
     *     12X,'VARIABLE ALREADY ALLOCATED WITH DIFFERENT TYPE AND/OR',
     *     ' DIMENSIONS'/12X,'NAME ',2A4,' - TYPE ',A4,' - DIMENSIONS ',
     *      10I5)
      GOTO 2000
C
 40   WRITE(IFMSG,400) NAME(1),NAME(2)
 400  FORMAT(//1X,10('-'),' GDBC ERROR 400'/
     *         12X,'THE VARIABLE ',2A4,' IS REQUESTED ',
     *         'BUT HAS NEVER BEEN ALLOCATED')
      GOTO 2000
C
 50   WRITE(IFMSG,500) NAME(1),NAME(2)
 500  FORMAT(//1X,10('-'),' GDBC WARNING 500'/
     *         12X,'THE VARIABLE ',2A4,' CANNOT BE DELETED ',
     *         'AS HAS NEVER BEEN ALLOCATED')
      GOTO 2000
C
C60   WRITE(IFMSG,600) NAME(1),NAME(2)
C600  FORMAT(//1X,10('-'),' GDBC ERROR 600'/
C    *         11X,' VARIABLE ',2A4,' -  WRONG DATA AGREEMENT ',
C    *         'DURING PACKING')
 60   GOTO 2000
C
 70   WRITE(IFMSG,700)
 700  FORMAT(//1X,10('-'),' GDBC WARNING 700'/
     *          12X,'REQUESTED PACK WHEN NO DELETED VARIABLES')
      GOTO 2000
C
 80   WRITE(IFMSG,800) NAME(1),NAME(2),ND
 800  FORMAT(//1X,10('-'),' GDBC WARNING 800'/
     *         12X,'THE VARIABLE ',2A4,' CANNOT BE PRINTED ',
     *         ' BECAUSE DELETED OR NEVER'/
     *         12X,'ALLOCATED (JPOS =',I6,' )')
      GOTO 2000
C
C90   WRITE(IFMSG,900) ND,ID(1)
C900  FORMAT(//1X,10('-'),' GDBC ERROR 900'/
C    *         11X,'ATTEMPT TO RESTORE A FILE WITH A LENGHT OF',
C    *         I10,' LOCATIONS, GREATER THAN CURRENT COMMON LENGHT',I10)
 90   GOTO 2000
C
 100  WRITE(IFMSG,1000) NTYPE,NAME(1),NAME(2)
 1000 FORMAT(//1X,10('-'),' GDBC ERROR 1000',/
     *       12X,'INCORRECT TYPE ',A4,' FOR VARIABLE ',2A4)
      GOTO 2000
C
 110  WRITE(IFMSG,1100) NAME(1),NAME(2),NTYPE,ND
 1100 FORMAT(//,1X,10('-'),' GDBC ERROR 1100',/
     *       12X,'VARIABLE ',2A4,' CAN BE ALLOCATED ONLY AFTER A PACK',
     *       /,12X,'AVAILABLE LOCATIONS (FREE + DELETED)',I10,
     *       /,12X,'REQUIRED LOCATIONS',I10)
      GOTO 2000
C
 120  WRITE(IFMSG,1200) NAME(1),NAME(2),NTYPE,ND
 1200 FORMAT(//,1X,10('-'),' GDBC ERROR 1200',/
     *       12X,'VARIABLE ',2A4,' IS DELETED AND NECESSARY LOCATIONS',
     *       I10,/
     *       12X,'ARE GREATER THAN JUST ALLOCATED LOCATIONS',I10)
      GOTO 2000
C
 130  WRITE(IFMSG,1300) NAME(1),NAME(2),NTYPE,ND
 1300 FORMAT(//,1X,10('-'),' GDBC ERROR 1300',/
     *       12X,'FOR VARIABLE ',2A4,' THE INDEX ',I10,' IS DIFFERENT'
     *       /,12X,' FROM INDEX ',I10,' OF THE TABLE')
C
 2000 RETURN
      END
C
C
C-----------------------------------------------------------------------
C-------------------------          JINOUT          --------------------
C-----------------------------------------------------------------------
C
C-UNI COMPILER (ARGCHK=OFF)
      SUBROUTINE JINOUT
C
C     SUBROUTINE PER LA STAMPA DELLA MEMORIA, DELLA TABELLA DELLA
C     MEMORIA, DEI DATI CONTENUTI IN MEMORIA O DI UNA SINGOLA
C     VARIABILE CONTENUTA IN MEMORIA, PER LA SUBR. GDBC
C
      DIMENSION NAME(2),NTYPE(7)
      CHARACTER CNAME*(*)
      CHARACTER TYPE(7)*4
      COMMON /JCRMNG/ LCOM,IDAT,ITAB,NVAR,NWDEL,NWN,NWT(7),NDMAX,LUTAB,
     *                IFMSG,IPRNT,LUPUN,IAPACK,LOCV,MAXWD
C
      COMMON IV(1)
      REAL V(1)
      DOUBLE PRECISION DV(1)
      COMPLEX CV(1)
      LOGICAL LV(1)
      EQUIVALENCE(IV(1),V(1),DV(1),CV(1),LV(1))
      EQUIVALENCE(TYPE,NTYPE)
      DATA TYPE/'INT.','REAL','D.P.','CMPL','ALFA','CMDP','LOG.'/
C
C
C-----------------------------------------------------------------------
      ENTRY JPRMEM
C-----------------------------------------------------------------------
      IGO1 = 2
      GO TO 34
C
C-----------------------------------------------------------------------
      ENTRY JPRTAB
C-----------------------------------------------------------------------
      IGO2 = 2
      GO TO 35
C
C-----------------------------------------------------------------------
      ENTRY JPRDAT
C-----------------------------------------------------------------------
      IGO2 = 1
      IGO3 = 1
      GO TO 35
C
C-----------------------------------------------------------------------
      ENTRY JPREL(*,CNAME)
C-----------------------------------------------------------------------
      CALL JCGDBC(CNAME,NAME,NWN)
      JPOS = IPOS(NAME,NWN,IV(1),NVAR,LUPUN,ITAB)
      IF(JPOS .LT. 0) GO TO 33
      IGO2 = 1
      IGO3 = 2
      JTAB = JPOS
      WRITE(IPRNT,950)
950   FORMAT(6X,'NAME -- TYPE --  LOCATION  --      DIMENSIONS      --'
     *      /)
      GOTO 36
 33   IER = 8
      CALL JCRERR(IER,NAME,IDUMMY,JPOS,IDUMMY)
      RETURN1
C
C     STAMPA MEMORIA
C
 34   MFRE = LCOM - IDAT
      WRITE(IPRNT,900) LCOM,IDAT,MFRE,NVAR,NWDEL
900   FORMAT(1H1,9X,10(1H-),2X,'STORAGE STATISTICS',2X,10(1H-)//
     *       11X,'COMMON LENGHT',16X,I10/
     *       11X,'LAST LOCATION IN DATA VECTOR ',I10/
     *       11X,'AVAILABLE LOCATIONS',10X,I10/
     *       11X,'ALLOCATED VARIABLES',10X,I10/,
     *       11X,'DELETED LOCATIONS',12X,I10)
      GO TO (35,47),IGO1
C
C     STAMPA TABELLA ( E DATI )
C
 35   IF(NVAR .LE. 0) RETURN
      WRITE(IPRNT,950)
      I = 0
      JTAB = ITAB
 36   I = I + 1
      ITIPO = IV(JTAB+NWN+1)/100
      ND    = IV(JTAB+NWN+1)-100*ITIPO
      NWORD = NWT(ITIPO)
      DO 37 K = 1,ND
      NWORD = NWORD*IV(JTAB+NWN+K+1)
 37   CONTINUE
      WRITE(IPRNT,1000)(IV(JTAB+J-1),J=1,NWN),NTYPE(ITIPO),
     *                 IV(JTAB+NWN),(IV(JTAB+NWN+K+1),K=1,NDMAX),
     *                 IV(JTAB+LUTAB-2),IV(JTAB+LUTAB-1)
 1000 FORMAT(5X,2A4,2X,A4,1X,I10,4X,10I8)
      GOTO (38,46),IGO2
 38   INDIR = IABS(IV(JTAB+NWN))-1
      NWORD = NWORD/NWT(ITIPO)
      GOTO(39,40,41,42,43,44,45),ITIPO
 39   WRITE(IPRNT,1100) (IV(INDIR+K),K=1,NWORD)
 1100 FORMAT(10I13)
      GOTO (46,47),IGO3
 40   WRITE(IPRNT,1200) (V(INDIR+K),K=1,NWORD)
 1200 FORMAT(10E13.5)
      GOTO (46,47),IGO3
 41   WRITE(IPRNT,1300) (DV(INDIR+K),K=1,NWORD)
 1300 FORMAT(10D13.5)
      GOTO (46,47),IGO3
 42   WRITE(IPRNT,1400) (CV(INDIR+K),K=1,NWORD)
 1400 FORMAT(5(2X,2E12.5))
      GOTO (46,47),IGO3
 43   WRITE(IPRNT,1500) (IV(INDIR+K),K=1,NWORD)
 1500 FORMAT(20(2X,A4))
      GOTO (46,47),IGO3
 44   NWORD = NWORD + NWORD
      WRITE(IPRNT,1600) (DV(INDIR+K),K=1,NWORD)
 1600 FORMAT(5(2X,2D12.5))
      GOTO (46,47),IGO3
 45   WRITE(IPRNT,1700) (LV(INDIR+K),K=1,NWORD)
 1700 FORMAT(8X,30L4)
      GOTO (46,47),IGO3
 46   JTAB = IV(JTAB+LUPUN)
      IF(I .LT. NVAR) GOTO 36
 47   RETURN
      END
C
C
C-----------------------------------------------------------------------
C-------------------     FUNCTION LOCVAR       -------------------------
C-----------------------------------------------------------------------
C
C     DETERMINA L'INDIRIZZO ASSOLUTO DI UNA VARIABILE. TALE FUNCTION
C     DIPENDE DAL CALCOLATORE UTILIZZATO E DAL LIVELLO FORTRAN
C
      FUNCTION LOCVAR(IVAR)
C
C --- VERSIONE PER P.C. COMPILATORE MICROSOFT --------------------------
C     NON DEVE ESSERE ATTIVATA TALE OPZIONE
      WRITE(*,*) ' *** NO FUNCTION LOCVAR PER MICROSOFT ***'
      WRITE(*,*) ' *** PORRE IAPACK=0 IN BLOCK DATA ***'
      STOP
C
C     LOCVAR = LOC(IVAR)
C
C
C
C --- VERSIONE PER ELABORATORE UNISYS ----------------------------------
C     ESISTE FUNCTION 'LOC' RICHIAMABILE DA FORTRAN CHE FORNISCE
C     DIRETTAMENTE L'INDIRIZZO
C
C     LOCVAR = LOC(IVAR)
C
C
C --- VERSIONE PER IBM 4381 CON FORTVS 4.1. ---------------------------
C     E' NECESSARIA ROUTINE IN ASSEMBLER 'IADDR' IL CUI LISTATO VIENE
C     DI SEGUITO ALLEGATO
C
C     CALL IADDR(IVAR,IVR)
C     LOCVAR = (IVR-3)/4+1
C
C
      RETURN
      END
C
C
C-----------------------------------------------------------------------
C-------------------      FUNCTION MCORE       -------------------------
C-----------------------------------------------------------------------
C
C     RITORNA L'INDIRIZZO ASSOLUTO DELL'AREA DI LAVORO COMMON ALLOCATA
C     IN MANIERA DINAMICA DURANTE L'ESECUZIONE. E' UTILIZZABILE SOLO
C     IN AMBIENTE UNISYS EXEC-8
C
      FUNCTION MCORE(IVAR)
C
C
C --- VERSIONE PER ELABORATORE UNISYS ----------------------------------
C     ESISTE FUNCTION 'MCORF$' RICHIAMABILE DA FORTRAN CHE FORNISCE
C     DIRETTAMENTE L'INDIRIZZO
C
C     MCORE = MCORF$(IVAR)
C
C
C --- VERSIONE PER TUTTI ALTRI CALCOLATORI -----------------------------
C     NON DEVE ESSERE ATTIVATA TALE OPZIONE
      WRITE(*,*) ' *** NO FUNCTION MCORE. USARE JGETCO CON ARGOMENTO=-1'
     *           ,' ***'
      WRITE(*,*) ' *** E DIMENSIONARE COMMON IN BLOCK DATA ***'
      STOP
C
      RETURN
      END
C
C
C-----------------------------------------------------------------------
C-------------------   SUBROUTINE  LCORE       -------------------------
C-----------------------------------------------------------------------
C
C     RILASCIA L'AREA DI LAVORO COMMON ALLOCATA IN MANIERA DINAMICA
C     DURANTE L'ESECUZIONE. E' UTILIZZABILE SOLO IN UNISYS EXEC-8
C
C-UNI COMPILER (ARGCHK=OFF)
      SUBROUTINE LCORE(IVAR)
C
C
C --- VERSIONE PER ELABORATORE UNISYS ----------------------------------
C     ESISTE FUNCTION 'LCORF$' RICHIAMABILE DA FORTRAN
C
C     CALL LCORF$(IVAR)
C
C
C --- VERSIONE PER TUTTI ALTRI CALCOLATORI -----------------------------
C     NON DEVE ESSERE ATTIVATA TALE OPZIONE
      WRITE(*,*) ' *** NO FUNCTION LCORE. USARE JFRECO CON ARGOMENTO=-1'
     *           ,' ***'
      STOP
C
      RETURN
      END
C
C
*----------------------------------------------------------------------
*----------------------          IADDR            ---------------------
*----------------------------------------------------------------------
*
*      SUBROUTINE DA COMPILARE IN ASSEMBLER IN IBM 4381
*
*      CALL IADDR (VAR,INDIR)
*      VAR  = VARIBILE
*      INDIR = INDIRIZZO VARIABILE
*
*ADDR CSECT                  * IADDR CSECT
*         USING IADDR,15
*         STM 14,12,12(13)
*         L   2,0(1)
*         L   3,4(1)
*         ST  2,0(3)
*         LM  14,12,12(13)
*         BR  14
*         END

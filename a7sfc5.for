C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C                                                                               
C@ELT,I ANBA*ANBA.REASFC                                                        
C*************************          REASFC          ********************MOD     
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE REASFC(COOR,SOL,NUME,NUINT,NRSOL,R,ISLELM)                     
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                        
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      DIMENSION R(6,6),COOR(2,1),SOL(NRSOL,1),NUME(1),NUINT(1)                  
C                                                                               
      CALL DZERO(R,36)                                                          
      DO 10 ISPC=1,NSPC                                                         
      CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JREAD(ISLELM,NODO,1)                                                 
      CALL JREAD(ISLELM,ICOMP,1)                                                
      CALL JREAD(ISLELM,RIGID,2)                                                
      CALL JPOSRL(ISLELM,2)                                                     
      DO 10 I=1,6                                                               
      IND=(NUINT(NODO)-1)*3+ICOMP                                               
      REA=SOL(NUME(IND),I)*RIGID                                                
      R(ICOMP,I)=R(ICOMP,I)+REA                                                 
      IF(ICOMP.EQ.1)THEN                                                        
      R(6,I)=R(6,I)-REA*COOR(2,NODO)                                            
      ENDIF                                                                     
      IF(ICOMP.EQ.2)THEN                                                        
      R(6,I)=R(6,I)+REA*COOR(1,NODO)                                            
      ENDIF                                                                     
      IF(ICOMP.EQ.3)THEN                                                        
      R(4,I)=R(4,I)+REA*COOR(2,NODO)                                            
      R(5,I)=R(5,I)-REA*COOR(1,NODO)                                            
      ENDIF                                                                     
  10  CONTINUE                                                                  
      RETURN                                                                    
      END                                                                       
                                                                                

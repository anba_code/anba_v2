C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.INPCOR                                                        
C*************************          INPCOR          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPCOR                                                         
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE E' FITTIZIA E CONTIENE GLI ENTRY RELATIVI               
C     AGLI ELEMENTI DI  "CORRENTE"                                              
C                                                                               
C     INMATC - LETTURA  MATERIALI                                               
C     INPROC - LETTURA  PROPRIETA'                                              
C     INCONC - LETTURA  CONNESSIONI                                             
C     WRMATC - STAMPA MATERIALI                                                 
C     WRPROC - STAMPA PROPRIETA'                                                
C                                                                               
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      DIMENSION IDMAT(*),CAR(2,*),IDPRO(*),SEZ(*),LABEL(*),COOR(2,*)            
     1          ,VETT(*),ICON(*),NODCNT(*)                                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /CORR/   E,RO,A                                                    
      common /outnas/ nasmod,naspch                                             
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat                            
C                                                                               
C                                                                               
C                                                                               
      ENTRY INMATC(*,*,MAXMAT,IDMAT,CAR,NMAT)                                   
C                                                                               
C                                                                               
      DO 10 I=1,MAXMAT                                                          
      CALL READFF(ISYM,0,NF,*100,*20)                                           
      CALL INTGFF(*101,IDMAT(I),0,LENFL)                                        
      CALL DOUBFF(*101,CAR(1,I),0,LENFL)                                        
      CALL DOUBFF(*101,CAR(2,I),0,LENFL)                                        
 10   CONTINUE                                                                  
7      CALL READFF(ISYM,0,NF,*100,*20)                                          
      WRITE(IOUT,900)MAXMAT                                                     
      CALL JHALT                                                                
      STOP                                                                      
 100  RETURN 1                                                                  
 101  RETURN 2                                                                  
 20   NMAT=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY INPROC(*,*,MAXPRO,IDPRO,SEZ,NPRO)                                   
C                                                                               
C                                                                               
      DO 30 I=1,MAXPRO                                                          
      CALL READFF(ISYM,0,NF,*102,*40)                                           
      CALL INTGFF(*103,IDPRO(I),0,LENFL)                                        
      CALL INTGFF(*103,IDPRO(I+MAXPRO),0,LENFL)                                 
      CALL DOUBFF(*103,SEZ(I),0,LENFL)                                          
 30   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*102,*40)                                           
      WRITE(IOUT,901)MAXPRO                                                     
      CALL JHALT                                                                
      STOP                                                                      
 102  RETURN 1                                                                  
 103  RETURN 2                                                                  
 40   NPRO=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY INCONC(*,*,IDMAT,CAR,NMAT,IDPRO,SEZ,NPRO,LABEL,COOR,VETT,           
     1             ICON,NODCNT,IER)                                             
C                                                                               
C                                                                               
      ITIP=1                                                                    
      IFLMR=1                                                                   
      NCON=1                                                                    
      MAXNOD=1                                                                  
      NEL=0                                                                     
      NCORR=0                                                                   
      NCOR=0                                                                    
      NWC=0                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 60   CALL READFF(ISYM,0,NF,*104,*50)                                           
      CALL INTGFF(*105,IDEL,0,LENFL)                                            
      CALL INTGFF(*105,IPROP,0,LENFL)                                           
      CALL INTGFF(*105,ICON,0,LENFL)                                            
      ICONNE=ICON(1)                                                            
      IF(NF.EQ.5)THEN                                                           
      CALL DOUBFF(*105,VX,0,LENFL)                                              
      CALL DOUBFF(*105,VY,0,LENFL)                                              
      END IF                                                                    
      NCOR=NCOR+1                                                               
      IF(IFLMR.NE.0)NCORR=NCORR+1                                               
      NEL=NEL+1                                                                 
      IF(NEL.LE.50)GOTO 61                                                      
      NEL=0                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
 61   IPP=IPOSL(IPROP,IDPRO,NPRO)                                               
      IF(IPP.EQ.0) GOTO 800                                                     
      IM=IDPRO(IPP+NPRO)                                                        
      IF(IM.LT.0)GOTO 1000                                                      
      IPM=IPOSL(IM,IDMAT,NMAT)                                                  
      IF(IPM.EQ.0) GOTO 801                                                     
      IFLMR=IDMAT(IPM)                                                          
      E=CAR(1,IPM)                                                              
      RO=CAR(2,IPM)                                                             
 70   A=SEZ(IPP)                                                                
      IER=0                                                                     
                                                                                
c -----------------------------------------------------------                   
      if(nstrat.gt.0) then
      do iok=1,nstrat                                                           
      icont=icont+1                                                             
      iux=10000000                                                              
      nt1=iux*(iok-1)                                                           
      nt2=iux*iok                                                               
      WRITE(nasmod,721)IDEL+nt1,IPROP,ICON(1)+nt1,icon(1)+nt2                   
721   format('CROD    ',6i8)                                                    
      enddo               
      else
      WRITE(nasmod,821)IDEL,IPROP,ICON(1)                                      
821   format('CELAS1  ',6i8)                                                   
      endif                                                      
c -----------------------------------------------------------                   
                                                                                
                                                                                
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      IF(NF.EQ.5) THEN                                                          
      VETT(1)=VX                                                                
      VETT(2)=VY                                                                
      END IF                                                                    
      WRITE(IOUT,609)NCOR,IDEL,ICONNE,IPROP,VETT(1),VETT(2),A,E,RO,             
     *   iflmr                                                                  
      IF(IER.EQ.0)GOTO 80                                                       
      IERG=1                                                                    
      WRITE(IOUT,905)IDEL                                                       
      GOTO 60                                                                   
 80   CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,1)                                                
      CALL JWRITE(ISLELM,VETT,4)                                                
      CALL JWRITE(ISLELM,E,6)                                                   
      NWC=NWC+15                                                                
      GOTO 60                                                                   
 50   RETURN                                                                    
 104  RETURN 1                                                                  
 105  RETURN 2                                                                  
 1000 CONTINUE                                                                  
      CALL READFL(IPM,IER,ITIP,E,IFL,RO)                                        
      IF(IER.EQ.0)GOTO 70                                                       
      WRITE(IOUT,904)IPM                                                        
  66  IER=1                                                                     
      GOTO 60                                                                   
 800  WRITE(IOUT,902)IPROP                                                      
      GOTO 66                                                                   
 801  WRITE(IOUT,903)IM                                                         
      GOTO 66                                                                   
 700  FORMAT(//50X,'***   ELEMENTI DI CORRENTE   ***')                          
 710  FORMAT(//5X,'N.RO  ',3X,'ID.ELEM',5X,'NODO',5X,'PROPRIETA''',4X,          
     1  'X',12X,'Y',5X,                                                         
     2  'SEZIONE',4X,'MOD.ELASTICO',4X,'DENSITA'''//)                           
 609  FORMAT(5X,I8,3I10,6X,5E12.6,i8)                                           
 900  FORMAT(' ***   AVVISO (INMATC) : OVERFLOW, N. MASSIMO DI',                
     1 ' MATERIALI AMMESSO E''',I8,'   ***')                                    
 901  FORMAT(' ***   AVVISO (INPROC) : OVERFLOW, N. MASSIMO DI',                
     1 ' PROPRIETA'' AMMESSO E''',I8,'   ***')                                  
 902  FORMAT(' ***   AVVISO (INCONC) : PROPRIETA'' NON DICHIARATA'              
     1 ,I8,'   ***')                                                            
 903  FORMAT(' ***   AVVISO (INCONC) : MATERIALE NON DICHIARATO'                
     1 ,I8,'   ***')                                                            
 904  FORMAT(' ***   AVVISO (INCONC) : MATERIALE NON PRESENTE IN'               
     1 ,' BIBLIOTECA',I8,'   ***')                                              
 905  FORMAT(' ***   AVVISO (INCONC) : NON RECUPERATI NODI '                    
     1 ,'DELL''ELEMENTO N.RO',I8,'   ***')                                      
C                                                                               
C                                                                               
C                                                                               
      ENTRY WRMATC(IDMAT,CAR,NMAT)                                              
C                                                                               
C                                                                               
      IMAT=0                                                                    
      NMPAG=55                                                                  
      NPAG=(NMAT+54)/NMPAG                                                      
      DO 2000 I=1,NPAG                                                          
      IF(I.EQ.NPAG)NMPAG=NMAT-(NPAG-1)*NMPAG                                    
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      DO 2100 L=1,NMPAG                                                         
      IMAT=IMAT+1                                                               
      WRITE(IOUT,3100)IMAT,IDMAT(IMAT),(CAR(K,IMAT),K=1,2)                      
 2100 CONTINUE                                                                  
 2000 CONTINUE                                                                  
 3000 FORMAT(/40X,'***   MATERIALI ELEMENTI DI CORRENTE   ***'//                
     1 33X,'   N.RO    ID.MAT.',8X,'MOD. ELASTICO        DENSITA'''/)           
 3100 FORMAT(31X,I8,2X,I8,7X,2E16.6)                                            
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
      ENTRY WRPROC(IDPRO,SEZ,NPRO)                                              
C                                                                               
C                                                                               
      IPRO=0                                                                    
      NPPAG=55                                                                  
      NPAG=(NPRO+54)/NPPAG                                                      
      DO 2200 I=1,NPAG                                                          
      IF(I.EQ.NPAG)NPPAG=NPRO-(NPAG-1)*NPPAG                                    
      CALL INTEST                                                               
      WRITE(IOUT,3200)                                                          
      DO 2300 L=1,NPPAG                                                         
      IPRO=IPRO+1                                                               
      WRITE(IOUT,3300)IPRO,IDPRO(IPRO),IDPRO(IPRO+NPRO),SEZ(IPRO)               
 2300 CONTINUE                                                                  
 2200 CONTINUE                                                                  
 3200 FORMAT(/40X,'***   PROPRIETA'' ELEMENTI DI CORRENTE   ***'//              
     1 32X,'   N.RO    ID.PROP.',12X,'ID.MAT.',12X,'AREA'/)                     
 3300 FORMAT(30X,I8,4X,I8,11X,I8,4X,E16.6)                                      
      RETURN                                                                    
      END                                                                       
C----------------------------------------------------inizio ------              
C@ELT,I ANBA*ANBA.INPELP                                                        
C************************         INPELP        ************************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPELP                                                         
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE E' FITTIZIA E CONTIENE GLI ENTRY RELATIVI               
C     AGLI ELEMENTI BIDIMENSIONALI (ELEMENTI PIANI)                             
C                                                                               
C     INMATE - LETTURA  MATERIALI                                               
C     INPROE - LETTURA  PROPRIETA'                                              
C     INCONE - LETTURA  CONNESSIONI                                             
C     WRMATE - STAMPA MATERIALI                                                 
C     WRPROE - STAMPA PROPRIETA'                                                
C                                                                               
C                                                                               
C     aggiunti ISTEMP , CDTER                                                   
C     modificati VOTT(30),                                                      
C                                                                               
      IMPLICIT REAL*8  (A-H,O-Z)                                                
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELPI2/  INT1,INT2,ANGOLO,DEN,ALFORT,IFLANG                        
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      common /outnas/ nasmod,naspch                                             
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat                            
      DIMENSION IDMAT(*),ELAS(6,*),DENS(*),PERM(*),TMPMT(6,*),IDPRO(*),         
     1        TETA(*),ALFA(*),ICON(*),VETT(*),LABEL(*),COOR(2,*),               
     2      SCOMP(6,6),TRSMT(6,6),IVP(6),VOTT(30),NODCNT(*),CDTER(6,*),         
     3   t1(3,3),t2(3,3),d(6,6),t(6,6),tt(6,6),cdtert(6),idnt(12)                        
C                                                                               
c     DATA IVP/6,5,3,1,2,4/                                                     
      DATA IVP/4,5,1,2,3,6/                                                     
      data icont/0/                                                             
C                                                                               
      ENTRY INMATE(*,*,MAXMAT,IDMAT,ELAS,DENS,CDTER,NMAT,PERM,TMPMT,IER)        
C                                                                               
      IER=0                                                                     
      DO 10 I=1,MAXMAT   +1                                                     
      IB=(I-1)*6                                                                
      NS=0                                                                      
 101  CALL READFF(ISYM,0,NF,*107,*40)                                           
      NI=NS+1                                                                   
      NS=NI+NF-1                                                                
      DO 100 K=NI,NS                                                            
      CALL DOUBFF(*108,VOTT(K),0,LENFL)                                         
 100  CONTINUE                                                                  
       IDMAT(I)=VOTT(1)                                                         
       IFLAG=VOTT(2)                                                            
c     IF((NS.LT.30.AND.IFLAG.NE.0) . OR .                                       
c    *   (NS.LT.18.AND.IFLAG.EQ.0))  GOTO 101                                   
c++++++++++++++   30agosto94  +++++++++++++++++++++++++++++++++++++++           
c     per pb termico non assegnato non leggo i coef. di dilat. termica          
c     NOTA BENE: i coeff. di dilat. termica devono essere inseriti nel          
c                file di input in una riga dove sia presente almeno un          
c                altro dato di input sui materiali !!                           
      if (((ns.ne.24.and.ns.ne.30).and.iflag.ne.0) .or.                         
     *   ((ns.ne.18.and.ns.ne.12).and.iflag.eq.0)) goto 101                     
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
      IF(NS.GT.30) THEN                                                         
       WRITE(IOUT,906) IDMAT(I)                                                 
 906   FORMAT(' ***   AVVISO (INMATE) : N.RO DI CAMPI INESATTO PER ',           
     *        'MATERIALE N.RO',I8,'   ***')                                     
       IER=1                                                                    
       GOTO 10                                                                  
      ELSE                                                                      
       IF(IFLAG.EQ.0) THEN                                                      
        IFLAG=-1                                                                
        EX=VOTT(3)                                                              
        EY=VOTT(4)                                                              
        EZ=VOTT(5)                                                              
        RNUXY=VOTT(6)                                                           
        RNUYZ=VOTT(7)                                                           
        RNUXZ=VOTT(8)                                                           
        GXY=VOTT(9)                                                             
        GYZ=VOTT(10)                                                            
        GZX=VOTT(11)                                                            
        DEN=VOTT(12)                                                            
C-------                                                                        
        VOTT(25)=VOTT(13)                                                       
        VOTT(26)=VOTT(14)                                                       
        VOTT(27)=VOTT(15)                                                       
        VOTT(28)=VOTT(16)                                                       
        VOTT(29)=VOTT(17)                                                       
        VOTT(30)=VOTT(18)                                                       
C-------                                                                        
        CALL DZERO(VOTT(3),22)                                                  
        VOTT(3)=1./EX                                                           
        VOTT(4)=-RNUXY/EX                                                       
        VOTT(5)=-RNUXZ/EX                                                       
        VOTT(9)=1./EY                                                           
        VOTT(10)=-RNUYZ/EY                                                      
        VOTT(14)=1./EZ                                                          
        VOTT(18)=1./GXY                                                         
        VOTT(21)=1./GYZ                                                         
        VOTT(23)=1./GZX                                                         
        VOTT(24)=DEN                                                            
       END IF                                                                   
       IND=2                                                                    
       DO 103 K=1,6                                                             
       DO 103 J=K,6                                                             
        IND=IND+1                                                               
        ELAS(K,J+IB)=VOTT(IND)                                                  
 103   CONTINUE                                                                 
       DENS(I)=VOTT(24)                                                         
C----- assegnazione e pivotaggio coefficienti dilatazione termici               
c+++++++++++++++  30agosto94  +++++++++++++++++++++++++++++++++++++++           
c     Se il pb termico non e' assegnato azzero il vettore dei coeef.            
c     di dilatatzione termica                                                   
c                                                                               
      if ((ns.eq.18).or.(ns.eq.30)) then                                        
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++          
       CDTER(1,I)=VOTT(IVP(1)+24)                                               
       CDTER(2,I)=VOTT(IVP(2)+24)                                               
       CDTER(3,I)=VOTT(IVP(3)+24)                                               
       CDTER(4,I)=VOTT(IVP(4)+24)                                               
       CDTER(5,I)=VOTT(IVP(5)+24)                                               
       CDTER(6,I)=VOTT(IVP(6)+24)                                               
c+++++++++++++++  30agosto94  ++++++++++++++++++++++++++++++++++++++++          
      else                                                                      
      call dzero(cdter(1,i),6)                                                  
      end if                                                                    
c++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++         
c      stampa di cdterm                                                         
c       write(iout,*) 'stampa di cdterm'                                        
c       ipippo=24                                                               
c       do 1,ii=1,6                                                             
c       write(iout,3) ii,cdter(ii,i),ipippo+ii,vott(ipippo+ii)                  
c3      format(/,'cdter(',i2,')=',f12.7,t50,'vott(',i2,')=',f12.7)              
c1      continue                                                                
c-------------------------------------                                          
C      assegnazione commentata: senza pivoting                                  
C      CDTER(1,I)=VOTT(25)                                                      
C      CDTER(2,I)=VOTT(26)                                                      
C      CDTER(3,I)=VOTT(27)                                                      
C      CDTER(4,I)=VOTT(28)                                                      
C      CDTER(5,I)=VOTT(29)                                                      
C      CDTER(6,I)=VOTT(30)                                                      
C------                                                                         
      END IF                                                                    
      DO 20 KR=2,6                                                              
      NSUP=KR-1                                                                 
      DO 20 KC=1,NSUP                                                           
      ELAS(KR,KC+IB)=ELAS(KC,KR+IB)                                             
 20   CONTINUE                                                                  
C       WRITE(IOUT,620)((ELAS(K,ib+J),J=K,6),K=1,6)                             
C  620 FORMAT('  MATRICE ELASTICA (TRIAN. SUP.) =',6E12.5/46X,5E12.5/58X        
C     1     ,4E12.5/,70X,3E12.5/82X,2E12.5/94X,E12.5)                           
      IF(IFLAG.GT.0)GOTO 55                                                     
      CALL INVERT(ELAS(1,IB+1),6,TMPMT,6,6,PERM)                                
      DO 30 J=1,6                                                               
      DO 30 K=1,6                                                               
      ELAS(J,K+IB)=TMPMT(J,K)                                                   
 30   CONTINUE                                                                  
C       WRITE(IOUT,620)((ELAS(K,ib+J),J=K,6),K=1,6)                             
 55   CONTINUE                                                                  
 10   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*107,*40)                                           
      WRITE(IOUT,900)MAXMAT                                                     
      CALL JHALT                                                                
      STOP                                                                      
 107  RETURN 1                                                                  
C 108  RETURN 2                                                                 
108   WRITE(IOUT,*) '****    NOTA: INMATE ESEGUE RETURN2 SU MAT.N.'
     *,VOTT(1)
      RETURN2                                                                   
 40   NMAT=I-1                                                                  
      WRITE(IOUT,*) 'NMAT=',NMAT                                                
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY INPROE(*,*,MAXPRO,IDPRO,ALFA,TETA,NPRO)                             
C                                                                               
      DO 50 I=1,MAXPRO                                                          
      CALL READFF(ISYM,0,NF,*109,*60)                                           
      CALL INTGFF(*110,IDPRO(I),0,LENFL)                                        
      CALL INTGFF(*110,IDPRO(I+MAXPRO),0,LENFL)                                 
      CALL DOUBFF(*110,ALFA(I),0,LENFL)                                         
      CALL INTGFF(*110,IDPRO(I+2*MAXPRO),0,LENFL)                               
      CALL DOUBFF(*110,TETA(I),0,LENFL)                                         
 50   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*109,*60)                                           
      WRITE(IOUT,901)MAXPRO                                                     
      CALL JHALT                                                                
      STOP                                                                      
 109  RETURN 1                                                                  
 110  WRITE(IOUT,*) '***   NOTA: INPROE: USCITA RETURN2'                        
      RETURN 2                                                                  
 60   NPRO=I-1                                                                  
      RETURN                                                                    
C                                                                               
C----------esegue la lettura e la stampa delle connessioni -------              
      ENTRY INCONE(*,*,IDMAT,ELAS,DENS,CDTER,NMAT,IDPRO,ALFA,TETA,NPRO,         
     1            ICON,VETT,LABEL,COOR,SCOMP,TRSMT,TMPMT,NODCNT,IER)            
C                                                                               
      IER=0                                                                     
      NEL=0                                                                     
      ITIP=5                                                                    
      IFLMR=1                                                                   
      NR6=6                                                                     
      NELP=0                                                                    
      NELPR=0                                                                   
      NWE=0                                                                     
C      NCON=12                                                                  
C      IF(MAXNOD.LT.NCON) MAXNOD=NCON                                           
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 70   CALL IZERO(ICON,NCON)                                                     
      CALL READFF(ISYM,0,NF,*111,*80)                                           
      CALL INTGFF(*112,IDEL,0,LENFL)                                            
      CALL INTGFF(*112,IPROP,0,LENFL)                                           
      CALL INTGFF(*112,INT1,0,LENFL)                                            
      CALL INTGFF(*112,INT2,0,LENFL)                                            
      NCON=NF-4                                                                 
      IF(MAXNOD.LT.NCON) MAXNOD=NCON                                            
      DO 77 L=5,NF                                                              
       CALL INTGFF(*112,ICON(L-4),0,LENFL)                                      
  77  CONTINUE                                                                  
      NEL=NEL+1                                                                 
      IF(NEL.LE.50)GOTO 90                                                      
      NEL=1                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
 90   CONTINUE                                                                  
      NELP=NELP+1                                                               
      IF(IFLMR.NE.0)NELPR=NELPR+1                                               
      WRITE(IOUT,720)NELP,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)
c -----------------------------------------------------------                   
c     inizializzazione di icont                                                 
      if(nstrat.gt.0) then
      do iok=1,nstrat                                                           
      icont=icont+1                                                             
      iux=10000000                                                              
      nt1=iux*(iok-1)                                                           
      nt2=iux*iok                                                               
      icont=icont+1                                                             
      if (ncon.eq.4) then                                                       
                                                                                
      WRITE(nasmod,921)IDEL+nt1,IPROP,(ICON(L)+nt1,L=1,4),                      
     + (ICON(L)+nt2,L=1,2),icont,icont,                                         
     + (ICON(L)+nt2,L=3,4)                                                      
                                                                                
921   format('CHEXA   ',8i8,'+CH',i5/'+CH',i5,2i8)                              
      elseif (ncon.eq.8) then                                                   
                                                                                
      WRITE(nasmod,721)IDEL+nt1,IPROP,(ICON(L)+nt1,L=1,4),                      
     + (ICON(L)+nt2,L=1,2),                                                     
     + icont                                                                    
      WRITE(nasmod,722)icont,(ICON(L)+nt2,L=3,4),(ICON(L)+nt1,L=5,8),           
     +  0,0,icont+1                                                             
      WRITE(nasmod,723)icont+1,0,0,(ICON(L)+nt2,L=5,8)                          
                                                                                
      icont=icont+1                                                             
721   format('CHEXA   ',8i8,'+CH',i5)                                           
722   format('+CH',i5,8i8,'+CH',i5)                                             
723   format('+CH',i5,6i8)                                                      
      endif                                                                     
      enddo               
      else
      endif                                                      
      if (ncon.eq.4) then                                                      
      WRITE(nasmod,821)IDEL,IPROP,(ICON(L),L=1,NCON)                           
821   format('CQUAD4  ',6i8)                                                   
      elseif (ncon.eq.8) then                                                  
      WRITE(nasmod,822)IDEL,IPROP,(ICON(L),L=1,6),idel                         
      WRITE(nasmod,823)IDEL,(ICON(L),L=7,8)                                    
822   format('CQUAD8  ',8i8,'+Q',i6)                                           
823   format('+Q',i6,2i8)                                                      
      endif                                                                    
c -----------------------------------------------------------                   
                                                                                
      do i=1,ncon
      idnt(i)=icon(i)
      enddo               
      IER=0                                                                     
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      dx1=vett(3)-vett(1)
      dy1=vett(4)-vett(2)
      dx2=vett(7)-vett(1)
      dy2=vett(8)-vett(2)
      prv=dx1*dy2-dx2*dy1 
c      print*,idel,dx1,dy1,dx2,dy2
c      print*,prv
      if(prv.lt.0) then
      do i=1,ncon
      icon(i)=idnt(i)
      enddo
      call iswap(icon(1),icon(2))
      call iswap(icon(3),icon(4))
      if(ncon.eq.8) then
        call iswap(icon(6),icon(8))
      endif
      if(ncon.eq.12) then
        call iswap(icon(5),icon(6))
        call iswap(icon(9),icon(10))
        call iswap(icon(11),icon(8))
        call iswap(icon(7),icon(12))
      endif                                         

      write(iout,*) 'Il verso di numerazione dell''elemento e'' stato',
     * ' invertito'
      WRITE(IOUT,720)NELP,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)               
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      endif      
      IF(IER.EQ.0)GOTO 86                                                       
      IERG=1                                                                    
      WRITE(IOUT,905)IDEL                                                       
      GOTO 70                                                                   
 86   IPP=IPOSL(IPROP,IDPRO,NPRO)                                               
      IF(IPP.EQ.0)GOTO 800                                                      
      IDM=IDPRO(IPP+NPRO)                                                       
c      write(iout,*) 'idm=',idm                                                 
      IF(IDM.LT.0)GOTO 1000                                                     
      IPM=IPOSL(IDM,IDMAT,NMAT)                                                 
c      write(iout,*) 'idm=',ipm                                                 
      IF(IPM.EQ.0) GOTO 801                                                     
      IFLMR=IDMAT(IPM)                                                          
      DEN=DENS(IPM)                                                             
c      write(iout,*) 'dens=',den                                                
 85   CONTINUE                                                                  
      ANGOLO=TETA(IPP)                                                          
      IFLANG=IDPRO(IPP+2*NPRO)                                                  
      CALL DMOVE(ELAS(1,(IPM-1)*6+1),TMPMT,36)                                  
      DO 777 II=1,6                                                             
      DO 777 IL=1,6                                                             
      TRSMT(II,IL)=TMPMT(II,IVP(IL))                                            
 777  CONTINUE                                                                  
      DO 778 II=1,6                                                             
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
C Inserita la trasformazione del vettore CDLTZ per tener conto                  
C dell' anisotropia (precedentemente era stata erroneamente inserita            
C in A7ASS1)                                                                    
C++++++++++++++++++++++++++++++++++++++                                         
      cdtert(ii)=cdter(ii,ipm)                                                  
C++++++++++++++++++++++++++++++++++++                                           
      DO 778 IL=1,6                                                             
      TMPMT(IL,II)=TRSMT(IVP(IL),II)                                            
 778  CONTINUE                                                                  
C      WRITE(IOUT,620)((TMPMT(K,J),J=K,6),K=1,6)                                
      ALFORT=ALFA(IPP)                                                          
      IF(ALFA(IPP).EQ.0.D0)GOTO 75                                              
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++                
c++++++++++++  26 luglio   ++++++++++++++++++++++++++++++++++                   
      CALL TRASFM(TMPMT,TRSMT,SCOMP,NR6,ALFA(IPP))                              
      call dpromm(trsmt,6,6,6,cdter(1,ipm),6,cdtert,6,1,0,0)                    
C     CALL TRASF5(TMPMT,NR6,ALFA(IPP))                                          
c**************************************************************                 
c+++++++    19 settembre   ++++++++++++++++                                     
c  commento le stampe seguenti                                                  
c      write(iout,'(a,6e12.5)') 'cdter0=',(cdter(io,ipm),io=1,6)                
c      write(iout,'(6e12.5)') ((trsmt(io,iw),iw=1,6),io=1,6)                    
c      write(iout,'(a,6e12.5)') 'cdterr=',cdtert                                
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++             
 75   CONTINUE                                                                  
C      WRITE(IOUT,620)((TMPMT(K,J),J=K,6),K=1,6)                                
C 620 FORMAT('  MATRICE ELASTICA (TRIAN. SUP.) =',6E12.5/46X,5E12.5/58X         
C    1     ,4E12.5/,70X,3E12.5/82X,2E12.5/94X,E12.5)                            
C                                                                               
      iflmr=iprop                                                               
      CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,NCON)                                             
      CALL JWRITE(ISLELM,VETT,NCON*4)                                           
      CALL JWRITE(ISLELM,9,1)                                                   
      CALL JWRITE(ISLELM,INT1,9)                                                
      CALL JWRITE(ISLELM,TMPMT,72)                                              
C----                                                                           
      CALL JWRITE(ISLELM,CDTERT,12)                                             
C----                                                                           
      NWE=NWE+83+5*NCON  +  12                                                  
C                                                                               
      GOTO 70                                                                   
 80   RETURN                                                                    
 111  RETURN 1                                                                  
 112  RETURN 2                                                                  
 800  WRITE(IOUT,902)IPROP,IDEL                                                 
      IER=1                                                                     
      GOTO 70                                                                   
 801  WRITE(IOUT,903)IDEL,IDM,IPROP                                             
      IER=1                                                                     
      GOTO 70                                                                   
 1000 CONTINUE                                                                  
      CALL READFL(IDM,IER,ITIP,TMPMT,IFLAG,DEN)                                 
      IF(IFLAG.GT.0)GOTO 85                                                     
      CALL INVERT(TMPMT,6,SCOMP,6,6,PERM)                                       
      DO 1010 J=1,6                                                             
      DO 1010 K=1,6                                                             
      TMPMT(J,K)=SCOMP(J,K)                                                     
 1010 CONTINUE                                                                  
      GOTO 85                                                                   
C                                                                               
C--------------- stampa anche coeff dilatz termica ------                       
      ENTRY WRMATE(IDMAT,ELAS,DENS,CDTER,NMAT)                                  
C                                                                               
      NR=0                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      WRITE(IOUT,3001)                                                          
      DO 2000 IMAT=1,NMAT                                                       
      NR=NR+6                                                                   
      IF(NR.LE.54)GOTO 2010                                                     
      NR=6                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3001)                                                          
 2010 CONTINUE                                                                  
      IB=(IMAT-1)*6                                                             
C      WRITE(IOUT,3010)IMAT,IDMAT(IMAT),(ELAS(1,K+IB),K=1,6),DENS(IMAT),        
C     1               ((ELAS(L,K+IB),K=1,6),L=2,6)                              
      WRITE(IOUT,3010) IMAT,IDMAT(IMAT),DENS(IMAT),(ELAS(1,K+IB),K=1,6),        
     +     CDTER(1,IMAT),((ELAS(L,K+IB),K=1,6),L,CDTER(L,IMAT),L=2,6)           
 2000 CONTINUE                                                                  
      RETURN                                                                    
C---------------- fine ---                                                      
C                                                                               
      ENTRY WRPROE(IDPRO,ALFA,TETA,NPRO,idmat,elas,nmat)                        
C                                                                               
      NR=0                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
      WRITE(IOUT,3101)                                                          
      DO 2100 IPRO=1,NPRO                                                       
      NR=NR+1                                                                   
      IF(NR.LE.50)GOTO 2110                                                     
      NR=1                                                                      
      WRITE(IOUT,3101)                                                          
 2110 CONTINUE                                                                  
      INDICE=IPRO+NPRO+NPRO                                                     
      WRITE(IOUT,3110)IPRO,IDPRO(IPRO),IDPRO(IPRO+NPRO),ALFA(IPRO),             
     1                IDPRO(INDICE),TETA(IPRO)                                  
                                                                                
                                                                                
      do 1986 ii=1,nmat                                                         
c      print*,ii,idpro(ii+npro),idmat(ii)                                       
      if( idpro(ipro+npro).eq.idmat(ii) ) then                                  
        ip=ii                                                                   
        goto 2390                                                               
      endif                                                                     
1986  continue                                                                  
2390  continue                                                                  
      IB=(Ip-1)*6                                                               
c      print*,ipro,idpro(ipro),idpro(ip+npro),ip,ib                             
                                                                                
c      do 8747 i=1,6                                                            
c 8747 write(iout,'(12x,6e12.5)')(elas(i,ll+ib),ll=1,6)                         
      DO 3777 II=1,6                                                            
      DO 3777 IL=1,6                                                            
      T(II,IL)=elas(ii,ivp(il)+ib)                                              
 3777  CONTINUE                                                                 
      DO 3778 II=1,6                                                            
      DO 3778 IL=1,6                                                            
      d(IL,II)=T(IVP(IL),II)                                                    
 3778  CONTINUE                                                                 
                                                                                
                                                                                
c      do 8763 i=1,6                                                            
c 8763 write(iout,'(12x,6e12.5)')(d(i,ll),ll=1,6)                               
C                                                                               
      CALL TRASFM(d,t,tt,6,ALFA(Ipro))                                          
                                                                                
      do 8743 i=1,6                                                             
 8743 write(iout,'(12x,6e12.5)')(d(i,ll),ll=1,6)                                
                                                                                
                                                                                
 2100 CONTINUE                                                                  
      RETURN                                                                    
 900  FORMAT(' ***   AVVISO (INMATE) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI MATERIALI E'' =',I8,'   ***'/)                                    
 901  FORMAT(' ***   AVVISO (INPROE) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI PROPRIETA'' E'' =',I8,'   ***'/)                                  
 902  FORMAT(' ***   AVVISO (INCONE) : NON RECUPERATA LA PROPRIETA'''           
     1   ,' N.RO',I8,' DELL''ELEMENTO N.RO',I8,'   ***'/)                       
 903  FORMAT(' ***   AVVISO (INCONE) : PER L''ELEMENTO N.RO',I8,                
     1   '  NON RECUPERATO IL MATERIALE N.RO',I8,'  INDICATO ',                 
     2   'DALLA PROPRIETA'' N.RO',I8,'   ***'/)                                 
 905  FORMAT(' ***   AVVISO (INCONE) : NON RECUPERATI NODI'                     
     1 ,' DELL''ELEMENTO N.RO',I8,'   ***')                                     
C                                                                               
 700  FORMAT(/40X,'***   ELEMENTI BIDIMENSIONALI PIANI   ***'/)                 
 710  FORMAT(//12X,'   N.RO  ID.ELEM  ID.PROP   N.PUNTI INTEG. ',               
     1  4X,'CONNESSIONI'/)                                                      
 720  FORMAT(10X,3I8,6X,2I4,8X,12I8)                                            
 3000 FORMAT(/40X,'***   MATERIALI ELEMENTI BIDIMENSIONALI ',                   
     1    'PIANI   ***'/)                                                       
C 3001 FORMAT(//9X,'   N.RO  ID.MAT  ',T30,'MATRICE ELASTICA',T114,             
C     1    'DENSITA'''//)                                                       
C 3010 FORMAT(6X,2I8,T30,6E12.5,T110,E12.5,5(/T30,6E12.5)/)                     
C----------------                                                               
3001  FORMAT(//3X,'  N.RO  ID.MAT    DENSITA  ',T37,'MATRICE ELASTICA',         
     +      T110,'COEFF DILATAZIONE'//)                                         
3010  FORMAT(1X,2I8,T20,E11.5,T34,6(E12.5),T110,'CF1=',E12.5,/,                 
     +      5(T34,6(E12.5),T110,'CF',I1,'=',E12.5,/))                           
C----------------                                                               
 3100 FORMAT(/40X,'***   PROPRIETA'' ELEMENTI BIDIMENSIONALI ',                 
     1    'PIANI   ***'/)                                                       
 3101 FORMAT(//19X,'   N.RO    ID.PROP    ID.MAT    ORIENT. FIBRE ',            
     1      '  FLAG PIANO ORTOT.   ORIENT. PIANO ORTOT.'/)                      
 3110 FORMAT(15X,3I10,4X,E12.5,5X,I8,10X,E12.5)                                 
      END                                                                       
      subroutine iswap(i1,i2)
      integer i1,i2,it
      it=i1
      i1=i2
      i2=it
      return
      end

C-------------------------------------------------------fine------------        
C@ELT,I ANBA*ANBA.INPGIU                                                        
C*************************          INPGIU          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPGIU                                                         
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE E' FITTIZIA E CONTIENE GLI ENTRY RELATIVI               
C     AGLI ELEMENTI DI  "GIUNZIONE"                                             
C                                                                               
C     INMATG - LETTURA  MATERIALI                                               
C     INPROG - LETTURA  PROPRIETA'                                              
C     INCONG - LETTURA  CONNESSIONI                                             
C     WRMATG - STAMPA MATERIALI                                                 
C     WRPROG - STAMPA PROPRIETA'                                                
C                                                                               
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      DIMENSION IDMAT(*),CAR(2,*),IDPRO(*),SPLU(2,*),LABEL(*),COOR(2,*)         
     1          ,VETT(*),ICON(*),NODCNT(*)                                      
      logical dump,incore                                                       
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JFTNIO/ INPT,IOUT,ISYM,dump(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /GIUNZ/  G,RO,RL,T                                                 
      common /outnas/ nasmod,naspch                                             
      COMMON /TRAVE3/ RLL,RVe(6),neltra,concio,nstrat                           
C                                                                               
C                                                                               
C                                                                               
      ENTRY INMATG(*,*,MAXMAT,IDMAT,CAR,NMAT)                                   
C                                                                               
C                                                                               
      DO 10 I=1,MAXMAT                                                          
      CALL READFF(ISYM,0,NF,*100,*20)                                           
      CALL INTGFF(*101,IDMAT(I),0,LENFL)                                        
      CALL DOUBFF(*101,CAR(1,I),0,LENFL)                                        
      CALL DOUBFF(*101,CAR(2,I),0,LENFL)                                        
 10   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*100,*20)                                           
      WRITE(IOUT,900)MAXMAT                                                     
      CALL JHALT                                                                
      STOP                                                                      
 100  RETURN 1                                                                  
 101  RETURN 2                                                                  
 20   NMAT=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY INPROG(*,*,MAXPRO,IDPRO,SPLU,NPRO)                                  
C                                                                               
C                                                                               
      DO 30 I=1,MAXPRO                                                          
      CALL READFF(ISYM,0,NF,*102,*40)                                           
      CALL INTGFF(*103,IDPRO(I),0,LENFL)                                        
      CALL INTGFF(*103,IDPRO(I+MAXPRO),0,LENFL)                                 
      CALL DOUBFF(*103,SPLU(1,I),0,LENFL)                                       
      CALL DOUBFF(*103,SPLU(2,I),0,LENFL)                                       
 30   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*102,*40)                                           
      WRITE(IOUT,901)MAXPRO                                                     
      CALL JHALT                                                                
      STOP                                                                      
 102  RETURN 1                                                                  
 103  RETURN 2                                                                  
 40   NPRO=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
C                                                                               
      ENTRY INCONG(*,*,IDMAT,CAR,NMAT,IDPRO,SPLU,NPRO,LABEL,COOR,VETT,          
     1             ICON,NODCNT,IER)                                             
C                                                                               
C                                                                               
      ITIP=2                                                                    
      IFLMR=1                                                                   
      NCON=2                                                                    
      MAXNOD=2                                                                  
      NEL=0                                                                     
      NGIU=0                                                                    
      NGIUR=0                                                                   
      NWG=0                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 60   CALL READFF(ISYM,0,NF,*104,*50)                                           
      CALL INTGFF(*105,IDEL,0,LENFL)                                            
      CALL INTGFF(*105,IPROP,0,LENFL)                                           
      CALL INTGFF(*105,ICON(1),0,LENFL)                                         
      CALL INTGFF(*105,ICON(2),0,LENFL)                                         
      NGIU=NGIU+1                                                               
      IF(IFLMR.NE.0)NGIUR=NGIUR+1                                               
      NEL=NEL+1                                                                 
      IF(NEL.LE.50)GOTO 61                                                      
      NEL=0                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
 61   IPP=IPOSL(IPROP,IDPRO,NPRO)                                               
      IF(IPP.EQ.0) GOTO 800                                                     
      IM=IDPRO(IPP+NPRO)                                                        
      IF(IM.LT.0)GOTO 1000                                                      
      IPM=IPOSL(IM,IDMAT,NMAT)                                                  
      IF(IPM.EQ.0) GOTO 801                                                     
      IFLMR=IDMAT(IPM)                                                          
      G=CAR(1,IPM)                                                              
      RO=CAR(2,IPM)                                                             
 70   RL=SPLU(1,IPP)                                                            
      T =SPLU(2,IPP)                                                            
C     WRITE(IOUT,610)NGIU,IDEL,ICON(1),ICON(2),IPROP,G,RL,T,RO                  
      WRITE(IOUT,610)NGIU,IDEL,IPROP,ICON(1),ICON(2)                            
                                                                                
c -----------------------------------------------------------                   
      if(nstrat.gt.0) then
      do iok=1,nstrat                                                           
      icont=icont+1                                                             
      iux=10000000                                                              
      nt1=iux*(iok-1)                                                           
      nt2=iux*iok                                                               
      WRITE(nasmod,721)IDEL+nt1,IPROP,ICON(1)+nt1,icon(2)+nt1,                  
     + icon(2)+nt2,icon(1)+nt2                                                  
      enddo                                                                     
721   format('CSHEAR  ',6i8)
      else                                                    
                                                                                
      WRITE(nasmod,821)IDEL,IPROP,(ICON(L),L=1,NCON)                           
821   format('CROD    ',6i8)
      endif                                                   
c -----------------------------------------------------------                   
                                                                                
      IER=0                                                                     
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      IF(IER.EQ.0)GOTO 86                                                       
      IERG=1                                                                    
      WRITE(IOUT,905)IDEL                                                       
      GOTO 60                                                                   
C610  FORMAT(10X,I8,1X,I8,4X,2I8,4X,I8,10X,4E13.6)                              
 610  FORMAT(28X,I8,2X,I8,2X,I8,9X,2I8)                                         
 86   CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,NCON)                                             
      CALL JWRITE(ISLELM,VETT,4*NCON)                                           
      CALL JWRITE(ISLELM,G,8)                                                   
      NWG=NWG+12+5*NCON                                                         
      GOTO 60                                                                   
 50   RETURN                                                                    
 104  RETURN 1                                                                  
 105  RETURN 2                                                                  
 1000 CONTINUE                                                                  
      CALL READFL(IPM,IER,ITIP,G,IFL,RO)                                        
      IF(IER.EQ.0)GOTO 70                                                       
      WRITE(IOUT,904)IPM                                                        
 66   IER=1                                                                     
      GOTO 60                                                                   
 800  WRITE(IOUT,902)IPROP                                                      
      GOTO 66                                                                   
 801  WRITE(IOUT,903)IM                                                         
      GOTO 66                                                                   
 700  FORMAT(//40X,'***   ELEMENTI DI GIUNZIONE   ***')                         
C710  FORMAT(//10X,'   N.RO ',2X,'ID.ELEM',10X,'NODI',10X,'PROPRIETA'''         
C    1  ,8X,'MODULO',' ELAST.',5X,'LUNGHEZZA',5X,'SPESSORE',5X                  
C    2  ,'DENSITA'''//)                                                         
 710  FORMAT(//33X,'N.RO',5X,'ID.ELEM',2X,'ID.PROP.',10X,'CONNESSIONI'/)        
 900  FORMAT(' ***   AVVISO (INMATG) : OVERFLOW, N. MASSIMO DI'                 
     1,' MATERIALI AMMESSO E''',I8,'   ***')                                    
 901  FORMAT(' ***   AVVISO (INPROG) : OVERFLOW, N. MASSIMO DI'                 
     1,' PROPRIETA'' AMMESSO E''',I8,'   ***')                                  
 902  FORMAT(' ***   AVVISO (INCONG) : PROPRIETA'' NON DICHIARATA'              
     1 ,I8,'   ***')                                                            
 903  FORMAT(' ***   AVVISO (INCONG) : MATERIALE NON DICHIARATO'                
     1 ,I8,'   ***')                                                            
 904  FORMAT(' ***   AVVISO (INCONG) : MATERIALE NON PRESENTE IN'               
     1 ,' BIBLIOTECA',I8,'   ***')                                              
 905  FORMAT(' ***   AVVISO (INCONG) : NON RECUPERATI NODI DELL''',             
     1  ' ELEMENTO N.RO',I8,'   ***')                                           
C                                                                               
C                                                                               
C                                                                               
      ENTRY WRMATG(IDMAT,CAR,NMAT)                                              
C                                                                               
C                                                                               
      IMAT=0                                                                    
      NMPAG=50                                                                  
      NPAG=(NMAT+49)/NMPAG                                                      
      DO 2000 I=1,NPAG                                                          
      IF(I.EQ.NPAG)NMPAG=NMAT-(NPAG-1)*NMPAG                                    
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      DO 2100 L=1,NMPAG                                                         
      IMAT=IMAT+1                                                               
      WRITE(IOUT,3100)IMAT,IDMAT(IMAT),(CAR(K,IMAT),K=1,2)                      
 2100 CONTINUE                                                                  
 2000 CONTINUE                                                                  
 3000 FORMAT(/40X,'***   MATERIALI ELEMENTI DI GIUNZIONE   ***'//               
     1 30X,'   N.RO   ID.MAT.',10X,'MOD. ELAST. TANG.      DENSITA'''/)         
 3100 FORMAT(27X,2I8,11X,E16.6,2X,E16.6)                                        
      RETURN                                                                    
C                                                                               
C                                                                               
C                                                                               
      ENTRY WRPROG(IDPRO,SPLU,NPRO)                                             
C                                                                               
C                                                                               
      IPRO=0                                                                    
      NPPAG=50                                                                  
      NPAG=(NPRO+49)/NPPAG                                                      
      DO 2200 I=1,NPAG                                                          
      IF(I.EQ.NPAG)NPPAG=NPRO-(NPAG-1)*NPPAG                                    
      CALL INTEST                                                               
      WRITE(IOUT,3200)                                                          
      DO 2300 L=1,NPPAG                                                         
      IPRO=IPRO+1                                                               
      WRITE(IOUT,3300)IPRO,IDPRO(IPRO),IDPRO(IPRO+NPRO),SPLU(1,IPRO),           
     1               SPLU(2,IPRO)                                               
 2300 CONTINUE                                                                  
 2200 CONTINUE                                                                  
 3200 FORMAT(/40X,'***   PROPRIETA'' ELEMENTI DI GIUNZIONE   ***'//             
     133X,'N.RO  ID.PROP.',12X,'ID.MAT.',12X,'SPESSORE      LUNGHEZZA'/)        
 3300 FORMAT(28X,2I8,12X,I8,8X,2E16.6)                                          
      RETURN                                                                    
      END                                                                       
C@ELT,I ANBA*ANBA.INPLAM                                                        
C**********************         INPLAM           **********************         
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE INPLAM                                                         
C                                                                               
C                                                                               
C     QUESTA SUBROUTINE E' FITTIZIA E CONTIENE GLI ENTRY RELATIVI               
C     AGLI ELEMENTI BIDIMENSIONALI DI LAMINA                                    
C                                                                               
C     INMATL - LETTURA  MATERIALI                                               
C     INPROL - LETTURA  PROPRIETA'                                              
C     INCONL - LETTURA  CONNESSIONI                                             
C     WRMATL - STAMPA MATERIALI                                                 
C     WRPROL - STAMPA PROPRIETA'                                                
C                                                                               
C                                                                               
      IMPLICIT REAL*8  (A-H,O-Z)                                                
      LOGICAL DUMP,incore                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /PARAM/  NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,          
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELPI2/  INT1,INT2,TETA,DEN,alfort,IFLTET                          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC,          
     2                NWC,NWG,NWP,NWL,NWE,NWV                                   
      common /outnas/ nasmod,naspch                                             
      COMMON /TRAVE3/ RL,RVe(6),neltra,concio,nstrat	                           
      DIMENSION IDMAT(*),ELAS(6,*),DENS(*),PERM(*),TMPMT(6,*),IDPRO(*),         
     1          SPES(4,*),ALFA(*),ICON(*),VETT(*),LABEL(*),COOR(2,*),           
     2          SCOMP(6,*),TRSMT(6,*),IVP(6),VOTT(24),NODCNT(*),idnt(8)                 
C                                                                               
c     DATA IVP/6,5,3,1,2,4/                                                     
      DATA IVP/4,5,1,2,3,6/                                                     
C                                                                               
      ENTRY INMATL(*,*,MAXMAT,IDMAT,ELAS,DENS,NMAT,PERM,TMPMT,IER)              
C                                                                               
      IER=0                                                                     
      DO 10 I=1,MAXMAT                                                          
      IB=(I-1)*6                                                                
      NS=0                                                                      
 101  CALL READFF(ISYM,0,NF,*107,*40)                                           
      NI=NS+1                                                                   
      NS=NI+NF-1                                                                
      DO 100 K=NI,NS                                                            
      CALL DOUBFF(*108,VOTT(K),0,LENFL)                                         
 100  CONTINUE                                                                  
       IDMAT(I)=VOTT(1)                                                         
       IFLAG=VOTT(2)                                                            
      IF((NS.LT.24.AND.IFLAG.NE.0) . OR .                                       
     *   (NS.LT.12.AND.IFLAG.EQ.0))  GOTO 101                                   
      IF(NS.GT.24) THEN                                                         
       WRITE(IOUT,906) IDMAT(I)                                                 
 906   FORMAT(' ***   AVVISO (INMATL) : N.RO DI CAMPI INESATTO PER ',           
     *        'MATERIALE N.RO',I8,'   ***')                                     
       IER=1                                                                    
       GOTO 10                                                                  
      ELSE                                                                      
       IF(IFLAG.EQ.0) THEN                                                      
        IFLAG=-1                                                                
        EX=VOTT(3)                                                              
        EY=VOTT(4)                                                              
        EZ=VOTT(5)                                                              
        RNUXY=VOTT(6)                                                           
        RNUYZ=VOTT(7)                                                           
        RNUXZ=VOTT(8)                                                           
        GXY=VOTT(9)                                                             
        GYZ=VOTT(10)                                                            
        GZX=VOTT(11)                                                            
        DEN=VOTT(12)                                                            
        CALL DZERO(VOTT(3),22)                                                  
        VOTT(3)=1./EX                                                           
        VOTT(4)=-RNUXY/EX                                                       
        VOTT(5)=-RNUXZ/EX                                                       
        VOTT(9)=1./EY                                                           
        VOTT(10)=-RNUYZ/EY                                                      
        VOTT(14)=1./EZ                                                          
        VOTT(18)=1./GXY                                                         
        VOTT(21)=1./GYZ                                                         
        VOTT(23)=1./GZX                                                         
        VOTT(24)=DEN                                                            
       END IF                                                                   
       IND=2                                                                    
       DO 103 K=1,6                                                             
       DO 103 J=K,6                                                             
        IND=IND+1                                                               
        ELAS(K,J+IB)=VOTT(IND)                                                  
 103   CONTINUE                                                                 
       DENS(I)=VOTT(24)                                                         
      END IF                                                                    
      DO 20 KR=2,6                                                              
      KRM1=KR-1                                                                 
      DO 20 KC=1,KRM1                                                           
      ELAS(KR,KC+IB)=ELAS(KC,KR+IB)                                             
 20   CONTINUE                                                                  
      IF(IFLAG.GT.0)GOTO 55                                                     
      CALL INVERT(ELAS(1,IB+1),6,TMPMT,6,6,PERM)                                
      DO 30 J=1,6                                                               
      DO 30 K=1,6                                                               
      ELAS(J,K+IB)=TMPMT(J,K)                                                   
 30   CONTINUE                                                                  
 55   CONTINUE                                                                  
 10   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*107,*40)                                           
      WRITE(IOUT,900)MAXMAT                                                     
      CALL JHALT                                                                
      STOP                                                                      
 107  RETURN 1                                                                  
 108  RETURN 2                                                                  
 40   NMAT=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY INPROL(*,*,MAXPRO,IDPRO,SPES,ALFA,NPRO)                             
C                                                                               
      DO 50 I=1,MAXPRO                                                          
      CALL READFF(ISYM,0,NF,*109,*60)                                           
      CALL INTGFF(*110,IDPRO(I),0,LENFL)                                        
      CALL INTGFF(*110,IDPRO(I+MAXPRO),0,LENFL)                                 
      CALL DOUBFF(*110,ALFA(I),0,LENFL)                                         
      DO 57 K=1,NF-3                                                            
      CALL DOUBFF(*110,SPES(K,I),0,LENFL)                                       
 57   CONTINUE                                                                  
 50   CONTINUE                                                                  
      CALL READFF(ISYM,0,NF,*109,*60)                                           
      WRITE(IOUT,901)MAXPRO                                                     
      CALL JHALT                                                                
      STOP                                                                      
 109  RETURN 1                                                                  
 110  RETURN 2                                                                  
 60   NPRO=I-1                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY INCONL(*,*,IDMAT,ELAS,DENS,NMAT,IDPRO,SPES,ALFA,NPRO,ICON,          
     1             VETT,LABEL,COOR,SCOMP,TRSMT,TMPMT,NODCNT,IER)                
C                                                                               
      IER=0                                                                     
      NEL=0                                                                     
      ITIP=4                                                                    
      IFLMR=1                                                                   
      NR6=6                                                                     
      NLAM=0                                                                    
      NLAMR=0                                                                   
      NWL=0                                                                     
      MAXNOD=8                                                                  
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      WRITE(IOUT,710)                                                           
 70   CALL IZERO(ICON,8)                                                        
      CALL READFF(ISYM,0,NF,*111,*80)                                           
      CALL INTGFF(*112,IDEL,0,LENFL)                                            
      CALL INTGFF(*112,IPROP,0,LENFL)                                           
      CALL INTGFF(*112,INT1,0,LENFL)                                            
      CALL INTGFF(*112,INT2,0,LENFL)                                            
      DO 77 L=5,NF                                                              
       CALL INTGFF(*112,ICON(L-4),0,LENFL)                                      
  77  CONTINUE                                                                  
      NEL=NEL+1                                                                 
      IF(NEL.LE.50)GOTO 90                                                      
      NEL=1                                                                     
      CALL INTEST                                                               
      WRITE(IOUT,710)                                                           
 90   NCON=NF-4                                                                 
      IF(NCON.NE.4.AND.NCON.NE.6.AND.NCON.NE.8)THEN                             
      WRITE(IOUT,916) IDEL,(ICON(IO),IO=1,NCON)                                 
      IERG=1                                                                    
      END IF                                                                    
      NCOPP=NCON/2                                                              
      NLAM=NLAM+1                                                               
      IF(IFLMR.NE.0)NLAMR=NLAMR+1                                               
      WRITE(IOUT,720)NLAM,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)               

                                                                                
c -----------------------------------------------------------                   
c     inizializzazione di icont
      if(nstrat.gt.0) then                                                 
      do iok=1,nstrat                                                           
      icont=icont+1                                                             
      iux=10000000                                                              
      nt1=iux*(iok-1)                                                           
      nt2=iux*iok                                                               
      if (ncon.eq.4) then                                                       
      WRITE(nasmod,921)IDEL+nt1,IPROP,(ICON(L)+nt1,L=1,NCON),                   
     + (ICON(L)+nt2,L=1,2),icont,icont,                                         
     + (ICON(L)+nt2,L=3,NCON)                                                   
921   format('CHEXA   ',6i8,'+C',i6/'+CH',i5,2i8)                               
                                                                                
      elseif (ncon.eq.6) then                                                   
      WRITE(nasmod,721)IDEL+nt1,IPROP,(ICON(L)+nt1,L=1,4),                      
     + (ICON(L)+nt2,L=1,2),icont                                                
      WRITE(nasmod,722)icont,(ICON(3)+nt2,L=3,4),ICON(5)+nt1,0,                 
     + ICON(6)+nt1,0,0,0,icont+1                                                
      WRITE(nasmod,723)icont+1,0,0,ICON(5)+nt2,0,ICON(6)+nt2,0                  
      icont=icont+2                                                             
                                                                                
      elseif (ncon.eq.8) then                                                   
c      do k=1,3                                                                 
c      WRITE(nasmod,721)IDEL,IPROP,(ICON(L),L=1,NCON),                          
c     + (ICON(L)+nnodi,L=1,2),icont,icont,                                      
c     + (ICON(L)+nnodi,L=3,NCON)                                                
c      enddo                                                                    
      endif                                                                     
      enddo                                                                     
721   format('CHEXA   ',6i8,'+CH',i5)                                           
722   format('+CH',i5,6i8,'+CH',i5)                                             
723   format('+CH',i5,6i8)                                                      
      else                                                                          
      if (ncon.eq.4) then                                                      
      WRITE(nasmod,821)IDEL,IPROP,(ICON(L),L=1,NCON)                           
821   format('CQUAD4  ',6i8)                                                   
      elseif (ncon.eq.6) then                                                  
      WRITE(nasmod,822)IDEL,IPROP,(ICON(L),L=1,5),0,idel                       
      WRITE(nasmod,823)IDEL,icon(6),0                                          
      elseif (ncon.eq.8) then                                                  
      WRITE(nasmod,822)IDEL,IPROP,ICON(1),icon(6),icon(3),icon(8),            
     *icon(5),icon(2),idel                                                    
      WRITE(nasmod,823)IDEL,ICON(7),icon(4)                                   
822   format('CQUAD8  ',8i8,'+Q',i6)                                           
823   format('+Q',i6,2i8)                                                      
      endif                                                                    
      endif                                                                          
c -----------------------------------------------------------                   
                                                                                
c      if (ncon.eq.4) then                                                      
c      WRITE(nasmod,721)IDEL,IPROP,(ICON(L),L=1,NCON)                           
c721   format('CQUAD4  ',6i8)                                                   
c      elseif (ncon.eq.6) then                                                  
c      WRITE(nasmod,722)IDEL,IPROP,(ICON(L),L=1,5),0,idel                       
c      WRITE(nasmod,723)IDEL,icon(6),0                                          
c      elseif (ncon.eq.8) then                                                  
c      WRITE(nasmod,722)IDEL,IPROP,ICON(1),icon(6),icon(3),icon(8),             
c     *icon(5),icon(2),idel                                                     
c      WRITE(nasmod,723)IDEL,ICON(7),icon(4)                                    
c722   format('CQUAD8  ',8i8,'+Q',i6)                                           
c723   format('+Q',i6,2i8)                                                      
c      endif                                                                    
                                                                                
C     WRITE(IOUT,720)NLAM,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)               

      do i=1,ncon
      idnt(i)=icon(i)
      enddo                   
      IER=0                                                                     
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      dx1=vett(3)-vett(1)
      dy1=vett(4)-vett(2)
      dx2=vett(7)-vett(1)
      dy2=vett(8)-vett(2)
      prv=dx1*dy2-dx2*dy1 
c      print*,idel,dx1,dy1,dx2,dy2
c      print*,prv
      if(prv.lt.0) then

      do i=1,ncon
      icon(i)=idnt(i)
      enddo

      call iswap(icon(1),icon(2))
      call iswap(icon(3),icon(4))
      if(ncon.eq.8) then
        call iswap(icon(5),icon(6))
        call iswap(icon(7),icon(8))
      endif

      write(iout,*) 'Il verso di numerazione dell''elemento e'' stato',
     * ' invertito'
      WRITE(IOUT,720)NELP,IDEL,IPROP,INT1,INT2,(ICON(L),L=1,NCON)               
      CALL RCPCON(ICON,NCON,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      endif      
      IF(IER.EQ.0)GOTO 86                                                       
      IERG=1                                                                    
      WRITE(IOUT,905)IDEL                                                       
      GOTO 70                                                                   
 86   IF(MAXNOD.LT.NCON)MAXNOD=NCON                                             
      IPP=IPOSL(IPROP,IDPRO,NPRO)                                               
      IF(IPP.EQ.0)GOTO 800                                                      
      IDM=IDPRO(IPP+NPRO)                                                       
      IF(IDM.LT.0)GOTO 1000                                                     
      IPM=IPOSL(IDM,IDMAT,NMAT)                                                 
      IF(IPM.EQ.0) GOTO 801                                                     
      IFLMR=idmat(ipm)                                                          
      DEN=DENS(IPM)                                                             
      CALL DMOVE(ELAS(1,(IPM-1)*6+1),TMPMT,36)                                  
 85   CONTINUE                                                                  
C     WRITE(IOUT,9910)ALFA(IPP),DEN,IDM,IPM,IPROP,IPP                           
C9910 FORMAT(' ALFA,DEN,IDM,IPM,IPROP,IPP:',2E12.5,4I8)                         
      DO 777 II=1,6                                                             
      DO 777 IL=1,6                                                             
      TRSMT(II,IL)=TMPMT(II,IVP(IL))                                            
 777  CONTINUE                                                                  
      DO 778 II=1,6                                                             
      DO 778 IL=1,6                                                             
      TMPMT(IL,II)=TRSMT(IVP(IL),II)                                            
 778  CONTINUE                                                                  
      alfort=ALFA(IPP)                                                          
      IF(ALFA(IPP).EQ.0.D0)GOTO 75                                              
      CALL TRASFM(TMPMT,TRSMT,SCOMP,NR6,ALFA(IPP))                              
 75   CONTINUE                                                                  
C620  FORMAT('  MATRICE ELASTICA (TRIAN. SUP.) =',6E12.5/46X,5E12.5/58X,        
C    1      4E12.5/,70X,3E12.5/82X,2E12.5/94X,E12.5)                            
C                                                                               
      CALL JWRITE(ISLELM,ITIP,4)                                                
      CALL JWRITE(ISLELM,ICON,NCON)                                             
      CALL JWRITE(ISLELM,SPES(1,IPP),NCOPP*2)                                   
      CALL JWRITE(ISLELM,VETT,NCON*4)                                           
      CALL JWRITE(ISLELM,9,1)                                                   
      CALL JWRITE(ISLELM,INT1,9)                                                
      CALL JWRITE(ISLELM,TMPMT,72)                                              
      NWL=NWL+83+6*NCON                                                         
C                                                                               
      GOTO 70                                                                   
 80   RETURN                                                                    
 800  WRITE(IOUT,902)IPROP,IDEL                                                 
      IER=1                                                                     
      GOTO 70                                                                   
 801  WRITE(IOUT,903)IDEL,IDM,IPROP                                             
      IER=1                                                                     
      GOTO 70                                                                   
 1000 CONTINUE                                                                  
      CALL READFL(IDM,IER,ITIP,TMPMT,IFLAG,DEN)                                 
      IF(IFLAG.GT.0)GOTO 85                                                     
      CALL INVERT(TMPMT,6,SCOMP,6,6,PERM)                                       
      DO 1010 J=1,6                                                             
      DO 1010 K=1,6                                                             
      TMPMT(J,K)=SCOMP(J,K)                                                     
 1010 CONTINUE                                                                  
      GOTO 85                                                                   
 111  RETURN 1                                                                  
 112  RETURN 2                                                                  
C                                                                               
C                                                                               
      ENTRY WRMATL(IDMAT,ELAS,DENS,NMAT)                                        
C                                                                               
      NR=0                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      WRITE(IOUT,3001)                                                          
      DO 2000 IMAT=1,NMAT                                                       
      NR=NR+6                                                                   
      IF(NR.LE.49)GOTO 2010                                                     
      NR=6                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3000)                                                          
      WRITE(IOUT,3001)                                                          
 2010 CONTINUE                                                                  
      IB=(IMAT-1)*6                                                             
      WRITE(IOUT,3010)IMAT,IDMAT(IMAT),(ELAS(1,K+IB),K=1,6),DENS(IMAT),         
     1               ((ELAS(L,K+IB),K=1,6),L=2,6)                               
 2000 CONTINUE                                                                  
      RETURN                                                                    
C                                                                               
C                                                                               
      ENTRY WRPROL(IDPRO,SPES,ALFA,NPRO,IER)                                    
C                                                                               
      NR=0                                                                      
      CALL INTEST                                                               
      WRITE(IOUT,3100)                                                          
      WRITE(IOUT,3101)                                                          
      DO 2100 IPRO=1,NPRO                                                       
      NR=NR+1                                                                   
      IF(NR.LE.50)GOTO 2110                                                     
      NR=1                                                                      
      WRITE(IOUT,3101)                                                          
      WRITE(IOUT,3101)                                                          
 2110 CONTINUE                                                                  
      NN=4                                                                      
      IF(SPES(4,IPRO).LE.0.D0)NN=3                                              
      IF(SPES(3,IPRO).LE.0.D0)NN=2                                              
      IF(SPES(2,IPRO).GT.0.D0.AND.SPES(1,IPRO).GT.0.D0) GOTO 2120               
      IER=1                                                                     
      WRITE(IOUT,3120)                                                          
 2120 WRITE(IOUT,3110) IPRO,IDPRO(IPRO),IDPRO(IPRO+NPRO),ALFA(IPRO),            
     1          SPES(1,IPRO),SPES(2,IPRO),SPES(3,IPRO),SPES(4,IPRO)             
 2100 CONTINUE                                                                  
      RETURN                                                                    
 900  FORMAT(' ***   AVVISO (INMATL) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI MATERIALI E'' =',I8,'   ***'/)                                    
 901  FORMAT(' ***   AVVISO (INPROL) : OVERFLOW, N.RO MASSIMO '                 
     1   ,'DI PROPRIETA'' E'' =',I8,'   ***'/)                                  
 902  FORMAT(' ***   AVVISO (INCONL) : NON RECUPERATA LA PROPRIETA'''           
     1   ,' N.RO',I8,' DELL''ELEMENTO N.RO',I8,'   ***'/)                       
 903  FORMAT(' ***   AVVISO (INCONL) : PER L''ELEMENTO N.RO',I8,                
     1   ' NON RECUPERATO MATERIALE N.RO',I8,' INDICATO ',                      
     2   'IN PROPRIETA'' N.RO',I8,'   ***'/)                                    
 905  FORMAT(' ***   AVVISO (INCONL) : NON RECUPERATI NODI DELL''',             
     1   ' ELEMENTO N.RO',I8,'   ***')                                          
 916  FORMAT(' ***   AVVISO (INCONL) : CONNESSIONI ERRATE DELL''',              
     1   ' ELEMENTO N.RO',I8,/23X,8I8,'   ***')                                 
 700  FORMAT(/40X,'***   ELEMENTI BIDIMENSIONALI DI LAMINA   ***'/)             
 710  FORMAT(//12X,'   N.RO  ID.LAM  ID.PROP    N.PUNTI INTEG. ',               
     1  4X,'CONNESSIONI'/)                                                      
 720  FORMAT(10X,3I8,6X,2I4,8X,8I8)                                             
 3000 FORMAT(/40X,'***   MATERIALI ELEMENTI BIDIMENSIONALI DI ',                
     1    'LAMINA   ***')                                                       
 3001 FORMAT(//9X,'   N.RO  ID.MAT  ',T35,'MATRICE ELASTICA',T114,              
     1    'DENSITA'''//)                                                        
 3010 FORMAT(6X,2I8,T30,6E12.5,T110,E12.5,5(/T30,6E12.5)/)                      
 3100 FORMAT(/40X,'***   PROPRIETA'' ELEMENTI BIDIMENSIONALI ',                 
     1    'DI LAMINA   ***')                                                    
 3101 FORMAT(//20X,'   N.RO    ID.PROP    ID.MAT    ORIENT. FIBRE ',15X         
     1      ,'SPESSORI'/)                                                       
 3110 FORMAT(15X,3I10,8X,E12.5,10X,4E12.5)                                      
 3120 FORMAT(/' ***   AVVISO (WRPROL) : LA SEGUENTE PRPRIETA'' NON E''',        
     1    ' CONFORME AI DATI RICHIESTI   ***'/)                                 
      END                                                                       
C@ELT,I ANBA*ANBA.INPMPC                                                        
C*************************          INPMPC          ********************        
C     COMPILER (ARGCHK=OFF)                                                     
      SUBROUTINE INPMPC(*,*,ICON,ICOMP,RCOMP,MAXL,NMPC,LMXMPC,LABEL,            
     *               NNODI,COOR,VETT,NODCNT,IERG)                               
      DOUBLE PRECISION RCOMP,VETT,COOR                                          
      LOGICAL DUMP,incore                                                       
      CHARACTER  CONT*4                                                         
      DIMENSION ICON(*),ICOMP(*),RCOMP(*),LABEL(*),COOR(2,*),VETT(*)            
     *         ,NODCNT(*)                                                       
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),incore,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLTN2,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /ELGEN/ ITIP,IDMPC,LMPC,IVAR                                       
      LMXMPC=1                                                                  
      ITIP=6                                                                    
      NPOS=1                                                                    
      NRIG=0                                                                    
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      I=0                                                                       
 50   I=I+1                                                                     
      NS=0                                                                      
      NMPC=0                                                                    
 52   CALL READFF(ISYM,0,NF,*200,*60)                                           
      NMPCA=NF/3                                                                
      NMPC=NMPC+NMPCA                                                           
      IF(NMPC.GT.MAXL) GOTO 600                                                 
      NI=NS+1                                                                   
      NS=NI+NMPCA-1                                                             
      DO 55 L=NI,NS                                                             
       CALL INTGFF(*201,ICON(L),0,LENFL)                                        
       CALL INTGFF(*201,ICOMP(L),0,LENFL)                                       
       CALL DOUBFF(*201,RCOMP(L),0,LENFL)                                       
 55   CONTINUE                                                                  
      IF(NMPCA*3.EQ.NF) GOTO 70                                                 
      CALL CHARFF(*201,CONT,0,LENFL)                                            
      IF(CONT.EQ.'CONT') GOTO 52                                                
      WRITE(IOUT,720)                                                           
      IERG=1                                                                    
      GOTO 50                                                                   
 70   IDMPC=I                                                                   
      LMPC=NS                                                                   
      NRIG=NRIG+LMPC+1                                                          
      IF(NRIG.LE.50) GOTO 22                                                    
      CALL INTEST                                                               
      WRITE(IOUT,700)                                                           
      NRIG=0                                                                    
  22  CONTINUE                                                                  
      IF(LMXMPC.LT.LMPC)LMXMPC=LMPC                                             
      WRITE(IOUT,710)I,LMPC,(ICON(L),ICOMP(L),RCOMP(L),L=1,LMPC)                
      IER=0                                                                     
      CALL RCPCON(ICON,LMPC,LABEL,NNODI,COOR,VETT,NODCNT,IER)                   
      IF(IER.NE.0) IERG=1                                                       
      CALL JWRITE(ISLNWD,NPOS,1)                                                
      CALL JWRITE(ISLMPC,ITIP,3)                                                
      CALL JWRITE(ISLMPC,ICON,LMPC)                                             
      CALL JWRITE(ISLMPC,ICOMP,LMPC)                                            
      CALL JWRITE(ISLMPC,RCOMP,LMPC*2)                                          
      NPOS=NPOS+LMPC*4+3                                                        
      GOTO 50                                                                   
 60   RETURN                                                            nmpc=i-1
 200  RETURN 1                                                                  
 201  RETURN 2                                                                  
600   WRITE(IOUT,601)IDMPC,LMPC,MAXL                                            
      CALL JHALT                                                                
 700  FORMAT(/40X,'***   VINCOLI DI TIPO MULTIPLO (MPC)   ***'//                
     1     10X,'N. MPC',8X,'N.RO TERMINI',10X,                                  
     2         'ID. NODO',8X,'ID. COMP',8X,'COEFFICIENTE'//)                    
 710  FORMAT(8X,I8,10X,I8,12X,I8,6X,I8,8X,E12.5/                                
     1       20(46X,I8,6X,I8,8X,E12.5/))                                        
601   FORMAT(' *** AVVISO (INPMPC): ELEM. N.RO',I4,' DI TIPO 6, DEFINITO        
     1 ',' DA',I4,' GDL ECCEDE IL DIMENS. MAX (',I4,' ) ***')                   
 720  FORMAT(' ***   AVVISO (INPMPC) : CONTINUAZIONE INCORRETTA   ***')         
      END                                                                       

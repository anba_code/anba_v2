C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.SFCSTR                                                        
C*************************          SFCSTR          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE SFCSTR(NUME,NUINT,SOL,SIGMAG,SIGMAL,SIGMAF,EPSI,EPSIL,         
     *             EPSIF,D,ICON,NSOL,NRSOL,ITY,IELSET,NESEL,GPQ)                
C                                                                               
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      REAL RMSP(6,6)                                                            
      character*32 filfit,filstr                                                
      LOGICAL DUMP,incore                                                       
C                                                                               
      COMMON/JFTNIO/INPT,IOUT,ISYM,DUMP(22),incore,iloca                        
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3          
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2                        
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /ELSTR1/ LRECD,NPINTG,ENNEST(3,12),CPINT(2),DAREA,BETA,            
     1                T11(3,3),T22(3,3),DGELAS(6,6),WGHT,DEL,COE(2)             
      COMMON /ELSTR2/ INTG1,INTG2,DENSI,THICK,ANGLAM                            
      COMMON /AUTCON/ ERR,CONST,X,YS,SS,CS,PGCON(6),CDC(6),                     
     1                IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C             
      COMMON /FILEOU/ NFR,ircfit,filfit,filstr,iunstr                           
C                                                                               
      DIMENSION ICON(1),NUME(1),NUINT(1),SOL(NRSOL,1),IELSET(1)                 
      DIMENSION SIGMAG(NPSI,1),SIGMAF(NPSI,1),SIGMF1(6,6),                      
     1          SIGMAL(NPSI,1),EPSI(NPSI,1),D(6,6),DMAU(6,6),                   
     2          EPSIL(NPSI,1),EPSIF(NPSI,1),TEC(6,6),GPQ(3,6),                  
     3           RIS(6,6),BTA(3,3),BTB(3,3)                                     
C                                                                               
C                                                                               
C                                                                               
C      write(iout,'(10i8)') NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,               
C     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC          
C      WRITE(IOUT,*)'IELSET',(IELSET(I),I=1,NESEL)                              
      CALL DZERO(RIS,36)                                                        
      NWORD=NSOL*NPSI*2                                                         
      NELI=NELS                                                                 
      DO 8500 IEL=1,NELI                                                        
 850  CALL JREAD(ISLELM,ITIP,4)                                                 
      PRINT*,IEL,ITIP,IDEL                                                      
      IF(ITIP.NE.8)GOTO 8900                                                    
      CALL JREAD(ISLELM,ICON,NCON)                                              
      CALL JPOSRL(ISLELM,NCON*4)                                                
      CALL JREAD(ISLELM,NSTRAT,1)                                               
      CALL JREAD(ISLELS,LRECD,2)                                                
      DO 8000 IST=1,NSTRAT                                                      
      CALL JREAD(ISLELM,INTG1,8)                                                
C      WRITE(IOUT,*)'SFCSTR: ORIENTAZIONE STRATO N. ',IST,' = ',ANGLAM          
      DO 85 I=1,6                                                               
      CALL JREAD(ISLELM,D(1,I),12)                                              
 85   CONTINUE                                                                  
C      WRITE(IOUT,*)'SFCSTR: D LETTA IN ISLELM'                                 
C      WRITE(IOUT,1111)((D(II,KK),KK=1,6),II=1,6)                               
 1111 FORMAT(/6(1X,6D12.5/))                                                    
  873 FORMAT(/3(1X,8D9.3/))                                                     
C                                                                               
      ICNT=1                                                                    
      DO 810 IN=1,NPINTG                                                        
      CALL JREAD(ISLELS,ENNEST,LRECD)                                           
C      WRITE(IOUT,*)'FUNZIONI DI FORMA DOPO JREAD IN SFCSTR'                    
C      WRITE(IOUT,873)((ENNEST(IOL,IOP),IOP=1,8),IOL=1,3)                       
      IPEL=1                                                                    
      IF(NESEL.NE.0) IPEL=IPOSL(IDEL,IELSET,NESEL)                              
      IF(IFLMR.EQ.0.OR.IPEL.EQ.0)GOTO 810                                       
      IF(IN.EQ.1 .AND. DUMP(16))  WRITE(IOUT,*) ITIP,IDEL,NPINTG                
C      WRITE(IOUT,*) ' p.integ ',NPINTG,CPINT(1),CPINT(2)                       
      IF(DUMP(16)) WRITE(IOUT,*) CPINT(1),CPINT(2)                              
      CALL DZERO(EPSI,36)                                                       
      DO 815 K1=1,6                                                             
      DO 815 K2=1,6                                                             
      DGELAS(K1,K2)=DGELAS(K1,K2)/WGHT                                          
 815  CONTINUE                                                                  
      DO 820 I=1,NSOL                                                           
C      PRINT*,' SOL',I                                                          
      IND1=NEQV-NPSI                                                            
C                                                                               
C     CALCOLO EPSILON=EPSILON+ZETA*PSI                                          
C                                                                               
      EPSI(1,I)=EPSI(1,I)+(SOL(IND1+1,I)-CPINT(2)*SOL(IND1+6,I))/DEL            
      EPSI(2,I)=EPSI(2,I)+(SOL(IND1+2,I)+CPINT(1)*SOL(IND1+6,I))/DEL            
      EPSI(3,I)=EPSI(3,I)+(SOL(IND1+3,I)+CPINT(2)*SOL(IND1+4,I)                 
     1                                  -CPINT(1)*SOL(IND1+5,I))/DEL            
      KK=0                                                                      
      DO 840 K=1,NCON                                                           
      IF(ICON(K).EQ.0) GOTO 840                                                 
      KK=KK+1                                                                   
      IND=(NUINT(ICON(K))-1)*NCS+1                                              
      IND=NUME(IND)-1                                                           
C                                                                               
C     CALCOLO EPSILON=EPSILON+ESSE*G DERIVATO/Z                                 
C                                                                               
      EPSI(1,I)=EPSI(1,I)+(ENNEST(1,KK)*SOL(IND+NEQV+1,I))/DEL                  
      EPSI(2,I)=EPSI(2,I)+(ENNEST(1,KK)*SOL(IND+NEQV+2,I))/DEL                  
      EPSI(3,I)=EPSI(3,I)+(ENNEST(1,KK)*SOL(IND+NEQV+3,I))/DEL                  
C                                                                               
C                                                                               
C     CALCOLO EPSILON=EPSILON+G*OPERATORE DERIVATA                              
C                                                                               
      CALL BTABUP(DEL,ENNEST(1,KK),BTA,COE,CDC(4))                              
      CALL BTBBUP(DEL,ENNEST(1,KK),BTB)                                         
      DO 830 L=1,3                                                              
      DO 830 M=1,3                                                              
      EPSI(L,I)=EPSI(L,I)+BTA(M,L)*SOL(IND+M,I)/DEL                             
      EPSI(L+3,I)=EPSI(L+3,I)+BTB(M,L)*SOL(IND+M,I)/DEL                         
 830  CONTINUE                                                                  
C      WRITE(IOUT,399) kk,epsi(5,i),sol(ind+2,i),ennest(3,kk)                   
399   format(i3,6e12.5)                                                         
 840  CONTINUE                                                                  
 820  CONTINUE                                                                  
CC                                                                              
      DO 1122 IJ=1,6                                                            
      DO 1113 IK=1,6                                                            
      EPSIL(IJ,IK)=0.D0                                                         
 1113 CONTINUE                                                                  
 1122 CONTINUE                                                                  
 1119 FORMAT(/1X,I3/,6(6D12.6/))                                                
      CALL DPROMM(DGELAS,6,6,6,EPSI,6,SIGMAG,6,6,0,0)                           
C      WRITE(IOUT,1119)ICNT,((DGELAS(II,KK),KK=1,6),II=1,6)                     
C      WRITE(IOUT,'(a,6e12.5)')' epsi: ',(EPSI(I,3),I=1,6)                      
C      WRITE(IOUT,'(a,6e12.5)')' sigm: ',(SIGMAG(I,3),I=1,6)                    
      ICNT=ICNT+1                                                               
C      WRITE(*,*)' trasf4 SFCSTR: BETA =',BETA                                  
      CALL TRASF4(EPSI,BETA,EPSIL)                                              
C     WRITE(IOUT,*)'SFCSTR: EPSIL'                                              
C     WRITE(IOUT,1111)((EPSIL(II,KK),II=1,6),KK=1,6)                            
      CALL DPROMM(D,6,6,6,EPSIL,6,SIGMAL,6,6,0,0)                               
C     WRITE(IOUT,*)'SFCSTR: SIGMAL'                                             
C     WRITE(IOUT,1111)((SIGMAL(II,KK),II=1,6),KK=1,6)                           
      if(iabs(iunstr).gt.0) then                                                      
        do i=1,nsol                                                             
         if(iunstr.gt.0) then
        write(iunstr,7)idel,itip,0,cpint(1),cpint(2),(sigmal(i,l),l=1,6)        
         else
        write(-iunstr)idel,itip,0,cpint(1),cpint(2),(sigmal(l,i),l=1,6)          
         endif
        enddo                                                                   
      endif                                                                     
7     format(3i8,8E12.5)                                                        
      IF(DUMP(16)) THEN                                                         
         WRITE(IOUT,*) ((SIGMAL(I,K),I=1,6),K=1,6)                              
         WRITE(IOUT,*) ((SIGMAG(I,K),I=1,6),K=1,6)                              
         WRITE(IOUT,*) ((EPSIL(I,K),I=1,6),K=1,6)                               
         WRITE(IOUT,*) ((EPSI(I,K),I=1,6),K=1,6)                                
      END IF                                                                    
          DO 44 IR1=1,6                                                         
            DO 44 IC1=1,6                                                       
44          RMSP(IR1,IC1)=EPSI(IR1,IC1)                                         
      IF(DUMP(14)) CALL JWRITE(ISLSFC,RMSP,36)                                  
C                                                                               
          DO 644 IR1=1,6                                                        
            DO 644 IC1=1,6                                                      
644         RMSP(IR1,IC1)=EPSIL(IR1,IC1)                                        
      IF(DUMP(14)) CALL JWRITE(ISLSFC,RMSP,36)                                  
C      WRITE(IOUT,*)'SFCSTR: EPSI DOPO LA SCRITTURA SU ISLSFC'                  
C      WRITE(IOUT,1111)((RMSP(II,KK),KK=1,6),II=1,6)                            
C                                                                               
          DO 55 IR1=1,6                                                         
            DO 55 IC1=1,6                                                       
55          RMSP(IR1,IC1)=SIGMAG(IR1,IC1)                                       
      CALL JWRITE(ISLSFC,RMSP,36)                                               
C      WRITE(IOUT,*)'SFCSTR: SIGMAG DOPO LA SCRITTURA SU ISLSFC'                
C      WRITE(IOUT,1111)((RMSP(II,KK),KK=1,6),II=1,6)                            
C                                                                               
C                                                                               
          DO 555 IR1=1,6                                                        
            DO 555 IC1=1,6                                                      
555         RMSP(IR1,IC1)=SIGMAL(IR1,IC1)                                       
      CALL JWRITE(ISLSFC,RMSP,36)                                               
C                                                                               
      CALL DZERO(DMAU,36)                                                       
      DO 666 I=1,6                                                              
      DO 666 K=1,6                                                              
      DMAU(I,K)=D(I,K)                                                          
 666  CONTINUE                                                                  
C      WRITE(IOUT,*)'SFCSTR: ANGLAM =',ANGLAM                                   
C      WRITE(*,*)' trasf2 e 3 SFCSTR: ANGLAM =',ANGLAM                          
      CALL TRASF2(DMAU,6,ANGLAM)                                                
      IF(DUMP(20))THEN                                                          
      CALL TRASF3(EPSIL,ANGLAM,EPSIF)                                           
C     WRITE(IOUT,*)'SFCSTR: EPSIF'                                              
C     WRITE(IOUT,1111)((EPSIF(II,KK),II=1,6),KK=1,6)                            
      CALL DZERO(SIGMAF,36)                                                     
C                                                                               
C                                                                               
      CALL DPROMM(DMAU,6,6,6,EPSIF,6,SIGMAF,6,6,0,0)                            
C     WRITE(IOUT,*)'SFCSTR: SIGMAF'                                             
C     WRITE(IOUT,1111)((SIGMAF(II,KK),II=1,6),KK=1,6)                           
          DO 4444 IR1=1,6                                                       
            DO 4444 IC1=1,6                                                     
4444        RMSP(IR1,IC1)=EPSIF(IR1,IC1)                                        
      IF(DUMP(14)) CALL JWRITE(ISLSFC,RMSP,36)                                  
C                                                                               
      END IF                                                                    
C                                                                               
      IF(DUMP(19)) THEN                                                         
C                                                                               
          DO 5555 IR1=1,6                                                       
            DO 5555 IC1=1,6                                                     
5555        RMSP(IR1,IC1)=SIGMAF(IR1,IC1)                                       
      CALL JWRITE(ISLSFC,RMSP,36)                                               
C                                                                               
C                                                                               
      END IF                                                                    
C                                                                               
C         CALCOLO TERMINI B                                                     
C                                                                               
      DO 78 K=1,6                                                               
      RIS(1,K)=RIS(1,K)+DAREA*SIGMAG(1,K)                                       
      RIS(2,K)=RIS(2,K)+DAREA*SIGMAG(2,K)                                       
      RIS(3,K)=RIS(3,K)+DAREA*SIGMAG(3,K)                                       
      RIS(4,K)=RIS(4,K)+DAREA*SIGMAG(3,K)*CPINT(2)                              
      RIS(5,K)=RIS(5,K)-DAREA*SIGMAG(3,K)*CPINT(1)                              
      RIS(6,K)=RIS(6,K)-DAREA*SIGMAG(1,K)*CPINT(2)+                             
     1                  DAREA*SIGMAG(2,K)*CPINT(1)                              
 78   CONTINUE                                                                  
      DO 88 I=1,NSOL                                                            
        GPQ(1,I)=GPQ(1,I)+DAREA*(DEL**2)*SIGMAG(3,I)*CPINT(2)*CPINT(2)          
        GPQ(2,I)=GPQ(2,I)+DAREA*(DEL**2)*SIGMAG(3,I)*CPINT(1)*CPINT(1)          
        GPQ(3,I)=GPQ(3,I)-DAREA*(DEL**2)*SIGMAG(3,I)*CPINT(1)*CPINT(2)          
 88   CONTINUE                                                                  
C                                                                               
C                                                                               
 810  CONTINUE                                                                  
C                                                                               
 8000 CONTINUE                                                                  
C                                                                               
      IF(IFLMR.EQ.0) GOTO 850                                                   
 8500 CONTINUE                                                                  
      WRITE(IOUT,'(/,A)')' RISULTANTI DEGLI SFORZI'                             
      WRITE(IOUT,'(1X,6E12.5)')((RIS(I,K),K=1,6),I=1,6)                         
      RETURN                                                                    
 8900 CALL JPOSRL(ISLELM,-4)                                                    
      RETURN          
                                                          
      END                                                                       

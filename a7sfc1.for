C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MODSFC                                                        
C*************************          MODSFC          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE MODSFC                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      INTEGER SPCCOD                                                            
      DIMENSION DV(1)                                                           
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,nn2,nn3          
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,iloca                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSFC,ISLSL2,         
     *                ISLASC,ISLCFC,ISLSC1,ISLTC2,ISLSC2,                       
     *                ISTEMP, ISMTER, ISTNOT, ISLSLT                            
      COMMON /ELGEN/ ITIP,IDEL,NCON,IFLMR                                       
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,NELS,                     
     1                NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NELSR,NSPC           
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO,IGEOM,C                             
      DATA MPCCOD/6/,SPCCOD/7/                                                  
C                                                                               
      IF(MODEX(6).EQ.1)THEN                                                     
c     CALL JDELET('*ASM    ')                                                   
c     CALL JDELET('*MRG    ')                                                   
c     CALL JDELET('*MTF    ')                                                   
c     CALL JDELET('*MTN    ')                                                   
c     CALL JDELET('*NWD    ')                                                   
c     CALL JDELET('*RIN    ')                                                   
c     CALL JDELET('*MPC    ')                                                   
c     CALL JDELET('*TAB    ')                                                   
C>>>  IF(.NOT.INCORE) CALL JDELET('*FCT    ')                                   
c     CALL JDELET('*VPZ    ')                                                   
c     CALL JDELET('*TEM    ')                                                   
c     CALL JDELET('*TN2    ')                                                   
      END IF                                                                    
      CALL JCRERS                                                               
      NSOL=6                                                                    
      NRSOL=NEQV+NEQV-NPSI                                                      
      CALL JALLOC(*2000,'GPQ     ','D.P.',1, 3*6      ,JGPQ  )                  
      CALL JALLOC(*2000,'WJX     ','D.P.',1,36        ,JWJX  )                  
      CALL JALLOC(*2000,'FM1     ','D.P.',1,36        ,JFM1  )                  
      CALL JALLOC(*2000,'SOL     ','D.P.',1,NRSOL*NSOL   ,JSOL  )               
C############                                                                   
      CALL JALLOC(*2000,'SOL1    ','D.P.',1,NRSOL*NSOL   ,JSOL1 )               
C############Y                                                                  
      IF(DUMP(10)) THEN                                                         
      CALL JOPEN('*CSZ',ISLCSZ,1)                                               
      CALL JREAD(ISLCSZ,DV(JFM1),72)                                            
      CALL JPOSRL(ISLCSZ,72)                                                    
      CALL JREAD(ISLCSZ,DV(JWJX),72)                                            
      CALL JCLOSE(ISLCSZ)                                                       
      END IF                                                                    
      CALL JOPEN('*SL1    ',ISLSL1,1)                                           
      CALL JOPEN('*SL2    ',ISLSL2,1)                                           
      CALL RCPSOL(DV(JSOL),NRSOL,NSOL)                                          
      CALL JCLOSE(ISLSL2)                                                       
      CALL JCLOSE(ISLSL1)                                                       
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JOPEN('*NUM    ',ISLNUM,1)                                           
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
C                                                                               
      CALL DZERO(DV(JGPQ), 3*6)                                                 
C                                                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXNOD    ,JICON )                  
      CALL JALLOC(*2000,'SIGMAG  ','D.P.',1,NSOL*NPSI ,JSIGMG)                  
      CALL JALLOC(*2000,'SIGMAL  ','D.P.',1,NSOL*NPSI ,JSIGML)                  
      CALL JALLOC(*2000,'SIGMAF  ','D.P.',1,NSOL*NPSI ,JSIGMF)                  
      CALL JALLOC(*2000,'EPSI    ','D.P.',1,NSOL*NPSI ,JEPSI )                  
      CALL JALLOC(*2000,'EPSIG   ','D.P.',1,NSOL*NPSI ,JEPSIG)                  
      CALL JALLOC(*2000,'EPSIL   ','D.P.',1,NSOL*NPSI ,JEPSIL)                  
      CALL JALLOC(*2000,'D       ','D.P.',1,6*6       ,JD    )                  
C                                                                               
      CALL JOPEN('*SEL    ',ISLSEL,1)                                           
      CALL JREAD(ISLSEL,NESEL,1)                                                
      CALL JALLOC(*2000,'IELSET  ','INT.',1,NESEL   ,JIELST)                    
      IF(NESEL.NE.0) CALL JREAD(ISLSEL,IV(JIELST),NESEL)                        
      CALL JCLOSE(ISLSEL)                                                       
      CALL JOPEN('*SFC    ',ISLSFC,1)                                           
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
      CALL JOPEN('*ELS    ',ISLELS,1)                                           
C                                                                               
      NE=0                                                                      
6     CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      print*,itip,idel,ncon                                                     
      IF(ITIP.EQ.MPCCOD.OR.ITIP.EQ.SPCCOD) THEN                                 
      JUMP=NCON*4+6                                                             
      CALL JPOSRL(ISLELM,JUMP)                                                  
      NE=NE+1                                                                   
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.1) THEN                                                   
C                                                                               
      CALL SFCCOR(IV(JICON),IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),           
     *      NSOL,NRSOL,IV(JIELST),NESEL,DV(JGPQ))                               
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.2) THEN                                                   
C                                                                               
      CALL SFCGIU(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),IV(JICON),           
     1            NSOL,NRSOL,IV(JIELST),NESEL,DV(JGPQ))                         
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.3) THEN                                                   
C
      print*,' chiamo sfcpan'                                                                               
      CALL SFCPAN(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
     *            IV(JICON),NSOL,NRSOL,DV(JEPSI),DV(JD),                        
     *            IV(JIELST),NESEL,DV(JGPQ))                                    
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.4) THEN                                                   
C                                                                               
c++++++++++++++++++  31 agosto   +++++++++++++++++++++++++++++++++++            
c     ho inserito l' ultimo indice [index=1]                                    
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      CALL SFCEPL(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
     1            DV(JEPSI),DV(JD),IV(JICON),NSOL,NRSOL,4,                      
     *           IV(JIELST),NESEL,DV(JGPQ),DV(JSOL1),1)                         
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.5) THEN                                                   
C                                                                               
c++++++++++++++++++  31 agosto   +++++++++++++++++++++++++++++++++++            
c     ho inserito l' ultimo indice [index=1]                                    
c+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++            
      CALL SFCEPL(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
     1            DV(JEPSI),DV(JD),IV(JICON),NSOL,NRSOL,5,                      
     *           IV(JIELST),NESEL,DV(JGPQ),DV(JSOL1),1)                         
      NE=NE+NELP                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
C                                                                               
      ELSE IF(ITIP.EQ.8) THEN                                                   
C                                                                               
      CALL SFCSTR(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
     1 DV(JSIGMF),DV(JEPSIG),DV(JEPSIL),DV(JEPSI),DV(JD),IV(JICON),             
     *                         NSOL,NRSOL,8,IV(JIELST),NESEL,DV(JGPQ))          
      NE=NE+NELS                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
      ELSE                                                                      
      WRITE(IOUT,818)ITIP                                                       
 818  FORMAT(/' ***   AVVISO (MODSFC) : NON RICONOSCIUTO ELEMENTO ',            
     *        ' N.RO',I8,'   ***'/)                                             
      END IF                                                                    
      GOTO 6                                                                    
7     CONTINUE                                                                  
C                                                                               
C     IF(DUMP(10)) THEN                                                         
C     IF(NESEL.NE.0) GOTO 710                                                   
C     CALL JALLOC(*2000,'TEMP  ',3,1,81        ,JTEMP )                         
C     CALL RIGEOM(DV(JFM1),DV(JTEMP),NSOL,DV(JWJX),DV(JGPQ))                    
C     END IF                                                                    
C     GOTO 77                                                                   
C710  WRITE(IOUT,700)                                                           
C700  FORMAT(/' ***   AVVISO (MODSFC): NECESSARI GLI SFORZI DI TUTTI ',         
C    *  'GLI ELEMENTI PER CALCOLO RIGIDEZZE GEOMETRICHE   ***'/)                
C                                                                               
277   IF(DUMP(18)) THEN                                                         
      NE=0                                                                      
C                                                                               
      CALL PRTEPS(IV(JIELST),DV(JSIGMG),DV(JSIGML),NSOL,DV(JSIGMG),             
     *            DV(JSIGMG),DV(JSIGML),DV(JSIGMF),DV(JGPQ),nsol)               

      if(nstrat.gt.0) then
       nlop=7
      else
       nlop=1
      endif                                                                
      do lop=1,nlop
      if(lop.eq.1) then
        ixsol=nsol
      else
        ixsol=-(lop-1)
      endif
      
      CALL JPOSAB(ISLSFC,1)                                                     
      CALL JPOSAB(ISLELM,1)                                                     
      CALL JPOSAB(ISLELS,1)                                                     
C                                                                               
C                                                                               
256   CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      IF(ITIP.EQ.MPCCOD.OR.ITIP.EQ.SPCCOD) THEN                                 
      JUMP=NCON*4+6                                                             
      CALL JPOSRL(ISLELM,JUMP)                                                  
      NE=NE+1                                                                   
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.1) THEN                                                   
C                                                                               
      CALL EPSCOR(DV(JSIGMG),                                                   
     *          IV(JIELST),NESEL,ixsol)                                          
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.2) THEN                                                   
C                                                                               
      CALL EPSGIU(DV(JSIGMG),                                                   
     1            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.3) THEN                                                   
C                                                                               
      CALL EPSPAN(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),                                                    
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.4) THEN                                                   
C                                                                               
      CALL EPSEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),4,                                                  
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.5) THEN                                                   
C                                                                               
      CALL EPSEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),5,                                                  
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NELP                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
C                                                                               
      ELSE IF(ITIP.EQ.8) THEN                                                   
C                                                                               
      CALL EPSSTR(DV(JSIGMG),DV(JSIGML),DV(JSIGMF),DV(JEPSI),                   
     *            DV(JEPSIG),DV(JEPSIL),8,                                      
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NELS                                                                
      IF(NE.EQ.NELTOT) GOTO 257                                                 
      END IF                                                                    
      GOTO 256                                                                  
257   CONTINUE                                                                  
C     
      enddo
                                                                                
      END IF                                                                    
C                                                                               
 77   IF(DUMP(8)) THEN                                                          
      NE=0                                                                      
C                                                                               
      CALL PRTSFC(IV(JIELST),DV(JSIGMG),DV(JSIGML),NSOL,                        
     *              DV(JSIGMG),DV(JSIGMG),DV(JSIGML),                           
     *              DV(JSIGMF),DV(JGPQ))                                        
      CALL JPOSAB(ISLSFC,1)                                                     
      CALL JPOSAB(ISLELM,1)                                                     
      CALL JPOSAB(ISLELS,1)                                                     
C               
      if(nstrat.gt.0) then
       nlop=7
      else
       nlop=1
      endif                                                                
      do lop=1,nlop
      if(lop.eq.1) then
        ixsol=nsol
      else
        ixsol=-(lop-1)
      endif
C                                                                               
56    CALL JREAD(ISLELM,ITIP,4)                                                 
      CALL JPOSRL(ISLELM,-4)                                                    
      IF(ITIP.EQ.MPCCOD.OR.ITIP.EQ.SPCCOD) THEN                                 
      JUMP=NCON*4+6                                                             
      CALL JPOSRL(ISLELM,JUMP)                                                  
      NE=NE+1                                                                   
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.1) THEN                                                   
C                                                                               
      CALL PRTCOR(DV(JSIGMG),                                                   
     *          IV(JIELST),NESEL,ixsol)                                          
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.2) THEN                                                   
C                                                                               
      CALL PRTGIU(DV(JSIGMG),                                                   
     1            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.3) THEN                                                   
C                                                                               
      CALL PRTPAN(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),                                                    
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.4) THEN                                                   
C                                                                               
      CALL PRTEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),4,                                                  
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
C                                                                               
      ELSE IF(ITIP.EQ.5) THEN                                                   
C                                                                               
      CALL PRTEPL(DV(JSIGMG),DV(JSIGML),                                        
     *            DV(JEPSI),5,                                                  
     *            IV(JIELST),NESEL,ixsol)                                        
      NE=NE+NELP                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
      ELSE IF(ITIP.EQ.8) THEN                                                   
C                                                                               
      CALL PRTSTR(DV(JSIGMG),DV(JSIGML),DV(JSIGMF),                             
     *            DV(JEPSIG),DV(JEPSIL),DV(JEPSI),                              
     *            8,IV(JIELST),NESEL,ixsol)                                      
      NE=NE+NELS                                                                
      IF(NE.EQ.NELTOT) GOTO 57                                                  
      END IF                                                                    
      GOTO 56                                                                   
57    CONTINUE                                                                  
C     
      enddo                                                                          
      END IF                                                                    
C                                                                               
      CALL JCLOSE(ISLELS)                                                       
      CALL JCLOSE(ISLELM)                                                       
      CALL JCLOSE(ISLSFC)                                                       
      RETURN                                                                    
2000  CALL JHALT                                                                
      STOP                                                                      
      END                                                                       
C                                                                               
C@ELT,I ANBA*ANBA.RIGEOM                                                        
C*************************          RIGEOM          ********************        
C     COMPILER (ARGCHK=OFF),(LINK=IBJ$)                                         
      SUBROUTINE RIGEOM(RIGID,TEMP,NSOL,W,GPQ)                                  
C                                                                               
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)                                       
      LOGICAL DUMP,INCORE                                                       
      CHARACTER*2 CNDC(6)                                                       
      DIMENSION RIGID(NSOL,1),TEMP(NSOL,1)                                      
      DIMENSION TTT(3,3),W(6,6),GPQ(6,6,1),GPQG(9,9),TRASF(9,9)                 
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      DATA CNDC/'T1','T2','T3','M1','M2','M3'/                                  
      DATA TTT/0.D0,1.D0,0.D0,-1.D0,5*0.D0/                                     
C                                                                               
C                                                                               
C                                                                               
      DO 400 NS=1,NSOL                                                          
      CALL DZERO(GPQG,81)                                                       
      GOTO (501,502,503,504,505,506),NS                                         
 501  GPQG(2,9)=1.D0                                                            
      GPQG(3,8)=-1.D0                                                           
      GPQG(7,9)=.5D0                                                            
      GOTO 510                                                                  
 502  GPQG(1,9)=-1.D0                                                           
      GPQG(3,7)=1.D0                                                            
      GPQG(8,9)=.5D0                                                            
      GOTO 510                                                                  
 503  GPQG(1,1)=1.D0                                                            
      GPQG(2,2)=1.D0                                                            
      GPQG(3,3)=1.D0                                                            
      GOTO 510                                                                  
 504  GPQG(1,6)=-1.D0                                                           
      GPQG(3,4)=1.D0                                                            
      GPQG(5,9)=.5D0                                                            
      GPQG(6,8)=.5D0                                                            
      GOTO 510                                                                  
 505  GPQG(2,6)=-1.D0                                                           
      GPQG(3,5)=1.D0                                                            
      GPQG(4,9)=-.5D0                                                           
      GPQG(6,7)=-.5D0                                                           
      GOTO 510                                                                  
 506  GPQG(4,8)=.5D0                                                            
      GPQG(5,7)=-.5D0                                                           
 510  DO 401 I=1,3                                                              
      CALL DMOVE(GPQ(1,I,NS),GPQG(4,3+I),I)                                     
 401  CONTINUE                                                                  
      DO 402 I=2,9                                                              
      DO 402 L=1,I-1                                                            
      GPQG(I,L)=GPQG(L,I)                                                       
 402  CONTINUE                                                                  
      CALL INTEST                                                               
      WRITE(IOUT,701)CNDC(NS),((GPQG(IR,IC),IC=1,9),IR=1,9)                     
 400  CONTINUE                                                                  
C                                                                               
C                                                                               
      RETURN                                                                    
 701  FORMAT(///10X,'MATRICE DI RIGIDEZZA GEOMETRICA  (',A2,')'/                
     *   10X,37('*')//9(6X,6E12.5,5X,3E12.5/))                                  
C                                                                               
      END                                                                       

C----------------------------------------------------------------------
C    Copyright 2018 Gian Luca Ghiringhelli
C----------------------------------------------------------------------
C
C    This file is part of Anba.
C
C    Anba is free software: you can redistribute it and/or modify
C    it under the terms of the GNU General Public License as published by
C    the Free Software Foundation, either version 3 of the License, or
C    (at your option) any later version.
C
C    Hanba is distributed in the hope that it will be useful,
C    but WITHOUT ANY WARRANTY; without even the implied warranty of
C    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
C    GNU General Public License for more details.
C
C    You should have received a copy of the GNU General Public License
C    along with Anba.  If not, see <https://www.gnu.org/licenses/>.
C
C----------------------------------------------------------------------
C
C@ELT,I ANBA*ANBA.MD1SLE                                                        
C*************************          MD1SLE          ********************        
      SUBROUTINE MD1SLE                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      DIMENSION DV(1)                                                           
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSL2          
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV                
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL INTGFF(*101,NAUTOV,0,LENFL)                                          
      CALL INTGFF(*101,NREQ,0,LENFL)                                            
      CALL INTGFF(*101,NMAX,0,LENFL)                                            
      CALL INTGFF(*101,INCR,0,LENFL)                                            
      IF(NF.GE.5) CALL DOUBFF(*101,TOLL,0,LENFL)                                
      IF(NF.GE.6) CALL DOUBFF(*101,ORTER,0,LENFL)                               
      IF(NF.GE.7) CALL DOUBFF(*101,ZERTOL,0,LENFL)                              
      CALL INTGFF(*101,ISHF,0,LENFL)                                            
C                                                                               
      CALL INTEST                                                               
      WRITE(IOUT,666)NAUTOV,NREQ,NMAX,INCR,TOLL,ORTER,ZERTOL                    
666   FORMAT(////////39X,'***  PARAMETRI DI CALCOLO AUTOSOLUZIONI  ***'/        
     1            //39X,'              METODO DI LANCZOS'//                     
     128X,'N.RO AUTOVALORI ',35('.'),' :',I12/                                  
     A28X,'N.RO AUTOVALORI RICHIESTI ',25('.'),' :',I12/                        
     228X,'N.RO MASSIMO DI AUTOVALORI ',24('.'),' :',I12/                       
     328X,'INCREMENTO DEL N.RO DI AUTOVALORI ',17('.'),' :',I12/                
     428X,'TOLLERANZA DI CONVERGENZA .........',16('.'),' :',E12.5/             
     528X,'ERRORE DI ORTOGONALIZZAZIONE ....',18('.'),' :',E12.5/               
     628X,'TOLLERANZA SULLO ZERO ',29('.'),' :',E12.5///)                       
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV     ,JNA   )                   
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
C                                                                               
      IF(.NOT.INCORE) THEN                                                      
       NST=1                                                                    
       DO 1 I=2,NEQV                                                            
        NST=NST+I-IV(JNA+I-1)+1                                                 
        IV(JNA+I-1)=NST                                                         
 1     CONTINUE                                                                 
      END IF                                                                    
C                                                                               
      NSTO=IABS(NSTO)                                                           
 900  FORMAT( )                                                                 
          N1=15+(NEQV*2+1)*4                                                    
          NW=NSTO*2                                                             
          MEMDIS=MAXDIM-NEQV*9-NSTO*2-17*7-898                                  
	  memdis=memdis*.8                                                             
          RESTO=FLOAT(N1*N1+16*MEMDIS)                                          
          IREST=INT(DSQRT(RESTO))                                               
          NNMAX=(-N1+IREST)/8                                                   
      IF(NMAX.GT.NEQV+NEQV) NMAX=NEQV+NEQV                                      
          IF(NMAX.GT.NNMAX)THEN                                                 
          NMAX=NNMAX                                                            
          NREQ=NMAX                                                             
          NAUTOV=NMAX                                                           
          WRITE(IOUT,667)NMAX                                                   
 667      FORMAT(//' *** AVVISO (MD1SLE) : OVERFLOW , N. AUTOVALORI ',          
     #   'RIDOTTO A ',I8,'   ***'/)                                             
          END IF                                                                
      CALL JALLOC(*2000,'VC      ','D.P.',1,NMAX*(NEQV*2+1),JVC )               
      CALL JALLOC(*2000,'VL      ','D.P.',1,NMAX*(NEQV*2+1),JVL )               
      CALL JALLOC(*2000,'A       ','D.P.',1,NMAX      ,JA    )                  
      CALL JALLOC(*2000,'B       ','D.P.',1,NMAX      ,JB    )                  
      CALL JALLOC(*2000,'C       ','D.P.',1,NMAX      ,JC    )                  
      CALL JALLOC(*2000,'RR      ','D.P.',1,NMAX      ,JRR   )                  
      CALL JALLOC(*2000,'RI      ','D.P.',1,NMAX      ,JRI   )                  
      CALL JALLOC(*2000,'IANA    ','INT.',1,NMAX      ,JIANA )                  
      CALL JALLOC(*2000,'RM      ','D.P.',1,NSTO      ,JRM   )                  
C                                                                               
      CALL JOPEN('*MAT    ',ISLMAT,1)                                           
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1 ,JIPIAZ)                   
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
C                                                                               
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
C                                                                               
C                                                                               
      CALL DZERO(DV(JRM),NSTO)                                                  
      CALL AUVASM(DV(JRM),IV(JIPIAZ),DV(JELMAT),                                
     1            IV(JNA),MAXDEL)                                               
          CALL JWRITE(ISLMAT,DV(JRM),NW)                                        
c  call jprtab                                                                  
	call matprt(iv(jna),dv(jrm),neqv,'M')                                          
      CALL DZERO(DV(JRM),NSTO)                                                  
      CALL AUVASD (DV(JRM),IV(JIPIAZ),DV(JELMAT),                               
     1            IV(JNA),MAXDEL)                                               
c  call jprtab                                                                  
	call matprt(iv(jna),dv(jrm),neqv,'D')                                          
          CALL JWRITE(ISLMAT,DV(JRM),NW)                                        
C                                                                               
      IEX=JEXIST('*ASM    ')                                                    
      IF(IEX.EQ.0.OR..NOT.INCORE)THEN                                           
      CALL DZERO(DV(JRM),NSTO)                                                  
      CALL AUVASK(DV(JRM),IV(JIPIAZ),DV(JELMAT),                                
     1            IV(JNA),MAXDEL)                                               
      ELSE                                                                      
      CALL JOPEN('*ASM    ',ISLASM,1)                                           
      CALL JREAD(ISLASM,DV(JRM),NW)                                             
C     DO 66 I=1,NEQV                                                            
C     JRMIND=IV(JNA+I-1)-1+JRM                                                  
C     IF(DV(JRMIND).GT.CONST) DV(JRMIND)=DV(JRMIND)-CONST                       
C 66  CONTINUE                                                                  
      CALL JCLOSE(ISLASM)                                                       
      END IF                                                                    
C                                                                               
      CALL JCLOSE(ISLMTN)                                                       
      CALL JCLOSE(ISLMTF)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
c  call jprtab                                                                  
	call matprt(iv(jna),dv(jrm),neqv,'K')                                          
      CALL JFREE(*2000,'ELMAT   ')                                              
      CALL JFREE(*2000,'IPIAZ   ')                                              
C                                                                               
      CALL JALLOC(*2000,'Z       ','D.P.',1,NMAX*NMAX*2 ,JZ  )                  
      CALL JALLOC(*2000,'TEMP    ','D.P.',1,NEQV*2    ,JTEMP )                  
      CALL JALLOC(*2000,'TEMP1   ','D.P.',1,NEQV*2      ,JTEMP1)                
      CALL JALLOC(*2000,'VOLD    ','D.P.',1,NMAX      ,JVOLD )                  
      CALL JALLOC(*2000,'VATT    ','D.P.',1,NMAX      ,JVATT )                  
C                                                                               
C         NEQV=NEQV-NPSI                                                        
      IEX=JEXIST('*FCT    ')                                                    
      IF(IEX.EQ.0.OR..NOT.INCORE)THEN                                           
      CALL FACTOR(DV(JRM),IV(JNA),NEQV)                                         
      ELSE                                                                      
      CALL JOPEN('*FCT    ',ISLFCT,1)                                           
      CALL JREAD(ISLFCT,DV(JRM),NW)                                             
      CALL JCLOSE(ISLFCT)                                                       
      END IF                                                                    
      CALL JWRITE(ISLMAT,DV(JRM),NW)                                            
      NEQ=NEQV+NEQV-ISHF                                                        
c     call jprtab                                                               
      CALL LANCZO(DV(JRM),DV(JVC),DV(JVL),DV(JA),DV(JB)                         
     1           ,DV(JC),NEQ,IV(JNA),DV(JTEMP),DV(JTEMP1),NMAX,DV(JVOLD)        
     2           ,NEQ+1,NEQ+1,DV(JZ),NMAX,DV(JRR),DV(JRI),IV(JIANA),NREQ        
     3           ,DV(JVATT),ISLMAT,NW)                                          
c     call jprtab                                                               
      CALL JFREE(*2000,'VATT    ')                                              
      CALL JFREE(*2000,'VOLD    ')                                              
      CALL JFREE(*2000,'TEMP    ')                                              
      CALL JFREE(*2000,'TEMP1   ')                                              
      CALL JFREE(*2000,'Z       ')                                              
      CALL JCLOSE(ISLMAT)                                                       
      CALL JFREE(*2000,'RM      ')                                              
C                                                                               
      CALL JALLOC(*2000,'WORK    ','D.P.',1,(NREQ*5+5)*2 ,JWORK )               
      CALL JALLOC(*2000,'IN      ','INT.',1,NMAX        ,JIN   )                
      CALL JOPEN('*SLE    ',ISLSLE,1)                                           
C                                                                               
      CALL AUTVET(DV(JA),DV(JB),DV(JC),DV(JWORK),DV(JVL),IV(JIN),               
     1         IV(JIANA),DV(JRR),DV(JRI),NAUTOV,NREQ,NREQ,DV(JVC),NEQ+1)        
C                                                                               
      NAOUT=0                                                                   
C                                                                               
      CALL JCLOSE(ISLSLE)                                                       
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JOPEN('*SLE    ',ISLSLE,1)                                           
      NWORD=NEQV*2                                                              
C                                                                               
      CALL JALLOC(*2000,'AUTVT   ','D.P.',1,NWORD      ,JAUTVT)                 
      CALL JALLOC(*2000,'COOR    ','D.P.',1,NNODI*2    ,JCOOR )                 
      CALL JALLOC(*2000,'A       ','D.P.',1,18       ,JA    )                   
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JPOSRL(ISLNOD,NNODI)                                                 
      CALL JREAD(ISLNOD,DV(JCOOR),NNODI*4)                                      
      CALL JCLOSE(ISLNOD)                                                       
C                                                                               
      CALL ORDSLE(DV(JAUTVT),NAUTOV,NEQV,DV(JCOOR),NNODI,DV(JA),NPSI)           
C                                                                               
      CALL JCLOSE(ISLSLE)                                                       
C                                                                               
      RETURN                                                                    
101   WRITE(IOUT,102)                                                           
102   FORMAT(' ***   AVVISO (MD1SLE) : ERRORE NEI DATI PER LE SOLUZIONI'        
     *      ,' DI ESTREMITA''   ***')                                           
 2000 CALL JHALT                                                                
      END                                                                       
C@ELT,I ANBA*ANBA.MD2SLE                                                        
C*************************          MD2SLE          ********************        
      SUBROUTINE MD2SLE                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      DIMENSION DV(1)                                                           
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSL2          
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV                
C                                                                               
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(8),INCOU,IAUTO                                     
      CALL JCRERS                                                               
C                                                                               
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL INTGFF(*101,NAUTOV,0,LENFL)                                          
      CALL DOUBFF(*101,SHIFT,0,LENFL)                                           
 900  FORMAT( )                                                                 
      CALL INTEST                                                               
      WRITE(IOUT,666)NAUTOV,SHIFT                                               
666   FORMAT(////////39X,'***  PARAMETRI DI CALCOLO AUTOSOLUZIONI  ***'/        
     1            //39X,'                 METODO   QR2'//                       
     128X,'N.RO AUTOVALORI ',34('.'),' :',I12/                                  
     128X,'VALORE DI SHIFT ',34('.'),' :',E12.5)                                
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV      ,JNA   )                  
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
C                                                                               
      IF(.NOT.INCORE) THEN                                                      
      NST=1                                                                     
      DO 1 I=2,NEQV                                                             
      NST=NST+I-IV(JNA+I-1)+1                                                   
      IV(JNA+I-1)=NST                                                           
 1    CONTINUE                                                                  
      END IF                                                                    
C                                                                               
      NSTO=IABS(NSTO)                                                           
C                                                                               
      CALL JALLOC(*2000,'RM      ','D.P.',1,NSTO      ,JRM   )                  
      CALL JALLOC(*2000,'RD      ','D.P.',1,NSTO      ,JRD   )                  
      CALL JALLOC(*2000,'RK      ','D.P.',1,NSTO      ,JRK   )                  
C                                                                               
      CALL DZERO(DV(JRM),NSTO)                                                  
      CALL DZERO(DV(JRK),NSTO)                                                  
      CALL DZERO(DV(JRD),NSTO)                                                  
C                                                                               
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1     ,JIPIAZ)               
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
C                                                                               
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
C                                                                               
      CALL AUVASM(DV(JRM),IV(JIPIAZ),DV(JELMAT),                                
     1            IV(JNA),MAXDEL)                                               
c     WRITE(10)NEQV-NPSI                                                        
c     WRITE(10)NSTO                                                             
c     WRITE(10)(DV(JRM+IPOL),IPOL=0,NSTO-1)                                     
C                                                                               
      CALL AUVASD (DV(JRD),IV(JIPIAZ),DV(JELMAT),                               
     1            IV(JNA),MAXDEL)                                               
c     WRITE(10)(DV(JRD+IPOL),IPOL=0,NSTO-1)                                     
C                                                                               
      IEX=JEXIST('*ASM    ')                                                    
      IF(IEX.EQ.0.OR..NOT.INCORE)THEN                                           
      CALL AUVASK(DV(JRK),IV(JIPIAZ),DV(JELMAT),                                
     1            IV(JNA),MAXDEL)                                               
      ELSE                                                                      
      CALL JOPEN('*ASM    ',ISLASM,1)                                           
      CALL JREAD(ISLASM,DV(JRK),NSTO*2)                                         
      CALL JCLOSE(ISLASM)                                                       
C-    DO 66 I=1,NEQV                                                            
C-    JRKIND=IV(JNA+I-1)-1+JRK                                                  
C-    IF(DV(JRKIND).GT.CONST) DV(JRKIND)=DV(JRKIND)-CONST                       
C-66  CONTINUE                                                                  
      END IF                                                                    
c     WRITE(10)(DV(JRD+IPOL),IPOL=0,NSTO-1)                                     
C                                                                               
      CALL JCLOSE(ISLMTN)                                                       
      CALL JCLOSE(ISLMTF)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
C                                                                               
      CALL JFREE(*2000,'ELMAT   ')                                              
      CALL JFREE(*2000,'IPIAZ   ')                                              
      CALL JALLOC(*2000,'CAPPA   ','D.P.',1,NEQV*NEQV   ,JCAPPA)                
      NEMPSI=NEQV-NPSI                                                          
      NE2=NEMPSI+NEMPSI                                                         
      CALL JOPEN('TMP*    ',ISLTMP,1)                                           
      CALL MATDMP(DV(JCAPPA),DV(JRK),IV(JNA),NEQV,0,1.D0,'CAPPA')               
      CALL JFREE(*2000,'RK      ')                                              
      DO 20 I=1,NEMPSI                                                          
      IB=(I-1)*NEQV                                                             
      CALL JWRITE(ISLTMP,DV(JCAPPA+IB),NEMPSI*2)                                
 20   CONTINUE                                                                  
      CALL MATDMP(DV(JCAPPA),DV(JRD),IV(JNA),NEQV,0,-1.D0,'DUMP')               
      CALL JFREE(*2000,'RD      ')                                              
      DO 21 I=1,NEMPSI                                                          
      IB=(I-1)*NEQV                                                             
      CALL JWRITE(ISLTMP,DV(JCAPPA+IB),NEMPSI*2)                                
 21   CONTINUE                                                                  
      CALL MATDMP(DV(JCAPPA),DV(JRM),IV(JNA),NEQV,0,1.D0,'MASS')                
      CALL JFREE(*2000,'RM      ')                                              
      DO 40 I=1,NEMPSI                                                          
      IB=(I-1)*NEQV                                                             
      CALL JWRITE(ISLTMP,DV(JCAPPA+IB),NEMPSI*2)                                
 40   CONTINUE                                                                  
      CALL JCLOSE(ISLTMP)                                                       
      CALL JCRERS                                                               
C                                                                               
      CALL JOPEN('TMP*    ',ISLTMP,1)                                           
      CALL JALLOC(*2000,'RR      ','D.P.',1,NEMPSI*2      ,JRR   )              
      CALL JALLOC(*2000,'RI      ','D.P.',1,NEMPSI*2      ,JRI   )              
      CALL JALLOC(*2000,'RMAT    ','D.P.',1,NEMPSI*NEMPSI*4,JRMAT)              
      CALL JALLOC(*2000,'RK      ','D.P.',1,NEMPSI*NEMPSI  ,JRK  )              
      CALL JALLOC(*2000,'IANA    ','INT.',1,NEMPSI*2      ,JIANA )              
      CALL JALLOC(*2000,'PERM    ','D.P.',1,NEMPSI        ,JPERM )              
C                                                                               
      CALL AUVALR(DV(JRMAT),DV(JRK),DV(JRR),DV(JRI),NEMPSI,NE2,IV(JIANA)        
     1  ,ISLTMP,DV(JPERM))                                                      
C                                                                               
      CALL JFREE(*2000,'PERM    ')                                              
      CALL JFREE(*2000,'IANA    ')                                              
      CALL JFREE(*2000,'RK      ')                                              
      CALL JFREE(*2000,'RMAT    ')                                              
      CALL JALLOC(*2000,'CEIGEN  ','D.P.',1,NE2*NE2*2   ,JCEIGN)                
      CALL JALLOC(*2000,'AUTVET  ','D.P.',1,NE2*2       ,JAUTVT)                
      CALL JALLOC(*2000,'PERM    ','D.P.',1,NE2         ,JPERM )                
C                                                                               
      CALL JOPEN('*SLE    ',ISLSLE,1)                                           
      CALL AUVETM(DV(JCEIGN),NE2,NE2,DV(JRR),DV(JRI),                           
     1            DV(JAUTVT),DV(JPERM),ISLTMP,ISLSLE)                           
C                                                                               
      CALL JCLOSE(ISLSLE)                                                       
      CALL JCLOSE(ISLTMP)                                                       
C                                                                               
      RETURN                                                                    
C                                                                               
101   WRITE(IOUT,102)                                                           
102   FORMAT(' ***   AVVISO (MD2SLE) : ERRORE NEI DATI PER LE SOLUZIONI'        
     *      ,' DI ESTREMITA''   ***')                                           
 2000 CALL JHALT                                                                
      END                                                                       
C@ELT,I ANBA*ANBA.MD1INS                                                        
C*************************          MD1INS          ********************        
      SUBROUTINE MD1INS                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      DIMENSION DV(1),COEF(6)                                                   
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSL2          
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      DO 34 I=1,NPSI                                                            
       CALL DOUBFF(*101,COEF(I),0,LENFL)                                        
  34  CONTINUE                                                                  
      WRITE(IOUT,222)(COEF(I),I=1,NPSI)                                         
222   FORMAT(///10X,' COEFFICIENTI DI COMBINAZIONE ',//8X,6E12.5/)              
C                                                                               
      NWELMA=MAXDEL*MAXDEL*3*NPSI                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXNOD,JICON )                      
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,NWELMA,JELMAT)                      
C                                                                               
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*MBK    ',ISLMBK,1)                                           
      CALL JOPEN('*SFC    ',ISLSFC,1)                                           
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
      CALL JOPEN('*ELS    ',ISLELS,1)                                           
C                                                                               
      CALL DZERO(DV(JELMAT),NWELMA)                                             
      CALL IZERO(IV(JICON),MAXNOD)                                              
C                                                                               
      CALL VTPZBK(IV(JICON),                                                    
     *            DV(JELMAT),MAXDEL,ISLELM,ISLELS,ISLSFC,ISLVPZ,ISLMBK)         
      CALL JCLOSE(ISLELS)                                                       
      CALL JCLOSE(ISLELM)                                                       
      CALL JCLOSE(ISLSFC)                                                       
      CALL JCLOSE(ISLMBK)                                                       
      CALL JCLOSE(ISLVPZ)                                                       
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV     ,JNA   )                   
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
      IF(.NOT.INCORE) THEN                                                      
       NSTO=1                                                                   
       DO 1 I=2,NEQV                                                            
        NSTO=NSTO+I-IV(JNA+I-1)+1                                               
        IV(JNA+I-1)=NSTO                                                        
 1     CONTINUE                                                                 
      END IF                                                                    
C                                                                               
      NSTO=IABS(NSTO)                                                           
      NW=NSTO+NSTO                                                              
      CALL JALLOC(*2000,'RM      ','D.P.',1,NSTO      ,JRM   )                  
C                                                                               
      CALL JOPEN('*MAT    ',ISLMAT,1)                                           
      CALL JALLOC(*2000,'IPIAZ   ','INT.',1,MAXDEL+1 ,JIPIAZ)                   
      CALL JALLOC(*2000,'ELMAT   ','D.P.',1,MAXDEL*MAXDEL,JELMAT)               
C                                                                               
      CALL JOPEN('*VPZ    ',ISLVPZ,1)                                           
      CALL JOPEN('*MTF    ',ISLMTF,1)                                           
      CALL JOPEN('*MTN    ',ISLMTN,1)                                           
C                                                                               
C                                                                               
      CALL DZERO(DV(JRM),NSTO)                                                  
      CALL AUVASM(DV(JRM),IV(JIPIAZ),DV(JELMAT),IV(JNA),MAXDEL)                 
      CALL JWRITE(ISLMAT,DV(JRM),NW)                                            
      CALL DZERO(DV(JRM),NSTO)                                                  
      CALL AUVASD (DV(JRM),IV(JIPIAZ),DV(JELMAT),IV(JNA),MAXDEL)                
      CALL JWRITE(ISLMAT,DV(JRM),NW)                                            
      IEX=JEXIST('*ASM    ')                                                    
      IF(IEX.EQ.0.OR..NOT.INCORE)THEN                                           
       CALL DZERO(DV(JRM),NSTO)                                                 
       CALL AUVASK(DV(JRM),IV(JIPIAZ),DV(JELMAT),IV(JNA),MAXDEL)                
      ELSE                                                                      
       CALL JOPEN('*ASM    ',ISLASM,1)                                          
       CALL JREAD(ISLASM,DV(JRM),NW)                                            
       CALL JCLOSE(ISLASM)                                                      
      END IF                                                                    
       CALL JCLOSE(ISLMTF)                                                      
       CALL JCLOSE(ISLMTN)                                                      
C                                                                               
      CALL JWRITE(ISLMAT,DV(JRM),NW)                                            
C                                                                               
      CALL JOPEN('*MBK    ',ISLMBK,1)                                           
      JUK=JRM                                                                   
      CALL JALLOC(*2000,'UD      ','D.P.',1,NSTO      ,JUD   )                  
      CALL JALLOC(*2000,'UM      ','D.P.',1,NSTO      ,JUM   )                  
      CALL JALLOC(*2000,'VETT    ','D.P.',1,MAXDEL    ,JVETT )                  
      CALL DZERO(DV(JUK),NSTO)                                                  
      CALL DZERO(DV(JUD),NSTO)                                                  
      CALL DZERO(DV(JUM),NSTO)                                                  
      CALL ASSUBK(DV(JUK),DV(JUD),DV(JUM),IV(JIPIAZ),DV(JELMAT),IV(JNA)         
     *,MAXDEL,DV(JVETT),COEF,ISLVPZ,ISLMBK)                                     
      CALL JWRITE(ISLMAT,DV(JUM),NW)                                            
      CALL JWRITE(ISLMAT,DV(JUD),NW)                                            
      CALL JWRITE(ISLMAT,DV(JUK),NW)                                            
C                                                                               
      CALL JCLOSE(ISLVPZ)                                                       
      CALL JCLOSE(ISLMBK)                                                       
      CALL JCLOSE(ISLMAT)                                                       
      CALL JFREE(*2000,'ELMAT   ')                                              
      CALL JFREE(*2000,'IPIAZ   ')                                              
C                                                                               
      RETURN                                                                    
101   WRITE(IOUT,102)                                                           
102   FORMAT(' ***   AVVISO (MD1INS) : ERRORE NEI DATI PER LA INIZIALIZ'        
     *      ,'ZAZIONE DELLA SOLUZIONE DI INSTABILITA''   ***')                  
 2000 CALL JHALT                                                                
      END                                                                       
C@ELT,I ANBA*ANBA.MD2INS                                                        
C*************************          MD2INS          ********************        
      SUBROUTINE MD2INS                                                         
      IMPLICIT REAL*8 (A-H,O-Z)                                                 
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      DIMENSION DV(1)                                                           
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQS,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLMPC,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSL2          
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV                
C                                                                               
      COMMON /AUTCON/ ERR,CONST,XS,YS,SS,CS,PGCON(6),CDC(6),                    
     *IAUTCN,IPGCON(6),MODEX(7),INCOU,IAUTO                                     
      CALL JCRERS                                                               
C                                                                               
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL READFF(ISYM,0,NF,*101,*101)                                          
      CALL DOUBFF(*101,SHIFT,0,LENFL)                                           
      CALL DOUBFF(*101,RHO,0,LENFL)                                             
 900  FORMAT( )                                                                 
      CALL INTEST                                                               
      WRITE(IOUT,666)SHIFT,RHO                                                  
666   FORMAT(////////39X,'***  PARAMETRI DI CALCOLO INSTABILITA''   ***'        
     1            //39X,'                 METODO   QR2'//                       
     128X,'VALORE DI SHIFT ',34('.'),' :',E12.5/                                
     128X,'COEFF. MOLTIPLICATIVO ',28('.'),' :',E12.5)                          
      CALL JALLOC(*2000,'NA      ','INT.',1,NEQV      ,JNA   )                  
      CALL JOPEN('*MRG    ',ISLMRG,1)                                           
      CALL JREAD(ISLMRG,IV(JNA),NEQV)                                           
      CALL JCLOSE(ISLMRG)                                                       
C                                                                               
      IF(.NOT.INCORE) THEN                                                      
      NSTO=1                                                                    
      DO 1 I=2,NEQV                                                             
      NSTO=NSTO+I-IV(JNA+I-1)+1                                                 
      IV(JNA+I-1)=NSTO                                                          
 1    CONTINUE                                                                  
      END IF                                                                    
C                                                                               
      NSTO=IABS(NSTO)                                                           
C                                                                               
      CALL JALLOC(*2000,'RM      ','D.P.',1,NSTO      ,JRM   )                  
      CALL JALLOC(*2000,'RD      ','D.P.',1,NSTO      ,JRD   )                  
      CALL JALLOC(*2000,'RK      ','D.P.',1,NSTO      ,JRK   )                  
      CALL JALLOC(*2000,'COOR    ','D.P.',1,NNODI*2   ,JCOOR )                  
      CALL JOPEN('*NOD    ',ISLNOD,1)                                           
      CALL JPOSRL(ISLNOD,NNODI)                                                 
      CALL JREAD(ISLNOD,DV(JCOOR),NNODI*4)                                      
      CALL JCLOSE(ISLNOD)                                                       
      CALL JALLOC(*2000,'TRAS    ','D.P.',1,NNODI*9   ,JTRAS )                  
      CALL JALLOC(*2000,'TEMP    ','D.P.',1,NNODI*9   ,JTEMP )                  
      CALL JALLOC(*2000,'COND    ','D.P.',1,9         ,JCOND )                  
C                                                                               
      CALL TTTRRR(DV(JCOOR),NNODI,DV(JTRAS))                                    
      CALL JOPEN('*MAT    ',ISLMAT,1)                                           
      CALL JREAD(ISLMAT,DV(JRM),NSTO*2)                                         
C+    CALL MATPRT(IV(JNA),DV(JRM),NEQV,' MATR M ESTREMITA')                     
      CALL CONDES(DV(JRM),IV(JNA),DV(JTRAS),DV(JTEMP),DV(JCOND))                
      CALL DWRITE(' ME COND',DV(JCOND),3,3,3,1)                                 
      CALL JREAD(ISLMAT,DV(JRD),NSTO*2)                                         
C+    CALL MATPRT(IV(JNA),DV(JRD),NEQV,' MATR D ESTREMITA')                     
      CALL CONDEA(DV(JRD),IV(JNA),DV(JTRAS),DV(JTEMP),DV(JCOND))                
      CALL DWRITE(' DE COND',DV(JCOND),3,3,3,1)                                 
      CALL JREAD(ISLMAT,DV(JRK),NSTO*2)                                         
C+    CALL MATPRT(IV(JNA),DV(JRK),NEQV,' MATR K ESTREMITA')                     
      CALL CONDES(DV(JRK),IV(JNA),DV(JTRAS),DV(JTEMP),DV(JCOND))                
      CALL DWRITE(' KE COND',DV(JCOND),3,3,3,1)                                 
      CALL JALLOC(*2000,'TEMPO   ','D.P.',1,NSTO,JTEMPO)                        
      CALL JREAD(ISLMAT,DV(JTEMPO),NSTO*2)                                      
C+    CALL MATPRT(IV(JNA),DV(JTEMPO),NEQV,' MATR M INSTABIL')                   
      JTP=JTEMPO                                                                
      CALL CONDES(DV(JTP),IV(JNA),DV(JTRAS),DV(JTEMP),DV(JCOND))                
      CALL DWRITE(' MI COND',DV(JCOND),3,3,3,1)                                 
      DO 1001 L=0,NSTO-1                                                        
       DV(JRM+L)=DV(JRM+L)+DV(JTEMPO+L)*RHO                                     
1001  CONTINUE                                                                  
      CALL JREAD(ISLMAT,DV(JTEMPO),NSTO*2)                                      
C     CALL MATPRT(IV(JNA),DV(JTEMPO),NEQV,' MATR H INSTABIL')                   
      DO 1002 L=0,NSTO-1                                                        
       DV(JRD+L)=DV(JRD+L)+DV(JTEMPO+L)*RHO                                     
1002  CONTINUE                                                                  
      CALL JREAD(ISLMAT,DV(JTEMPO),NSTO*2)                                      
C     CALL MATPRT(IV(JNA),DV(JTEMPO),NEQV,' MATR K INSTABIL')                   
      DO 1003 L=0,NSTO-1                                                        
       DV(JRK+L)=DV(JRK+L)+DV(JTEMPO+L)*RHO                                     
1003  CONTINUE                                                                  
      CALL JFREE(*2000,'TEMPO   ')                                              
      CALL JCLOSE(ISLMAT)                                                       
C                                                                               
      CALL JALLOC(*2000,'CAPPA   ','D.P.',1,NEQV*NEQV   ,JCAPPA)                
      NEMPSI=NEQV-NPSI                                                          
      NE2=NEMPSI+NEMPSI                                                         
      CALL JOPEN('TMP*    ',ISLTMP,1)                                           
      CALL MATDMP(DV(JCAPPA),DV(JRK),IV(JNA),NEQV,0,1.D0,'CAPPA')               
C     CALL DWRITE(' CAPPA  ',DV(JCAPPA),NEQV,NEQV,NEQV,1)                       
      CALL JFREE(*2000,'RK      ')                                              
      DO 20 I=1,NEMPSI                                                          
       IB=(I-1)*NEQV                                                            
       CALL JWRITE(ISLTMP,DV(JCAPPA+IB),NEMPSI*2)                               
 20   CONTINUE                                                                  
      CALL MATDMP(DV(JCAPPA),DV(JRD),IV(JNA),NEQV,0,-1.D0,'DUMP')               
C     CALL DWRITE(' DUMP   ',DV(JCAPPA),NEQV,NEQV,NEQV,1)                       
      CALL JFREE(*2000,'RD      ')                                              
      DO 21 I=1,NEMPSI                                                          
       IB=(I-1)*NEQV                                                            
       CALL JWRITE(ISLTMP,DV(JCAPPA+IB),NEMPSI*2)                               
 21   CONTINUE                                                                  
      CALL MATDMP(DV(JCAPPA),DV(JRM),IV(JNA),NEQV,0,1.D0,'MASS')                
C     CALL DWRITE(' MASS   ',DV(JCAPPA),NEQV,NEQV,NEQV,1)                       
      CALL JFREE(*2000,'RM      ')                                              
      DO 40 I=1,NEMPSI                                                          
       IB=(I-1)*NEQV                                                            
       CALL JWRITE(ISLTMP,DV(JCAPPA+IB),NEMPSI*2)                               
 40   CONTINUE                                                                  
      CALL JCLOSE(ISLTMP)                                                       
      CALL JCRERS                                                               
C                                                                               
      CALL JOPEN('TMP*    ',ISLTMP,1)                                           
      CALL JALLOC(*2000,'RR      ','D.P.',1,NEMPSI*2      ,JRR   )              
      CALL JALLOC(*2000,'RI      ','D.P.',1,NEMPSI*2      ,JRI   )              
      CALL JALLOC(*2000,'RMAT    ','D.P.',1,NEMPSI*NEMPSI*4,JRMAT)              
      CALL JALLOC(*2000,'RK      ','D.P.',1,NEMPSI*NEMPSI  ,JRK  )              
      CALL JALLOC(*2000,'IANA    ','INT.',1,NEMPSI*2      ,JIANA )              
      CALL JALLOC(*2000,'PERM    ','D.P.',1,NEMPSI        ,JPERM )              
C                                                                               
      CALL AUVALR(DV(JRMAT),DV(JRK),DV(JRR),DV(JRI),NEMPSI,NE2,IV(JIANA)        
     1  ,ISLTMP,DV(JPERM))                                                      
C                                                                               
      CALL JPOSAB(ISLTMP,1)                                                     
      CALL JFREE(*2000,'PERM    ')                                              
      CALL JFREE(*2000,'IANA    ')                                              
      CALL JFREE(*2000,'RK      ')                                              
      CALL JFREE(*2000,'RMAT    ')                                              
      CALL JALLOC(*2000,'CEIGEN  ','D.P.',1,NE2*NE2*2   ,JCEIGN)                
      CALL JALLOC(*2000,'AUTVET  ','D.P.',1,NE2*2       ,JAUTVT)                
      CALL JALLOC(*2000,'PERM    ','D.P.',1,NE2         ,JPERM )                
C                                                                               
      CALL JOPEN('*SLE    ',ISLSLE,1)                                           
      CALL AUVETM(DV(JCEIGN),NE2,NE2,DV(JRR),DV(JRI),                           
     1            DV(JAUTVT),DV(JPERM),ISLTMP,ISLSLE)                           
C                                                                               
      CALL JCLOSE(ISLSLE)                                                       
      CALL JCLOSE(ISLTMP)                                                       
C                                                                               
C                                                                               
      RETURN                                                                    
C                                                                               
101   WRITE(IOUT,102)                                                           
102   FORMAT(' ***   AVVISO (MD2INS) : ERRORE NEI DATI PER IL CALCOLO'          
     *      ,' DEGLI AUTOVALORI DI INSTABILITA''   ***')                        
 2000 CALL JHALT                                                                
      END                                                                       
C@ELT,I ANBA*ANBA.MODSFE                                                        
C*************************          MODSFE          ********************        
C     COMPILER (ARGCHK=OFF)                                                     
      SUBROUTINE MODSFE                                                         
      DOUBLE PRECISION DV(1),TOLL,ORTER,ZERTOL,APREA,APIMM,SHIFT                
      LOGICAL DUMP,INCORE                                                       
      COMMON IV(1)                                                              
      EQUIVALENCE (IV(1),DV(1))                                                 
      COMMON /PARAM/ NELEM,NCS,NPSI,NNODI,NMPC,NEQV,NEQT,NODI,MEMDIS,           
     1               NELMAX,MAXDEL,MAXFRO,LMXMPC,MAXLRG,MAXNOD,NEQA,NSTO        
      COMMON /JCRMNG/ MAXDIM                                                    
      COMMON /JFTNIO/ INPT,IOUT,ISYM,DUMP(22),INCORE,ILOCA                      
      COMMON /ISLOTS/ ISLNOD,ISLELS,ISLNWD,ISLELM,ISLNUM,ISLTAB,ISLVPZ,         
     *                ISLMTN,ISLMTF,ISLASM,ISLFCT,ISLSL1,ISLSLE,ISLSFE          
      COMMON /ELGEN/  ITIP,IDEL,NCON,IFLMR                                      
      COMMON /AUTOVL/ TOLL,ORTER,ZERTOL,SHIFT,MAXITE,INCR,NAUTOV                
      COMMON /PARELM/ NELTOT,NCOR,NGIU,NPAN,NLAM,NELP,                          
     1            NELRIG,NCORR,NGIUR,NPANR,NLAMR,NELPR,NSPC,                    
     2            NWC,NWG,NWP,NWL,NWE,NWV                                       
      NSOL=2                                                                    
      NRSOL=NEQV+NEQV-NPSI                                                      
C                                                                               
      CALL JCRERS                                                               
C                                                                               
      CALL JALLOC(*2000,'NUME    ','INT.',1,NEQT      ,JNUME )                  
      CALL JALLOC(*2000,'NUINT   ','INT.',1,NNODI+NMPC,JNUINT)                  
      CALL JOPEN('*NUM',ISLNUM,1)                                               
      CALL JREAD(ISLNUM,IV(JNUME),NEQT)                                         
      CALL JREAD(ISLNUM,IV(JNUINT),NNODI+NMPC)                                  
      CALL JCLOSE(ISLNUM)                                                       
C                                                                               
      CALL JOPEN('*SLE    ',ISLSLE,1)                                           
      CALL JOPEN('*ELM    ',ISLELM,1)                                           
      CALL JOPEN('*ELS    ',ISLELS,1)                                           
      CALL JOPEN('*SFE    ',ISLSFE,1)                                           
C                                                                               
      CALL JALLOC(*2000,'ICON    ','INT.',1,MAXNOD    ,JICON )                  
      CALL JALLOC(*2000,'XY      ','D.P.',1,MAXNOD*4  ,JXY   )                  
      CALL JALLOC(*2000,'SIGMAG  ','D.P.',1,NSOL*NPSI ,JSIGMG)                  
      CALL JALLOC(*2000,'SIGMAL  ','D.P.',1,NSOL*NPSI ,JSIGML)                  
      CALL JALLOC(*2000,'EPSI    ','D.P.',1,NPSI*NPSI ,JEPSI )                  
      CALL JALLOC(*2000,'D       ','D.P.',1,6*6       ,JD    )                  
      CALL JALLOC(*2000,'SOL     ','D.P.',1,NRSOL*NSOL,JSOL  )                  
C                                                                               
C                                                                               
      DO 10 INAUTV=1,NAUTOV                                                     
C                                                                               
      NE=0                                                                      
C                                                                               
      CALL INTEST                                                               
      CALL RCPSLE(DV(JSOL),NRSOL,NSOL,APREA,APIMM,ITIPO,INAUTV)                 
C                                                                               
      CALL JREAD(ISLELM,ITIP,1)                                                 
      CALL JPOSRL(ISLELM,-1)                                                    
      IF(ITIP.NE.1) GOTO 2                                                      
C++   CALL SFECOR(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),NSOL,NRSOL)          
      NE=NE+NCOR                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
      CALL JREAD(ISLELM,ITIP,1)                                                 
      CALL JPOSRL(ISLELM,-1)                                                    
C                                                                               
2     IF(ITIP.NE.2) GOTO 3                                                      
C++   CALL SFEGIU(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),IV(JICON),           
C++  1            DV(JXY),NSOL,NRSOL)                                           
      NE=NE+NGIU                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
      CALL JREAD(ISLELM,ITIP,1)                                                 
      CALL JPOSRL(ISLELM,-1)                                                    
C                                                                               
3     IF(ITIP.NE.3) GOTO 4                                                      
C++   CALL SFEPAN(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
C++  1            IV(JICON),DV(JXY),NSOL,NRSOL,DV(JEPSI),DV(JD))                
      NE=NE+NPAN                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
      CALL JREAD(ISLELM,ITIP,1)                                                 
      CALL JPOSRL(ISLELM,-1)                                                    
C                                                                               
4     IF(ITIP.NE.4) GOTO 5                                                      
C++   CALL SFELAM(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
C++  1            DV(JEPSI),DV(JD),IV(JICON),DV(JXY),NSOL,NRSOL)                
      NE=NE+NLAM                                                                
      IF(NE.EQ.NELTOT) GOTO 7                                                   
      CALL JREAD(ISLELM,ITIP,1)                                                 
      CALL JPOSRL(ISLELM,-1)                                                    
C                                                                               
5     IF(ITIP.NE.5) GOTO 7                                                      
C++   CALL SFEELP(IV(JNUME),IV(JNUINT),DV(JSOL),DV(JSIGMG),DV(JSIGML),          
C++  1            DV(JEPSI),DV(JD),IV(JICON),DV(JXY),NSOL,NRSOL)                
7     CONTINUE                                                                  
C                                                                               
      CALL JPOSAB(ISLELS,1)                                                     
      CALL JPOSAB(ISLELM,1)                                                     
C                                                                               
 10   CONTINUE                                                                  
C                                                                               
C                                                                               
C                                                                               
      CALL JCLOSE(ISLSFE)                                                       
      CALL JCLOSE(ISLELS)                                                       
      CALL JCLOSE(ISLELM)                                                       
      CALL JCLOSE(ISLSLE)                                                       
      RETURN                                                                    
2000  CALL JHALT                                                                
      END                                                                       
